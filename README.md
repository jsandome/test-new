# CAPA investor platform

commands:

- INIT.- Este comando actualiza las dependencias globales y las dependencias de cada package que conforma al proyecto

```bash
[~] $ npm run init
[~] $ npm run build
[~] $ npm run start
[~] $ npm run test
```

---

- ADD.- Este comando agrega nuevas dependencias a un paquete en específico

```bash
[~] $ npx lerna add <<package>> --scope=<<localPackage>>
```

Ejemplo:

```bash
[~] $ npx lerna add lodash --scope=@capa/core
```

---

si se quiere agregar un nuevo paquete a todos los paquetes se ejecuta de la siguiente manera:

```bash
[~] $ npx lerna add <<package>>
```

Ejemplo:

```bash
[~] $ npx lerna add lodash
```

---

- COMPILE.- Este comando compila todos los proyectos y actualiza las dependencias en cada uno:

```bash
[~] $ npm run build
```

## Enviroment variables

### Environment

```conf
process.env.NODE_ENV
```

### JWT

```conf
process.env.SECRETWORD
process.env.SESSIONTIME
process.env.EXPIREDAYSTOKEN
```

### MongoDB

```conf
process.env.DBNAME
process.env.DBPASS
process.env.DBURL
process.env.DBUSER
process.env.MONGOB_SRV
process.env.DBHOST
```

### Firebase

```conf
process.env.FBPROJECTID
process.env.FBPRIVATEKEY
process.env.FBCLIENTEMAIL
process.env.FBBUCKET
process.env.FBDATABASEURL
```

### Google Cloud Storage

```conf
process.env.FBPRIVATEKEY
process.env.FBCLIENTEMAIL
process.env.FBBUCKET
```

### Email

```conf
process.env.SMTPHOST
process.env.SMTPUSER
process.env.SMTPPASSWORD
process.env.SMTPPORT
```

### Property General Parameters

```conf
process.env.CAPITALGAINSTAXPAID
```
