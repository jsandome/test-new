import { EnumTwoFactorType, EnumUserTypes, IBaseName } from '../common';

export interface IAuthLoginIn {
  user: string;
  password: string;
  type: EnumUserTypes;
}
export interface IAuthLoginSingleOut {
  twoFactor: boolean;
  name: IBaseName;
  customToken: string;
  id: string;
  userToken: string;
  type: string;
}
export interface IAuthLoginEmailOut {
  twoFactor: boolean;
  secure: string;
  twoFactorType: EnumTwoFactorType;
}
export interface IAuthLoginPhoneOut {
  twoFactor: boolean;
  secure: string;
  phone: string;
  twoFactorType: EnumTwoFactorType;
}
export interface IAuthRecoveryPassword {
  email: string;
}
export interface IAuthRestorePassword {
  token: string;
  password: string;
}
export interface IAuthChangePassword {
  currentPassword: string;
  newPassword: string;
}
export interface IAuthVerifyCode {
  user: string;
  secure: string;
  code: string;
}
export interface IAuthVerifyCodeOut {
  name: IBaseName;
  customToken: string;
  id: string;
  type: string;
}
export interface IAuth2FAcodeOut {
  code: string;
}
export interface IAuth2FAVerifyTokenIn {
  userId: string;
  token: string;
  secret: string;
}
