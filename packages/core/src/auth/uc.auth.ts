import {
  IBnd2FAInfra,
  IBndBaseTokenInfra,
  IBndMailerInfra,
  IJwtParamsTokenIn,
  IPasswordParams,
} from '../infra';
import {
  BaseError,
  EnumTwoFactorType,
  EnumUserTypes,
  IBaseEntityUser,
  IBaseName,
} from '../common';
import { IBndBaseUserRead, IBndBaseUserWrite } from '../common/boundaries';
import {
  IAuthLoginIn,
  IAuthLoginEmailOut,
  IAuthLoginPhoneOut,
  IAuthLoginSingleOut,
  IAuthRecoveryPassword,
  IAuthChangePassword,
  IAuthVerifyCodeOut,
  IAuthRestorePassword,
  IAuthVerifyCode,
  IAuth2FAcodeOut,
} from '.';
import { IBndAdminRead } from '../admin';
import { IBndInvestorRead } from '../investor';
import { secureCode } from '../common/ReExp/re.exp.secure';
import { IAuth2FAVerifyTokenIn } from '.';

export class UseCaseAuth {
  constructor(
    private bndUserRead: IBndBaseUserRead,
    private bndUserWrite: IBndBaseUserWrite,
    private bndAdminRead: IBndAdminRead,
    private bndInvesRead: IBndInvestorRead,
    private bndInfra: IBndBaseTokenInfra,
    private bndMailer: IBndMailerInfra,
    private bnd2FA: IBnd2FAInfra
  ) {}

  private async _returnSingleLogin(
    user: IBaseEntityUser
  ): Promise<IAuthLoginSingleOut> {
    let name: IBaseName = null;
    let type: EnumUserTypes;

    if (user.admin) {
      const admin = await this.bndAdminRead.getById(user.admin);
      name = admin.name;
      type = admin.type;
    }
    if (user.investor) {
      const investor = await this.bndInvesRead.getById(user.investor);
      name = investor.name;
      type = investor.type;
    }
    const userToken = await this.bndInfra.generateToken({
      id: user.id,
      email: user.email,
      type: user.type,
    });

    await this.bndUserWrite.updateJwt({
      id: user.id,
      tokenJwt: userToken,
    });
    const customToken = await this.bndInfra.generateTokenFireBase(user.type);
    return {
      name,
      twoFactor: false,
      customToken,
      id: user.id,
      userToken,
      type,
    };
  }
  private async _return2FAEmail(
    user: IBaseEntityUser
  ): Promise<IAuthLoginEmailOut> {
    const secureData = this.bndInfra.generateSecure(user.id);
    const code = this.bndInfra.generateCode();
    await this.bndUserWrite.updateSecure({
      id: user.id,
      date: secureData.expireDateToken,
      secure: secureData.expireToken,
      code,
    });

    // Send the mail with code
    const url = '/';
    const dataToMail = {
      to: user.email,
      subject: 'CAPA Login verification',
      template: '/client-verify-code/html',
      data: { code, url },
    };

    this.bndMailer.send(dataToMail);

    return {
      secure: secureData.expireToken,
      twoFactor: true,
      twoFactorType: EnumTwoFactorType.EMAIL,
    };
  }
  private async _return2FAPhone(
    user: IBaseEntityUser
  ): Promise<IAuthLoginPhoneOut> {
    const secureData = this.bndInfra.generateSecure(user.id);
    let phone: string = '';
    if (user.admin) {
      const admin = await this.bndAdminRead.getById(user.admin);
      phone = admin.phone;
    }
    if (user.investor) {
      const investor = await this.bndInvesRead.getById(user.investor);
      phone = investor.phone;
    }
    await this.bndUserWrite.updateSecure({
      id: user.id,
      date: secureData.expireDateToken,
      secure: secureData.expireToken,
      code: null,
    });
    return {
      secure: secureData.expireToken,
      twoFactor: true,
      twoFactorType: EnumTwoFactorType.PHONE,
      phone,
    };
  }

  public async execLogin(
    params: IAuthLoginIn
  ): Promise<IAuthLoginSingleOut | IAuthLoginEmailOut | IAuthLoginPhoneOut> {
    const user = await this.bndUserRead.getUserByEmailGETPASS(params.user);
    if (!user || !user.active)
      throw new BaseError(
        'The username or password is not correct.',
        401,
        'LoginError'
      );
    if (user.type === EnumUserTypes.SUPERADMIN) user.type = EnumUserTypes.ADMIN;

    const validPass = await this.bndInfra.validPassword({
      hashPassword: user.password,
      inputPassword: params.password,
    });
    if (!validPass)
      throw new BaseError(
        'The username or password is not correct.',
        401,
        'LoginError'
      );
    if (params.type !== user.type)
      throw new BaseError(
        'The resource is not available to the user.',
        403,
        'TypeUserNotValid'
      );

    if (!user.twoFactor.enabled) return this._returnSingleLogin(user);

    if (
      user.twoFactor.enabled &&
      user.twoFactor.type === EnumTwoFactorType.EMAIL
    )
      return this._return2FAEmail(user);

    return this._return2FAPhone(user);
  }

  public async execLogout(id: string): Promise<void> {
    await this.bndUserWrite.clearSession(id);
  }

  public async execSendMailRecovery(
    params: IAuthRecoveryPassword
  ): Promise<boolean> {
    const user = await this.bndUserRead.getLoginDataByEmail(params.email);

    if (user) {
      const token = this.bndInfra.generateExpireDataToken(user.id);

      await this.bndUserWrite.updateExpirePass(user.id, token);

      let url = `/new-password?token=${token.expireToken}`;
      if (
        user.type === EnumUserTypes.ADMIN ||
        user.type === EnumUserTypes.SUPERADMIN
      )
        url = `/admin${url}`;

      const dataToMail = {
        to: params.email,
        subject: 'Mail to recovery password',
        template: '/client-new-password/html',
        data: { url },
      };

      this.bndMailer.send(dataToMail);
    }
    return true;
  }

  public async execRestorePassword(
    params: IAuthRestorePassword
  ): Promise<boolean> {
    const { token, password } = params;
    const user = await this.bndUserRead.getUserByToken(token);

    if (!user)
      throw new BaseError('Token is incorrect.', 401, 'execRestorePassword');

    if (user.expirePass.date < new Date())
      throw new BaseError('The token has expired.', 403, 'execRestorePassword');

    try {
      const newPassword = await this.bndInfra.generateHash(password);
      await this.bndUserWrite.updatePassword({
        email: user.email,
        password: newPassword,
      });

      return true;
    } catch (e) {
      throw new BaseError(
        'The users password could not be updated.',
        400,
        'execRestorePassword'
      );
    }
  }

  public async execChangePassword(
    params: IAuthChangePassword,
    email: string
  ): Promise<boolean> {
    const { currentPassword, newPassword } = params;

    try {
      const user = await this.bndUserRead.getUserByEmailGETPASS(email);

      if (!user)
        throw new BaseError('User not found.', 400, 'execChangePassword');

      const passwordData = {
        inputPassword: currentPassword,
        hashPassword: user.password,
      } as IPasswordParams;

      const validPass = await this.bndInfra.validPassword(passwordData);

      if (!validPass) {
        throw new BaseError(
          'Password is incorrect.',
          401,
          'execChangePassword'
        );
      }

      const updatedPassword = await this.bndInfra.generateHash(newPassword);

      await this.bndUserWrite.changePassword(email, updatedPassword);

      return true;
    } catch (error) {
      const msgError = 'The users password could not be updated.';
      throw new BaseError(msgError, 400, 'execRestorePassword', error);
    }
  }

  public async execVerifyCode(
    params: IAuthVerifyCode
  ): Promise<IAuthVerifyCodeOut> {
    const { user, secure, code } = params;
    const userSecure = await this.bndUserRead.getUserBySecure(user, secure);
    const customToken = await this.bndInfra.generateTokenFireBase(
      userSecure.type
    );

    if (!userSecure) {
      throw new BaseError(
        'The secure code is not correct.',
        401,
        'execVerifyCode'
      );
    }

    let name: IBaseName = null;
    let type: EnumUserTypes;
    if (userSecure.admin) {
      const admin = await this.bndAdminRead.getById(userSecure.admin);
      name = admin.name;
      type = admin.type;
    }
    if (userSecure.investor) {
      const investor = await this.bndInvesRead.getById(userSecure.investor);
      name = investor.name;
      type = investor.type;
    }

    // Verify code by email
    if (userSecure.twoFactor.type == EnumTwoFactorType.EMAIL) {
      if (
        userSecure.twoFactor.code.match(secureCode) &&
        userSecure.twoFactor.code === code
      ) {
        return { name, customToken, id: userSecure.admin, type };
      } else {
        throw new BaseError(
          'The 6 digits code is not valid.',
          401,
          'execVerifyCode'
        );
      }
    } else if (userSecure.twoFactor.type == EnumTwoFactorType.PHONE) {
      // TODO Implement verify code by phone
      return { name, customToken, id: userSecure.admin, type };
    }

    return { name, customToken, id: userSecure.admin, type };
  }

  public async createTokenFireBase(user: string): Promise<string> {
    return await this.bndInfra.generateTokenFireBase(user);
  }

  //TODO new 2FA
  public async execCreateCode(
    params: IJwtParamsTokenIn
  ): Promise<IAuth2FAcodeOut> {
    const code2FA = await this.bnd2FA.createCode(params);
    await this.bndUserWrite.update2FASecret(params.id, code2FA.secret);
    return {
      code: code2FA.qrCode,
    };
  }
  public async execValidCode(params: IAuth2FAVerifyTokenIn): Promise<void> {
    const secret = await this.bndUserRead.get2FASECRET(params.userId);
    if (!secret)
      throw new BaseError('Token is incorrect.', 401, 'execValidCode');
    const code2FA = await this.bnd2FA.validCode(secret, params.token);
    if (!code2FA)
      throw new BaseError('Token is incorrect.', 401, 'execValidCode');
  }
}
