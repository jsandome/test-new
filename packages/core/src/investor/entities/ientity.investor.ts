import { IBaseName } from '../../common';
import { EnumUserTypes } from '../../common/enums/enum.users';

export interface IEntityInvestor {
  id?: string;
  name: IBaseName;
  phone: string;
  active: boolean;
  type: EnumUserTypes;
}
