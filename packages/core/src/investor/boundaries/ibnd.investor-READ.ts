import { IEntityInvestor } from '../entities';

export interface IBndInvestorRead {
  getById(id: string): Promise<IEntityInvestor>;
}
