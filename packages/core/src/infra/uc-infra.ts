import {
  BaseError,
  EnumUserTypes,
  IBndBaseUserRead,
  IBndOnboardingRead,
} from '../common';
import {
  IBndBaseTokenInfra,
  IJwtParamsTokenIn,
  IJwtParamsTokenOnboarding,
} from '.';
import { DateTime } from 'luxon';
export class BaseUseCaseInfra {
  constructor(
    private bndInfra: IBndBaseTokenInfra,
    private bndUserRead: IBndBaseUserRead,
    private bndOnboardingToken: IBndOnboardingRead
  ) {}

  public async execValidTokenJwt(
    token: string,
    type: string
  ): Promise<IJwtParamsTokenIn> {
    const decoded = await this.bndInfra.validJwtToken(token);
    const userExists = await this.bndUserRead.getById(decoded.id);
    const validSession = await this.bndUserRead.validateSessionToken(
      token,
      decoded.id
    );

    if (!validSession)
      throw new BaseError(
        'Your account has been opened in another device.',
        403,
        'UserDoesntPermissions'
      );

    if (!userExists)
      throw new BaseError('Invalid session.', 401, 'UserDoesntExistsError');

    if (
      (userExists.type !== EnumUserTypes.INVESTOR &&
        type === EnumUserTypes.ADMIN) ||
      type !== EnumUserTypes.ADMIN
    ) {
      return decoded;
    }

    throw new BaseError('Forbidden.', 403, 'UserDoesntPermissions');
  }

  public async execValidOnboardingTokenJwt(
    token: string
  ): Promise<IJwtParamsTokenOnboarding> {
    const itFound = await this.bndOnboardingToken.getNotExistDocumentsByToken(
      token
    );
    if (itFound)
      throw new BaseError(
        `Doesn't exist an Onboarding for this token ${token}.`,
        404,
        'BaseUseCaseOnboarding | updateStepOneOnboarding'
      );
    const onboarding = await this.bndOnboardingToken.getInfoOnboarding(token);
    const nowLuxon = DateTime.now().toMillis();
    const expiredTimeLuxon = DateTime.fromISO(
      onboarding.tokenDate.toISOString()
    );

    if (expiredTimeLuxon.toMillis() <= nowLuxon)
      throw new BaseError(
        `Onboarding expired for this token ${token}.`,
        401,
        'BaseUseCaseOnboarding | updateStepOneOnboarding'
      );
    return { id: onboarding.id };
  }
}
