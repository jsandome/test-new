export interface IPasswordParams {
  hashPassword: string;
  inputPassword: string;
}
