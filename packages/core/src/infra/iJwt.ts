import { EnumUserTypes } from '../common';

export interface IJwtParamsToken {
  _id: string;
}
export interface IJwtParamsTokenIn {
  id: string;
  email: string;
  type: EnumUserTypes;
}

export interface IJwtParamsTokenOnboarding {
  id: string;
}
