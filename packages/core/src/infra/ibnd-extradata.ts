export interface IBndExtraData {
  bucket?: string;
  name?: string;
  url?: string;
  code?: string;
}
