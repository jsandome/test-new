export interface IBaseEntityUserToken {
  token: string;
}
export interface IBaseExpireDataTokenOut {
  expireToken: string;
  expireDateToken: Date;
}
