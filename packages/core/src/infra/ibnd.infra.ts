import { IBaseExpireDataTokenOut } from './ientity.user-Token';
import { IJwtParamsTokenIn, IPasswordParams } from '.';

export interface IBndBaseTokenInfra {
  generateToken(data: IJwtParamsTokenIn): Promise<string>;
  validJwtToken(token: string): Promise<IJwtParamsTokenIn>;
  generateHash(password: string): Promise<string>;
  generateSecure(userId: string): IBaseExpireDataTokenOut;
  generateCode(): string;
  generateExpireDataToken(userId: string): IBaseExpireDataTokenOut;
  validPassword(passwordData: IPasswordParams): Promise<boolean>;
  generateTokenFireBase(user: string): Promise<string>;
}
