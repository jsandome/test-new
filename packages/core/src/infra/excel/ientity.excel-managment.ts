import {
  IBndBasePropertyRead,
  IBndBaseGCloudInfra,
  IBndBaseLocations,
} from '../../';

export interface IPropertyUploadPropertyInformationByExcelParams {
  bndGCloudInfra?: IBndBaseGCloudInfra;
  bndPropertyRead: IBndBasePropertyRead;
  bndLocationsRead?: IBndBaseLocations;
  pathFile: string;
  fileName: string;
}
export interface IExcelReadParams {
  fileName: string;
  SheetName: string;
  bndPropertyRead: IBndBasePropertyRead;
}
