import {
  IExcelPropertiesAndProjections,
  IPropertyProfitsAndLossExcelResponse,
} from '../../common';
import { IPropertyUploadPropertyInformationByExcelParams } from './';

export interface IBndBaseExcelManagementInfra {
  getExcelPropertiesAndProjectionsInfo(
    data: IPropertyUploadPropertyInformationByExcelParams
  ): Promise<IExcelPropertiesAndProjections>;
  getProfitsAndLossByExcelPreview(
    data: IPropertyUploadPropertyInformationByExcelParams
  ): Promise<IPropertyProfitsAndLossExcelResponse>;
}
