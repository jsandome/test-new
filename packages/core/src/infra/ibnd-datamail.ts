import { IBndAttachments } from './ibnd-attachments';
import { IBndExtraData } from './ibnd-extradata';

export interface IBndDataMail {
  to: string;
  subject: string;
  template: string;
  data?: IBndExtraData;
  attachments?: Array<IBndAttachments>;
}
