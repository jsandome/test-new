export interface IBndBaseGCloudInfra {
  uploadFile(urlObject: string): Promise<boolean>;
  moveFile(
    urlTmpObject: string,
    destinationObjectPath: string
  ): Promise<boolean>;
  copyFile(originalPath: string, destinationPath: string): Promise<void>;
  checkFileExist(url: string): Promise<boolean>;
  getPublicUrl(): Promise<string>;
  deleteFile(urlObject: string): Promise<void>;
  getRelativePathFromFull(fullPath: string): string;
  getListOfFiles(prefix?: string): Promise<string[]>;
  dowloandFileAsBuffer(path: string): Promise<Buffer>;
}
