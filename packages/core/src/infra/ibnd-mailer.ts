import { IBndDataMail } from './ibnd-datamail';

export interface IBndMailerInfra {
  send(dataToMail: IBndDataMail): Promise<void>;
}
