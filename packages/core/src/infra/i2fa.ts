export interface I2FAKeys {
  secret: string;
  hex: string;
  base32: string;
  otpauthUrl: string;
  qrCode: string;
}
