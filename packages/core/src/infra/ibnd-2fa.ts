import { IJwtParamsTokenIn, I2FAKeys } from '.';

export interface IBnd2FAInfra {
  createCode(params: IJwtParamsTokenIn): Promise<I2FAKeys>;
  validCode(secret: string, token: string): boolean;
}
