import { ListTemplates, UserInfoDocusign } from '../common/entities';
export interface IBndBaseDocusingInfra {
  listTemplates(): Promise<ListTemplates[]>;
  getEnvelope(): Promise<void>;
  sendEnvelope(dataUser: UserInfoDocusign): Promise<void>;
}
