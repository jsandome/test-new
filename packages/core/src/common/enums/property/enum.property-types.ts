export enum EnumPropertyTypes {
  MULTI_FAMILY = 'Multi family',
  SINGLE_FAMILY_RESIDENTIAL = 'Single Family Residential',
  COMMERCIAL = 'Commercial',
  LAND = 'Land',
}
