export enum EnumPropertyImageFormats {
  PNG = 'png',
  JPG = 'jpg',
  JPEG = 'jpeg',
}
