export enum EnumPropertyStrategies {
  BUY_HOLD = 'Buy and hold',
  DEVELOP_HOLD = 'Develop and hold',
  DEVELOP_SELL = 'Develop and sell',
  INMEDIATE_SELL = 'Inmediate sell',
  REPOSITION_HOLD = 'Reposition and hold',
  REPOSITION_SELL = 'Reposition and sell',
}
