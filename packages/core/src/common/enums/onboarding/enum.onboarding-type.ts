
export enum EnumTypeFormOnboarding {
  INDIVIDUAL = 'Individual',
  JOINTTENACY = 'Joint tenancy with right of survivorship',
  TRUST = 'Trust (except employee benefit trust or IRA)',
  LLC = 'LLC',
  OTHERJOINT = 'Other joint ownership',
}

export enum EnumTypeStepOnboarding {
  STEPONE = 'stepOne',
  STEPTWO = 'stepTwo',
  STEPTHREE = 'stepThree',
  STEPFOUR = 'stepFour',
  FINISHED = 'finished',
}