export enum EnumGCloudDocumentsValidFormats {
  PDF = 'pdf',
}

export enum EnumGCloudDocumentsStatus {
  INPROCESS = 'inProcess', //In the tmp folder
  DELETED = 'deleted',
  SUCCESS = 'success',
  NOTFOUND = 'notFound',
}
