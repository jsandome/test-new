export enum IEnumAlgorithm {
  MD5 = 'md5',
  SHA256 = 'sha256',
  SHA512 = 'sha512',
  HS512 = 'HS512',
  RSA_SHA256 = 'RSA-SHA256',
}
export enum IEnumEncoding {
  HEX = 'hex',
  UTF8 = 'utf8',
  BASE64 = 'base64',
}
