export enum IEnumTypesPayment {
  WIRE = 'Wire',
  CHECK = 'Check',
  ACH = 'ACH',
  ROLLOVER = 'Rollover',
  NOSPECIFIED = 'No payment specified',
}
