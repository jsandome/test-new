export enum EnumOfferingStatus {
  CONTACTED = 'Contacted',
  SOFTCIRCLED = 'Soft-Circled',
  CLOSED = 'Closed',
  DECLINED = 'Declined',
}
