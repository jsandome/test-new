export enum EnumOfferingType {
  FUND = 'Fund',
  PACKAGE = 'Package',
  PROPERTY = 'Property',
}
