export enum EnumTwoFactorType {
  EMAIL = 'email',
  PHONE = 'phone',
}
