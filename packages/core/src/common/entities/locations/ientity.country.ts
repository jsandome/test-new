
export interface IBaseEntityCountries {
  id?: string;
  name: string;
  code: string;
}
