export interface InterfacePropertyStrategies {
  buyAndHold: Boolean,
  repositionAndHold: Boolean,
  repositionAndSell: Boolean,
  developAndHold: Boolean,
  developAndSell: Boolean,
  inmediateSell: Boolean
}