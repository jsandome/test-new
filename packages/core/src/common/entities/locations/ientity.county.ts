import { IBaseEntityStates } from '.';

export interface IBaseEntityCounties {
  id?: string;
  name: string;
  state?: string | IBaseEntityStates;
  fips?: number;
}
