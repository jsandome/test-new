export interface IBaseEntityStates {
  id?: string;
  name: string;
  code?: string;
}
