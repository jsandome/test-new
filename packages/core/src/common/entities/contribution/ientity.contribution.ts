import { IPaginationResponse } from '../../entities';
import { IEnumTypesPayment } from '../../';

export interface IBndReturnCreateContribution {
  id: string;
}
export interface IBndReturnRecordContribution {}
export interface IBndReturnListContribution {
  pagination: IPaginationResponse;
  details: IDetailsListContribution[];
}
export interface IBndReturnGetContribution {
  account: IGetContributionAccount;
  contributionDate: Date;
  capitalCall: IGetContributionCapitalCall;
}
export interface IBndReturnUpdateContribution {
  id: string;
}
export interface IBndReturnDeleteContribution {}

export interface IGetContributionAccount {
  id: string;
  name: string;
}
export interface IGetContributionCapitalCall {
  id: string;
  description: string;
  amount: number;
  paymentType: IEnumTypesPayment;
  paymentNumber: string;
  notes: string;
}
export interface IDetailsListContribution {
  id: string;
  contributionDate: Date;
  investor: string;
  description: string;
  amountContributed: number;
}

interface ICapitalCallContribution {
  id?: string;
  amount: number;
  paymentType: IEnumTypesPayment;
  paymentNumber: string;
  notes: string;
}

export interface IParamsCreateContribution {
  investor: string;
  dateReceived: Date;
  capitalCall: ICapitalCallContribution;
}

export interface IParamsRecordContribution {
  amountContribution: number;
  paymentDate: Date;
  paymentType: IEnumTypesPayment;
  paymentNumber: string;
  notes: string;
  investorId: string;
}

export interface IParamsUpdateContribution {
  account: string;
  dateReceived: Date;
  capitalCall: ICapitalCallContribution;
}
