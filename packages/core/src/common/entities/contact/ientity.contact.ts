import { IPaginationResponse } from '../';

export interface IBaseEntityContact {
  id?: string;
  name: string;
  jobTitle: string;
  companyName: string;
  currentJob: boolean;
  email: string;
  phone: string;
  address: string;
  city: string;
  county: string;
  state: string;
  zip: string;
}

export interface IParamsCreateContact {
  name: string;
  jobTitle: string;
  companyName: string;
  currentJob: boolean;
  email: string;
  phone: string;
  address: string;
  city: string;
  county: string;
  state: string;
  zip: string;
}

export interface IParamsUpdateContact {
  name: string;
  jobTitle: string;
  companyName: string;
  currentJob: boolean;
  email: string;
  phone: string;
  address: string;
  city: string;
  county: string;
  state: string;
  zip: string;
}

export interface IBndSelectContacts {
  id: string;
  name: string;
  email: string;
}

export interface IBndListContacts {
  id: string;
  name: string;
  company: string;
  jobTitle: string;
  lastUpdate: Date;
  email: string;
}

export interface IBndReturnListContacts {
  pagination?: IPaginationResponse;
  details: IBndListContacts[];
}

export interface IPaginateParamsContacts {
  limit?: number;
  page?: number;
}

export interface IBndResponseCreateContact {
  id: string;
}

export interface IBndResponseUpdateContact {}
