import {
  EnumTypeFormOnboarding
} from '../../enums';
import { IBaseEntityContact,
  IBaseEntityProperty,
  IBaseEntityFunds,
  IBaseEntityPackage,
  IBasePackageDocuments } from './../index';

/**
* Commons interfaces
*/

export interface IBndmailingAddres {
  streetLineOne: string;
  streetLineTwo: string;
  city: string;
  state: string;
  zip: string;
  country: string;
}

export interface IBndDomicilieAddress {
  streetLineOne: string;
  streetLineTwo: string;
  city: string;
  state: string;
  zip: string;
  country: string;
}

export interface IBndListGeneralInformation {
  firstName: string;
  middleName?: string;
  lastName?: string;
  phoneNumber: string;
  email: string;
  country: string;
  dateBirthday?: Date;
  taxID?: string;
  employer?: string;
  jobTitle?: string;
  usPerson: boolean;
}

export interface IBndListSignatoriesInformation {
  firstName: string;
  middleName?: string;
  lastName?: string;
  phoneNumber: string;
  email: string;
  title: string;
  dateBirthday?: Date;
  taxID?: string;
}

export interface IBndListOtherJointInformation {
  firstName: string;
  middleName?: string;
  lastName?: string;
  phoneNumber: string;
  email: string;
  citizenship: string;
  title: string;
  dateBirthday?: Date;
  taxID?: string;
}

export interface IBndTrustLegalEntity {
  entityName: string;
  entityTitle: string;
  basicSignatureBlock: boolean;
}

export interface IBndListGeneralInformationWithNaturalPerson {
  naturalPerson: boolean;
  person?: IBndListSignatoriesInformation;
  entity?: IBndTrustLegalEntity;
}

export interface IBndListGeneralInformationWithPerson {  
  person?: IBndListOtherJointInformation;
  usPerson: boolean;
}

/**
 *
 * Interfaces for form Individual (Onboarding)
 *
 */

export interface IBndIndividualIdentify {
  firstName: string;
  middleName?: string;
  lastName?: string;
  phoneNumber: string;
  email: string;
  country: string;
  dateBirthday?: Date;
  taxID?: string;
  employer: string;
  jobTitle: string;
  entityTaxPurpose: boolean;
}


/**
 *
 * Interfaces for form Joint tenancy with right of survivorship (Onboarding)
 *
 */

export interface IBndJointTenancy {
  subscriberLegalName: string;
  relationshipOwners: string;
  entityTaxPurpose: boolean;
  //Depends on entityTaxPurpose
  ultimateBeneficial?: string;
  ultimateBeneficialTaxId?: string;
  owners: IBndListGeneralInformation[];
}

/**
 * Interfaces for form Trust (Onboarding)
 */

 export interface IBndTrust {
  subscriberLegalName: string;
  taxID: string;
  entityTaxPurpose: boolean;
  ultimateBeneficial?: string;
  ultimateBeneficialOwnerTaxId?: string;
  grantorTrust: boolean;
  authorized: boolean;

  owners: IBndListGeneralInformationWithNaturalPerson[];
}

/**
 *
 * Interfaces for form Employee benefit plan or trust (including IRA) (Onboarding)
 *
 */

export interface IBndEmploye {
  subscriberLegalName: string;
  taxID: string;
  organizationalJurisdiction: string;
  owners: IBndListGeneralInformationWithNaturalPerson[];
}

/**
 *
 * Interfaces for form Parthership (Onboarding)
 *
 */

export interface IBndParthership {
  subscriberLegalName: string;
  taxID: string;
  usPerson: boolean;
  ultimateBeneficial?: string;
  ultimateBeneficialOwnerTaxId?: string;
  country?: string;
  organizationalJurisdiction?: string;
  entityTaxPurpose?: boolean;
  owners: IBndListGeneralInformationWithNaturalPerson[];
}

/**
 * Interfaces for form Corporation (Onboarding)
 */

export interface IBndCorporation {
  subscriberLegal: string;
  taxID: string;
  organizationalJurisdiction: boolean;
  owners: IBndListGeneralInformationWithNaturalPerson[];
}

/**
 * Interfaces for form LLC (Onboarding)
 */

export interface IBndLLC {
  subscriberLegalName: string;
  taxID: string;
  usPerson: boolean;
  ultimateBeneficial?: string;
  ultimateBeneficialOwnerTaxId?: string;
  country?: string;
  organizationalJurisdiction?: string;
  entityTaxPurpose?: boolean;
  owners: IBndListGeneralInformationWithNaturalPerson[];
}

/**
 *  Interfaces for form 501 (c)(3) organization (Onboarding)
 */

export interface IBndOrganization {
  subscriberLegalName: string;
  taxID: string;
  organizationalJurisdiction?: string;
  entityTaxPurpose?: boolean;
  owners: IBndListGeneralInformationWithNaturalPerson[];
}

/**
 * Interfaces for form OtherJoint (Onboarding)
 */

export interface IBndOtherJoint {
  subscriberLegalName: string;
  taxID: string;
  entityTaxPurpose?: boolean;
  owners: IBndListGeneralInformationWithPerson[];
}

/**
 * Interfaces for form Other (Onboarding)
 */

export interface IBndOther {
  subscriberLegalName: string;
  taxID: string;
  usPerson: boolean;
  ultimateBeneficial?: string;
  ultimateBeneficialOwnerTaxId?: string;
  country?: string;
  organizationalJurisdiction?: string;
  entityTaxPurpose?: boolean;
  owners: IBndListGeneralInformationWithNaturalPerson[];
}

export interface IBaseOnboardingInformation {
  investmentAmount: number;
  mailingAddres: IBndmailingAddres;
  sameMailingAddress: boolean;
  domicileAddress?: IBndDomicilieAddress;
  typeFormOnboarding: EnumTypeFormOnboarding;
  individualIdentity?: IBndIndividualIdentify;
  jointTenancy?: IBndJointTenancy;
  tenancyCommon?: IBndJointTenancy;
  trust?: IBndTrust;
  employee?: IBndEmploye;
  partnership?: IBndParthership;
  corporation?: IBndCorporation;
  llc?: IBndLLC;
  opt501?: IBndOrganization;
  otherJoint?: IBndOtherJoint;
  otherOut?: IBndOther;
}


/* IParams Type */

export interface IBndResponseOnboarding {
  id?: string;
  email?: string;
  envelopeId?: string;
  url?: string;
  token?: string;
  tokenDate?: Date;
  nameContact?: string;
  idContact?: string | IBaseEntityContact;  
  idProperty?: string | IBaseEntityProperty;
  idFund?: string | IBaseEntityFunds;
  idPackage?: string | IBaseEntityPackage;
  idTemplete?: string;
  idPackageDocument?: string | IBasePackageDocuments;
  baseInformation?: IBndIndividualIdentify | IBndJointTenancy |
  IBndTrust | IBndLLC | IBndOtherJoint;
  subscriberDetails?: object;
  contats?: object[];
}

export interface IParamsCreateOnboarding  {
  email: string;
  userRequest: string;
  nameContact?: string;
  propertyId?: string;
  fundId?: string;
  packageId?: string;
  templateId?: string;
  packageDocumentId?: string;
}

export interface IParamsUpdateStepOneOnboarding  {
  userRequestId: string;
  investmentAmount: number;
  mailingAddres: IBndmailingAddres;
  sameMailingAddress: boolean;
  domicileAddress?: IBndDomicilieAddress;
  typeFormOnboarding: EnumTypeFormOnboarding;
  individualIdentity?: IBndIndividualIdentify;
  jointTenancy?: IBndJointTenancy;
  trust?: IBndTrust;
  llc?: IBndLLC;
  otherJoint?: IBndOtherJoint;
}
