import { IPaginationResponse } from '..';

interface ICapitalCallInvestor {
  id: string;
  capitalAmount: number;
}

export interface IParamsCreateCapitalCall {
  description: string;
  dueDate: Date;
  investors: ICapitalCallInvestor[];
}

export interface IParamsUpdateCapitalCall {
  description: string;
  dueDate: Date;
}

export interface IBndReturnCreateCapitallCall {
  id: string;
}

interface ITotalsGetCapitalCall {
  dueDate: string;
  totalAmount: number;
  totalAccounts: number;
  received: number;
  receivedAccounts: number;
  outstanding: number;
  outstandingAccounts: number;
}
export interface IDetailsGetCapitalCallInvestor {
  account: string;
  commitment: number;
  called: number;
  lifetimeCalled: number;
  received: number;
  receivedDate: Date;
  investorId: string;
}
export interface IBndReturnGetCapitallCall {
  totals: ITotalsGetCapitalCall;
  details: IDetailsGetCapitalCallInvestor[];
}
export interface IBndReturnSelectCapitallCall {
  id: string;
  description: string;
  amountCalled: number;
  result: number;
}

export interface IBndCapitalCallInvestorTotalAmount {
  detailsInvestor: IDetailsGetCapitalCallInvestor[];
  totalAmount: number;
  totalAccounts: number;
  received: number;
  receivedAccounts: number;
  outstanding: number;
  outstandingAccounts: number;
}

export interface ITotalsListCapitalCall {
  amountCalled: number;
  result: number;
}
export interface IDetailsListCapitalCall {
  id: string;
  dueDate: Date;
  description: string;
  parties: number;
  amountCalled: number;
  result: number;
}
export interface IBndReturnListCapitalCall {
  pagination: IPaginationResponse;
  totals: ITotalsListCapitalCall;
  details: IDetailsListCapitalCall[];
}

interface AggregateCapitalCall {
  details: IDetailsListCapitalCall[];
  totals: ITotalsListCapitalCall[];
}

export interface ReponseAggregateCapitalCall {
  docs: AggregateCapitalCall[];
}
