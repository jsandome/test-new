import { EnumUserTypes } from '../enums';

export interface IBaseEntityCreateUser {
  id?: string;
  email: string;
  admin: string;
  type: EnumUserTypes;
  password?: string;
}
export interface IUpdateJwt {
  id: string;
  tokenJwt: string;
}
export interface IUpdateSecure {
  id: string;
  secure: string;
  code?: string;
  date: Date;
}
export interface IUpdatePassword {
  email: string;
  password: string;
}
export interface IExpirePass {
  token: string;
  date: Date;
}
