
export interface IBaseTemplateDocusing {
  templateId: string;
  name: string;
  date: Date;
}

export interface IBasePackageDocuments {
  id?: string;
  name: string;
  templates: IBaseTemplateDocusing[];
}

export interface IParamsCreatePackageDocuments {
  name: string;
  templates: string[];
  userRequest: string;
}

export interface IBndResponseCreatePackageDocuments {
  id: string;
}

export interface IBndResponsIfExist {
  message: string;
}

export interface IBndResponseListPackageDocuments {
  id?: string;
  name: string;
  templates: IBaseTemplateDocusing[];
}

export interface IParamsUpdatePackageDocuments {
  name: string;
  newTemplates?: string[];
  deleteTemplates?: string[];
}