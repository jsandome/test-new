export interface IBaseEntityPackage {
  id?: string;
  name: string;
  totalValue: number;
  mainImage: string;
  shareAvailable: number;
}

export interface IInvestorsDetailPackage {
  id: string;
  name: string;
  minCommitment: number;
  initCommitment: number;
  totalCommitment: number;
  termYears: number;
}
export interface IPropertiesDetailPackage {
  _id: string;
  name: string;
}
interface IDetailNameInvestor {
  firstName: string;
  lastName: string;
}
export interface IInvestorDetailPackage {
  _id: string;
  name: IDetailNameInvestor;
}

export interface IReturnPropertiesDetailPackage {
  id: string;
  name: string;
  share: number;
}

export interface IPropertiesPackage {
  id: string;
  share: number;
}

export interface IParamsCreatePackage {
  name: string;
  totalValue: number;
  investors: IInvestorsDetailPackage[];
  properties: IPropertiesPackage[];
}

export interface IParamsUpdatePackage {
  name: string;
  totalValue: number;
  investors: IInvestorsDetailPackage[];
  properties: IPropertiesPackage[];
}

export interface IParamsPreviewPackage {
  properties: IPropertiesPackage[];
}

export interface IBndReturnCreatePackage {
  id: string;
}

export interface IBndReturnPreviewPackage {
  totalValue: Number;
}

export interface IBndReturnUpdatePackage {}

export interface IBndReturnDeletePackage {}

export interface IBndReturnGetPackage {
  id: string;
  name: string;
  totalValue: Number;
}

export interface IPropertyPackageShare {
  id: string;
  share: number;
}

export interface IBndReturnDetailPackage {
  id: string;
  name: string;
  totalValue: number;
  investors: IInvestorsDetailPackage[];
  properties: IReturnPropertiesDetailPackage[];
}
