export interface IModelPaginate<T> {
  docs?: T;
  hasNextPage: boolean;
  hasPrevPage: boolean;
  limit: number;
  nextPage: number;
  prevPage: number;
  page: number;
  pagingCounter: number;
  totalDocs: number;
  totalPages: number;
}

export interface IPaginationResponse {
  itemCount: number;
  offset: number;
  limit: number;
  totalPages: number;
  page: number;
  nextPage: number;
  prevPage: number;
}
export interface IPaginateParams {
  limit?: number;
  page?: number;
}
