export interface IBaseEntityFundInvestors {
  id: string;
  minCommitment: number;
  initCommitment: number;
  totalCommitment: number;
  termYears: number;
}

export interface IBaseEntityFundProperty {
  id: string;
  share: number;
}

export interface IBaseEntityFundPackage {
  id: string;
  share: number;
}

export interface IBaseEntityFunds {
  id?: string;
  name: string;
  cashHand: number;
  active?: boolean;
}
export interface IParamsEntityFunds {
  id?: string;
  name: string;
  cashHand: number;
  investors: IBaseEntityFundInvestors[];
  properties?: IBaseEntityFundProperty[];
  packages?: IBaseEntityFundPackage[];
  active?: boolean;
}

export interface IParamsPreviewFund {
  cashHand: number;
  properties: IBaseEntityFundProperty[];
  packages: IBaseEntityFundPackage[];
}

export interface IBndReturnCreateFund {
  id?: string;
  name?: string;
}

interface IBaseFundInvestors {
  id: string;
  name: string;
  minCommitment: number;
  initCommitment: number;
  totalCommitment: number;
  termYears: number;
}

interface IBaseFundProperties {
  id: string;
  name: string;
  mainImage: string;
  share: number;
}

export interface IBaseFundPackages {
  id: string;
  name: string;
  mainImage: string;
  share: number;
}

export interface IBaseReturnDetailFund {
  id: string;
  name: string;
  cashHand: number;
  investors: IBaseFundInvestors[];
  properties: IBaseFundProperties[];
  packages?: IBaseFundPackages[];
}

export interface IBndReturnPreviewFund {
  totalValue: number;
}
