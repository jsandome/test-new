import { IEnumTypesPayment } from '../..';
import { IPaginationResponse } from '../../entities';

export interface IBndReturnCreateDistribution {
  id: string;
}
export interface IBndReturnListDistributionDetail {
  id: string;
  distributionDate: Date;
  totalDistribution: number;
  totalPaymentAmount: number;
  totalOutstanding: number;
  parties: number;
  description: string;
  yieldPeriod: string;
}
export interface IDistributionDetailAccount {
  id: string;
  name: string;
  amount: number;
  paymentType: string;
  paymentNumber: string;
}
export interface IBndReturnDetailDistribution {
  description: string;
  distributionDate: Date;
  yieldPeriod: string;
  account: IDistributionDetailAccount[];
}
export interface IBndReturnUpdateDistribution {
  id: string;
}
export interface IBndReturnDeleteDistribution {}

export interface IBndReturnListDistribution {
  pagination: IPaginationResponse;
  details: IBndReturnListDistributionDetail[];
}
export interface IDistributionDetailAccount {
  id: string;
  name: string;
  amount: number;
  paymentType: string;
  paymentNumber: string;
}
export interface IBndReturnDetailDistribution {
  description: string;
  distributionDate: Date;
  yieldPeriod: string;
  account: IDistributionDetailAccount[];
}
export interface IBndReturnUpdateDistribution {
  id: string;
}
export interface IBndReturnDeleteDistribution {}

export interface IDistributionAccount {
  id: string;
  paymentType: IEnumTypesPayment;
  paymentNumber?: string;
  amount: number;
}

export interface IParamsCreateDistribution {
  description: string;
  distributionDate: Date;
  yieldPeriod: string;
  account: IDistributionAccount[];
}

export interface IParamsUpdateDistribution {
  description: string;
  distributionDate: Date;
  yieldPeriod: string;
  account: IDistributionAccount[];
}
