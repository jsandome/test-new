import { IBaseEntityUser } from './ientity.user';

export interface IBaseEntityLoginResponse {
  user: IBaseEntityUser;
  token: string;
}
