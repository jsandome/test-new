import { IPaginationResponse } from '../ientity.pagination.user';

export interface IBndReturnLlc {
  id?: string;
  taxId?: string;
}

export interface InterfacePropertiesLLC {
  id: string;
  name?: string;
  share: number;
}

export interface IBaseEntityLlcProperty {
  id: string;
  llc?: string;
  property?: string;
  share: number;
}

export interface IBaseEntityLlc {
  id?: string;
  name: string;
  usPerson: boolean;
  taxId: string;
  address: string;
  state: string;
  date: Date;
  active?: boolean;
  investors: string[];
  properties: InterfacePropertiesLLC[];
}

export interface IBaseEntityGetLlc {
  id?: string;
  name: string;
  usPerson: boolean;
  taxId: string;
  address: string;
  state: string;
  date: Date;
  active?: boolean;
}

export interface IBaseTotalsLlc {
  activeCommitment?: number;
  contributions?: number;
}
export interface IBaseDetailsLlc {
  id?: string;
  name: string;
  positions: number;
  activeCommitment: number[];
  contributions: number;
}
export interface IBaseResponseList {
  totals?: IBaseTotalsLlc;
  details: IBaseDetailsLlc[];
  pagination: IPaginationResponse;
}

export interface IBaseResponseInvestors {
  id: string;
  name: string;
}
interface IBaseResponseEntity {
  name: string;
  usPerson: boolean;
  taxId: string;
  address: string;
  date: Date;
}

interface IBaseResponseProperties {
  id: string;
  name: string;
  address: string;
  projectedIRR: number;
  myShare: number;
  estimatedValue: number;
  loanAmmount: number;
  estimateEquity: number;
  type: string;
  pictures: string[];
  mainPicture: string;
}

export interface IBaseResponseOverviewDetails {
  entity: IBaseResponseEntity;
  investors: IBaseResponseInvestors[];
  properties: IBaseResponseProperties[];
}

export interface IBaseGetDetails {
  name: string;
  usPerson: boolean;
  taxId: string;
  address: string;
  date: Date;
  state: string;
  investors: IBaseResponseInvestors[];
  properties: InterfacePropertiesLLC[];
  share?: number[];
}

export interface IBaseModelLlcInvestor {
  id?: string;
  llc: string;
  investor: string;
}

export interface IBaseModelLlcProperty {
  id?: string;
  llc: string;
  property: string;
  share: number;
}

export interface IBasePreviewLLC {
  date: string;
  description: string;
}

export interface IBaseResponsePreviewLLC {
  activity: IBasePreviewLLC[];
}
