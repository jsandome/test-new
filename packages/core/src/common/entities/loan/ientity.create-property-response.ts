export interface IBaseEntityLoanCreateResponse {
  id: string;
}

export interface IBaseEntityLoanAmortizationSchedule {
  id: string;
}
