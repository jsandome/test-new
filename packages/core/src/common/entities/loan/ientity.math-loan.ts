export interface IScheduleAmortizationExtraPayment {
  amount: number;
  name?: string;
  paymentNumbers: number[];
}

export interface ISAPropertyShareInput {
  id: string;
  share: number;
}

export interface IScheduleAmortizationInputBase {
  interest: number;
  amortizationYears: number;
  amount: number;
  scheduleExtraPayment: number;
  paymentsPerYear: number;
  beginningDate: string;
  extraPayments: IScheduleAmortizationExtraPayment[];
  properties: ISAPropertyShareInput[];
}

export interface IPropertyPaymentAmortizationResponse {
  id: string;
  name: string;
  address: string;
  principalPayment: number;
  payment: number;
  share: number;
}

export interface IMonthlyAmortizationResponse {
  paymentNumber: number;
  date: string;
  beginningBalance: number;
  extraPayment: number;
  totalPayment: number;
  interest: number;
  principal: number;
  endingBalance: number;
  accumulativeInterest: number;
  schedulePayment: number;
  propertiesPayment: IPropertyPaymentAmortizationResponse[];
}

export interface IScheduleAmortizationResponse {
  interestRate: number;
  paymentsPerYear: number;
  amortizationYears: number;
  amount: number;
  schedulePayment: number;
  programedNumberPayments: number;
  totalNumberPayments: number;
  totalEarlyPayments: number;
  totalInterest: number;
  generalPropertyInfo: IPropertySheduleGralData[];
  amortizationSchedule: IMonthlyAmortizationResponse[];
}

export interface IPropertySheduleGralData {
  id: string;
  name: string;
  address: string;
  share: number;
  loanAmount: number;
  loanPayment: number;
}
