export interface ILoanExtraPayment {
  name: string;
  amount: number;
  paymentNumbers: number[];
}
export interface ILoanPropertyResponse {
  id: string;
  share: number;
}

interface IPropertyPayment {
  share: number;
  firstPayment: number;
  finalPayment: number;
}

export interface ILoanProperty {
  id: string;
  payments: IPropertyPayment[];
}

export interface IBaseEntityLoan {
  id?: string;
  name: string;
  amount: number;
  interest: number;
  amortizationYears: number;
  paymentsPerYear?: number;
  termYears: number;
  beginningDate: string;
  extraPayments?: ILoanExtraPayment[];
  properties: ILoanProperty[] | ILoanPropertyResponse[];
}
