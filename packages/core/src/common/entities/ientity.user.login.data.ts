export interface IBaseEntityLoginData {
  id?: string;
  email: string;
  password: string;
}
