import { IPaginationResponse } from '../../ientity.pagination.user';

export interface IGCloudDocumentCreateParams {
  path: string;
  addTaxCenter: Boolean;
  userRequest: string;
}

export interface IGCloudDocumentCreateResponse {
  id: string;
  name: string;
}

export interface IGCloudDocumentList {
  pagination: IPaginationResponse;
  details: IGCloudDocumentListResponse[];
}
export interface IGCloudDocumentListResponse {
  id: string;
  name: string;
  date: string;
  url: string;
  addTaxCenter: Boolean;
}

export interface IGCloudDocumentSelectResponse {
  id: string;
  name: string;
}

export interface IGCloudDocumentsQueryParams {
  limit?: number;
  page?: number;
  search?: string;
}
