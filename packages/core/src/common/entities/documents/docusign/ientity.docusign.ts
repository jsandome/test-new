export interface ListTemplates {
  endPosition?: string;
  envelopeTemplates?: EnvelopeTemplate[];
  resultSetSize?: string;
  startPosition?: string;
  totalSetSize?: string;
}

export interface EnvelopeTemplate {
  id?: string;
  allowMarkup?: string;
  allowReassign?: string;
  anySigner?: string;
  authoritativeCopy?: string;
  autoMatch?: string;
  autoMatchSpecifiedByUser?: string;
  created?: Date;
  description?: string;
  disableResponsiveDocument?: string;
  emailBlurb?: string;
  emailSubject?: string;
  enableWetSign?: string;
  enforceSignerVisibility?: string;
  envelopeLocation?: string;
  folderId?: string;
  folderIds?: string[];
  folderName?: string;
  lastModified?: Date;
  lastUsed?: Date;
  name?: string;
  owner?: Owner;
  pageCount?: string;
  passwordProtected?: string;
  shared?: string;
  signingLocation?: string;
  templateId?: string;
  uri?: string;
}

export interface Owner {
  email: string;
  userId: string;
  userName: string;
}

export interface UserInfoDocusign {
  firstName: string;
  lastName: string;
  date: Date;
  address: string;
  email: string;
}
