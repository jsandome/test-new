import { EnumTwoFactorType, EnumUserTypes } from '..';

export interface IBaseEntityUser {
  id?: string;
  email: string;
  password?: string;
  admin: string;
  investor: string;
  tokenJwt?: string;
  expirePass?: {
    token?: string;
    date?: Date;
  };
  twoFactor?: {
    type?: EnumTwoFactorType;
    date?: Date;
    code?: string;
    secure?: string;
    enabled: boolean;
  };
  type: EnumUserTypes;
  lastLogin?: Date;
  active: boolean;
}
