import { IBaseEntityPropertyFinancial } from './';
export interface IBaseEntityPropertyFinancialQuarter {
  _id?: string;
  propertyFinancial: string | IBaseEntityPropertyFinancial;
  year: number;
  quarter: number;
  totalUnits: number;
  rents: {
    month1: number;
    month2: number;
    month3: number;
  };
  propertyManagement: number;
  assetManagement: number;
  repairsMaintenance: number;
  taxes: number;
  insurance: number;
  reserve: number;
  vacancy: number;
  operativeExpenses?: IBaseOperativeExpense[];
}

export interface IBaseOperativeExpense {
  name: string;
  total: number;
}
