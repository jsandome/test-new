import { EnumPropertyTypes, EnumPropertyStatus } from '../../../common';
import {
  IBaseEntityCounties,
  IBaseEntityStates,
  InterfacePropertyStrategies,
} from '../locations';
import {
  IBasePropertyProjection,
  IBaseEntityPropertyFinancial,
  IBaseEntityInvestorProperty,
  IBasePropertyImportInformation,
} from '.';
export interface IBasePropertyLocation {
  lat: string;
  lng: string;
}
export interface IBaseEntityProperty {
  id?: string;
  name: string;
  address: string;
  type: EnumPropertyTypes;
  state: string | IBaseEntityStates;
  county: string | IBaseEntityCounties;
  bedRooms: number;
  bathRooms: number;
  size: number;
  parcelSize: number;
  parcelNumber: number;
  strategies: InterfacePropertyStrategies;
  propertyDescription: string;
  pictures?: string[] | { url: string }[];
  mainPicture?: string | { url: string };
  newPictures?: string[] | { url: string }[];
  deletePictures?: string[] | { url: string }[];
  newMainPicture?: string | { url: string };
  status?: EnumPropertyStatus;
  location: IBasePropertyLocation;
  projection?: IBasePropertyProjection;
  isComplete?: boolean;
  financialInformation?: string | IBaseEntityPropertyFinancial;
  financialQuarterInformation?: string[];
  investors?: string | IBaseEntityInvestorProperty[];
  isDeleted?: boolean;
  share?: number;
  scheduleGrossIncome?: number;
  operatingExpenses?: number;
  debtService?: number;
  cashHandReturn?: number;
  equitityBuildUp?: number;
  totalReturn?: number;
  totalValue?: number;
  personalShare?: number;
  initCommitment?: number;
  excelImportInformation?: IBasePropertyImportInformation;
}

export interface IBaseEntityPropertySelect {
  id?: string;
  address: string;
  name: string;
  shareAvailable: number;
  mainImage: string;
  type: EnumPropertyTypes;
  isComplete: boolean;
}

export interface IBaseEntityPropertyFundResponse {
  id: string;
  name: string;
  address: string;
  type: EnumPropertyTypes;
  location: {
    lat: string;
    lng: string;
  };
  pictures: string[];
  mainPicture: string | { url: string };
  estimatedValue: number;
  fundsShare: number;
  myShare: number;
  fundEquity: number;
  myEquity: number;
  projectedIRR: number;
  projectEquityMultiple: number;
  state: string;
}

export interface IBaseEntityPropertyPackageResponse {
  id: string;
  name: string;
  address: string;
  type: string;
  location: {
    lat: string;
    lng: string;
  };
  estimatedValue: number;
  myShare: number;
  projectedIRR: number;
  loanAmmount: number;
  estimateEquity: number;
  pictures: string[];
  mainPicture: string;
  state: string;
}
