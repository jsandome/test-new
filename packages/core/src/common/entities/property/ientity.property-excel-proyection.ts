import { ICellAddressInformation } from './';

interface IPropertyProyectionExcelCellInformation {
  propertyName: ICellAddressInformation;
  value: {
    totalValue: ICellAddressInformation;
    purchasePrice: ICellAddressInformation;
  };
  exitStrategy: {
    acquisitionFee: ICellAddressInformation;
    dispositionFee: ICellAddressInformation;
    amountAllocatedToLand: ICellAddressInformation;
    depreciation: ICellAddressInformation;
    commissions: ICellAddressInformation;
    downPayment: ICellAddressInformation;
    capitalGainsTaxPaid: ICellAddressInformation;
  };
  projectedLoan: {
    amount: ICellAddressInformation;
    interest: ICellAddressInformation;
    amortizationyears: ICellAddressInformation;
    loanTerms: ICellAddressInformation;
    paymentsPerYear?: ICellAddressInformation;
  };
  incomeRowInformation: {
    begginingColumn: ICellAddressInformation;
    year: number;
    units: number;
    rentRoll: number;
    salesPrice: number;
    propertyManagement: number;
    insurance: number;
    assetmanagement: number;
    reserve: number;
    repairs: number;
    vacancy: number;
    taxes: number;
    extraExpenses: number;
  };
}

export const propertyProjectionCellAddressInfo: /*propertyProjectionCellAddressInfo */ IPropertyProyectionExcelCellInformation =
  {
    propertyName: {
      row: 3,
      column: 'C',
      identification: 'C3',
    },
    value: {
      totalValue: {
        row: 5,
        column: 'C',
        identification: 'C5',
      },
      purchasePrice: {
        row: 6,
        column: 'C',
        identification: 'C6',
      },
    },
    exitStrategy: {
      acquisitionFee: {
        row: 8,
        column: 'C',
        identification: 'C8',
      },
      dispositionFee: {
        row: 9,
        column: 'C',
        identification: 'C9',
      },
      amountAllocatedToLand: {
        row: 10,
        column: 'C',
        identification: 'C10',
      },
      depreciation: {
        row: 11,
        column: 'C',
        identification: 'C11',
      },
      commissions: {
        row: 12,
        column: 'C',
        identification: 'C12',
      },
      downPayment: {
        row: 13,
        column: 'C',
        identification: 'C13',
      },
      capitalGainsTaxPaid: {
        row: 14,
        column: 'C',
        identification: 'C14',
      },
    },
    projectedLoan: {
      amount: {
        row: 16,
        column: 'C',
        identification: 'C16',
      },
      interest: {
        row: 17,
        column: 'C',
        identification: 'C17',
      },
      amortizationyears: {
        row: 18,
        column: 'C',
        identification: 'C18',
      },
      loanTerms: {
        row: 19,
        column: 'C',
        identification: 'C19',
      },
    },
    incomeRowInformation: {
      begginingColumn: {
        column: 'D',
        columnNumber: 3,
      },
      year: 23,
      units: 24,
      rentRoll: 25,
      salesPrice: 26,
      propertyManagement: 27,
      insurance: 28,
      assetmanagement: 29,
      reserve: 30,
      repairs: 31,
      vacancy: 32,
      taxes: 33,
      extraExpenses: 34,
    },
  };
