import { ICellAddressInformation } from './';

export interface IPropertyProfitsAndLossExcelCellInformation {
  dateInfo: ICellAddressInformation;
  propertiesStartInfo: ICellAddressInformation;
}

export interface IPropertyProfitsAndLossDateInfo {
  month: string;
  year: string;
}

export interface IPropertyProfitsAndLossConcept {
  name: string;
  value: number;
  details?: IPropertyProfitsAndLossConcept[];
  total?: number;
}

export interface IPropertyProfitsAndLossExcelInfo {
  dateInfo: IPropertyProfitsAndLossDateInfo;
  propertyName: string;
  propertyId?: string;
  income: IPropertyProfitsAndLossConcept;
  grossProfit: number;
  expenses: IPropertyProfitsAndLossConcept;
  netInformation: {
    netIncome: IPropertyProfitsAndLossConcept;
    operatingIncome: IPropertyProfitsAndLossConcept;
    otherIncome?: IPropertyProfitsAndLossConcept[];
  };
}

export interface IPropertyProfitsAndLossExcelResponse {
  profitsAndLoss: IPropertyProfitsAndLossExcelInfo[];
  errors: string[];
}

export interface IProfitsAndLossRowConcept {
  name: string;
  row: number;
  details?: IProfitsAndLossRowConcept[];
  total?: IProfitsAndLossRowConcept;
}

export interface IProfitsAndLossRowConceptResponse {
  concept: IProfitsAndLossRowConcept;
  newRowLevel: number;
  newConceptName: string;
  currentRow: number;
}

export interface IProfitsAndlossIncomeExcelRows {
  investmentRevenue: IProfitsAndLossRowConcept;
  totalIncome: IProfitsAndLossRowConcept;
  grossProfit: IProfitsAndLossRowConcept;
}

export interface IProfitsAndLossExpensesRowInformationResponse {
  income: IProfitsAndlossIncomeExcelRows;
  currentRow: number;
}

export interface IProfitsAndlossNetIncomeExcelRowsInfo {
  netOperatingIncome: IProfitsAndLossRowConcept;
  netIncome: IProfitsAndLossRowConcept;
  otherIncomes: IProfitsAndLossRowConcept[];
}

export interface IProfitsAndLossSheetRowsInformation {
  income: IProfitsAndlossIncomeExcelRows;
  expenses: IProfitsAndLossRowConcept;
  netIncome: IProfitsAndlossNetIncomeExcelRowsInfo;
}
