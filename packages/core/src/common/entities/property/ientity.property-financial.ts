import { IBaseEntityProperty } from './ientity.property';
import { IBaseEntityPropertyFinancialQuarter } from './';

export interface IBaseEntityPropertyFinancial {
  _id?: string;
  property: string | IBaseEntityProperty;
  totalValue: number;
  initialInvestment: number;
  purchasePrice: number;
  aquisitionFee: number;
  dispositionFee: number;
  amountAllocatedToLand: number;
  depreciation: number;
  commissions: number;
  downPayment: number;
  capitalGainsTaxPaid: number;
  salesPrice: {
    year1: number;
    year2: number;
    year3: number;
    year4: number;
    year5: number;
  };
  quarters?: [IBaseEntityPropertyFinancialQuarter] | [string];
}
