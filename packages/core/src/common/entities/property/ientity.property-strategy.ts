export interface PropertyStrategy {
  id: string;
  label: string;
}
