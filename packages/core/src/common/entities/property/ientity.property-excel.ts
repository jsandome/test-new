import { IBndBaseGCloudInfra } from '../../../infra';
import { IPropertyProjection, IBaseEntityProperty } from './';

export interface IExcelPropertyInformation {
  sheetName: string;
  properties: IBaseEntityProperty[];
  errors: IPropertyBaseError[];
}

export interface IExcelPropertiesAndProjections {
  propertiesInfo: IExcelPropertyInformation;
  projections: IBaseSheetProjectionData[];
}

export interface IExcelPropertiesAndProjectionsSaveResponse {
  properties: IExcelPropertiesResponse[];
  projections: IExcelProjectionsResponse[];
}

export interface IExcelPropertiesProfitsAndLossResponse {
  profitsAndLossIds: string[];
}

interface IExcelPropertiesResponse {
  id: string;
  name: string;
}

interface IExcelProjectionsResponse {
  id: string;
}

export interface IBasePropertyImportInformation {
  column: string;
  excelName: string;
  importDate: string;
  isValid: Boolean;
}
export interface IPropertyPropertyInfoByExcelParams {
  pathFile: string;
  userRequest?: string;
}

export interface IPropertyBaseError {
  column: string;
  errors: string[];
}

export interface IPropertiesDuplicatedExcelParams {
  names: string[];
  address: string[];
}

export interface IBasePropertyExcelValidation {
  id: string;
  name: string;
  address: string;
}

export interface IBaseSheetProjectionData {
  sheetName: string;
  projection: IPropertyProjection;
  errors: string[];
}

export interface IPropertySaveExcelParams {
  propertiesAndProjections: IExcelPropertiesAndProjections;
  userRequest: string;
  bndGCloudInfra: IBndBaseGCloudInfra;
}

/*Excel information */
export interface IPropertyColumnInformation {
  propertyId: string;
  column: string;
}

export interface ICellAddressInformation {
  row?: number;
  column?: string;
  columnNumber?: number;
  identification?: string;
}
