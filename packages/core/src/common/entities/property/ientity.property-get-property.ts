import {
  IBaseEntityPropertyFundResponse,
  IBaseEntityPropertyPackageResponse,
} from './ientity.property';

export interface IBEPropertyGetParams {
  status: string[];
  type: string[];
  category: string[];
}

export interface IBEPropertyGetResponseCategoryCheck {
  funds: boolean;
  single: boolean;
  package: boolean;
}

export interface FundPropertyResponse {
  id: string;
  name: string;
  properties: IBaseEntityPropertyFundResponse[];
}
export interface PackagePropertyResponse {
  id: string;
  name: string;
  properties: IBaseEntityPropertyPackageResponse[];
}

export interface IBEGetPropertiesResponse {
  funds: FundPropertyResponse[];
  packages: PackagePropertyResponse[];
  single: IBaseEntityPropertyPackageResponse[];
}

export interface IBEParamsGetProperty {
  previewData?: boolean;
  financial?: boolean;
}
