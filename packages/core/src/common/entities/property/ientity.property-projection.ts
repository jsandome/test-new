import { IBaseEntityProperty } from '.';

export interface IBasePropertyProjection {
  property: string | IBaseEntityProperty;
  totalValue: number;
  purchasePrice: number;
  aquisitionFee: number;
  dispositionFee: number;
  amountAllocatedToLand: number;
  depreciation: number;
  commissions: number;
  downPayment: number;
  capitalGainsTaxPaid: number;
  extraExpenses: IBasePropertyProjectionExtraExpense[];
  loans: IBasePropertyProjectionLoan[];
  projectionYears: IBasePropertyProjectionYear[];
}

export interface IBasePropertyProjectionYear {
  propertyProjection: IBasePropertyProjection | string;
  year: number;
  units: number;
  rent: number;
  vacancy: number;
  propertyManagement: number;
  assetManagement: number;
  taxes: number;
  insurance: number;
  repairs: number;
  reserve: number;
  salesPrice: number;
}

export interface IBasePropertyProjectionLoan {
  propertyProjection: IBasePropertyProjection | string;
  amount: number;
  interest: number;
  termYears: number;
  paymentsPerYear: number;
  amortizationYears: number;
  ltc?: number;
}

export interface IBasePropertyProjectionExtraExpense {
  name: string;
  years: IBasePropertyProjectionExtraExpenseYear[];
}

export interface IBasePropertyProjectionExtraExpenseYear {
  year: number;
  amount: number;
}
