export interface PropertyType {
  id: string;
  label: string;
}
