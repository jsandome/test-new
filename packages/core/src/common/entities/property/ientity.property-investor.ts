export interface IBaseEntityInvestorProperty {
  _id?: string;
  id: string;
  property?: string;
  minCommitment: number;
  initCommitment: number;
  totalCommitment: number;
  termYears: number;
}

export interface IBEPropertyInvestorResponse {
  id?: string;
  name: string;
  minCommitment: number;
  initCommitment: number;
  totalCommitment: number;
  termYears: number;
}

export interface IBEPropertyLoans {
  id: string;
  loanAmount: number;
  scheduledPayment: number;
  interestRate: number;
  amortization: number;
  share: number;
  annualDebtService: number;
}
