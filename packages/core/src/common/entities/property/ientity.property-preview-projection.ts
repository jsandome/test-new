import BigNumber from 'bignumber.js';
import { EnumPropertyTypes } from '../../../common';
import {
  IBaseEntityCounties,
  IBaseEntityStates,
  InterfacePropertyStrategies,
} from '../locations';
import { IBaseOperativeExpense } from './';

/*IBasePropertyProjectionOperativeInfo */
export interface IBasePPOperativeInfo {
  year: number;
  rentRoll: number;
  scheduleGrossIncome: number;
  vacancy: number;
  propertyManagement: number;
  assetManagement: number;
  taxes: number;
  insurance: number;
  repairs: number;
  reserve: number;
  units: number;
  extraExpenses?: IOperatingExtraExpense[];
  totalOperatingExpenses: BigNumber;
  netOperatingIncome: BigNumber;
  capRate: BigNumber;
  purchasePriceAndImprovements: BigNumber;
}

export interface IOperatingExtraExpense {
  name: string;
  amount: number;
}

export interface IBaseScheduleOfProspective {
  year: number;
  annualDebtService: number;
  annualCashFlow: number;
  annualCashOnCashReturn: number;
  eoyPrincipalBalance: number;
  annualEquityBuildUp: number;
  annualEquityReturnOnEquityBuildIp: number;
  totalAnnualReturn: number;
}

export interface IBaseDebtOverview {
  initialEquity: number;
  amount: number;
  ltc?: number;
  interestRate: number;
  amortizationYears: number;
  loanTerm: number;
  schedulePayment: number;
  scheduleOfProspectives: IBaseScheduleOfProspective[];
}

export interface IBaseExitStrategy {
  year: number;
  salesPrice: number;
  costOfSale: number;
  propertyTaxBasis: number;
  amountSubjecttoCapitalGainsTax: number;
  capitalGainsTaxPaid: number;
  debtPrincipalBalance: number;
  cashReceivedFromSale: number;
  cashFlowReceived: number;
  totalReturn: number;
  totalAnnualROI: number;
}

export interface IBaseEntityPropertyPreview {
  name: string;
  address: string;
  type: EnumPropertyTypes;
  state: IBaseEntityStates;
  county: IBaseEntityCounties;
  bedRooms: number;
  bathRooms: number;
  size: number;
  parcelSize: number;
  parcelNumber: number;
  strategies: InterfacePropertyStrategies;
  propertyDescription: string;
  aquisitionFee: number;
  commissions: number;
  dispositionFee: number;
  depreciation: number;
  amountAllocatedToLand: number;
  downPayment: number;
  aquisitionPrice: number;
  capitalGainsTaxPaid?: number;
  extraExpenses?: IPropertyProjectionExtraExpenses[];
  operativeInfo: IBasePPOperativeInfo[];
  debtOverviews: IBaseDebtOverview[];
  exitStrategies: IBaseExitStrategy[];
}

export interface IBEPropertyPreviewInput {
  year: number;
  quarter: number;
  totalValue: number;
  initialInvestment: number;
  purchasePrice: number;
  totalUnits: number;
  rents: {
    month1: number;
    month2: number;
    month3: number;
  };
  propertyManagement: number;
  assetManagement: number;
  repairsMaintenance: number;
  taxes: number;
  insurance: number;
  reserve: number;
  vacancy: number;
  operativeExpenses?: IBaseOperativeExpense[];
}

export interface IBEPropertyPreviewInput {
  year: number;
  quarter: number;
  totalValue: number;
  initialInvestment: number;
  purchasePrice: number;
  totalUnits: number;
  rents: {
    month1: number;
    month2: number;
    month3: number;
  };
  propertyManagement: number;
  assetManagement: number;
  repairsMaintenance: number;
  taxes: number;
  insurance: number;
  reserve: number;
  vacancy: number;
  operativeExpenses?: IBaseOperativeExpense[];
}

export interface IPropertyOperatingYear {
  year: number;
  rent: number;
  vacancy: number;
  propertyManagement: number;
  assetManagement: number;
  taxes: number;
  insurance: number;
  repairs: number;
  reserve: number;
  units: number;
  salesPrice: number;
  extraExpenses?: IOperatingExtraExpense[];
  excelImportInformation?: IOperativeYearExcelInformation;
}

export interface IOperativeYearExcelInformation {
  column: string;
}

export interface ILoansByPropertyParams {
  amount: number;
  interest: number;
  termYears: number;
  paymentsPerYear?: number;
  amortizationYears: number;
  ltc?: number;
}

export interface IPropertyProjection {
  propertyName?: string;
  propertyId?: string;
  operativeInfo: IPropertyOperatingYear[];
  aquisitionFee: number;
  dispositionFee: number;
  amountAllocatedToLand: number;
  depreciation: number;
  commissions: number;
  downPayment: number;
  purchasePrice: number;
  totalValue: number;
  capitalGainsTaxPaid: number;
  loans: ILoansByPropertyParams[];
  extraExpenses?: IPropertyProjectionExtraExpenses[];
}

export interface IPropertyProjectionExtraExpensesYear {
  year: number;
  amount: number;
}

export interface IPropertyProjectionExtraExpenses {
  name: string;
  years: IPropertyProjectionExtraExpensesYear[];
}

export interface IPropertyProjectionParams {
  projectionInformation: IPropertyProjection;
  idProperty: string;
}

export interface IPropertyCreateProjectionResponse {
  idProjection: string;
}
