export interface IBaseEntityPropertyFinancialCreateResponse {
  propertyId: string;
  financialId: string;
  financialQuarterId: string[];
}
