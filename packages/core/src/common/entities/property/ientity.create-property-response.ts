export interface IBaseEntityPropertyResponse {
  id: string;
  isDeleted?: boolean;
}
