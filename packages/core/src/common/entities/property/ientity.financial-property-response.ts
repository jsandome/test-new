import { IBaseOperativeExpense } from './';

export interface IQueryEntityPropertyFinancial {
  propertyId: string;
  financialId: string;
  quarter: number;
  year: number;
}
export interface IBaseEntityPropertyFinancialQuarterResponse {
  year: number;
  quarter: number;
  totalUnits: number;
  rents: {
    month1: number;
    month2: number;
    month3: number;
  };
  propertyManagement: number;
  assetManagement: number;
  repairsMaintenance: number;
  taxes: number;
  insurance: number;
  reserve: number;
  vacancy: number;
  operativeExpenses?: IBaseOperativeExpense[];
}

export interface IBaseEntityPropertyFinancialResponse {
  //financial
  totalValue: number;
  initialInvestment: number;
  purchasePrice: number;
  aquisitionFee: number;
  dispositionFee: number;
  amountAllocatedToLand: number;
  depreciation: number;
  commissions: number;
  downPayment: number;
  capitalGainsTaxPaid: number;
  salesPrice: {
    year1: number;
    year2: number;
    year3: number;
    year4: number;
    year5: number;
  };
  //quarter
  quarter: IBaseEntityPropertyFinancialQuarterResponse;
}
