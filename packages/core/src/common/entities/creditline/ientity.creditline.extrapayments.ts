export interface IExtraPayments {
  name: string;
  date: Date;
  amount: number;
}
