import { IExtraPayments, IPropertyCreditlineShare } from './';

export interface IBaseEntityCreditline {
  id?: string;
  name: string;
  principal: number;
  interest: number;
  extraPayments: IExtraPayments[];
  active: boolean;
}

export interface IParamsCreateCreditline {
  name: string;
  principal: number;
  interest: number;
  extraPayments: IExtraPayments[];
  properties: IPropertyCreditlineShare[];
}

export interface IParamsPreviewCreditline {
  extraPayments: IExtraPayments[];
  properties: IPropertyCreditlineShare[];
}

export interface IParamsUpdateCreditline {
  name: string;
  principal: number;
  interest: number;
  extraPayments: IExtraPayments[];
  properties: IPropertyCreditlineShare[];
}

export interface IBndReturnCreate {
  id: string;
}

export interface IBndReturnPreviewCreditline {
  totalExtraPayments: Number;
}

export interface IBndReturnUpdate {}

export interface ResponseGetCreditline {
  response: IBaseEntityCreditline[];
}
