export interface IPropertyCreditlineShare {
  id: string;
  share: number;
}
