import {
  IBaseInvestorAddress,
  IBaseInvestorBigraphical,
  IPaginationResponse,
} from '../../../common';
import {
  EnumOfferingType,
  EnumOfferingStatus,
  EnumPhoneType,
  EnumEmailType,
} from '../../enums';

export interface IAdditionalProspect {
  id: string;
  primary: boolean;
}

export interface IParamsCreateOfferingProspective {
  offeringType: EnumOfferingType;
  offering: string;
  status: EnumOfferingStatus;
  expectedAmount: number;
  likeliHood: number;
  additionalProspect: IAdditionalProspect[];
}

export interface IParamsUpdateOfferingProspective {
  status: EnumOfferingStatus;
  likeliHood: number;
  expectedAmount: number;
}

export interface IParamsUpdateInvestor {
  email: EnumOfferingStatus;
  emailType: EnumEmailType;
  phone: string;
  phoneType: EnumPhoneType;
  addresses: IBaseInvestorAddress[];
  biographical: IBaseInvestorBigraphical;
  additionalDetails: string;
}

export interface IBndResponseCreateOffering {
  id: string;
}

export interface IBndResponseSelectInvestors {
  id: string;
  name: string;
  email: string;
}

export interface IBndResponseListInvestors {
  id: string;
  name: string;
  company: string;
  commitment: string;
  investment: string;
  lastUpdate: string;
}

export interface IBndResponseUpdateOffering {}

export interface IBndListInvestors {
  id: string;
  name: string;
  company: string;
  commitment: number;
  investment: number;
  lastUpdate: Date;
}

export interface IBndReturnListInvestors {
  pagination?: IPaginationResponse;
  details: IBndListInvestors[];
}
