export interface IBaseCreatedUpdatedBy {
  id?: string;
  dateTime?: Date;
}
