import {
  IBndCreditlineRead,
  IBndCreditlineWrite,
  IBndValidShareData,
} from '../../boundaries';
import {
  IBaseEntityCreditline,
  IBndReturnCreate,
  IBndReturnPreviewCreditline,
  IBndReturnUpdate,
  IExtraPayments,
  IPropertyCreditlineShare,
  IParamsCreateCreditline,
  IParamsPreviewCreditline,
  IParamsUpdateCreditline,
} from '../../entities';
import { BaseError } from '../../base.error';

export class BaseUseCaseCreditline {
  constructor(
    private bndCreditlineRead: IBndCreditlineRead,
    private bndCreditlineWrite: IBndCreditlineWrite,
    private bndValidShareData: IBndValidShareData
  ) {}

  public async execCreateCreditline(
    params: IParamsCreateCreditline
  ): Promise<IBndReturnCreate> {
    try {
      const properties: IPropertyCreditlineShare[] = params.properties;

      await this.bndValidShareData.validProperties(properties);

      return await this.bndCreditlineWrite.createCreditLine(params);
    } catch (error) {
      throw new BaseError(
        error.message,
        error.code,
        error.name,
        error
      );
    }
  }

  public async execPreviewCreditline(
    params: IParamsPreviewCreditline
  ): Promise<IBndReturnPreviewCreditline> {
    try {
      const properties: IPropertyCreditlineShare[] = params.properties;
      const extraPayments: IExtraPayments[] = params.extraPayments;

      await this.bndValidShareData.validProperties(properties);
      await this.bndValidShareData.validPercentages(properties);

      let totalExtraPayments = await this.bndValidShareData.sumExtraPayments(
        extraPayments
      );

      return { totalExtraPayments };
    } catch (error) {
      throw new BaseError(
        `Error trying to preview a credit loan`,
        error.code,
        error.name,
        error
      );
    }
  }

  public async execSelectCreditline(
    search: string
  ): Promise<IBaseEntityCreditline[]> {
    const creditlines = await this.bndCreditlineRead.selectCreditlines(search);

    return creditlines;
  }

  public async execUpdateCreditline(
    creditlineId: string,
    params: IParamsUpdateCreditline
  ): Promise<IBndReturnUpdate> {
    try {
      const properties: IPropertyCreditlineShare[] = params.properties;

      await this.bndValidShareData.validProperties(properties);
      await this.bndValidShareData.validPercentages(properties);
      await this.bndCreditlineWrite.updateCreditline(creditlineId, params);

      return {};
    } catch (error) {
      throw new BaseError(
        `Error trying to preview a credit loan`,
        error.code,
        error.name,
        error
      );
    }
  }

  public async execDeleteCreditline(creditlineId: string): Promise<void> {
    await this.bndCreditlineWrite.deleteCreditline(creditlineId);
  }
}
