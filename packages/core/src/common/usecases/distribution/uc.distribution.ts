import {
  IBndReturnCreateDistribution,
  IBndReturnDeleteDistribution,
  IBndReturnDetailDistribution,
  IBndReturnListDistribution,
  IBndReturnUpdateDistribution,
  IPaginateParams,
  IParamsCreateDistribution,
  IParamsUpdateDistribution,
} from '../../entities';
import {
  IBndBaseDistributionRead,
  IBndBaseDistributionWrite,
} from '../../boundaries';

export class BaseUseCaseDistribution {
  constructor(
    private bndDistributionRead: IBndBaseDistributionRead,
    private bndDistributionWrite: IBndBaseDistributionWrite
  ) {}

  public async execCreateDistribution(
    llcId: string,
    params: IParamsCreateDistribution
  ): Promise<IBndReturnCreateDistribution> {
    return await this.bndDistributionWrite.createNewDistribution(llcId, params);
  }

  public async execListDistribution(
    llcId: string,
    pagination: IPaginateParams,
    search: string
  ): Promise<IBndReturnListDistribution> {
    return await this.bndDistributionRead.listDistributions(
      llcId,
      pagination,
      search
    );
  }

  public async execDetailDistribution(
    llcId: string,
    distributionId: string
  ): Promise<IBndReturnDetailDistribution> {
    return await this.bndDistributionRead.detailDistributions(
      llcId,
      distributionId
    );
  }

  public async execUpdateDistribution(
    llcId: string,
    distributionId: string,
    params: IParamsUpdateDistribution
  ): Promise<IBndReturnUpdateDistribution> {
    return await this.bndDistributionWrite.updateDistribution(
      llcId,
      distributionId,
      params
    );
  }

  public async execDeleteDistribution(
    llcId: string,
    distributionId: string,
    userId: string
  ): Promise<IBndReturnDeleteDistribution> {
    return await this.bndDistributionWrite.deleteDistribution(
      llcId,
      distributionId,
      userId
    );
  }
}
