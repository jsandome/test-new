import {
  IBndPackageRead,
  IBndPackageWrite,
  IBndValidShareData,
} from '../../boundaries';
import {
  IBaseEntityPackage,
  IBndReturnCreatePackage,
  IBndReturnDetailPackage,
  IBndReturnPreviewPackage,
  IBndReturnUpdatePackage,
  IInvestorsDetailPackage,
  IParamsCreatePackage,
  IParamsPreviewPackage,
  IParamsUpdatePackage,
  IPropertiesPackage,
  IPropertyPackageShare,
} from '../../entities';
import { BaseError } from '../../base.error';
import { IBndBaseGCloudInfra } from '../../../infra';

export class BaseUseCasePackage {
  constructor(
    private bndPackageRead: IBndPackageRead,
    private bndPackageWrite: IBndPackageWrite,
    private bndValidShareData: IBndValidShareData,
    private bndGCloudInfra: IBndBaseGCloudInfra
  ) {}

  public async execCreatePackage(
    params: IParamsCreatePackage
  ): Promise<IBndReturnCreatePackage> {
    try {
      const properties: IPropertiesPackage[] = params.properties;
      const investors: IInvestorsDetailPackage[] = params.investors;

      await this.bndValidShareData.validProperties(properties);
      await this.bndValidShareData.validInvestors(
        investors.map((e) => e.id as string)
      );

      const packageId = await this.bndPackageWrite.createPackage(params);

      return packageId;
    } catch (error) {
      throw new BaseError(error.message, error.code, error.name, error);
    }
  }

  public async execPreviewPackage(
    params: IParamsPreviewPackage
  ): Promise<IBndReturnPreviewPackage> {
    const properties: IPropertyPackageShare[] = params.properties;
    await this.bndValidShareData.validProperties(properties);

    const totalValue = await this.bndPackageRead.getTotalValuePackage(
      properties
    );
    return { totalValue };
  }

  public async execGetPackage(
    packageId: string
  ): Promise<IBndReturnDetailPackage> {
    return await this.bndPackageRead.getDetailPackage(packageId);
  }

  public async execSelectPackages(
    search: string
  ): Promise<IBaseEntityPackage[]> {
    return await this.bndPackageRead.selectPackages(
      search,
      this.bndGCloudInfra
    );
  }

  public async execUpdatePackage(
    packageId: string,
    params: IParamsUpdatePackage
  ): Promise<IBndReturnUpdatePackage> {
    try {
      const properties: IPropertiesPackage[] = params.properties;
      const investors: IInvestorsDetailPackage[] = params.investors;

      await this.bndValidShareData.validProperties(properties);
      await this.bndValidShareData.validInvestors(
        investors.map((e) => e.id as string)
      );
      await this.bndPackageWrite.updatePackage(packageId, params);

      return {};
    } catch (error) {
      throw new BaseError(
        `Error trying to update a package. ${error.message}`,
        error.code,
        error.name,
        error
      );
    }
  }

  public async execDeletePackage(packageId: string): Promise<void> {
    await this.bndPackageWrite.deletePackage(packageId);
  }
}
