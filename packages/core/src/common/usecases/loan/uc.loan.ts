import { BaseError } from '../../../common';
import {
  IBndBaseLoanRead,
  IBndBaseLoanMath,
  IBndBaseLoanWrite,
} from '../../boundaries';
import {
  IBaseEntityLoan,
  IBaseEntityLoanCreateResponse,
  ILoanPropertyResponse,
  ISAPropertyShareInput,
  IScheduleAmortizationInputBase,
  IScheduleAmortizationResponse,
} from '../../entities';

export class BaseUseCaseLoan {
  constructor(
    private bndLoanRead: IBndBaseLoanRead,
    private bndLoanWrite: IBndBaseLoanWrite,
    private bndLoanMath: IBndBaseLoanMath
  ) {}

  public async execGetLoans(search: string): Promise<IBaseEntityLoan[]> {
    const loans = await this.bndLoanRead.getLoans(search);
    return loans;
  }

  public async execCreateLoan(
    loanData: IBaseEntityLoan
  ): Promise<IBaseEntityLoanCreateResponse> {
    const loanByName: IBaseEntityLoan =
      await this.bndLoanRead.getLoanByNameIfExist(loanData.name, {
        isDeleted: false,
      });
    if (loanByName) {
      throw new BaseError(
        `Already exist a loan with the name: ${loanData.name}.`,
        409,
        'BaseUseCaseLoan | execCreateLoan'
      );
    }
    if ((loanData.properties as ILoanPropertyResponse[]).length > 0) {
      this.bndLoanMath.isTheLoanPropertyShareValid(
        (loanData.properties as ILoanPropertyResponse[]).map((p) => p.share)
      );
    } else {
      throw new BaseError(
        `The loan need at least a property to be created.`,
        409,
        'BaseUseCaseLoan | execCreateLoan'
      );
    }
    return await this.bndLoanWrite.createLoan(loanData);
  }

  public async execUpdateLoan(
    loanData: IBaseEntityLoan,
    loanId: string
  ): Promise<void> {
    const loan: IBaseEntityLoan = await this.bndLoanRead.findLoanByIdIfExist(
      loanId
    );
    if (!loan) {
      throw new BaseError(
        'Loan not found.',
        404,
        'BaseUseCaseLoan | execUpdateLoan'
      );
    }
    if ((loanData.properties as ILoanPropertyResponse[]).length > 0) {
      this.bndLoanMath.isTheLoanPropertyShareValid(
        (loanData.properties as ILoanPropertyResponse[]).map((p) => p.share)
      );
    }
    const loanByName: IBaseEntityLoan =
      await this.bndLoanRead.getLoanByNameIfExist(loanData.name, {
        isDeleted: false,
      });
    if (loanByName) {
      if (loanByName.id !== loanId)
        throw new BaseError(
          `Already exist a loan with the name: ${loanData.name}.`,
          409,
          'BaseUseCaseLoan | execUpdateLoan'
        );
    }
    await this.bndLoanWrite.updateLoan(loanData, loanId);
  }

  public async execGetAmortizationSchedule(
    loanData: IScheduleAmortizationInputBase
  ): Promise<IScheduleAmortizationResponse> {
    if ((loanData.properties as ISAPropertyShareInput[]).length > 0) {
      this.bndLoanMath.isTheLoanPropertyShareValid(
        (loanData.properties as ISAPropertyShareInput[]).map((p) => p.share)
      );
    }
    return await this.bndLoanMath.getAmortizationSchedule(loanData);
  }

  public async execDeleteLoan(loanId: string): Promise<void> {
    await this.bndLoanWrite.deleteLoan(loanId);
  }
}
