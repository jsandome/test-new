import { BaseError } from '..';
import {
  IBndBasePropertyRead,
  IBndBasePropertyWrite,
  IBndBasePropertyMath,
  IBndBaseLocations,
} from '../boundaries';
import {
  IBaseEntityProperty,
  IBaseEntityPropertyResponse,
  IBaseEntityPropertyFinancialCreate,
  IBaseEntityPropertyFinancialResponse,
  PropertyType,
  PropertyStrategy,
  IBEPropertyGetParams,
  IBEGetPropertiesResponse,
  IBaseEntityPropertyPreview,
  IBaseEntityPropertyFinancialCreateResponse,
  IBaseEntityPropertySelect,
  IPropertyProjection,
  IPropertyCreateProjectionResponse,
  IPropertyProjectionParams,
  IPropertyPropertyInfoByExcelParams,
  IExcelPropertiesAndProjectionsSaveResponse,
  IExcelPropertiesAndProjections,
  IPropertyProfitsAndLossExcelResponse,
  IExcelPropertiesProfitsAndLossResponse,
} from '../entities';
import { IBndBaseExcelManagementInfra, IBndBaseGCloudInfra } from '../../infra';
import { EnumEnvironments } from '../base.environment';
import { EnumPropertyImageFormats } from '../enums/property/enum.property-image-formats';

export class BaseUseCaseProperty {
  constructor(
    private bndPropertyRead: IBndBasePropertyRead,
    private bndPropertyWrite: IBndBasePropertyWrite,
    private bndGCloudInfra: IBndBaseGCloudInfra,
    private bndPropertyMath: IBndBasePropertyMath,
    private bndExcelManagement: IBndBaseExcelManagementInfra,
    private bndLocationsRead: IBndBaseLocations
  ) {}

  public async execGetPropertyTypes(): Promise<PropertyType[]> {
    const propertyTypes = await this.bndPropertyRead.getPropertyTypes();

    return propertyTypes;
  }

  public async execGetPropertyStrategies(): Promise<PropertyStrategy[]> {
    const propertyStrategies =
      await this.bndPropertyRead.getPropertyStrategies();

    return propertyStrategies;
  }

  public async execGetProperty(
    propertyId: string
  ): Promise<IBaseEntityProperty> {
    const property = await this.bndPropertyRead.getProperty(
      propertyId,
      this.bndGCloudInfra
    );
    return property;
  }

  public async execCreatePropertyBase(
    property: IBaseEntityProperty
  ): Promise<IBaseEntityPropertyResponse> {
    const idByAddress = await this.bndPropertyRead.getPropertyIdByAddress(
      property.address
    );
    if (idByAddress) {
      throw new BaseError(
        `A property already exists with the address provided`,
        409,
        'execCreatePropertyBase'
      );
    }
    if (process.env.NODE_ENV !== EnumEnvironments.TEST) {
      const mainPicture = property.mainPicture as string;
      const pictures = property.pictures.map((p) => p as string);
      const pictureNames = pictures.map((fullName) =>
        this._getCreatedImageName(fullName)
      );
      const repeated = pictureNames.some(
        (elem, index) => pictureNames.indexOf(elem, index + 1) > -1
      );
      if (repeated) {
        throw new BaseError(
          `Some image its repeated in pictures array`,
          409,
          'execCreatePropertyBase'
        );
      }
      const propertyImages = [mainPicture].concat(pictures);
      for (let pic of propertyImages) {
        const isTheFormatValid = this._checkImageFormat(pic);
        if (!isTheFormatValid)
          throw new BaseError(
            `The picture ${pic} doesnt have a valid format, the valid formats are ${Object.values(
              EnumPropertyImageFormats
            ).map((e: string) => e)}`,
            400,
            'execCreatePropertyBase'
          );
        const isImageValid = await this.bndGCloudInfra.checkFileExist(pic);
        if (!isImageValid)
          throw new BaseError(
            `The picture ${pic} doesnt exist`,
            404,
            'execCreatePropertyBase'
          );
      }
    }
    const newProperty = await this.bndPropertyWrite.createNewProperty(
      property,
      this.bndGCloudInfra
    );
    return newProperty;
  }

  public async execUpdatePropertyBase(
    property: IBaseEntityProperty,
    propertyId: string
  ): Promise<IBaseEntityPropertyResponse> {
    const idByAddress = await this.bndPropertyRead.getPropertyIdByAddress(
      property.address
    );
    if (idByAddress != propertyId && idByAddress != null) {
      throw new BaseError(
        `A property already exists with the address provided`,
        409,
        'execCreatePropertyBase'
      );
    }

    const newMainPicture: string = property.newMainPicture as string;
    const newPictures: string[] = property.newPictures as string[];
    const deletePictures: string[] = property.newPictures as string[];

    if (process.env.NODE_ENV !== EnumEnvironments.TEST) {
      let propertyImages = [];

      if (newMainPicture) {
        // From tmp folder or properties folder
        propertyImages.push(
          this.bndGCloudInfra.getRelativePathFromFull(newMainPicture)
        );
      }
      if (newPictures) {
        // From tmp folder
        propertyImages.concat(
          newPictures.map((p) => {
            this.bndGCloudInfra.getRelativePathFromFull(p);
          })
        );
      }
      if (deletePictures) {
        // From properties folder
        propertyImages.concat(
          deletePictures.map((p) => {
            this.bndGCloudInfra.getRelativePathFromFull(p);
          })
        );
      }
      for (let pic of propertyImages) {
        const isImageValid = await this.bndGCloudInfra.checkFileExist(pic);
        if (!isImageValid) {
          throw new BaseError(
            `The picture ${pic} doesnt exist`,
            404,
            'execUpdatePropertyBase'
          );
        }
      }
    }

    const updatedProperty = await this.bndPropertyWrite.updateProperty(
      property,
      propertyId,
      this.bndGCloudInfra
    );
    return updatedProperty;
  }

  public async execSelectProperties(
    search: string,
    searchType: string
  ): Promise<IBaseEntityPropertySelect[]> {
    const properties = await this.bndPropertyRead.selectProperties(
      search,
      searchType,
      this.bndGCloudInfra,
      this.bndPropertyMath
    );

    return properties;
  }
  public async execGetProperties(
    params: IBEPropertyGetParams
  ): Promise<IBEGetPropertiesResponse> {
    const propertiesInfo: IBEGetPropertiesResponse =
      await this.bndPropertyRead.getProperties(params, this.bndGCloudInfra);
    return propertiesInfo;
  }

  public async execCreatePropertyFinancialData(
    propertyFinancialData: IBaseEntityPropertyFinancialCreate,
    propertyId: string
  ): Promise<IBaseEntityPropertyFinancialCreateResponse> {
    const isQuarterDateValid = await this.bndPropertyRead.checkQuarterDate(
      propertyFinancialData.year,
      propertyFinancialData.quarter,
      propertyId
    );
    if (!isQuarterDateValid)
      throw new BaseError(
        'The date of the quarter are not valid.',
        400,
        'execCreatePropertyFinancialData'
      );
    const response =
      await this.bndPropertyWrite.createPropertyFinancialInformation(
        propertyFinancialData,
        propertyId
      );
    return response;
  }

  public async execGetPropertyFinancial(
    propertyId: string,
    financialId: string,
    quarter: number,
    year: number
  ): Promise<IBaseEntityPropertyFinancialResponse> {
    const response = await this.bndPropertyRead.getFinancialInfo({
      propertyId,
      financialId,
      quarter,
      year,
    });
    return response;
  }

  public async deleteProperties(
    propertyId: string
  ): Promise<IBaseEntityProperty> {
    const response = await this.execGetProperty(propertyId);
    await this.bndPropertyWrite.deleteProperties(propertyId);
    if (!response || response.isDeleted) {
      throw new BaseError(
        `Property not found`,
        404,
        'BaseUseCaseProperty | deleteProperties'
      );
    }
    return response;
  }

  public async execGetPropertyPreview(
    data: IPropertyProjection,
    idProperty: string
  ): Promise<IBaseEntityPropertyPreview> {
    const response = await this.bndPropertyMath.getPropertyPreview(
      data,
      idProperty
    );
    return response;
  }
  public async execPropertyProjection(
    params: IPropertyProjectionParams
  ): Promise<IPropertyCreateProjectionResponse> {
    const response = await this.bndPropertyWrite.savePropertyProjection(params);
    return response;
  }

  public async execGetPropertyPreviewByExcel(
    data: IPropertyPropertyInfoByExcelParams
  ): Promise<IExcelPropertiesAndProjections> {
    return await this.bndExcelManagement.getExcelPropertiesAndProjectionsInfo({
      bndGCloudInfra: this.bndGCloudInfra,
      bndPropertyRead: this.bndPropertyRead,
      bndLocationsRead: this.bndLocationsRead,
      pathFile: data.pathFile,
      fileName: data.pathFile.slice(
        data.pathFile.lastIndexOf('/') + 1,
        data.pathFile.length
      ),
    });
  }
  public async execSavePropertiesAndProjectionsByExcel(
    data: IPropertyPropertyInfoByExcelParams
  ): Promise<IExcelPropertiesAndProjectionsSaveResponse> {
    const propertiesAndProjections: IExcelPropertiesAndProjections =
      await this.bndExcelManagement.getExcelPropertiesAndProjectionsInfo({
        bndGCloudInfra: this.bndGCloudInfra,
        bndPropertyRead: this.bndPropertyRead,
        bndLocationsRead: this.bndLocationsRead,
        pathFile: data.pathFile,
        fileName: data.pathFile.slice(
          data.pathFile.lastIndexOf('/') + 1,
          data.pathFile.length
        ),
      });
    if (
      propertiesAndProjections.propertiesInfo.errors ||
      propertiesAndProjections.projections.reduce((p, c) => {
        return !!c.errors || p;
      }, false)
    ) {
      throw new BaseError(
        `Errors in the excel information import, run the preview to see details`,
        404,
        'BaseUseCaseProperty | execGetPropertyPreviewByExcel'
      );
    }
    const response: IExcelPropertiesAndProjectionsSaveResponse =
      await this.bndPropertyWrite.savePropertiesAndProjectionsByExcel({
        bndGCloudInfra: this.bndGCloudInfra,
        propertiesAndProjections: propertiesAndProjections,
        userRequest: data.userRequest,
      });
    return response;
  }

  public async execGetPropertyProfitsAndLossPreview(
    data: IPropertyPropertyInfoByExcelParams
  ): Promise<IPropertyProfitsAndLossExcelResponse> {
    return await this.bndExcelManagement.getProfitsAndLossByExcelPreview({
      bndGCloudInfra: this.bndGCloudInfra,
      bndPropertyRead: this.bndPropertyRead,
      bndLocationsRead: this.bndLocationsRead,
      pathFile: data.pathFile,
      fileName: data.pathFile.slice(
        data.pathFile.lastIndexOf('/') + 1,
        data.pathFile.length
      ),
    });
  }

  public async execSavePropertyProfitsAndLossPreview(
    data: IPropertyPropertyInfoByExcelParams
  ): Promise<IExcelPropertiesProfitsAndLossResponse> {
    const profitsAndLoss: IPropertyProfitsAndLossExcelResponse =
      await this.bndExcelManagement.getProfitsAndLossByExcelPreview({
        bndGCloudInfra: this.bndGCloudInfra,
        bndPropertyRead: this.bndPropertyRead,
        bndLocationsRead: this.bndLocationsRead,
        pathFile: data.pathFile,
        fileName: data.pathFile.slice(
          data.pathFile.lastIndexOf('/') + 1,
          data.pathFile.length
        ),
      });
    return await this.bndPropertyWrite.saveProfitsAndLoss(
      profitsAndLoss.profitsAndLoss.filter((e) => e.propertyId != null)
    );
  }

  private _checkImageFormat(image: string): boolean {
    return (
      image.match(
        new RegExp(
          `\\.(${Object.values(EnumPropertyImageFormats).join('|')})$`,
          'i'
        )
      ) != null
    );
  }

  private _getCreatedImageName(imageName: string): string {
    let splitedName: string[] = imageName.split('/');
    const tmpName: string[] = splitedName[splitedName.length - 1].split('-');
    tmpName.splice(0, 1);
    return tmpName.join('-');
  }
}
