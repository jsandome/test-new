import { IBndPackageDocumentsWrite, IBndPackageDocumentsRead } from '../../boundaries';
import { IParamsCreatePackageDocuments,
  IBndResponseCreatePackageDocuments,
  IBndResponseListPackageDocuments,
  IParamsUpdatePackageDocuments } from '../../entities';
import { BaseError } from '../../base.error';

export class BaseUseCasePackageDocuments {
  constructor(
    private bndPackageDocumentsWrite: IBndPackageDocumentsWrite,
    private bndPackageDocumentsRead: IBndPackageDocumentsRead
  ) {}

  public async createPackageDocuments(
    packageDoc: IParamsCreatePackageDocuments
  ): Promise<IBndResponseCreatePackageDocuments> {
    let messagePackageDoc =
      await this.bndPackageDocumentsRead.checkIfNameExists(packageDoc.name, "");

    if (messagePackageDoc)
      throw new BaseError(
        `Already exist a DocuSign Package with the name: ${packageDoc.name}.`,
        409,
        'BaseUseCaseFound | createPackageDocuments'
      );

    return await this.bndPackageDocumentsWrite.createPackageDocuments(packageDoc);
  }

  public async deletePackageDocuments(packageId: string, userDeleteId: string): Promise<void> {
    try {
      await this.bndPackageDocumentsWrite.deletePackageDocuments(packageId, userDeleteId);
    } catch (error) {
      throw new BaseError(
        `An error occurred while trying to delete a packagedocuments: ${error["message"]}`,
        error["code"],
        error["name"],
        error as Error
      );
    }
  }
  public async getPackageDocumentsById(packageId: string): Promise<IBndResponseListPackageDocuments> {
    try {
      return await this.bndPackageDocumentsRead.getPackageDocumentsById(packageId);      
    } catch (error) {
      throw new BaseError(
        error.message,
        error.code,
        error.name,
        error as Error
      );
    }
  }
  public async listPackageDocuments(): Promise<IBndResponseListPackageDocuments[]> {
    try {
      return await this.bndPackageDocumentsRead.listPackageDocuments();      
    } catch (error) {
      throw new BaseError(
        error.message,
        error.code,
        error.name,
        error as Error
      );
    }
  }
  public async updatePackageDocuments(packageId: string, updateParams: IParamsUpdatePackageDocuments): Promise<void> {
    try {
      let messagePackageDoc =
      await this.bndPackageDocumentsRead.checkIfExistsPackageDocuments(packageId);

      if (messagePackageDoc)
        throw new BaseError(
          `No exist a DocuSign Package with te id: ${packageId}.`,
          404,
          'BaseUseCaseNotFound | updatePackageDocuments'
        );
      
      let messagePackageDocByName =
        await this.bndPackageDocumentsRead.checkIfNameExists(updateParams.name, packageId);
  
      if (messagePackageDocByName)
        throw new BaseError(
          `Already exist a DocuSign Package with the name: ${updateParams.name}.`,
          409,
          'BaseUseCaseFound | createPackageDocuments'
        );
      await this.bndPackageDocumentsWrite.updatePackageDocuments(packageId, updateParams);
    } catch (error) {
      throw new BaseError(
        error.message,
        error.code,
        error.name,
        error as Error
      );
    }
  }

}