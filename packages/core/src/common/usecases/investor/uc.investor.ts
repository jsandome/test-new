import { IBndBaseInvestorWrite, IBndBaseInvestorRead } from '../..';
import { IBndMailerInfra } from '../../../infra';
import {
  IBndResponseCreateOffering,
  IBndResponseSelectInvestors,
  IBndReturnListInvestors,
  IPaginateParams,
  IParamsCreateOfferingProspective,
  IParamsUpdateInvestor,
  IParamsUpdateOfferingProspective,
} from '../../entities';

export class BaseUseCaseInvestor {
  constructor(
    private bndMailer: IBndMailerInfra,
    private bndInvestorWrite: IBndBaseInvestorWrite,
    private bndInvestorRead: IBndBaseInvestorRead
  ) {}

  public async execCreateOfferingProspective(
    investorId: string,
    params: IParamsCreateOfferingProspective
  ): Promise<IBndResponseCreateOffering> {
    const offeringId = await this.bndInvestorWrite.createOfferingProspective(
      investorId,
      params,
      this.bndMailer
    );
    return offeringId;
  }

  public async execSelectInvestors(
    search?: string,
    llcId?: string
  ): Promise<IBndResponseSelectInvestors[]> {
    const investors = await this.bndInvestorRead.selectInvestors(search, llcId);
    return investors;
  }

  public async execListInvestors(
    pagination: IPaginateParams
  ): Promise<IBndReturnListInvestors> {
    const listInvestors = await this.bndInvestorRead.listInvestors(pagination);
    return listInvestors;
  }

  public async execUpdateOfferingProspective(
    investorId: string,
    prospectiveId: string,
    params: IParamsUpdateOfferingProspective
  ): Promise<void> {
    await this.bndInvestorWrite.updateOfferingProspective(
      investorId,
      prospectiveId,
      params
    );
  }

  public async execUpdateInvestor(
    investorId: string,
    params: IParamsUpdateInvestor
  ): Promise<void> {
    await this.bndInvestorWrite.updateInvestor(investorId, params);
  }
}
