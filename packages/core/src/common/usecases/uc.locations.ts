import { IBndBaseLocations } from '../boundaries';
import {
  IBaseEntityCountries,
  IBaseEntityStates,
  IBaseEntityCounties,
} from '../entities';

export class BaseUseCaseLocations {
  constructor(private bndLocations: IBndBaseLocations) {}

  public async execGetCountries(
    search: string
  ): Promise<IBaseEntityCountries[]> {
    const countries = await this.bndLocations.getCountries(search);
    return countries;
  }

  public async execGetStates(search: string): Promise<IBaseEntityStates[]> {
    const states = await this.bndLocations.getStates(search);
    return states;
  }

  public async execGetCounties(
    stateId: string,
    search: string
  ): Promise<IBaseEntityCounties[]> {
    const counties = await this.bndLocations.getCountiesByState({
      search,
      stateId,
    });
    return counties;
  }
}
