import {
  IBndReturnCreateCapitallCall,
  IBndReturnGetCapitallCall,
  IBndReturnListCapitalCall,
  IBndReturnSelectCapitallCall,
  IPaginateParams,
  IParamsCreateCapitalCall,
  IParamsUpdateCapitalCall,
} from '../../entities';
import {
  IBndBaseCapitalCallRead,
  IBndBaseCapitalCallWrite,
} from '../../boundaries';

export class BaseUseCaseCapitalCall {
  constructor(
    private bndCapitalCallWrite: IBndBaseCapitalCallWrite,
    private bndCapitalCallRead: IBndBaseCapitalCallRead
  ) {}

  public async execCreateNewCapitalCall(
    params: IParamsCreateCapitalCall,
    llcId: string
  ): Promise<IBndReturnCreateCapitallCall> {
    return this.bndCapitalCallWrite.createNewCapitalCall(params, llcId);
  }

  public async execListCapitalCall(
    llcId: string,
    pagination: IPaginateParams,
    search: string
  ): Promise<IBndReturnListCapitalCall> {
    return await this.bndCapitalCallRead.listCapitalCall(
      llcId,
      pagination,
      search
    );
  }

  public async execDetailCapitalCall(
    llcId: string,
    capitalCallId: string
  ): Promise<IBndReturnGetCapitallCall> {
    return await this.bndCapitalCallRead.detailCapitalCall(
      llcId,
      capitalCallId
    );
  }

  public async execSelectCapitalCall(
    llcId: string,
    search: string
  ): Promise<IBndReturnSelectCapitallCall[]> {
    return await this.bndCapitalCallRead.selectCapitalCall(llcId, search);
  }

  public async execUpdateCapitalCall(
    params: IParamsUpdateCapitalCall,
    llcId: string,
    capitalCallId: string
  ): Promise<void> {
    await this.bndCapitalCallWrite.updateCapitalCall(
      params,
      llcId,
      capitalCallId
    );
  }

  public async execDeleteCapitalCall(
    llcId: string,
    capitalCallId: string,
    userId: string
  ): Promise<void> {
    await this.bndCapitalCallWrite.deleteCapitalCall(
      llcId,
      capitalCallId,
      userId
    );
  }
}
