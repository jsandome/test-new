import { ListTemplates, UserInfoDocusign } from '../../../entities';
import { IBndBaseDocusingInfra } from '../../../../infra';

export class BaseUseCaseDocuSign {
  constructor(private bndDocusignInfra: IBndBaseDocusingInfra) {}

  public async listTemplates(): Promise<ListTemplates[]> {
    return await this.bndDocusignInfra.listTemplates();
  }

  public async getEnvelope(): Promise<void> {
    return await this.bndDocusignInfra.getEnvelope();
  }

  public async sendEnvelope(dataUserDocuSing: UserInfoDocusign): Promise<void> {
    return await this.bndDocusignInfra.sendEnvelope(dataUserDocuSing);
  }
}
