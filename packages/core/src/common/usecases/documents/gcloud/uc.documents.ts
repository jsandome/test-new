import { IBndBaseGCloudInfra } from '../../../../infra';

import {
  IGCloudDocumentCreateParams,
  IGCloudDocumentCreateResponse,
  BaseError,
} from '../../../../common';
import {
  IBndBaseUserRead,
  IBndGCloudDocumentsWrite,
  IBndGCloudDocumentsRead,
} from '../../../boundaries';
import {
  IBaseEntityUser,
  IGCloudDocumentList,
  IGCloudDocumentSelectResponse,
  IGCloudDocumentsQueryParams,
} from '../../../entities';

export class BaseUseCaseGCloudDocuments {
  constructor(
    private bndGCloudInfra: IBndBaseGCloudInfra,
    private bndGCloudDocumentsWrite: IBndGCloudDocumentsWrite,
    private bndUser: IBndBaseUserRead,
    private bndGCloudDocumentsRead: IBndGCloudDocumentsRead
  ) {}

  public async execCreateGCloudDocument(
    params: IGCloudDocumentCreateParams
  ): Promise<IGCloudDocumentCreateResponse> {
    const user: IBaseEntityUser = await this.bndUser.getById(
      params.userRequest
    );
    if (!user)
      throw new BaseError(
        `Cannot find a user with the provided ID`,
        404,
        'BaseUseCaseGCloudDocuments | execCreateGCloudDocument'
      );
    const isValidFile = await this.bndGCloudInfra.checkFileExist(params.path);
    if (!isValidFile)
      throw new BaseError(
        `The document is not found`,
        409,
        'BaseUseCaseGCloudDocuments | execCreateGCloudDocument'
      );

    const response = this.bndGCloudDocumentsWrite.saveGClodDocument(
      this.bndGCloudInfra,
      params
    );
    return response;
  }

  public async execGetListGCloudDocuments(
    queryParams: IGCloudDocumentsQueryParams
  ): Promise<IGCloudDocumentList> {
    const publicURLGCloud: string = await this.bndGCloudInfra.getPublicUrl();
    return await this.bndGCloudDocumentsRead.getGCloudDocumentsList(
      queryParams,
      publicURLGCloud
    );
  }

  public async execDeleteGCloudDocument(documentId: string): Promise<void> {
    await this.bndGCloudDocumentsWrite.deleteGCloudDocument(
      documentId,
      this.bndGCloudInfra
    );
  }

  public async execSelectGCloudDocument(
    searchId: string
  ): Promise<IGCloudDocumentSelectResponse[]> {
    const publicURLGCloud: string = await this.bndGCloudInfra.getPublicUrl();
    return await this.bndGCloudDocumentsRead.selectDocuments(
      searchId,
      publicURLGCloud
    );
  }
}
