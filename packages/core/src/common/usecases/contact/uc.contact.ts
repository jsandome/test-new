import { IBndBaseContactRead, IBndBaseContactWrite } from '../../boundaries';
import {
  IBaseEntityContact,
  IBndResponseCreateContact,
  IBndResponseUpdateContact,
  IBndReturnListContacts,
  IBndSelectContacts,
  IPaginateParamsContacts,
  IParamsCreateContact,
  IParamsUpdateContact,
} from '../../entities';

export class BaseUseCaseContact {
  constructor(
    private bndContactRead: IBndBaseContactRead,
    private bndContactWrite: IBndBaseContactWrite
  ) {}

  public async execCreateContact(
    params: IParamsCreateContact
  ): Promise<IBndResponseCreateContact> {
    return await this.bndContactWrite.createContact(params);
  }

  public async execGetContact(contactId: string): Promise<IBaseEntityContact> {
    return await this.bndContactRead.getContact(contactId);
  }

  public async execListContacts(
    pagination: IPaginateParamsContacts
  ): Promise<IBndReturnListContacts> {
    return await this.bndContactRead.listContacts(pagination);
  }

  public async execSelectContacts(
    search: string
  ): Promise<IBndSelectContacts[]> {
    return await this.bndContactRead.selectContacts(search);
  }

  public async execUpdateContact(
    contactId: string,
    params: IParamsUpdateContact
  ): Promise<IBndResponseUpdateContact> {
    return await this.bndContactWrite.updateContact(contactId, params);
  }

  public async execDeleteContact(contactId: string): Promise<void> {
    return await this.bndContactWrite.deleteContact(contactId);
  }
}
