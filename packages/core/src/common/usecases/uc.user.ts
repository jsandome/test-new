import {
  IBndBaseTokenInfra,
  IBndBaseGCloudInfra,
  IBndMailerInfra,
  IBndDataMail,
  IBndExtraData,
} from '../../infra';
import { BaseError } from '../base.error';
import { IBndBaseUserRead, IBndBaseUserWrite } from '../boundaries';
import {
  IBaseEntityUser,
  IBaseEntityCreateUser,
  IBaseEntityLoginData,
  IBaseEntityLoginResponse,
} from '../entities';
import fs from 'fs';

export class BaseUseCaseUser {
  constructor(
    private bndUserRead: IBndBaseUserRead,
    private bndUserWrite: IBndBaseUserWrite,
    private bndInfra: IBndBaseTokenInfra,
    private bndInfraGCloudInfra: IBndBaseGCloudInfra,
    private bndMailerInfra: IBndMailerInfra
  ) {}

  public async execGetUserById(id: string): Promise<IBaseEntityUser> {
    const user = await this.bndUserRead.getById(id);
    if (!user)
      throw new BaseError(
        `User not found.`,
        404,
        'BaseUseCaseUser | execGetUserById'
      );
    return user;
  }

  public async execCreateNewUser(
    user: IBaseEntityCreateUser
  ): Promise<IBaseEntityCreateUser> {
    const userIdByEmail: string = await this.bndUserRead.getUserIdByEmail(
      user.email
    );
    if (userIdByEmail)
      throw new BaseError(
        `Email already registered.`,
        409,
        'BaseUseCaseUser | execCreateNewUser'
      );
    const hash = await this.bndInfra.generateHash(user.password);
    user.password = hash;
    const newUser: IBaseEntityCreateUser =
      await this.bndUserWrite.createNewUser(user);
    return newUser;
  }

  public async execGetUserLoginData(
    userData: IBaseEntityLoginData
  ): Promise<IBaseEntityLoginResponse> {
    const user: IBaseEntityUser = await this.bndUserRead.getLoginDataByEmail(
      userData.email
    );
    if (!user)
      throw new BaseError(
        `Email not found.`,
        404,
        'BaseUseCaseUser | execGetUserLoginData'
      );
    const validPassword = await this.bndInfra.validPassword({
      hashPassword: user.password,
      inputPassword: userData.password,
    });
    if (!validPassword)
      throw new BaseError(
        `Password not valid`,
        401,
        'BaseUseCaseUser | execGetUserLoginData'
      );
    delete user['password'];
    const token = await this.bndInfra.generateToken({
      email: user.email,
      id: user.id,
      type: user.type,
    });
    return { user, token };
  }

  // TODO
  public async execUploadFile(path: string): Promise<boolean> {
    if (fs.existsSync(path)) {
      await this.bndInfraGCloudInfra.uploadFile(path);
      return true;
    } else {
      throw new BaseError('File not found', 404, 'GCloudStorageError');
    }
  }

  // TODO
  public async execSendMail(): Promise<boolean> {
    let infoMail: IBndDataMail;
    let data: IBndExtraData;
    infoMail = {
      to: 'magdiel.juarez@umvel.com',
      subject: 'Test mail to validate nodemailer CONFIG',
      template: 'client-invite-subscribe/html',
      data,
    };

    await this.bndMailerInfra.send(infoMail);

    return true;
  }
}
