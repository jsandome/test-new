import {
  IBaseEntityLlc,
  IBndReturnLlc,
  InterfacePropertiesLLC,
  IBaseEntityGetLlc,
  IBaseResponseList,
  IPaginateParams,
  IBaseResponseOverviewDetails,
  IBaseGetDetails,
  IBaseResponsePreviewLLC,
} from '../../entities';
import {
  IBndBaseLlcWrite,
  IBndBaseLlcRead,
  IBndValidShareData,
} from '../../boundaries';
import { BaseError } from '../../base.error';
import { IBndBaseGCloudInfra } from '../../../infra';

export class BaseUseCaseLlc {
  constructor(
    private bndLlcWrite: IBndBaseLlcWrite,
    private bndLlcRead: IBndBaseLlcRead,
    private bndValidShareData: IBndValidShareData,
    private bndGCloudInfra: IBndBaseGCloudInfra
  ) {}

  public async createNewLlc(newLlc: IBaseEntityLlc): Promise<IBndReturnLlc> {
    try {
      const existTaxId = await this.bndLlcRead.checkExistsTaxId(newLlc.taxId);
      const properties: InterfacePropertiesLLC[] = newLlc.properties;
      const investorId: string[] = newLlc.investors;

      await this.bndLlcRead.checkExistName(newLlc.name);
      await this.bndValidShareData.validProperties(properties);
      await this.bndValidShareData.validInvestors(investorId);

      if (existTaxId) {
        throw new BaseError(
          `Already exist an Entity with the Tax Id: ${newLlc.taxId}.`,
          409,
          'BaseUseCaseLLC | createNewLlc'
        );
      }

      return await this.bndLlcWrite.createNewLlc(newLlc);
    } catch (error) {
      throw new BaseError(error.message, error.code, error.name, error);
    }
  }

  public async searchLlcs(search?: string): Promise<IBaseEntityGetLlc[]> {
    try {
      const getLlcs = await this.bndLlcRead.searchLlcs(search);
      return getLlcs;
    } catch (error) {
      throw new BaseError(
        `Entity not found`,
        404,
        'BaseUseCaseLLC | searchLlcs'
      );
    }
  }
  public async deleteLlc(llcId: string): Promise<void> {
    try {
      return await this.bndLlcWrite.deleteLlc(llcId);
    } catch (error) {
      throw new BaseError(error.message, error.code, error.name, error);
    }
  }
  public async updateLlc(
    llcId: string,
    updatedLlc: IBaseEntityLlc
  ): Promise<IBndReturnLlc> {
    const existTaxId = await this.bndLlcRead.checkExistsTaxId(updatedLlc.taxId);
    const properties: InterfacePropertiesLLC[] = updatedLlc.properties;
    const investorId: string[] = updatedLlc.investors;

    await this.bndValidShareData.validProperties(properties);
    await this.bndValidShareData.validInvestors(investorId);

    const isIdDuplicated = existTaxId?.id === llcId;

    if (existTaxId && !isIdDuplicated) {
      throw new BaseError(
        `Already exist an Entity with the Tax Id: ${updatedLlc.taxId}.`,
        409,
        'BaseUseCaseLLC | updateFund'
      );
    }

    const response = await this.bndLlcWrite.updateLlc(llcId, updatedLlc);
    return { id: response.id };
  }

  // TODO: The contribution sum flow has yet to be defined

  public async llcList(
    pagination: IPaginateParams
  ): Promise<IBaseResponseList> {
    const data = await this.bndLlcRead.llcList(pagination);
    const totals = {
      activeCommitment: 0,
      contributions: 0,
    };

    const details = [];

    let totalActiveCommitment = 0;

    for (let i = 0; i < data.details.length; i++) {
      const res = totalActiveCommitment;
      const e = data.details[i];
      const activeCommitment =
        await this.bndValidShareData.sumActiveCommitmentLLC(e.activeCommitment);

      const totalActive =
        await this.bndValidShareData.sumActiveCommitmentTotalLLC(
          e.activeCommitment
        );

      const resp = {
        id: e.id,
        contributions: e.contributions,
        name: e.name,
        positions: e.positions,
        activeCommitment,
      };
      totalActiveCommitment = totalActive + res;

      details.push(resp);
    }
    totals.activeCommitment = totalActiveCommitment;

    return { totals, pagination: data.pagination, details };
  }

  //TODO: Change the value of property fields

  public async detailsOverview(
    llcId: string
  ): Promise<IBaseResponseOverviewDetails> {
    const data = await this.bndLlcRead.detailsOverview(
      llcId,
      this.bndGCloudInfra
    );

    let investors = data.investors.filter((vA, iA, array) => {
      return (
        array.findIndex((i) => JSON.stringify(i) === JSON.stringify(vA)) === iA
      );
    });

    let properties = data.properties.filter((vA, iA, array) => {
      return (
        array.findIndex((i) => JSON.stringify(i) === JSON.stringify(vA)) === iA
      );
    });

    return {
      entity: data.entity,
      investors,
      properties,
    };
  }

  public async getDetails(llcId: string): Promise<IBaseGetDetails> {
    try {
      const existLlc = await this.bndLlcRead.getByIdLlc(llcId);
      if (!existLlc) {
        throw new BaseError(
          `Entity not found.`,
          404,
          'BaseUseCaseLLC | getDetails'
        );
      }

      const existLlcInvestor = await this.bndLlcRead.getByIdLlcInvestor(llcId);
      if (existLlcInvestor.length < 0) {
        throw new BaseError(
          `Entity Investor not found.`,
          404,
          'BaseUseCaseLLC | getDetails'
        );
      }
      const existLlcProperty = await this.bndLlcRead.getByIdLlcProperty(llcId);

      if (existLlcProperty.length < 0) {
        throw new BaseError(
          `Entity Property not found.`,
          404,
          'BaseUseCaseLLC | getDetails'
        );
      }

      return await this.bndLlcRead.getDetails(llcId);
    } catch (error) {
      throw new BaseError(error.message, error.code, error.name, error);
    }
  }

  // TODO: Implement the request params llcId and then check the activity for each llc
  public async previewLlc(llcId: string): Promise<IBaseResponsePreviewLLC> {
    return await this.bndLlcRead.previewLlc(llcId);
  }
}
