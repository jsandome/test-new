import {
  IBndReturnGetContribution,
  IBndReturnListContribution,
  IPaginateParams,
  IParamsCreateContribution,
  IParamsRecordContribution,
  IParamsUpdateContribution,
} from '../../entities';
import {
  IBndBaseContributionRead,
  IBndBaseContributionWrite,
} from '../../boundaries';

export class BaseUseCaseContribution {
  constructor(
    private bndContributionRead: IBndBaseContributionRead,
    private bndContributionWrite: IBndBaseContributionWrite
  ) {}

  public async execCreateContribution(
    llcId: string,
    params: IParamsCreateContribution
  ) {
    return await this.bndContributionWrite.createNewContribution(llcId, params);
  }

  public async execRecordContribution(
    llcId: string,
    capitalCallId: string,
    params: IParamsRecordContribution
  ) {
    return await this.bndContributionWrite.recordContribution(
      llcId,
      capitalCallId,
      params
    );
  }

  public async execListContribution(
    llcId: string,
    pagination: IPaginateParams,
    search?: string
  ): Promise<IBndReturnListContribution> {
    return await this.bndContributionRead.getListContributions(
      llcId,
      pagination,
      search
    );
  }

  public async execGetContribution(
    llcId: string,
    contributionId: string
  ): Promise<IBndReturnGetContribution> {
    return await this.bndContributionRead.getContribution(
      llcId,
      contributionId
    );
  }

  public async execUpdateContribution(
    llcId: string,
    contributionId: string,
    params: IParamsUpdateContribution
  ) {
    return await this.bndContributionWrite.updateContribution(
      llcId,
      contributionId,
      params
    );
  }

  public async execDeleteContribution(
    llcId: string,
    contributionId: string,
    userId: string
  ) {
    return await this.bndContributionWrite.deleteContribution(
      llcId,
      contributionId,
      userId
    );
  }
}
