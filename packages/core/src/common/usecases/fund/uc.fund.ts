import {
  IBndBaseFundWrite,
  IBndBaseFundRead,
  IBndValidShareData,
  IBndShareMathFund,
} from '../../boundaries';
import {
  IBaseEntityFunds,
  IBaseReturnDetailFund,
  IBndReturnCreateFund,
  IBndReturnPreviewFund,
  IParamsEntityFunds,
  IParamsPreviewFund,
} from '../../entities';
import { BaseError } from '../../base.error';
import { IBndBaseGCloudInfra } from '../../../infra';

export class BaseUseCaseFound {
  constructor(
    private bndFundWrite: IBndBaseFundWrite,
    private bndFundRead: IBndBaseFundRead,
    private bndShareData: IBndValidShareData,
    private bndGCloudInfra: IBndBaseGCloudInfra,
    private bndShareMathFund: IBndShareMathFund
  ) {}

  public async execCreateNewFund(
    fund: IParamsEntityFunds
  ): Promise<IBndReturnCreateFund> {
    let newFund: IBndReturnCreateFund =
      await this.bndFundRead.checkIfNameExists(fund.name);
    await this.bndShareData.validInvestors(fund.investors.map((i) => i.id));
    await this.bndShareData.validProperties(fund.properties);
    await this.bndShareData.validPackages(fund.packages);

    if (newFund)
      throw new BaseError(
        `Already exist a fund with the name: ${fund.name}.`,
        409,
        'BaseUseCaseFound | createNewFound'
      );

    newFund = await this.bndFundWrite.createFund(fund, this.bndShareMathFund);

    return newFund;
  }

  public async execPreviewFund(
    params: IParamsPreviewFund
  ): Promise<IBndReturnPreviewFund> {
    await this.bndShareData.validProperties(params.properties);
    await this.bndShareData.validPackages(params.packages);

    return {
      totalValue: await this.bndFundRead.getPreviewFund(
        params,
        this.bndShareMathFund
      ),
    };
  }
  public async searchFund(search?: string): Promise<IBaseEntityFunds[]> {
    const searchFund = await this.bndFundRead.searchFund(search);
    if (searchFund) {
      return searchFund;
    } else {
      throw new BaseError(
        `Funds not found`,
        404,
        'BaseUseCaseFound | searchFund'
      );
    }
  }

  public async execGetFund(fundId: string): Promise<IBaseReturnDetailFund> {
    return await this.bndFundRead.getDetailFund(fundId, this.bndGCloudInfra);
  }

  public async updateFund(
    fundId: string,
    updateFund: IParamsEntityFunds
  ): Promise<void> {
    const fundByName = await this.bndFundRead.checkIfNameExists(
      updateFund.name
    );

    await this.bndShareData.validInvestors(
      updateFund.investors.map((i) => i.id)
    );
    await this.bndShareData.validProperties(updateFund.properties);
    await this.bndShareData.validPackages(updateFund.packages);

    if (fundByName && fundByName.id !== fundId) {
      throw new BaseError(
        `Already exist a fund with the name: ${updateFund.name}.`,
        409,
        'BaseUseCaseFound | updateFund'
      );
    }

    await this.bndFundWrite.updateFund(
      fundId,
      updateFund,
      this.bndShareMathFund
    );
  }

  public async deleteFund(fundId: string): Promise<IBndReturnCreateFund> {
    const response = await this.bndFundRead.getFund(fundId);
    if (!response || !response.active) {
      throw new BaseError(
        `Investor with id doesn't exists.`,
        404,
        'BaseUseCaseFound | deleteFund'
      );
    }

    await this.bndFundWrite.deleteFund(fundId);
    return { id: response.id };
  }
}
