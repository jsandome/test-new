import { IBndOnboardingWrite, IBndOnboardingRead } from '../../boundaries';
import { IParamsCreateOnboarding,
  IParamsUpdateStepOneOnboarding } from '../../entities';
import { BaseError } from '../../base.error';
import { IBndBaseTokenInfra, IBndMailerInfra } from '../../../infra';

export class BaseUseCaseOnboarding {
  constructor(
    private bndInfra: IBndBaseTokenInfra,
    private bndMailer: IBndMailerInfra,
    private bndOnboardingWrite: IBndOnboardingWrite,
    private bndOnboardingRead: IBndOnboardingRead
  ) {}

  public async createOnboarding(
    params: IParamsCreateOnboarding
  ): Promise<string> {
    const resp = await this.bndOnboardingRead.getExistDocuments(params.email);
    if (!resp)
      throw new BaseError(
        `Already exist a Onboarding with the email: ${params.email}.`,
        409,
        'BaseUseCaseOnboarding | createOnboarding'
      );
    return await this.bndOnboardingWrite.createOnboarding(
      params,
      this.bndInfra,
      this.bndMailer
    );
  }

  public async  updateStepOneOnboarding(
    params: IParamsUpdateStepOneOnboarding
  ): Promise<void> {
    return await this.bndOnboardingWrite.updateStepOneOnboarding(
      params
    );
  }
}
