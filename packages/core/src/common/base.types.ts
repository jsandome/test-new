export const BASETYPES = {
  Log: Symbol.for('Log'),
  IMongoConfig: Symbol.for('IMongoConfig'),
  IFirebaseConfig: Symbol.for('IFirebaseConfig'),
  IJwtConfig: Symbol.for('IJwtConfig'),
  IGCloudConfig: Symbol.for('IGCloudConfig'),
  IMailerConfig: Symbol.for('IMailerConfig'),
  FrontendEnvironment: Symbol.for('FrontendEnvironment'),
  DocusingEnvironment: Symbol.for('DocusingEnvironment'),
  IPropertyParams: Symbol.for('IPropertyParams'),
};
