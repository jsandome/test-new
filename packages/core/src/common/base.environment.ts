export enum EnumEnvironments {
  DEV = 'DEVELOPMENT',
  LOCAL = 'LOCAL',
  STAGING = 'STAGING',
  DEMO = 'DEMO',
  PROD = 'PRODUCTION',
  TEST = 'TEST',
}
export interface IFirebaseEnvironment {
  projectId: string;
  privateKey: string;
  clientEmail: string;
  bucket: string;
  databaseUrl: string;
}
export interface IMongoEnvironment {
  dbUser: string;
  dbPass: string;
  dbUrl: string;
  dbName: string;
}
export interface IEmailEnvironment {
  smtpUser: string;
  smtpPassword: string;
  smtpHost: string;
  smtpPort: number;
}
export interface IJwtEnvironment {
  secretWord: string;
  sessionTime: string;
  expireDaysToken: number;
}
export interface IGCloudEnvironment {
  private_key: string;
  client_email: string;
  bucket: string;
}
export interface FrontendEnvironment {
  url: string;
}

export interface DocusingEnvironment {
  DOCUSIGN_USER_ID: string;
  DOCUSIGN_ACCOUNT_ID: string;
  DOCUSIGN_APP_ID: string;
  DOCUSIGN_TESTING_TEMPLATE_ID: string;
  DOCUSIGN_PRIVATE_RSA: string;
  DOCUSIGN_AUTH_SERVER: string;
  DOCUSIGN_BASEPATH: string;
  DOCUSIGN_ACCESS_TOKEN: string;
}
export interface IPropertyParamsEnvironment {
  capitalGainsTaxPaid: number;
}
