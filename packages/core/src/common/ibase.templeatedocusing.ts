export interface ITemplateDocusing {
  templateId: string;
  name: string;
  date: Date;
}