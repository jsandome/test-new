/**
 * @class Core Base Error
 * @author Umvel
 * @description Common Clase for errors
 */
export class BaseError extends Error {
  public code: number;
  public source: string;
  public parent: Error;
  /**
   * Constructor
   *
   * @param msg message error
   * @param code HttpCode (example: 404 not found, 500 internal server error)
   * @param name Name of clase to launch the error (ej. InfraBaseEmailError)
   * @param parent Original error
   */
  constructor(msg: string, code?: number, name?: string, parent?: Error) {
    super(msg);
    this.name = name;
    this.code = code;
    this.source = this.stack;
    this.parent = parent;
  }
}
