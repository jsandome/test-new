import { IParamsCreateOnboarding,
  IParamsUpdateStepOneOnboarding } from '../../entities';
import { IBndBaseTokenInfra, IBndMailerInfra } from '../../../infra';

export interface IBndOnboardingWrite {
  createOnboarding(
    params: IParamsCreateOnboarding,
    tokenMod: IBndBaseTokenInfra,
    useMailer: IBndMailerInfra
  ): Promise<string>;

  updateStepOneOnboarding(
    params: IParamsUpdateStepOneOnboarding
  ): Promise<void>;  
}
