import { IBndResponseOnboarding } from '../../entities';

export interface IBndOnboardingRead {
  getCurrentStep(token: string): Promise<string>;
  getExistDocuments(email: string): Promise<boolean>;
  getInfoOnboarding(token: string): Promise<IBndResponseOnboarding>;
  getNotExistDocumentsByToken(token: string): Promise<boolean>;
}
