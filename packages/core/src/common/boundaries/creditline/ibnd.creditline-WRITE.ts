import {
  IBndReturnCreate,
  IParamsCreateCreditline,
  IParamsUpdateCreditline,
} from '../../entities';

export interface IBndCreditlineWrite {
  createCreditLine(params: IParamsCreateCreditline): Promise<IBndReturnCreate>;
  deleteCreditline(creditlineId: string): Promise<void>;
  updateCreditline(
    creditlineId: string,
    params: IParamsUpdateCreditline
  ): Promise<void>;
}
