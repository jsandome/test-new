import { IBaseEntityCreditline } from "../../entities";

export interface IBndCreditlineRead {
  selectCreditlines(search: string): Promise<IBaseEntityCreditline[]>;
}
