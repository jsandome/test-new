import { IBaseEntityLlc, IBndReturnLlc } from '../../entities';

export interface IBndBaseLlcWrite {
  createNewLlc(newLlc: IBaseEntityLlc): Promise<IBndReturnLlc>;
  deleteLlc(llcId: string): Promise<void>;
  updateLlc(llcId: string, updatedLlc: IBaseEntityLlc): Promise<IBndReturnLlc>;
}
