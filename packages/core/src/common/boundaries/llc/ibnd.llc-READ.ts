import { IBndBaseGCloudInfra } from '../../../infra';
import {
  IBaseEntityGetLlc,
  IPaginateParams,
  IBaseResponseList,
  IBaseResponseOverviewDetails,
  IBaseGetDetails,
  IBaseModelLlcInvestor,
  IBaseModelLlcProperty,
  IBaseResponsePreviewLLC,
} from '../../entities';

export interface IBndBaseLlcRead {
  checkExistsTaxId(taxId: string): Promise<IBaseEntityGetLlc>;
  checkExistName(name: string): Promise<void>;
  searchLlcs(search?: string): Promise<IBaseEntityGetLlc[]>;
  getByIdLlc(llcId: string): Promise<IBaseEntityGetLlc>;
  llcList(pagination: IPaginateParams): Promise<IBaseResponseList>;
  detailsOverview(
    llcId: string,
    bndGCloudInfra: IBndBaseGCloudInfra
  ): Promise<IBaseResponseOverviewDetails>;
  getDetails(llcId: string): Promise<IBaseGetDetails>;
  getByIdLlcInvestor(llcId: string): Promise<IBaseModelLlcInvestor[]>;
  getByIdLlcProperty(llcId: string): Promise<IBaseModelLlcProperty[]>;
  previewLlc(llcId: string): Promise<IBaseResponsePreviewLLC>;
}
