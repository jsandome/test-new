import { IBaseExpireDataTokenOut } from '../../infra/ientity.user-Token';
import {
  IBaseEntityCreateUser,
  IUpdateJwt,
  IUpdatePassword,
  IUpdateSecure,
} from '../entities';

export interface IBndBaseUserWrite {
  createNewUser(newUser: IBaseEntityCreateUser): Promise<IBaseEntityCreateUser>;
  updateJwt(params: IUpdateJwt): Promise<void>;
  clearSession(id: string): Promise<void>;
  updateSecure(params: IUpdateSecure): Promise<void>;
  updateExpirePass(
    userId: string,
    expirePass: IBaseExpireDataTokenOut
  ): Promise<void>;
  updatePassword(toUpdate: IUpdatePassword): Promise<void>;
  changePassword(email: string, password: string): Promise<void>;
  update2FASecret(id: string, secret: string): Promise<void>;
}
