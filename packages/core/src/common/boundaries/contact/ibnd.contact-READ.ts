import {
  IBaseEntityContact,
  IBndReturnListContacts,
  IBndSelectContacts,
  IPaginateParamsContacts,
} from '../../entities';

export interface IBndBaseContactRead {
  getContact(contactId: string): Promise<IBaseEntityContact>;
  listContacts(
    pagination: IPaginateParamsContacts
  ): Promise<IBndReturnListContacts>;
  selectContacts(search: string): Promise<IBndSelectContacts[]>;
}
