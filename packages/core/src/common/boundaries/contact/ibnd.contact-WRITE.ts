import {
  IParamsCreateContact,
  IParamsUpdateContact,
  IBndResponseCreateContact,
  IBndResponseUpdateContact,
} from '../../entities';

export interface IBndBaseContactWrite {
  createContact(
    params: IParamsCreateContact
  ): Promise<IBndResponseCreateContact>;
  updateContact(
    contactId: string,
    params: IParamsUpdateContact
  ): Promise<IBndResponseUpdateContact>;
  deleteContact(contactId: string): Promise<void>;
}
