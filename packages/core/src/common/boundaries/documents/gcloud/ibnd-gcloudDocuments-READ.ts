import {
  IGCloudDocumentList,
  IGCloudDocumentsQueryParams,
  IGCloudDocumentSelectResponse,
} from '../../../../common';

export interface IBndGCloudDocumentsRead {
  getGCloudDocumentsList(
    pagination: IGCloudDocumentsQueryParams,
    publicURLGCloud: string
  ): Promise<IGCloudDocumentList>;
  selectDocuments(
    search: string,
    publicUrlGcloud: string
  ): Promise<IGCloudDocumentSelectResponse[]>;
}
