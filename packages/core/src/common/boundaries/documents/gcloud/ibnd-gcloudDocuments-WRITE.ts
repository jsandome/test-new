import { IBndBaseGCloudInfra } from '../../../../infra';
import {
  IGCloudDocumentCreateResponse,
  IGCloudDocumentCreateParams,
} from '../../../../common';

export interface IBndGCloudDocumentsWrite {
  saveGClodDocument(
    gcloudInfra: IBndBaseGCloudInfra,
    params: IGCloudDocumentCreateParams
  ): Promise<IGCloudDocumentCreateResponse>;
  deleteGCloudDocument(
    documentId: string,
    gcloudInfra: IBndBaseGCloudInfra
  ): Promise<void>;
}
