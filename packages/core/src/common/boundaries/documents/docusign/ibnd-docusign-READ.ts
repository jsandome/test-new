import { ListTemplates, UserInfoDocusign } from '../../../entities';
export interface IBndDocusignRead {
  listTemplates(): Promise<ListTemplates[]>;
  getEnvelope(): Promise<void>;
  sendEnvelope(dataUser: UserInfoDocusign): Promise<void>;
}
