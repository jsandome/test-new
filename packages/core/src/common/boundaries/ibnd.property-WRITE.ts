import {
  IBaseEntityPropertyResponse,
  IBaseEntityPropertyFinancialCreateResponse,
  IBaseEntityPropertyFinancialCreate,
  IPropertyCreateProjectionResponse,
  IPropertyProjectionParams,
  IExcelPropertiesAndProjectionsSaveResponse,
  IBaseEntityProperty,
  IPropertySaveExcelParams,
  IPropertyProfitsAndLossExcelInfo,
  IExcelPropertiesProfitsAndLossResponse,
} from '../entities';
import { IBndBaseGCloudInfra } from '../../infra';

export interface IBndBasePropertyWrite {
  createNewProperty(
    property: IBaseEntityProperty,
    bndGCloudInfra: IBndBaseGCloudInfra
  ): Promise<IBaseEntityPropertyResponse>;
  createPropertyFinancialInformation(
    PropertyfinancialInfo: IBaseEntityPropertyFinancialCreate,
    propertyId: string
  ): Promise<IBaseEntityPropertyFinancialCreateResponse>;
  updateProperty(
    property: IBaseEntityProperty,
    propertyId: string,
    bndGCloudInfra: IBndBaseGCloudInfra
  ): Promise<IBaseEntityPropertyResponse>;
  deleteProperties(propertyId: string): Promise<IBaseEntityPropertyResponse>;
  updatePicturesProperty(
    propertyId: string,
    mainPicture: string,
    pictures: string[]
  );
  savePropertyProjection(
    params: IPropertyProjectionParams
  ): Promise<IPropertyCreateProjectionResponse>;
  savePropertiesAndProjectionsByExcel(
    params: IPropertySaveExcelParams
  ): Promise<IExcelPropertiesAndProjectionsSaveResponse>;
  saveProfitsAndLoss(
    profitsAndLoss: IPropertyProfitsAndLossExcelInfo[]
  ): Promise<IExcelPropertiesProfitsAndLossResponse>;
}
