import { IBaseEntityPropertyPreview, IPropertyProjection } from '../entities';

export interface IBndBasePropertyMath {
  getPropertyPreview(
    data: IPropertyProjection,
    idProperty: string
  ): Promise<IBaseEntityPropertyPreview>;

  calculateShareCreditlineLoan(propertyId: string): Promise<number>;
  calculateSharePackageFund(propertyId: string): Promise<number>;
}
