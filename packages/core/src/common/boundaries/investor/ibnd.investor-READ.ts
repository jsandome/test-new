import {
  IBndResponseSelectInvestors,
  IBndReturnListInvestors,
  IPaginateParams,
} from '../../entities';

export interface IBndBaseInvestorRead {
  selectInvestors(
    search?: string,
    llcId?: string
  ): Promise<IBndResponseSelectInvestors[]>;
  listInvestors(pagination: IPaginateParams): Promise<IBndReturnListInvestors>;
}
