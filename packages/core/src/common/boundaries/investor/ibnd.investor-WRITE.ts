import {
  IBndResponseCreateOffering,
  IParamsCreateOfferingProspective,
  IParamsUpdateInvestor,
  IParamsUpdateOfferingProspective,
} from '../../entities';
import { IBndMailerInfra } from '../../../infra';

export interface IBndBaseInvestorWrite {
  createOfferingProspective(
    investorId: string,
    dataOffering: IParamsCreateOfferingProspective,
    bndMailer: IBndMailerInfra
  ): Promise<IBndResponseCreateOffering>;
  updateOfferingProspective(
    investorId: string,
    prospectiveId: string,
    params: IParamsUpdateOfferingProspective
  ): Promise<void>;
  updateInvestor(
    investorId: string,
    params: IParamsUpdateInvestor
  ): Promise<void>;
}
