import { IBaseEntityUser } from '../entities';

export interface IBndBaseUserRead {
  getById(id: string): Promise<IBaseEntityUser>;
  getUserByEmailGETPASS(email: string): Promise<IBaseEntityUser>;
  getLoginDataByEmail(email: string): Promise<IBaseEntityUser>;
  getUserIdByEmail(email: string): Promise<string>;
  getUserByToken(token: string): Promise<IBaseEntityUser>;
  getUserBySecure(email: string, secure: string): Promise<IBaseEntityUser>;
  validateSessionToken(token: string, id: string): Promise<boolean>;
  get2FASECRET(id: string): Promise<string>;
}
