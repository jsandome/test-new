import { IBndResponseListPackageDocuments } from '../../entities';

export interface IBndPackageDocumentsRead {
  checkIfNameExists(
    name: string,
    packageId: string): Promise<Boolean>;
  checkIfExistsPackageDocuments(packageId: string): Promise<Boolean>;
  listPackageDocuments(): Promise<IBndResponseListPackageDocuments[]>;
  getPackageDocumentsById(packageId: string): Promise<IBndResponseListPackageDocuments>;
}
