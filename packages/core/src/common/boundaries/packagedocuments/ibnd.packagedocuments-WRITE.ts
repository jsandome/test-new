import { 
  IParamsCreatePackageDocuments,
  IBndResponseCreatePackageDocuments,
  IParamsUpdatePackageDocuments
} from '../../entities';

export interface IBndPackageDocumentsWrite {
  createPackageDocuments(
    params: IParamsCreatePackageDocuments
  ): Promise<IBndResponseCreatePackageDocuments>;
  deletePackageDocuments(packageDocumentId: string, userDeleteId: string): Promise<void>;
  updatePackageDocuments(packageDocumentId: string, updateParams: IParamsUpdatePackageDocuments): Promise<void>;
}
