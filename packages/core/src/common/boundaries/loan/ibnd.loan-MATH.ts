import {
  IScheduleAmortizationInputBase,
  IScheduleAmortizationResponse,
} from '../../../common/entities';

export interface IBndBaseLoanMath {
  getAmortizationSchedule(
    loanData: IScheduleAmortizationInputBase
  ): Promise<IScheduleAmortizationResponse>;
  isTheLoanPropertyShareValid(shares: number[]): void;
}
