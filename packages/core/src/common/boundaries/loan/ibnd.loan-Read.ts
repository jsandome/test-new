import { IBaseEntityLoan, IGetLoanByNameParams } from '../../entities';
export interface IBndBaseLoanRead {
  getLoans(search: string): Promise<IBaseEntityLoan[]>;
  findLoanByIdIfExist(id: string): Promise<IBaseEntityLoan>;
  getLoanByNameIfExist(
    name: string,
    params: IGetLoanByNameParams
  ): Promise<IBaseEntityLoan>;
}
