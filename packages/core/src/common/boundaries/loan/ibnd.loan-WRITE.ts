import { IBaseEntityLoan, IBaseEntityLoanCreateResponse } from '../../entities';
export interface IBndBaseLoanWrite {
  createLoan(loanData: IBaseEntityLoan): Promise<IBaseEntityLoanCreateResponse>;
  deleteLoan(loanId: string): Promise<void>;
  updateLoan(loanData: IBaseEntityLoan, loanId: string): Promise<void>;
}
