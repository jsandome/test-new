import {
  IBaseEntityFundPackage,
  IBaseEntityFundProperty,
} from '../../entities';

export interface IBndShareMathFund {
  _validateSharePackageInFund(pack: IBaseEntityFundPackage): Promise<void>;
  _validateSharePropertyInFund(prop: IBaseEntityFundProperty): Promise<void>;
}
