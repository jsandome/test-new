import { IBndShareMathFund } from '.';
import { IBndBaseGCloudInfra } from '../../../infra';
import {
  IBndReturnCreateFund,
  IBaseEntityFunds,
  IBaseReturnDetailFund,
  IParamsPreviewFund,
} from '../../entities';

export interface IBndBaseFundRead {
  checkIfNameExists(name: string): Promise<IBndReturnCreateFund>;
  searchFund(search?: string): Promise<IBaseEntityFunds[]>;
  getFund(id: string): Promise<IBaseEntityFunds>;
  getDetailFund(
    id: string,
    bndGCloudInfra: IBndBaseGCloudInfra
  ): Promise<IBaseReturnDetailFund>;
  getPreviewFund(
    params: IParamsPreviewFund,
    bndShareMathFund: IBndShareMathFund
  ): Promise<number>;
}
