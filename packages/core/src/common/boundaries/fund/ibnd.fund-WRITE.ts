import { IBndShareMathFund } from '.';
import { IBndReturnCreateFund, IParamsEntityFunds } from '../../entities';

export interface IBndBaseFundWrite {
  createFund(
    newFund: IParamsEntityFunds,
    bndShareMathFund: IBndShareMathFund
  ): Promise<IBndReturnCreateFund>;
  updateFund(
    id: string,
    fund: IParamsEntityFunds,
    bndShareMathFund: IBndShareMathFund
  ): Promise<void>;
  deleteFund(id: string): Promise<IBndReturnCreateFund>;
}
