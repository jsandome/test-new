import {
  IBaseEntityProperty,
  IBaseEntityPropertyFinancialResponse,
  IQueryEntityPropertyFinancial,
  PropertyType,
  PropertyStrategy,
  IBEPropertyGetParams,
  IBEGetPropertiesResponse,
  IBaseEntityPropertySelect,
  IPropertiesDuplicatedExcelParams,
  IBasePropertyExcelValidation,
} from '../entities';
import { IBndBaseGCloudInfra } from '../../infra';
import { IBndBasePropertyMath } from '.';

export interface IBndBasePropertyRead {
  getPropertyTypes(): Promise<PropertyType[]>;
  getPropertyStrategies(): Promise<PropertyStrategy[]>;
  selectProperties(
    search: string,
    searchType: string,
    bndGCloudInfra: IBndBaseGCloudInfra,
    bndPropertyMath: IBndBasePropertyMath
  ): Promise<IBaseEntityPropertySelect[]>;
  getProperties(
    params: IBEPropertyGetParams,
    bndGCloudInfra: IBndBaseGCloudInfra
  ): Promise<IBEGetPropertiesResponse>;
  getProperty(
    propertyId: string,
    bndGCloudInfra: IBndBaseGCloudInfra
  ): Promise<IBaseEntityProperty>;
  getFinancialInfo(
    financialData: IQueryEntityPropertyFinancial
  ): Promise<IBaseEntityPropertyFinancialResponse>;
  checkQuarterDate(
    year: number,
    quarter: number,
    propertyId: string
  ): Promise<boolean>;
  getPropertyIdByAddress(address: string): Promise<string>;
  getPropertyIdByName(name: string): Promise<string>;
  getPropertiesForExcel(
    data: IPropertiesDuplicatedExcelParams
  ): Promise<IBasePropertyExcelValidation[]>;
}
