import {
  IBaseEntityCountries,
  IBaseEntityCounties,
  IBaseEntityStates,
  IBaseEntityCountyParam,
} from '../entities/locations';

export interface IBndBaseLocations {
  getCountries(search: string): Promise<IBaseEntityCountries[]>;
  getStates(search: string): Promise<IBaseEntityStates[]>;
  getCountiesByState(
    searchData: IBaseEntityCountyParam
  ): Promise<IBaseEntityCounties[]>;
  getStateByName(name: string): Promise<IBaseEntityStates>;
  getCountyByNameAndState(
    name: string,
    state: string
  ): Promise<IBaseEntityCounties>;
}
