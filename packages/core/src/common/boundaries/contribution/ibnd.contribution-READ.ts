import {
  IBndReturnGetContribution,
  IBndReturnListContribution,
} from '../../entities';

export interface IBndBaseContributionRead {
  getListContributions(
    llcId,
    pagination,
    search?
  ): Promise<IBndReturnListContribution>;
  getContribution(
    llcId: string,
    contributionId: string
  ): Promise<IBndReturnGetContribution>;
}
