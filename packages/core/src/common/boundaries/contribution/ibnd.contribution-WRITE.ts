import {
  IBndReturnCreateContribution,
  IBndReturnDeleteContribution,
  IBndReturnRecordContribution,
  IBndReturnUpdateContribution,
  IParamsCreateContribution,
  IParamsRecordContribution,
  IParamsUpdateContribution,
} from '../../entities';

export interface IBndBaseContributionWrite {
  createNewContribution(
    llcId: string,
    newContribution: IParamsCreateContribution
  ): Promise<IBndReturnCreateContribution>;
  recordContribution(
    llcId: string,
    capitalCallId: string,
    newContribution: IParamsRecordContribution
  ): Promise<IBndReturnRecordContribution>;
  updateContribution(
    llcId: string,
    contributionId: string,
    params: IParamsUpdateContribution
  ): Promise<IBndReturnUpdateContribution>;
  deleteContribution(
    llcId: string,
    contributionId: string,
    userId: string
  ): Promise<IBndReturnDeleteContribution>;
}
