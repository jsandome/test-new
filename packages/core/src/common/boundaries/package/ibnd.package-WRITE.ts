import { 
  IBndReturnCreatePackage,
  IParamsCreatePackage,
  IParamsUpdatePackage,
} from '../../entities';

export interface IBndPackageWrite {
  createPackage(params: IParamsCreatePackage): Promise<IBndReturnCreatePackage>;
  updatePackage(packageId: string, params: IParamsUpdatePackage): Promise<void>;
  deletePackage(packageId: string): Promise<void>;
}
