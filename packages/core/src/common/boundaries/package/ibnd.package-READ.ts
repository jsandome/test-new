import {
  IBaseEntityPackage,
  IBndReturnDetailPackage,
  IPropertyPackageShare,
} from '../../entities';
import { IBndBaseGCloudInfra } from '../../../infra';

export interface IBndPackageRead {
  selectPackages(
    search: string,
    bndGCloudInfra: IBndBaseGCloudInfra
  ): Promise<IBaseEntityPackage[]>;
  calculateTotalValue(properties: IPropertyPackageShare[]): Promise<number>;
  getDetailPackage(packageId: string): Promise<IBndReturnDetailPackage>;
  getTotalValuePackage(properties: IPropertyPackageShare[]): Promise<number>;
}
