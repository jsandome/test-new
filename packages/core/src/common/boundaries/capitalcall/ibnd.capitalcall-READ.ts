import {
  IBndReturnGetCapitallCall,
  IBndReturnListCapitalCall,
  IBndReturnSelectCapitallCall,
  IPaginateParams,
} from '../../entities';

export interface IBndBaseCapitalCallRead {
  listCapitalCall(
    llcId: string,
    pagination: IPaginateParams,
    search: string
  ): Promise<IBndReturnListCapitalCall>;
  detailCapitalCall(
    llcId: string,
    capitalCallId: string
  ): Promise<IBndReturnGetCapitallCall>;
  selectCapitalCall(
    llcId: string,
    search: string,
  ): Promise<IBndReturnSelectCapitallCall[]>;
}
