import {
  IBndReturnCreateCapitallCall,
  IParamsCreateCapitalCall,
  IParamsUpdateCapitalCall,
} from '../../entities';

export interface IBndBaseCapitalCallWrite {
  createNewCapitalCall(
    newCapitalCall: IParamsCreateCapitalCall,
    llcId: string
  ): Promise<IBndReturnCreateCapitallCall>;
  updateCapitalCall(
    params: IParamsUpdateCapitalCall,
    llcId: string,
    capitalCallId: string
  ): Promise<void>;
  deleteCapitalCall(
    llcId: string,
    capitalCallId: string,
    userId: string
  ): Promise<void>;
}
