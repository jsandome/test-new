import {
  IBndReturnCreateDistribution,
  IBndReturnDeleteDistribution,
  IBndReturnUpdateDistribution,
  IParamsCreateDistribution,
  IParamsUpdateDistribution,
} from '../../entities';

export interface IBndBaseDistributionWrite {
  createNewDistribution(
    llcId: string,
    newDistribution: IParamsCreateDistribution
  ): Promise<IBndReturnCreateDistribution>;
  updateDistribution(
    llcId: string,
    distributionId: string,
    params: IParamsUpdateDistribution
  ): Promise<IBndReturnUpdateDistribution>;
  deleteDistribution(
    llcId: string,
    distributionId: string,
    userId: string
  ): Promise<IBndReturnDeleteDistribution>;
}
