import {
  IBndReturnDetailDistribution,
  IBndReturnListDistribution,
  IPaginateParams,
} from '../../entities';

export interface IBndBaseDistributionRead {
  listDistributions(
    llcId: string,
    pagination: IPaginateParams,
    search: string
  ): Promise<IBndReturnListDistribution>;
  detailDistributions(
    llcId: string,
    distributionId: string
  ): Promise<IBndReturnDetailDistribution>;
}
