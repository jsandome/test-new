import {
  IBaseEntityFundPackage,
  IExtraPayments,
  IPropertyCreditlineShare,
  IPropertyPackageShare,
} from '../../entities';

export interface IBndValidShareData {
  sumExtraPayments(extraPayments: IExtraPayments[]): Promise<Number>;
  validPercentages(
    percentages: IPropertyCreditlineShare[] | IPropertyPackageShare[]
  ): Promise<void>;
  validProperties(
    properties: IPropertyCreditlineShare[] | IPropertyPackageShare[]
  ): Promise<void>;
  validInvestors(investors: string[]): Promise<void>;
  validPackages(packages: IBaseEntityFundPackage[]): Promise<void>;
  validInvestors(investors: string[]): Promise<void>;
  sumActiveCommitmentLLC(data: number[]): number;
  sumActiveCommitmentTotalLLC(sum: number[]): number;
}
