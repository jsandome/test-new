export interface IBaseName {
  firstName: string;
  lastName?: string;
}

export interface IBaseInvestorAddress {
  address: string;
  city: string;
  zipCode: string;
  county: string;
  state: string;
  isPrincipal?: boolean;
}

export interface IBaseInvestorBigraphical {
  name: string;
  city: string;
  jobTitle: string;
  companyName: string;
  referrer: string;
  assignee: string;
}
