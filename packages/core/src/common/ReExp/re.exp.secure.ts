export const secureToken = /^[A-Za-f0-9]{32}$/;

export const secureCode = /^[A-Z0-9]{6}$/;

export const code6Digits = /^(|[a-zA-Z0-9]{6})$/;
