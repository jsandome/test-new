export interface IBaseLogger {
  error(message: string): void;

  debug(message: string): void;

  info(message: string): void;

  warning(message: string): void;
}
