import { EnumUserTypes, IBaseName } from '../../common';

export interface IEntityAdmin {
  id?: string;
  name: IBaseName;
  phone: string;
  type: EnumUserTypes;
  active: boolean;
}
