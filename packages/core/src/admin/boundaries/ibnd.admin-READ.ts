import { IEntityAdmin } from '../entities';

export interface IBndAdminRead {
  getById(id: string): Promise<IEntityAdmin>;
}
