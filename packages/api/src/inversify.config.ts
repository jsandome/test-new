import { Container } from 'inversify';

import { COMMON, ConfigModule, DataModule, InfraModule } from './config';
import {
  AuthModule,
  BaseMiddlewareError,
  CommonModule,
  MiddlewareToken,
  MiddlewareParameters,
  MiddlewareOnboardingToken
} from './modules';

const capaContainer = new Container();
capaContainer.load(ConfigModule);
capaContainer.load(InfraModule);
capaContainer.load(CommonModule);
capaContainer.load(AuthModule);
capaContainer.load(DataModule);

capaContainer
  .bind<MiddlewareParameters>(COMMON.midParameters)
  .to(MiddlewareParameters);
capaContainer
  .bind<BaseMiddlewareError>(COMMON.midError)
  .to(BaseMiddlewareError);
capaContainer.bind<MiddlewareToken>(COMMON.midValidToken).to(MiddlewareToken);
capaContainer.bind<MiddlewareOnboardingToken>(COMMON.midValidOnboargingToken).to(MiddlewareOnboardingToken);

export { capaContainer };
