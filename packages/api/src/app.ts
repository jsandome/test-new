import 'reflect-metadata';
import express, { Application } from 'express';
import * as helmet from 'helmet';
import * as cookieParser from 'cookie-parser';
import * as methodOverride from 'method-override';
import { InversifyExpressServer } from 'inversify-express-utils';
import { Server } from 'http';
import * as morgan from 'morgan';
import chalk from 'chalk';
import figlet from 'figlet';
import { DateTime } from 'luxon';

import { BASETYPES, IBaseLogger } from '@capa/core';
import { MongoConfig } from '@capa/data';
import {
  FirebaseConfig,
  GCloudConfig,
  MailerConfig,
  DocusingConfig,
} from '@capa/infra';
import { capaContainer } from './inversify.config';
import { BaseMiddlewareError } from './modules';
import { COMMON, CorsConfig, EnvCapa, MyStream } from './config';
export class ServerApp {
  private red = chalk.bold.red;
  private green = chalk.bold.green;
  private yellow = chalk.yellow;
  private grey = chalk.bold.grey;
  private blue = chalk.bold.blue;
  private log: IBaseLogger = capaContainer.get<IBaseLogger>(BASETYPES.Log);
  private cors: CorsConfig = capaContainer.get<CorsConfig>(CorsConfig);
  private myStream: MyStream = capaContainer.get<MyStream>(MyStream);

  private _initialConfig(app: Application): void {
    (global as { logger: IBaseLogger }).logger = this.log;
    const handleRejection = (err: Error) => {
      this.log.info('Ocurrió un error y no se pudo controlar');
      this.log.error(`${err}`);
      this.log.error(`${JSON.stringify(err)}`);
    };
    process.on('unhandledRejection', handleRejection.bind(this));
    process.on('uncaughtException', handleRejection.bind(this));

    app.disable('etag');
    app.use(methodOverride.default());
    app.use(cookieParser.default());
    app.use(helmet.default());
    app.use(express.json());
  }
  private _corsConfig(app: Application): void {
    this.log.info('init cors config');
    app.use(this.cors.initCors());
  }
  private async _mongoConfig(): Promise<void> {
    const mongo: MongoConfig = capaContainer.get<MongoConfig>(MongoConfig);
    await mongo.initConnection();
    mongo.setAutoReconnect();
    mongo.setEvents();
  }
  private _firebaseConfig(): void {
    const firebase: FirebaseConfig =
      capaContainer.get<FirebaseConfig>(FirebaseConfig);
    firebase.init();
  }
  private async _logConfig(app: express.Application): Promise<void> {
    app.use(
      morgan.default(
        (tokens, req, res) => {
          const argumentsInBody = req.body.password
            ? JSON.stringify(req.body, null, 4).replace(
                req.body.password,
                '******'
              )
            : JSON.stringify(req.body, null, 4);
          return [
            this.blue('---------------------------------------------------\n'),
            this.yellow(DateTime.now().toISO()),
            this.grey(tokens.method(req, res)),
            this.grey(tokens.url(req, res)),
            '\n',
            `Arguments in body: ${argumentsInBody}\n`,
            `Arguments in query: ${JSON.stringify(req.query, null, 4)}\n`,
          ].join(' ');
        },
        { immediate: true, stream: this.myStream }
      )
    );

    app.use(
      morgan.default(
        (tokens, req, res) => {
          return [
            this.blue('---------------------------------------------------\n'),
            this.yellow(DateTime.now().toISO()),
            this.grey(tokens.method(req, res)),
            this.grey(tokens.url(req, res)),
            this.red(tokens.status(req, res)),
            this.green(tokens.res(req, res, 'content-length')),
            this.green('-'),
            this.red(tokens['response-time'](req, res), 'ms'),
            '\n',
          ].join(' ');
        },
        { stream: this.myStream }
      )
    );
  }
  private _gCloudConfig(): void {
    const gcloud: GCloudConfig = capaContainer.get<GCloudConfig>(GCloudConfig);
    gcloud._init();
  }

  private _mailerConfig(): void {
    const mailer: MailerConfig = capaContainer.get<MailerConfig>(MailerConfig);
    mailer._init();
  }

  private _docusingConfig(): void {
    const docusing: DocusingConfig =
      capaContainer.get<DocusingConfig>(DocusingConfig);
    docusing._init();
  }

  public async listen(): Promise<Server> {
    await this._mongoConfig();
    this._firebaseConfig();
    this._gCloudConfig();
    this._mailerConfig();
    this._docusingConfig();
    const env: EnvCapa = capaContainer.get<EnvCapa>(EnvCapa);
    const err: BaseMiddlewareError = capaContainer.get<BaseMiddlewareError>(
      COMMON.midError
    );

    const inversifyServer = new InversifyExpressServer(capaContainer);
    inversifyServer
      .setConfig(async (app: Application) => {
        this._initialConfig(app);
        this._corsConfig(app);
        await this._logConfig(app);
      })
      .setErrorConfig((app) => {
        app.use(err.handler.bind(err));
      });
    const serverInstance = inversifyServer.build();
    return serverInstance.listen(env.port, () => {
      this.log.info(
        chalk.greenBright(
          `${figlet.textSync(`CAPA ${env.environment}`, {
            horizontalLayout: 'full',
          })}`
        )
      );
      this.log.info(chalk.yellow(`Server listening on port ${env.port}`));
    });
  }
}
