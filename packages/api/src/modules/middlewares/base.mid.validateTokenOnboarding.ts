import { inject, injectable } from 'inversify';
import { BaseMiddleware } from 'inversify-express-utils';
import {
  BaseError,
  BASETYPES,
  BaseUseCaseInfra,
  IBaseLogger,
} from '@capa/core';
import { NextFunction, Request, Response } from 'express';

@injectable()
export class MiddlewareOnboardingToken extends BaseMiddleware {
  @inject('BaseUseCaseInfra') private ucInfra: BaseUseCaseInfra;
  constructor(@inject(BASETYPES.Log) private logger: IBaseLogger) {
    super();
  }
  public async handler(req: Request, res: Response, next: NextFunction) {
    try {
      const token = req.params['token'];
      const userInfo = await this.ucInfra.execValidOnboardingTokenJwt(
        token
      );
      if (req.body) {
        req.body.userRequestId = userInfo.id;
      }else {
        req["userRequestId"] = userInfo.id;
      }
      next();
    } catch (err) {
      this.logger.error(`Invalid session.`);
      next(new BaseError(err.message, err.code, err.name, err));
    }
  }
}
