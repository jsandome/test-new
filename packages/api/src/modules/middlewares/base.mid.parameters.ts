import { BaseMiddleware } from 'inversify-express-utils';
import { NextFunction, Request, Response } from 'express';
import { validationResult } from 'express-validator';
import { injectable } from 'inversify';
import { IDtoBaseOutput } from '../';

@injectable()
export class MiddlewareParameters extends BaseMiddleware {
  constructor() {
    super();
  }
  public handler(req: Request, res: Response, next: NextFunction) {
    const paramErrors = validationResult(req);
    if (!paramErrors.isEmpty()) {
      const messages = [];
      const params = [];
      const errors = paramErrors.mapped();
      const response: IDtoBaseOutput<{}> = {
        response: {},
        message: '',
        details: '400 - Badrequest: ',
      };
      Object.values(errors).forEach((item) => {
        messages.push(item['msg']);
        params.push(item['param']);
      });
      response.message = messages.join(', ');
      response.details = params.join(', ');
      global.logger.error(response);
      return res.status(400).json(response);
    }
    next();
    return null;
  }
}
