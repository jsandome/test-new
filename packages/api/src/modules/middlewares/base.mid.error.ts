import { NextFunction, Request, Response } from 'express';
import { inject, injectable } from 'inversify';
import { BaseError, BASETYPES, IBaseLogger } from '@capa/core';
import { ApiErrors, IDtoBaseOutput } from '../';

@injectable()
export class BaseMiddlewareError {
  private _hideErrorDetails: boolean;
  constructor(@inject(BASETYPES.Log) private _log: IBaseLogger) {
    this._hideErrorDetails = String(process.env.NODE_ENV) === 'PRODUCTION';
  }

  public handler(err: Error, req: Request, res: Response, next: NextFunction) {
    let status = 500;
    let response: IDtoBaseOutput<{}>;
    global.logger.error(err);

    if (err instanceof ApiErrors) {
      const errDetails = `Details: ${JSON.stringify(
        err.errorCodeObject
      )}|Stack: ${err.stack}`;
      if (this._hideErrorDetails) this._log.error(errDetails);
      response = {
        response: null,
        message: err.message,
        details: this._hideErrorDetails ? '' : errDetails,
      };
      if (err.data) {
        response.details += `|Data: ${err.data}`;
      }
      status = err.errorCodeObject.status;
    } else if (err instanceof BaseError) {
      const errDetails = `${err.code} - ${err.name}: ${err.stack}`;
      if (this._hideErrorDetails) this._log.error(errDetails);
      if (err.parent) global.logger.error(err.parent);
      response = {
        response: null,
        message: err.message,
        details: this._hideErrorDetails ? '' : errDetails,
      };
      status = err.code;
    } else if (err instanceof Error) {
      const errDetails = `Message: ${err.message}| Stack: ${err.stack}`;
      if (this._hideErrorDetails) this._log.error(errDetails);
      response = {
        response: null,
        message: '',
        details: this._hideErrorDetails ? '' : errDetails,
      };
    }

    switch (status) {
      case 400:
        return this.badRequest(response, res);
      case 401:
        return this.unauthorized(response, res);
      case 403:
        return this.forbidden(response, res);
      case 404:
        return this.notFound(response, res);
      case 407:
        return this.authRequired(response, res);
      case 409:
        return this.conflict(response, res);
      default:
        return this.serverError(response, res);
    }
  }

  private badRequest(response: IDtoBaseOutput<{}>, res: Response) {
    this._log.error(response.details);
    res.statusCode = 400;
    res.json(response);
  }

  private unauthorized(response: IDtoBaseOutput<{}>, res: Response) {
    res.statusCode = 401;
    res.json(response);
  }

  private forbidden(response: IDtoBaseOutput<{}>, res: Response) {
    res.statusCode = 403;
    res.json(response);
  }

  private notFound(response: IDtoBaseOutput<{}>, res: Response) {
    res.statusCode = 404;
    res.json(response);
    this._log.error(response.details);
  }

  private authRequired(response: IDtoBaseOutput<{}>, res: Response) {
    res.statusCode = 407;
    res.json(response);
    this._log.error(response.details);
  }

  private conflict(response: IDtoBaseOutput<{}>, res: Response) {
    res.statusCode = 409;
    res.json(response);
    this._log.error(response.details);
  }

  private serverError(response: IDtoBaseOutput<{}>, res: Response) {
    res.statusCode = 500;
    res.json(response);
    this._log.error(response.details);
  }
}
