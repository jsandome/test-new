import { inject, injectable } from 'inversify';
import { BaseMiddleware } from 'inversify-express-utils';
import {
  BaseError,
  BASETYPES,
  BaseUseCaseInfra,
  IBaseLogger,
} from '@capa/core';
import { NextFunction, Request, Response } from 'express';

@injectable()
export class MiddlewareToken extends BaseMiddleware {
  @inject('BaseUseCaseInfra') private ucInfra: BaseUseCaseInfra;
  constructor(@inject(BASETYPES.Log) private logger: IBaseLogger) {
    super();
  }
  public async handler(req: Request, res: Response, next: NextFunction) {
    try {
      const token = req.cookies['capaSession'];
      const userInfo = await this.ucInfra.execValidTokenJwt(
        token,
        req.originalUrl.split('/')[1]
      );
      if (req.body) {
        req.body.userRequest = userInfo.id;
      }
      req.userCapa = userInfo;
      next();
    } catch (err) {
      this.logger.error(`Invalid session.`);
      next(new BaseError(err.message, err.code, err.name, err));
    }
  }
}
