import {
  IAuthLoginIn,
  IAuthRecoveryPassword,
  IAuthRestorePassword,
  IAuthChangePassword,
  IAuthVerifyCode,
  IAuth2FAcodeOut,
  IAuth2FAVerifyTokenIn,
} from '@capa/core';
import { IDtoBaseJsonOut, IDtoBaseOutput } from '../common';

export interface IDtoAuthLoginIn extends IAuthLoginIn {}
export interface IDtoAuthLoginOut extends IDtoBaseJsonOut {}

export interface IDtoAuthLogoutOut extends IDtoBaseJsonOut {}
export interface IAuthRecoveryPasswordIn extends IAuthRecoveryPassword {}
export interface IAuthRecoveryPasswordOut extends IDtoBaseJsonOut {}
export interface IAuthRestorePasswordIn extends IAuthRestorePassword {}
export interface IAuthRestorePasswordOut extends IDtoBaseJsonOut {}
export interface IAuthChangePasswordIn extends IAuthChangePassword {}
export interface IDtoAuthQrcodeOut extends IDtoBaseOutput<IAuth2FAcodeOut> {}
export interface IDtoAuth2FAVerifyTokenIn extends IAuth2FAVerifyTokenIn {}
export interface IAuthChangePasswordOut extends IDtoBaseJsonOut {}
export interface IAuthVerifyCodeIn extends IAuthVerifyCode {}
export interface IAuthVerifyCodeOut extends IDtoBaseJsonOut {}
