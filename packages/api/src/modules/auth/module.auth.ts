import { ContainerModule, interfaces } from 'inversify';
import { controller } from 'inversify-express-utils';

import {
  IBndBaseUserRead,
  IBndBaseTokenInfra,
  IBndBaseUserWrite,
  UseCaseAuth,
  IBndAdminRead,
  IBndInvestorRead,
  IBndMailerInfra,
  IBnd2FAInfra,
} from '@capa/core';
import { ADMIN, COMMON, INVESTOR } from '../../config';
import { AuthController } from '.';
import { ImplBndAdminReadMongo, ImplBndInvestorReadMongo } from '@capa/data';

export const AuthModule = new ContainerModule((bind: interfaces.Bind) => {
  // TODO mover cuando se tenga la implementación de investor y admin
  bind<IBndAdminRead>(ADMIN.bndAdminRead).to(ImplBndAdminReadMongo);
  bind<IBndInvestorRead>(INVESTOR.bndInvestorRead).to(ImplBndInvestorReadMongo);

  bind<UseCaseAuth>('UseCaseAuth').toDynamicValue((context) => {
    return new UseCaseAuth(
      context.container.get<IBndBaseUserRead>(COMMON.bndBaseUserRead),
      context.container.get<IBndBaseUserWrite>(COMMON.bndBaseUserWrite),
      context.container.get<IBndAdminRead>(ADMIN.bndAdminRead),
      context.container.get<IBndInvestorRead>(INVESTOR.bndInvestorRead),
      context.container.get<IBndBaseTokenInfra>(COMMON.bndBaseInfra),
      context.container.get<IBndMailerInfra>(COMMON.BndMailerInfra),
      context.container.get<IBnd2FAInfra>(COMMON.BndBase2FAInfra)
    );
  });
  controller('')(AuthController);
});
