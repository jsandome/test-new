import { inject } from 'inversify';
import {
  BaseHttpController,
  httpDelete,
  httpPatch,
  httpPost,
  request,
  requestBody,
} from 'inversify-express-utils';
import { checkSchema } from 'express-validator';
import { Request } from 'express';

import {
  EnumEnvironments,
  EnumUserTypes,
  IJwtParamsTokenIn,
  UseCaseAuth,
} from '@capa/core';
import {
  IAuthChangePasswordIn,
  IAuthChangePasswordOut,
  IAuthRecoveryPasswordIn,
  IAuthRecoveryPasswordOut,
  IAuthRestorePasswordIn,
  IAuthRestorePasswordOut,
  IAuthVerifyCodeIn,
  IAuthVerifyCodeOut,
  IDtoAuth2FAVerifyTokenIn,
  IDtoAuthLoginIn,
  IDtoAuthLoginOut,
  IDtoAuthLogoutOut,
  IDtoAuthQrcodeOut,
} from '.';
import {
  SchemaLogin,
  schemaRecoveryPass,
  schemaRecoveryPassPatch,
  schemaChangePass,
  SchemaLoginCode,
} from '../common/schemas';
import { COMMON } from '../../config';
import { IDtoBaseJsonOut } from '..';

export class AuthController extends BaseHttpController {
  @inject('UseCaseAuth') private ucAuth: UseCaseAuth;

  constructor() {
    super();
  }

  @httpPost('/admin/login', ...checkSchema(SchemaLogin), COMMON.midParameters)
  public async adminLogin(
    @requestBody() params: IDtoAuthLoginIn
  ): Promise<IDtoAuthLoginOut> {
    params.type = EnumUserTypes.ADMIN;
    const response = await this.ucAuth.execLogin(params);
    let msg = undefined;

    if (!!response['name']) {
      msg = 'Welcome to CAPA!';
      this.httpContext.response.cookie('capaSession', response['userToken'], {
        httpOnly: true,
        sameSite: 'none',
        secure: process.env.NODE_ENV === EnumEnvironments.TEST ? false : true,
      });
      delete response['userToken'];
    }
    return {
      response,
      message: msg,
    };
  }
  @httpPost(
    '/investor/login',
    ...checkSchema(SchemaLogin),
    COMMON.midParameters
  )
  public async investorLogin(
    @requestBody() params: IDtoAuthLoginIn
  ): Promise<IDtoAuthLoginOut> {
    params.type = EnumUserTypes.INVESTOR;
    const response = await this.ucAuth.execLogin(params);
    let msg = undefined;

    if (!!response['name']) {
      msg = 'Welcome to CAPA!';
      this.httpContext.response.cookie('capaSession', response['userToken'], {
        httpOnly: true,
        sameSite: 'none',
        secure: process.env.NODE_ENV === EnumEnvironments.TEST ? false : true,
      });
      delete response['userToken'];
    }
    return {
      response,
      message: msg,
    };
  }
  @httpDelete('/logout', COMMON.midValidToken)
  public async logout(@request() req: Request): Promise<IDtoAuthLogoutOut> {
    await this.ucAuth.execLogout(req.userCapa.id);
    this.httpContext.response.clearCookie('capaSession');

    return {
      response: {},
      message: 'Session has been closed succesfully.',
    };
  }

  @httpPost(
    '/recovery',
    ...checkSchema(schemaRecoveryPass),
    COMMON.midParameters
  )
  public async sendMailRecovery(
    @requestBody() params: IAuthRecoveryPasswordIn
  ): Promise<IAuthRecoveryPasswordOut> {
    await this.ucAuth.execSendMailRecovery(params);

    return {
      response: {},
      message: 'Email successfully sent!',
    };
  }

  @httpPatch(
    '/recovery',
    ...checkSchema(schemaRecoveryPassPatch),
    COMMON.midParameters
  )
  public async restorePassword(
    @requestBody() params: IAuthRestorePasswordIn
  ): Promise<IAuthRestorePasswordOut> {
    await this.ucAuth.execRestorePassword(params);

    return {
      response: {},
      message: 'The new password has been set.',
    };
  }

  @httpPatch(
    '/password',
    ...checkSchema(schemaChangePass),
    COMMON.midParameters,
    COMMON.midValidToken
  )
  public async changePassword(
    @request() req: Request,
    @requestBody() params: IAuthChangePasswordIn
  ): Promise<IAuthChangePasswordOut> {
    await this.ucAuth.execChangePassword(params, req.userCapa.email);

    return {
      response: {},
      message: 'The password has been updated successfully.',
    };
  }

  // TODO Add middleware by rol
  @httpPost(
    '/admin/login/code',
    ...checkSchema(SchemaLoginCode),
    COMMON.midParameters
  )
  public async adminVerifyCode(
    @requestBody() params: IAuthVerifyCodeIn
  ): Promise<IAuthVerifyCodeOut> {
    let message = undefined;
    const response = await this.ucAuth.execVerifyCode(params);

    if (!!response['name']) {
      message = 'Welcome to CAPA';
    }

    return {
      response,
      message,
    };
  }

  @httpPost(
    '/investor/login/code',
    ...checkSchema(SchemaLoginCode),
    COMMON.midParameters
  )
  public async investorVerifyCode(
    @requestBody() params: IAuthVerifyCodeIn
  ): Promise<IAuthVerifyCodeOut> {
    let message = undefined;
    const response = await this.ucAuth.execVerifyCode(params);

    if (!!response['name']) {
      message = 'Welcome to CAPA';
    }

    return {
      response,
      message,
    };
  }

  //TODO new 2FA
  @httpPost('/2fa/code', COMMON.midValidToken)
  public async generateQrcode(
    @request() req: Request
  ): Promise<IDtoAuthQrcodeOut> {
    const params: IJwtParamsTokenIn = req.userCapa;
    const response = await this.ucAuth.execCreateCode(params);
    return {
      response,
    };
  }
  @httpPost('/2fa/verify', COMMON.midValidToken)
  public async verifyCode(
    @request() req: Request,
    @requestBody() params: IDtoAuth2FAVerifyTokenIn
  ): Promise<IDtoBaseJsonOut> {
    (params.userId = req.userCapa.id), await this.ucAuth.execValidCode(params);

    return {
      response: {},
    };
  }
}
