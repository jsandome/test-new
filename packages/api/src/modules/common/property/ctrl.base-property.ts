import { inject } from 'inversify';
import {
  BaseUseCaseProperty,
  IBaseEntityProperty,
  IBaseEntityPropertyFinancialCreate,
  IExcelPropertiesProfitsAndLossResponse,
  IPropertyProjection,
  IPropertyPropertyInfoByExcelParams,
} from '@capa/core';
import {
  BaseHttpController,
  httpGet,
  httpPost,
  requestBody,
  requestParam,
  queryParam,
  httpPut,
  httpDelete,
} from 'inversify-express-utils';
import {
  GetPropertyCtlgs,
  CreateProperty,
  CreatePropertyFinancial,
  GetProperty,
  SelectProperties,
  GetProperties,
  GetPropertyFinancial,
  UpdateProperty,
  DeleteProperties,
  GetPropertyPreviewResponse,
  savePropertyProjectionResponse,
  getPropertiesProfitAndLossPreview,
  savePropertiesFromExcel,
  savePropertiesProfitAndLossPreview,
  getPropertyPreviewOfPropertyUpload,
} from '.';
import { checkSchema } from 'express-validator';
import {
  schemaCreateProperty,
  schemaUpdateProperty,
  schemaCreatePropertyFinancial,
  schemaPropertyProjection,
  schemaPropertyPreviewByExcel,
} from '../schemas';
import { COMMON } from '../../../config/types-common';

export class BasePropertyController extends BaseHttpController {
  @inject('BaseUseCaseProperty') private ucProperty: BaseUseCaseProperty;

  constructor() {
    super();
  }

  @httpGet('/property/types')
  public async getPropertyTypes(): Promise<GetPropertyCtlgs> {
    const response = await this.ucProperty.execGetPropertyTypes();

    return {
      response,
    };
  }

  @httpGet('/property/strategies')
  public async getPropertyStrategies(): Promise<GetPropertyCtlgs> {
    const response = await this.ucProperty.execGetPropertyStrategies();
    return {
      response,
    };
  }

  @httpGet('/property/select', COMMON.midValidToken, COMMON.midParameters)
  public async selectProperties(
    @queryParam('search') search: string,
    @queryParam('searchType') searchType: string
  ): Promise<SelectProperties> {
    const response = await this.ucProperty.execSelectProperties(
      search,
      searchType
    );
    return {
      response,
    };
  }

  @httpGet('/property', COMMON.midValidToken)
  public async getProperties(
    @queryParam('status') status: string,
    @queryParam('type') type: string,
    @queryParam('category') category: string
  ): Promise<GetProperties> {
    const response = await this.ucProperty.execGetProperties({
      category: category
        ? category.split('|').filter((param) => param.length > 0)
        : [],
      status: status
        ? status.split('|').filter((param) => param.length > 0)
        : [],
      type: type ? type.split('|').filter((param) => param.length > 0) : [],
    });
    return {
      response,
    };
  }

  @httpPost(
    '/admin/property',
    ...checkSchema(schemaCreateProperty),
    COMMON.midParameters,
    COMMON.midValidToken
  )
  public async createPropertyBase(
    @requestBody() property: IBaseEntityProperty
  ): Promise<CreateProperty> {
    const response = await this.ucProperty.execCreatePropertyBase(property);
    return {
      response: response,
      message: `Property created!`,
      details: `You can now view the created Property`,
    };
  }

  @httpPut(
    '/admin/property/:propertyId',
    ...checkSchema(schemaUpdateProperty),
    COMMON.midParameters,
    COMMON.midValidToken
  )
  public async updatePropertyBase(
    @requestBody() property: IBaseEntityProperty,
    @requestParam('propertyId') propertyId: string
  ): Promise<UpdateProperty> {
    const response = await this.ucProperty.execUpdatePropertyBase(
      property,
      propertyId
    );
    return {
      response: response,
      message: 'Property has been updated succesfully.',
    };
  }

  @httpPost(
    '/admin/property/:propertyId/financial',
    ...checkSchema(schemaCreatePropertyFinancial),
    COMMON.midParameters,
    COMMON.midValidToken
  )
  public async createPropertyFinancial(
    @requestBody() propertyFinancial: IBaseEntityPropertyFinancialCreate,
    @requestParam('propertyId') propertyId: string
  ): Promise<CreatePropertyFinancial> {
    const response = await this.ucProperty.execCreatePropertyFinancialData(
      propertyFinancial,
      propertyId
    );
    return {
      response,
      message: 'Property created!',
      details: `You can now view the created Property`,
    };
  }

  @httpGet('/admin/property/:propertyId', COMMON.midValidToken)
  public async getProperty(
    @requestParam('propertyId') propertyId: string
  ): Promise<GetProperty> {
    const response = await this.ucProperty.execGetProperty(propertyId);
    return {
      response,
    };
  }

  @httpGet(
    '/admin/property/:propertyId/financial/:financialId',
    COMMON.midValidToken
  )
  public async GetPropertyFinancial(
    @requestParam('propertyId') propertyId: string,
    @requestParam('financialId') financialId: string,
    @queryParam('year') year: string,
    @queryParam('quarter') quarter: string
  ): Promise<GetPropertyFinancial> {
    const response = await this.ucProperty.execGetPropertyFinancial(
      propertyId,
      financialId,
      parseInt(quarter),
      parseInt(year)
    );
    return {
      response,
    };
  }

  @httpDelete('/admin/property/:propertyId', COMMON.midValidToken)
  public async deleteProperties(
    @requestParam('propertyId') propertyId: string
  ): Promise<DeleteProperties> {
    await this.ucProperty.deleteProperties(propertyId);
    return {
      response: null,
      message: 'Property has been deleted succesfully.',
    };
  }

  @httpPost(
    '/admin/property/preview/:idProperty',
    ...checkSchema(schemaPropertyProjection),
    COMMON.midParameters,
    COMMON.midValidToken
  )
  public async getPropertyPreview(
    @requestParam('idProperty') idProperty: string,
    @requestBody() data: IPropertyProjection
  ): Promise<GetPropertyPreviewResponse> {
    const propertyPreview = await this.ucProperty.execGetPropertyPreview(
      data,
      idProperty
    );
    return {
      response: propertyPreview,
    };
  }

  @httpPost(
    '/admin/property/projection/:idProperty',
    ...checkSchema(schemaPropertyProjection),
    COMMON.midParameters,
    COMMON.midValidToken
  )
  public async savePropertyProjection(
    @requestParam('idProperty') idProperty: string,
    @requestBody() data: IPropertyProjection
  ): Promise<savePropertyProjectionResponse> {
    const propertyPreview = await this.ucProperty.execPropertyProjection({
      idProperty,
      projectionInformation: data,
    });
    return {
      response: propertyPreview,
    };
  }

  @httpGet(
    '/admin/properties/preview/excel',
    ...checkSchema(schemaPropertyPreviewByExcel),
    COMMON.midParameters,
    COMMON.midValidToken
  )
  public async getPropertiesBaseByExcel(
    @queryParam('pathFile') pathFile: string
  ): Promise<getPropertyPreviewOfPropertyUpload> {
    const response = await this.ucProperty.execGetPropertyPreviewByExcel({
      pathFile,
    });
    return { response };
  }

  @httpPost(
    '/admin/properties/excel',
    ...checkSchema(schemaPropertyPreviewByExcel),
    COMMON.midParameters,
    COMMON.midValidToken
  )
  public async savePropertiesAndProjectionsByExcel(
    @requestBody() data: IPropertyPropertyInfoByExcelParams
  ): Promise<savePropertiesFromExcel> {
    if (!data.userRequest) {
      data.userRequest = null;
    }
    const response =
      await this.ucProperty.execSavePropertiesAndProjectionsByExcel(data);
    return { response };
  }

  @httpGet(
    '/admin/properties/profits-and-loss/excel',
    ...checkSchema(schemaPropertyPreviewByExcel),
    COMMON.midParameters,
    COMMON.midValidToken
  )
  public async getProfitsAndLossPreviewByExcel(
    @queryParam('pathFile') pathFile: string
  ): Promise<getPropertiesProfitAndLossPreview> {
    const response = await this.ucProperty.execGetPropertyProfitsAndLossPreview(
      { pathFile }
    );
    return { response };
  }

  @httpPost(
    '/admin/properties/profits-and-loss/excel',
    ...checkSchema(schemaPropertyPreviewByExcel),
    COMMON.midParameters,
    COMMON.midValidToken
  )
  public async saveProfitsAndLossPreviewByExcel(
    @requestBody() data: IPropertyPropertyInfoByExcelParams
  ): Promise<savePropertiesProfitAndLossPreview> {
    const response: IExcelPropertiesProfitsAndLossResponse =
      await this.ucProperty.execSavePropertyProfitsAndLossPreview({
        pathFile: data.pathFile,
      });
    return { response };
  }
}
