import {
  IBaseEntityPropertyFinancialCreateResponse,
  IBaseEntityProperty,
  IBaseEntityPropertyResponse,
  IBaseEntityPropertyFinancialResponse,
  IBEGetPropertiesResponse,
  IBaseEntityPropertyPreview,
  IBaseEntityPropertySelect,
  IPropertyCreateProjectionResponse,
  IBaseSheetProjectionData,
  IExcelPropertiesAndProjectionsSaveResponse,
  IExcelPropertiesAndProjections,
  IPropertyProfitsAndLossExcelResponse,
  IExcelPropertiesProfitsAndLossResponse,
} from '@capa/core';
import { IDtoBaseOutput } from '..';

export interface GetPropertyCtlgs extends IDtoBaseOutput<{}> {}
export interface GetProperty extends IDtoBaseOutput<IBaseEntityProperty> {}
export interface SelectProperties
  extends IDtoBaseOutput<IBaseEntityPropertySelect[]> {}
export interface GetProperties
  extends IDtoBaseOutput<IBEGetPropertiesResponse> {}
export interface GetPropertyFinancial
  extends IDtoBaseOutput<IBaseEntityPropertyFinancialResponse> {}

export interface CreateProperty
  extends IDtoBaseOutput<IBaseEntityPropertyResponse> {}
export interface UpdateProperty
  extends IDtoBaseOutput<IBaseEntityPropertyResponse> {}

export interface CreatePropertyFinancial
  extends IDtoBaseOutput<IBaseEntityPropertyFinancialCreateResponse> {}

export interface DeleteProperties extends IDtoBaseOutput<string> {}

export interface GetPropertyPreview
  extends IDtoBaseOutput<IBaseEntityPropertyPreview> {}

export interface GetPropertyPreviewResponse
  extends IDtoBaseOutput<IBaseEntityPropertyPreview> {}
export interface savePropertyProjectionResponse
  extends IDtoBaseOutput<IPropertyCreateProjectionResponse> {}

export interface getPropertyPreviewOfPropertyUpload
  extends IDtoBaseOutput<IExcelPropertiesAndProjections> {}
export interface savePropertiesFromExcel
  extends IDtoBaseOutput<IExcelPropertiesAndProjectionsSaveResponse> {}
export interface getPropertyProjectionExcelPreview
  extends IDtoBaseOutput<IBaseSheetProjectionData[]> {}

export interface getPropertiesProfitAndLossPreview
  extends IDtoBaseOutput<IPropertyProfitsAndLossExcelResponse> {}

export interface savePropertiesProfitAndLossPreview
  extends IDtoBaseOutput<IExcelPropertiesProfitsAndLossResponse> {}
