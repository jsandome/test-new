import { IDtoBaseOutput } from '..';
import {
  IBaseEntityContact,
  IBndResponseCreateContact,
  IBndReturnListContacts,
  IBndSelectContacts,
} from '@capa/core';

export interface IDResponseCreateContact
  extends IDtoBaseOutput<IBndResponseCreateContact> {}
export interface IDResponseGetContact
  extends IDtoBaseOutput<IBaseEntityContact> {}
export interface IDResponseListContacts
  extends IDtoBaseOutput<IBndReturnListContacts> {}
export interface IDResponseSelectContacts
  extends IDtoBaseOutput<IBndSelectContacts[]> {}
export interface IDResponseUpdateContact {}
export interface IDResponseDeleteContact {}
