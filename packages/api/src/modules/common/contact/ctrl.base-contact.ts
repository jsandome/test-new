import { inject } from 'inversify';
import {
  BaseHttpController,
  httpPost,
  httpGet,
  requestBody,
  requestParam,
  queryParam,
  httpPut,
  httpDelete,
} from 'inversify-express-utils';
import {
  BaseUseCaseContact,
  IPaginateParamsContacts,
  IParamsCreateContact,
  IParamsUpdateContact,
} from '@capa/core';
import { COMMON } from '../../../config/types-common';
import { SchemaCreateContact, SchemaUpdateContact } from '../schemas';
import { checkSchema } from 'express-validator';
import {
  IDResponseCreateContact,
  IDResponseDeleteContact,
  IDResponseGetContact,
  IDResponseListContacts,
  IDResponseSelectContacts,
  IDResponseUpdateContact,
} from '.';

export class BaseContactController extends BaseHttpController {
  @inject('BaseUseCaseContact') private ucContact: BaseUseCaseContact;

  constructor() {
    super();
  }

  @httpPost(
    '/admin/contact',
    ...checkSchema(SchemaCreateContact),
    COMMON.midParameters,
    COMMON.midValidToken
  )
  public async createContact(
    @requestBody() params: IParamsCreateContact
  ): Promise<IDResponseCreateContact> {
    const response = await this.ucContact.execCreateContact(params);
    return {
      response,
      message: 'Contact has been created succesfully.',
    };
  }

  @httpGet('/contact/select', COMMON.midValidToken)
  public async selectContacts(
    @queryParam('search') search: string
  ): Promise<IDResponseSelectContacts> {
    const response = await this.ucContact.execSelectContacts(search);
    return {
      response,
    };
  }

  @httpGet('/admin/contact/:contactId', COMMON.midValidToken)
  public async getContact(
    @requestParam('contactId') contactId: string
  ): Promise<IDResponseGetContact> {
    const response = await this.ucContact.execGetContact(contactId);
    return { response };
  }

  @httpGet('/contact/list', COMMON.midValidToken)
  public async listContacts(
    @queryParam('limit') limit?: number,
    @queryParam('page') page?: number
  ): Promise<IDResponseListContacts> {
    const pagination: IPaginateParamsContacts = {
      limit,
      page,
    };
    const response = await this.ucContact.execListContacts(pagination);
    return { response };
  }

  @httpPut(
    '/admin/contact/:contactId',
    ...checkSchema(SchemaUpdateContact),
    COMMON.midParameters,
    COMMON.midValidToken
  )
  public async updateContact(
    @requestParam('contactId') contactId: string,
    @requestBody() params: IParamsUpdateContact
  ): Promise<IDResponseUpdateContact> {
    await this.ucContact.execUpdateContact(contactId, params);
    return {
      response: {},
      message: 'Contact has been updated succesfully.',
    };
  }

  @httpDelete('/admin/contact/:contactId', COMMON.midValidToken)
  public async deleteContact(
    @requestParam('contactId') contactId: string
  ): Promise<IDResponseDeleteContact> {
    await this.ucContact.execDeleteContact(contactId);
    return {
      response: {},
      message: 'Contact has been deleted succesfully.',
    };
  }
}
