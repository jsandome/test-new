export interface IDtoBaseOutput<T> {
  response: T;
  message?: string;
  details?: string;
}

export interface IDtoBaseJsonOut extends IDtoBaseOutput<{}> {}
export interface IDtoBaseJsonArrayOut extends IDtoBaseOutput<{}[]> {}
