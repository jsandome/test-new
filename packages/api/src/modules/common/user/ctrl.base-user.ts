import { inject } from 'inversify';
import {
  BaseHttpController,
  httpGet,
  requestParam,
} from 'inversify-express-utils';
import { BaseUseCaseUser } from '@capa/core';
import { IDtoGetUserByIdOut } from '.';
export class BaseUserController extends BaseHttpController {
  @inject('BaseUseCaseUser') private ucUser: BaseUseCaseUser;

  constructor() {
    super();
  }

  @httpGet('/user/:userId')
  public async getUserById(
    @requestParam('userId') id: string
  ): Promise<IDtoGetUserByIdOut> {
    const response = await this.ucUser.execGetUserById(id);
    return {
      response,
    };
  }
}
