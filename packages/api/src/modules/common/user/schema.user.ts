

import { ParamSchema } from 'express-validator';
import { EnumUserTypes } from '@capa/core';
import * as regExp from '../ReExp';
export const schemaCreateUser: Record<string, ParamSchema> = {
  email: {
    in: ['body'],
    isEmail: true,
    errorMessage: 'Email is wrong',
    isLength: {
      errorMessage: 'Email cannot have more than 25 characters',
      options: { max: 25 },
    },
  },
  password: {
    in: ['body'],
    notEmpty: true,
    errorMessage: 'Password is invalid',
    matches: {
      options: [regExp.weakPassword],
      errorMessage: 'Invalid Password',
    },
    isLength: {
      errorMessage: 'Password cannot have more than 25 characters',
      options: { max: 25 },
    },
  },
  type: {
    in: ['body'],
    custom: {
      errorMessage: `Type no valid, valid types: ${Object.values(EnumUserTypes).toString()}`,
      options: (value) => {
        return Object.values(EnumUserTypes).includes(value);
      },
    },
  }
};
