import { IBaseEntityUser } from '@capa/core';
import { IDtoBaseOutput } from '..';

export interface IDtoGetUserByIdOut extends IDtoBaseOutput<IBaseEntityUser> {}
export interface IDtoCreateNewUser extends IDtoBaseOutput<IBaseEntityUser> {}
export interface IDtoLogin {
    
}

