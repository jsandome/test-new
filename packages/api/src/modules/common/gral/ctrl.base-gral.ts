import { BaseHttpController, httpGet } from 'inversify-express-utils';

import { IDtoGralEndpoint } from './';

export class BaseGralController extends BaseHttpController {
  constructor() {
    super();
  }

  @httpGet('/ping')
  public async getPingInformation(): Promise<IDtoGralEndpoint> {
    return {
      response: null,
      message: 'pong',
    };
  }
}
