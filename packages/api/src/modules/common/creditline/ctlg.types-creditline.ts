import { IExtraPayments, IPropertyCreditlineShare } from '@capa/core';

export interface IParamsCreditline {
  name: string;
  principal: number;
  interest: number;
  extraPayments: IExtraPayments[];
  properties: IPropertyCreditlineShare;
}
