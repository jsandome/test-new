import { IDtoBaseOutput } from '..';
import {
  IBndReturnCreate,
  IBaseEntityCreditline,
  IBndReturnPreviewCreditline,
} from '@capa/core';

export interface IDResponseGetCreditlines
  extends IDtoBaseOutput<IBaseEntityCreditline[]> {}
export interface IDResponseCreateCreditline
  extends IDtoBaseOutput<IBndReturnCreate> {}
export interface IDResponsePreviewCreditline
  extends IDtoBaseOutput<IBndReturnPreviewCreditline> {}
export interface IDResponseUpdateCreditline {}
export interface IDResponseDeleteCreditline {}
