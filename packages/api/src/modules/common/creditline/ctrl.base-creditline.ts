import { inject } from 'inversify';
import {
  BaseHttpController,
  httpGet,
  httpPost,
  httpDelete,
  httpPut,
  queryParam,
  requestBody,
  requestParam,
} from 'inversify-express-utils';
import {
  BaseUseCaseCreditline,
  IParamsCreateCreditline,
  IParamsPreviewCreditline,
  IParamsUpdateCreditline,
} from '@capa/core';
import {
  IDResponseGetCreditlines,
  IDResponseCreateCreditline,
  IDResponsePreviewCreditline,
  IDResponseUpdateCreditline,
  IDResponseDeleteCreditline,
} from '.';
import { COMMON } from '../../../config/types-common';
import {
  SchemaCreateCreditline,
  SchemaPreviewCreditline,
  SchemaUpdateCreditline,
} from '../schemas';
import { checkSchema } from 'express-validator';

export class BaseCreditLineController extends BaseHttpController {
  @inject('BaseUseCaseCreditline') private ucCreditline: BaseUseCaseCreditline;

  constructor() {
    super();
  }

  @httpPost(
    '/admin/credit-line',
    ...checkSchema(SchemaCreateCreditline),
    COMMON.midParameters,
    COMMON.midValidToken
  )
  public async createCreditline(
    @requestBody() params: IParamsCreateCreditline
  ): Promise<IDResponseCreateCreditline> {
    const response = await this.ucCreditline.execCreateCreditline(params);

    return {
      response,
      message: 'Credit line created!',
      details: `You can now view the created Credit Line`,
    };
  }

  @httpPost(
    '/admin/credit-line/preview',
    ...checkSchema(SchemaPreviewCreditline),
    COMMON.midParameters,
    COMMON.midValidToken
  )
  public async previewCreditline(
    @requestBody() params: IParamsPreviewCreditline
  ): Promise<IDResponsePreviewCreditline> {
    const response = await this.ucCreditline.execPreviewCreditline(params);

    return {
      response,
    };
  }

  @httpGet('/credit-line/select', COMMON.midValidToken)
  public async selectCreditline(
    @queryParam('search') search: string
  ): Promise<IDResponseGetCreditlines> {
    const response = await this.ucCreditline.execSelectCreditline(search);

    return {
      response,
    };
  }

  @httpPut(
    '/admin/credit-line/:creditline',
    ...checkSchema(SchemaUpdateCreditline),
    COMMON.midParameters,
    COMMON.midValidToken
  )
  public async updateCreditline(
    @requestParam('creditline') creditlineId: string,
    @requestBody() params: IParamsUpdateCreditline
  ): Promise<IDResponseUpdateCreditline> {
    const response = await this.ucCreditline.execUpdateCreditline(
      creditlineId,
      params
    );

    return {
      response,
    };
  }

  @httpDelete('/admin/credit-line/:creditline', COMMON.midValidToken)
  public async deleteCreditline(
    @requestParam('creditline') creditlineId: string
  ): Promise<IDResponseDeleteCreditline> {
    const response = await this.ucCreditline.execDeleteCreditline(creditlineId);

    return {
      response,
      message: 'Credit line has been deleted succesfully.',
    };
  }
}
