import { ParamSchema } from 'express-validator';
import { isEmpty, isNil } from 'lodash';
import { EnumTypeFormOnboarding } from '@capa/core';
import { Request } from 'express-validator/src/base';
import { DateTime } from 'luxon';

const genericSameMailingAddress = (req: Request) => {
  if(req.body.sameMailingAddress) {
    return false;
  }
  return true;
};


export const SchemaCreteOnboarding: Record<string, ParamSchema> = {
  email: {
    in: 'body',
    isEmail: true,
    errorMessage: 'Email is wrong',
    isLength: {
      errorMessage:
        'Email cannot have less than 3 characters.',
      options: { min: 3, max: 150 },
    },
    notEmpty: true,
  },
  nameContact: {
    in: 'body',
    isString: true,
    optional: true,
    isLength: {
      options: { min: 3, max: 150 },
      errorMessage: 'Name of the contact must be greater than 3 characters and less than 15.',
    },
  },
  propertyId: {
    in: 'body',
    isMongoId: true,
    optional: true,
    errorMessage: 'The field Property Id must be a valid MongoDB ID',
  },
  fundId: {
    in: 'body',
    isMongoId: true,
    optional: true,
    errorMessage: 'The field Fund Id must be a valid MongoDB ID',
  },
  packageId: {
    in: 'body',
    isMongoId: true,
    optional: true,
    errorMessage: 'The field Package Id be a valid MongoDB ID',
  },
  templateId: {
    in: 'body',
    isString: true,
    optional: true,
    isLength: {
      options: { min: 3, max: 36 },
      errorMessage: 'The field Template Id is not correct',
    },
  },
  packageDocumentId: {
    in: 'body',
    isMongoId: true,
    optional: true,
    errorMessage: 'The field PackageDocument Id must be a valid MongoDB ID',
  },
};

export const SchemaStepOneOnboarding: Record<string, ParamSchema> = {
  investmentAmount: {
    in: 'body',
    errorMessage: 'The Investment Amount is required.',
    isNumeric: {
      errorMessage: 'The Investment Amount must be a number.',
    },
  },
	mailingAddres: {
    in: 'body',
    isObject: {
      errorMessage: 'Mailing Addres must be an object.',
    },
	},
  'mailingAddres.streetLineOne': {
    in: 'body',
    isString: true,
    isLength: {
      options: { min: 3, max: 150 },
      errorMessage: 'Street Line One must be greater than 3 characters and less than 150.',
    },
	},
  'mailingAddres.streetLineTwo': {
    in: 'body',
    isString: true,
    isLength: {
      options: { min: 3, max: 150 },
      errorMessage: 'Street Line Two must be greater than 3 characters and less than 150.',
    },
	},
  'mailingAddres.city': {
    in: 'body',
    isString: true,
    isLength: {
      options: { min: 3, max: 150 },
      errorMessage: 'City must be greater than 3 characters and less than 150.',
    },
	},
  'mailingAddres.state': {
    in: 'body',
    isString: true,
    isLength: {
      options: { min: 3, max: 150 },
      errorMessage: 'State must be greater than 3 characters and less than 150.',
    },
	},
  'mailingAddres.zip': {
    in: 'body',
    isString: true,
    isLength: {
      options: { min: 3, max: 32 },
      errorMessage: 'Zip must be greater than 3 characters and less than 32.',
    },
	},
  'mailingAddres.country': {
    in: 'body',
    isString: true,
    isLength: {
      options: { min: 3, max: 150 },
      errorMessage: 'Country must be greater than 3 characters and less than 150.',
    },
	},
  sameMailingAddress: {
    in: 'body',
    isBoolean: true,
    custom: {
      errorMessage: 'Domicile Address is required.',
      options: (value, { req }) => {
        if(!value) {
          if (!isEmpty(req.body.domicileAddress)){
            return !isEmpty(req.body.domicileAddress.streetLineOne) &&
            !isEmpty(req.body.domicileAddress.city) && !isEmpty(req.body.domicileAddress.state) 
            && !isEmpty(req.body.domicileAddress.zip) && !isEmpty(req.body.domicileAddress.country);
          }
          return false;
        }
        return true;
      },
    },
	},
  domicileAddress: {
    in: 'body',
    optional: true,
    isObject: {
      errorMessage: 'Domicile Addres must be an object.',
    },
    custom: {
      errorMessage: 'Domicile Address is not required.',
      options: (__, { req }) => genericSameMailingAddress(req),
    },
	},
  'domicileAddress.streetLineOne': {
    in: 'body',
    isString: true,
    optional: true,
    isLength: {
      options: { min: 3, max: 150 },
      errorMessage: 'Street Line One must be greater than 3 characters and less than 150.',
    },
	},
  'domicileAddress.streetLineTwo': {
    in: 'body',
    optional: true,
    isString: true,
    isLength: {
      options: { min: 3, max: 150 },
      errorMessage: 'Street Line Two must be greater than 3 characters and less than 150.',
    },
	},
  'domicileAddress.city': {
    in: 'body',
    isString: true,    
    optional: true,
    isLength: {
      options: { min: 3, max: 150 },
      errorMessage: 'City must be greater than 3 characters and less than 150.',
    },
	},
  'domicileAddress.state': {
    in: 'body',
    optional: true,
    isString: true,
    isLength: {
      options: { min: 3, max: 150 },
      errorMessage: 'State must be greater than 3 characters and less than 150.',
    },
	},
  'domicileAddress.zip': {
    in: 'body',
    optional: true,
    isString: true,
    isLength: {
      options: { min: 3, max: 32 },
      errorMessage: 'Zip must be greater than 3 characters and less than 32.',
    },
	},
  'domicileAddress.country': {
    in: 'body',
    optional: true,
    isString: true,
    isLength: {
      options: { min: 3, max: 150 },
      errorMessage: 'Country must be greater than 3 characters and less than 150.',
    },
	},
  typeFormOnboarding: {
    in: 'body',
    custom: {
      errorMessage: 'Type Onboarding is not valid.',
      options: (value, { req }) => {
        const firstVal = Object.values(EnumTypeFormOnboarding).includes(value);
        if(firstVal) {
          return !isEmpty(req.body.individualIdentity) || !isEmpty(req.body.jointTenancy) ||
          !isEmpty(req.body.trust) || !isEmpty(req.body.llc) || 
          !isEmpty(req.body.otherJoint);
        }
        return false;
      },
    },
  },
  individualIdentity: {
    in: 'body',
    optional: true,
    isObject: {
      errorMessage: 'Individual Identity must be an object.',
    },
    custom: {
      errorMessage: 'Individual Identity is not valid.',
      options: (__, { req }) => {
        if(req.body.typeFormOnboarding === EnumTypeFormOnboarding.INDIVIDUAL) {
          return !isEmpty(req.body.individualIdentity.firstName) && !isEmpty(req.body.individualIdentity.middleName)
          && !isEmpty(req.body.individualIdentity.lastName) && !isEmpty(req.body.individualIdentity.phoneNumber) &&
          !isEmpty(req.body.individualIdentity.email) && !isEmpty(req.body.individualIdentity.employer)
          && !isEmpty(req.body.individualIdentity.jobTitle) && !isNil(req.body.individualIdentity.entityTaxPurpose);
        }
        return false;
      },
    },
	},
  'individualIdentity.firstName': {
    in: 'body',
    isString: true,    
    optional: true,
    isLength: {
      options: { min: 3, max: 150 },
      errorMessage: 'Firstname must be greater than 3 characters and less than 150.',
    },
	},
  'individualIdentity.middleName': {
    in: 'body',
    isString: true,    
    optional: true,
    isLength: {
      options: { min: 3, max: 150 },
      errorMessage: 'Middlename must be greater than 3 characters and less than 150.',
    },
	},
  'individualIdentity.lastName': {
    in: 'body',
    isString: true,    
    optional: true,
    isLength: {
      options: { min: 3, max: 150 },
      errorMessage: 'Lastname must be greater than 3 characters and less than 150.',
    },
	},
  'individualIdentity.phoneNumber': {
    in: 'body',
    isString: true,    
    optional: true,
    isLength: {
      options: { min: 3, max: 150 },
      errorMessage: 'Phone Number must be greater than 3 characters and less than 150.',
    },
	},
  'individualIdentity.email': {
    in: 'body',
    isString: true,    
    optional: true,
    isLength: {
      options: { min: 3, max: 150 },
      errorMessage: 'E-mail must be greater than 3 characters and less than 150.',
    },
	},
  'individualIdentity.country': {
    in: 'body',
    isString: true,    
    optional: true,
    isLength: {
      options: { min: 3, max: 150 },
      errorMessage: 'Country must be greater than 3 characters and less than 150.',
    },
	},
  'individualIdentity.dateBirthday': {
    in: 'body',
    isISO8601: true, 
    optional: true,
    errorMessage: 'The dateBirthday doesn´t have Date format',
    custom: {
      errorMessage: 'Need to be of legal age.',
      options: (value) => {
        const brith = DateTime.fromISO(value);
        return brith.diffNow('years').years < -21;
      },
    },
	},
  'individualIdentity.taxID': {
    in: 'body',
    isString: true,
    optional: true,
    errorMessage: 'The TaxID is not valid.',
    isLength: {
      options: { min: 3, max: 150 },
      errorMessage: 'TaxID must be greater than 3 characters and less than 150.',
    },
  },
  'individualIdentity.employer': {
    in: 'body',
    isString: true,    
    optional: true,
    isLength: {
      options: { min: 3, max: 150 },
      errorMessage: 'Employer must be greater than 3 characters and less than 150.',
    },
	},
  'individualIdentity.jobTitle': {
    in: 'body',
    isString: true,    
    optional: true,
    isLength: {
      options: { min: 3, max: 150 },
      errorMessage: 'Job Title must be greater than 3 characters and less than 150.',
    },
	},
  'individualIdentity.entityTaxPurpose': {
    in: 'body',
    isBoolean: true,
    optional: true,
	},
  jointTenancy: {
    in: 'body',
    optional: true,
    isObject: {
      errorMessage: 'Joint Tenancy must be an object.',
    },
    custom: {
      errorMessage: 'Joint Tenancy is not valid.',
      options: (__, { req }) => {
        if(req.body.typeFormOnboarding === EnumTypeFormOnboarding.JOINTTENACY) {
          if(req.body.jointTenancy.entityTaxPurpose){
            return !isEmpty(req.body.jointTenancy.subscriberLegalName) && !isEmpty(req.body.jointTenancy.relationshipOwners)
            && !isEmpty(req.body.jointTenancy.ultimateBeneficial) && !isEmpty(req.body.jointTenancy.ultimateBeneficialTaxId)
            && !isEmpty(req.body.jointTenancy.owners);
          }
          return !isEmpty(req.body.jointTenancy.subscriberLegalName) && !isEmpty(req.body.jointTenancy.relationshipOwners)
          && !isNil(req.body.jointTenancy.entityTaxPurpose) && !isEmpty(req.body.jointTenancy.owners);
        }
        return false;
      },
    },
	},
  'jointTenancy.subscriberLegalName': {
    in: 'body',
    isString: true,
    optional: true,
    isLength: {
      options: { min: 3, max: 150 },
      errorMessage: 'Subscriber Legal Name must be greater than 3 characters and less than 150.',
    },
	},
  'jointTenancy.relationshipOwners': {
    in: 'body',
    isString: true,
    optional: true,
    isLength: {
      options: { min: 3, max: 150 },
      errorMessage: 'Relationship Owners must be greater than 3 characters and less than 150.',
    },
	},
  'jointTenancy.entityTaxPurpose': {
    in: 'body',
    isBoolean: true,
    optional: true,
	},
  'jointTenancy.ultimateBeneficial': {
    in: 'body',
    isString: true,
    optional: true,    
    custom: {
      errorMessage: 'Ultimate Beneficial is not required.',
      options: (__, { req }) => req.body.jointTenancy.entityTaxPurpose,
    },
    isLength: {
      options: { min: 3, max: 150 },
      errorMessage: 'Ultimate Beneficial must be greater than 3 characters and less than 150.',
    },
	},
  'jointTenancy.ultimateBeneficialTaxId': {
    in: 'body',
    isString: true,
    optional: true,
    custom: {
      errorMessage: 'Ultimate Beneficial TaxId is not required.',
      options: (__, { req }) => req.body.jointTenancy.entityTaxPurpose,
    },
    isLength: {
      options: { min: 3, max: 150 },
      errorMessage: 'Ultimate Beneficial TaxId must be greater than 3 characters and less than 150.',
    },
	},
  'jointTenancy.owners': {
    in: 'body',
    isArray: {
      errorMessage: 'The field owners must be an array.',
    },
    optional: true,
    custom: {
      errorMessage: 'Joint Tenancy Owners is invalid',
      options: (value) => {
        if(value.length === 0) return false;
        for (let index = 0; index < value.length; index++) {
          const element = value[index];
          if(isEmpty(element.firstName) || isEmpty(element.phoneNumber) ||
          isEmpty(element.middleName) || isEmpty(element.lastName) || 
          isEmpty(element.email) || isEmpty(element.country) || isNil(element.usPerson)) {
            return false;
          }
        }
        return true;
      },
    },
	},
  ['jointTenancy.owners.*.firstName']: {
    in: 'body',
    isString: true,
    optional: true,
    errorMessage: 'The First Name is not valid.',
    isLength: {
      options: { min: 3, max: 150 },
      errorMessage: 'First Name must be greater than 3 characters and less than 150.',
    },
  },
  ['jointTenancy.owners.*.middleName']: {
    in: 'body',
    isString: true,
    optional: true,
    errorMessage: 'The Middle Name is not valid.',
    isLength: {
      options: { min: 3, max: 150 },
      errorMessage: 'Relationship Owners must be greater than 3 characters and less than 150.',
    },
  },
  ['jointTenancy.owners.*.lastName']: {
    in: 'body',
    isString: true,
    optional: true,
    errorMessage: 'The Last Name is not valid.',
    isLength: {
      options: { min: 3, max: 150 },
      errorMessage: 'Relationship Owners must be greater than 3 characters and less than 150.',
    },
  },
  ['jointTenancy.owners.*.phoneNumber']: {
    in: 'body',
    isString: true,
    optional: true,
    errorMessage: 'The Phone Number is not valid.',
    isLength: {
      options: { min: 3, max: 150 },
      errorMessage: 'Relationship Owners must be greater than 3 characters and less than 150.',
    },
  },
  ['jointTenancy.owners.*.email']: {
    in: 'body',
    isString: true,
    optional: true,
    errorMessage: 'The E-mail is not valid.',
    isLength: {
      options: { min: 3, max: 150 },
      errorMessage: 'Relationship Owners must be greater than 3 characters and less than 150.',
    },
  },
  ['jointTenancy.owners.*.country']: {
    in: 'body',
    isString: true,
    optional: true,
    errorMessage: 'The Country is not valid.',
    isLength: {
      options: { min: 3, max: 150 },
      errorMessage: 'Relationship Owners must be greater than 3 characters and less than 150.',
    },
  },
  ['jointTenancy.owners.*.dateBirthday']: {
    in: 'body',
    isISO8601: true, 
    optional: true,
    errorMessage: 'The dateBirthday doesn´t have Date  format',
  },
  ['jointTenancy.owners.*.taxID']: {
    in: 'body',
    isString: true,
    optional: true,
    errorMessage: 'The TaxID is not valid.',
    isLength: {
      options: { min: 3, max: 150 },
      errorMessage: 'Relationship Owners must be greater than 3 characters and less than 150.',
    },
  },
  ['jointTenancy.owners.*.employer']: {
    in: 'body',
    isString: true,
    optional: true,
    errorMessage: 'The Employer is not valid.',
    isLength: {
      options: { min: 3, max: 150 },
      errorMessage: 'The Employer must be greater than 3 characters and less than 150.',
    },
  },
  ['jointTenancy.owners.*.jobTitle']: {
    in: 'body',
    isString: true,
    optional: true,
    errorMessage: 'The Job Title is not valid.',
    isLength: {
      options: { min: 3, max: 150 },
      errorMessage: 'The Job Title must be greater than 3 characters and less than 150.',
    },
  },
  ['jointTenancy.owners.*.usPerson']: {
    in: 'body',
    isBoolean: true,
    optional: true,
  },
  trust: {
    in: 'body',
    optional: true,
    isObject: {
      errorMessage: 'Trust must be an object.',
    },
    custom: {
      errorMessage: 'Trust is not valid.',
      options: (value, { req }) => {
        if(req.body.typeFormOnboarding === EnumTypeFormOnboarding.TRUST) {
          if(value.entityTaxPurpose) {
            return !isEmpty(value.subscriberLegalName) && !isEmpty(value.taxID)
            && !isEmpty(value.ultimateBeneficial) && !isEmpty(value.ultimateBeneficialOwnerTaxId)
            && !isNil(value.grantorTrust) && !isNil(value.authorized)
            && !isEmpty(value.owners);
          }
          return !isEmpty(value.subscriberLegalName) && !isEmpty(value.taxID)
          && !isNil(value.entityTaxPurpose) && !isNil(value.grantorTrust)
          && !isNil(value.authorized) && !isEmpty(value.owners);
        }
        return false;
      },
    },
  },
  'trust.subscriberLegalName': {
    in: 'body',
    isString: true,
    optional: true,
    isLength: {
      options: { min: 3, max: 150 },
      errorMessage: 'Subscriber Legal Name must be greater than 3 characters and less than 150.',
    },
	},
  'trust.taxID': {
    in: 'body',
    isString: true,
    optional: true,
    isLength: {
      options: { min: 3, max: 150 },
      errorMessage: 'TaxID must be greater than 3 characters and less than 150.',
    },
	},
  'trust.entityTaxPurpose': {
    in: 'body',
    isBoolean: true,
    optional: true,
	},
  'trust.ultimateBeneficial': {
    in: 'body',
    isString: true,
    optional: true,    
    custom: {
      errorMessage: 'Ultimate Beneficial is not required.',
      options: (__, { req }) => req.body.trust.entityTaxPurpose,
    },
    isLength: {
      options: { min: 3, max: 150 },
      errorMessage: 'Ultimate Beneficial must be greater than 3 characters and less than 150.',
    },
	},
  'trust.ultimateBeneficialOwnerTaxId': {
    in: 'body',
    isString: true,
    optional: true,
    custom: {
      errorMessage: 'Ultimate Beneficial Owner TaxId is not required.',
      options: (__, { req }) => req.body.trust.entityTaxPurpose,
    },
    isLength: {
      options: { min: 3, max: 150 },
      errorMessage: 'Ultimate Beneficial TaxId must be greater than 3 characters and less than 150.',
    },
	},
  'trust.grantorTrust': {
    in: 'body',
    isBoolean: true,
    optional: true,
	},
  'trust.authorized': {
    in: 'body',
    isBoolean: true,
    optional: true,
	},
  'trust.owners': {
    in: 'body',
    isArray: {
      errorMessage: 'The field owners must be an array.',
    },
    optional: true,
    custom: {
      errorMessage: 'Trust Owners is invalid',
      options: (value) => {
        if(value.length === 0) return false;
        for (let index = 0; index < value.length; index++) {
          const element = value[index];
          if(isNil(element.naturalPerson)) {
            return false;
          }
          if(element.naturalPerson) {
            if(isEmpty(element.person) || isEmpty(element.person.firstName) ||
            isEmpty(element.person.middleName) || isEmpty(element.person.lastName) ||
            isEmpty(element.person.phoneNumber) || isEmpty(element.person.email)
            || isEmpty(element.person.title) || !isEmpty(element.entity)) {
              return false;
            }
          } else {
            if(isEmpty(element.entity) || isEmpty(element.entity.entityName) ||
            isEmpty(element.entity.entityTitle) || isNil(element.entity.basicSignatureBlock)
            || !isEmpty(element.person)) {
              return false;
            }
          }
        }
        return true;
      },
    },
	},
  ['trust.owners.*.naturalPerson']: {
    in: 'body',
    isBoolean: true,
    optional: true,
  },
  ['trust.owners.*.person']: {
    in: 'body',
    optional: true,
    isObject: {
      errorMessage: 'Person must be an object.',
    },
	},
  ['trust.owners.*.person.firstName']: {
    in: 'body',
    isString: true,
    optional: true,
    errorMessage: 'The First Name is not valid.',
    isLength: {
      options: { min: 3, max: 150 },
      errorMessage: 'First Name must be greater than 3 characters and less than 150.',
    },
	},
  ['trust.owners.*.person.middleName']: {
    in: 'body',
    isString: true,
    optional: true,
    errorMessage: 'The Middle Name is not valid.',
    isLength: {
      options: { min: 3, max: 150 },
      errorMessage: 'Middle Name must be greater than 3 characters and less than 150.',
    },
	},
  ['trust.owners.*.person.lastName']: {
    in: 'body',
    isString: true,
    optional: true,
    errorMessage: 'The Last Name is not valid.',
    isLength: {
      options: { min: 3, max: 150 },
      errorMessage: 'Last Name must be greater than 3 characters and less than 150.',
    },
	},
  ['trust.owners.*.person.phoneNumber']: {
    in: 'body',
    isString: true,
    optional: true,
    errorMessage: 'The Phone Number is not valid.',
    isLength: {
      options: { min: 3, max: 150 },
      errorMessage: 'Phone Number must be greater than 3 characters and less than 150.',
    },
	},
  ['trust.owners.*.person.email']: {
    in: 'body',
    isString: true,
    optional: true,
    errorMessage: 'The E-mail is not valid.',
    isLength: {
      options: { min: 3, max: 150 },
      errorMessage: 'E-mail must be greater than 3 characters and less than 150.',
    },
	},
  ['trust.owners.*.person.title']: {
    in: 'body',
    isString: true,
    optional: true,
    errorMessage: 'The Title is not valid.',
    isLength: {
      options: { min: 3, max: 150 },
      errorMessage: 'Title must be greater than 3 characters and less than 150.',
    },
	},
  ['trust.owners.*.person.dateBirthday']: {
    in: 'body',
    isISO8601: true, 
    optional: true,
    errorMessage: 'The dateBirthday doesn´t have Date  format',
    custom: {
      errorMessage: 'Need to be of legal age.',
      options: (value) => {
        const brith = DateTime.fromISO(value);
        return brith.diffNow('years').years < -21;
      },
    },
	},
  ['trust.owners.*.person.taxID']: {
    in: 'body',
    isString: true,
    optional: true,
    errorMessage: 'The TaxID is not valid.',
    isLength: {
      options: { min: 3, max: 150 },
      errorMessage: 'TaxID must be greater than 3 characters and less than 150.',
    },
	},
  ['trust.owners.*.entity']: {
    in: 'body',
    optional: true,
    isObject: {
      errorMessage: 'Person must be an object.',
    },
	},
  ['trust.owners.*.entity.entityName']: {
    in: 'body',
    isString: true,
    optional: true,
    errorMessage: 'Entity Name is not valid.',
    isLength: {
      options: { min: 3, max: 150 },
      errorMessage: 'Entity Name must be greater than 3 characters and less than 150.',
    },
	},
  ['trust.owners.*.entity.entityTitle']: {
    in: 'body',
    isString: true,
    optional: true,
    errorMessage: 'The Entity Title is not valid.',
    isLength: {
      options: { min: 3, max: 150 },
      errorMessage: 'Entity Title must be greater than 3 characters and less than 150.',
    },
	},
  ['trust.owners.*.entity.basicSignatureBlock']: {
    in: 'body',
    isBoolean: true,
    optional: true,
	},
  
  llc: {
    in: 'body',
    optional: true,
    isObject: {
      errorMessage: 'LLC must be an object.',
    },
    custom: {
      errorMessage: 'LLC is not valid.',
      options: (value, { req }) => {
        if(req.body.typeFormOnboarding === EnumTypeFormOnboarding.LLC) {
          if(isNil(value.usPerson)) return false;
          if(value.usPerson) {
            return !isEmpty(value.subscriberLegalName) && !isEmpty(value.taxID)
            && !isEmpty(value.ultimateBeneficial) && !isEmpty(value.ultimateBeneficialOwnerTaxId)
            && !isEmpty(value.country) && !isEmpty(value.owners);
          } else {
            return !isEmpty(value.subscriberLegalName) && !isEmpty(value.taxID)
            && !isNil(value.organizationalJurisdiction) && !isNil(value.entityTaxPurpose)
            && !isEmpty(value.owners);
          }
        }
        return false;
      },
    },
  },
  'llc.subscriberLegalName': {
    in: 'body',
    isString: true,
    optional: true,    
    errorMessage: 'Subscriber Legal Name is not valid.',
    isLength: {
      options: { min: 3, max: 150 },
      errorMessage: 'Subscriber Legal Name must be greater than 3 characters and less than 150.',
    },
	},
  'llc.taxID': {
    in: 'body',
    isString: true,
    optional: true,
    errorMessage: 'TaxID is not valid.',
    isLength: {
      options: { min: 3, max: 150 },
      errorMessage: 'TaxID must be greater than 3 characters and less than 150.',
    },
	},
  'llc.usPerson': {
    in: 'body',
    isBoolean: true,
    optional: true,
	},
  'llc.ultimateBeneficial': {
    in: 'body',
    isString: true,
    optional: true,
    errorMessage: 'Ultimate Beneficial is not valid.',
    custom: {
      errorMessage: 'Ultimate Beneficial is not required.',
      options: (__, { req }) => req.body.llc.usPerson,
    },
    isLength: {
      options: { min: 3, max: 150 },
      errorMessage: 'Ultimate Beneficial must be greater than 3 characters and less than 150.',
    },
	},
  'llc.ultimateBeneficialOwnerTaxId': {
    in: 'body',
    isString: true,
    optional: true,
    errorMessage: 'Ultimate Beneficial Owner TaxId is not valid.',
    custom: {
      errorMessage: 'Ultimate Beneficial Owner TaxId is not required.',
      options: (__, { req }) => req.body.llc.usPerson,
    },
    isLength: {
      options: { min: 3, max: 150 },
      errorMessage: 'Ultimate Beneficial Owner TaxId must be greater than 3 characters and less than 150.',
    },
	},
  'llc.country': {
    in: 'body',
    isString: true,
    optional: true,
    errorMessage: 'Country is not valid.',
    custom: {
      errorMessage: 'Country is not required.',
      options: (__, { req }) => req.body.llc.usPerson,
    },
    isLength: {
      options: { min: 3, max: 150 },
      errorMessage: 'Country must be greater than 3 characters and less than 150.',
    },
	},
  'llc.organizationalJurisdiction': {
    in: 'body',
    isString: true,
    optional: true,
    errorMessage: 'Organizational Jurisdiction is not valid.',
    custom: {
      errorMessage: 'Organizational Jurisdiction is not required.',
      options: (__, { req }) => req.body.llc.usPerson === false,
    },
    isLength: {
      options: { min: 3, max: 150 },
      errorMessage: 'Organizational Jurisdictio must be greater than 3 characters and less than 150.',
    },
	},
  'llc.entityTaxPurpose': {
    in: 'body',
    isBoolean: true,
    optional: true,
    custom: {
      errorMessage: 'Entity Tax Purpose is not required.',
      options: (__, { req }) => req.body.llc.usPerson === false,
    },
	},
  'llc.owners': {
    in: 'body',
    isArray: {
      errorMessage: 'The field owners must be an array.',
    },
    optional: true,
    custom: {
      errorMessage: 'LLC Owners is invalid',
      options: (value) => {
        if(value.length === 0) return false;
        for (let index = 0; index < value.length; index++) {
          const element = value[index];
          if(isNil(element.naturalPerson)) {
            return false;
          }
          if(element.naturalPerson) {
            if(isEmpty(element.person) || isEmpty(element.person.firstName) ||
            isEmpty(element.person.middleName) || isEmpty(element.person.lastName) ||
            isEmpty(element.person.phoneNumber) || isEmpty(element.person.email)
            || isEmpty(element.person.title) || !isEmpty(element.entity)) {
              return false;
            }
          } else {
            if(isEmpty(element.entity) || isEmpty(element.entity.entityName) ||
            isEmpty(element.entity.entityTitle) || isNil(element.entity.basicSignatureBlock)
            || !isEmpty(element.person)) {
              return false;
            }
          }
        }
        return true;
      },
    },
	},
  ['llc.owners.*.naturalPerson']: {
    in: 'body',
    isBoolean: true,
    optional: true,
  },
  ['llc.owners.*.person']: {
    in: 'body',
    optional: true,
    isObject: {
      errorMessage: 'Person must be an object.',
    },
	},
  ['llc.owners.*.person.firstName']: {
    in: 'body',
    isString: true,
    optional: true,
    errorMessage: 'The First Name is not valid.',
    isLength: {
      options: { min: 3, max: 150 },
      errorMessage: 'First Name must be greater than 3 characters and less than 150.',
    },
	},
  ['llc.owners.*.person.middleName']: {
    in: 'body',
    isString: true,
    optional: true,
    errorMessage: 'The Middle Name is not valid.',
    isLength: {
      options: { min: 3, max: 150 },
      errorMessage: 'Middle Name must be greater than 3 characters and less than 150.',
    },
	},
  ['llc.owners.*.person.lastName']: {
    in: 'body',
    isString: true,
    optional: true,
    errorMessage: 'The Last Name is not valid.',
    isLength: {
      options: { min: 3, max: 150 },
      errorMessage: 'Last Name must be greater than 3 characters and less than 150.',
    },
	},
  ['llc.owners.*.person.phoneNumber']: {
    in: 'body',
    isString: true,
    optional: true,
    errorMessage: 'The Phone Number is not valid.',
    isLength: {
      options: { min: 3, max: 150 },
      errorMessage: 'Phone Number must be greater than 3 characters and less than 150.',
    },
	},
  ['llc.owners.*.person.email']: {
    in: 'body',
    isString: true,
    optional: true,
    errorMessage: 'The E-mail is not valid.',
    isLength: {
      options: { min: 3, max: 150 },
      errorMessage: 'E-mail must be greater than 3 characters and less than 150.',
    },
	},
  ['llc.owners.*.person.title']: {
    in: 'body',
    isString: true,
    optional: true,
    errorMessage: 'The Title is not valid.',
    isLength: {
      options: { min: 3, max: 150 },
      errorMessage: 'Title must be greater than 3 characters and less than 150.',
    },
	},
  ['llc.owners.*.person.dateBirthday']: {
    in: 'body',
    isISO8601: true, 
    optional: true,
    errorMessage: 'The dateBirthday doesn´t have Date  format',
    custom: {
      errorMessage: 'Need to be of legal age.',
      options: (value) => {
        const brith = DateTime.fromISO(value);
        return brith.diffNow('years').years < -21;
      },
    },
	},
  ['llc.owners.*.person.taxID']: {
    in: 'body',
    isString: true,
    optional: true,
    errorMessage: 'The TaxID is not valid.',
    isLength: {
      options: { min: 3, max: 150 },
      errorMessage: 'TaxID must be greater than 3 characters and less than 150.',
    },
	},
  ['llc.owners.*.entity']: {
    in: 'body',
    optional: true,
    isObject: {
      errorMessage: 'Entity must be an object.',
    },
	},
  ['llc.owners.*.entity.entityName']: {
    in: 'body',
    isString: true,
    optional: true,
    errorMessage: 'Entity Name is not valid.',
    isLength: {
      options: { min: 3, max: 150 },
      errorMessage: 'Entity Name must be greater than 3 characters and less than 150.',
    },
	},
  ['llc.owners.*.entity.entityTitle']: {
    in: 'body',
    isString: true,
    optional: true,
    errorMessage: 'The Entity Title is not valid.',
    isLength: {
      options: { min: 3, max: 150 },
      errorMessage: 'Entity Title must be greater than 3 characters and less than 150.',
    },
	},
  ['llc.owners.*.entity.basicSignatureBlock']: {
    in: 'body',
    isBoolean: true,
    optional: true,
	},
  otherJoint: {
    in: 'body',
    optional: true,
    isObject: {
      errorMessage: 'Other joint ownership must be an object.',
    },
    custom: {
      errorMessage: 'Other joint ownership is not valid.',
      options: (value, { req }) => {
        if(req.body.typeFormOnboarding === EnumTypeFormOnboarding.OTHERJOINT) {
          return !isEmpty(value.subscriberLegalName) && !isEmpty(value.taxID)
            && !isEmpty(value.owners);
        }
        return false;
      },
    },
  },
  'otherJoint.subscriberLegalName': {
    in: 'body',
    isString: true,
    optional: true,    
    errorMessage: 'Subscriber Legal Name is not valid.',
    isLength: {
      options: { min: 3, max: 150 },
      errorMessage: 'Subscriber Legal Name must be greater than 3 characters and less than 150.',
    },
  },
  'otherJoint.taxID': {
    in: 'body',
    isString: true,
    optional: true,
    errorMessage: 'TaxID is not valid.',
    isLength: {
      options: { min: 3, max: 150 },
      errorMessage: 'TaxID must be greater than 3 characters and less than 150.',
    },
  },
  'otherJoint.entityTaxPurpose': {
    in: 'body',
    isBoolean: true,
    optional: true,
  },
  'otherJoint.owners': {
    in: 'body',
    isArray: {
      errorMessage: 'The field owners must be an array.',
    },
    optional: true,
    custom: {
      errorMessage: 'Other joint ownership Owners is invalid',
      options: (value) => {
        if(value.length === 0) return false;
        for (let index = 0; index < value.length; index++) {
          const element = value[index];
          if(isNil(element.usPerson)) {
            return false;
          }
        }
        return true;
      },
    },
  },
  ['otherJoint.owners.*.usPerson']: {
    in: 'body',
    isBoolean: true,
    optional: true,
  },
  ['otherJoint.owners.*.person']: {
    in: 'body',
    optional: true,
    isObject: {
      errorMessage: 'Person must be an object.',
    },
    custom: {
      errorMessage: 'Other joint Ownership Owners Person is invalid',
      options: (value) => {
       return !isEmpty(value.firstName) && !isEmpty(value.phoneNumber) 
          && !isEmpty(value.middleName) && !isEmpty(value.lastName)
          && !isEmpty(value.email) && !isEmpty(value.citizenship)
          && !isEmpty(value.title);
      },
    },
  },
  ['otherJoint.owners.*.person.firstName']: {
    in: 'body',
    isString: true,
    optional: true,
    errorMessage: 'The First Name is not valid.',
    isLength: {
      options: { min: 3, max: 150 },
      errorMessage: 'First Name must be greater than 3 characters and less than 150.',
    },
  },
  ['otherJoint.owners.*.person.middleName']: {
    in: 'body',
    isString: true,
    optional: true,
    errorMessage: 'The Middle Name is not valid.',
    isLength: {
      options: { min: 3, max: 150 },
      errorMessage: 'The Middle Name must be greater than 3 characters and less than 150.',
    },
  },
  ['otherJoint.owners.*.person.lastName']: {
    in: 'body',
    isString: true,
    optional: true,
    errorMessage: 'The Last Name is not valid.',
    isLength: {
      options: { min: 3, max: 150 },
      errorMessage: 'Last Name must be greater than 3 characters and less than 150.',
    },
  },
  ['otherJoint.owners.*.person.phoneNumber']: {
    in: 'body',
    isString: true,
    optional: true,
    errorMessage: 'The Phone Number is not valid.',
    isLength: {
      options: { min: 3, max: 150 },
      errorMessage: 'Phone Number must be greater than 3 characters and less than 150.',
    },
  },
  ['otherJoint.owners.*.person.email']: {
    in: 'body',
    isString: true,
    optional: true,
    errorMessage: 'The E-mail is not valid.',
    isLength: {
      options: { min: 3, max: 150 },
      errorMessage: 'E-mail must be greater than 3 characters and less than 150.',
    },
  },
  ['otherJoint.owners.*.person.citizenship']: {
    in: 'body',
    isString: true,
    optional: true,
    errorMessage: 'The Citizenship is not valid.',
    isLength: {
      options: { min: 3, max: 150 },
      errorMessage: 'Citizenship must be greater than 3 characters and less than 150.',
    },
  },
  ['otherJoint.owners.*.person.title']: {
    in: 'body',
    isString: true,
    optional: true,
    errorMessage: 'The Title is not valid.',
    isLength: {
      options: { min: 3, max: 150 },
      errorMessage: 'The Title must be greater than 3 characters and less than 150.',
    },
  },
  ['otherJoint.owners.*.person.dateBirthday']: {
    in: 'body',
    isISO8601: true, 
    optional: true,
    errorMessage: 'The dateBirthday doesn´t have Date  format',
    custom: {
      errorMessage: 'Need to be of legal age.',
      options: (value) => {
        const brith = DateTime.fromISO(value);
        return brith.diffNow('years').years < -21;
      },
    },
  },
  ['otherJoint.owners.*.person.taxID']: {
    in: 'body',
    isString: true,
    optional: true,
    errorMessage: 'The TaxID is not valid.',
    isLength: {
      options: { min: 3, max: 150 },
      errorMessage: 'TaxID must be greater than 3 characters and less than 150.',
    },
  },
};