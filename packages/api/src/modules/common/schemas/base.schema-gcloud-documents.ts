import { ParamSchema } from 'express-validator';
export const createGCloudDocumentSchema: Record<string, ParamSchema> = {
  path: {
    in: 'body',
    isString: {
      errorMessage: 'path should be a string',
    },
    notEmpty: {
      errorMessage: 'path cannot be empty',
    },
  },
  addTaxCenter: {
    in: 'body',
    isBoolean: {
      errorMessage: 'addTaxCenter should be a boolean',
    },
    notEmpty: {
      errorMessage: 'addTaxCenter cannot be empty',
    },
  },
};

export const listGCloudDocumentsSchema: Record<string, ParamSchema> = {
  limit: {
    in: 'query',
    isNumeric: {
      errorMessage: 'limit should be a number',
    },
    optional: true,
  },
  page: {
    in: 'query',
    isNumeric: {
      errorMessage: 'page should be a number',
    },
    optional: true,
  },
  search: {
    in: 'query',
    isString: {
      errorMessage: 'search should be a string',
    },
    optional: true,
  },
};

export const deleteGCloudDocumentSchema: Record<string, ParamSchema> = {
  idDocument: {
    in: 'params',
    isMongoId: {
      errorMessage: 'The ID of the submitted document is not a MongoId',
    },
  },
};

export const selectGCloudDocumentSchema: Record<string, ParamSchema> = {
  search: {
    in: 'query',
    isString: true,
    optional: true,
  },
};
