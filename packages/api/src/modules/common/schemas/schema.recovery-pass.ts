import { ParamSchema } from 'express-validator';
import * as regExp from '../ReExp';

export const schemaRecoveryPass: Record<string, ParamSchema> = {
  email: {
    in: ['body'],
    isEmail: true,
    errorMessage: 'Email is wrong',
    matches: {
      options: [regExp.validEmail],
      errorMessage: "Email doesn't have a valid structure",
    },
  },
};

export const schemaRecoveryPassPatch: Record<string, ParamSchema> = {
  token: {
    in: ['body'],
    errorMessage: 'Token is invalid',
  },
  password: {
    in: ['body'],
    errorMessage: 'Password is invalid.',
    isLength: {
      errorMessage: 'The password must be at least 6 characters.',
      options: { min: 6 },
    },
    matches: {
      options: [regExp.customPassword],
      errorMessage: 'Password is not valid',
    },
  },
};

export const schemaChangePass: Record<string, ParamSchema> = {
  currentPassword: {
    in: ['body'],
    exists: {
      errorMessage: 'The current password is required.',
    },
    errorMessage: 'Current password is invalid.',
    matches: {
      options: [regExp.customPassword],
      errorMessage: 'Your password is not enough safe.',
    },
  },
  newPassword: {
    in: ['body'],
    exists: {
      errorMessage: 'The new password is required.',
    },
    errorMessage: 'New password is invalid.',
    matches: {
      options: [regExp.customPassword],
      errorMessage: 'Your password is not enough safe.',
    },
  },
};
