import { ParamSchema } from 'express-validator';

export const SchemaCreateCapitalCall: Record<string, ParamSchema> = {
  description: {
    in: 'body',
    isString: true,
    errorMessage: 'Description is a required value.',
    isLength: {
      options: { min: 3, max: 150 },
      errorMessage:
        'Description must be greater than 3 characters and less than 150.',
    },
  },
  dueDate: {
    in: 'body',
    errorMessage: 'The due date must be MM/DD/YYYY format',
    isISO8601: true,
  },
  investors: {
    in: 'body',
    isArray: {
      errorMessage: 'The field investors must be an array.',
    },
  },
  ['investors.*.id']: {
    in: 'body',
    isMongoId: true,
    errorMessage: 'The field must be a valid MongoDB ID and is required.',
  },
  ['investors.*.capitalAmount']: {
    in: 'body',
    errorMessage: 'The field capital amount must be a decimal and is required.',
  },
};

export const SchemaUpdateCapitalCall: Record<string, ParamSchema> = {
  description: {
    in: 'body',
    isString: true,
    errorMessage: 'Description is a required value.',
    isLength: {
      options: { min: 3, max: 150 },
      errorMessage:
        'Description must be greater than 3 characters and less than 150.',
    },
  },
  dueDate: {
    in: 'body',
    errorMessage: 'The due date must be MM/DD/YYYY format',
    isISO8601: true,
  },
};
