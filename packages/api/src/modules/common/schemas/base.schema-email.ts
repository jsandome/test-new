import { ParamSchema } from 'express-validator';

export const baseSchemaEmail: Record<string, ParamSchema> = {
  email: {
    in: ['body'],
    isEmail: true,
    errorMessage: 'Email is wrong',
    isLength: {
      errorMessage: 'Email cannot has more than 25 characters',
      options: { max: 25 },
    },
  },
};
