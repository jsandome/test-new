import { ParamSchema } from 'express-validator';
import { EnumEmailType, EnumPhoneType } from '@capa/core';
import * as regExp from '../ReExp';

export const SchemaUpdateInvestor: Record<string, ParamSchema> = {
  email: {
    in: 'body',
    errorMessage: 'Email is wrong',
    matches: {
      options: [regExp.validEmail],
      errorMessage: "Email doesn't have a valid structure",
    },
    isLength: {
      options: { min: 3, max: 50 },
      errorMessage: 'email must be greater than 3 characters and less than 50',
    },
  },
  emailType: {
    in: 'body',
    custom: {
      errorMessage: `Type no valid, valid types: ${Object.values(
        EnumEmailType
      ).toString()}`,
      options: (value) => {
        return Object.values(EnumEmailType).includes(value);
      },
    },
    notEmpty: true,
  },
  phone: {
    in: 'body',
    isString: true,
    optional: true,
    isLength: {
      options: { min: 3, max: 15 },
      errorMessage: 'phone must be greater than 3 characters and less than 15.',
    },
  },
  phoneType: {
    in: 'body',
    custom: {
      errorMessage: `Type no valid, valid types: ${Object.values(
        EnumPhoneType
      ).toString()}`,
      options: (value) => {
        return Object.values(EnumPhoneType).includes(value);
      },
    },
    notEmpty: true,
  },
  addresses: {
    in: 'body',
    isArray: {
      errorMessage: 'addresses must be an array',
    },
  },
  ['addresses.*.address']: {
    in: 'body',
    isString: true,
    errorMessage: 'address its a required field',
    isLength: {
      options: { min: 3, max: 500 },
      errorMessage: 'The minimum length is 3 characters and maximum is 500',
    },
  },
  ['addresses.*.city']: {
    in: 'body',
    isString: true,
    errorMessage: 'city its a required field',
    isLength: {
      options: { min: 3, max: 50 },
      errorMessage: 'The minimum length is 3 characters and maximum is 50',
    },
  },
  ['addresses.*.state']: {
    isMongoId: true,
    notEmpty: {
      errorMessage: 'State Id cannot be empty',
    },
  },
  ['addresses.*.county']: {
    isMongoId: true,
    notEmpty: {
      errorMessage: 'County Id cannot be empty',
    },
  },
  ['addresses.*.zipCode']: {
    in: ['body'],
    isPostalCode: {
      options: 'US',
      errorMessage: `Zip code doesn't have a valid format`,
    },
  },
  ['addresses.*.isPrincipal']: {
    in: ['body'],
    isBoolean: {
      errorMessage: `isPrincipal must be a boolean`,
    },
  },
  biographical: {
    in: 'body',
    isObject: true
  },
  ['biographical.name']: {
    in: 'body',
    isString: true,
    errorMessage: 'name cannot be empty',
    isLength: {
      options: { min: 3, max: 100 },
      errorMessage: 'Name must be greater than 3 characters and less than 100.',
    },
  },
  ['biographical.city']: {
    in: 'body',
    isString: true,
    errorMessage: 'city cannot be empty',
    isLength: {
      options: { min: 3, max: 100 },
      errorMessage: 'city must be greater than 3 characters and less than 100.',
    },
  },
  ['biographical.jobTitle']: {
    in: 'body',
    isString: true,
    errorMessage: 'jobTitle cannot be empty',
    isLength: {
      options: { min: 3, max: 100 },
      errorMessage:
        'jobTitle must be greater than 3 characters and less than 100.',
    },
  },
  ['biographical.companyName']: {
    in: 'body',
    isString: true,
    errorMessage: 'companyName cannot be empty',
    isLength: {
      options: { min: 3, max: 100 },
      errorMessage:
        'companyName must be greater than 3 characters and less than 100.',
    },
  },
  ['biographical.referrer']: {
    in: 'body',
    isString: true,
    errorMessage: 'referrer cannot be empty',
    isLength: {
      options: { min: 3, max: 100 },
      errorMessage:
        'referrer must be greater than 3 characters and less than 100.',
    },
  },
  ['biographical.assignee']: {
    in: 'body',
    isString: true,
    errorMessage: 'assignee cannot be empty',
    isLength: {
      options: { min: 3, max: 100 },
      errorMessage:
        'assignee must be greater than 3 characters and less than 100.',
    },
  },
  additionalDetails: {
    in: 'body',
    isString: true,
    optional: true,
    errorMessage: 'additionalDetails cannot be empty',
    isLength: {
      options: { min: 3, max: 500 },
      errorMessage:
        'The minimum length of additionalDetails is 3 characters and maximum is 500',
    },
  },
};
