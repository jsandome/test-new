import { ParamSchema } from 'express-validator';

export const schemaCreateLlc: Record<string, ParamSchema> = {
  name: {
    in: ['body'],
    errorMessage: 'name is required',
    isLength: {
      errorMessage: 'The name must to have at least 3 characters',
      options: { min: 3, max: 25 },
    },
  },
  usPerson: {
    in: ['body'],
    errorMessage: 'usPerson is required',
    isBoolean: true,
  },
  taxId: {
    in: ['body'],
    errorMessage: 'taxId is required',
    isString: true,
  },
  address: {
    in: ['body'],
    errorMessage: 'address is required',
    isString: true,
  },
  state: {
    in: ['body'],
    isMongoId: true,
    errorMessage: 'The field must be a valid MongoDB ID and is required',
  },
  date: {
    in: ['body'],
    errorMessage: 'The date must be MM/DD/YYYY format',
    isISO8601: true,
  },
  investors: {
    in: ['body'],
    isMongoId: true,
    errorMessage: 'The field should be an array',
  },
  properties: {
    in: ['body'],
    isArray: {
      errorMessage: 'The field should be an array',
    },
  },
  ['properties.*.id']: {
    in: ['body'],
    isMongoId: true,
    errorMessage: 'The field must be a valid MongoDB ID and is required',
  },
  ['properties.*.share']: {
    in: ['body'],
    isDecimal: {
      errorMessage: 'The share must be a decimal and is required',
    },
  },
};
