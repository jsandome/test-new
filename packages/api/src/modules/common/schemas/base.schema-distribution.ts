import { IEnumTypesPayment } from '@capa/core';
import { ParamSchema } from 'express-validator';
import * as regExp from '../ReExp';

export const SchemaCreateDistribution: Record<string, ParamSchema> = {
  description: {
    in: 'body',
    isString: {
      errorMessage: 'description must be a string.',
    },
    isLength: {
      options: { min: 3, max: 500 },
      errorMessage:
        'description must be greater than 3 characters and less than 500',
    },
  },
  distributionDate: {
    in: 'body',
    errorMessage: 'Distribution date must be MM/DD/YYYY format',
    isISO8601: true,
  },
  yieldPeriod: {
    in: 'body',
    isString: {
      errorMessage: 'yield period must be a string.',
    },
    matches: {
      options: [regExp.yieldPeriod],
      errorMessage:
        "The distribution yield period doesn't have a valid structure.",
    },
  },
  account: {
    in: 'body',
    isArray: {
      errorMessage: 'The field investors must be an array.',
    },
  },
  ['account.*.id']: {
    in: 'body',
    isMongoId: true,
    errorMessage: 'The field must be a valid MongoDB ID and is required.',
  },
  ['account.*.paymentType']: {
    in: 'body',
    custom: {
      errorMessage: `Type no valid, valid types: ${Object.values(
        IEnumTypesPayment
      ).toString()}`,
      options: (value) => Object.values(IEnumTypesPayment).includes(value),
    },
  },
  ['account.*.paymentNumber']: {
    in: 'body',
    optional: true,
    isString: {
      errorMessage: 'The distribution payment number must be a string.',
    },
    matches: {
      options: [regExp.alphaNumeric],
      errorMessage:
        "The distribution payment number doesn't have a valid structure.",
    },
  },
  ['account.*.amount']: {
    in: 'body',
    isNumeric: {
      errorMessage:
        'The field distribution amount must be a decimal and is required.',
    },
  },
};

export const SchemaUpdateDistribution: Record<string, ParamSchema> = {
  description: {
    in: 'body',
    isString: {
      errorMessage: 'description must be a string.',
    },
    isLength: {
      options: { min: 3, max: 500 },
      errorMessage:
        'description must be greater than 3 characters and less than 500',
    },
  },
  distributionDate: {
    in: 'body',
    errorMessage: 'Distribution date must be MM/DD/YYYY format',
    isISO8601: true,
  },
  yieldPeriod: {
    in: 'body',
    isString: {
      errorMessage: 'yield period must be a string.',
    },
    matches: {
      options: [regExp.yieldPeriod],
      errorMessage:
        "The distribution yield period doesn't have a valid structure.",
    },
  },
  account: {
    in: 'body',
    isArray: {
      errorMessage: 'The field investors must be an array.',
    },
  },
  ['account.*.id']: {
    in: 'body',
    isMongoId: true,
    errorMessage: 'The field must be a valid MongoDB ID and is required.',
  },
  ['account.*.paymentType']: {
    in: 'body',
    custom: {
      errorMessage: `Type no valid, valid types: ${Object.values(
        IEnumTypesPayment
      ).toString()}`,
      options: (value) => Object.values(IEnumTypesPayment).includes(value),
    },
  },
  ['account.*.paymentNumber']: {
    in: 'body',
    optional: true,
    isString: {
      errorMessage: 'The distribution payment number must be a string.',
    },
    matches: {
      options: [regExp.alphaNumeric],
      errorMessage:
        "The distribution payment number doesn't have a valid structure.",
    },
  },
  ['account.*.amount']: {
    in: 'body',
    isNumeric: {
      errorMessage:
        'The field distribution amount must be a decimal and is required.',
    },
  },
};
