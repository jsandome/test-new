import { IEnumTypesPayment } from '@capa/core';
import { ParamSchema } from 'express-validator';
import * as regExp from '../ReExp';

export const SchemaCreateContribution: Record<string, ParamSchema> = {
  investor: {
    in: 'body',
    isMongoId: true,
    errorMessage: 'The field must be a valid MongoDB ID and is required.',
  },
  dateReceived: {
    in: 'body',
    errorMessage: 'The date received must be MM/DD/YYYY format',
    isISO8601: true,
  },
  capitalCall: {
    in: 'body',
    isObject: {
      errorMessage: 'The Capital Call mus be an object.',
    },
  },
  'capitalCall.id': {
    in: 'body',
    isMongoId: true,
    errorMessage:
      'The Capital Call id must be a valid MongoDB ID and is required.',
  },
  'capitalCall.amount': {
    in: 'body',
    errorMessage: 'The Capital Call it`s required.',
    isNumeric: {
      errorMessage: 'The Capital Call amount must be a number.',
    },
  },
  'capitalCall.paymentType': {
    in: 'body',
    custom: {
      errorMessage: `Type no valid, valid types: ${Object.values(
        IEnumTypesPayment
      ).toString()}`,
      options: (value) => Object.values(IEnumTypesPayment).includes(value),
    },
  },
  'capitall.paymentNumber': {
    in: 'body',
    optional: true,
    isString: {
      errorMessage: 'The Capital Call payment number must be a string.',
    },
    matches: {
      options: [regExp.alphaNumeric],
      errorMessage:
        "The Capital Call payment number doesn't have a valid structure.",
    },
  },
  'capitall.notes': {
    in: 'body',
    optional: true,
    isString: {
      errorMessage: 'The Capital Call notes must be a string.',
    },
    isLength: {
      options: { min: 3, max: 150 },
      errorMessage: 'Notes must be greater than 3 characters and less than 150',
    },
  },
};

export const SchemaRecordContribution: Record<string, ParamSchema> = {
  amountContribution: {
    in: 'body',
    errorMessage: 'The Contribution amount it`s required.',
    isNumeric: {
      errorMessage: 'The Contribution amount must be a number.',
    },
  },
  paymentDate: {
    in: 'body',
    errorMessage: 'The payment date must be MM/DD/YYYY format',
    isISO8601: true,
  },
  paymentType: {
    in: 'body',
    custom: {
      errorMessage: `Type no valid, valid types: ${Object.values(
        IEnumTypesPayment
      ).toString()}`,
      options: (value) => Object.values(IEnumTypesPayment).includes(value),
    },
  },
  paymentNumber: {
    in: 'body',
    optional: true,
    isString: {
      errorMessage: 'The Contribution payment number must be a string.',
    },
    matches: {
      options: [regExp.alphaNumeric],
      errorMessage:
        "The Contribution payment number doesn't have a valid structure",
    },
  },
  notes: {
    in: 'body',
    optional: true,
    isString: {
      errorMessage: 'The Contribution notes number must be a string.',
    },
    isLength: {
      options: { min: 3, max: 150 },
      errorMessage: 'notes must be greater than 3 characters and less than 150',
    },
  },
};

export const SchemaListContribution: Record<string, ParamSchema> = {
  limit: {
    in: 'query',
    isNumeric: {
      errorMessage: 'limit should be a number',
    },
    optional: true,
  },
  page: {
    in: 'query',
    isNumeric: {
      errorMessage: 'page should be a number',
    },
    optional: true,
  },
  search: {
    in: 'query',
    isString: {
      errorMessage: 'search should be a string',
    },
    optional: true,
  },
  llcId: {
    in: 'params',
    isMongoId: {
      errorMessage: 'The ID of the Entity is not a MongoId',
    },
  },
};

export const SchemaUpdateContribution: Record<string, ParamSchema> = {
  dateReceived: {
    in: 'body',
    errorMessage: 'The date received must be MM/DD/YYYY format',
    isISO8601: true,
  },
  capitalCall: {
    in: 'body',
    isObject: {
      errorMessage: 'The Capital call must be an object',
    },
  },
  'capitalCall.amount': {
    in: 'body',
    errorMessage: 'The Capital call it`s required.',
    isNumeric: {
      errorMessage: 'The Capital call amount must be a number.',
    },
  },
  'capitalCall.paymentType': {
    in: 'body',
    custom: {
      errorMessage: `Type no valid, valid types: ${Object.values(
        IEnumTypesPayment
      ).toString()}`,
      options: (value) => Object.values(IEnumTypesPayment).includes(value),
    },
  },
  'capitall.paymentNumber': {
    in: 'body',
    optional: true,
    isString: {
      errorMessage: 'The Capital call payment number must be a string.',
    },
    matches: {
      options: [regExp.alphaNumeric],
      errorMessage:
        "The Capital call payment number doesn't have a valid structure",
    },
  },
  'capitall.notes': {
    in: 'body',
    optional: true,
    isString: {
      errorMessage: 'The Capital call notes must be a string.',
    },
    isLength: {
      options: { min: 3, max: 150 },
      errorMessage: 'Notes must be greater than 3 characters and less than 150',
    },
  },
};
