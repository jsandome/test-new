import { ParamSchema } from 'express-validator';

export const SchemaLogin: Record<string, ParamSchema> = {
  user: {
    in: ['body'],
    errorMessage: 'User is wrong',
    isLength: {
      errorMessage: 'Email cannot has more than 40 characters',
      options: { max: 40 },
    },
  },
  password: {
    in: ['body'],
    errorMessage: 'Password is invalid.',
    isLength: {
      errorMessage: 'The password must be at least 6 characters.',
      options: { min: 6 },
    },
  },
};
