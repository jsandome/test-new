import { ParamSchema } from 'express-validator';
import * as regExp from '../ReExp';

export const SchemaLoginCode: Record<string, ParamSchema> = {
  user: {
    in: ['body'],
    errorMessage: 'User is not valid.',
    matches: {
      options: [regExp.validEmail],
      errorMessage: "Email doesn't have a valid structure",
    },
  },
  secure: {
    in: ['body'],
    errorMessage: 'Secure is not valid.',
  },
  code: {
    in: ['body'],
    errorMessage: 'Code is not valid.',
    matches: {
      options: [regExp.code6Digits],
      errorMessage: "Code doesn't have a valid structure",
    },
  },
};
