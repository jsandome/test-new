import { ParamSchema } from 'express-validator';

export const SchemaCreateOfferingProspective: Record<string, ParamSchema> = {
  offering: {
    in: 'body',
    isMongoId: {
      errorMessage: 'offering id should be a mongoId',
    },
    errorMessage: 'offering its a required field',
  },
  offeringType: {
    in: 'body',
    isString: true,
    errorMessage: 'offeringType its a required field',
  },
  status: {
    in: 'body',
    isString: true,
    errorMessage: 'status its a required field',
  },
  expectedAmount: {
    in: 'body',
    isNumeric: true,
    errorMessage: 'expectedAmount its a required field',
  },
  likeliHood: {
    in: 'body',
    isDecimal: {
      errorMessage: 'likeliHood its a percentage',
    },
    notEmpty: {
      errorMessage: 'likeliHood cannot be empty',
    },
    custom: {
      errorMessage: 'likeliHood should be greather than 0 and maximum 100',
      options: (likeliHood) => likeliHood > 0 && likeliHood <= 100,
    },
    errorMessage: 'likeliHood its a required field',
  },
  ['additionalProspect']: {
    in: 'body',
    isArray: true,
    errorMessage: 'additionalProspect its a required field',
  },
  ['additionalProspect.*.id']: {
    in: 'body',
    isMongoId: {
      errorMessage: 'additionalProspect id should be a mongoId',
    },
  },
  ['additionalProspect.*.primary']: {
    in: 'body',
    isBoolean: {
      errorMessage: 'additionalProspect primary should be a boolean',
    },
  },
};

export const SchemaUpdateOfferingProspective: Record<string, ParamSchema> = {
  status: {
    in: 'body',
    isString: true,
    errorMessage: 'status its a required field',
  },
  likeliHood: {
    in: 'body',
    isDecimal: {
      errorMessage: 'likeliHood its a percentage',
    },
    notEmpty: {
      errorMessage: 'likeliHood cannot be empty',
    },
    custom: {
      errorMessage: 'likeliHood should be greather than 0 and maximum 100',
      options: (likeliHood) => likeliHood > 0 && likeliHood <= 100,
    },
    errorMessage: 'likeliHood its a required field',
  },
  expectedAmount: {
    in: 'body',
    isNumeric: true,
    errorMessage: 'expectedAmount its a required field',
  },
};
