import { ParamSchema } from 'express-validator';

export const SchemaCreatePackageDocuments: Record<string, ParamSchema> = {
  name: {
    in: ['body'],
    errorMessage: 'Name is required.',
    isLength: {
      errorMessage:
        'Name cannot have less than 3 characters.',
      options: { min: 3, max: 150 },
    },
  },
  templates: {
    in: ['body'],
    isArray: {
      errorMessage: 'The field must be an array.',
    },
    notEmpty: true,
  }
};

export const SchemaDuplicatePackageDocuments: Record<string, ParamSchema> = {
  name: {
    in: ['body'],
    errorMessage: 'Name is required.',
    notEmpty: {
      errorMessage: 'Name cannot be empty',
    },
    isLength: {
      errorMessage:
        'Name cannot have less than 3 characters.',
        options: { min: 3, max: 150 },
    },
  },
  newTemplates: {
    in: ['body'],
    custom: {
      errorMessage: 'newTemplates not allow empty array',
      options: (value) => {
        return value.length > 0;
      },
    },
    isArray: {
      errorMessage: 'The field must be an array.',
    },
    optional: true,
  },
  deleteTemplates: {
    in: ['body'],
    custom: {
      errorMessage: 'deleteTemplates not allow empty array',
      options: (value) => {
        return value.length > 0;
      },
    },
    isArray: {
      errorMessage: 'The field must be an array.',
    },
    optional: true,
  }
};