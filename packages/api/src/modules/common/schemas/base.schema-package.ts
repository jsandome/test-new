import { ParamSchema } from 'express-validator';

export const SchemaCreatePackage: Record<string, ParamSchema> = {
  name: {
    in: ['body'],
    errorMessage: 'Name is required.',
    isLength: {
      errorMessage:
        'Name cannot have less than 3 characters and maximum is 150.',
      options: { min: 3, max: 150 },
    },
  },
  totalValue: {
    in: ['body'],
    errorMessage: 'totalValue is invalid.',
    isNumeric: {
      errorMessage: 'The totalValue must be a number and is required.',
    },
  },
  investors: {
    in: ['body'],
    isArray: {
      errorMessage: 'The field must be an array.',
    },
  },
  ['investors.*.id']: {
    in: ['body'],
    isMongoId: true,
    errorMessage: 'The field must be a valid MongoDB ID and is required.',
  },
  ['investors.*.minCommitment']: {
    in: ['body'],
    isDecimal: {
      errorMessage: 'The minCommitment must be a decimal and is required.',
    },
  },
  ['investors.*.initCommitment']: {
    in: ['body'],
    isDecimal: {
      errorMessage: 'The initCommitment must be a decimal and is required.',
    },
  },
  ['investors.*.totalCommitment']: {
    in: ['body'],
    isDecimal: {
      errorMessage: 'The totalCommitment must be a decimal and is required.',
    },
  },
  ['investors.*.termYears']: {
    in: ['body'],
    isInt: {
      errorMessage: 'The termYears must be a int and is required.',
    },
  },
  properties: {
    in: ['body'],
    isArray: {
      errorMessage: 'The field must be an array.',
    },
  },
  ['properties.*.id']: {
    in: ['body'],
    isMongoId: true,
    errorMessage: 'The field must be a valid MongoDB ID and is required.',
  },
  ['properties.*.share']: {
    in: ['body'],
    isDecimal: {
      errorMessage: 'The share must be a decimal and is required.',
    },
  },
};

export const SchemaPreviewPackage: Record<string, ParamSchema> = {
  properties: {
    in: ['body'],
    isArray: {
      errorMessage: 'The field must be an array.',
    },
  },
  ['properties.*.id']: {
    in: ['body'],
    isMongoId: true,
    errorMessage: 'The field must be a valid MongoDB ID and is required.',
  },
  ['properties.*.share']: {
    in: ['body'],
    isDecimal: {
      errorMessage: 'The share must be a decimal and is required.',
    },
  },
};

export const SchemaUpdatePackage: Record<string, ParamSchema> = {
  name: {
    in: ['body'],
    errorMessage: 'Name is required.',
    isLength: {
      errorMessage:
        'Name cannot have less than 3 characters and maximum is 150.',
      options: { min: 3, max: 150 },
    },
  },
  totalValue: {
    in: ['body'],
    errorMessage: 'totalValue is invalid.',
    isNumeric: {
      errorMessage: 'The totalValue must be a number and is required.',
    },
  },
  investors: {
    in: ['body'],
    isArray: {
      errorMessage: 'The field nust be an array.',
    },
  },
  ['investors.*.id']: {
    in: ['body'],
    isMongoId: true,
    errorMessage: 'The field must be a valid MongoDB ID and is required.',
  },
  ['investors.*.minCommitment']: {
    in: ['body'],
    isDecimal: {
      errorMessage: 'The minCommitment must be a decimal and is required.',
    },
  },
  ['investors.*.initCommitment']: {
    in: ['body'],
    isDecimal: {
      errorMessage: 'The initCommitment must be a decimal and is required.',
    },
  },
  ['investors.*.totalCommitment']: {
    in: ['body'],
    isDecimal: {
      errorMessage: 'The totalCommitment must be a decimal and is required.',
    },
  },
  ['investors.*.termYears']: {
    in: ['body'],
    isInt: {
      errorMessage: 'The termYears must be a int and is required.',
    },
  },
  properties: {
    in: ['body'],
    isArray: {
      errorMessage: 'The field must be an array.',
    },
  },
  ['properties.*.id']: {
    in: ['body'],
    isMongoId: true,
    errorMessage: 'The field must be a valid MongoDB ID and is required.',
  },
  ['properties.*.share']: {
    in: ['body'],
    isDecimal: {
      errorMessage: 'The share must be a decimal and is required.',
    },
  },
};
