import { ParamSchema } from 'express-validator';
export const loanGeneralSchema: Record<string, ParamSchema> = {
  name: {
    in: 'body',
    isString: {
      errorMessage: 'Name should be a string',
    },
    notEmpty: {
      errorMessage: 'Name cannot be empty',
    },
    isLength: {
      errorMessage: 'Loan Name should be less than 100 and more than 3',
      options: {
        max: 100,
        min: 3,
      },
    },
  },
  interest: {
    in: 'body',
    isDecimal: {
      errorMessage: 'Interest is a percentage',
    },
    notEmpty: {
      errorMessage: 'Interest cannot be empty',
    },
    custom: {
      errorMessage: 'Interest should be greather than 0 and maximum 100',
      options: (interest) => interest > 0 && interest <= 100,
    },
  },
  amortizationYears: {
    in: 'body',
    isNumeric: {
      errorMessage: 'amortizationYears should be a number',
    },
    notEmpty: {
      errorMessage: 'amortizationYears cannot be empty',
    },
    custom: {
      errorMessage:
        'Amortization years should be greather than 0 and maximum 99',
      options: (amortizationYears) =>
        amortizationYears > 0 && amortizationYears <= 99,
    },
  },
  termYears: {
    in: 'body',
    isNumeric: {
      errorMessage: 'termYears should be a number',
    },
    notEmpty: {
      errorMessage: 'termYears cannot be empty',
    },
    custom: {
      errorMessage: 'Term years should be greather than 0 and maximum 99',
      options: (termYears) => termYears > 0 && termYears <= 99,
    },
  },
  amount: {
    in: 'body',
    isDecimal: {
      errorMessage: 'amount should be a number',
    },
    notEmpty: {
      errorMessage: 'amount cannot be empty',
    },
    custom: {
      errorMessage: 'Amount should be greather than 0 and maximum 9999999999',
      options: (amount) => amount > 0 && amount <= 9999999999,
    },
  },
  scheduleExtraPayment: {
    in: 'body',
    isDecimal: {
      errorMessage: 'scheduleExtraPayment should be a number',
    },
    custom: {
      errorMessage:
        'Schedule extra payment should be greather than 0 and maximum 9999999999',
      options: (sEP) => sEP > 0 && sEP <= 9999999999,
    },
    optional: true,
  },
  paymentsPerYear: {
    in: 'body',
    isNumeric: {
      errorMessage: 'paymentsPerYear should be a number',
    },
    custom: {
      errorMessage:
        'Payments Per Year should be greather than 0 and maximum 365',
      options: (paymentsPerYear) =>
        paymentsPerYear > 0 && paymentsPerYear <= 365,
    },
    optional: true,
  },
  beginningDate: {
    in: 'body',
    isISO8601: true,
    notEmpty: {
      errorMessage: 'beginningDate cannot be empty',
    },
  },
  ['extraPayments']: {
    isArray: {
      errorMessage: 'extraPayments should be an array',
    },
  },
  ['extraPayments.*.amount']: {
    in: 'body',
    notEmpty: {
      errorMessage: 'Extra payment amoung cannot be empty',
    },
    custom: {
      errorMessage:
        'Extra Payment Amount should be greather than 0 and maximum 9999999999',
      options: (amount) => amount > 0 && amount <= 9999999999,
    },
  },
  ['extraPayments.*.name']: {
    in: 'body',
    notEmpty: {
      errorMessage: 'Extra payment name cannot be empty',
    },
    isLength: {
      errorMessage:
        'Loan Extra payment Name should be less than 100 and more than 3',
      options: { max: 100, min: 1 },
    },
  },
  ['extraPayments.*.paymentNumbers']: {
    in: 'body',
    isArray: true,
    isLength: {
      errorMessage: 'You have to set at leats one payment number',
      options: { min: 1 },
    },
  },
  ['extraPayments.*.paymentNumbers.*']: {
    in: 'body',
    isNumeric: true,
  },
  ['properties']: {
    in: 'body',
    isArray: true,
  },
  ['properties.*.id']: {
    isMongoId: true,
    notEmpty: {
      errorMessage: 'Property Id cannot be empty',
    },
  },
  ['properties.*.share']: {
    isDecimal: true,
    custom: {
      errorMessage: 'Payment should be greather than 0 and maximum 100',
      options: (share) => share > 0 && share <= 100,
    },
    notEmpty: {
      errorMessage: 'Property share cannot be empty',
    },
  },
};
