import { ParamSchema } from 'express-validator';

export const SchemaCreateCreditline: Record<string, ParamSchema> = {
  name: {
    in: ['body'],
    isString: true,
    errorMessage: 'Required value.',
    isLength: {
      options: { min: 3, max: 50 },
      errorMessage: 'Name must be greater than 3 characters and less than 50.',
    },
  },
  principal: {
    in: ['body'],
    isNumeric: true,
    errorMessage: 'Required value.',
  },
  interest: {
    in: ['body'],
    isFloat: {
      options: { min: 0, max: 100 },
      errorMessage: 'Interest should be between 0 and 100.',
    },
  },
  extraPayments: {
    in: ['body'],
    optional: true,
    isArray: {
      errorMessage: 'extraPayments should be an array.',
    },
  },
  ['extraPayments.*.name']: {
    in: ['body'],
    isString: true,
    isLength: {
      options: { min: 3, max: 50 },
      errorMessage: 'Name must be greater than 3 characters and less than 50.',
    },
    errorMessage: 'Required value.',
  },
  ['extraPayments.*.date']: {
    in: ['body'],
    isISO8601: true,
    errorMessage: 'Invalid format from extra payment date.',
  },
  ['extraPayments.*.amount']: {
    in: ['body'],
    isNumeric: {
      errorMessage: 'Amount invalid value from ExtraPayment.',
    },
    errorMessage: 'Required value.',
  },
  properties: {
    in: ['body'],
    isArray: true,
    errorMessage: 'Invalid value.',
  },
  ['properties.*.id']: {
    in: ['body'],
    optional: {
      options: { checkFalsy: true },
    },
    isMongoId: true,
    errorMessage: 'The field must be a valid MongoDB ID and is required.',
  },
  ['properties.*.share']: {
    in: ['body'],
    optional: {
      options: { checkFalsy: true },
    },
    isNumeric: {
      errorMessage: 'Share should be a number.',
    },
    custom: {
      errorMessage: 'Share should be greather than 0 and maximum 100',
      options: (share) => (share > 0 && share <= 100),
    },
  },
};

export const SchemaUpdateCreditline: Record<string, ParamSchema> = {
  name: {
    in: ['body'],
    isString: true,
    errorMessage: 'Required value.',
    isLength: {
      options: { min: 3, max: 50 },
      errorMessage: 'Name must be greater than 3 characters and less than 50.',
    },
  },
  principal: {
    in: ['body'],
    isNumeric: true,
    errorMessage: 'Required value.',
  },
  interest: {
    in: ['body'],
    isFloat: {
      options: { min: 0, max: 100 },  
      errorMessage: 'Interest should be between 0 and 100.',
    },
  },
  extraPayments: {
    in: ['body'],
    optional: true,
    isArray: {
      errorMessage: 'extraPayments should be an array.',
    },
  },
  ['extraPayments.*.name']: {
    in: ['body'],
    isString: true,
    isLength: {
      options: { min: 3, max: 50 },
      errorMessage: 'Name must be greater than 3 characters and less than 50.',
    },
    errorMessage: 'Required value.',
  },
  ['extraPayments.*.date']: {
    in: ['body'],
    isISO8601: true,
    errorMessage: 'Invalid format from extra payment date.',
  },
  ['extraPayments.*.amount']: {
    in: ['body'],
    isNumeric: {
      errorMessage: 'Amount invalid value from ExtraPayment.',
    },
    errorMessage: 'Required value.',
  },
  properties: {
    in: ['body'],
    isArray: true,
    errorMessage: 'Invalid value.',
  },
  ['properties.*.id']: {
    in: ['body'],
    optional: {
      options: { checkFalsy: true },
    },
    isMongoId: true,
    errorMessage: 'The field must be a valid MongoDB ID and is required',
  },
  ['properties.*.share']: {
    in: ['body'],
    optional: {
      options: { checkFalsy: true },
    },
    isNumeric: {
      errorMessage: 'Share should be a number.',
    },
    custom: {
      errorMessage: 'Share should be greather than 0 and maximum 100',
      options: (share) => (share > 0 && share <= 100),
    },
  },
};

export const SchemaPreviewCreditline: Record<string, ParamSchema> = {
  properties: {
    in: ['body'],
    isArray: true,
    errorMessage: 'Invalid value.',
  },
  ['properties.*.id']: {
    in: ['body'],
    optional: {
      options: { checkFalsy: true },
    },
    isMongoId: true,
    errorMessage: 'The field must be a valid MongoDB ID and is required.',
  },
  ['properties.*.share']: {
    in: ['body'],
    optional: {
      options: { checkFalsy: true },
    },
    isNumeric: {
      errorMessage: 'Share should be a number.',
    },
    custom: {
      errorMessage: 'Share should be greather than 0 and maximum 100',
      options: (share) => (share > 0 && share <= 100),
    },
  },
  extraPayments: {
    in: ['body'],
    isArray: true,
    optional: true,
  },
  ['extraPayments.*.name']: {
    in: ['body'],
    isString: true,
    errorMessage: 'Required name value from extra payment.',
    isLength: {
      options: { min: 3, max: 50 },
      errorMessage: 'Name must be greater than 3 characters and less than 50.',
    },
  },
  ['extraPayments.*.date']: {
    in: ['body'],
    isISO8601: true,
    errorMessage: 'Invalid format from extra payment date.',
  },
  ['extraPayments.*.amount']: {
    isNumeric: {
      errorMessage: 'Amount invalid value from extra payment.',
    },
  },
};
