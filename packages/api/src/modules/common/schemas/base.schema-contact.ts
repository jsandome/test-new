import { ParamSchema } from 'express-validator';
import * as regExp from '../ReExp';

export const SchemaCreateContact: Record<string, ParamSchema> = {
  name: {
    in: ['body'],
    isString: true,
    errorMessage: 'name is a required value.',
    isLength: {
      options: { min: 3, max: 100 },
      errorMessage: 'Name must be greater than 3 characters and less than 100.',
    },
  },
  jobTitle: {
    in: ['body'],
    isString: true,
    errorMessage: 'jobTitle is a required value.',
    isLength: {
      options: { min: 3, max: 100 },
      errorMessage:
        'jobTitle must be greater than 3 characters and less than 100.',
    },
  },
  companyName: {
    in: ['body'],
    isString: true,
    errorMessage: 'companyName is a required value.',
    isLength: {
      options: { min: 3, max: 100 },
      errorMessage:
        'companyName must be greater than 3 characters and less than 100.',
    },
  },
  currentJob: {
    in: ['body'],
    isBoolean: true,
    errorMessage: 'currentJob is a required value.',
  },
  email: {
    in: ['body'],
    errorMessage: 'Email is wrong',
    matches: {
      options: [regExp.validEmail],
      errorMessage: "Email doesn't have a valid structure",
    },
    isLength: {
      options: { min: 3, max: 50 },
      errorMessage: 'email must be greater than 3 characters and less than 50.',
    },
  },
  phone: {
    in: ['body'],
    isString: true,
    optional: true,
    isLength: {
      options: { min: 3, max: 15 },
      errorMessage: 'phone must be greater than 3 characters and less than 15.',
    },
  },
  address: {
    in: ['body'],
    isString: true,
    optional: true,
    isLength: {
      options: { min: 3, max: 50 },
      errorMessage:
        'address must be greater than 3 characters and less than 50.',
    },
  },
  city: {
    in: ['body'],
    isString: true,
    optional: true,
    isLength: {
      options: { min: 3, max: 30 },
      errorMessage: 'city must be greater than 3 characters and less than 30.',
    },
  },
  county: {
    in: 'body',
    optional: true,
    isMongoId: {
      errorMessage: 'County id should be a mongoId',
    },
  },
  state: {
    in: 'body',
    optional: true,
    isMongoId: {
      errorMessage: 'State id should be a mongoId',
    },
  },
  zip: {
    in: ['body'],
    optional: true,
    isPostalCode: {
      options: 'US',
      errorMessage: `Zip code doesn't have a valid format.`,
    },
  },
};

export const SchemaUpdateContact: Record<string, ParamSchema> = {
  name: {
    in: ['body'],
    isString: true,
    errorMessage: 'name is a required value.',
    isLength: {
      options: { min: 3, max: 100 },
      errorMessage: 'Name must be greater than 3 characters and less than 100.',
    },
  },
  jobTitle: {
    in: ['body'],
    isString: true,
    errorMessage: 'jobTitle is a required value.',
    isLength: {
      options: { min: 3, max: 100 },
      errorMessage:
        'jobTitle must be greater than 3 characters and less than 100.',
    },
  },
  companyName: {
    in: ['body'],
    isString: true,
    errorMessage: 'companyName is a required value.',
    isLength: {
      options: { min: 3, max: 100 },
      errorMessage:
        'companyName must be greater than 3 characters and less than 100.',
    },
  },
  currentJob: {
    in: ['body'],
    isBoolean: true,
    errorMessage: 'currentJob is a required value.',
  },
  email: {
    in: ['body'],
    errorMessage: 'Email is wrong',
    matches: {
      options: [regExp.validEmail],
      errorMessage: "Email doesn't have a valid structure",
    },
    isLength: {
      options: { min: 3, max: 50 },
      errorMessage: 'email must be greater than 3 characters and less than 50.',
    },
  },
  phone: {
    in: ['body'],
    isString: true,
    optional: true,
    isLength: {
      options: { min: 3, max: 15 },
      errorMessage: 'phone must be greater than 3 characters and less than 15.',
    },
  },
  address: {
    in: ['body'],
    isString: true,
    optional: true,
    isLength: {
      options: { min: 3, max: 50 },
      errorMessage:
        'address must be greater than 3 characters and less than 50.',
    },
  },
  city: {
    in: ['body'],
    isString: true,
    optional: true,
    isLength: {
      options: { min: 3, max: 30 },
      errorMessage: 'city must be greater than 3 characters and less than 30.',
    },
  },
  county: {
    in: 'body',
    optional: true,
    isMongoId: {
      errorMessage: 'County id should be a mongoId',
    },
  },
  state: {
    in: 'body',
    optional: true,
    isMongoId: {
      errorMessage: 'State id should be a mongoId',
    },
  },
  zip: {
    in: ['body'],
    optional: true,
    isPostalCode: {
      options: 'US',
      errorMessage: `Zip code doesn't have a valid format.`,
    },
  },
};
