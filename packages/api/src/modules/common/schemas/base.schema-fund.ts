import { ParamSchema } from 'express-validator';

export const schemaCreateFund: Record<string, ParamSchema> = {
  name: {
    in: ['body'],
    errorMessage: 'name is required',
    isLength: {
      errorMessage: 'Name cannot have less than 3 characters and maximum is 15',
      options: { min: 3, max: 15 },
    },
  },
  cashHand: {
    in: ['body'],
    errorMessage: 'cashHand is invalid',
    isDecimal: {
      errorMessage: 'The cashHand must be a decimal and is required',
    },
  },
  investors: {
    in: ['body'],
    isArray: {
      errorMessage: 'The field is an array',
    },
  },
  ['investors.*.id']: {
    in: ['body'],
    isMongoId: true,
    errorMessage: 'The field must be a valid MongoDB ID and is required',
  },
  ['investors.*.minCommitment']: {
    in: ['body'],
    isDecimal: {
      errorMessage: 'The minCommitment must be a decimal and is required',
    },
  },
  ['investors.*.initCommitment']: {
    in: ['body'],
    isDecimal: {
      errorMessage: 'The initCommitment must be a decimal and is required',
    },
  },
  ['investors.*.totalCommitment']: {
    in: ['body'],
    isDecimal: {
      errorMessage: 'The totalCommitment must be a decimal and is required',
    },
  },
  ['investors.*.termYears']: {
    in: ['body'],
    isInt: {
      errorMessage: 'The termYears must be a int and is required',
    },
  },
  properties: {
    in: ['body'],
    isArray: {
      errorMessage: 'The field properties must be an array',
    },
  },
  ['properties.*.id']: {
    in: ['body'],
    optional: {
      options: { checkFalsy: true },
    },
    isMongoId: true,
    errorMessage: 'The field must be a valid MongoDB ID and is required',
  },
  ['properties.*.share']: {
    in: ['body'],
    optional: {
      options: { checkFalsy: true },
    },
    isDecimal: {
      errorMessage: 'The share must be a decimal and is required',
    },
  },
  packages: {
    in: ['body'],
    optional: true,
    isArray: {
      errorMessage: 'The field packages must be an array',
    },
  },
  ['packages.*.id']: {
    in: ['body'],
    optional: {
      options: { checkFalsy: true },
    },
    isMongoId: true,
    errorMessage: 'The field must be a valid MongoDB ID and is required',
  },
  ['packages.*.share']: {
    in: ['body'],
    optional: {
      options: { checkFalsy: true },
    },
    isDecimal: {
      errorMessage: 'The share must be a decimal and is required',
    },
  },
};

export const schemaPreviewFund: Record<string, ParamSchema> = {
  properties: {
    in: ['body'],
    isArray: {
      errorMessage: 'The field properties must be an array',
    },
  },
  ['properties.*.id']: {
    in: ['body'],
    optional: {
      options: { checkFalsy: true },
    },
    isMongoId: true,
    errorMessage: 'The field must be a valid MongoDB ID and is required',
  },
  ['properties.*.share']: {
    in: ['body'],
    optional: {
      options: { checkFalsy: true },
    },
    isDecimal: {
      errorMessage: 'The share must be a decimal and is required',
    },
  },
  packages: {
    in: ['body'],
    optional: true,
    isArray: {
      errorMessage: 'The field packages must be an array',
    },
  },
  ['packages.*.id']: {
    in: ['body'],
    optional: {
      options: { checkFalsy: true },
    },
    isMongoId: true,
    errorMessage: 'The field must be a valid MongoDB ID and is required',
  },
  ['packages.*.share']: {
    in: ['body'],
    optional: {
      options: { checkFalsy: true },
    },
    isDecimal: {
      errorMessage: 'The share must be a decimal and is required',
    },
  },
};
