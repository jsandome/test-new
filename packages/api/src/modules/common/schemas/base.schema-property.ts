import { ParamSchema } from 'express-validator';
import { EnumPropertyTypes } from '@capa/core';

export const schemaCreateProperty: Record<string, ParamSchema> = {
  name: {
    in: 'body',
    errorMessage: 'Name is required',
    isLength: {
      errorMessage: 'The minimum length is 5 characters and maximum is 150',
      options: {
        max: 150,
        min: 5,
      },
    },
    isString: {
      errorMessage: 'Name should be a string',
    },
    notEmpty: true,
  },
  type: {
    in: ['body'],
    custom: {
      errorMessage: `Type no valid, valid types: ${Object.values(
        EnumPropertyTypes
      ).toString()}`,
      options: (value) => {
        return Object.values(EnumPropertyTypes).includes(value);
      },
    },
    notEmpty: true,
  },
  state: {
    in: 'body',
    errorMessage: 'State is required',
    isMongoId: true,
    notEmpty: true,
  },
  county: {
    in: 'body',
    isMongoId: true,
    notEmpty: true,
  },
  address: {
    in: 'body',
    errorMessage: 'Address is required',
    isLength: {
      errorMessage: 'The minimum length is 5 characters and maximum is 200',
      options: {
        max: 200,
        min: 5,
      },
    },
    isString: {
      errorMessage: 'Name should be a string',
    },
    notEmpty: true,
  },
  bedRooms: {
    in: 'body',
    isNumeric: true,
    notEmpty: true,
    errorMessage: 'bedRooms is required',
  },
  bathRooms: {
    in: 'body',
    isNumeric: true,
    notEmpty: true,
    errorMessage: 'bathRooms is required',
  },
  size: {
    in: 'body',
    isNumeric: true,
    notEmpty: true,
    errorMessage: 'size is required',
  },
  parcelSize: {
    in: 'body',
    isNumeric: true,
    notEmpty: true,
    errorMessage: 'parcelSize is required',
  },
  parcelNumber: {
    in: 'body',
    isNumeric: true,
    notEmpty: true,
    errorMessage: 'parcelNumber is required',
  },
  ['strategies.buyAndHold']: {
    in: 'body',
    isBoolean: true,
    notEmpty: true,
    errorMessage: 'The strategies-buyAndHold should be a boolean',
  },
  ['strategies.repositionAndHold']: {
    in: 'body',
    isBoolean: true,
    notEmpty: true,
    errorMessage: 'The strategies-repositionAndHold should be a boolean',
  },
  ['strategies.repositionAndSell']: {
    in: 'body',
    isBoolean: true,
    notEmpty: true,
    errorMessage: 'The strategies-repositionAndSell should be a boolean',
  },
  ['strategies.developAndHold']: {
    in: 'body',
    isBoolean: true,
    notEmpty: true,
    errorMessage: 'The strategies-developAndHold should be a boolean',
  },
  ['strategies.developAndSell']: {
    in: 'body',
    isBoolean: true,
    notEmpty: true,
    errorMessage: 'The strategies-developAndSell should be a boolean',
  },
  ['strategies.inmediateSell']: {
    in: 'body',
    isBoolean: true,
    notEmpty: true,
    errorMessage: 'The strategies-inmediateSell should be a boolean',
  },
  propertyDescription: {
    isString: true,
    notEmpty: true,
    isLength: {
      errorMessage: 'The maximun length es 500 characters',
      options: {
        max: 500,
        min: 1,
      },
    },
  },
  ['pictures']: {
    isArray: true,
  },
  ['pictures.*']: {
    isString: true,
  },
  mainPicture: {
    isString: true,
  },
  //TODO find a regex to valid location object
  ['location']: {
    isObject: true,
  },
  ['location.lat']: {
    isString: true,
  },
  ['location.lng']: {
    isString: true,
  },
};

export const schemaUpdateProperty: Record<string, ParamSchema> = {
  name: {
    in: 'body',
    errorMessage: 'Name is required',
    isLength: {
      errorMessage: 'The minimum length is 5 characters and maximum is 150',
      options: {
        max: 150,
        min: 5,
      },
    },
    isString: {
      errorMessage: 'Name should be a string',
    },
    notEmpty: true,
  },
  type: {
    in: ['body'],
    custom: {
      errorMessage: `Type no valid, valid types: ${Object.values(
        EnumPropertyTypes
      ).toString()}`,
      options: (value) => {
        return Object.values(EnumPropertyTypes).includes(value);
      },
    },
    notEmpty: true,
  },
  state: {
    in: 'body',
    errorMessage: 'State is required',
    isMongoId: true,
    notEmpty: true,
  },
  county: {
    in: 'body',
    isMongoId: true,
    notEmpty: true,
  },
  address: {
    in: 'body',
    errorMessage: 'Address is required',
    isLength: {
      errorMessage: 'The minimum length is 5 characters and maximum is 200',
      options: {
        max: 200,
        min: 5,
      },
    },
    isString: {
      errorMessage: 'Name should be a string',
    },
    notEmpty: true,
  },
  bedRooms: {
    in: 'body',
    isNumeric: true,
    notEmpty: true,
    errorMessage: 'bedRooms is required',
  },
  bathRooms: {
    in: 'body',
    isNumeric: true,
    notEmpty: true,
    errorMessage: 'bathRooms is required',
  },
  size: {
    in: 'body',
    isNumeric: true,
    notEmpty: true,
    errorMessage: 'size is required',
  },
  parcelSize: {
    in: 'body',
    isNumeric: true,
    notEmpty: true,
    errorMessage: 'parcelSize is required',
  },
  parcelNumber: {
    in: 'body',
    isNumeric: true,
    notEmpty: true,
    errorMessage: 'parcelNumber is required',
  },
  ['strategies.buyAndHold']: {
    in: 'body',
    isBoolean: true,
    notEmpty: true,
    errorMessage: 'The strategies-buyAndHold should be a boolean',
  },
  ['strategies.repositionAndHold']: {
    in: 'body',
    isBoolean: true,
    notEmpty: true,
    errorMessage: 'The strategies-repositionAndHold should be a boolean',
  },
  ['strategies.repositionAndSell']: {
    in: 'body',
    isBoolean: true,
    notEmpty: true,
    errorMessage: 'The strategies-repositionAndSell should be a boolean',
  },
  ['strategies.developAndHold']: {
    in: 'body',
    isBoolean: true,
    notEmpty: true,
    errorMessage: 'The strategies-developAndHold should be a boolean',
  },
  ['strategies.developAndSell']: {
    in: 'body',
    isBoolean: true,
    notEmpty: true,
    errorMessage: 'The strategies-developAndSell should be a boolean',
  },
  ['strategies.inmediateSell']: {
    in: 'body',
    isBoolean: true,
    notEmpty: true,
    errorMessage: 'The strategies-inmediateSell should be a boolean',
  },
  propertyDescription: {
    isString: true,
    notEmpty: true,
    isLength: {
      errorMessage: 'The maximun length es 500 characters',
      options: {
        max: 500,
        min: 1,
      },
    },
  },
  newPictures: {
    in: ['body'],
    optional: true,
    isArray: true,
  },
  'newPictures.*': {
    isString: true,
  },
  deletePictures: {
    in: ['body'],
    optional: true,
    isArray: true,
  },
  'deletePictures.*': {
    isString: true,
  },
  newMainPicture: {
    in: ['body'],
    isString: true,
    optional: true,
  },
  ['location']: {
    isObject: true,
  },
  ['location.lat']: {
    isString: true,
  },
  ['location.lng']: {
    isString: true,
  },
};

export const schemaCreatePropertyFinancial: Record<string, ParamSchema> = {
  //financial
  aquisitionFee: {
    in: 'body',
    isNumeric: {
      errorMessage: 'aquisitionFee should be a number',
    },
    notEmpty: {
      errorMessage: 'aquisitionFee cannot be empty',
    },
  },
  dispositionFee: {
    in: 'body',
    isNumeric: {
      errorMessage: 'dispositionFee should be a number',
    },
    notEmpty: {
      errorMessage: 'dispositionFee cannot be empty',
    },
  },
  amountAllocatedToLand: {
    in: 'body',
    isNumeric: {
      errorMessage: 'amountAllocatedToLand should be a number',
    },
    notEmpty: {
      errorMessage: 'amountAllocatedToLand cannot be empty',
    },
  },
  depreciation: {
    in: 'body',
    isNumeric: {
      errorMessage: 'depreciation should be a number',
    },
    notEmpty: {
      errorMessage: 'depreciation cannot be empty',
    },
  },
  commissions: {
    in: 'body',
    isNumeric: {
      errorMessage: 'commissions should be a number',
    },
    notEmpty: {
      errorMessage: 'commissions cannot be empty',
    },
  },
  downPayment: {
    in: 'body',
    isNumeric: {
      errorMessage: 'downPayment should be a number',
    },
    notEmpty: {
      errorMessage: 'downPayment cannot be empty',
    },
  },
  capitalGainsTaxPaid: {
    in: 'body',
    isNumeric: {
      errorMessage: 'capitalGainsTaxPaid should be a number',
    },
    notEmpty: {
      errorMessage: 'capitalGainsTaxPaid cannot be empty',
    },
  },
  ['salesPrice.year1']: {
    in: 'body',
    isNumeric: {
      errorMessage: 'year1 should be a number',
    },
    notEmpty: {
      errorMessage: 'year1 cannot be empty',
    },
  },
  ['salesPrice.year2']: {
    in: 'body',
    isNumeric: {
      errorMessage: 'year2 should be a number',
    },
    notEmpty: {
      errorMessage: 'year2 cannot be empty',
    },
  },
  ['salesPrice.year3']: {
    in: 'body',
    isNumeric: {
      errorMessage: 'year3 should be a number',
    },
    notEmpty: {
      errorMessage: 'year3 cannot be empty',
    },
  },
  ['salesPrice.year4']: {
    in: 'body',
    isNumeric: {
      errorMessage: 'year4 should be a number',
    },
    notEmpty: {
      errorMessage: 'year4 cannot be empty',
    },
  },
  ['salesPrice.year5']: {
    in: 'body',
    isNumeric: {
      errorMessage: 'year5 should be a number',
    },
    notEmpty: {
      errorMessage: 'year5 cannot be empty',
    },
  },
  totalValue: {
    in: 'body',
    isNumeric: {
      errorMessage: 'totalValue should be a number',
    },
    notEmpty: {
      errorMessage: 'totalValue cannot be empty',
    },
  },
  initialInvestment: {
    in: 'body',
    isNumeric: {
      errorMessage: 'initialInvestment should be a number',
    },
    notEmpty: {
      errorMessage: 'initialInvestment cannot be empty',
    },
  },
  purchasePrice: {
    in: 'body',
    isNumeric: {
      errorMessage: 'purchasePrice should be a number',
    },
    notEmpty: {
      errorMessage: 'purchasePrice cannot be empty',
    },
  },
  //quarter
  year: {
    in: 'body',
    isNumeric: {
      errorMessage: 'year should be a number',
    },
    notEmpty: {
      errorMessage: 'year cannot be empty',
    },
  },
  quarter: {
    in: 'body',
    isNumeric: {
      errorMessage: 'quarter should be a number',
    },
    notEmpty: {
      errorMessage: 'quarter cannot be empty',
    },
  },
  totalUnits: {
    in: 'body',
    isNumeric: {
      errorMessage: 'totalUnits should be a number',
    },
    notEmpty: {
      errorMessage: 'totalUnits cannot be empty',
    },
  },
  ['rents.month1']: {
    in: 'body',
    isNumeric: {
      errorMessage: 'month1 should be a number',
    },
    notEmpty: {
      errorMessage: 'month1 cannot be empty',
    },
  },
  ['rents.month2']: {
    in: 'body',
    isNumeric: {
      errorMessage: 'month2 should be a number',
    },
    notEmpty: {
      errorMessage: 'month2 cannot be empty',
    },
  },
  ['rents.month3']: {
    in: 'body',
    isNumeric: {
      errorMessage: 'month3 should be a number',
    },
    notEmpty: {
      errorMessage: 'month3 cannot be empty',
    },
  },
  propertyManagement: {
    in: 'body',
    isNumeric: {
      errorMessage: 'propertyManagement should be a number',
    },
    notEmpty: {
      errorMessage: 'propertyManagement cannot be empty',
    },
  },
  assetManagement: {
    in: 'body',
    isNumeric: {
      errorMessage: 'assetManagement should be a number',
    },
    notEmpty: {
      errorMessage: 'assetManagement cannot be empty',
    },
  },
  repairsMaintenance: {
    in: 'body',
    isNumeric: {
      errorMessage: 'repairsMaintenance should be a number',
    },
    notEmpty: {
      errorMessage: 'repairsMaintenance cannot be empty',
    },
  },
  taxes: {
    in: 'body',
    isNumeric: {
      errorMessage: 'taxes should be a number',
    },
    notEmpty: {
      errorMessage: 'taxes cannot be empty',
    },
  },
  insurance: {
    in: 'body',
    isNumeric: {
      errorMessage: 'insurance should be a number',
    },
    notEmpty: {
      errorMessage: 'insurance cannot be empty',
    },
  },
  reserve: {
    in: 'body',
    isNumeric: {
      errorMessage: 'reserve should be a number',
    },
    notEmpty: {
      errorMessage: 'reserve cannot be empty',
    },
  },
  vacancy: {
    in: 'body',
    isNumeric: {
      errorMessage: 'vacancy should be a number',
    },
    notEmpty: {
      errorMessage: 'vacancy cannot be empty',
    },
  },
  ['operativeExpenses']: {
    in: 'body',
    isArray: {
      errorMessage: 'operativeExpenses Should be an array ',
    },
  },
  ['operativeExpenses.*.name']: {
    in: 'body',
    notEmpty: true,
    isString: true,
  },
  ['operativeExpenses.*.total']: {
    in: 'body',
    notEmpty: true,
    isNumeric: true,
  },
};

export const schemaPropertyProjection: Record<string, ParamSchema> = {
  ['operativeInfo']: {
    in: 'body',
    isArray: {
      errorMessage: 'operativeInfo Should be an array ',
    },
  },
  ['operativeInfo.*.year']: {
    in: 'body',
    isNumeric: {
      errorMessage: 'year should be a number',
    },
    notEmpty: {
      errorMessage: 'year cannot be empty',
    },
  },
  ['operativeInfo.*.units']: {
    in: 'body',
    isNumeric: {
      errorMessage: 'units should be a number',
    },
    notEmpty: {
      errorMessage: 'units cannot be empty',
    },
  },
  ['operativeInfo.*.rent']: {
    in: 'body',
    isDecimal: {
      errorMessage: 'rent should be a number',
    },
    notEmpty: {
      errorMessage: 'rent cannot be empty',
    },
  },
  ['operativeInfo.*.vacancy']: {
    in: 'body',
    isDecimal: {
      errorMessage: 'vacancy should be a number',
    },
    notEmpty: {
      errorMessage: 'vacancy cannot be empty',
    },
  },
  ['operativeInfo.*.propertyManagement']: {
    in: 'body',
    isDecimal: {
      errorMessage: 'propertyManagement should be a number',
    },
    notEmpty: {
      errorMessage: 'propertyManagement cannot empty',
    },
  },
  ['operativeInfo.*.assetManagement']: {
    in: 'body',
    isDecimal: {
      errorMessage: 'assetManagement should be a number',
    },
    notEmpty: {
      errorMessage: 'assetManagement be empty',
    },
  },
  ['operativeInfo.*.taxes']: {
    in: 'body',
    isDecimal: {
      errorMessage: 'taxes should be a number',
    },
    notEmpty: {
      errorMessage: 'taxes cannot be empty',
    },
  },
  ['operativeInfo.*.insurance']: {
    in: 'body',
    isDecimal: {
      errorMessage: 'insurance should be a number',
    },
    notEmpty: {
      errorMessage: 'insurance be empty',
    },
  },
  ['operativeInfo.*.repairs']: {
    in: 'body',
    isDecimal: {
      errorMessage: 'repairs should be a number',
    },
    notEmpty: {
      errorMessage: 'repairs cannot be empty',
    },
  },
  ['operativeInfo.*.reserve']: {
    in: 'body',
    isDecimal: {
      errorMessage: 'reserve should be a number',
    },
    notEmpty: {
      errorMessage: 'reserve cannot be empty',
    },
  },
  ['operativeInfo.*.salesPrice']: {
    in: 'body',
    isDecimal: {
      errorMessage: 'salesPrice should be a number',
    },
    notEmpty: {
      errorMessage: 'salesPrice be empty',
    },
  },
  ['extraExpenses']: {
    in: 'body',
    isArray: {
      errorMessage: 'extraExpenses Should be an array ',
    },
    optional: true,
  },
  ['extraExpenses.*.name']: {
    in: 'body',
    isString: true,
  },
  ['extraExpenses.*.years']: {
    in: 'body',
    isArray: {
      errorMessage: 'extraExpenses Should be an array ',
    },
  },
  ['extraExpenses.*.years.*.year']: {
    in: 'body',
    isNumeric: true,
  },
  ['extraExpenses.*.years.*.amount']: {
    in: 'body',
    isNumeric: true,
  },
  aquisitionFee: {
    in: 'body',
    isNumeric: true,
    notEmpty: true,
    errorMessage: 'aquisitionFee is a number and cannot be empty',
  },
  dispositionFee: {
    in: 'body',
    isNumeric: true,
    notEmpty: true,
    errorMessage: 'dispositionFee is a number and cannot be empty',
  },
  amountAllocatedToLand: {
    in: 'body',
    isNumeric: true,
    notEmpty: true,
    errorMessage: 'amountAllocatedToLand is a number and cannot be empty',
  },
  depreciation: {
    in: 'body',
    isNumeric: true,
    notEmpty: true,
    errorMessage: 'depreciation is a number and cannot be empty',
  },
  commissions: {
    in: 'body',
    isNumeric: true,
    notEmpty: true,
    errorMessage: 'commissions is a number and cannot be empty',
  },
  downPayment: {
    in: 'body',
    isNumeric: true,
    notEmpty: true,
    errorMessage: 'downPayment is a number and cannot be empty',
  },
  purchasePrice: {
    in: 'body',
    isNumeric: true,
    notEmpty: true,
    errorMessage: 'purchasePrice is a number and cannot be empty',
  },
  totalValue: {
    in: 'body',
    isNumeric: true,
    notEmpty: true,
    errorMessage: 'totalValue is a number and cannot be empty',
  },
  capitalGainsTaxPaid: {
    in: 'body',
    isNumeric: true,
    notEmpty: true,
    errorMessage: 'capitalGainsTaxPaid is a number and cannot be empty',
  },
  ['loans']: {
    in: 'body',
    isArray: {
      errorMessage: 'loans Should be an array ',
    },
  },
  ['loans.*.amount']: {
    in: 'body',
    isDecimal: {
      errorMessage: 'loan amount should be a number',
    },
    notEmpty: {
      errorMessage: 'loan amount salesPrice be empty',
    },
  },
  ['loans.*.interest']: {
    in: 'body',
    isDecimal: {
      errorMessage: 'loan interest should be a number',
    },
    notEmpty: {
      errorMessage: 'loan interest salesPrice be empty',
    },
  },
  ['loans.*.termYears']: {
    in: 'body',
    isDecimal: {
      errorMessage: 'loan termYears should be a number',
    },
    notEmpty: {
      errorMessage: 'loan termYears salesPrice be empty',
    },
  },
  ['loans.*.paymentsPerYear']: {
    in: 'body',
    isDecimal: {
      errorMessage: 'loan paymentsPerYear should be a number',
    },
    optional: true,
  },
  ['loans.*.amortizationYears']: {
    in: 'body',
    isDecimal: {
      errorMessage: 'loan amortizationYears should be a number',
    },
    notEmpty: {
      errorMessage: 'loan amortizationYears salesPrice be empty',
    },
  },
  ['loans.*.ltc']: {
    in: 'body',
    isDecimal: {
      errorMessage: 'loan ltc should be a number',
    },
    optional: true,
  },
};

export const schemaPropertyPreviewByExcel: Record<string, ParamSchema> = {
  pathFile: {
    in: ['query', 'body'],
    isString: true,
    notEmpty: true,
    errorMessage: 'Path File cannot be empty',
  },
};
