import { inject } from 'inversify';
import {
  BaseHttpController,
  httpPost,
  requestBody,
  httpGet,
  queryParam,
  httpPut,
  requestParam,
  httpDelete,
} from 'inversify-express-utils';
import {
  BaseUseCaseFound,
  IParamsEntityFunds,
  IParamsPreviewFund,
} from '@capa/core';
import { COMMON } from '../../../config/types-common';
import { schemaCreateFund, schemaPreviewFund } from '../schemas';
import { checkSchema } from 'express-validator';
import {
  IDtoGetFunds,
  IDtoCreateNewFund,
  IDtoUpdatedFund,
  IDtoDeletedFund,
  IDtoGetAFund,
  IDtoPreviewFund,
} from '.';

export class BaseFundController extends BaseHttpController {
  @inject('BaseUseCaseFound') private ucFund: BaseUseCaseFound;
  constructor() {
    super();
  }

  @httpPost(
    '/admin/fund',
    ...checkSchema(schemaCreateFund),
    COMMON.midParameters,
    COMMON.midValidToken
  )
  public async createNewFund(
    @requestBody() fund: IParamsEntityFunds
  ): Promise<IDtoCreateNewFund> {
    const response = await this.ucFund.execCreateNewFund(fund);
    return {
      response,
      message: 'Fund created!',
      details: `You can now view the created Fund`,
    };
  }

  @httpPost(
    `/admin/fund/preview`,
    ...checkSchema(schemaPreviewFund),
    COMMON.midParameters,
    COMMON.midValidToken
  )
  public async previewFund(
    @requestBody() params: IParamsPreviewFund
  ): Promise<IDtoPreviewFund> {
    const response = await this.ucFund.execPreviewFund(params);
    return { response };
  }

  @httpGet(`/fund/select`, COMMON.midValidToken)
  public async searchFund(
    @queryParam('search') search?: string
  ): Promise<IDtoGetFunds> {
    const response = await this.ucFund.searchFund(search);
    return { response };
  }

  @httpGet(`/admin/fund/:fundId`, COMMON.midParameters, COMMON.midValidToken)
  public async getFund(
    @requestParam('fundId') fundId: string
  ): Promise<IDtoGetAFund> {
    const response = await this.ucFund.execGetFund(fundId);
    return { response };
  }

  @httpPut(
    '/admin/fund/:fundId',
    ...checkSchema(schemaCreateFund),
    COMMON.midParameters,
    COMMON.midValidToken
  )
  public async udpatedFund(
    @requestParam('fundId') fundId: string,
    @requestBody() updateFund: IParamsEntityFunds
  ): Promise<IDtoUpdatedFund> {
    await this.ucFund.updateFund(fundId, updateFund);
    return { response: {}, message: 'Fund has been updated succesfully.' };
  }

  @httpDelete('/admin/fund/:fundId', COMMON.midValidToken)
  public async deleteFund(
    @requestParam('fundId') fundId: string
  ): Promise<IDtoDeletedFund> {
    await this.ucFund.deleteFund(fundId);
    return { response: {}, message: 'Fund has been deleted succesfully.' };
  }
}
