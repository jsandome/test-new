import {
  IBaseEntityFunds,
  IBaseReturnDetailFund,
  IBndReturnCreateFund,
  IBndReturnPreviewFund,
} from '@capa/core';
import { IDtoBaseOutput } from '..';

export interface IDtoCreateNewFund
  extends IDtoBaseOutput<IBndReturnCreateFund> {}
export interface IDtoPreviewFund
  extends IDtoBaseOutput<IBndReturnPreviewFund> {}
export interface IDtoGetFunds extends IDtoBaseOutput<IBaseEntityFunds[]> {}
export interface IDtoGetAFund extends IDtoBaseOutput<IBaseReturnDetailFund> {}
export interface IDtoDeleteFund extends IDtoBaseOutput<string> {}
export interface IDtoUpdatedFund extends IDtoBaseOutput<IBndReturnCreateFund> {}
export interface IDtoDeletedFund extends IDtoBaseOutput<IBndReturnCreateFund> {}
