export enum ErrorCode {
  Unauthorized = 'unauthorized',
  UserInvalidPassword = 'userinvalidpassword',
  DuplicateMail = 'duplicatemail',
  UserNotFound = 'usernotfound',
  EndpointNotFound = 'endpointnotfound',
  ValidationError = 'validationerror',
  UnreachableExternalAPI = 'unreachableexternalapi',
  InternalServerError = 'internalservererror',
}

export interface IErrorCodeObject {
  code?: number;
  label: ErrorCode;
  message?: string;
  status: 400 | 401 | 403 | 404 | 500;
}

export class ApiErrors<T> extends Error {
  errorCodeObject: IErrorCodeObject;
  data: T;

  constructor(code: ErrorCode, data: T) {
    if (!errorMap.has(code)) {
      super('Invalid error code throw');
    } else {
      const errorCodeObject = errorMap.get(code);
      super(errorCodeObject.message);
      this.errorCodeObject = errorCodeObject;
      this.data = data;
    }
  }
}

const errorMap: Map<ErrorCode, IErrorCodeObject> = new Map<
  ErrorCode,
  IErrorCodeObject
>();
