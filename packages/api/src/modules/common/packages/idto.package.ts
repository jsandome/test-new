import { IDtoBaseOutput } from '..';
import {
  IBndReturnCreatePackage,
  IBndReturnUpdatePackage,
  IBaseEntityPackage,
  IBndReturnDetailPackage,
} from '@capa/core';

export interface IDResponseCreatePackage
  extends IDtoBaseOutput<IBndReturnCreatePackage> {}
export interface IDResponseUpdatePackage
  extends IDtoBaseOutput<IBndReturnUpdatePackage> {}
export interface IDResponseDeletePackage {}
export interface IDResponsePreviewPackage {}
export interface IDResponseSelectPackages
  extends IDtoBaseOutput<IBaseEntityPackage[]> {}
export interface IDResponseGetPackage
  extends IDtoBaseOutput<IBndReturnDetailPackage> {}
