import { inject } from 'inversify';
import {
  BaseHttpController,
  httpDelete,
  httpGet,
  httpPost,
  httpPut,
  queryParam,
  requestBody,
  requestParam,
} from 'inversify-express-utils';
import {
  BaseUseCasePackage,
  IParamsCreatePackage,
  IParamsUpdatePackage,
} from '@capa/core';
import {
  IDResponseCreatePackage,
  IDResponseDeletePackage,
  IDResponseUpdatePackage,
  IDResponseSelectPackages,
  IDResponseGetPackage,
} from '.';
import { COMMON } from '../../../config/types-common';
import {
  SchemaCreatePackage,
  SchemaPreviewPackage,
  SchemaUpdatePackage,
} from '../schemas';
import { checkSchema } from 'express-validator';

export class BasePackageController extends BaseHttpController {
  @inject('BaseUseCasePackage') private ucPackage: BaseUseCasePackage;

  constructor() {
    super();
  }

  @httpPost(
    '/admin/package',
    ...checkSchema(SchemaCreatePackage),
    COMMON.midParameters,
    COMMON.midValidToken
  )
  public async createPackage(
    @requestBody() params: IParamsCreatePackage
  ): Promise<IDResponseCreatePackage> {
    const response = await this.ucPackage.execCreatePackage(params);

    return {
      response,
      message: 'Package created!',
      details: `You can now view the created Package`,
    };
  }

  @httpPost(
    '/admin/package/preview',
    ...checkSchema(SchemaPreviewPackage),
    COMMON.midParameters,
    COMMON.midValidToken
  )
  public async previewPackage(
    @requestBody() params: IParamsUpdatePackage
  ): Promise<IDResponseDeletePackage> {
    const response = await this.ucPackage.execPreviewPackage(params);

    return {
      response,
    };
  }

  @httpGet('/package/select', COMMON.midValidToken)
  public async selectPackage(
    @queryParam('search') search: string
  ): Promise<IDResponseSelectPackages> {
    const response = await this.ucPackage.execSelectPackages(search);

    return {
      response,
    };
  }

  @httpGet(
    '/admin/package/:packageId',
    COMMON.midValidToken
  )
  public async getPackage(
    @requestParam('packageId') packageId: string,
  ): Promise<IDResponseGetPackage> {
    const response = await this.ucPackage.execGetPackage(packageId);
    return { response }
  }

  @httpPut(
    '/admin/package/:package',
    ...checkSchema(SchemaUpdatePackage),
    COMMON.midParameters,
    COMMON.midValidToken
  )
  public async updatePackage(
    @requestParam('package') packageId: string,
    @requestBody() params: IParamsUpdatePackage
  ): Promise<IDResponseUpdatePackage> {
    const response = await this.ucPackage.execUpdatePackage(packageId, params);

    return {
      response,
      message: 'Package has been updated succesfully.',
    };
  }

  @httpDelete('/admin/package/:package', COMMON.midValidToken)
  public async deletePackage(
    @requestParam('package') packageId: string
  ): Promise<IDResponseDeletePackage> {
    await this.ucPackage.execDeletePackage(packageId);

    return {
      response: {},
      message: 'Package has been deleted succesfully.',
    };
  }
}
