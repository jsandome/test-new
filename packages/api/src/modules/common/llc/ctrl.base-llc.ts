import { inject } from 'inversify';
import {
  BaseHttpController,
  httpPost,
  requestBody,
  httpGet,
  queryParam,
  requestParam,
  httpDelete,
  httpPut,
} from 'inversify-express-utils';
import { BaseUseCaseLlc, IBaseEntityLlc, IPaginateParams } from '@capa/core';
import { checkSchema } from 'express-validator';
import { schemaCreateLlc } from '../schemas';
import { COMMON } from '../../../config/types-common';
import {
  IDtoCreateNewLlc,
  IDtoGetLlcs,
  IDtoDeleteLLC,
  IDtoUpdateLLC,
  IDtoListLLC,
  IDtoDetailsOverviewLLC,
  IDtoGetDetailsLlc,
  IDtoPreviewLlc,
} from './';

export class BaseLlcController extends BaseHttpController {
  @inject('BaseUseCaseLlc') private ucLlc: BaseUseCaseLlc;
  constructor() {
    super();
  }
  @httpPost(
    '/admin/llc',
    ...checkSchema(schemaCreateLlc),
    COMMON.midParameters,
    COMMON.midValidToken
  )
  public async createNewLlc(
    @requestBody() newLlc: IBaseEntityLlc
  ): Promise<IDtoCreateNewLlc> {
    const response = await this.ucLlc.createNewLlc(newLlc);
    return {
      response,
      message: 'Entity created!',
      details: `You can now view the created Entity`,
    };
  }

  @httpGet(`/llc/select`, COMMON.midValidToken)
  public async searchLlcs(
    @queryParam('search') search?: string
  ): Promise<IDtoGetLlcs> {
    const response = await this.ucLlc.searchLlcs(search);
    return { response };
  }

  @httpDelete('/admin/llc/:llcId', COMMON.midValidToken)
  public async deletedFund(
    @requestParam('llcId') llcId: string
  ): Promise<IDtoDeleteLLC> {
    await this.ucLlc.deleteLlc(llcId);
    return { response: null, message: 'Entity has been deleted succesfully.' };
  }

  @httpPut(
    '/admin/llc/:llcId',
    ...checkSchema(schemaCreateLlc),
    COMMON.midParameters,
    COMMON.midValidToken
  )
  public async updatedLlc(
    @requestParam('llcId') llcId: string,
    @requestBody() updatedLlc: IBaseEntityLlc
  ): Promise<IDtoUpdateLLC> {
    const response = await this.ucLlc.updateLlc(llcId, updatedLlc);
    return { response, message: 'Entity has been updated succesfully.' };
  }
  @httpGet('/admin/llc/list', COMMON.midValidToken)
  public async llcList(
    @queryParam('limit') limit?: number,
    @queryParam('page') page?: number
  ): Promise<IDtoListLLC> {
    const pagination: IPaginateParams = {
      limit,
      page,
    };
    const response = await this.ucLlc.llcList(pagination);
    return { response };
  }

  @httpGet('/admin/llc/:llcId/overview', COMMON.midValidToken)
  public async detailsOverview(
    @requestParam('llcId') llcId: string
  ): Promise<IDtoDetailsOverviewLLC> {
    const response = await this.ucLlc.detailsOverview(llcId);
    return { response };
  }

  @httpGet('/admin/llc/:llcId', COMMON.midValidToken)
  public async getDetails(
    @requestParam('llcId') llcId?: string
  ): Promise<IDtoGetDetailsLlc> {
    const response = await this.ucLlc.getDetails(llcId);
    return { response };
  }

  // TODO: Implement the request params llcId and then check the activity for each llc
  @httpGet('/admin/llc/:llcId/preview', COMMON.midValidToken)
  public async previewLlc(
    @requestParam('llcId') llcId: string
  ): Promise<IDtoPreviewLlc> {
    const response = await this.ucLlc.previewLlc(llcId);
    return { response };
  }
}
