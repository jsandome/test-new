import {
  IBndReturnLlc,
  IBaseEntityGetLlc,
  IBaseResponseList,
  IBaseResponseOverviewDetails,
  IBaseGetDetails,
  IBaseResponsePreviewLLC,
} from '@capa/core';
import { IDtoBaseOutput } from '..';

export interface IDtoCreateNewLlc extends IDtoBaseOutput<IBndReturnLlc> {}
export interface IDtoGetLlcs extends IDtoBaseOutput<IBaseEntityGetLlc[]> {}
export interface IDtoDeleteLLC extends IDtoBaseOutput<void> {}
export interface IDtoUpdateLLC extends IDtoBaseOutput<IBndReturnLlc> {}
export interface IDtoListLLC extends IDtoBaseOutput<IBaseResponseList> {}
export interface IDtoDetailsOverviewLLC
  extends IDtoBaseOutput<IBaseResponseOverviewDetails> {}

export interface IDtoGetDetailsLlc extends IDtoBaseOutput<IBaseGetDetails> {}

export interface IDtoPreviewLlc extends IDtoBaseOutput<IBaseResponsePreviewLLC> {}
