import { inject } from 'inversify';
import { checkSchema } from 'express-validator';
import {
  BaseUseCaseGCloudDocuments,
  IGCloudDocumentCreateParams,
  IGCloudDocumentsQueryParams,
} from '@capa/core';
import {
  BaseHttpController,
  httpDelete,
  httpGet,
  httpPost,
  queryParam,
  requestBody,
  requestParam,
} from 'inversify-express-utils';
import {
  saveGCloudDocument,
  listGCloudDocuments,
  deleteGCloudDocument,
  selectGCloudDocument,
} from './';
import { COMMON } from '../../../../config/types-common';
import {
  createGCloudDocumentSchema,
  deleteGCloudDocumentSchema,
  listGCloudDocumentsSchema,
  selectGCloudDocumentSchema,
} from '../../schemas';
export class BaseGCloudDocumentsController extends BaseHttpController {
  @inject('BaseUseCaseGCloudDocuments')
  private ucGCloudDocuments: BaseUseCaseGCloudDocuments;

  constructor() {
    super();
  }

  @httpPost(
    '/admin/document',
    ...checkSchema(createGCloudDocumentSchema),
    COMMON.midParameters,
    COMMON.midValidToken
  )
  public async createGCloudDocument(
    @requestBody() params: IGCloudDocumentCreateParams
  ): Promise<saveGCloudDocument> {
    const response = await this.ucGCloudDocuments.execCreateGCloudDocument(
      params
    );
    return {
      response: response,
      message: 'Document has been uploaded succesfully.',
    };
  }

  @httpGet(
    '/admin/document/list',
    ...checkSchema(listGCloudDocumentsSchema),
    COMMON.midParameters,
    COMMON.midValidToken
  )
  public async getListOfDocuments(
    @queryParam('limit') limit?: number,
    @queryParam('page') page?: number,
    @queryParam('search') search?: string
  ): Promise<listGCloudDocuments> {
    const queryParams: IGCloudDocumentsQueryParams = {
      limit: limit ? limit : 10,
      page: page ? page : 1,
      search: search ? search : null,
    };
    const response = await this.ucGCloudDocuments.execGetListGCloudDocuments(
      queryParams
    );
    return { response };
  }

  @httpGet(
    '/admin/document/select',
    ...checkSchema(selectGCloudDocumentSchema),
    COMMON.midParameters,
    COMMON.midValidToken
  )
  public async selectGCloudDocument(
    @queryParam('search') search: string
  ): Promise<selectGCloudDocument> {
    const response = await this.ucGCloudDocuments.execSelectGCloudDocument(
      search
    );
    return { response };
  }

  @httpDelete(
    '/admin/document/:idDocument',
    ...checkSchema(deleteGCloudDocumentSchema),
    COMMON.midParameters,
    COMMON.midValidToken
  )
  public async deleteGCloudDocument(
    @requestParam('idDocument') documentId: string
  ): Promise<deleteGCloudDocument> {
    await this.ucGCloudDocuments.execDeleteGCloudDocument(documentId);
    return {
      response: {},
      message: 'Document has been deleted succesfully.',
    };
  }
}
