import {
  IGCloudDocumentCreateResponse,
  IGCloudDocumentList,
  IGCloudDocumentSelectResponse,
} from '@capa/core';
import { IDtoBaseOutput } from '../../';

export interface saveGCloudDocument
  extends IDtoBaseOutput<IGCloudDocumentCreateResponse> {}

export interface listGCloudDocuments
  extends IDtoBaseOutput<IGCloudDocumentList> {}

export interface deleteGCloudDocument extends IDtoBaseOutput<{}> {}

export interface selectGCloudDocument
  extends IDtoBaseOutput<IGCloudDocumentSelectResponse[]> {}
