import { inject } from 'inversify';
import {
  BaseHttpController,
  httpGet,
  httpPost,
  requestBody,
} from 'inversify-express-utils';
import { BaseUseCaseDocuSign, UserInfoDocusign } from '@capa/core';
import { IDtoListTemplatesDocuSign } from './';
import { COMMON } from '../../../../config/types-common';

export class DocuSignController extends BaseHttpController {
  @inject('BaseUseCaseDocuSign') private ucDocuSign: BaseUseCaseDocuSign;
  constructor() {
    super();
  }

  @httpGet('/admin/document/docusign/list', COMMON.midValidToken)
  public async listTemplates(): Promise<IDtoListTemplatesDocuSign> {
    const response = await this.ucDocuSign.listTemplates();
    return { response };
  }

  @httpGet('/admin/document/docusign/get-envelope', COMMON.midValidToken)
  public async getEnvelope(): Promise<void> {
    const response = await this.ucDocuSign.getEnvelope();
    return response;
  }

  @httpPost('/admin/document/docusign/send-envelope')
  public async sendEnvelope(
    @requestBody() userInfo: UserInfoDocusign
  ): Promise<void> {
    const response = await this.ucDocuSign.sendEnvelope(userInfo);
    return response;
  }
}
