import { IDtoBaseOutput } from '../../';
import { ListTemplates } from '@capa/core';

export interface IDtoListTemplatesDocuSign
  extends IDtoBaseOutput<ListTemplates[]> {}
