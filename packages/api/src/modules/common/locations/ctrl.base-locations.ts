import { inject } from 'inversify';
import {
    BaseHttpController,
    httpGet,
    requestParam,
    queryParam
} from 'inversify-express-utils';
import {
    BaseUseCaseLocations
} from '@capa/core';
import { IDtoGetCountries,IDtoGetStates,IDtoGetCounties } from '.';
export class BaseLocationsController extends BaseHttpController {
    @inject('BaseUseCaseLocations') private ucLocations: BaseUseCaseLocations;

    constructor() {
        super();
    }

    @httpGet('/country')
    public async getCountries( @queryParam('search') search: string): Promise<IDtoGetCountries> {
        const response = await this.ucLocations.execGetCountries(search ? search:null);
        return {
            response
        };
    }

    @httpGet('/state')
    public async getStates(@queryParam('search') search: string): Promise<IDtoGetStates> {
        const response = await this.ucLocations.execGetStates(search ? search:null);
        return {
            response
        };
    }
    @httpGet('/state/:stateId/county')
    public async getCounties(@requestParam('stateId') stateId: string, @queryParam('search') search: string): Promise<IDtoGetCounties> {
        const response = await this.ucLocations.execGetCounties(stateId,search ? search : null);
        return {
            response
        };
    }
}
