import { IBaseEntityCounties, IBaseEntityCountries, IBaseEntityStates} from '@capa/core';
import { IDtoBaseOutput } from '..';

export interface IDtoGetCountries extends IDtoBaseOutput<IBaseEntityCountries[]> {}
export interface IDtoGetStates extends IDtoBaseOutput<IBaseEntityStates[]> {}
export interface IDtoGetCounties extends IDtoBaseOutput<IBaseEntityCounties[]> {}


