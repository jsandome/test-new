import { IDtoBaseOutput } from '..';
import {
  IBndReturnCreateContribution,
  IBndReturnDeleteContribution,
  IBndReturnGetContribution,
  IBndReturnListContribution,
  IBndReturnRecordContribution,
  IBndReturnUpdateContribution,
} from '@capa/core';

export interface IDResponseCreateContribution
  extends IDtoBaseOutput<IBndReturnCreateContribution> {}
export interface IDResponseRecordContribution
  extends IDtoBaseOutput<IBndReturnRecordContribution> {}
export interface IDResponseListContribution
  extends IDtoBaseOutput<IBndReturnListContribution> {}
export interface IDResponseGetContribution
  extends IDtoBaseOutput<IBndReturnGetContribution> {}
export interface IDResponseUpdateContribution
  extends IDtoBaseOutput<IBndReturnUpdateContribution> {}
export interface IDResponseDeleteContribution
  extends IDtoBaseOutput<IBndReturnDeleteContribution> {}
