import { inject } from 'inversify';
import {
  BaseUseCaseContribution,
  IPaginateParams,
  IParamsCreateContribution,
  IParamsRecordContribution,
  IParamsUpdateContribution,
} from '@capa/core';
import {
  BaseHttpController,
  httpDelete,
  httpGet,
  httpPost,
  httpPut,
  queryParam,
  request,
  requestBody,
  requestParam,
} from 'inversify-express-utils';
import { COMMON } from '../../../config/types-common';
import { checkSchema } from 'express-validator';
import {
  SchemaCreateContribution,
  SchemaListContribution,
  SchemaRecordContribution,
  SchemaUpdateContribution,
} from '../schemas';
import {
  IDResponseCreateContribution,
  IDResponseDeleteContribution,
  IDResponseGetContribution,
  IDResponseListContribution,
  IDResponseRecordContribution,
  IDResponseUpdateContribution,
} from '.';
import { Request } from 'express';

export class BaseContributionController extends BaseHttpController {
  @inject('BaseUseCaseContribution')
  private ucContribution: BaseUseCaseContribution;

  constructor() {
    super();
  }

  @httpPost(
    '/admin/llc/:llcId/contribution',
    ...checkSchema(SchemaCreateContribution),
    COMMON.midParameters,
    COMMON.midValidToken
  )
  public async createContribution(
    @requestBody() params: IParamsCreateContribution,
    @requestParam('llcId') llcId: string
  ): Promise<IDResponseCreateContribution> {
    const response = await this.ucContribution.execCreateContribution(
      llcId,
      params
    );
    return {
      response,
      message: 'Contribution has been created successfully!',
    };
  }

  @httpPost(
    '/admin/llc/:llcId/capital-call/:capitalCallId/contribution',
    ...checkSchema(SchemaRecordContribution),
    COMMON.midParameters,
    COMMON.midValidToken
  )
  public async recordContribution(
    @requestBody() params: IParamsRecordContribution,
    @requestParam('llcId') llcId: string,
    @requestParam('capitalCallId') capitalCallId: string
  ): Promise<IDResponseRecordContribution> {
    const response = await this.ucContribution.execRecordContribution(
      llcId,
      capitalCallId,
      params
    );
    return {
      response,
      message: 'Contribution has been recorded.!',
    };
  }

  @httpGet(
    '/admin/llc/:llcId/transactions/contribution',
    ...checkSchema(SchemaListContribution),
    COMMON.midParameters,
    COMMON.midValidToken
  )
  public async listContribution(
    @requestParam('llcId') llcId: string,
    @queryParam('search') search: string,
    @queryParam('page') page: number,
    @queryParam('limit') limit: number
  ): Promise<IDResponseListContribution> {
    const pagination: IPaginateParams = {
      limit,
      page,
    };
    const response = await this.ucContribution.execListContribution(
      llcId,
      pagination,
      search
    );
    return { response };
  }

  @httpGet(
    '/admin/llc/:llcId/contribution/:contributionId',
    COMMON.midParameters,
    COMMON.midValidToken
  )
  public async getContribution(
    @requestParam('llcId') llcId: string,
    @requestParam('contributionId') contributionId: string
  ): Promise<IDResponseGetContribution> {
    const response = await this.ucContribution.execGetContribution(
      llcId,
      contributionId
    );

    return { response };
  }

  @httpPut(
    '/admin/llc/:llcId/contribution/:contributionId',
    ...checkSchema(SchemaUpdateContribution),
    COMMON.midParameters,
    COMMON.midValidToken
  )
  public async updateContribution(
    @requestBody() params: IParamsUpdateContribution,
    @requestParam('llcId') llcId: string,
    @requestParam('contributionId') contributionId: string
  ): Promise<IDResponseUpdateContribution> {
    const response = await this.ucContribution.execUpdateContribution(
      llcId,
      contributionId,
      params
    );
    return {
      response,
      message: 'Contribution has been updated successfully!',
    };
  }

  @httpDelete(
    '/admin/llc/:llcId/contribution/:contributionId',
    COMMON.midParameters,
    COMMON.midValidToken
  )
  public async deleteContribution(
    @requestParam('llcId') llcId: string,
    @requestParam('contributionId') contributionId: string,
    @request() req: Request
  ): Promise<IDResponseDeleteContribution> {
    await this.ucContribution.execDeleteContribution(
      llcId,
      contributionId,
      req.userCapa.id
    );
    return {
      response: {},
      message: 'Ready!',
      details: 'Contribution deleted succesfully!',
    };
  }
}
