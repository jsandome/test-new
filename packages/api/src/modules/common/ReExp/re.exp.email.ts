// Regex to validate email structure
export const validEmail = /^[^\s@]+@[^\s.@]+(\.[^\s.@]+)+$/;
