//Minimum four characters, at least one letter and one number
export const weakPassword = /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{4,}$/;

//Minimum eight characters, at least one uppercase letter, one lowercase letter, one number and one special character
export const strongPassword =
  /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/;

export const customPassword = /^((?=.*\d)(?=.*[A-Z])(?=.*[\W_]).{8,32})$/;
