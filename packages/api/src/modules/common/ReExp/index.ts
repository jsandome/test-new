export * from './re.exp.coordenates';
export * from './re.exp.email';
export * from './re.exp.general';
export * from './re.exp.mongoose-id';
export * from './re.exp.password';
export * from './re.exp.secure';
export * from './re.exp.url';
