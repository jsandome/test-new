export const alphaNumeric = /^([a-zA-Z0-9-.]{0,40})$/;

// Ex: January 2021
// Ex: February 2021
export const yieldPeriod =
  /\b(?:Jan(?:uary)?|Feb(?:ruary)?|March|April|May|June|July|August|September|October|Nov(?:ember)|Dec(?:ember)?) (?:2\d{3})(?=\D|$)/;
