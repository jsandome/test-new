import {
  IBndResponseCreateOffering,
  IBndResponseSelectInvestors,
  IBndResponseUpdateOffering,
  IBndReturnListInvestors,
} from '@capa/core';
import { IDtoBaseOutput } from '..';

export interface IDResponseCreateOfferingProspective
  extends IDtoBaseOutput<IBndResponseCreateOffering> {}
export interface IDResponseSelectInvestors
  extends IDtoBaseOutput<IBndResponseSelectInvestors[]> {}
export interface IDResponseListInvestors
  extends IDtoBaseOutput<IBndReturnListInvestors> {}
export interface IDResponseUpdateOfferingProspective
  extends IDtoBaseOutput<IBndResponseUpdateOffering> {}
