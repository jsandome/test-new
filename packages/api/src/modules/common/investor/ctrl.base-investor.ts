import { inject } from 'inversify';
import {
  BaseHttpController,
  httpGet,
  httpPatch,
  httpPost,
  httpPut,
  queryParam,
  requestBody,
  requestParam,
} from 'inversify-express-utils';
import { COMMON } from '../../../config/types-common';
import { checkSchema } from 'express-validator';
import {
  SchemaCreateOfferingProspective,
  SchemaUpdateOfferingProspective,
  SchemaUpdateInvestor,
} from '../schemas';
import {
  BaseUseCaseInvestor,
  IPaginateParams,
  IParamsCreateOfferingProspective,
  IParamsUpdateInvestor,
} from '@capa/core';
import {
  IDResponseCreateOfferingProspective,
  IDResponseListInvestors,
  IDResponseSelectInvestors,
  IDResponseUpdateOfferingProspective,
} from '.';

export class BaseInvestorController extends BaseHttpController {
  @inject('BaseUseCaseInvestor') private ucInvestor: BaseUseCaseInvestor;

  constructor() {
    super();
  }

  @httpPost(
    '/admin/investor/:investorId/prospective',
    ...checkSchema(SchemaCreateOfferingProspective),
    COMMON.midParameters,
    COMMON.midValidToken
  )
  public async createOfferingProspective(
    @requestParam('investorId') investorId: string,
    @requestBody() params: IParamsCreateOfferingProspective
  ): Promise<IDResponseCreateOfferingProspective> {
    const response = await this.ucInvestor.execCreateOfferingProspective(
      investorId,
      params
    );
    return {
      response,
      message: 'Prospective has been added succesfully.',
    };
  }

  @httpGet('/admin/investor/select', COMMON.midValidToken)
  public async selectInvestors(
    @queryParam('search') search?: string,
    @queryParam('llcId') llcId?: string
  ): Promise<IDResponseSelectInvestors> {
    const response = await this.ucInvestor.execSelectInvestors(search, llcId);
    return { response };
  }

  @httpGet('/admin/investor/list', COMMON.midValidToken)
  public async listInvestors(
    @queryParam('page') page: number,
    @queryParam('limit') limit: number
  ): Promise<IDResponseListInvestors> {
    const pagination: IPaginateParams = {
      limit,
      page,
    };
    const response = await this.ucInvestor.execListInvestors(pagination);
    return { response };
  }

  @httpPatch(
    '/admin/investor/:investorId/prospective/:prospectiveId',
    ...checkSchema(SchemaUpdateOfferingProspective),
    COMMON.midParameters,
    COMMON.midValidToken
  )
  public async updateOfferingProspective(
    @requestBody() params: IParamsCreateOfferingProspective,
    @requestParam('investorId') investorId: string,
    @requestParam('prospectiveId') prospectiveId: string
  ): Promise<IDResponseUpdateOfferingProspective> {
    await this.ucInvestor.execUpdateOfferingProspective(
      investorId,
      prospectiveId,
      params
    );

    return {
      response: {},
      message: 'Prospective investor has been updated succesfully',
    };
  }

  @httpPut(
    '/admin/investor/:investorId/general',
    ...checkSchema(SchemaUpdateInvestor),
    COMMON.midParameters,
    COMMON.midValidToken
  )
  public async updateInvestor(
    @requestBody() params: IParamsUpdateInvestor,
    @requestParam('investorId') investorId: string
  ): Promise<IDResponseUpdateOfferingProspective> {
    await this.ucInvestor.execUpdateInvestor(investorId, params);

    return {
      response: {},
      message: 'Investor has been updated succesfully.',
    };
  }
}
