import { ContainerModule, interfaces } from 'inversify';
import { controller } from 'inversify-express-utils';

import {
  BaseUseCaseUser,
  IBndBaseUserRead,
  IBndBaseTokenInfra,
  IBndBaseUserWrite,
  IBndBaseGCloudInfra,
  IBndMailerInfra,
  BaseUseCaseFound,
  IBndBaseFundWrite,
  IBndBaseFundRead,
  IBndValidShareData,
  IBndShareMathFund,
} from '@capa/core';
import {
  ImplBndBaseUserReadMongo,
  ImplBndBaseUserWriteMongo,
  ImplBndBaseFundReadMongo,
  ImplBndBaseFundWriteMongo,
  ImplBndBaseShareMathFund,
} from '@capa/data';
import { BaseFundController } from './fund/ctrl.base-fund';
import { BaseGralController } from './gral/ctrl.base-gral';
import { COMMON } from '../../config';
import { BaseUserController } from './user';

export const CommonModule = new ContainerModule((bind: interfaces.Bind) => {
  bind<IBndBaseUserRead>(COMMON.bndBaseUserRead).to(ImplBndBaseUserReadMongo);
  bind<IBndBaseUserWrite>(COMMON.bndBaseUserWrite).to(
    ImplBndBaseUserWriteMongo
  );

  bind<BaseUseCaseUser>('BaseUseCaseUser').toDynamicValue((context) => {
    return new BaseUseCaseUser(
      context.container.get<IBndBaseUserRead>(COMMON.bndBaseUserRead),
      context.container.get<IBndBaseUserWrite>(COMMON.bndBaseUserWrite),
      context.container.get<IBndBaseTokenInfra>(COMMON.bndBaseInfra),
      context.container.get<IBndBaseGCloudInfra>(COMMON.BndBaseGCloudInfra),
      context.container.get<IBndMailerInfra>(COMMON.BndMailerInfra)
    );
  });
  controller('')(BaseUserController);

  // Funds
  bind<IBndBaseFundWrite>(COMMON.bndBaseFundWrite).to(
    ImplBndBaseFundWriteMongo
  );
  bind<IBndBaseFundRead>(COMMON.bndBaseFundRead).to(ImplBndBaseFundReadMongo);
  bind<IBndShareMathFund>(COMMON.bndShareMathFund).to(ImplBndBaseShareMathFund);

  bind<BaseUseCaseFound>('BaseUseCaseFound').toDynamicValue((context) => {
    return new BaseUseCaseFound(
      context.container.get<IBndBaseFundWrite>(COMMON.bndBaseFundWrite),
      context.container.get<IBndBaseFundRead>(COMMON.bndBaseFundRead),
      context.container.get<IBndValidShareData>(COMMON.bndBaseValidShareData),
      context.container.get<IBndBaseGCloudInfra>(COMMON.BndBaseGCloudInfra),
      context.container.get<IBndShareMathFund>(COMMON.bndShareMathFund)
    );
  });
  controller('')(BaseFundController);

  //Gral controller
  controller('')(BaseGralController);
});
