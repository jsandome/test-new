import {
  IBaseEntityLoan,
  BaseUseCaseLoan,
  IScheduleAmortizationInputBase,
} from '@capa/core';

import { inject } from 'inversify';
import { COMMON } from '../../../config/types-common';
import { checkSchema } from 'express-validator';
import {
  BaseHttpController,
  httpPost,
  httpDelete,
  requestBody,
  requestParam,
  httpGet,
  queryParam,
  httpPut,
} from 'inversify-express-utils';
import { loanGeneralSchema } from '../schemas/base.schema-loan';
import {
  createLoan,
  deleteLoan,
  getAmortizationScheduleLoan,
  getLoans,
  updateLoan,
} from './ctlg.types-loans';

export class BaseLoanController extends BaseHttpController {
  @inject('BaseUseCaseLoan') private ucLoan: BaseUseCaseLoan;

  constructor() {
    super();
  }

  @httpGet('/loan/select', COMMON.midValidToken)
  public async getLoans(
    @queryParam('search') search: string
  ): Promise<getLoans> {
    const response = await this.ucLoan.execGetLoans(search);
    return {
      response,
    };
  }

  @httpPost(
    '/admin/loan',
    ...checkSchema(loanGeneralSchema),
    COMMON.midParameters,
    COMMON.midValidToken
  )
  public async createLoanBase(
    @requestBody() loan: IBaseEntityLoan
  ): Promise<createLoan> {
    const response = await this.ucLoan.execCreateLoan(loan);
    return {
      response,
      message: 'Loan created!',
      details: `You can now view the created Loan`,
    };
  }

  @httpPut(
    '/admin/loan/:loanId',
    ...checkSchema(loanGeneralSchema),
    COMMON.midParameters,
    COMMON.midValidToken
  )
  public async uploadLoan(
    @requestBody() loan: IBaseEntityLoan,
    @requestParam('loanId') loanId: string
  ): Promise<updateLoan> {
    await this.ucLoan.execUpdateLoan(loan, loanId);
    return {
      response: {},
      message: 'Loan has been updated succesfully.',
    };
  }

  @httpDelete('/admin/loan/:loanId', COMMON.midValidToken)
  public async deleteLoan(
    @requestParam('loanId') loanId: string
  ): Promise<deleteLoan> {
    await this.ucLoan.execDeleteLoan(loanId);
    return {
      response: {},
      message: 'Loan has been deleted succesfully.',
    };
  }

  @httpPost(
    '/admin/loan/preview/amortization-schedule',
    ...checkSchema(loanGeneralSchema),
    COMMON.midParameters,
    COMMON.midValidToken
  )
  public async getLoanAmortizationSchedule(
    @requestBody() loanData: IScheduleAmortizationInputBase
  ): Promise<getAmortizationScheduleLoan> {
    const response = await this.ucLoan.execGetAmortizationSchedule(loanData);
    return {
      response,
    };
  }
}
