import {
  IBaseEntityLoan,
  IBaseEntityLoanCreateResponse,
  IScheduleAmortizationResponse,
} from '@capa/core';
import { IDtoBaseOutput } from '../idto.base.output';

export interface getLoans extends IDtoBaseOutput<IBaseEntityLoan[]> {}
export interface createLoan
  extends IDtoBaseOutput<IBaseEntityLoanCreateResponse> {}

export interface getAmortizationScheduleLoan
  extends IDtoBaseOutput<IScheduleAmortizationResponse> {}
export interface deleteLoan extends IDtoBaseOutput<{}> {}
export interface updateLoan extends IDtoBaseOutput<{}> {}
