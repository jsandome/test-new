import { inject } from 'inversify';
import { COMMON } from '../../../config/types-common';
import {
  BaseHttpController,
  httpPost,
  httpPut,
  requestBody,

} from 'inversify-express-utils';
import { BaseUseCaseOnboarding } from '@capa/core';
import { IParamsCreateOnboarding,
  IParamsUpdateStepOneOnboarding } from '@capa/core';
import { ctlgCreateOnboarding } from './';
import { SchemaCreteOnboarding,
  SchemaStepOneOnboarding } from '../schemas';
import { checkSchema } from 'express-validator';

export class BaseOnboaringController extends BaseHttpController {
  @inject('BaseUseCaseOnboarding') private ucOnboarding: BaseUseCaseOnboarding;

  constructor() {
    super();
  }
  @httpPost(
    '/admin/onboarding/create',
    COMMON.midValidToken,
    ...checkSchema(SchemaCreteOnboarding),
    COMMON.midParameters
  )
  public async createOnboarding(
    @requestBody() onboardingeDoc: IParamsCreateOnboarding
  ): Promise<ctlgCreateOnboarding> {
    const response = await this.ucOnboarding.createOnboarding(onboardingeDoc);
    return {
      response,
      message: 'Onboarding created!',
      details: `You can now view the created Onboarding`,
    };
  }

  @httpPut(
    '/onboarding/:token/basic-information',
    ...checkSchema(SchemaStepOneOnboarding),
    COMMON.midParameters,
    COMMON.midValidOnboargingToken    
  )
  public async updateStepOneOnboarding(
    @requestBody() onboardingeDoc: IParamsUpdateStepOneOnboarding
  ): Promise<ctlgCreateOnboarding> {
    await this.ucOnboarding.updateStepOneOnboarding(onboardingeDoc);
    return {
      response: {},
      message: 'The information has been saved successfully.',
    };
  }
}
