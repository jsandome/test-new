import {
  BaseUseCaseDistribution,
  IPaginateParams,
  IParamsCreateDistribution,
  IParamsUpdateDistribution,
} from '@capa/core';
import { checkSchema } from 'express-validator';
import { inject } from 'inversify';
import {
  BaseHttpController,
  httpDelete,
  httpGet,
  httpPost,
  httpPut,
  queryParam,
  request,
  requestBody,
  requestParam,
} from 'inversify-express-utils';
import {
  IDResponseCreateDistribution,
  IDResponseDeleteDistribution,
  IDResponseDetailDistribution,
  IDResponseListDistribution,
  IDResponseUpdateDistribution,
} from '.';
import { COMMON } from '../../../config/types-common';
import {
  SchemaCreateDistribution,
  SchemaUpdateDistribution,
} from '../schemas';
import { Request } from 'express';

export class BaseDistributionController extends BaseHttpController {
  @inject('BaseUseCaseDistribution')
  private ucDistribution: BaseUseCaseDistribution;

  constructor() {
    super();
  }

  @httpPost(
    '/admin/llc/:llcId/distribution',
    ...checkSchema(SchemaCreateDistribution),
    COMMON.midParameters,
    COMMON.midValidToken
  )
  public async createDistribution(
    @requestBody() params: IParamsCreateDistribution,
    @requestParam('llcId') llcId: string
  ): Promise<IDResponseCreateDistribution> {
    const response = await this.ucDistribution.execCreateDistribution(
      llcId,
      params
    );
    return {
      response,
      message: 'Distribution created!',
      details: 'You can now view the created Distribution',
    };
  }

  @httpGet(
    '/admin/llc/:llcId/transactions/distribution',
    COMMON.midParameters,
    COMMON.midValidToken
  )
  public async listDistribution(
    @requestParam('llcId') llcId: string,
    @queryParam('search') search: string,
    @queryParam('page') page: number,
    @queryParam('limit') limit: number
  ): Promise<IDResponseListDistribution> {
    const pagination: IPaginateParams = {
      limit,
      page,
    };
    const response = await this.ucDistribution.execListDistribution(
      llcId,
      pagination,
      search
    );
    return { response };
  }

  @httpGet(
    '/admin/llc/:llcId/distribution/:distributionId',
    COMMON.midParameters,
    COMMON.midValidToken
  )
  public async detailDistribution(
    @requestParam('llcId') llcId: string,
    @requestParam('distributionId') distributionId: string
  ): Promise<IDResponseDetailDistribution> {
    const response = await this.ucDistribution.execDetailDistribution(
      llcId,
      distributionId
    );
    return { response };
  }

  @httpPut(
    '/admin/llc/:llcId/distribution/:distributionId',
    ...checkSchema(SchemaUpdateDistribution),
    COMMON.midParameters,
    COMMON.midValidToken
  )
  public async updateDistribution(
    @requestParam('llcId') llcId: string,
    @requestParam('distributionId') distributionId: string,
    @requestBody() params: IParamsUpdateDistribution
  ): Promise<IDResponseUpdateDistribution> {
    const response = await this.ucDistribution.execUpdateDistribution(
      llcId,
      distributionId,
      params
    );
    return { response, message: 'Distributions has been updated!.' };
  }

  @httpDelete(
    '/admin/llc/:llcId/distribution/:distributionId',
    COMMON.midParameters,
    COMMON.midValidToken
  )
  public async deleteDistribution(
    @requestParam('llcId') llcId: string,
    @requestParam('distributionId') distributionId: string,
    @request() req: Request
  ): Promise<IDResponseDeleteDistribution> {
    const response = await this.ucDistribution.execDeleteDistribution(
      llcId,
      distributionId,
      req.userCapa.id
    );
    return { response, message: 'Distribution has been deleted succesfully.' };
  }
}
