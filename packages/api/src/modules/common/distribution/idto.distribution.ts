import {
  IBndReturnCreateDistribution,
  IBndReturnDeleteDistribution,
  IBndReturnDetailDistribution,
  IBndReturnListDistribution,
  IBndReturnUpdateDistribution,
} from '@capa/core';
import { IDtoBaseOutput } from '..';

export interface IDResponseCreateDistribution
  extends IDtoBaseOutput<IBndReturnCreateDistribution> {}
export interface IDResponseListDistribution
  extends IDtoBaseOutput<IBndReturnListDistribution> {}
export interface IDResponseDetailDistribution
  extends IDtoBaseOutput<IBndReturnDetailDistribution> {}
export interface IDResponseUpdateDistribution
  extends IDtoBaseOutput<IBndReturnUpdateDistribution> {}
export interface IDResponseDeleteDistribution
  extends IDtoBaseOutput<IBndReturnDeleteDistribution> {}
