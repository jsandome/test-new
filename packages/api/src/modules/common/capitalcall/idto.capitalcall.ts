import { IDtoBaseOutput } from '..';
import {
  IBndReturnCreateCapitallCall,
  IBndReturnGetCapitallCall,
  IBndReturnListCapitalCall,
  IBndReturnSelectCapitallCall,
} from '@capa/core';

export interface IDResponseCreateCapitallCall
  extends IDtoBaseOutput<IBndReturnCreateCapitallCall> {}
export interface IDResponseListCapitallCall
  extends IDtoBaseOutput<IBndReturnListCapitalCall> {}
export interface IDResponseDetailCapitallCall
  extends IDtoBaseOutput<IBndReturnGetCapitallCall> {}
export interface IDResponseSelectCapitallCall
  extends IDtoBaseOutput<IBndReturnSelectCapitallCall[]> {}
export interface IDResponseUpdateCapitallCall {}
export interface IDResponseDeleteCapitallCall {}
