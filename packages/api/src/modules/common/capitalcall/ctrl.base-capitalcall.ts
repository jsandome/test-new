import { inject } from 'inversify';
import {
  BaseHttpController,
  httpDelete,
  httpGet,
  httpPost,
  httpPut,
  queryParam,
  request,
  requestBody,
  requestParam,
} from 'inversify-express-utils';
import {
  BaseUseCaseCapitalCall,
  IPaginateParams,
  IParamsCreateCapitalCall,
  IParamsUpdateCapitalCall,
} from '@capa/core';
import { SchemaCreateCapitalCall, SchemaUpdateCapitalCall } from '../schemas';
import { checkSchema } from 'express-validator';
import { COMMON } from '../../../config/types-common';
import {
  IDResponseCreateCapitallCall,
  IDResponseDeleteCapitallCall,
  IDResponseDetailCapitallCall,
  IDResponseListCapitallCall,
  IDResponseSelectCapitallCall,
  IDResponseUpdateCapitallCall,
} from '.';
import { Request } from 'express';

export class BaseCapitalCallController extends BaseHttpController {
  @inject('BaseUseCaseCapitalCall')
  private ucCapitalCall: BaseUseCaseCapitalCall;

  constructor() {
    super();
  }

  @httpPost(
    '/admin/llc/:llcId/capital-call',
    ...checkSchema(SchemaCreateCapitalCall),
    COMMON.midParameters,
    COMMON.midValidToken
  )
  public async createNewCapitalCall(
    @requestBody() params: IParamsCreateCapitalCall,
    @requestParam('llcId') llcId: string
  ): Promise<IDResponseCreateCapitallCall> {
    const response = await this.ucCapitalCall.execCreateNewCapitalCall(
      params,
      llcId
    );
    return {
      response,
      message: 'Ready!',
      details: 'You have created a new Capital Call successfully',
    };
  }

  @httpGet('/admin/llc/:llcId/transactions/capital-call', COMMON.midValidToken)
  public async getListCapitalCall(
    @requestParam('llcId') llcId: string,
    @queryParam('search') search: string,
    @queryParam('page') page: number,
    @queryParam('limit') limit: number
  ): Promise<IDResponseListCapitallCall> {
    const pagination: IPaginateParams = {
      limit,
      page,
    };
    const response = await this.ucCapitalCall.execListCapitalCall(
      llcId,
      pagination,
      search
    );
    return { response };
  }

  @httpGet(
    '/admin/llc/:llcId/capital-call/:capitalCallId',
    COMMON.midValidToken
  )
  public async getDetailCapitalCall(
    @requestParam('llcId') llcId: string,
    @requestParam('capitalCallId') capitalCallId: string
  ): Promise<IDResponseDetailCapitallCall> {
    const response = await this.ucCapitalCall.execDetailCapitalCall(
      llcId,
      capitalCallId
    );
    return { response };
  }

  @httpGet(
    '/admin/llc/:llcId/transactions/capital-call/select',
    COMMON.midValidToken
  )
  public async getSelectCapitalCall(
    @requestParam('llcId') llcId: string,
    @queryParam('search') search: string
  ): Promise<IDResponseSelectCapitallCall> {
    const response = await this.ucCapitalCall.execSelectCapitalCall(
      llcId,
      search
    );
    return { response };
  }

  @httpPut(
    '/admin/llc/:llcId/capital-call/:capitalCallId',
    ...checkSchema(SchemaUpdateCapitalCall),
    COMMON.midParameters,
    COMMON.midValidToken
  )
  public async updateCapitallCall(
    @requestBody() params: IParamsUpdateCapitalCall,
    @requestParam('llcId') llcId: string,
    @requestParam('capitalCallId') capitalCallId: string
  ): Promise<IDResponseUpdateCapitallCall> {
    await this.ucCapitalCall.execUpdateCapitalCall(
      params,
      llcId,
      capitalCallId
    );
    return {
      response: {},
      message: 'Ready!',
      details: 'Changes have been saved.',
    };
  }

  @httpDelete(
    '/admin/llc/:llcId/capital-call/:capitalCallId',
    COMMON.midValidToken
  )
  public async deleteCapitalCall(
    @requestParam('llcId') llcId: string,
    @requestParam('capitalCallId') capitalCallId: string,
    @request() req: Request
  ): Promise<IDResponseDeleteCapitallCall> {
    await this.ucCapitalCall.execDeleteCapitalCall(
      llcId,
      capitalCallId,
      req.userCapa.id
    );
    return {
      response: {},
      message: 'Capital call has been deleted succesfully.',
    };
  }
}
