import { inject } from 'inversify';
import { COMMON } from '../../../config/types-common';
import {
  BaseHttpController,
  httpPost,
  httpDelete,
  requestBody,
  requestParam,
  httpGet,
  httpPut,
  request
} from 'inversify-express-utils';
import {
  BaseUseCasePackageDocuments,
  IParamsCreatePackageDocuments,
  IParamsUpdatePackageDocuments
} from '@capa/core';
import { SchemaCreatePackageDocuments,
  SchemaDuplicatePackageDocuments } from '../schemas';
import { checkSchema } from 'express-validator';
import {
  ctlgCreatePackageDocuments,
  ctlgDeletePackageDocuments,
  ctlgGetPackageDocumentsById,
  ctlgGetListPackageDocuments
} from './';
import { Request } from 'express';

export class BasePackageDocumentsController extends BaseHttpController {
  @inject('BaseUseCasePackageDocuments') private ucPackageDocuments: BaseUseCasePackageDocuments;

  constructor() {
    super();
  }

  @httpPost(
    '/admin/document/docusign/package',
    COMMON.midValidToken,
    ...checkSchema(SchemaCreatePackageDocuments),
    COMMON.midParameters,
  )
  public async createPackageDocuments(
    @requestBody() packageDoc: IParamsCreatePackageDocuments
  ): Promise<ctlgCreatePackageDocuments> {
    const response = await this.ucPackageDocuments.createPackageDocuments(packageDoc);
    return {
      response,
      message: 'PackageDocuments created!',
      details: `You can now view the created PackageDocuments`,
    };
  }

  @httpDelete('/admin/document/docusign/package/:packageId', COMMON.midValidToken)
  public async deletePackageDocuments(
    @requestParam('packageId') packageId: string,
    @request() request: Request
  ): Promise<ctlgDeletePackageDocuments> {
    const userDeleteId = request.userCapa.id;
    await this.ucPackageDocuments.deletePackageDocuments(packageId, userDeleteId);
    return {
      response: {},
      message: 'Package has been deleted succesfully.',
    };
  }

  @httpGet('/admin/document/docusign/package/list', COMMON.midValidToken)
  public async listPackageDocuments(
  ): Promise<ctlgGetListPackageDocuments> {
    const response = await this.ucPackageDocuments.listPackageDocuments();
    return {
      response
    };
  }

  @httpGet('/admin/document/docusign/package/:packageId', COMMON.midValidToken)
  public async getPackageDocumentsById(
    @requestParam('packageId') packageId: string
  ): Promise<ctlgGetPackageDocumentsById> {
    const response = await this.ucPackageDocuments.getPackageDocumentsById(packageId);
    return {
      response
    };
  }

  @httpPut('/admin/document/docusign/package/:packageId', 
  COMMON.midValidToken,
  ...checkSchema(SchemaDuplicatePackageDocuments),
  COMMON.midParameters)
  public async updatePackageDocuments(
    @requestParam('packageId') packageId: string,
    @requestBody() updateParams: IParamsUpdatePackageDocuments
  ): Promise<ctlgDeletePackageDocuments> {
    await this.ucPackageDocuments.updatePackageDocuments(packageId, updateParams);
    return {
      response: {},
      message: 'Package has been updated succesfully.',
    };
  }
}
