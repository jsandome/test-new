import {
  IBndResponseCreatePackageDocuments,
  IBndResponseListPackageDocuments
} from '@capa/core';
import { IDtoBaseOutput } from '../idto.base.output';


export interface ctlgCreatePackageDocuments
  extends IDtoBaseOutput<IBndResponseCreatePackageDocuments> {}

export interface ctlgDeletePackageDocuments extends IDtoBaseOutput<{}> {}

export interface ctlgGetPackageDocumentsById extends IDtoBaseOutput<IBndResponseListPackageDocuments> {}

export interface ctlgGetListPackageDocuments extends IDtoBaseOutput<IBndResponseListPackageDocuments[]> {}