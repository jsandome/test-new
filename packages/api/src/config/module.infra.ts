import { ContainerModule, interfaces } from 'inversify';
import {
  BaseUseCaseInfra,
  IBndBaseTokenInfra,
  IBndBaseGCloudInfra,
  IBndBaseUserRead,
  IBndMailerInfra,
  IBndBaseDocusingInfra,
  IBndBaseExcelManagementInfra,
  IBndOnboardingRead,
  IBnd2FAInfra,
} from '@capa/core';
import {
  ImplBndBaseTokenInfra,
  ImplBndBaseGCloudInfra,
  ImplBndMailerInfra,
  ImplBndDocusing,
  ImplBndBaseExcelManagement,
  ImplBnd2FAInfra,
} from '@capa/infra';
import { COMMON } from '.';

export const InfraModule = new ContainerModule((bind: interfaces.Bind) => {
  bind<IBndBaseTokenInfra>(COMMON.bndBaseInfra).to(ImplBndBaseTokenInfra);
  bind<IBndBaseGCloudInfra>(COMMON.BndBaseGCloudInfra).to(
    ImplBndBaseGCloudInfra
  );
  bind<IBndMailerInfra>(COMMON.BndMailerInfra).to(ImplBndMailerInfra);
  bind<IBndBaseDocusingInfra>(COMMON.BndBaseDocusingInfra).to(ImplBndDocusing);
  bind<IBnd2FAInfra>(COMMON.BndBase2FAInfra).to(ImplBnd2FAInfra);

  bind<BaseUseCaseInfra>('BaseUseCaseInfra').toDynamicValue((context) => {
    return new BaseUseCaseInfra(
      context.container.get<IBndBaseTokenInfra>(COMMON.bndBaseInfra),
      context.container.get<IBndBaseUserRead>(COMMON.bndBaseUserRead),
      context.container.get<IBndOnboardingRead>(COMMON.bndBaseOnboardingRead)
    );
  });

  //Excel management
  bind<IBndBaseExcelManagementInfra>(COMMON.bndBaseExcelManagement).to(
    ImplBndBaseExcelManagement
  );
});
