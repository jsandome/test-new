import { injectable } from 'inversify';
import { Logger, createLogger, format, transports } from 'winston';
import { IBaseLogger } from '@capa/core';
import {
  ConsoleTransportInstance,
  FileTransportInstance,
} from 'winston/lib/winston/transports';
const { combine, timestamp, prettyPrint } = format;

@injectable()
export class Log implements IBaseLogger {
  private _logger: Logger;

  constructor() {
    this._logger = createLogger({
      format: combine(timestamp(), prettyPrint()),
      transports: [
        this.instanceTransportConsole(),
        this.instanceTransportFile(),
      ],
      exitOnError: false,
    });
  }

  private instanceTransportConsole(): ConsoleTransportInstance {
    return new transports.Console({
      level: 'debug',
      format: format.combine(
        format.colorize(),
        format.prettyPrint(),
        format.printf((p) => `${p.level}: ${p.message}`)
      ),
    });
  }

  private instanceTransportFile(): FileTransportInstance {
    return new transports.File({
      level: 'error',
      filename: './logs/error-logs.log',
      handleExceptions: true,
      maxsize: 5242880,
      maxFiles: 10,
      format: format.combine(format.prettyPrint()),
    });
  }

  public error(...args: (string | object)[]): void {
    args.forEach((a) => {
      if (typeof a === 'string') this._logger.error(a);
      else this._logger.error(JSON.stringify(a, null, 4));
    });
  }
  public debug(...args: (string | object)[]): void {
    args.forEach((a) => {
      if (typeof a === 'string') this._logger.debug(a);
      else this._logger.debug(JSON.stringify(a, null, 4));
    });
  }
  public info(...args: (string | object)[]): void {
    args.forEach((a) => {
      if (typeof a === 'string') this._logger.info(a);
      else this._logger.info(JSON.stringify(a, null, 4));
    });
  }
  public warning(...args: (string | object)[]): void {
    args.forEach((a) => {
      if (typeof a === 'string') this._logger.warning(a);
      else this._logger.warning(JSON.stringify(a, null, 4));
    });
  }
}
