import { injectable } from 'inversify';

import {
  EnumEnvironments,
  IEmailEnvironment,
  IFirebaseEnvironment,
  IJwtEnvironment,
  IMongoEnvironment,
  IGCloudEnvironment,
  FrontendEnvironment,
  DocusingEnvironment,
  IPropertyParamsEnvironment,
} from '@capa/core';

@injectable()
export class EnvCapa {
  private _env: EnumEnvironments;
  private _port: number;
  private _jwt: IJwtEnvironment;
  private _firebase: IFirebaseEnvironment;
  private _mongo: IMongoEnvironment;
  private _email: IEmailEnvironment;
  private _gcloud: IGCloudEnvironment;
  private _urlFront: FrontendEnvironment;
  private _docusing: DocusingEnvironment;
  private _property: IPropertyParamsEnvironment;

  constructor() {
    this._env = this.setEnv();
    this._port = this.setPort();
    this._firebase = this.setFirebase();
    this._mongo = this.setMongo();
    this._email = this.setEmail();
    this._jwt = this.setJWT();
    this._gcloud = this.setGCloud();
    this._urlFront = this.setFrontUrl();
    this._docusing = this.setDocusing();
    this._property = this.setProperty();
  }

  private setJWT(): IJwtEnvironment {
    return {
      secretWord: process.env.SECRETWORD,
      sessionTime: process.env.SESSIONTIME,
      expireDaysToken: parseInt(process.env.EXPIREDAYSTOKEN, 0),
    };
  }

  private setPort(): number {
    return parseInt(process.env.PORT, 0);
  }

  private setEnv(): EnumEnvironments {
    const environment = process.env.NODE_ENV;
    return environment as EnumEnvironments;
  }

  private setFirebase(): IFirebaseEnvironment {
    return {
      projectId: process.env.FBPROJECTID,
      privateKey: (process.env.FBPRIVATEKEY || '').replace(/\\n/g, '\n'),
      clientEmail: process.env.FBCLIENTEMAIL,
      bucket: process.env.FBBUCKET,
      databaseUrl: process.env.FBDATABASEURL,
    } as IFirebaseEnvironment;
  }

  private setMongo(): IMongoEnvironment {
    return {
      dbName: process.env.DBNAME,
      dbUser: process.env.DBUSER,
      dbPass: process.env.DBPASS,
      dbUrl: process.env.DBURL,
    } as IMongoEnvironment;
  }

  private setEmail(): IEmailEnvironment {
    return {
      smtpHost: process.env.SMTPHOST,
      smtpUser: process.env.SMTPUSER,
      smtpPassword: process.env.SMTPPASSWORD,
      smtpPort: parseInt(process.env.SMTPPORT, 0),
    } as IEmailEnvironment;
  }

  private setGCloud(): IGCloudEnvironment {
    return {
      private_key: (process.env.FBPRIVATEKEY || '').replace(/\\n/g, '\n'),
      client_email: process.env.FBCLIENTEMAIL,
      bucket: process.env.FBBUCKET,
    } as IGCloudEnvironment;
  }

  private setFrontUrl(): FrontendEnvironment {
    const frontendEnv = { url: process.env.BASEURLFE };
    return frontendEnv as FrontendEnvironment;
  }

  private setDocusing(): DocusingEnvironment {
    const docusingEnv = {
      DOCUSIGN_USER_ID: process.env.DOCUSIGN_USER_ID,
      DOCUSIGN_ACCOUNT_ID: process.env.DOCUSIGN_ACCOUNT_ID,
      DOCUSIGN_APP_ID: process.env.DOCUSIGN_APP_ID,
      DOCUSIGN_TESTING_TEMPLATE_ID: process.env.DOCUSIGN_TESTING_TEMPLATE_ID,
      DOCUSIGN_ACCESS_TOKEN: process.env.DOCUSIGN_ACCESS_TOKEN || '',
      DOCUSIGN_PRIVATE_RSA: (process.env.DOCUSIGN_PRIVATE_RSA || '').replace(
        /\\n/g,
        '\n'
      ),
      DOCUSIGN_AUTH_SERVER: process.env.DOCUSIGN_AUTH_SERVER,
      DOCUSIGN_BASEPATH: process.env.DOCUSIGN_BASEPATH,
    };
    return docusingEnv as DocusingEnvironment;
  }
  private setProperty(): IPropertyParamsEnvironment {
    return {
      capitalGainsTaxPaid: parseFloat(process.env.CAPITALGAINSTAXPAID),
    } as IPropertyParamsEnvironment;
  }

  public get environment(): EnumEnvironments {
    return this._env;
  }

  public get port(): number {
    return this._port;
  }

  public get firebase(): IFirebaseEnvironment {
    return this._firebase;
  }

  public get mongo(): IMongoEnvironment {
    return this._mongo;
  }

  public get email(): IEmailEnvironment {
    return this._email;
  }

  public get JWT(): IJwtEnvironment {
    return this._jwt;
  }

  public get gCloud(): IGCloudEnvironment {
    return this._gcloud;
  }

  public get setFront(): FrontendEnvironment {
    return this._urlFront;
  }

  public get setDocusingAPI(): DocusingEnvironment {
    return this._docusing;
  }
  public get propertyParams(): IPropertyParamsEnvironment {
    return this._property;
  }
}
