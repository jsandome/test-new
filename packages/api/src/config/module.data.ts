import { ContainerModule, interfaces } from 'inversify';
import { controller } from 'inversify-express-utils';

import {
  BaseUseCaseLocations,
  IBndBaseLocations,
  BaseUseCaseProperty,
  IBndBasePropertyRead,
  IBndBasePropertyWrite,
  BaseUseCaseCreditline,
  IBndCreditlineRead,
  IBndCreditlineWrite,
  IBndValidShareData,
  BaseUseCaseLoan,
  IBndBaseLoanWrite,
  IBndBaseLoanMath,
  IBndBaseLoanRead,
  BaseUseCasePackage,
  IBndPackageRead,
  IBndPackageWrite,
  IBndBaseGCloudInfra,
  IBndBasePropertyMath,
  IBndBaseLlcWrite,
  BaseUseCaseLlc,
  IBndBaseLlcRead,
  BaseUseCaseContact,
  IBndBaseContactWrite,
  IBndBaseContactRead,
  BaseUseCaseInvestor,
  IBndBaseInvestorWrite,
  IBndBaseInvestorRead,
  IBndMailerInfra,
  IBndDocusignRead,
  BaseUseCaseDocuSign,
  IBndGCloudDocumentsWrite,
  BaseUseCaseGCloudDocuments,
  IBndBaseUserRead,
  IBndGCloudDocumentsRead,
  IBndPackageDocumentsRead,
  IBndPackageDocumentsWrite,
  BaseUseCasePackageDocuments,
  IBndBaseCapitalCallWrite,
  BaseUseCaseCapitalCall,
  IBndBaseCapitalCallRead,
  IBndBaseExcelManagementInfra,
  BaseUseCaseContribution,
  IBndBaseContributionWrite,
  IBndBaseContributionRead,
  IBndBaseDistributionRead,
  IBndBaseDistributionWrite,
  BaseUseCaseDistribution,
  IBndOnboardingRead,
  IBndOnboardingWrite,
  IBndBaseTokenInfra,
  BaseUseCaseOnboarding,  
} from '@capa/core';

import {
  ImplBndBaseLocations,
  ImplBndBasePropertyRead,
  ImplBndBasePropertyWrite,
  ImplBndBaseCreditlineRead,
  ImplBndBaseCreditlineWrite,
  ImplBndBaseValidShareData,
  ImplBndBaseLoanMath,
  ImplBndBaseLoanRead,
  ImplBndBaseLoanWriteMongo,
  ImplBndBasePackageRead,
  ImplBndBasePackageWrite,
  ImplBndBasePropertyMath,
  ImplBndBaseLlcWriteMongo,
  ImplBndBaseLlcReadMongo,
  ImplBndBaseContactWrite,
  ImplBndBaseContactRead,
  ImplBndBaseInvestorWrite,
  ImplBndBaseInvestorRead,
  ImplBndGCloudDocumentsWrite,
  ImplBndGCloudDocumentsRead,
  ImplBndBasePackageDocumentsRead,
  ImplBndBasePackageDocumentsWrite,
  ImplBndCapitalCallWrite,
  ImplBndCapitalCallRead,
  ImplBndContributionWrite,
  ImplBndContributionRead,
  ImplBndDistributionRead,
  ImplBndDistributionWrite,
  ImplBndBaseOnboardingRead,
  ImplBndBaseOnboardingWrite,
} from '@capa/data';

import { ImplBndDocusing } from '@capa/infra';
import { COMMON } from './';
import {
  BaseLocationsController,
  BasePropertyController,
  BaseCreditLineController,
  BaseLoanController,
  BasePackageController,
  BaseLlcController,
  BaseContactController,
  BaseInvestorController,
  DocuSignController,
  BaseGCloudDocumentsController,
  BasePackageDocumentsController,
  BaseCapitalCallController,
  BaseContributionController,
  BaseDistributionController,
  BaseOnboaringController,
} from '../modules/common';

export const DataModule = new ContainerModule((bind: interfaces.Bind) => {
  //locations
  bind<IBndBaseLocations>(COMMON.BndBaseLocations).to(ImplBndBaseLocations);
  bind<BaseUseCaseLocations>('BaseUseCaseLocations').toDynamicValue(
    (context) => {
      return new BaseUseCaseLocations(
        context.container.get<IBndBaseLocations>(COMMON.BndBaseLocations)
      );
    }
  );
  controller('')(BaseLocationsController);
  //Property
  bind<IBndBasePropertyRead>(COMMON.bndBasePropertyRead).to(
    ImplBndBasePropertyRead
  );
  bind<IBndBasePropertyWrite>(COMMON.bndBasePropertyWrite).to(
    ImplBndBasePropertyWrite
  );
  bind<IBndBasePropertyMath>(COMMON.bndBasePropertyMath).to(
    ImplBndBasePropertyMath
  );
  bind<BaseUseCaseProperty>('BaseUseCaseProperty').toDynamicValue((context) => {
    return new BaseUseCaseProperty(
      context.container.get<IBndBasePropertyRead>(COMMON.bndBasePropertyRead),
      context.container.get<IBndBasePropertyWrite>(COMMON.bndBasePropertyWrite),
      context.container.get<IBndBaseGCloudInfra>(COMMON.BndBaseGCloudInfra),
      context.container.get<IBndBasePropertyMath>(COMMON.bndBasePropertyMath),
      context.container.get<IBndBaseExcelManagementInfra>(
        COMMON.bndBaseExcelManagement
      ),
      context.container.get<IBndBaseLocations>(COMMON.BndBaseLocations)
    );
  });
  controller('')(BasePropertyController);

  //Creditline
  bind<IBndCreditlineRead>(COMMON.bndBaseCreditlineRead).to(
    ImplBndBaseCreditlineRead
  );
  bind<IBndCreditlineWrite>(COMMON.bndBaseCreditlineWrite).to(
    ImplBndBaseCreditlineWrite
  );
  bind<IBndValidShareData>(COMMON.bndBaseValidShareData).to(
    ImplBndBaseValidShareData
  );
  bind<BaseUseCaseCreditline>('BaseUseCaseCreditline').toDynamicValue(
    (context) => {
      return new BaseUseCaseCreditline(
        context.container.get<IBndCreditlineRead>(COMMON.bndBaseCreditlineRead),
        context.container.get<IBndCreditlineWrite>(
          COMMON.bndBaseCreditlineWrite
        ),
        context.container.get<IBndValidShareData>(COMMON.bndBaseValidShareData)
      );
    }
  );
  controller('')(BaseCreditLineController);

  //Loan
  bind<IBndBaseLoanWrite>(COMMON.bndBaseLoanWrite).to(
    ImplBndBaseLoanWriteMongo
  );
  bind<IBndBaseLoanMath>(COMMON.bndBaseLoanMath).to(ImplBndBaseLoanMath);
  bind<IBndBaseLoanRead>(COMMON.bndBaseLoanRead).to(ImplBndBaseLoanRead);

  bind<BaseUseCaseLoan>('BaseUseCaseLoan').toDynamicValue((context) => {
    return new BaseUseCaseLoan(
      context.container.get<IBndBaseLoanRead>(COMMON.bndBaseLoanRead),
      context.container.get<IBndBaseLoanWrite>(COMMON.bndBaseLoanWrite),
      context.container.get<IBndBaseLoanMath>(COMMON.bndBaseLoanMath)
    );
  });
  controller('')(BaseLoanController);

  // Package
  bind<IBndPackageRead>(COMMON.bndBasePackageRead).to(ImplBndBasePackageRead);
  bind<IBndPackageWrite>(COMMON.bndBasePackageWrite).to(
    ImplBndBasePackageWrite
  );
  bind<BaseUseCasePackage>('BaseUseCasePackage').toDynamicValue((context) => {
    return new BaseUseCasePackage(
      context.container.get<IBndPackageRead>(COMMON.bndBasePackageRead),
      context.container.get<IBndPackageWrite>(COMMON.bndBasePackageWrite),
      context.container.get<IBndValidShareData>(COMMON.bndBaseValidShareData),
      context.container.get<IBndBaseGCloudInfra>(COMMON.BndBaseGCloudInfra)
    );
  });
  controller('')(BasePackageController);

  // LLC
  bind<IBndBaseLlcWrite>(COMMON.bndBaseLlcWrite).to(ImplBndBaseLlcWriteMongo);
  bind<IBndBaseLlcRead>(COMMON.bndBaseLlcRead).to(ImplBndBaseLlcReadMongo);

  bind<BaseUseCaseLlc>('BaseUseCaseLlc').toDynamicValue((context) => {
    return new BaseUseCaseLlc(
      context.container.get<IBndBaseLlcWrite>(COMMON.bndBaseLlcWrite),
      context.container.get<IBndBaseLlcRead>(COMMON.bndBaseLlcRead),
      context.container.get<IBndValidShareData>(COMMON.bndBaseValidShareData),
      context.container.get<IBndBaseGCloudInfra>(COMMON.BndBaseGCloudInfra)
    );
  });
  controller('')(BaseLlcController);

  // Contact
  bind<IBndBaseContactWrite>(COMMON.bndBaseContactWrite).to(
    ImplBndBaseContactWrite
  );
  bind<IBndBaseContactRead>(COMMON.bndBaseContactRead).to(
    ImplBndBaseContactRead
  );

  bind<BaseUseCaseContact>('BaseUseCaseContact').toDynamicValue((context) => {
    return new BaseUseCaseContact(
      context.container.get<IBndBaseContactRead>(COMMON.bndBaseContactRead),
      context.container.get<IBndBaseContactWrite>(COMMON.bndBaseContactWrite)
    );
  });
  controller('')(BaseContactController);

  // Investor
  bind<IBndBaseInvestorWrite>(COMMON.bndBaseInvestorWrite).to(
    ImplBndBaseInvestorWrite
  );
  bind<IBndBaseInvestorRead>(COMMON.bndBaseInvestorRead).to(
    ImplBndBaseInvestorRead
  );

  bind<BaseUseCaseInvestor>('BaseUseCaseInvestor').toDynamicValue((context) => {
    return new BaseUseCaseInvestor(
      context.container.get<IBndMailerInfra>(COMMON.BndMailerInfra),
      context.container.get<IBndBaseInvestorWrite>(COMMON.bndBaseInvestorWrite),
      context.container.get<IBndBaseInvestorRead>(COMMON.bndBaseInvestorRead)
    );
  });
  controller('')(BaseInvestorController);

  //Docusign
  bind<IBndDocusignRead>(COMMON.bndBaseDocusign).to(ImplBndDocusing);

  bind<BaseUseCaseDocuSign>('BaseUseCaseDocuSign').toDynamicValue((context) => {
    return new BaseUseCaseDocuSign(
      context.container.get<IBndDocusignRead>(COMMON.bndBaseDocusign)
    );
  });
  controller('')(DocuSignController);

  //Documents
  bind<IBndGCloudDocumentsWrite>(COMMON.bndGCloudDocumentsWrite).to(
    ImplBndGCloudDocumentsWrite
  );
  bind<IBndGCloudDocumentsRead>(COMMON.bndGCloudDocumentsRead).to(
    ImplBndGCloudDocumentsRead
  );

  bind<BaseUseCaseGCloudDocuments>('BaseUseCaseGCloudDocuments').toDynamicValue(
    (context) => {
      return new BaseUseCaseGCloudDocuments(
        context.container.get<IBndBaseGCloudInfra>(COMMON.BndBaseGCloudInfra),
        context.container.get<IBndGCloudDocumentsWrite>(
          COMMON.bndGCloudDocumentsWrite
        ),
        context.container.get<IBndBaseUserRead>(COMMON.bndBaseUserRead),
        context.container.get<IBndGCloudDocumentsRead>(
          COMMON.bndGCloudDocumentsRead
        )
      );
    }
  );
  controller('')(BaseGCloudDocumentsController);

  //Package Documents
  bind<IBndPackageDocumentsRead>(COMMON.bndBasePackageDocumentsRead).to(
    ImplBndBasePackageDocumentsRead
  );
  bind<IBndPackageDocumentsWrite>(COMMON.bndBasePackageDocumentsWrite).to(
    ImplBndBasePackageDocumentsWrite
  );
  bind<BaseUseCasePackageDocuments>(
    'BaseUseCasePackageDocuments'
  ).toDynamicValue((context) => {
    return new BaseUseCasePackageDocuments(
      context.container.get<IBndPackageDocumentsWrite>(
        COMMON.bndBasePackageDocumentsWrite
      ),
      context.container.get<IBndPackageDocumentsRead>(
        COMMON.bndBasePackageDocumentsRead
      )
    );
  });
  controller('')(BasePackageDocumentsController);

  //Capital Call
  bind<ImplBndCapitalCallWrite>(COMMON.bndBaseCapitalCallWrite).to(
    ImplBndCapitalCallWrite
  );
  bind<ImplBndCapitalCallRead>(COMMON.bndBaseCapitalCallRead).to(
    ImplBndCapitalCallRead
  );

  bind<BaseUseCaseCapitalCall>('BaseUseCaseCapitalCall').toDynamicValue(
    (context) => {
      return new BaseUseCaseCapitalCall(
        context.container.get<IBndBaseCapitalCallWrite>(
          COMMON.bndBaseCapitalCallWrite
        ),
        context.container.get<IBndBaseCapitalCallRead>(
          COMMON.bndBaseCapitalCallRead
        )
      );
    }
  );
  controller('')(BaseCapitalCallController);

  //Contribution
  bind<ImplBndContributionRead>(COMMON.bndBaseContributionRead).to(
    ImplBndContributionRead
  );
  bind<ImplBndContributionWrite>(COMMON.bndBaseContributionWrite).to(
    ImplBndContributionWrite
  );

  bind<BaseUseCaseContribution>('BaseUseCaseContribution').toDynamicValue(
    (context) => {
      return new BaseUseCaseContribution(
        context.container.get<IBndBaseContributionRead>(
          COMMON.bndBaseContributionRead
        ),
        context.container.get<IBndBaseContributionWrite>(
          COMMON.bndBaseContributionWrite
        )
      );
    }
  );
  controller('')(BaseContributionController);

  //Distribution
  bind<ImplBndDistributionRead>(COMMON.bndBaseDistributionRead).to(
    ImplBndDistributionRead
  );
  bind<ImplBndDistributionWrite>(COMMON.bndBaseDistributionWrite).to(
    ImplBndDistributionWrite
  );

  bind<BaseUseCaseDistribution>('BaseUseCaseDistribution').toDynamicValue(
    (context) => {
      return new BaseUseCaseDistribution(
        context.container.get<IBndBaseDistributionRead>(
          COMMON.bndBaseDistributionRead
        ),
        context.container.get<IBndBaseDistributionWrite>(
          COMMON.bndBaseDistributionWrite
        )
      );
    }
  );
  controller('')(BaseDistributionController);
  //Onboarding
  bind<IBndOnboardingRead>(COMMON.bndBaseOnboardingRead).to(
    ImplBndBaseOnboardingRead
  );
  bind<IBndOnboardingWrite>(COMMON.bndBaseOnboardingWrite).to(
    ImplBndBaseOnboardingWrite
  );
  bind<BaseUseCaseOnboarding>('BaseUseCaseOnboarding').toDynamicValue(
    (context) => {
      return new BaseUseCaseOnboarding(
        context.container.get<IBndBaseTokenInfra>(COMMON.bndBaseInfra),
        context.container.get<IBndMailerInfra>(COMMON.BndMailerInfra),
        context.container.get<IBndOnboardingWrite>(
          COMMON.bndBaseOnboardingWrite
        ),
        context.container.get<IBndOnboardingRead>(COMMON.bndBaseOnboardingRead)
      );
    }
  );
  controller('')(BaseOnboaringController);
});
