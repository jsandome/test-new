import { ContainerModule, interfaces } from 'inversify';
import {
  BASETYPES,
  IBaseLogger,
  IFirebaseEnvironment,
  IJwtEnvironment,
  IMongoEnvironment,
  IGCloudEnvironment,
  IEmailEnvironment,
  FrontendEnvironment,
  DocusingEnvironment,
  IPropertyParamsEnvironment,
} from '@capa/core';
import { MongoConfig } from '@capa/data';
import {
  FirebaseConfig,
  GCloudConfig,
  MailerConfig,
  DocusingConfig,
} from '@capa/infra';
import { CorsConfig, EnvCapa, Log, MyStream } from '.';

export const ConfigModule = new ContainerModule((bind: interfaces.Bind) => {
  bind<EnvCapa>(EnvCapa).toSelf().inSingletonScope();
  bind<IMongoEnvironment>(BASETYPES.IMongoConfig).toDynamicValue(
    (context) => context.container.get<EnvCapa>(EnvCapa).mongo
  );
  bind<IFirebaseEnvironment>(BASETYPES.IFirebaseConfig).toDynamicValue(
    (context) => context.container.get<EnvCapa>(EnvCapa).firebase
  );
  bind<IJwtEnvironment>(BASETYPES.IJwtConfig).toDynamicValue(
    (context) => context.container.get<EnvCapa>(EnvCapa).JWT
  );
  bind<IGCloudEnvironment>(BASETYPES.IGCloudConfig).toDynamicValue(
    (context) => context.container.get<EnvCapa>(EnvCapa).gCloud
  );
  bind<IEmailEnvironment>(BASETYPES.IMailerConfig).toDynamicValue(
    (context) => context.container.get<EnvCapa>(EnvCapa).email
  );

  bind<CorsConfig>(CorsConfig).toSelf().inSingletonScope();
  bind<Log>(Log).toSelf().inSingletonScope();
  bind<IBaseLogger>(BASETYPES.Log).to(Log).inSingletonScope();
  bind<MyStream>(MyStream).toSelf().inSingletonScope();

  bind<MongoConfig>(MongoConfig).toSelf().inSingletonScope();
  bind<FirebaseConfig>(FirebaseConfig).toSelf().inSingletonScope();
  bind<GCloudConfig>(GCloudConfig).toSelf().inSingletonScope();
  bind<MailerConfig>(MailerConfig).toSelf().inSingletonScope();
  bind<DocusingConfig>(DocusingConfig).toSelf().inSingletonScope();

  bind<FrontendEnvironment>(BASETYPES.FrontendEnvironment).toDynamicValue(
    (context) => context.container.get<EnvCapa>(EnvCapa).setFront
  );

  bind<DocusingEnvironment>(BASETYPES.DocusingEnvironment).toDynamicValue(
    (context) => context.container.get<EnvCapa>(EnvCapa).setDocusingAPI
  );
  bind<IPropertyParamsEnvironment>(BASETYPES.IPropertyParams).toDynamicValue(
    (context) => context.container.get<EnvCapa>(EnvCapa).propertyParams
  );
});
