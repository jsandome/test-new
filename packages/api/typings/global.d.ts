import { IJwtParamsTokenIn } from '@capa/core';
export {};
declare global {
  type Logger = {
    info: (msg: any) => void;
    error: (msg: any) => void;
    debug: (msg: any) => void;
    warning: (msg: any) => void;
  };
  namespace NodeJS {
    interface Global {
      logger: Logger;
    }
  }
  namespace Express {
    export interface Request {
      reqId: string;
      userCapa: IJwtParamsTokenIn;
    }
  }
}
