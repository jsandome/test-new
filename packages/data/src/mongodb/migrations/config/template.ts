
import { startSession } from "mongoose";
​
/*import { ... } from "@capa/core";*/ //Importación de enums
/*import { ... } from "../models";*/ //Importación de los modelos de moongoose
​
async function up () {
  const session = await startSession();
  session.startTransaction();
  

//   await model.save({ session }); //Guarda todos los cambios dentro de una sesión 
​
​
  await session.commitTransaction();
  session.endSession();
}
​
async function down () {

}
​
export  = { up, down };
