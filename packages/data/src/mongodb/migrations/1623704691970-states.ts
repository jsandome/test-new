import mongoose from 'mongoose';
import { BaseError } from '@capa/core';
import { ModelState } from '../models/locations';
import { States } from './data';

const getStateModels = (states) => {
  return states.map((state) => {
    return new ModelState({
      _id: mongoose.Types.ObjectId(state._id),
      name: state.name,
      code: state.acronym,
    });
  });
};

const getStateIds = (states) => {
  return states.map((state) => {
    return mongoose.Types.ObjectId(state._id);
  });
};

const up = async () => {
  try {
    const states = getStateModels(States);
    await ModelState.insertMany(states);
  } catch (error) {
    throw new BaseError(
      `An error occurred in migration. UP ${error}`,
      500,
      'states',
      error
    );
  }
};

const down = async () => {
  try {
    const session = await mongoose.startSession();
    session.startTransaction();
    const stateIds = getStateIds(States);
    await ModelState.deleteMany({ _id: { $in: stateIds } }, { session });
    await session.commitTransaction();
    session.endSession();
  } catch (error) {
    throw new BaseError(
      `An error occurred in migration. Down ${error}`,
      500,
      'states',
      error
    );
  }
};

export = { up, down };
