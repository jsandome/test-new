import { EnumUserTypes } from '@capa/core';
import { startSession } from 'mongoose';
import { ModelAdministrator, ModelInvestor, ModelUser } from '../models';
import { users } from './data';
async function up() {
  for (const user of users) {
    const newUser = new ModelUser({
      email: user.email,
      admin: null,
      investor: null,
      password: user.password,
      type: user.type,
    });
    await newUser.save();
    if (user.type !== EnumUserTypes.INVESTOR) {
      const newPerson = new ModelAdministrator({
        user: newUser._id,
        name: {
          firstName: user.firstName,
          lastName: user.lastName,
        },
        type: user.type,
      });
      await newPerson.save();
      newUser.admin = newPerson._id;
      await newUser.save();
    }
    if (user.type === EnumUserTypes.INVESTOR) {
      const newPerson = new ModelInvestor({
        user: newUser._id,
        name: {
          firstName: user.firstName,
          lastName: user.lastName,
        },
      });
      await newPerson.save();
      newUser.investor = newPerson._id;
      await newUser.save();
    }
  }
}
async function down() {
  const session = await startSession();
  session.startTransaction();

  for (const user of users) {
    const findUser = await ModelUser.findOne({ email: user.email });
    if (!!findUser) {
      if (!!findUser.investor) {
        const findInvestor = await ModelInvestor.findById(findUser.investor);
        await findInvestor.deleteOne({ session });
      }
      if (!!findUser.admin) {
        const findAdministrator = await ModelAdministrator.findById(
          findUser.admin
        );
        await findAdministrator.deleteOne({ session });
      }
      await findUser.deleteOne({ session });
    }
  }

  await session.commitTransaction();
  session.endSession();
}
export = { up, down };
