import mongoose from 'mongoose';
import { BaseError } from '@capa/core';
import { ModelCounty } from '../models/locations';
import { Counties } from './data';

const getCountyModels = (counties) => {
  return counties.map((county) => {
    return new ModelCounty({
      name: county.name,
      state: county.state,
      fips: county.fips,
      _id: mongoose.Types.ObjectId(county._id),
    });
  });
};

const getCountyIds = (counties) => {
  return counties.map((county) => {
    return mongoose.Types.ObjectId(county._id);
  });
};

const up = async () => {
  try {
    const counties = getCountyModels(Counties);
    await ModelCounty.insertMany(counties);
  } catch (error) {
    throw new BaseError(
      `An error occurred in migration. UP ${error}`,
      500,
      'counties',
      error
    );
  }
};

const down = async () => {
  try {
    const session = await mongoose.startSession();
    session.startTransaction();
    const countyIds = getCountyIds(Counties);
    await ModelCounty.deleteMany({ _id: { $in: countyIds } }, { session });
    await session.commitTransaction();
    session.endSession();
  } catch (error) {
    throw new BaseError(
      `An error occurred in migration. Down ${error}`,
      500,
      'counties',
      error
    );
  }
};

export = { up, down };
