import mongoose from 'mongoose';
import { BaseError } from '@capa/core';
import { ModelCountry } from '../models/locations';
import { Countries } from './data';

const getCountryModels = (countries) => {
  return countries.map((country) => {
    return new ModelCountry({
      _id: mongoose.Types.ObjectId(country._id),
      name: country.name,
      code: country.code,
    });
  });
};

const getCountryIds = (countries) => {
  return countries.map((country) => {
    return mongoose.Types.ObjectId(country._id);
  });
};
async function up() {
  try {
    const countriesModels = getCountryModels(Countries);
    await ModelCountry.insertMany(countriesModels);
  } catch (error) {
    throw new BaseError(
      `An error occurred in migration. UP ${error}`,
      500,
      'Add countries',
      error
    );
  }
}
async function down() {
  try {
    const session = await mongoose.startSession();
    session.startTransaction();
    const countryIds = getCountryIds(Countries);
    await ModelCountry.deleteMany({ _id: { $in: countryIds } }, { session });
    await session.commitTransaction();
    session.endSession();
  } catch (error) {
    throw new BaseError(
      `An error occurred in migration. Down ${error}`,
      500,
      'Delete countries',
      error
    );
  }
}
export = { up, down };
