import { EnumPhoneType, IBaseInvestorAddress } from '@capa/core';
import { IModelInvestor, ModelInvestor } from '../models';
async function up() {
  const investors: IModelInvestor[] = await ModelInvestor.find({});

  for (let investor of investors) {
    investor.phoneType = 'Work' as EnumPhoneType;
    investor.addresses = [
      {
        address: 'Amsterdam 173-int. 3, Hipódromo, Cuauhtémoc',
        city: 'Ciudad de México',
        zipCode: '06100',
        county: '60c7bd3e9af6ecdf0eed781c', //Los Angeles
        state: '60c7bd3e9af6ecdf0eed781b', //California,
      },
    ] as IBaseInvestorAddress[];
    investor.biographical = {
      name: 'Umveller',
      city: 'Ciudad de México',
      jobTitle: 'Partner',
      companyName: 'Umvel Inc',
      referrer: 'User name',
      assignee: 'User name',
    };

    await investor.save();
  }
}
async function down() {}
export = { up, down };
