import { EnumUserTypes } from '@capa/core';

export const users = [
  {
    firstName: 'Monica',
    lastName: 'García',
    email: 'monica.garcia@umvel.com',
    password: '$2b$10$urd9l359swkW0t2HTeTbiujQzGqKMFW6fxdm2l3s/eDaaGZCtn6Ji',
    type: EnumUserTypes.ADMIN,
  },
  {
    firstName: 'Miguel',
    lastName: 'Tec',
    email: 'miguel.tec@umvel.com',
    password: '$2b$10$urd9l359swkW0t2HTeTbiujQzGqKMFW6fxdm2l3s/eDaaGZCtn6Ji',
    type: EnumUserTypes.ADMIN,
  },
  {
    firstName: 'Ricardo',
    lastName: 'Chavarria',
    email: 'ricardo.chavarria@umvel.com',
    password: '$2b$10$urd9l359swkW0t2HTeTbiujQzGqKMFW6fxdm2l3s/eDaaGZCtn6Ji',
    type: EnumUserTypes.SUPERADMIN,
  },
  {
    firstName: 'Fernando',
    lastName: 'Reyes',
    email: 'fernando.reyes@umvel.com',
    password: '$2b$10$SuFAb8z2rOU79bJHDIBAuub.rIXSheNv0oo5Qhp/KsYmKRhspbDWa',
    type: EnumUserTypes.INVESTOR,
  },
  {
    firstName: 'Jaime',
    lastName: 'Reixach',
    email: 'jaime.reixach@umvel.com',
    password: '$2b$10$urd9l359swkW0t2HTeTbiujQzGqKMFW6fxdm2l3s/eDaaGZCtn6Ji',
    type: EnumUserTypes.ADMIN,
  },
  {
    firstName: 'Magdiel',
    lastName: 'Juarez',
    email: 'magdiel.juarez@umvel.com',
    password: '$2b$10$SuFAb8z2rOU79bJHDIBAuub.rIXSheNv0oo5Qhp/KsYmKRhspbDWa',
    type: EnumUserTypes.INVESTOR,
  },
  {
    firstName: 'Frank',
    lastName: 'Vandeven',
    email: 'frank.vandeven@umvel.com',
    password: '$2b$10$SuFAb8z2rOU79bJHDIBAuub.rIXSheNv0oo5Qhp/KsYmKRhspbDWa',
    type: EnumUserTypes.INVESTOR,
  },
];
