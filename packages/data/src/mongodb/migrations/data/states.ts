export const States = [
  {
    name: 'California',
    _id: '60c7bd3e9af6ecdf0eed781b',
    acronym: 'CA',
  },
  {
    name: 'Illinois',
    _id: '60c7bd3e9af6ecdf0eed781d',
    acronym: 'IL',
  },
  {
    name: 'Texas',
    _id: '60c7bd3e9af6ecdf0eed781f',
    acronym: 'TX',
  },
  {
    name: 'Arizona',
    _id: '60c7bd3e9af6ecdf0eed7821',
    acronym: 'AZ',
  },
  {
    name: 'Florida',
    _id: '60c7bd3e9af6ecdf0eed7825',
    acronym: 'FL',
  },
  {
    name: 'New York',
    _id: '60c7bd3e9af6ecdf0eed7828',
    acronym: 'NY',
  },
  {
    name: 'Washington',
    _id: '60c7bd3e9af6ecdf0eed782c',
    acronym: 'WA',
  },
  {
    name: 'Nevada',
    _id: '60c7bd3e9af6ecdf0eed782e',
    acronym: 'NV',
  },
  {
    name: 'Michigan',
    _id: '60c7bd3e9af6ecdf0eed7835',
    acronym: 'MI',
  },
  {
    name: 'Massachusetts',
    _id: '60c7bd3e9af6ecdf0eed7839',
    acronym: 'MA',
  },
  {
    name: 'Pennsylvania',
    _id: '60c7bd3e9af6ecdf0eed783b',
    acronym: 'PA',
  },
  {
    name: 'Ohio',
    _id: '60c7bd3e9af6ecdf0eed7844',
    acronym: 'OH',
  },
  {
    name: 'Minnesota',
    _id: '60c7bd3e9af6ecdf0eed7848',
    acronym: 'MN',
  },
  {
    name: 'Virginia',
    _id: '60c7bd3e9af6ecdf0eed784c',
    acronym: 'VA',
  },
  {
    name: 'Utah',
    _id: '60c7bd3e9af6ecdf0eed784f',
    acronym: 'UT',
  },
  {
    name: 'North Carolina',
    _id: '60c7bd3e9af6ecdf0eed7851',
    acronym: 'NC',
  },
  {
    name: 'Maryland',
    _id: '60c7bd3e9af6ecdf0eed7854',
    acronym: 'MD',
  },
  {
    name: 'Georgia',
    _id: '60c7bd3e9af6ecdf0eed7856',
    acronym: 'GA',
  },
  {
    name: 'Missouri',
    _id: '60c7bd3e9af6ecdf0eed7859',
    acronym: 'MO',
  },
  {
    name: 'Hawaii',
    _id: '60c7bd3e9af6ecdf0eed785b',
    acronym: 'HI',
  },
  {
    name: 'Indiana',
    _id: '60c7bd3e9af6ecdf0eed7861',
    acronym: 'IN',
  },
  {
    name: 'Wisconsin',
    _id: '60c7bd3e9af6ecdf0eed7863',
    acronym: 'WI',
  },
  {
    name: 'Connecticut',
    _id: '60c7bd3e9af6ecdf0eed7865',
    acronym: 'CT',
  },
  {
    name: 'Tennessee',
    _id: '60c7bd3e9af6ecdf0eed7867',
    acronym: 'TN',
  },
  {
    name: 'New Jersey',
    _id: '60c7bd3e9af6ecdf0eed786a',
    acronym: 'NJ',
  },
  {
    name: 'Oregon',
    _id: '60c7bd3e9af6ecdf0eed787f',
    acronym: 'OR',
  },
  {
    name: 'Oklahoma',
    _id: '60c7bd3e9af6ecdf0eed7884',
    acronym: 'OK',
  },
  {
    name: 'Kentucky',
    _id: '60c7bd3e9af6ecdf0eed7888',
    acronym: 'KY',
  },
  {
    name: 'Colorado',
    _id: '60c7bd3e9af6ecdf0eed7890',
    acronym: 'CO',
  },
  {
    name: 'District of Columbia',
    _id: '60c7bd3e9af6ecdf0eed7896',
    acronym: 'DC',
  },
  {
    name: 'New Mexico',
    _id: '60c7bd3e9af6ecdf0eed789b',
    acronym: 'NM',
  },
  {
    name: 'Alabama',
    _id: '60c7bd3e9af6ecdf0eed789e',
    acronym: 'AL',
  },
  {
    name: 'Rhode Island',
    _id: '60c7bd3e9af6ecdf0eed78a3',
    acronym: 'RI',
  },
  {
    name: 'Kansas',
    _id: '60c7bd3e9af6ecdf0eed78aa',
    acronym: 'KS',
  },
  {
    name: 'Nebraska',
    _id: '60c7bd3e9af6ecdf0eed78b3',
    acronym: 'NE',
  },
  {
    name: 'Delaware',
    _id: '60c7bd3e9af6ecdf0eed78b5',
    acronym: 'DE',
  },
  {
    name: 'South Carolina',
    _id: '60c7bd3e9af6ecdf0eed78c6',
    acronym: 'SC',
  },
  {
    name: 'Iowa',
    _id: '60c7bd3e9af6ecdf0eed78cf',
    acronym: 'IA',
  },
  {
    name: 'Idaho',
    _id: '60c7bd3e9af6ecdf0eed78da',
    acronym: 'ID',
  },
  {
    name: 'Louisiana',
    _id: '60c7bd3e9af6ecdf0eed78e0',
    acronym: 'LA',
  },
  {
    name: 'New Hampshire',
    _id: '60c7bd3e9af6ecdf0eed78ec',
    acronym: 'NH',
  },
  {
    name: 'Arkansas',
    _id: '60c7bd3e9af6ecdf0eed78f5',
    acronym: 'AR',
  },
  {
    name: 'Alaska',
    _id: '60c7bd3e9af6ecdf0eed792d',
    acronym: 'AK',
  },
  {
    name: 'Maine',
    _id: '60c7bd3e9af6ecdf0eed792f',
    acronym: 'ME',
  },
  {
    name: 'Mississippi',
    _id: '60c7bd3e9af6ecdf0eed795f',
    acronym: 'MS',
  },
  {
    name: 'South Dakota',
    _id: '60c7bd3e9af6ecdf0eed79a2',
    acronym: 'SD',
  },
  {
    name: 'West Virginia',
    _id: '60c7bd3e9af6ecdf0eed79ab',
    acronym: 'WV',
  },
  {
    name: 'North Dakota',
    _id: '60c7bd3e9af6ecdf0eed79bc',
    acronym: 'ND',
  },
  {
    name: 'Vermont',
    _id: '60c7bd3e9af6ecdf0eed79da',
    acronym: 'VT',
  },
  {
    name: 'Montana',
    _id: '60c7bd3e9af6ecdf0eed79e7',
    acronym: 'MT',
  },
  {
    name: 'Wyoming',
    _id: '60c7bd3e9af6ecdf0eed7aa8',
    acronym: 'WY',
  },
];
