import { EnumUserTypes } from '@capa/core';

export const newusers = [
  {
    firstName: 'Katia',
    lastName: 'García',
    email: 'katia.garcia@umvel.com',
    password: '$2b$10$urd9l359swkW0t2HTeTbiujQzGqKMFW6fxdm2l3s/eDaaGZCtn6Ji',
    type: EnumUserTypes.SUPERADMIN,
  },
  {
    firstName: 'Adibeth',
    lastName: 'Sanchez',
    email: 'adibeth.sanchez@umvel.com',
    password: '$2b$10$urd9l359swkW0t2HTeTbiujQzGqKMFW6fxdm2l3s/eDaaGZCtn6Ji',
    type: EnumUserTypes.ADMIN,
  },
  {
    firstName: 'Evelyn',
    lastName: 'Francisco',
    email: 'evelyn.francisco@umvel.com',
    password: '$2b$10$urd9l359swkW0t2HTeTbiujQzGqKMFW6fxdm2l3s/eDaaGZCtn6Ji',
    type: EnumUserTypes.INVESTOR,
  },
  {
    firstName: 'Gerardo',
    lastName: 'Lucio',
    email: 'gerardo.lucio+1@umvel.com',
    password: '$2b$10$urd9l359swkW0t2HTeTbiujQzGqKMFW6fxdm2l3s/eDaaGZCtn6Ji',
    type: EnumUserTypes.ADMIN,
  },
  {
    firstName: 'Richard',
    lastName: 'H',
    email: 'richard@capa.management',
    password: '$2b$10$urd9l359swkW0t2HTeTbiujQzGqKMFW6fxdm2l3s/eDaaGZCtn6Ji',
    type: EnumUserTypes.ADMIN,
  },
  {
    firstName: 'Gaston',
    lastName: 'Albergotti',
    email: 'gaston@capa.management',
    password: '$2b$10$urd9l359swkW0t2HTeTbiujQzGqKMFW6fxdm2l3s/eDaaGZCtn6Ji',
    type: EnumUserTypes.ADMIN,
  },
  {
    firstName: 'Jocelyn',
    lastName: 'Espinosa',
    email: 'jocelyn.espinosa@umvel.com',
    password: '$2b$10$urd9l359swkW0t2HTeTbiujQzGqKMFW6fxdm2l3s/eDaaGZCtn6Ji',
    type: EnumUserTypes.INVESTOR,
  },
  {
    firstName: 'Jocelyn',
    lastName: 'Espinosa',
    email: 'jocelyn.espinosa+1@umvel.com',
    password: '$2b$10$urd9l359swkW0t2HTeTbiujQzGqKMFW6fxdm2l3s/eDaaGZCtn6Ji',
    type: EnumUserTypes.ADMIN,
  },
];
