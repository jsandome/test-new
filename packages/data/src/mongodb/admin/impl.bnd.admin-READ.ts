import { injectable } from 'inversify';
import { BaseError, IBndAdminRead, IEntityAdmin } from '@capa/core';
import { IModelAdministrator, ModelAdministrator } from '../models';

@injectable()
export class ImplBndAdminReadMongo implements IBndAdminRead {
  public async getById(id: string): Promise<IEntityAdmin> {
    try {
      const admin: IModelAdministrator = await ModelAdministrator.findById(id);
      if (!admin) return null;

      const res = admin.toObject();
      return {
        id: res._id,
        name: res.name,
        phone: res.phone,
        type: res.type,
        active: res.active,
      };
    } catch (error) {
      throw new BaseError(
        `An error occurred while trying to get an administrator. ERROR ${error}`,
        500,
        'GenerateDBError',
        error
      );
    }
  }
}
