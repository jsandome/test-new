import { Document, Types } from 'mongoose';
import { EnumUserTypes, IBaseCreatedUpdatedBy, IBaseName } from '@capa/core';
import { IModelUser } from '.';

export interface IModelAdministrator extends Document {
  user: Types.ObjectId | IModelUser;
  name: IBaseName;
  phone: string;
  type: EnumUserTypes.ADMIN | EnumUserTypes.SUPERADMIN;
  active: boolean;
  createdBy: IBaseCreatedUpdatedBy;
  updatedBy: IBaseCreatedUpdatedBy;
}
