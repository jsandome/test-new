import { Document, Types } from 'mongoose';
import {
  EnumTwoFactorType,
  EnumUserTypes,
  IBaseCreatedUpdatedBy,
} from '@capa/core';

export interface IModelUser extends Document {
  id: string;
  email: string;
  password: string;
  admin: Types.ObjectId;
  investor: Types.ObjectId;
  tokenJwt: string;
  expirePass: {
    token: string;
    date: Date;
  };
  twoFactor: {
    type: EnumTwoFactorType;
    date: Date;
    code: string;
    secure: string;
    secret?: string;
    enabled: boolean;
  };
  type: EnumUserTypes;
  lastLogin: Date;
  active: boolean;
  createdBy: IBaseCreatedUpdatedBy;
  updatedBy: IBaseCreatedUpdatedBy;
}
