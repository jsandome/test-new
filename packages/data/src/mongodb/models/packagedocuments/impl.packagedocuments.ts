import { Schema, Model, model,Types } from 'mongoose';
import { IModelPackageDocuments } from '.';

class SchemaPackageDocuments extends Schema {
  constructor() {
    super(
      {
        name: {
          type: String,
          required: true,
        },
        templates: [
          {
            templateId: { type: String, default: null, required: true },
            name: { type: String, default: null, required: true },
            date: { type: Date, default: null, required: true },
          },
        ],
        createdBy: {
          id: {
            type: Types.ObjectId,
            ref: 'User',
            required: true,
          },
          dateTime: Date,
        },
        updatedBy: {
          id: {
            type: Types.ObjectId,
            ref: 'User',
            required: true,
          },
          dateTime: Date,
        },
        deletedBy: {
          id: { type: Types.ObjectId, required: false, ref: 'User'},
          dateTime: { type: Date, required: false },
        },
      },
      {
        timestamps: {
          createdAt: 'createdBy.dateTime',
          updatedAt: 'updatedBy.dateTime',
        },
      }
    );
  }
}

const schemaPackageDocuments = new SchemaPackageDocuments();
schemaPackageDocuments.index({name:1},  {collation: {locale: "en", strength: 2}})

export const ModelPackageDocuments: Model<IModelPackageDocuments> =
  model<IModelPackageDocuments>(
    'PackageDocument',
    schemaPackageDocuments
  );
