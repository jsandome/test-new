import { Document } from 'mongoose';
import { IBaseCreatedUpdatedBy, IBaseTemplateDocusing } from '@capa/core';

export interface IModelPackageDocuments extends Document {
  name: string;
  templates: IBaseTemplateDocusing[];
  createdBy: IBaseCreatedUpdatedBy;
  updatedBy: IBaseCreatedUpdatedBy;
  deletedBy?: IBaseCreatedUpdatedBy | null;
}
