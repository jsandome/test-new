import { EnumGCloudDocumentsStatus } from '@capa/core';
import { Document, Types } from 'mongoose';

interface IDocumentDate {
  id: Types.ObjectId;
  dateTime: string;
}

export interface IModelGCloudDocument extends Document {
  originalName: string;
  status: EnumGCloudDocumentsStatus;
  url: string;
  taxCenter: Boolean;
  createdBy: IDocumentDate;
  updatedBy: IDocumentDate;
  deletedBy: IDocumentDate;
}
