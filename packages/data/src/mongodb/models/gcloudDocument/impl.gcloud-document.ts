import { EnumGCloudDocumentsStatus } from '@capa/core';
import { Schema, model, Types } from 'mongoose';
import paginate from 'mongoose-paginate-v2';
import { IModelGCloudDocument } from '.';
class SchemaGCloudDocument extends Schema {
  constructor() {
    super({
      originalName: {
        type: String,
        required: true,
      },
      status: {
        type: String,
        enum: Object.values(EnumGCloudDocumentsStatus),
        default: EnumGCloudDocumentsStatus.INPROCESS,
      },
      url: {
        type: String,
        required: true,
      },
      taxCenter: {
        type: Boolean,
        default: null,
      },
      createdBy: {
        id: {
          type: Types.ObjectId,
          ref: 'User',
          required: true,
        },
        dateTime: String,
      },
      updatedBy: [
        {
          id: { type: String, default: null },
          dateTime: { type: String, default: null },
        },
      ],
      deletedBy: {
        id: { type: String, default: null },
        dateTime: { type: String, default: null },
      },
    });
  }
}

const schemaGCloudDocument = new SchemaGCloudDocument().plugin(paginate);

export const ModelGCloudDocument: any = model<IModelGCloudDocument>(
  'GCloudDocument',
  schemaGCloudDocument
);
