import { Schema, model, Types } from 'mongoose';
import { IModelPayment } from '.';
import paginate from 'mongoose-paginate-v2';

class SchemaPayment extends Schema {
  constructor() {
    super(
      {
        yieldPeriod: {
          type: String,
          required: true,
        },
        investor: {
          type: Types.ObjectId,
          ref: 'Investor',
          required: true,
        },
        amount: {
          type: Types.Decimal128,
          required: true,
        },
        description: {
          type: String,
          required: true,
        },
        distributionInvestor: {
          type: Types.ObjectId,
          ref: 'DistributionInvestor',
          required: true,
        },
        paymentType: {
          type: String,
          required: false,
        },
        paymentNumber: {
          type: String,
          required: true,
        },
        llc: {
          type: Types.ObjectId,
          ref: 'LLC',
          required: true,
        },
        createdBy: {
          id: { type: String, default: null },
          dateTime: Date,
        },
        updatedBy: {
          id: { type: String, default: null },
          dateTime: { type: Date, default: null },
        },
        deletedBy: {
          id: { type: String, default: null },
          dateTime: { type: Date, default: null },
        },
      },
      {
        timestamps: {
          createdAt: 'createdBy.dateTime',
          updatedAt: 'updatedBy.dateTime',
        },
      }
    );
  }
}

const schemaPayment = new SchemaPayment().plugin(paginate);

export const ModelPayment: any = model<IModelPayment>('Payment', schemaPayment);
