import { IModelInvestor, IModelDistributionInvestor, IModelLlc } from '../';
import { Types, Document } from 'mongoose';
import { IEnumTypesPayment } from '@capa/core';

export interface IModelPayment extends Document {
  yieldPeriod: string;
  investor: Types.ObjectId | IModelInvestor;
  amount: number;
  description: string;
  distributionInvestor: Types.ObjectId | IModelDistributionInvestor;
  paymentType: IEnumTypesPayment;
  paymentNumber: string;
  notes: string;
  llc: Types.ObjectId | IModelLlc;
}
