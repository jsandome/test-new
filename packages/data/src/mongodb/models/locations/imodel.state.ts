import { Document } from 'mongoose';

export interface IModelState extends Document {
  _id?:string;
  name: string;
  code: string;
}
