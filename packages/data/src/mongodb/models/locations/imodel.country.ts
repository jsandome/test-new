import {  Document } from 'mongoose';

export interface IModelCountry extends Document {
  name: string;
  code: string,
}
