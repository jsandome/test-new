import { Schema, Model, model } from 'mongoose';
import { IModelCountry } from '.';

class SchemaCountry extends Schema {
  constructor() {
    super({
      name: {
        type: String,
        required: true,
        unique:true
      },
      code: {
        type: String,
        required: true,
      },
    });
  }
}

const schemaCountry = new SchemaCountry();
export const ModelCountry: Model<IModelCountry> = model<IModelCountry>(
  'Country',
  schemaCountry
);
