import { Schema, Model, model } from 'mongoose';
import { IModelState } from '.';

class SchemaState extends Schema {
  constructor() {
    super({
      name: {
        type: String,
        required: true,
        unique:true
      },
      code: {
        type: String,
        required: true,
      },
    });
  }
}

const schemaState = new SchemaState();
export const ModelState: Model<IModelState> = model<IModelState>(
  'State',
  schemaState
);
