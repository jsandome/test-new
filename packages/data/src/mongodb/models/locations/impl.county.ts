import { Schema, Model, model } from 'mongoose';
import { IModelCounty } from '.';

class SchemaCounty extends Schema {
  constructor() {
    super({
      name: {
        type: String,
        required: true,
      },
      state: {
        type: Schema.Types.ObjectId,
        ref: 'State',
        required: true,
      },
      fips:{
        type: Number,
        required:true
      }
    });
  }
}


const schemaCounty = new SchemaCounty();
schemaCounty.index({name:1,fips:1},{unique:true});

export const ModelCounty: Model<IModelCounty> = model<IModelCounty>(
  'County',
  schemaCounty
);

