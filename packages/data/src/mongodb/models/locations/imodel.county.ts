import { Document, Types } from 'mongoose';
import { IModelState } from '.';

export interface IModelCounty extends Document {
  _id?: string;
  name: string;
  state: Types.ObjectId | IModelState;
  fips: number;
}
