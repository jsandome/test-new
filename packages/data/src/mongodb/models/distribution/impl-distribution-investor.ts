import { Schema, model, Types } from 'mongoose';
import { IModelDistributionInvestor } from '.';
import paginate from 'mongoose-paginate-v2';

class SchemaDistributionInvestor extends Schema {
  constructor() {
    super(
      {
        distribution: {
          type: Types.ObjectId,
          required: true,
          ref: 'Distribution',
        },
        investor: {
          type: Types.ObjectId,
          required: true,
          ref: 'Investor',
        },
        paymentType: {
          type: String,
          required: false,
        },
        paymentNumber: {
          type: String,
          required: true,
        },
        amount: {
          type: Types.Decimal128,
          default: false,
        },
        createdBy: {
          id: { type: String, default: null },
          dateTime: Date,
        },
        updatedBy: {
          id: { type: String, default: null },
          dateTime: { type: Date, default: null },
        },
        deletedBy: {
          id: { type: String, default: null },
          dateTime: { type: Date, default: null },
        },
      },
      {
        timestamps: {
          createdAt: 'createdBy.dateTime',
          updatedAt: 'updatedBy.dateTime',
        },
      }
    );
  }
}

const schemaDistributionInvestor = new SchemaDistributionInvestor().plugin(paginate);

export const ModelDistributionInvestor: any = 
  model<IModelDistributionInvestor>(
    'DistributionInvestor', 
    schemaDistributionInvestor
  );
