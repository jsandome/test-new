import { IBaseCreatedUpdatedBy, IEnumTypesPayment } from "@capa/core";
import { Types, Document } from "mongoose";
import { IModelDistribution, IModelInvestor } from "../";

export interface IModelDistributionInvestor extends Document {
  _id?: Types.ObjectId;
  distribution: Types.ObjectId | IModelDistribution;
  investor: Types.ObjectId | IModelInvestor;
  paymentType: IEnumTypesPayment;
  paymentNumber: string;
  amount: number;
  deletedBy?: IBaseCreatedUpdatedBy;
  updatedBy?: IBaseCreatedUpdatedBy;
  createdBy?: IBaseCreatedUpdatedBy;
}
