import { Schema, model, Types } from 'mongoose';
import { IModelDistribution } from '.';
import paginate from 'mongoose-paginate-v2';

class SchemaDistribution extends Schema {
  constructor() {
    super(
      {
        description: {
          type: String,
          require: true,
        },
        distributionDate: {
          type: Date,
          require: true,
        },
        yieldPeriod: {
          type: String,
          required: true,
        },
        llc: {
          type: Types.ObjectId,
          ref: 'LLC',
          required: true,
        },
        createdBy: {
          id: { type: String, default: null },
          dateTime: Date,
        },
        updatedBy: {
          id: { type: String, default: null },
          dateTime: { type: Date, default: null },
        },
        deletedBy: {
          id: { type: String, default: null },
          dateTime: { type: Date, default: null },
        },
        active: {
          type: Boolean,
          default: true,
        },
      },
      {
        timestamps: {
          createdAt: 'createdBy.dateTime',
          updatedAt: 'updatedBy.dateTime',
        },
      }
    );
  }
}

const schemaDistribution = new SchemaDistribution().plugin(paginate);

export const ModelDistribution: any = model<IModelDistribution>(
  'Distribution',
  schemaDistribution
);
