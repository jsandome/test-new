import { IBaseCreatedUpdatedBy } from '@capa/core';
import { Types, Document } from 'mongoose';
import { IModelLlc } from '../';

export interface IModelDistribution extends Document {
  _id?: Types.ObjectId;
  llc: Types.ObjectId | IModelLlc;
  description: string;
  distributionDate: Date;
  yieldPeriod: string;
  deletedBy?: IBaseCreatedUpdatedBy;
  updatedBy?: IBaseCreatedUpdatedBy;
  createdBy?: IBaseCreatedUpdatedBy;
  active: boolean;
}
