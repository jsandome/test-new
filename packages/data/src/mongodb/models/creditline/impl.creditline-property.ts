import { Schema, Model, model, Types } from 'mongoose';
import { IModelCreditlineProperty } from '.';

class SchemaCreditlineProperty extends Schema {
  constructor() {
    super({
      creditline: {
        type: Types.ObjectId,
        ref: 'Creditline',
        required: true,
      },
      property: {
        type: Types.ObjectId,
        ref: 'Property',
        required: true,
      },
      share: {
        type: Types.Decimal128,
        required: true,
      },
    });
  }
}

const schemaCreditlineProperty = new SchemaCreditlineProperty();

export const ModelCreditlineProperty: Model<IModelCreditlineProperty> =
  model<IModelCreditlineProperty>(
    'CreditlineProperty',
    schemaCreditlineProperty
  );
