import { Types, Document } from 'mongoose';

export interface IModelCreditlineProperty extends Document {
  _id: Types.ObjectId;
  creditline: Types.ObjectId;
  property: Types.ObjectId;
  share: number;
}
