import { Schema, Model, model, Types } from 'mongoose';
import { IModelCreditline } from '.';

class SchemaCreditline extends Schema {
  constructor() {
    super({
      name: {
        type: String,
        required: true,
      },
      principal: {
        type: Types.Decimal128,
        required: true,
      },
      interest: {
        type: Number,
        required: true,
      },
      extraPayments: [
        {
          name: {
            type: String,
            required: true,
          },
          date: {
            type: Date,
            required: true,
          },
          amount: {
            type: Types.Decimal128,
            required: true,
          },
        },
      ],
      active: {
        type: Boolean,
        default: false,
      },
    });
  }
}

const schemaCreditline = new SchemaCreditline();

export const ModelCreditline: Model<IModelCreditline> = model<IModelCreditline>(
  'Creditline',
  schemaCreditline
);
