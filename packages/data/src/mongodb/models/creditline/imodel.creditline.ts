import { Types, Document } from 'mongoose';
import { IExtraPayments } from '@capa/core';

export interface IModelCreditline extends Document {
  id?: Types.ObjectId;
  name: string;
  principal: number;
  interest: number;
  extraPayments: IExtraPayments[];
  active: boolean;
}
