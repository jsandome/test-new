import { Types, Document } from 'mongoose';
import { IBaseCreatedUpdatedBy } from '@capa/core';

export interface IModelContact extends Document {
  _id?: Types.ObjectId;
  name: string;
  jobTitle: string;
  companyName: string;
  currentJob: boolean;
  email: string;
  phone: string;
  address: string;
  city: string;
  county: string;
  state: string;
  zip: string;
  createdBy: IBaseCreatedUpdatedBy;
  updatedBy: IBaseCreatedUpdatedBy;
}
