import { Schema, model, Types } from 'mongoose';
import { IModelContact } from '.';
import paginate from 'mongoose-paginate-v2';

class SchemaContact extends Schema {
  constructor() {
    super(
      {
        name: {
          type: String,
          required: true,
        },
        jobTitle: {
          type: String,
          required: true,
        },
        companyName: {
          type: String,
          required: true,
        },
        currentJob: {
          type: Boolean,
          required: true,
        },
        email: {
          type: String,
          required: true,
        },
        phone: {
          type: String,
        },
        address: {
          type: String,
        },
        city: {
          type: String,
        },
        county: {
          type: Types.ObjectId,
        },
        state: {
          type: Types.ObjectId,
        },
        zip: {
          type: String,
        },
        createdBy: {
          id: { type: String, default: null },
          dateTime: Date,
        },
        updatedBy: {
          id: { type: String, default: null },
          dateTime: { type: Date, default: null },
        },
      },
      {
        timestamps: {
          createdAt: 'createdBy.dateTime',
          updatedAt: 'updatedBy.dateTime',
        },
      }
    );
  }
}

const schemaContact = new SchemaContact().plugin(paginate);

export const ModelContact: any = model<IModelContact>('Contact', schemaContact);
