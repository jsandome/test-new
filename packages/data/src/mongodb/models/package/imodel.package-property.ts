import { IModelProperty } from '../../models';
import { Types, Document } from 'mongoose';

export interface IModelPackageProperty extends Document {
  _id?: Types.ObjectId;
  package: Types.ObjectId;
  property: Types.ObjectId | IModelProperty;
  share: number;
}
