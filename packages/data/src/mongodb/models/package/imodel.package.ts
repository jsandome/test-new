import { Types, Document } from 'mongoose';

export interface IModelPackage extends Document {
  _id?: Types.ObjectId;
  name: string;
  totalValue: number;
  active: boolean;
}