import { Schema, Model, model, Types } from 'mongoose';
import { IModelPackageProperty } from '.';

class SchemaPackageProperty extends Schema {
  constructor() {
    super({
      package: {
        type: Types.ObjectId,
        ref: 'Package',
        required: true,
      },
      property: {
        type: Types.ObjectId,
        ref: 'Property',
        required: true,
      },
      share: {
        type: Types.Decimal128,
        required: true,
      },
    });
  }
}

const schemaPackageProperty = new SchemaPackageProperty();

export const ModelPackageProperty: Model<IModelPackageProperty> =
  model<IModelPackageProperty>('PackageProperty', schemaPackageProperty);
