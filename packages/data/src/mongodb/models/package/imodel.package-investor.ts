import { Types, Document } from 'mongoose';

export interface IModelPackageInvestor extends Document {
  _id?: Types.ObjectId;
  package: Types.ObjectId;
  investor: Types.ObjectId;
  minCommitment: number;
  initCommitment: number;
  totalCommitment: number;
  termYears: number;
}
