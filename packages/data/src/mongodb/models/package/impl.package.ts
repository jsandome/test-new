import { Schema, Model, model, Types } from 'mongoose';
import { IModelPackage } from './imodel.package';

class SchemaPackage extends Schema {
  constructor() {
    super({
      name: {
        type: String,
        required: true,
      },
      totalValue: {
        type: Types.Decimal128,
        required: true,
      },
      active: {
        type: Boolean,
        default: true,
      },
    });
  }
}

const schemaPackage = new SchemaPackage();

export const ModelPackage: Model<IModelPackage> = model<IModelPackage>(
  'Package',
  schemaPackage
);
