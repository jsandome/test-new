import { Schema, Model, model, Types } from 'mongoose';
import { IModelPackageInvestor } from '.';

class SchemaPackageInvestor extends Schema {
  constructor() {
    super({
      package: {
        type: Types.ObjectId,
        ref: 'Package',
        required: true,
      },
      investor: {
        type: Types.ObjectId,
        ref: 'Investor',
        required: true,
      },
      minCommitment: {
        type: Types.Decimal128,
        required: true,
      },
      initCommitment: {
        type: Types.Decimal128,
        required: true,
      },
      totalCommitment: {
        type: Types.Decimal128,
        required: true,
      },
      termYears: {
        type: Number,
        required: true,
      },
    });
  }
}

const schemaPackageInvestor = new SchemaPackageInvestor();

export const ModelPackageInvestor: Model<IModelPackageInvestor> =
  model<IModelPackageInvestor>('PackageInvestor', schemaPackageInvestor);
