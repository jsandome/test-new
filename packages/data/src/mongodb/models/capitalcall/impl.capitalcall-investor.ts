import { Schema, Model, model, Types } from 'mongoose';
import { IModelCapitalCallInvestor } from '.';

class SchemaCapitalCallInvestor extends Schema {
  constructor() {
    super(
      {
        investor: {
          type: Types.ObjectId,
          ref: 'Investor',
          required: true,
        },
        dueDate: {
          type: Date,
          require: true,
        },
        capitalAmount: {
          type: Types.Decimal128,
          require: true,
        },
        capitalCall: {
          type: Types.ObjectId,
          ref: 'CapitalCall',
          required: true,
        },
        createdBy: {
          id: { type: String, default: null },
          dateTime: Date,
        },
        updatedBy: {
          id: { type: String, default: null },
          dateTime: { type: Date, default: null },
        },
        deletedBy: {
          id: { type: String, default: null },
          dateTime: { type: Date, default: null },
        },
      },
      {
        timestamps: {
          createdAt: 'createdBy.dateTime',
          updatedAt: 'updatedBy.dateTime',
        },
      }
    );
  }
}

const schemaCapitalCallInvestor = new SchemaCapitalCallInvestor();

export const ModelCapitalCallInvestor: Model<IModelCapitalCallInvestor> =
  model<IModelCapitalCallInvestor>(
    'CapitalCallInvestor',
    schemaCapitalCallInvestor
  );
