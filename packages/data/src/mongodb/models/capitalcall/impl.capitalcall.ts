import { Schema, model, Types } from 'mongoose';
import { IModelCapitalCall } from '.';
import AggregatePaginate from 'mongoose-aggregate-paginate-v2';
import paginate from 'mongoose-paginate-v2';

class SchemaCapitalCall extends Schema {
  constructor() {
    super(
      {
        description: {
          type: String,
          require: true,
        },
        dueDate: {
          type: Date,
          require: true,
        },
        llc: {
          type: Types.ObjectId,
          ref: 'LLC',
          required: true,
        },
        isCompleted: {
          type: Boolean,
          default: false,
          required: false,
        },
        createdBy: {
          id: { type: String, default: null },
          dateTime: Date,
        },
        updatedBy: {
          id: { type: String, default: null },
          dateTime: { type: Date, default: null },
        },
        deletedBy: {
          id: { type: String, default: null },
          dateTime: { type: Date, default: null },
        },
      },
      {
        timestamps: {
          createdAt: 'createdBy.dateTime',
          updatedAt: 'updatedBy.dateTime',
        },
      }
    );
  }
}

const schemaCapitalCall = new SchemaCapitalCall()
  .plugin(AggregatePaginate)
  .plugin(paginate);

export const ModelCapitalCall: any = model<IModelCapitalCall>(
  'CapitalCall',
  schemaCapitalCall
);
