import { IModelCapitalCall, IModelInvestor } from '../';
import { Types, Document } from 'mongoose';
import { IBaseCreatedUpdatedBy } from '@capa/core';

export interface IModelCapitalCallInvestor extends Document {
  _id?: Types.ObjectId;
  investor: Types.ObjectId | IModelInvestor;
  capitalAmount: number;
  capitalCall: Types.ObjectId | IModelCapitalCall;
  deletedBy?: IBaseCreatedUpdatedBy;
  updatedBy?: IBaseCreatedUpdatedBy;
  createdBy?: IBaseCreatedUpdatedBy;
}
