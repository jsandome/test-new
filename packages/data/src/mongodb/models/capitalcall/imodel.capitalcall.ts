import { IBaseCreatedUpdatedBy } from '@capa/core';
import { Types, Document } from 'mongoose';

export interface IModelCapitalCall extends Document {
  _id?: Types.ObjectId;
  description: string;
  dueDate: Date;
  llc: Types.ObjectId | string;
  isCompleted: boolean;
  deletedBy?: IBaseCreatedUpdatedBy;
  updatedBy?: IBaseCreatedUpdatedBy;
  createdBy?: IBaseCreatedUpdatedBy;
}
