import { Document, Types } from 'mongoose';
import { IBaseCreatedUpdatedBy,
  IBaseOnboardingInformation,
  IBaseEntityContact,
  IBaseEntityProperty,
  IBaseEntityFunds,
  IBaseEntityPackage,
  IBasePackageDocuments } from '@capa/core';

export interface IModelOnboarding extends Document {
  email: string;
  envelopeId: string;
  url: string;
  token?: string;
  tokenDate?: Date;
  currentStep: string;
  nameContact?: string;
  contactId?: Types.ObjectId | IBaseEntityContact;  
  propertyId?: Types.ObjectId | IBaseEntityProperty;
  fundId?: Types.ObjectId | IBaseEntityFunds;
  packageId?: Types.ObjectId | IBaseEntityPackage;
  templateId?: string;
  packageDocumentId?: Types.ObjectId | IBasePackageDocuments;
  createdBy: IBaseCreatedUpdatedBy;
  updatedBy: IBaseCreatedUpdatedBy;
  basicInformation?: IBaseOnboardingInformation;
  subscriberDetails?: object;
  contats?: object[];
}
