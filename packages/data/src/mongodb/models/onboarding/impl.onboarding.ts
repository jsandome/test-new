import { Schema, Model, model, Types } from 'mongoose';
import { IModelOnboarding } from '.';
import { EnumTypeStepOnboarding } from '@capa/core';

class SchemaOnboarding extends Schema {
  constructor() {
    super(
      {
        email: {
          type: String,
          required: true,
        },
        envelopeId: {
          type: String,
          required: false,
        },
        url: {
          type: String,
          required: false,
        },
        token: {
          type: String,
          required: false,
        },
        tokenDate: {
          type: Date,
          required: false,
        },
        currentStep: {
          type: String,
          enum: Object.values(EnumTypeStepOnboarding),
          required: true,
          default: EnumTypeStepOnboarding.STEPONE,
        },
        nameContact: {
          type: String,
          required: false,
        },
        contactId: {
          type: Types.ObjectId,
          required: false,
          ref: 'Contact',
        },
        propertyId: {
          type: Types.ObjectId,
          required: false,
          ref: 'Property',
        },
        fundId: {
          type: Types.ObjectId,
          required: false,
          ref: 'Fund',
        },
        packageId: {
          type: Types.ObjectId,
          required: false,
          ref: 'Package',
        },
        templateId: {
          type: String,
          required: false,
        },
        packageDocumentId: {
          type: Types.ObjectId,
          required: false,
          ref: 'PackageDocument',
        },
        createdBy: {
          id: {
            type: Types.ObjectId,
            ref: 'User',
            required: true,
          },
          dateTime: Date,
        },
        updatedBy: {
          id: {
            type: Types.ObjectId,
            ref: 'User',
            required: true,
          },
          dateTime: Date,
        },
        basicInformation: {
          type: Object,
          required: false,
        },
        subscriberDetails: {
          type: Object,
          required: false,
        },
        contats: [
          {
            type: Object,
            required: false,
          },
        ],
      },
      {
        timestamps: {
          createdAt: 'createdBy.dateTime',
          updatedAt: 'updatedBy.dateTime',
        },
      }
    );
  }
}

const schemaOnboarding = new SchemaOnboarding();

export const ModelOnboarding: Model<IModelOnboarding> = model<IModelOnboarding>(
  'Onboarding',
  schemaOnboarding
);
