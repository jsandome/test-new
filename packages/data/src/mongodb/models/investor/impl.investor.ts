import { Types } from 'mongoose';
import { Schema,  model } from 'mongoose';
import { IModelInvestor } from '..';
import paginate from 'mongoose-paginate-v2';

class SchemaInvestor extends Schema {
  constructor() {
    super(
      {
        user: {
          type: Schema.Types.ObjectId,
          ref: 'User',
        },
        name: {
          firstName: {
            index: true,
            type: String,
          },
          lastName: {
            index: true,
            type: String,
          },
        },
        phone: {
          type: String,
          default: null,
        },
        // TODO Validate types for phoneType
        phoneType: {
          type: String,
          default: null,
        },
        email: {
          type: String,
          default: null,
        },
        emailType: {
          type: String,
          default: null,
        },
        active: {
          type: Boolean,
          default: true,
        },
        addresses: [
          {
            address: {
              type: String,
              default: null,
              required: false,
            },
            city: {
              type: String,
              default: null,
              required: false,
            },
            zipCode: {
              type: String,
              default: null,
              required: false,
            },
            county: {
              type: Types.ObjectId,
              ref: 'County',
              required: false,
            },
            state: {
              type: Types.ObjectId,
              ref: 'State',
              required: false,
            },
            isPrincipal: {
              type: Boolean,
              default: true,
              required: false,
            },
          },
        ],
        biographical: {
          name: {
            type: String,
            default: null,
            required: false,
          },
          city: {
            type: String,
            default: null,
            required: false,
          },
          jobTitle: {
            type: String,
            default: null,
            required: false,
          },
          companyName: {
            type: String,
            default: null,
            required: false,
          },
          referrer: {
            type: String,
            default: null,
            required: false,
          },
          assignee: {
            type: String,
            default: null,
            required: false,
          },
        },
        additionalDetails: {
          type: String,
          default: null,
          required: false,
        },
        createdBy: {
          id: { type: String, default: null },
          dateTime: Date,
        },
        updatedBy: {
          id: { type: String, default: null },
          dateTime: { type: Date, default: null },
        },
      },
      {
        timestamps: {
          createdAt: 'createdBy.dateTime',
          updatedAt: 'updatedBy.dateTime',
        },
      }
    );
  }
}

const schemaInvestor = new SchemaInvestor().plugin(paginate);
export const ModelInvestor: any = model<IModelInvestor>(
  'Investor',
  schemaInvestor
);
