import { Document, Types } from 'mongoose';
import {
  EnumEmailType,
  EnumPhoneType,
  IBaseCreatedUpdatedBy,
  IBaseInvestorAddress,
  IBaseInvestorBigraphical,
  IBaseName,
} from '@capa/core';
import { IModelUser } from '..';

export interface IModelInvestor extends Document {
  user: Types.ObjectId | IModelUser;
  name: IBaseName;
  phone: string;
  phoneType: EnumPhoneType;
  email: string;
  emailType: EnumEmailType;
  active: boolean;
  addresses: IBaseInvestorAddress[];
  biographical: IBaseInvestorBigraphical;
  additionalDetails: string;
  createdBy: IBaseCreatedUpdatedBy;
  updatedBy: IBaseCreatedUpdatedBy;
}
