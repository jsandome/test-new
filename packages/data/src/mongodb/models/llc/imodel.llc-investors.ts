import { Document } from 'mongoose';

export interface IModelLlcInvestor extends Document {
  llc: string;
  investor: string;
}
