import { Schema, Model, model, Types } from 'mongoose';
import { IModelLlcProperty } from '.';

class SchemaLlcProperty extends Schema {
  constructor() {
    super({
      llc: {
        type: Types.ObjectId,
        ref: 'LLC',
        required: true,
      },
      property: {
        type: Types.ObjectId,
        ref: 'Property',
        required: true,
      },
      share: {
        type: Types.Decimal128,
        required: true,
      },
    });
  }
}

const schemaLlcProperty = new SchemaLlcProperty();

export const ModelLlcProperty: Model<IModelLlcProperty> =
  model<IModelLlcProperty>('LlcProperty', schemaLlcProperty);
