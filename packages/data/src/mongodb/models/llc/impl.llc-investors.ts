import { Schema, Model, model, Types } from 'mongoose';
import { IModelLlcInvestor } from '.';

class SchemaLlcInvestor extends Schema {
  constructor() {
    super({
      llc: {
        type: Types.ObjectId,
        ref: 'LLC',
        required: true,
      },
      investor: {
        type: Types.ObjectId,
        ref: 'Investor',
        required: true,
      },
    });
  }
}

const schemaLlcInvestor = new SchemaLlcInvestor();

export const ModelLlcInvestor: Model<IModelLlcInvestor> =
  model<IModelLlcInvestor>('LlcInvestor', schemaLlcInvestor);
