import { Schema, model } from 'mongoose';
import { IModelLlc } from '.';
import AggregatePaginate from 'mongoose-aggregate-paginate-v2';

class SchemaLlc extends Schema {
  constructor() {
    super({
      state: {
        type: Schema.Types.ObjectId,
        ref: 'State',
        required: true,
      },
      name: { type: String, require: true },
      usPerson: { type: Boolean, require: true },
      taxId: { type: String, require: true },
      address: { type: String, require: true },
      date: { type: Date, require: true },
      active: { type: Boolean, default: true },
    });
  }
}

const schemaLlc = new SchemaLlc().plugin(AggregatePaginate);
export const ModelLlc: any = model<IModelLlc>('LLC', schemaLlc);
