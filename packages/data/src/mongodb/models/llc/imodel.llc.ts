import { Document } from 'mongoose';

export interface IModelLlc extends Document {
  id: string;
  state: string;
  name: string;
  usPerson: boolean;
  taxId: string;
  address: string;
  date: Date;
  active: boolean;
}
