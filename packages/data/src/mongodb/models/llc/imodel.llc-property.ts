import { Document } from 'mongoose';

export interface IModelLlcProperty extends Document {
  llc: string;
  property: string;
  share: number;
}
