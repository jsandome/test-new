import { Schema, model, Types } from 'mongoose';
import { IModelContribution } from '.';
import paginate from 'mongoose-paginate-v2';
import AggregatePaginate from 'mongoose-aggregate-paginate-v2';
class SchemaContribution extends Schema {
  constructor() {
    super(
      {
        receivedDate: {
          type: Date,
          required: true,
        },
        paymentAmount: {
          type: Types.Decimal128,
          required: true,
        },
        paymentMethod: {
          type: String,
          required: false,
        },
        isCompleted: {
          type: Boolean,
          default: false,
        },
        capitalCallInvestor: {
          type: Types.ObjectId,
          ref: 'CapitalCallInvestor',
        },
        checkNumber: {
          type: String,
          required: false,
        },
        notes: {
          type: String,
          required: false,
        },
        createdBy: {
          id: { type: String, default: null },
          dateTime: Date,
        },
        updatedBy: {
          id: { type: String, default: null },
          dateTime: { type: Date, default: null },
        },
        deletedBy: {
          id: { type: String, default: null },
          dateTime: { type: Date, default: null },
        },
        llc: { type: Types.ObjectId, ref: 'LLC' },
      },
      {
        timestamps: {
          createdAt: 'createdBy.dateTime',
          updatedAt: 'updatedBy.dateTime',
        },
      }
    );
  }
}

const schemaContribution = new SchemaContribution()
  .plugin(paginate)
  .plugin(AggregatePaginate);

export const ModelContribution: any = model<IModelContribution>(
  'Contribution',
  schemaContribution
);
