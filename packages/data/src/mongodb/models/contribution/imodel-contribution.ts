import { IBaseCreatedUpdatedBy, IEnumTypesPayment } from '@capa/core';
import { Types, Document, ObjectId } from 'mongoose';
import { IModelCapitalCallInvestor } from '../capitalcall';

export interface IModelContribution extends Document {
  _id?: Types.ObjectId;
  receivedDate: Date;
  paymentAmount: number;
  paymentMethod: IEnumTypesPayment;
  isCompleted: boolean;
  capitalCallInvestor: ObjectId | IModelCapitalCallInvestor;
  checkNumber?: string;
  notes: string;
  deletedBy?: IBaseCreatedUpdatedBy;
  updatedBy?: IBaseCreatedUpdatedBy;
  createdBy?: IBaseCreatedUpdatedBy;
  llc: Types.ObjectId;
}
