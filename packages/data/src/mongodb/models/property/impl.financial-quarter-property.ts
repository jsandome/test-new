import { Schema, Model, model, Types } from 'mongoose';
import { IModelPropertyFinancialQuarter } from '.';

class SchemaPropertyFinancialQuarter extends Schema {
  constructor() {
    super({
      propertyFinancial: {
        type: Types.ObjectId,
        ref: 'PropertyFinancial',
        required: true,
      },
      year: {
        type: Number,
        required: true,
      },
      quarter: {
        type: Number,
        required: true,
      },
      totalUnits: {
        type: Number,
        required: true,
      },
      rents: {
        month1: { type: Types.Decimal128, required: true },
        month2: { type: Types.Decimal128, required: true },
        month3: { type: Types.Decimal128, required: true },
      },
      propertyManagement: {
        type: Types.Decimal128,
        required: true,
      },
      assetManagement: {
        type: Types.Decimal128,
        required: true,
      },
      repairsMaintenance: {
        type: Types.Decimal128,
        required: true,
      },
      taxes: {
        type: Types.Decimal128,
        required: true,
      },
      insurance: {
        type: Types.Decimal128,
        required: true,
      },
      reserve: {
        type: Types.Decimal128,
        required: true,
      },
      vacancy: {
        type: Types.Decimal128,
        required: true,
      },
      operativeExpenses: [
        {
          name: {
            type: String,
            required: true,
          },
          total: {
            type: Types.Decimal128,
            required: true,
          },
        },
      ],
    });
  }
}

const schemaPropertyFinancialQuarter = new SchemaPropertyFinancialQuarter();
export const ModelPropertyFinancialQuarter: Model<IModelPropertyFinancialQuarter> =
  model<IModelPropertyFinancialQuarter>(
    'PropertyFinancialQuarter',
    schemaPropertyFinancialQuarter
  );
