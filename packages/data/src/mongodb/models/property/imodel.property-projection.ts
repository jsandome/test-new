import {
  IModelProperty,
  IModelPropertyProjectionYear,
  IModelPropertyProjectionLoan,
} from './';
import { Document, Types } from 'mongoose';
import { IPropertyProjectionExtraExpenses } from '@capa/core';

export interface IModelPropertyProjection extends Document {
  property: Types.ObjectId | IModelProperty;
  totalValue: number;
  purchasePrice: number;
  aquisitionFee: number;
  dispositionFee: number;
  amountAllocatedToLand: number;
  depreciation: number;
  commissions: number;
  downPayment: number;
  capitalGainsTaxPaid: number;
  loans: IModelPropertyProjectionLoan[];
  projectionYears: IModelPropertyProjectionYear[];
  extraExpenses: IPropertyProjectionExtraExpenses[];
}
