import { Schema, Model, model, Types } from 'mongoose';
import { IModelPropertyActualProfitsAndLoss } from '.';

class SchemaPropertyActualProfitsAndLoss extends Schema {
  constructor() {
    super({
      propertyActual: {
        type: Types.ObjectId,
        ref: 'PropertyActual',
        required: true,
      },
      dateInfo: {
        month: {
          type: String,
        },
        year: {
          type: String,
        },
      },
      income: {
        investmentRevenue: {
          type: Types.Decimal128,
        },
        ownership100: {
          type: Types.Decimal128,
        },
        totalInvestmentRevenue: {
          type: Types.Decimal128,
        },
        totalIncome: {
          type: Types.Decimal128,
        },
      },
      grossProfit: {
        type: Types.Decimal128,
      },
      expenses: {
        acquisitionFee: {
          type: Types.Decimal128,
        },
        bankChargesAndFees: {
          type: Types.Decimal128,
        },
        interestPaid: {
          type: Types.Decimal128,
        },
        investmentExpensesInfo: {
          investmentExpenses: {
            type: Types.Decimal128,
          },
          cleaningOrMaintenance: {
            type: Types.Decimal128,
          },
          dispositionFee: {
            type: Types.Decimal128,
          },
          landscaping: {
            type: Types.Decimal128,
          },
          managementFee_RPGInc: {
            type: Types.Decimal128,
          },
          propertyOnboardingFee: {
            type: Types.Decimal128,
          },
          repairs: {
            type: Types.Decimal128,
          },
          supplies: {
            type: Types.Decimal128,
          },
          utilitiesInfo: {
            utilities: {
              type: Types.Decimal128,
            },
            electricity: {
              type: Types.Decimal128,
            },
            water: {
              type: Types.Decimal128,
            },
            totalUtilities: {
              type: Types.Decimal128,
            },
          },
          totalInvestmentExpenses: {
            type: Types.Decimal128,
          },
        },
        legalAndProfessionalServices: {
          type: Types.Decimal128,
        },
        managementFee_CAPA: {
          type: Types.Decimal128,
        },
        otherBusinessExpenses: {
          type: Types.Decimal128,
        },
        propertyInsurance: {
          type: Types.Decimal128,
        },
        repairsAndMaintenance: {
          type: Types.Decimal128,
        },
        taxesAndLicenses: {
          type: Types.Decimal128,
        },
        totalExpenses: {
          type: Types.Decimal128,
        },
      },
      netOperatingIncome: {
        type: Types.Decimal128,
      },
      otherIncome: {
        type: Types.Decimal128,
      },
      gainOnSaleOfProperty: {
        type: Types.Decimal128,
      },
      totalOtherIncome: {
        type: Types.Decimal128,
      },
      netOtherIncome: {
        type: Types.Decimal128,
      },
      netIncome: {
        type: Types.Decimal128,
      },
    });
  }
}

const schemaPropertyActualProfitsAndLoss =
  new SchemaPropertyActualProfitsAndLoss();
export const ModelPropertyActualProfitsAndLoss: Model<IModelPropertyActualProfitsAndLoss> =
  model<IModelPropertyActualProfitsAndLoss>(
    'PropertyActualProfitsAndLoss',
    schemaPropertyActualProfitsAndLoss
  );
