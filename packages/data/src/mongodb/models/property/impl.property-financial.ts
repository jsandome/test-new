import { Schema, Model, model, Types } from 'mongoose';
import { IModelPropertyFinancial } from '.';

class SchemaPropertyFinancial extends Schema {
  constructor() {
    super({
      property: {
        type: Types.ObjectId,
        ref: 'Property',
        required: true,
      },
      totalValue: {
        type: Types.Decimal128,
        required: true,
      },
      initialInvestment: {
        type: Types.Decimal128,
        required: true,
      },
      purchasePrice: {
        type: Types.Decimal128,
        required: true,
      },
      aquisitionFee: {
        type: Types.Decimal128,
        required: true,
      },
      dispositionFee: {
        type: Types.Decimal128,
        required: true,
      },
      amountAllocatedToLand: {
        type: Types.Decimal128,
        required: true,
      },
      depreciation: {
        type: Types.Decimal128,
        required: true,
      },
      commissions: {
        type: Types.Decimal128,
        required: true,
      },
      downPayment: {
        type: Types.Decimal128,
        required: true,
      },
      capitalGainsTaxPaid: {
        type: Types.Decimal128,
        required: true,
      },
      salesPrice: {
        year1: {
          type: Types.Decimal128,
          required: true,
        },
        year2: {
          type: Types.Decimal128,
          required: true,
        },
        year3: {
          type: Types.Decimal128,
          required: true,
        },
        year4: {
          type: Types.Decimal128,
          required: true,
        },
        year5: {
          type: Types.Decimal128,
          required: true,
        },
      },
      quarters: [
        {
          type: Types.ObjectId,
          ref: 'PropertyFinancialQuarter',
        },
      ],
    });
  }
}

const schemaPropertyFinancial = new SchemaPropertyFinancial();
export const ModelPropertyFinancial: Model<IModelPropertyFinancial> =
  model<IModelPropertyFinancial>('PropertyFinancial', schemaPropertyFinancial);
