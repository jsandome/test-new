import { Document, Types } from 'mongoose';
import {
  EnumPropertyStatus,
  EnumPropertyTypes,
  IBaseCreatedUpdatedBy,
  IBasePropertyImportInformation,
  InterfacePropertyStrategies,
} from '@capa/core';
import {
  IModelPropertyFinancial,
  IModelPropertyProjection,
  IModelPropertyActual,
} from './';
import { IModelCounty, IModelState, IModelPropertyInvestor } from '..';

export interface IModelPictures {
  url: string;
}
export interface IModelProperty extends Document {
  _id?: Types.ObjectId;
  name: string;
  address: string;
  type: EnumPropertyTypes;
  state: Types.ObjectId | IModelState;
  county: Types.ObjectId | IModelCounty;
  bedRooms: number;
  bathRooms: number;
  size: number;
  parcelSize: number;
  parcelNumber: number;
  strategies: InterfacePropertyStrategies;
  propertyDescription: string;
  pictures: IModelPictures[] | string[];
  mainPicture: IModelPictures | string;
  status: EnumPropertyStatus;
  location: {
    lat: string;
    lng: string;
  };
  isDeleted: boolean;
  isComplete: boolean;
  actual: Types.ObjectId | IModelPropertyActual;
  propertyFinancial: Types.ObjectId | IModelPropertyFinancial;
  projection: Types.ObjectId | IModelPropertyProjection;
  investors: Types.ObjectId[] | IModelPropertyInvestor[];
  excelImportInformation?: IBasePropertyImportInformation;
  deletedBy?: IBaseCreatedUpdatedBy;
  updatedBy?: IBaseCreatedUpdatedBy;
  createdBy?: IBaseCreatedUpdatedBy;
}
