import { IModelPropertyProjection } from './';
import { Document, Types } from 'mongoose';

export interface IModelPropertyProjectionLoan extends Document {
  propertyProjection: IModelPropertyProjection | Types.ObjectId;
  amount: number;
  interest: number;
  termYears: number;
  paymentsPerYear: number;
  amortizationYears: number;
  ltc?: number;
}
