import { Document, Types } from 'mongoose';
import { IModelPropertyFinancialQuarter, IModelProperty } from './';

export interface IModelPropertyFinancial extends Document {
  property: Types.ObjectId | IModelProperty;
  totalValue: number;
  initialInvestment: number;
  purchasePrice: number;
  aquisitionFee: number;
  dispositionFee: number;
  amountAllocatedToLand: number;
  depreciation: number;
  commissions: number;
  downPayment: number;
  capitalGainsTaxPaid: number;
  salesPrice: {
    year1: number;
    year2: number;
    year3: number;
    year4: number;
    year5: number;
  };
  quarters?: IModelPropertyFinancialQuarter[] | Types.ObjectId[];
}
