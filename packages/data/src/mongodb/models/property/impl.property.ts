import { Schema, model, Types } from 'mongoose';
import { EnumPropertyTypes, EnumPropertyStatus } from '@capa/core';
import { IModelProperty } from '.';
import MongoosePaginate from 'mongoose-paginate-v2';

class SchemaProperty extends Schema {
  constructor() {
    super({
      name: {
        type: String,
        required: true,
      },
      address: {
        type: String,
        required: true,
      },
      type: {
        type: String,
        enum: Object.values(EnumPropertyTypes),
        required: true,
      },
      state: {
        type: Schema.Types.ObjectId,
        ref: 'State',
        required: true,
      },
      county: {
        type: Schema.Types.ObjectId,
        ref: 'County',
        required: true,
      },
      bedRooms: {
        type: Types.Decimal128,
        required: true,
      },
      bathRooms: {
        type: Types.Decimal128,
        required: true,
      },
      size: {
        type: Types.Decimal128,
        required: true,
      },
      parcelSize: {
        type: Types.Decimal128,
        required: true,
      },
      parcelNumber: {
        type: Types.Decimal128,
        required: true,
      },
      strategies: {
        type: Object,
      },
      propertyDescription: {
        type: String,
        required: true,
      },
      pictures: [
        {
          url: String,
        },
      ],
      mainPicture: {
        url: String,
      },
      location: {
        lat: String,
        lng: String,
      },
      isDeleted: {
        type: Boolean,
        default: false,
      },
      isComplete: {
        type: Boolean,
        default: false,
      },
      status: {
        type: String,
        enum: Object.values(EnumPropertyStatus),
        default: EnumPropertyStatus.ACTIVE,
      },
      propertyFinancial: {
        type: Schema.Types.ObjectId,
        ref: 'PropertyFinancial',
        default: null,
      },
      projection: {
        type: Schema.Types.ObjectId,
        ref: 'PropertyProjection',
        default: null,
      },
      actual: {
        type: Schema.Types.ObjectId,
        ref: 'PropertyActual',
        default: null,
      },
      investors: [
        {
          type: Schema.Types.ObjectId,
          ref: 'PropertyInvestor',
          default: null,
        },
      ],
      excelImportInformation: {
        type: {
          column: String,
          excelName: String,
          importDate: String,
          isValid: Boolean,
        },
        default: null,
      },
      createdBy: {
        id: { type: String, default: null },
        dateTime: Date,
      },
      updatedBy: {
        id: { type: String, default: null },
        dateTime: { type: Date, default: null },
      },
      deletedBy: {
        id: { type: String, default: null },
        dateTime: { type: Date, default: null },
      },
    });
  }
}

const schemaProperty = new SchemaProperty().plugin(MongoosePaginate);

//TODO find a way to enable typed of the propert model
export const ModelProperty: any = model<IModelProperty>(
  'Property',
  schemaProperty
);
