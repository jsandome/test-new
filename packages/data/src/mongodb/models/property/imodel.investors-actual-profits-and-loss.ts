import {
  IPropertyProfitsAndLossConcept,
  IPropertyProfitsAndLossDateInfo,
} from '@capa/core';
import { IModelPropertyActual } from './';
import { Types, Document } from 'mongoose';

export interface IModelPropertyActualProfitsAndLoss extends Document {
  dateInfo: IPropertyProfitsAndLossDateInfo;
  propertyActual: Types.ObjectId | IModelPropertyActual;
  income: IPropertyProfitsAndLossConcept;
  grossProfit: number;
  expenses: IPropertyProfitsAndLossConcept;
  netOperatingIncome: number;
  otherIncome: number;
  gainOnSaleOfProperty: number;
  totalOtherIncome: number;
  netOtherIncome: number;
  netIncome: number;
}
