import { Schema, Model, model, Types } from 'mongoose';
import { IModelPropertyInvestor } from '.';

class SchemaPropertyInvestor extends Schema {
  constructor() {
    super({
      investor: {
        type: Types.ObjectId,
        ref: 'Investor',
        required: true,
      },
      property: {
        type: Types.ObjectId,
        ref: 'Property',
        required: true,
      },
      minCommitment: {
        type: Types.Decimal128,
        required: true,
      },
      initCommitment: {
        type: Types.Decimal128,
        required: true,
      },
      totalCommitment: {
        type: Types.Decimal128,
        required: true,
      },
      termYears: {
        type: Types.Decimal128,
        required: true,
      },
    });
  }
}

const schemaPropertyInvestor = new SchemaPropertyInvestor();
export const ModelPropertyInvestor: Model<IModelPropertyInvestor> =
  model<IModelPropertyInvestor>('PropertyInvestor', schemaPropertyInvestor);
