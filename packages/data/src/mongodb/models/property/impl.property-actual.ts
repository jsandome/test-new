import { Schema, Model, model, Types } from 'mongoose';
import { IModelPropertyActual } from '.';

class SchemaPropertyActual extends Schema {
  constructor() {
    super({
      property: {
        type: Types.ObjectId,
        ref: 'Property',
        required: true,
      },
      profitsAndLoss: {
        type: [
          {
            type: Types.ObjectId,
            ref: 'PropertyActualProfitsAndLoss',
          },
        ],
        default: [],
      },
    });
  }
}

const schemaPropertyActual = new SchemaPropertyActual();
export const ModelPropertyActual: Model<IModelPropertyActual> =
  model<IModelPropertyActual>('PropertyActual', schemaPropertyActual);
