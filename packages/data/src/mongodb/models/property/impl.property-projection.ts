import { Schema, model, Types, Model } from 'mongoose';
import { IModelPropertyProjection } from './';

class SchemaPropertyProjection extends Schema {
  constructor() {
    super({
      property: {
        type: Types.ObjectId,
        ref: 'Property',
        required: true,
      },
      totalValue: {
        type: Types.Decimal128,
        required: true,
      },
      purchasePrice: {
        type: Types.Decimal128,
        required: true,
      },
      aquisitionFee: {
        type: Types.Decimal128,
        required: true,
      },
      dispositionFee: {
        type: Types.Decimal128,
        required: true,
      },
      amountAllocatedToLand: {
        type: Types.Decimal128,
        required: true,
      },
      depreciation: {
        type: Types.Decimal128,
        required: true,
      },
      commissions: {
        type: Types.Decimal128,
        required: true,
      },
      downPayment: {
        type: Types.Decimal128,
        required: true,
      },
      capitalGainsTaxPaid: {
        type: Types.Decimal128,
        required: true,
      },
      extraExpenses: {
        type: [
          {
            name: {
              type: String,
              required: true,
            },
            years: {
              type: [
                {
                  year: {
                    type: Number,
                    required: true,
                  },
                  amount: {
                    type: Types.Decimal128,
                    required: true,
                  },
                },
              ],
            },
          },
        ],
      },
      loans: {
        type: [
          {
            type: Types.ObjectId,
            ref: 'PropertyProjectionLoan',
          },
        ],
        default: [],
      },
      projectionYears: {
        type: [
          {
            type: Types.ObjectId,
            ref: 'PropertyProjectionYear',
          },
        ],
        default: [],
      },
    });
  }
}

const schemaPropertyProjection = new SchemaPropertyProjection();

export const ModelPropertyProjection: Model<IModelPropertyProjection> =
  model<IModelPropertyProjection>(
    'PropertyProjection',
    schemaPropertyProjection
  );
