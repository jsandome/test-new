import { Document, Types } from 'mongoose';
import { IModelProperty, IModelPropertyActualProfitsAndLoss } from './';

export interface IModelPropertyActual extends Document {
  property: Types.ObjectId | IModelProperty;
  profitsAndLoss: IModelPropertyActualProfitsAndLoss[];
}
