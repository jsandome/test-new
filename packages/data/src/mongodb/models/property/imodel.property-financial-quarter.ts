import { IBaseOperativeExpense } from '@capa/core';
import { Document, Types } from 'mongoose';
export interface IModelPropertyFinancialQuarter extends Document {
  _id?: Types.ObjectId;
  propertyFinancial: Types.ObjectId;
  year: number;
  quarter: number;

  totalUnits: number;
  rents: {
    month1: number;
    month2: number;
    month3: number;
  };
  propertyManagement: number;
  assetManagement: number;
  repairsMaintenance: number;
  taxes: number;
  insurance: number;
  reserve: number;
  vacancy: number;
  operativeExpenses?: IBaseOperativeExpense[];
}
