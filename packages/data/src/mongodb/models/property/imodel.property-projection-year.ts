import { IModelPropertyProjection } from './';
import { Document, Types } from 'mongoose';

export interface IModelPropertyProjectionYear extends Document {
  propertyProjection: Types.ObjectId | IModelPropertyProjection;
  year: number;
  units: number;
  rent: number;
  vacancy: number;
  propertyManagement: number;
  assetManagement: number;
  taxes: number;
  insurance: number;
  repairs: number;
  reserve: number;
  salesPrice: number;
}
