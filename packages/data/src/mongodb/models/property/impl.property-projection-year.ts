import { Schema, model, Types, Model } from 'mongoose';
import { IModelPropertyProjectionYear } from './';

class SchemaPropertyProjectionYear extends Schema {
  constructor() {
    super({
      propertyProjection: {
        type: Types.ObjectId,
        ref: 'PropertyProjection',
        required: true,
      },
      year: {
        type: Number,
        required: true,
      },
      units: {
        type: Types.Decimal128,
        required: true,
      },
      rent: {
        type: Types.Decimal128,
        required: true,
      },
      vacancy: {
        type: Types.Decimal128,
        required: true,
      },
      propertyManagement: {
        type: Types.Decimal128,
        required: true,
      },
      assetManagement: {
        type: Types.Decimal128,
        required: true,
      },
      taxes: {
        type: Types.Decimal128,
        required: true,
      },
      insurance: {
        type: Types.Decimal128,
        required: true,
      },
      repairs: {
        type: Types.Decimal128,
        required: true,
      },
      reserve: {
        type: Types.Decimal128,
        required: true,
      },
      salesPrice: {
        type: Types.Decimal128,
        default: null,
      },
    });
  }
}

const schemaPropertyProjectionYear = new SchemaPropertyProjectionYear();

export const ModelPropertyProjectionYear: Model<IModelPropertyProjectionYear> =
  model<IModelPropertyProjectionYear>(
    'PropertyProjectionYear',
    schemaPropertyProjectionYear
  );
