import { Schema, model, Types, Model } from 'mongoose';
import { IModelPropertyProjectionLoan } from './';

class SchemaPropertyProjectionLoan extends Schema {
  constructor() {
    super({
      propertyProjection: {
        type: Types.ObjectId,
        ref: 'PropertyProjection',
        required: true,
      },
      amount: {
        type: Types.Decimal128,
        required: true,
      },
      interest: {
        type: Types.Decimal128,
        required: true,
      },
      termYears: {
        type: Types.Decimal128,
        required: true,
      },
      paymentsPerYear: {
        type: Types.Decimal128,
        required: true,
      },
      amortizationYears: {
        type: Types.Decimal128,
        required: true,
      },
      ltc: {
        type: Types.Decimal128,
        default: null,
      },
    });
  }
}

const schemaPropertyProjectionLoan = new SchemaPropertyProjectionLoan();

export const ModelPropertyProjectionLoan: Model<IModelPropertyProjectionLoan> =
  model<IModelPropertyProjectionLoan>(
    'PropertyProjectionLoan',
    schemaPropertyProjectionLoan
  );
