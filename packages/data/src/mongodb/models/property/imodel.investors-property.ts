import { Document, Types } from 'mongoose';

export interface IModelPropertyInvestor extends Document {
  _id: Types.ObjectId;
  investor: Types.ObjectId;
  property?: Types.ObjectId;
  minCommitment: number;
  initCommitment: number;
  totalCommitment: number;
  termYears: number;
}
