import { Schema, Model, model, Types } from 'mongoose';
import { IModelLoanProperty } from '.';
class SchemaLoanProperty extends Schema {
  constructor() {
    super({
      property: {
        type: Types.ObjectId,
        ref: 'Property',
        required: true,
      },
      loan: {
        type: Types.ObjectId,
        ref: 'Loan',
        required: true,
      },
      payments: [
        {
          firstPayment: {
            type: Types.Decimal128,
            required: true,
          },
          finalPayment: {
            type: Types.Decimal128,
            required: true,
          },
          share: {
            type: Types.Decimal128,
            required: true,
          },
        },
      ],
    });
  }
}

const schemaLoanProperty = new SchemaLoanProperty();

export const ModelLoanProperty: Model<IModelLoanProperty> =
  model<IModelLoanProperty>('LoanProperty', schemaLoanProperty);
