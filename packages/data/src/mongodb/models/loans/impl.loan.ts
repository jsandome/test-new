import { Schema, Model, model, Types } from 'mongoose';
import { IModelLoan } from '.';
class SchemaLoan extends Schema {
  constructor() {
    super({
      name: {
        type: String,
        required: true,
      },
      amount: {
        type: Types.Decimal128,
        required: true,
      },
      interest: {
        type: Types.Decimal128,
        required: true,
      },
      amortizationYears: {
        type: Types.Decimal128,
        required: true,
      },
      termYears: {
        type: Types.Decimal128,
        required: true,
      },
      beginningDate: {
        type: String,
        required: true,
      },
      paymentsPerYear: {
        type: Number,
        default: 12,
      },
      active: {
        type: Boolean,
        default: true,
      },
      extraPayments: [
        {
          name: {
            type: String,
            required: true,
          },
          amount: {
            type: Types.Decimal128,
            required: true,
          },
          paymentNumbers: [
            {
              type: Number,
              required: true,
            },
          ],
        },
      ],
      properties: [
        {
          type: Schema.Types.ObjectId,
          ref: 'LoanProperty',
          required: true,
        },
      ],
      isDeleted: {
        type: Boolean,
        default: false,
        required: true,
      },
      createdBy: {
        id: { type: String, default: null },
        dateTime: Date,
      },
      updatedBy: {
        id: { type: String, default: null },
        dateTime: { type: Date, default: null },
      },
      deletedBy: {
        id: { type: String, default: null },
        dateTime: { type: Date, default: null },
      },
    });
  }
}

const schemaLoan = new SchemaLoan();

export const ModelLoan: Model<IModelLoan> = model<IModelLoan>(
  'Loan',
  schemaLoan
);
