import { ILoanExtraPayment } from '@capa/core';
import { IModelLoanProperty } from './';
import { Document, Types } from 'mongoose';

export interface IModelLoan extends Document {
  _id?: Types.ObjectId;
  name: string;
  amount: number;
  interest: number;
  amortizationYears: number;
  termYears: number;
  beginningDate: string;
  paymentsPerYear: number;
  extraPayments?: ILoanExtraPayment[];
  properties: Types.ObjectId[] | IModelLoanProperty[];
  isDeleted: boolean;
}
