import { IModelLoan, IModelProperty } from '../../';
import { Document, Types } from 'mongoose';

export interface IModelLoanPropertyPayments {
  firstPayment: number;
  finalPayment: number;
  share: number;
}
export interface IModelLoanProperty extends Document {
  _id?: Types.ObjectId;
  property: Types.ObjectId | IModelProperty;
  loan: Types.ObjectId | IModelLoan;
  payments: IModelLoanPropertyPayments[];
}

export interface ILoanPropertyUpdateResponse {
  newOrUpdatedProperties: IModelLoanProperty[];
  deletedproperties: IModelLoanProperty[];
}
