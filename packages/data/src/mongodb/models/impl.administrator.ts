import { Schema, Model, model } from 'mongoose';
import { EnumUserTypes } from '@capa/core';
import { IModelAdministrator } from '.';

class SchemaAdministrator extends Schema {
  constructor() {
    super(
      {
        user: {
          type: Schema.Types.ObjectId,
          ref: 'User',
        },
        name: {
          firstName: {
            index: true,
            type: String,
          },
          lastName: {
            index: true,
            type: String,
          },
        },
        phone: {
          type: String,
          default: null,
        },
        type: {
          type: String,
          enum: Object.values(EnumUserTypes),
          required: true,
        },
        active: {
          type: Boolean,
          default: true,
        },
        createdBy: {
          id: { type: String, default: null },
          dateTime: Date,
        },
        updatedBy: {
          id: { type: String, default: null },
          dateTime: { type: Date, default: null },
        },
      },
      {
        timestamps: {
          createdAt: 'createdBy.dateTime',
          updatedAt: 'updatedBy.dateTime',
        },
      }
    );
  }
}

const schemaAdministrator = new SchemaAdministrator();
export const ModelAdministrator: Model<IModelAdministrator> =
  model<IModelAdministrator>('Administrator', schemaAdministrator);
