import { Schema, Model, model } from 'mongoose';
import { EnumUserTypes } from '@capa/core';
import { IModelUser } from '.';

class SchemaUser extends Schema {
  constructor() {
    super(
      {
        email: {
          type: String,
          required: true,
          index: true,
          unique: true,
        },
        admin: {
          type: Schema.Types.ObjectId,
          ref: 'Administrator',
          default: null,
        },
        investor: {
          type: Schema.Types.ObjectId,
          ref: 'Investor',
          default: null,
        },
        password: {
          type: String,
          default: null,
          select: false,
        },
        tokenJwt: {
          type: String,
          default: null,
        },
        expirePass: {
          token: {
            type: String,
            default: null,
          },
          date: {
            type: Date,
            default: null,
          },
        },
        twoFactor: {
          code: {
            type: String,
            default: null,
          },
          secure: {
            type: String,
            default: null,
          },
          date: {
            type: Date,
            default: null,
          },
          type: {
            type: String,
            default: null,
          },
          secret: {
            type: String,
            default: null,
            select: false,
          },
          enabled: {
            type: Boolean,
            default: false,
          },
        },
        type: {
          type: String,
          enum: Object.values(EnumUserTypes),
          required: true,
        },
        lastLogin: {
          type: Date,
          default: null,
        },
        active: {
          type: Boolean,
          default: true,
        },
        createdBy: {
          id: { type: String, default: null },
          dateTime: Date,
        },
        updatedBy: {
          id: { type: String, default: null },
          dateTime: { type: Date, default: null },
        },
      },
      {
        timestamps: {
          createdAt: 'createdBy.dateTime',
          updatedAt: 'updatedBy.dateTime',
        },
      }
    );
  }
}

const schemaUser = new SchemaUser();
export const ModelUser: Model<IModelUser> = model<IModelUser>(
  'User',
  schemaUser
);
