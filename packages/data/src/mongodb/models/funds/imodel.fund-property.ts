import { IModelProperty } from '..';
import { Types, Document } from 'mongoose';

export interface IModelFundProperty extends Document {
  id: Types.ObjectId;
  property: Types.ObjectId | IModelProperty;
  share: number;
  fund: Types.ObjectId;
}
