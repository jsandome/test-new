import { Schema, Model, model, Types } from 'mongoose';
import { IModelFundInvestor } from '.';

class SchemaFundInvestor extends Schema {
  constructor() {
    super({
      fund: {
        type: Types.ObjectId,
        ref: 'Fund',
        required: true,
      },
      investor: {
        type: Types.ObjectId,
        ref: 'Investor',
        required: true,
      },
      minCommitment: {
        type: Types.Decimal128,
        required: true,
      },
      initCommitment: {
        type: Types.Decimal128,
        required: true,
      },
      totalCommitment: {
        type: Types.Decimal128,
        required: true,
      },
      termYears: {
        type: Number,
        required: true,
      },
    });
  }
}

const schemaFundInvestor = new SchemaFundInvestor();

export const ModelFundInvestor: Model<IModelFundInvestor> =
  model<IModelFundInvestor>('FundInvestor', schemaFundInvestor);
