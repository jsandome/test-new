import { IModelPackage } from '..';
import { Types, Document } from 'mongoose';

export interface IModelFundPackage extends Document {
  id: Types.ObjectId;
  package: Types.ObjectId | IModelPackage;
  share: number;
  fund: Types.ObjectId;
}
