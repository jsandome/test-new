import { IModelInvestor } from '..';
import { Document } from 'mongoose';

export interface IModelFundInvestor extends Document {
  fund: string;
  investor: string | IModelInvestor;
  minCommitment: number;
  initCommitment: number;
  totalCommitment: number;
  termYears: number;
}
