import { Schema, Model, model, Types } from 'mongoose';
import { IModelFund } from './imodel.fund';

class SchemaFund extends Schema {
  constructor() {
    super({
      name: {
        type: String,
        require: true,
      },
      cashHand: {
        type: Types.Decimal128,
        require: true,
      },
      active: {
        type: Boolean,
        default: true,
      },
    });
  }
}

const schemaFund = new SchemaFund();
export const ModelFund: Model<IModelFund> = model<IModelFund>(
  'Fund',
  schemaFund
);
