import { Schema, Model, model, Types } from 'mongoose';
import { IModelFundProperty } from '.';

class SchemaFundProperty extends Schema {
  constructor() {
    super({
      fund: {
        type: Types.ObjectId,
        ref: 'Fund',
        required: true,
      },
      property: {
        type: Types.ObjectId,
        ref: 'Property',
        required: true,
      },
      share: {
        type: Types.Decimal128,
        required: true,
      },
    });
  }
}

const schemaFundProperty = new SchemaFundProperty();

export const ModelFundProperty: Model<IModelFundProperty> =
  model<IModelFundProperty>('FundProperty', schemaFundProperty);
