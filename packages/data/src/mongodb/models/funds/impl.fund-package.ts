import { Schema, Model, model, Types } from 'mongoose';
import { IModelFundPackage } from '.';

class SchemaFundPackage extends Schema {
  constructor() {
    super({
      fund: {
        type: Types.ObjectId,
        ref: 'Fund',
        required: true,
      },
      package: {
        type: Types.ObjectId,
        ref: 'Package',
        required: true,
      },
      share: {
        type: Types.Decimal128,
        required: true,
      },
    });
  }
}

const schemaFundPackage = new SchemaFundPackage();

export const ModelFundPackage: Model<IModelFundPackage> =
  model<IModelFundPackage>('FundPackage', schemaFundPackage);
