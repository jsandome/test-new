import { Document, Types } from 'mongoose';

export interface IModelFund extends Document {
  _id?: Types.ObjectId;
  name: string;
  cashHand: number;
  active: boolean;
}
