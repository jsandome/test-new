import { EnumOfferingStatus, EnumOfferingType } from '@capa/core';
import { Schema, Model, model, Types } from 'mongoose';
import { IModelOfferingProspective } from '.';

class SchemaOfferingProspective extends Schema {
  constructor() {
    super({
      investor: {
        type: Types.ObjectId,
        required: true,
        ref: 'Investor',
      },
      offeringType: {
        type: EnumOfferingType,
        required: true,
      },
      offering: {
        type: Types.ObjectId,
        required: true,
      },
      status: {
        type: EnumOfferingStatus,
        required: true,
      },
      expectedAmount: {
        type: Types.Decimal128,
        required: true,
      },
      likeliHood: {
        type: Types.Decimal128,
        required: true,
      },
      additionalProspect: [
        {
          id: {
            type: Types.ObjectId,
            required: true,
          },
          primary: {
            type: Boolean,
            required: true,
          },
        },
      ],
    });
  }
}

const schemaOfferingProspective = new SchemaOfferingProspective();

export const ModelOfferingProspective: Model<IModelOfferingProspective> =
  model<IModelOfferingProspective>(
    'OfferingProspective',
    schemaOfferingProspective
  );
