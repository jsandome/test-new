import { EnumOfferingType, EnumOfferingStatus } from '@capa/core';
import { Types, Document } from 'mongoose';

export interface IAdditionalProspect {
  id: Types.ObjectId;
  primary: boolean;
}

export interface IModelOfferingProspective extends Document {
  _id?: Types.ObjectId;
  investor: Types.ObjectId;
  offeringType: EnumOfferingType;
  offering: Types.ObjectId;
  status: EnumOfferingStatus;
  expectedAmount: number;
  likeliHood: number;
  additionalProspect: IAdditionalProspect[];
}
