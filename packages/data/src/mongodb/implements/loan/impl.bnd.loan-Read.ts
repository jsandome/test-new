import { injectable } from 'inversify';
import {
  BaseError,
  IBaseEntityLoan,
  IBndBaseLoanRead,
  IGetLoanByNameParams,
  ILoanExtraPayment,
  ILoanPropertyResponse,
} from '@capa/core';
import { ModelLoan, IModelLoan, IModelLoanProperty } from '../../models';
import { _gDN } from '../general/impl.gral.decimal';
import { Types } from 'mongoose';
@injectable()
export class ImplBndBaseLoanRead implements IBndBaseLoanRead {
  //TODO set Loan type when ready
  public async getLoans(search: string): Promise<IBaseEntityLoan[]> {
    try {
      const searchObject = {};
      if (search) {
        searchObject['name'] = {
          $regex: search,
          $options: 'i',
        };
      }
      const loans: IModelLoan[] = await ModelLoan.find(searchObject).populate(
        'properties'
      );

      return loans.map((l) => this._getLoanObject(l));
    } catch (error) {
      throw new BaseError(
        `An error occurred while trying to get loans: ${error.message}`,
        error.code,
        error.name,
        error
      );
    }
  }

  public async findLoanByIdIfExist(id: string): Promise<IBaseEntityLoan> {
    try {
      const loan: IModelLoan = await ModelLoan.findOne({ _id: id }).populate(
        'properties'
      );
      if (loan) {
        return this._getLoanObject(loan);
      }
      return null;
    } catch (error) {
      throw new BaseError(
        `An error occurred while trying to get loan: ${error.message}`,
        error.code,
        error.name,
        error
      );
    }
  }

  public async getLoanByNameIfExist(
    name: string,
    params: IGetLoanByNameParams
  ): Promise<IBaseEntityLoan> {
    const searchObject = { name: name };
    if (params.isDeleted !== undefined) {
      searchObject['isDeleted'] = params.isDeleted;
    }

    try {
      const loan: IModelLoan = await ModelLoan.findOne(searchObject).populate(
        'properties'
      );
      if (loan) {
        const response = this._getLoanObject(loan);
        return response;
      }
      return null;
    } catch (error) {
      throw new BaseError(
        `An error occurred while trying to get a loan by name: ${error.message}`,
        error.code,
        error.name,
        error
      );
    }
  }

  private _getLoanObject(loan: IModelLoan): IBaseEntityLoan {
    return {
      id: loan._id.toHexString(),
      name: loan.name,
      amount: _gDN(loan.amount),
      interest: _gDN(loan.interest),
      amortizationYears: _gDN(loan.amortizationYears),
      termYears: _gDN(loan.termYears),
      beginningDate: loan.beginningDate,
      extraPayments: loan.extraPayments.map((ep): ILoanExtraPayment => {
        return {
          amount: _gDN(ep.amount),
          name: ep.name,
          paymentNumbers: ep.paymentNumbers,
        };
      }),
      properties: (loan.properties as IModelLoanProperty[]).map(
        (p): ILoanPropertyResponse => {
          return {
            id: (p.property as Types.ObjectId).toHexString(),
            share: p.payments.length > 0 ? _gDN(p.payments[0].share) : null,
          };
        }
      ),
    };
  }
}
