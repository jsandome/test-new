import { DateTime } from 'luxon';
import BigNumber from 'bignumber.js';
import { Types } from 'mongoose';

export interface IASMontlyPayment {
  paymentNumber: number;
  date: string;
  beginningBalance: BigNumber;
  extraPayment: BigNumber;
  totalPayment: BigNumber;
  interest: BigNumber;
  principal: BigNumber;
  endingBalance: BigNumber;
  accumulativeInterest: BigNumber;
  schedulePayment: BigNumber;
  propertiesPayment: IASPropertyPayment[];
}

export interface IASPropertyPayment {
  id: string;
  name: string;
  address: string;
  payment: BigNumber;
  principalPayment: BigNumber;
  share: number;
}

export interface IExtraPaymentCalc {
  beginningBalance: BigNumber;
  scheduleExtraPayment: BigNumber;
  schedulePayment: BigNumber;
  extraPayments: BigNumber[];
}

export interface IExtraPaymentParam {
  amount: BigNumber;
  name?: string;
  paymentNumbers: number[];
}
export interface IPaymentProperty {
  firstPayment: BigNumber;
  finalPayment: BigNumber;
  share: BigNumber;
}

export interface IShareProperty {
  _id: Types.ObjectId;
  payments: IPaymentProperty[];
}

export interface IPropertyGralData {
  id: string;
  name: string;
  address: string;
  share: BigNumber;
  loanAmount: BigNumber;
  loanPayment: BigNumber;
}

export interface IScheduleAmortizationRAWDataParams {
  interest: BigNumber;
  amortizationYears: BigNumber;
  loanAmount: BigNumber;
  scheduleExtraPayment?: BigNumber;
  paymentsPerYear: BigNumber;
  beginningDate: DateTime;
  extraPayments: IExtraPaymentParam[];
  propertiesShare: IShareProperty[];
  estimatedNumberOfPayments: BigNumber;
}

export interface IScheduleAmortizationRAWResponse {
  interest: BigNumber;
  amount: BigNumber;
  programedNumberPayments: BigNumber;
  realNumberPayments: BigNumber;
  schedulePayment: BigNumber;
  paymentsPerYear: BigNumber;
  amortizationYears: BigNumber;
  initialLoanAmount: BigNumber;
  scheduleExtraPayment: BigNumber;
  startDate: string;
  totalEarlyPayments: BigNumber;
  totalInterest: BigNumber;
  generalPropertyInfo: IPropertyGralData[];
  amortizationSchedule: IASMontlyPayment[];
}
