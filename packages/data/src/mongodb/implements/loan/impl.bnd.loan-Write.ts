import { injectable } from 'inversify';
import {
  BaseError,
  IBaseEntityLoan,
  IBaseEntityLoanCreateResponse,
  IBndBaseLoanWrite,
  ILoanPropertyResponse,
} from '@capa/core';
import {
  ModelLoan,
  IModelLoan,
  IModelLoanProperty,
  ModelLoanProperty,
  ILoanPropertyUpdateResponse,
} from '../../models';
import { Types, startSession } from 'mongoose';
import BigNumber from 'bignumber.js';

@injectable()
export class ImplBndBaseLoanWriteMongo implements IBndBaseLoanWrite {
  public async createLoan(
    loanData: IBaseEntityLoan
  ): Promise<IBaseEntityLoanCreateResponse> {
    try {
      const newLoan: IModelLoan = new ModelLoan({
        name: loanData.name,
        amount: loanData.amount,
        interest: loanData.interest,
        amortizationYears: loanData.amortizationYears,
        termYears: loanData.termYears,
        beginningDate: loanData.beginningDate,
        extraPayments: loanData.extraPayments,
      });
      const totalPayments = new BigNumber(
        loanData.amortizationYears
      ).multipliedBy(loanData.paymentsPerYear || 12);
      const { newOrUpdatedProperties } =
        await this._createOrUpdateLoanPropertiesInvestors(
          loanData.properties as ILoanPropertyResponse[],
          newLoan._id,
          totalPayments
        );
      newLoan.properties = newOrUpdatedProperties.map(
        (property) => property._id
      );
      await ModelLoanProperty.insertMany(newOrUpdatedProperties);
      await newLoan.save();
      return {
        id: newLoan._id.toHexString(),
      };
    } catch (error) {
      throw new BaseError(
        `An error occurred while trying to create a loan: ${error.message}`,
        error.code,
        error.name,
        error
      );
    }
  }

  public async deleteLoan(loanId: string): Promise<void> {
    try {
      //TODO make the logic about the logic delete
      const loan: IModelLoan = await ModelLoan.findOne({ _id: loanId });
      if (!loan)
        throw new BaseError(
          'Loan not found.',
          404,
          'ImplBndBaseLoanWriteMongo | deleteLoan'
        );
      await loan.deleteOne();
    } catch (error) {
      throw new BaseError(
        `An error occurred while trying to create a loan: ${error.message}`,
        error.code,
        error.name,
        error
      );
    }
  }

  public async updateLoan(
    loanData: IBaseEntityLoan,
    loanId: string
  ): Promise<void> {
    try {
      const session = await startSession();
      session.startTransaction();
      const loan: IModelLoan = await ModelLoan.findOne({ _id: loanId });
      const totalPayments = new BigNumber(
        loanData.amortizationYears
      ).multipliedBy(loanData.paymentsPerYear || 12);
      const { newOrUpdatedProperties, deletedproperties } =
        await this._createOrUpdateLoanPropertiesInvestors(
          loanData.properties as ILoanPropertyResponse[],
          loan._id,
          totalPayments
        );
      loan.properties = newOrUpdatedProperties.map((property) => property._id);
      for (let i = 0; i < newOrUpdatedProperties.length; i++) {
        await newOrUpdatedProperties[i].save({ session });
      }
      await ModelLoanProperty.deleteMany({
        _id: {
          $in: deletedproperties.map((p) => {
            return p._id;
          }),
        },
      });
      if (!loan)
        throw new BaseError(
          'Loan not found.',
          404,
          'ImplBndBaseLoanWriteMongo| updateLoan'
        );
      loan.name = loanData.name;
      loan.amount = loanData.amount;
      loan.interest = loanData.interest;
      loan.amortizationYears = loanData.amortizationYears;
      loan.termYears = loanData.termYears;
      loan.beginningDate = loanData.beginningDate;
      loan.extraPayments = loanData.extraPayments;
      await loan.save({ session });
      await session.commitTransaction();
      session.endSession();
    } catch (error) {
      throw new BaseError(
        `An error occurred while trying to update a loan: ${error.message}`,
        error.code,
        error.name,
        error
      );
    }
  }

  private async _createOrUpdateLoanPropertiesInvestors(
    properties: ILoanPropertyResponse[],
    loanId: Types.ObjectId,
    totalPayments: BigNumber
  ): Promise<ILoanPropertyUpdateResponse> {
    const newOrUpdatedProperties: IModelLoanProperty[] = [];
    let currentProperties: IModelLoanProperty[] = await ModelLoanProperty.find({
      loan: loanId,
    });
    for (let i = 0; i < properties.length; i++) {
      const index = currentProperties.findIndex(
        (cProperty) =>
          (cProperty.property as Types.ObjectId).toHexString() ===
          properties[i].id
      );
      if (index > -1) {
        const property: IModelLoanProperty = this._updateLoanPropertyObject(
          currentProperties[index],
          properties[i],
          totalPayments
        );
        newOrUpdatedProperties.push(property);
        currentProperties.splice(index, 1);
      } else {
        const property: IModelLoanProperty = this._updateLoanPropertyObject(
          new ModelLoanProperty(),
          properties[i],
          totalPayments
        );
        property.property = Types.ObjectId(properties[i].id);
        property.loan = loanId;
        newOrUpdatedProperties.push(property);
      }
    }
    return {
      newOrUpdatedProperties,
      deletedproperties: currentProperties,
    };
  }

  private _updateLoanPropertyObject(
    property: IModelLoanProperty,
    propertyData: ILoanPropertyResponse,
    totalPayments: BigNumber
  ): IModelLoanProperty {
    property.payments = [
      {
        finalPayment: totalPayments.toNumber(),
        firstPayment: 1,
        share: propertyData.share,
      },
    ];
    return property;
  }
}
