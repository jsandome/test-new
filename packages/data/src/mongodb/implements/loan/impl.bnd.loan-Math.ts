import { injectable } from 'inversify';
import {
  IBndBaseLoanMath,
  IScheduleAmortizationInputBase,
  IScheduleAmortizationResponse,
  IMonthlyAmortizationResponse,
  BaseError,
  IPropertyPaymentAmortizationResponse,
  IPropertySheduleGralData,
} from '@capa/core';
import { DateTime } from 'luxon';
import * as FJS from '@formulajs/formulajs';
import BigNumber from 'bignumber.js';
import { ModelProperty } from '../../models';
import { Types } from 'mongoose';

import {
  IASMontlyPayment,
  IASPropertyPayment,
  IExtraPaymentCalc,
  IExtraPaymentParam,
  IPaymentProperty,
  IShareProperty,
  IPropertyGralData,
  IScheduleAmortizationRAWDataParams,
  IScheduleAmortizationRAWResponse,
} from '.';

export const getSchedulePayment = (
  interestRate: BigNumber,
  paymentsPerYear: BigNumber,
  amortizationYears: BigNumber,
  loanAmount: BigNumber
): BigNumber => {
  const rate: BigNumber = interestRate.dividedBy(paymentsPerYear);
  const periods: BigNumber = amortizationYears.multipliedBy(paymentsPerYear);
  const present: BigNumber = loanAmount;
  return new BigNumber(FJS.PMT(rate, periods, present));
};

const getMontlyExtraPayment = (data: IExtraPaymentCalc): BigNumber => {
  const monthExtraPayment: BigNumber =
    data.extraPayments.length === 0
      ? new BigNumber(0)
      : data.extraPayments.reduce((a, c) => a.plus(c));
  if (
    monthExtraPayment
      .plus(data.scheduleExtraPayment)
      .plus(data.schedulePayment)
      .isLessThan(data.beginningBalance)
  ) {
    return monthExtraPayment.plus(data.scheduleExtraPayment);
  } else if (
    data.beginningBalance.minus(data.schedulePayment).isGreaterThan(0)
  ) {
    return data.beginningBalance.minus(data.schedulePayment);
  } else {
    return new BigNumber(0);
  }
};

const getMonthDate = (
  startDate: DateTime,
  paymentsPerYear: BigNumber,
  paymentNumber: BigNumber
): string => {
  let date: DateTime = startDate.plus({
    months: paymentNumber
      .multipliedBy(12)
      .dividedBy(paymentsPerYear)
      .toNumber(),
  });
  const daysOfTheMonth: BigNumber = new BigNumber(date.endOf('month').day);
  if (daysOfTheMonth.isLessThan(startDate.day)) {
    const daysToAdd: BigNumber = new BigNumber(startDate.day).minus(
      daysOfTheMonth
    );
    date = date.plus({ days: daysToAdd.toNumber() });
  }
  return date.toISO();
};
const getPropertyPayment = (
  properties: IShareProperty[],
  propertyGralInfo,
  payment: BigNumber,
  principal: BigNumber,
  paymentNumber: number
): IASPropertyPayment[] => {
  const response: IASPropertyPayment[] = [];
  for (let property of properties) {
    if (!propertyGralInfo[`${property._id}`]) {
      throw new BaseError(
        `Cannot found name of property with Id ${property._id}`,
        404,
        `An error occurred while trying to get property share (amortization shedule)`
      );
    }

    const currentShare: IPaymentProperty = property.payments.find(
      (payment) =>
        payment.firstPayment.isLessThanOrEqualTo(paymentNumber) &&
        payment.finalPayment.isGreaterThanOrEqualTo(paymentNumber)
    );
    if (currentShare) {
      const principalPayment: BigNumber = principal.multipliedBy(
        currentShare.share.dividedBy(100)
      );
      const propertyPayment: BigNumber = payment.multipliedBy(
        currentShare.share.dividedBy(100)
      );
      propertyGralInfo[`${property._id}`].loanAmount =
        propertyGralInfo[`${property._id}`].loanAmount.plus(principalPayment);
      propertyGralInfo[`${property._id}`].loanPayment =
        propertyGralInfo[`${property._id}`].loanPayment.plus(propertyPayment);
      response.push({
        id: property._id.toHexString(),
        name: propertyGralInfo[`${property._id}`].name,
        address: propertyGralInfo[`${property._id}`].address,
        share: currentShare.share.toNumber(),
        payment: propertyPayment,
        principalPayment: principalPayment,
      });
    }
  }
  return response;
};

export const getPropertyInfoObject = async (
  propertiesIds: Types.ObjectId[]
) => {
  const properties = await ModelProperty.find({
    _id: { $in: propertiesIds },
  }).select('name address');
  const propertyResponse = {};
  for (let property of properties) {
    propertyResponse[`${property.id}`] = {
      id: property.id,
      name: property.name,
      address: property.address,
      loanAmount: new BigNumber(0),
      loanPayment: new BigNumber(0),
    };
  }

  return propertyResponse;
};
const getGeneralPropertyInfoResponse = (
  loanAmount: BigNumber,
  propertyInfoResponse
): IPropertyGralData[] => {
  const response: IPropertyGralData[] = [];
  for (let key in propertyInfoResponse) {
    response.push({
      id: `${key}`,
      name: propertyInfoResponse[`${key}`].name,
      address: propertyInfoResponse[`${key}`].address,
      share: propertyInfoResponse[`${key}`].loanAmount.dividedBy(loanAmount),
      loanAmount: propertyInfoResponse[`${key}`].loanAmount,
      loanPayment: propertyInfoResponse[`${key}`].loanPayment,
    });
  }
  return response;
};

export const getAmortizationScheduleRawData = (
  data: IScheduleAmortizationRAWDataParams,
  generalPropertyInfoObject
): IScheduleAmortizationRAWResponse => {
  const {
    interest,
    paymentsPerYear,
    amortizationYears,
    loanAmount,
    estimatedNumberOfPayments,
    extraPayments,
    propertiesShare,
    beginningDate,
  } = data;
  const scheduleExtraPayment: BigNumber =
    data.scheduleExtraPayment || new BigNumber(0);
  const schedulePayment: BigNumber = getSchedulePayment(
    interest,
    paymentsPerYear,
    amortizationYears,
    loanAmount
  ).multipliedBy(-1);
  const montlyPayments: IASMontlyPayment[] = [];
  let accumulativeInterest: BigNumber = new BigNumber(0);
  let totalExtraPayments: BigNumber = new BigNumber(0);
  const startDate: string = getMonthDate(
    beginningDate,
    paymentsPerYear,
    new BigNumber(1)
  );
  for (
    let paymentNumber = 1;
    paymentNumber <= estimatedNumberOfPayments.toNumber();
    paymentNumber++
  ) {
    const beginningBalance: BigNumber =
      paymentNumber > 1
        ? montlyPayments[paymentNumber - 2].endingBalance
        : loanAmount;
    if (beginningBalance.isEqualTo(0)) {
      break;
    }

    const adjustedInterest = beginningBalance
      .multipliedBy(interest)
      .dividedBy(paymentsPerYear);
    const monthExtraPayment: BigNumber = getMontlyExtraPayment({
      beginningBalance,
      scheduleExtraPayment,
      schedulePayment,
      extraPayments: extraPayments
        .filter((p) => p.paymentNumbers.includes(paymentNumber))
        .map((p) => new BigNumber(p.amount)),
    });
    totalExtraPayments = totalExtraPayments.plus(monthExtraPayment);

    const principal: BigNumber =
      monthExtraPayment
        .plus(schedulePayment)
        .isGreaterThanOrEqualTo(beginningBalance.plus(adjustedInterest)) ||
      estimatedNumberOfPayments.isEqualTo(paymentNumber)
        ? beginningBalance
        : monthExtraPayment.plus(schedulePayment).minus(adjustedInterest);
    const interestImport: BigNumber = monthExtraPayment
      .plus(schedulePayment)
      .isGreaterThanOrEqualTo(principal.plus(adjustedInterest))
      ? adjustedInterest
      : monthExtraPayment.plus(schedulePayment).minus(principal);
    const totalPayment: BigNumber = interestImport.plus(principal);
    const endingBalance = beginningBalance.minus(principal);
    const propertiesPayment = getPropertyPayment(
      propertiesShare,
      generalPropertyInfoObject,
      totalPayment,
      principal,
      paymentNumber
    );

    accumulativeInterest = accumulativeInterest.plus(interestImport);
    const montlyPayment: IASMontlyPayment = {
      paymentNumber: paymentNumber,
      date: getMonthDate(
        beginningDate,
        paymentsPerYear,
        new BigNumber(paymentNumber)
      ),
      beginningBalance: beginningBalance,
      extraPayment: monthExtraPayment,
      totalPayment: totalPayment,
      interest: interestImport,
      principal: principal,
      endingBalance: endingBalance,
      accumulativeInterest: accumulativeInterest,
      schedulePayment,
      propertiesPayment: propertiesPayment,
    };
    montlyPayments.push(montlyPayment);
  }
  const generalPropertyInfoResponse: IPropertyGralData[] =
    getGeneralPropertyInfoResponse(loanAmount, generalPropertyInfoObject);
  return {
    interest: interest,
    amount: loanAmount,
    programedNumberPayments: estimatedNumberOfPayments,
    realNumberPayments: new BigNumber(montlyPayments.length),
    schedulePayment: schedulePayment,
    paymentsPerYear: paymentsPerYear,
    amortizationYears: amortizationYears,
    initialLoanAmount: loanAmount,
    scheduleExtraPayment: scheduleExtraPayment,
    startDate: startDate,
    totalEarlyPayments: totalExtraPayments,
    totalInterest: accumulativeInterest,
    generalPropertyInfo: generalPropertyInfoResponse,
    amortizationSchedule: montlyPayments,
  };
};

@injectable()
export class ImplBndBaseLoanMath implements IBndBaseLoanMath {
  public async getAmortizationSchedule(
    loanData: IScheduleAmortizationInputBase
  ): Promise<IScheduleAmortizationResponse> {
    const estimatedNumberOfPayments: BigNumber = new BigNumber(
      loanData.amortizationYears
    ).multipliedBy(loanData.paymentsPerYear || 12);
    const propertiesShare = loanData.properties.map(
      (property): IShareProperty => {
        return {
          _id: Types.ObjectId(property.id),
          payments: [
            {
              finalPayment: estimatedNumberOfPayments,
              firstPayment: new BigNumber(1),
              share: new BigNumber(property.share),
            },
          ],
        };
      }
    );
    const propertiesGralInfo = await getPropertyInfoObject(
      propertiesShare.map((propertyShare) => propertyShare._id)
    );
    const aRAWData /* Amortization RAW Data */ = getAmortizationScheduleRawData(
      {
        interest: new BigNumber(loanData.interest).dividedBy(100),
        amortizationYears: new BigNumber(loanData.amortizationYears),
        loanAmount: new BigNumber(loanData.amount),
        scheduleExtraPayment: new BigNumber(loanData.scheduleExtraPayment || 0),
        paymentsPerYear: new BigNumber(loanData.paymentsPerYear || 12),
        beginningDate: DateTime.fromISO(loanData.beginningDate),
        estimatedNumberOfPayments,
        extraPayments: loanData.extraPayments.map(
          (extraPayment): IExtraPaymentParam => {
            return {
              amount: new BigNumber(extraPayment.amount),
              name: extraPayment.name,
              paymentNumbers: extraPayment.paymentNumbers,
            };
          }
        ),
        propertiesShare,
      },
      propertiesGralInfo
    );

    return {
      interestRate: aRAWData.interest.multipliedBy(100).toNumber(),
      paymentsPerYear: aRAWData.paymentsPerYear.toNumber(),
      amortizationYears: aRAWData.amortizationYears.toNumber(),
      amount: aRAWData.amount.toNumber(),
      schedulePayment: aRAWData.schedulePayment.toNumber(),
      programedNumberPayments: estimatedNumberOfPayments.toNumber(),
      totalNumberPayments: aRAWData.realNumberPayments.toNumber(),
      totalEarlyPayments: aRAWData.totalEarlyPayments.toNumber(),
      totalInterest: aRAWData.totalInterest.toNumber(),
      generalPropertyInfo: aRAWData.generalPropertyInfo.map(
        (propertyGral): IPropertySheduleGralData => {
          return {
            id: propertyGral.id,
            name: propertyGral.name,
            address: propertyGral.address,
            share: propertyGral.share.multipliedBy(100).toNumber(),
            loanAmount: propertyGral.loanAmount.toNumber(),
            loanPayment: propertyGral.loanPayment.toNumber(),
          };
        }
      ),
      amortizationSchedule: aRAWData.amortizationSchedule.map(
        (AM): IMonthlyAmortizationResponse => {
          return {
            paymentNumber: AM.paymentNumber,
            date: AM.date.toString(),
            beginningBalance: AM.beginningBalance.toNumber(),
            extraPayment: AM.extraPayment.toNumber(),
            totalPayment: AM.totalPayment.toNumber(),
            interest: AM.interest.toNumber(),
            principal: AM.principal.toNumber(),
            endingBalance: AM.endingBalance.toNumber(),
            accumulativeInterest: AM.accumulativeInterest.toNumber(),
            schedulePayment: AM.schedulePayment.toNumber(),
            propertiesPayment: AM.propertiesPayment.map(
              (p): IPropertyPaymentAmortizationResponse => {
                return {
                  id: p.id,
                  name: p.name,
                  address: p.address,
                  principalPayment: p.principalPayment.toNumber(),
                  payment: p.payment.toNumber(),
                  share: p.share,
                };
              }
            ),
          };
        }
      ),
    };
  }

  public isTheLoanPropertyShareValid(shares: number[]): void {
    let total: BigNumber = new BigNumber(0);
    for (let i = 0; i < shares.length; i++) {
      const currentShare: BigNumber = new BigNumber(shares[i]);
      if (currentShare.isEqualTo(0)) {
        throw new BaseError(
          'Loan properties share cannot be 0.',
          400,
          'ImplBndBaseLoanMath | isTheLoanPropertyShareValid'
        );
      }
      total = total.plus(currentShare);
    }
    if (!total.isEqualTo(100)) {
      throw new BaseError(
        'Loan properties share is not equal to 100.',
        400,
        'ImplBndBaseLoanWriteMongo | createLoan'
      );
    }
  }
}
