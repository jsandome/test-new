import { injectable } from 'inversify';
import {
  BaseError,
  IBaseEntityUser,
  IBaseEntityCreateUser,
  IBndBaseUserWrite,
  IUpdateJwt,
  IUpdateSecure,
  IUpdatePassword,
  IBaseExpireDataTokenOut,
} from '@capa/core';

import { IModelUser, ModelUser } from '../models';

@injectable()
export class ImplBndBaseUserWriteMongo implements IBndBaseUserWrite {
  public async clearSession(id: string): Promise<void> {
    try {
      const user = await ModelUser.findById(id);
      user.tokenJwt = null;
      user.expirePass.date = null;
      user.expirePass.token = null;
      user.twoFactor.code = null;
      user.twoFactor.date = null;
      user.twoFactor.secure = null;
      await user.save();
    } catch (error) {
      throw new BaseError(
        `An error occurred while trying to update a user. ERROR ${error}`,
        500,
        'GenerateDBError',
        error as Error
      );
    }
  }
  public async updateSecure(params: IUpdateSecure): Promise<void> {
    try {
      const user = await ModelUser.findById(params.id);
      user.tokenJwt = null;
      if (params.code) user.twoFactor.code = params.code;
      user.twoFactor.date = params.date;
      user.twoFactor.secure = params.secure;
      await user.save();
    } catch (error) {
      throw new BaseError(
        `An error occurred while trying to update a user. ERROR ${error}`,
        500,
        'GenerateDBError',
        error as Error
      );
    }
  }
  public async update2FASecret(id: string, secret: string): Promise<void> {
    try {
      const user = await ModelUser.findById(id);
      user.twoFactor.secret = secret;
      await user.save();
    } catch (error) {
      throw new BaseError(
        `An error occurred while trying to update a 2FA secret. ERROR ${error}`,
        500,
        'GenerateDBError',
        error as Error
      );
    }
  }
  public async updateJwt(params: IUpdateJwt): Promise<void> {
    try {
      const user = await ModelUser.findById(params.id);
      user.tokenJwt = params.tokenJwt;
      user.expirePass.date = null;
      user.expirePass.token = null;
      user.twoFactor.code = null;
      user.twoFactor.date = null;
      user.twoFactor.secure = null;
      user.lastLogin = new Date();
      await user.save();
    } catch (error) {
      throw new BaseError(
        `An error occurred while trying to update a user. ERROR ${error}`,
        500,
        'GenerateDBError',
        error as Error
      );
    }
  }

  public async createNewUser(
    user: IBaseEntityCreateUser
  ): Promise<IBaseEntityUser> {
    try {
      const newUser: IModelUser = new ModelUser({
        email: user.email,
        type: user.type,
        password: user.password,
      });
      await newUser.save();
      return {
        active: newUser.active,
        admin: newUser.admin === null ? null : `${newUser.admin}`,
        email: newUser.email,
        investor: newUser.investor === null ? null : `${newUser.investor}`,
        type: newUser.type,
        id: newUser._id,
      };
    } catch (error) {
      throw new BaseError(
        `An error occurred while trying to create a user. ERROR ${error}`,
        500,
        'GenerateDBError',
        error as Error
      );
    }
  }

  public async updateExpirePass(
    userId: string,
    expirePass: IBaseExpireDataTokenOut
  ): Promise<void> {
    try {
      const user = await ModelUser.findById(userId);
      const { expireToken, expireDateToken } = expirePass;

      user.expirePass.date = expireDateToken;
      user.expirePass.token = expireToken;

      await user.save();
    } catch (error) {
      throw new BaseError(
        `An error occurred while trying to update a user. ERROR ${error}`,
        500,
        'GenerateDBError',
        error as Error
      );
    }
  }

  public async updatePassword(toUpdate: IUpdatePassword): Promise<void> {
    try {
      const { email, password } = toUpdate;
      const user = await ModelUser.findOne({ email }).exec();

      user.password = password;
      user.expirePass.date = null;
      user.expirePass.token = null;

      await user.save();
    } catch (error) {
      throw new BaseError(
        `An error occurred while trying to update a user. ERROR ${error}`,
        500,
        'GenerateDBError',
        error as Error
      );
    }
  }

  public async changePassword(email: string, password: string): Promise<void> {
    try {
      const user = await ModelUser.findOne({
        email: { $regex: email, $options: 'i' },
      });

      user.password = password;

      await user.save();
    } catch (error) {
      throw new BaseError(
        `An error occurred while trying to change user password. ERROR ${error}`,
        500,
        'GenerateDBError',
        error as Error
      );
    }
  }
}
