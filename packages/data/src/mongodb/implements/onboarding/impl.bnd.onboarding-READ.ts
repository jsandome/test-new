import { injectable } from 'inversify';
import {
  BaseError,
  IBndOnboardingRead,
  IBndResponseOnboarding
} from '@capa/core';
import { IModelOnboarding, ModelOnboarding } from '../../models';
import { isEmpty, isNil, isNaN } from 'lodash';

@injectable()
export class ImplBndBaseOnboardingRead implements IBndOnboardingRead {  
  public async getCurrentStep(token: string):
  Promise<string> {
    try {
      const onboarding: IModelOnboarding = await ModelOnboarding.findOne( { "token": token } );
      return onboarding.currentStep;
    } catch (error) {
      throw new BaseError(
        `An error occurred while to get a Onboarding record: ${error.message}`,
        error.code,
        error.name,
        error as Error
      );
    }
  }
  
  public async getInfoOnboarding(token: string): Promise<IBndResponseOnboarding> {
    try {
      const onboarding: IModelOnboarding = await ModelOnboarding.findOne( { "token": token } );
      return onboarding;
    } catch (error) {
      throw new BaseError(
        `An error occurred while to get a Onboarding record: ${error.message}`,
        error.code,
        error.name,
        error as Error
      );
    }
  }

  public async getExistDocuments(email: string): Promise<boolean> {
    try {
      const onboarding: IModelOnboarding = await ModelOnboarding.findOne( { "email": email } );
      return isEmpty(onboarding) || isNil(onboarding) || isNaN(onboarding);
    } catch (error) {
      throw new BaseError(
        `An error occurred while to get a Onboarding record: ${error.message}`,
        error.code,
        error.name,
        error as Error
      );
    }
  }

  public async getNotExistDocumentsByToken(token: string): Promise<boolean> {
    try {
      const onboarding: IModelOnboarding = await ModelOnboarding.findOne( { "token": token } );
      return isEmpty(onboarding) || isNil(onboarding) || isNaN(onboarding);
    } catch (error) {
      throw new BaseError(
        `An error occurred while to get a Onboarding record: ${error.message}`,
        error.code,
        error.name,
        error as Error
      );
    }
  }
}
