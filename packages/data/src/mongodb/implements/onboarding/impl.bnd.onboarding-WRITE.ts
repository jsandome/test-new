import { injectable, inject } from 'inversify';
import {
  BaseError,
  IBndOnboardingWrite,
  IParamsCreateOnboarding,
  IBndBaseTokenInfra,
  FrontendEnvironment,
  IBndMailerInfra,
  BASETYPES,
  BaseUseCaseDocuSign,
  IParamsUpdateStepOneOnboarding,
  EnumTypeStepOnboarding
} from '@capa/core';
import {
  IModelOnboarding,
  ModelOnboarding,
  ModelInvestor,
  ModelContact,
  ModelProperty,
  ModelFund,
  ModelPackage,
  ModelPackageDocuments
} from '../../models';
import { DateTime } from 'luxon';
import { startSession } from 'mongoose';
import { isEmpty, isNil, isNaN } from 'lodash';

@injectable()
export class ImplBndBaseOnboardingWrite implements IBndOnboardingWrite {
  //Inject FrontendEnvironment and DocuSing
  @inject(BASETYPES.FrontendEnvironment) private _config: FrontendEnvironment;
  @inject('BaseUseCaseDocuSign') private ucDocuSign: BaseUseCaseDocuSign;
  
  public async createOnboarding(
    params: IParamsCreateOnboarding,
    tokenMod: IBndBaseTokenInfra,
    useMailer: IBndMailerInfra
  ): Promise<string> {
    const session = await startSession();
    session.startTransaction();
    try {
      const investor = await ModelInvestor.findOne({ email: params.email });
      if (investor) {
        throw new BaseError(
          'Investor found.',
          409,
          'ImplBndBaseOnboardingWrite | createOnboarding'
        );
      }
      const newRecord = {};
      newRecord['email'] = params.email;
      const contact = await ModelContact.findOne({ email: params.email });
      if (contact) {
        newRecord['contactId'] = contact._id;
      }
      let itHasType = false;
      if (params.propertyId) {
        const property = await ModelProperty.findById(params.propertyId);
        if(property) {
          newRecord['propertyId'] = params.propertyId;
          itHasType = true;
        } else {
          throw new BaseError(
            'Property not found',
            404,
            'ImplBndBaseOnboardingWrite | createOnboarding'
          );
        }
      }
      if (params.fundId) {
        const fund = await ModelFund.findById(params.fundId);
        if(fund) {
          if (!itHasType) {
            newRecord['fundId'] = params.fundId;
            itHasType = true;
          } else {
            throw new BaseError(
              'Imposible to have more that one resource',
              409,
              'ImplBndBaseOnboardingWrite | createOnboarding'
            );
          }
        } else {
          throw new BaseError(
            'Fund not found',
            404,
            'ImplBndBaseOnboardingWrite | createOnboarding'
          );
        }
      }
      if (params.packageId) {
        const packageRes = await ModelPackage.findById(params.packageId);
        if(packageRes) {
          if (!itHasType) {
            newRecord['packageId'] = params.packageId;
            itHasType = true;
          } else {
            throw new BaseError(
              'Imposible to have more that one resource',
              409,
              'ImplBndBaseOnboardingWrite | createOnboarding'
            );
          }
        } else {
          throw new BaseError(
            'Package not found',
            404,
            'ImplBndBaseOnboardingWrite | createOnboarding'
          );
        }
      }
      if(!itHasType) {
        throw new BaseError(
          'You need to send one resource',
          400,
          'ImplBndBaseOnboardingWrite | createOnboarding'
        );
      }
      let itHasTemplete = false;
      if (params.templateId) {
        const templatesInDoc = await this.ucDocuSign.listTemplates();
        const found = templatesInDoc.find(element => element["templateId"] === params.templateId);
        if(isEmpty(found)) {
          throw new BaseError(
            'Templete not Found',
            404,
            'ImplBndBaseOnboardingWrite | Templete not Found'
          );
        }
        newRecord['templateId'] = params.templateId;
        itHasTemplete = true;
      }
      if (params.packageDocumentId) {
        if (!itHasTemplete) {
          const packageDocuments = await ModelPackageDocuments.findById(params.packageDocumentId);
          if(packageDocuments) {
            newRecord['packageDocumentId'] = params.packageDocumentId;
            itHasTemplete = true;
          } else {
            throw new BaseError(
              'PackageDocuments not found',
              404,
              'ImplBndBaseOnboardingWrite | createOnboarding'
            );
          }
        } else {
          throw new BaseError(
            'Imposible to have more that one resource to singed',
            409,
            'ImplBndBaseOnboardingWrite | createOnboarding'
          );
        }
      }
      if(!itHasTemplete) {
        throw new BaseError(
          'You need to send a document or package to sign',
          400,
          'ImplBndBaseOnboardingWrite | createOnboarding'
        );
      }
      if (params.nameContact) {
        newRecord['nameContact'] = params.nameContact;
      }
      newRecord['createdBy'] = {
        id: params.userRequest,
        dateTime: DateTime.now().toISODate(),
      };
      newRecord['updatedBy'] = {
        id: params.userRequest,
        dateTime: DateTime.now().toISODate(),
      };
      const newOnboarding: IModelOnboarding = new ModelOnboarding(newRecord);
      await newOnboarding.save({ session });
      const tokens = tokenMod.generateExpireDataToken(newOnboarding._id);
      newOnboarding.token = tokens.expireToken;
      newOnboarding.tokenDate = tokens.expireDateToken;
      newOnboarding.url = `${this._config.url}/onboarding?token=${newOnboarding.token}`;
      await newOnboarding.save({ session });
      const dataEmail = {
        to: newOnboarding.email,
        subject: 'Complete your profile for CAPA',
        template: '/client-invite-subscribe/html',
        data: {
          url: `/onboarding?token=${newOnboarding.token}`,
        },
      };
      await useMailer.send(dataEmail);
      await session.commitTransaction();
      session.endSession();
      return newOnboarding.url;
    } catch (error) {
      await session.abortTransaction();
      session.endSession();
      throw new BaseError(
        'Error creating a new Onboarding',
        error.code,
        error.name,
        error as Error
      );
    }
  }

  public async updateStepOneOnboarding(
    params: IParamsUpdateStepOneOnboarding
  ): Promise<void> {
    const session = await startSession();
    session.startTransaction();
    try {
      const onboarding: IModelOnboarding = await ModelOnboarding.findById(params.userRequestId);
      if(isEmpty(onboarding) || isNil(onboarding) || isNaN(onboarding)) {
        throw new BaseError(
          `Doesn't exist an Onboarding for the provided token`,
          404,
          'BaseUseCaseOnboarding | updateStepOneOnboarding'
        );
      }
      const cleanParams = params;
      delete cleanParams.userRequestId
      onboarding.basicInformation = cleanParams;
      if(onboarding.currentStep === EnumTypeStepOnboarding.STEPONE) {
        onboarding.currentStep = EnumTypeStepOnboarding.STEPTWO;
      }
      await onboarding.save({ session });
      await session.commitTransaction();
      session.endSession();
    } catch (error) {
      await session.abortTransaction();
      session.endSession();
      throw new BaseError(
        'Error creating a new Onboarding',
        error.code,
        error.name,
        error as Error
      );
    }
  }
}
