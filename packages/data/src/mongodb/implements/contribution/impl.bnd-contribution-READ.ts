import { injectable } from 'inversify';
import {
  BaseError,
  IBndBaseContributionRead,
  IBndReturnGetContribution,
  IBndReturnListContribution,
  IPaginateParams,
} from '@capa/core';
import {
  IModelLlc,
  ModelContribution,
  ModelLlc,
  IModelContribution,
  IModelCapitalCallInvestor,
  IModelInvestor,
  IModelCapitalCall,
} from '../../models';
import Bignumber from 'bignumber.js';
import { ObjectId } from 'mongodb';

@injectable()
export class ImplBndContributionRead implements IBndBaseContributionRead {
  public async getListContributions(
    llcId: string,
    { limit = 10, page = 1 }: IPaginateParams,
    search?: string
  ): Promise<IBndReturnListContribution> {
    try {
      const llc: IModelLlc = await ModelLlc.findOne({
        _id: llcId,
        active: true,
      });
      if (!llc) {
        throw new BaseError(
          'Entity not found.',
          404,
          'BaseUseCaseContribution | getListContributions'
        );
      }

      const options = {
        limit,
        page,
        offset: (page - 1) * limit,
      };

      const aggregate = ModelContribution.aggregate([
        {
          $match: { llc: new ObjectId(llcId), 'deletedBy.dateTime': null },
        },
        {
          $lookup: {
            from: 'capitalcallinvestors',
            let: { capitalCallInvestor: '$capitalCallInvestor' },
            pipeline: [
              { $match: { $expr: { $eq: ['$_id', '$$capitalCallInvestor'] } } },
              {
                $lookup: {
                  from: 'investors',
                  localField: 'investor',
                  foreignField: '_id',
                  as: 'investors',
                },
              },
              { $unwind: { path: '$investors' } },
              {
                $lookup: {
                  from: 'capitalcalls',
                  localField: 'capitalCall',
                  foreignField: '_id',
                  as: 'capitalcalls',
                },
              },
              { $unwind: { path: '$capitalcalls' } },
              { $match: { 'capitalcalls.deletedBy.dateTime': null } },
              {
                $project: {
                  investor: {
                    $concat: [
                      '$investors.name.firstName',
                      ' ',
                      '$investors.name.lastName',
                    ],
                  },
                  description: '$capitalcalls.description',
                },
              },
            ],
            as: 'capitalcallinvestors',
          },
        },
        { $unwind: { path: '$capitalcallinvestors' } },
        {
          $project: {
            _id: 0,
            id: '$capitalcallinvestors._id',
            contributionDate: '$receivedDate',
            investor: '$capitalcallinvestors.investor',
            description: '$capitalcallinvestors.description',
            amountContributed: '$paymentAmount',
          },
        },
      ]);

      const data = await ModelContribution.aggregatePaginate(
        aggregate,
        options
      );

      return {
        pagination: {
          itemCount: data.totalDocs,
          offset: (page - 1) * limit,
          limit: data.limit,
          totalPages: data.totalPages,
          page: data.page,
          nextPage: data.nextPage,
          prevPage: data.prevPage,
        },
        details: data.docs.map((e) => {
          return {
            id: e.id,
            contributionDate: e.contributionDate,
            investor: e.investor,
            description: e.description,
            amountContributed: new Bignumber(e.amountContributed).toNumber(),
          };
        }),
      };
    } catch (error) {
      throw new BaseError(error.message, error.code, error.name, error);
    }
  }

  public async getContribution(
    llcId: string,
    contributionId: string
  ): Promise<IBndReturnGetContribution> {
    try {
      const llc: IModelLlc = await ModelLlc.findOne({
        _id: llcId,
        active: true,
      });
      if (!llc) {
        throw new BaseError(
          'Entity not found.',
          404,
          'BaseUseCaseContribution | getContribution'
        );
      }

      const contribution: IModelContribution = await ModelContribution.findOne({
        _id: contributionId,
        'deletedBy.dateTime': null,
      }).populate({
        path: 'capitalCallInvestor',
        populate: [
          {
            path: 'investor',
          },
          {
            path: 'capitalCall',
          },
        ],
      });
      if (!contribution) {
        throw new BaseError(
          'Contribution not found.',
          404,
          'BaseUseCaseContribution | getContribution'
        );
      }

      const thisInvestor = (
        contribution.capitalCallInvestor as IModelCapitalCallInvestor
      ).investor as IModelInvestor;
      const capitalCall = (
        contribution.capitalCallInvestor as IModelCapitalCallInvestor
      ).capitalCall as IModelCapitalCall;
      return {
        account: {
          id: thisInvestor.id,
          name: `${thisInvestor.name.firstName} ${thisInvestor.name.lastName}`,
        },
        contributionDate: contribution.receivedDate,
        capitalCall: {
          id: capitalCall.id,
          description: capitalCall.description,
          amount: new Bignumber(contribution.paymentAmount).toNumber(),
          paymentType: contribution.paymentMethod,
          paymentNumber: contribution.checkNumber,
          notes: contribution.notes,
        },
      };
    } catch (error) {
      throw new BaseError(error.message, error.code, error.name, error);
    }
  }
}
