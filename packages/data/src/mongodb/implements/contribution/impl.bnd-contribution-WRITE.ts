import {
  BaseError,
  IBndBaseContributionWrite,
  IBndReturnCreateContribution,
  IBndReturnDeleteContribution,
  IBndReturnRecordContribution,
  IBndReturnUpdateContribution,
  IParamsCreateContribution,
  IParamsRecordContribution,
  IParamsUpdateContribution,
} from '@capa/core';
import {
  IModelCapitalCall,
  IModelCapitalCallInvestor,
  IModelContribution,
  IModelLlc,
  ModelCapitalCall,
  ModelCapitalCallInvestor,
  ModelContribution,
  ModelLlc,
} from '../../models';
import { injectable } from 'inversify';
import { startSession } from 'mongoose';
import Bignumber from 'bignumber.js';
import { ObjectId } from 'mongodb';

@injectable()
export class ImplBndContributionWrite implements IBndBaseContributionWrite {
  public async createNewContribution(
    llcId: string,
    dataContribution: IParamsCreateContribution
  ): Promise<IBndReturnCreateContribution> {
    const session = await startSession();
    session.startTransaction();

    try {
      const { investor, dateReceived, capitalCall } = dataContribution;
      const llc: IModelLlc = await ModelLlc.findById(llcId);
      if (!llc && !llc.active) {
        throw new BaseError(
          'Entity not found.',
          404,
          'BaseUseCaseContribution | createNewContribution'
        );
      }

      const capitalCallExist: IModelCapitalCall =
        await ModelCapitalCall.findOne({
          _id: capitalCall.id,
          'deletedBy.dateTime': null,
          llc: llcId,
        });

      if (!capitalCallExist) {
        throw new BaseError(
          'Capital Call not found.',
          404,
          'BaseUseCaseContribution | createNewContribution'
        );
      }

      const capitalCallInvestor: IModelCapitalCallInvestor =
        await ModelCapitalCallInvestor.findOne({
          investor,
          capitalCall: capitalCallExist.id,
          'deletedBy.dateTime': null,
        });

      if (!capitalCallInvestor) {
        throw new BaseError(
          'Capital Call Investor not found.',
          404,
          'BaseUseCaseContribution | createNewContribution'
        );
      }

      const newContribution: IModelContribution = new ModelContribution({
        receivedDate: dateReceived,
        paymentAmount: new Bignumber(capitalCall.amount),
        paymentMethod: capitalCall.paymentType,
        capitalCallInvestor: capitalCallInvestor.id,
        checkNumber: capitalCall.paymentNumber,
        notes: capitalCall.notes,
        llc: llcId,
      });
      await newContribution.save({ session });
      await session.commitTransaction();
      session.endSession();

      return { id: newContribution.id };
    } catch (error) {
      await session.abortTransaction();
      session.endSession();
      throw new BaseError(error.message, error.code, error.name, error);
    }
  }

  public async recordContribution(
    llcId: string,
    capitalCallId: string,
    dataContribution: IParamsRecordContribution
  ): Promise<IBndReturnRecordContribution> {
    const session = await startSession();
    session.startTransaction();

    try {
      const {
        amountContribution,
        paymentDate,
        paymentType,
        paymentNumber,
        notes,
        investorId,
      } = dataContribution;
      const llc: IModelLlc = await ModelLlc.findById(llcId);
      if (!llc) {
        throw new BaseError(
          'Entity not found.',
          404,
          'BaseUseCaseContribution | recordContribution'
        );
      }

      const capitalcall: IModelCapitalCall = await ModelCapitalCall.findOne({
        _id: capitalCallId,
        llc: llcId,
        'deletedBy.dateTime': null,
      });
      if (!capitalcall) {
        throw new BaseError(
          'Capital Call not found.',
          404,
          'BaseUseCaseContribution | recordContribution'
        );
      }

      const capitalCallInvestor: IModelCapitalCallInvestor =
        await ModelCapitalCallInvestor.findOne({
          investor: investorId,
          capitalCall: capitalcall.id,
          'deletedBy.dateTime': null,
        });
      if (!capitalCallInvestor) {
        throw new BaseError(
          'Capital Call Investor not found.',
          404,
          'BaseUseCaseContribution | recordContribution'
        );
      }

      const newContribution: IModelContribution = new ModelContribution({
        receivedDate: paymentDate,
        paymentAmount: amountContribution,
        paymentMethod: paymentType,
        capitalCallInvestor: capitalCallInvestor.id,
        checkNumber: paymentNumber,
        notes,
      });

      await newContribution.save({ session });
      await session.commitTransaction();
      session.endSession();

      return {};
    } catch (error) {
      await session.abortTransaction();
      session.endSession();
      throw new BaseError(error.message, error.code, error.name, error);
    }
  }

  public async updateContribution(
    llcId: string,
    contributionId: string,
    dataContribution: IParamsUpdateContribution
  ): Promise<IBndReturnUpdateContribution> {
    const session = await startSession();
    session.startTransaction();

    try {
      const { account, dateReceived, capitalCall } = dataContribution;
      const llc: IModelLlc = await ModelLlc.findById(llcId);
      if (!llc) {
        throw new BaseError(
          'Entity not found.',
          404,
          'BaseUseCaseContribution | updateContribution'
        );
      }

      let contribution: IModelContribution = await ModelContribution.findOne({
        _id: contributionId,
        'deletedBy.dateTime': null,
      }).populate({ path: 'capitalCallInvestor' });
      if (!contribution) {
        throw new BaseError(
          'Contribution not found.',
          404,
          'BaseUseCaseContribution | updateContribution'
        );
      }
      contribution.receivedDate = dateReceived;
      contribution.paymentAmount = capitalCall.amount;
      contribution.paymentMethod = capitalCall.paymentType;
      contribution.checkNumber = capitalCall.paymentNumber;

      const ccInvestor =
        contribution.capitalCallInvestor as IModelCapitalCallInvestor;
      ccInvestor.investor = new ObjectId(account);
      await ccInvestor.save({ session });
      await contribution.save({ session });
      await session.commitTransaction();
      session.endSession();

      return { id: contribution.id };
    } catch (error) {
      await session.abortTransaction();
      session.endSession();
      throw new BaseError(error.message, error.code, error.name, error);
    }
  }

  public async deleteContribution(
    llcId: string,
    contributionId: string,
    userId: string
  ): Promise<IBndReturnDeleteContribution> {
    try {
      const llc: IModelLlc = await ModelLlc.findById(llcId);
      if (!llc) {
        throw new BaseError(
          'Entity not found.',
          404,
          'BaseUseCaseContribution | updateContribution'
        );
      }

      let contribution: IModelContribution = await ModelContribution.findOne({
        _id: contributionId,
        'deletedBy.dateTime': null,
      });
      if (!contribution) {
        throw new BaseError(
          'Contribution not found.',
          404,
          'BaseUseCaseContribution | updateContribution'
        );
      }
      contribution.deletedBy.id = userId;
      contribution.deletedBy.dateTime = new Date();
      await contribution.save();

      return {};
    } catch (error) {
      throw new BaseError(error.message, error.code, error.name, error);
    }
  }
}
