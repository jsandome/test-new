import { injectable } from 'inversify';
import {
  BaseError,
  IBndBaseFundWrite,
  IBndReturnCreateFund,
  IBaseEntityFundInvestors,
  IBaseEntityFundProperty,
  IBaseEntityFundPackage,
  IParamsEntityFunds,
  IBndShareMathFund,
} from '@capa/core';
import {
  IModelFund,
  ModelFund,
  IModelFundInvestor,
  ModelFundInvestor,
  IModelFundProperty,
  ModelFundProperty,
  IModelFundPackage,
  ModelFundPackage,
} from '../../models';
import { startSession } from 'mongoose';

@injectable()
export class ImplBndBaseFundWriteMongo implements IBndBaseFundWrite {
  public async createFund(
    fund: IParamsEntityFunds,
    bndShareMathFund: IBndShareMathFund
  ): Promise<IBndReturnCreateFund> {
    const { name, cashHand } = fund;
    const investors: IBaseEntityFundInvestors[] = fund.investors;
    const packages: IBaseEntityFundPackage[] = fund.packages;
    const properties: IBaseEntityFundProperty[] = fund.properties;
    const session = await startSession();
    session.startTransaction();

    try {
      const newFund: IModelFund = new ModelFund({ name, cashHand });
      await newFund.save({ session });

      for (let investor of investors) {
        const { minCommitment, initCommitment, totalCommitment, termYears } =
          investor;
        const newFundInvestor: IModelFundInvestor = new ModelFundInvestor({
          minCommitment,
          initCommitment,
          totalCommitment,
          termYears,
          fund: newFund._id,
          investor: investor.id,
        });
        await newFundInvestor.save({ session });
      }

      if (packages && packages.length) {
        for (let pack of packages) {
          await bndShareMathFund._validateSharePackageInFund(pack);
          const { id, share } = pack;
          const newFundPackage: IModelFundPackage = new ModelFundPackage({
            fund: newFund._id,
            package: id,
            share,
          });
          await newFundPackage.save({ session });
        }
      }

      if (properties && properties.length) {
        for (let property of properties) {
          await bndShareMathFund._validateSharePropertyInFund(property);
          const { id, share } = property;
          const newFundProperty: IModelFundProperty = new ModelFundProperty({
            fund: newFund._id,
            property: id,
            share: share,
          });
          await newFundProperty.save({ session });
        }
      }

      await newFund.save({ session });
      await session.commitTransaction();
      session.endSession();

      return { id: newFund._id.toHexString() };
    } catch (error) {
      await session.abortTransaction();
      session.endSession();
      throw new BaseError(error.message, error.code, error.name, error);
    }
  }

  public async updateFund(
    fundId: string,
    updateFund: IParamsEntityFunds,
    bndShareMathFund: IBndShareMathFund
  ): Promise<void> {
    const { name, cashHand } = updateFund;
    const investors: IBaseEntityFundInvestors[] = updateFund.investors;
    const packages: IBaseEntityFundPackage[] = updateFund.packages;
    const properties: IBaseEntityFundProperty[] = updateFund.properties;
    const session = await startSession();
    session.startTransaction();

    try {
      const fund: IModelFund = await ModelFund.findOne({
        _id: fundId,
        active: true,
      });
      if (!fund) {
        throw new BaseError(
          `Fund with ID: ${fundId} doesn't exists.`,
          404,
          'BaseUseCaseFound | updateFund'
        );
      }
      fund.name = name;
      fund.cashHand = cashHand;

      // Update collection ModelFundInvestor
      for (let investor of investors) {
        const { minCommitment, initCommitment, totalCommitment, termYears } =
          investor;
        const updateFundInvestor: IModelFundInvestor =
          await ModelFundInvestor.findOne({
            fund: fund.id,
            investor: investor.id,
          });
        if (updateFundInvestor) {
          updateFundInvestor.minCommitment = minCommitment;
          updateFundInvestor.initCommitment = initCommitment;
          updateFundInvestor.totalCommitment = totalCommitment;
          updateFundInvestor.termYears = termYears;
          await updateFundInvestor.save({ session });
        } else {
          const newFundInvestor: IModelFundInvestor = new ModelFundInvestor({
            minCommitment,
            initCommitment,
            totalCommitment,
            termYears,
            fund: fund._id,
            investor: investor.id,
          });
          await newFundInvestor.save({ session });
        }
      }
      const deletedInvestors: IModelFundInvestor[] =
        await ModelFundInvestor.find({
          fund: fund._id.toHexString(),
          investor: { $nin: investors.map((i) => i.id) },
        });
      for (let dInv of deletedInvestors) {
        await dInv.deleteOne({ session });
      }

      // Update collection ModelFundPackage
      if (packages && packages.length) {
        for (let pack of packages) {
          await bndShareMathFund._validateSharePackageInFund(pack);
          const updateFundPackage: IModelFundPackage =
            await ModelFundPackage.findOne({
              fund: fund.id,
              package: pack.id,
            });

          if (updateFundPackage) {
            updateFundPackage.share = pack.share;
            await updateFundPackage.save({ session });
          } else {
            const newFundPackage: IModelFundPackage = new ModelFundPackage({
              fund: fund._id,
              package: pack.id,
              share: pack.share,
            });
            await newFundPackage.save({ session });
          }
        }
        const deletedPackages: IModelFundPackage[] =
          await ModelFundPackage.find({
            fund: fund._id.toHexString(),
            package: { $nin: packages.map((i) => i.id) },
          });
        for (let dPacks of deletedPackages) {
          await dPacks.deleteOne({ session });
        }
      }

      // Update collectionModelFundProperty
      if (properties && properties.length) {
        for (const property of properties) {
          await bndShareMathFund._validateSharePropertyInFund(property);
          const updateFundProp: IModelFundProperty =
            await ModelFundProperty.findOne({
              fund: fund.id,
              property: property.id,
            });
          if (updateFundProp) {
            updateFundProp.share = property.share;
            await updateFundProp.save({ session });
          } else {
            const newFundProp: IModelFundProperty = new ModelFundProperty({
              fund: fund.id,
              property: property.id,
              share: property.share,
            });
            await newFundProp.save({ session });
          }
        }
        const deletedProperties: IModelFundProperty[] =
          await ModelFundProperty.find({
            fund: fund._id.toHexString(),
            property: { $nin: properties.map((i) => i.id) },
          });
        for (let dProps of deletedProperties) {
          await dProps.deleteOne({ session });
        }
      }

      await fund.save({ session });
      await session.commitTransaction();
      session.endSession();
    } catch (error) {
      await session.abortTransaction();
      session.endSession();
      throw new BaseError(error.message, error.code, error.name, error);
    }
  }

  public async deleteFund(fundId: string): Promise<IBndReturnCreateFund> {
    try {
      const fund = await ModelFund.findOne({ _id: fundId });
      if (!fund) {
        throw new BaseError(
          `Fund not found`,
          404,
          'BaseUseCaseFunds | deleteFund'
        );
      }
      const fundInvestors = await ModelFundInvestor.findOne({ fund: fundId });
      const fundPackage = await ModelFundPackage.findOne({ fund: fundId });
      const fundPrperty = await ModelFundInvestor.findOne({ fund: fundId });

      if (!fundInvestors && !fundPackage && !fundPrperty) {
        const deletedFund = await ModelFund.findOneAndDelete({
          _id: fundId,
        });

        return { id: deletedFund.id };
      }

      fund.active = false;
      await fund.save();
      return { id: fund.id };
    } catch (error) {
      throw new BaseError(
        `An error has been ocurred.`,
        500,
        'GenerateDBError',
        error
      );
    }
  }
}
