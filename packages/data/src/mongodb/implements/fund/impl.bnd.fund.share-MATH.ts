import { injectable } from 'inversify';
import {
  BaseError,
  IBaseEntityFundPackage,
  IBaseEntityFundProperty,
  IBndShareMathFund,
} from '@capa/core';
import {
  IModelFundPackage,
  IModelFundProperty,
  ModelFundPackage,
  ModelFundProperty,
} from '../../models';
import BigNumber from 'bignumber.js';

@injectable()
export class ImplBndBaseShareMathFund implements IBndShareMathFund {
  public async _validateSharePackageInFund(
    pack: IBaseEntityFundPackage
  ): Promise<void> {
    try {
      let sumShare = new BigNumber(0);
      const fundPackages: IModelFundPackage[] = await ModelFundPackage.find({
        package: pack.id,
      });

      for (let fPack of fundPackages) sumShare = sumShare.plus(fPack.share);

      sumShare = sumShare.plus(pack.share);

      if (sumShare.isGreaterThan(100)) {
        throw new BaseError(
          'The package share value should be lees o equal than 100',
          400,
          '_validateSharePackageInFund'
        );
      }
    } catch (error) {
      throw new BaseError(error.message, error.code, error.name, error);
    }
  }

  public async _validateSharePropertyInFund(
    prop: IBaseEntityFundProperty
  ): Promise<void> {
    try {
      let sumShare = new BigNumber(0);
      const fundProperties: IModelFundProperty[] = await ModelFundProperty.find(
        {
          package: prop.id,
        }
      );

      for (let fProp of fundProperties) sumShare = sumShare.plus(fProp.share);

      sumShare = sumShare.plus(prop.share);

      if (sumShare.isGreaterThan(100)) {
        throw new BaseError(
          'The property share value should be lees o equal than 100',
          400,
          '_validateSharePropertyInFund'
        );
      }
    } catch (error) {
      throw new BaseError(error.message, error.code, error.name, error);
    }
  }
}
