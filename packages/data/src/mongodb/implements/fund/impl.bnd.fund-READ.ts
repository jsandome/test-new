import { injectable } from 'inversify';
import {
  BaseError,
  IBaseReturnDetailFund,
  IBaseFundPackages,
  IBaseEntityFunds,
  IBndReturnCreateFund,
  IBndBaseGCloudInfra,
  IBndBaseFundRead,
  IParamsPreviewFund,
  IBaseEntityFundPackage,
  IBaseEntityFundProperty,
  IBndShareMathFund,
} from '@capa/core';
import {
  IModelFund,
  ModelFund,
  IModelFundInvestor,
  IModelFundPackage,
  ModelFundInvestor,
  ModelFundPackage,
  IModelFundProperty,
  ModelFundProperty,
  IModelInvestor,
  IModelProperty,
  IModelPictures,
  IModelPackage,
  IModelPackageProperty,
  ModelPackageProperty,
  ModelProperty,
  IModelPropertyFinancial,
  ModelPackage,
} from '../../models';
import BigNumber from 'bignumber.js';

@injectable()
export class ImplBndBaseFundReadMongo implements IBndBaseFundRead {
  public async checkIfNameExists(name: string): Promise<IBndReturnCreateFund> {
    try {
      const res: IModelFund = await ModelFund.findOne({ name, active: true });
      return res;
    } catch (error) {
      throw new BaseError(
        `An error occurred while trying to get a name Fund. ERROR ${error}`,
        500,
        'GenerateDBError. ImplBndBaseFundWriteMongo | checkIfNameExists',
        error
      );
    }
  }

  public async searchFund(search?: string): Promise<IBaseEntityFunds[]> {
    try {
      const query = { active: true };
      if (search) {
        query['name'] = {
          $regex: search,
          $options: 'i',
        };
      }
      const data = await ModelFund.find(query)
        .populate('investors')
        .populate('properties');

      const response = data.map((resp) => this._getFundObject(resp));
      return response;
    } catch (error) {
      throw new BaseError(
        `An error has been ocurred.`,
        500,
        'MongoDbUserError',
        error
      );
    }
  }

  public async getFund(fundId: string): Promise<IBaseEntityFunds> {
    try {
      const getFund: IModelFund = await ModelFund.findOne({
        _id: fundId,
        active: true,
      })
        .populate('investors')
        .populate('properties');

      if (!getFund || !getFund.active) {
        throw new BaseError('Fund not found.', 404, 'getFund');
      }
      return this._getFundObject(getFund);
    } catch (error) {
      throw new BaseError(
        `An error has been ocurred.`,
        500,
        'MongoDbUserError',
        error
      );
    }
  }

  public async getDetailFund(
    fundId: string,
    bndGCloudInfra: IBndBaseGCloudInfra
  ): Promise<IBaseReturnDetailFund> {
    try {
      const fullPath = await bndGCloudInfra.getPublicUrl();
      const fund: IModelFund = await ModelFund.findOne({
        _id: fundId,
        active: true,
      });
      if (!fund || !fund.active)
        throw new BaseError(
          `Fund not found`,
          404,
          'BaseUseCaseFound | execGetFund'
        );
      const fundInvestors: IModelFundInvestor[] = await ModelFundInvestor.find({
        fund: fund._id.toHexString(),
      }).populate({ path: 'investor' });
      const fundPackages: IModelFundPackage[] = await ModelFundPackage.find({
        fund: fund._id,
      }).populate({ path: 'package' });
      const fundProperties: IModelFundProperty[] = await ModelFundProperty.find(
        {
          fund: fund._id,
        }
      ).populate({ path: 'property' });

      const response: IBaseReturnDetailFund = {
        id: fund._id.toHexString(),
        name: fund.name,
        cashHand: new BigNumber(fund.cashHand).toNumber(),
        investors: fundInvestors.map((inv) => {
          const investor: IModelInvestor = inv.investor as IModelInvestor;
          return {
            id: investor._id.toHexString(),
            name: investor.name.firstName,
            minCommitment: new BigNumber(inv.minCommitment).toNumber(),
            initCommitment: new BigNumber(inv.initCommitment).toNumber(),
            totalCommitment: new BigNumber(inv.totalCommitment).toNumber(),
            termYears: new BigNumber(inv.termYears).toNumber(),
          };
        }),
        properties: fundProperties.map((p) => {
          const property: IModelProperty = p.property as IModelProperty;

          const mainPicture: IModelPictures =
            property.mainPicture as IModelPictures;
          return {
            id: property._id.toHexString(),
            name: property.name,
            mainImage: `${fullPath}/${mainPicture.url}`,
            share: new BigNumber(p.share).toNumber(),
          };
        }),
      };

      if (fundPackages && fundPackages.length) {
        let allPackages: IBaseFundPackages[] = [];
        for (let fPack of fundPackages) {
          const thisPack: IModelPackage = fPack.package as IModelPackage;
          const imageProperty = await this._getPicturePropertyFromPackage(
            thisPack._id.toHexString()
          );
          const pack: IModelPackage = fPack.package as IModelPackage;
          allPackages.push({
            id: pack._id.toHexString(),
            name: pack.name,
            mainImage: `${fullPath}/${imageProperty}`,
            share: new BigNumber(fPack.share).toNumber(),
          });
        }

        response.packages = allPackages;
      }

      return response;
    } catch (error) {
      throw new BaseError(error.message, error.code, error.name, error);
    }
  }

  public async getPreviewFund(
    params: IParamsPreviewFund,
    bndShareMathFund: IBndShareMathFund
  ): Promise<number> {
    const { cashHand, packages, properties } = params;
    let totalValue = new BigNumber(0);

    if (packages && packages.length) {
      for (let pack of packages) {
        await bndShareMathFund._validateSharePackageInFund(pack);
        const sharePackage = await this._calculateAmountSharePackage(pack);
        totalValue = totalValue.plus(sharePackage);
      }
    }

    if (properties && properties.length) {
      for (let prop of properties) {
        await bndShareMathFund._validateSharePropertyInFund(prop);
        const shareProperty = await this._calculateAmountShareProperty(prop);
        totalValue = totalValue.plus(shareProperty);
      }
    }

    totalValue = totalValue.plus(cashHand);
    return parseFloat(totalValue.toNumber().toFixed(2));
  }

  private async _calculateAmountSharePackage(
    pack: IBaseEntityFundPackage
  ): Promise<BigNumber> {
    try {
      const thisPackage: IModelPackage = await ModelPackage.findById(
        pack.id
      ).select('totalValue');
      const share = new BigNumber(pack.share)
        .dividedBy(100)
        .multipliedBy(thisPackage.totalValue);

      return share;
    } catch (error) {
      throw new BaseError(
        `Error calculating amount share of packages. ${error.message}`,
        500,
        'execPreviewFund | _calculateAmountSharePackage',
        error
      );
    }
  }

  private async _calculateAmountShareProperty(
    prop: IBaseEntityFundProperty
  ): Promise<BigNumber> {
    try {
      const thisProperty: IModelProperty = await ModelProperty.findById(
        prop.id
      ).populate({ path: 'propertyFinancial', select: 'totalValue' });
      const finantial: IModelPropertyFinancial =
        thisProperty.propertyFinancial as IModelPropertyFinancial;
      const share = new BigNumber(prop.share)
        .dividedBy(100)
        .multipliedBy(finantial.totalValue);

      return share;
    } catch (error) {
      throw new BaseError(
        `Error calculating amount share of properties. ${error.message}`,
        500,
        'execPreviewFund |_calculateAmountShareProperty',
        error
      );
    }
  }

  private async _getPicturePropertyFromPackage(
    packId: string
  ): Promise<string> {
    try {
      const packProperties: IModelPackageProperty[] =
        await ModelPackageProperty.find({
          package: packId,
        });

      const properties: IModelProperty[] = await ModelProperty.find({
        _id: { $in: packProperties.map((e) => e.property) },
      });
      const imagesProperty = properties.map((e) => {
        const mainPicture: IModelPictures = e.mainPicture as IModelPictures;
        return mainPicture.url;
      });

      if (imagesProperty && imagesProperty.length) return imagesProperty[0];
      else return '';
    } catch (error) {
      throw new BaseError(
        `Error getting the url images for a package. ${error.message}`,
        500,
        error.name,
        error
      );
    }
  }

  private _getFundObject(fund: IModelFund): IBaseEntityFunds {
    try {
      const { id, name, cashHand, active } = fund;
      const response: IBaseEntityFunds = {
        id,
        name,
        cashHand,
        active,
      };

      return response;
    } catch (error) {
      throw new BaseError(
        `An error has been ocurred.`,
        500,
        'MongoDbUserError',
        error
      );
    }
  }
}
