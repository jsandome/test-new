import { injectable } from 'inversify';
import {
  BaseError,
  IAdditionalProspect,
  IBaseInvestorAddress,
  IBaseInvestorBigraphical,
  IBndBaseInvestorWrite,
  IBndMailerInfra,
  IBndResponseCreateOffering,
  IParamsCreateOfferingProspective,
  IParamsUpdateInvestor,
  IParamsUpdateOfferingProspective,
} from '@capa/core';
import {
  IModelInvestor,
  IModelOfferingProspective,
  ModelInvestor,
  ModelOfferingProspective,
} from '../../models';

@injectable()
export class ImplBndBaseInvestorWrite implements IBndBaseInvestorWrite {
  public async createOfferingProspective(
    investorId: string,
    dataOffering: IParamsCreateOfferingProspective,
    bndMailer: IBndMailerInfra
  ): Promise<IBndResponseCreateOffering> {
    try {
      const { offeringType, offering, status, expectedAmount, likeliHood } =
        dataOffering;
      const additionalProspect: IAdditionalProspect[] =
        dataOffering.additionalProspect;
      const investor: IModelInvestor = await ModelInvestor.findById(investorId);

      if (!investor) {
        throw new BaseError(
          'Investor not found',
          404,
          'BaseUseCaseInvestor | createOffering'
        );
      }

      const newOffering: IModelOfferingProspective =
        new ModelOfferingProspective({
          offeringType,
          offering,
          status,
          expectedAmount,
          likeliHood,
          additionalProspect,
        });
      newOffering.investor = investor._id;
      await newOffering.save();

      // TODO Add email sending when template its ready

      return { id: newOffering.id };
    } catch (error) {
      throw new BaseError(
        error.message,
        error.code,
        error.name,
        error
      );
    }
  }

  public async updateOfferingProspective(
    investorId: string,
    prospectiveId: string,
    params: IParamsUpdateOfferingProspective
  ): Promise<void> {
    try {
      const investor: IModelInvestor = await ModelInvestor.findById(investorId);
      if (!investor) {
        throw new BaseError(
          'Investor not found',
          404,
          'BaseUseCaseInvestor | updateOfferingProspective'
        );
      }
      const prospectiveOff: IModelOfferingProspective =
        await ModelOfferingProspective.findOne({
          _id: prospectiveId,
          investor: investor._id,
        });
      if (!prospectiveOff) {
        throw new BaseError(
          'Prospective offering not found',
          404,
          'BaseUseCaseInvestor | updateOfferingProspective'
        );
      }

      prospectiveOff.status = params.status;
      prospectiveOff.likeliHood = params.likeliHood;
      prospectiveOff.expectedAmount = params.expectedAmount;
      await prospectiveOff.save({});
    } catch (error) {
      throw new BaseError(
        error.message,
        error.code,
        error.name,
        error
      );
    }
  }

  public async updateInvestor(
    investorId: string,
    params: IParamsUpdateInvestor
  ): Promise<void> {
    const { email, emailType, phone, additionalDetails } = params;
    const addresses: IBaseInvestorAddress[] = params.addresses;
    const biographical: IBaseInvestorBigraphical = params.biographical;

    try {
      const investor: IModelInvestor = await ModelInvestor.findById(investorId);

      if (!investor) {
        throw new BaseError(
          'Investor not found',
          404,
          'BaseUseCaseInvestor | updateInvestor'
        );
      }

      investor.email = email;
      investor.emailType = emailType;
      investor.phone = phone;
      investor.additionalDetails = additionalDetails;
      investor.addresses = addresses;
      investor.biographical = biographical;
      await investor.save();
    } catch (error) {
      throw new BaseError(
        error.message,
        error.code,
        error.name,
        error
      );
    }
  }
}
