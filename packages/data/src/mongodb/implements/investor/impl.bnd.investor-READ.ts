import {
  BaseError,
  IBndBaseInvestorRead,
  IBndListInvestors,
  IBndResponseSelectInvestors,
  IBndReturnListInvestors,
  IModelPaginate,
  IPaginateParams,
} from '@capa/core';
import { injectable } from 'inversify';
import {
  IModelInvestor,
  IModelUser,
  ModelInvestor,
  ModelLlcInvestor,
} from '../../models';
import faker from 'faker';

@injectable()
export class ImplBndBaseInvestorRead implements IBndBaseInvestorRead {
  public async selectInvestors(
    search?: string,
    llcId?: string
  ): Promise<IBndResponseSelectInvestors[]> {
    try {
      let query = {};
      if (search) {
        query = {
          $or: [
            { 'name.firstName': { $regex: search, $options: 'i' } },
            { 'name.lastName': { $regex: search, $options: 'i' } },
          ],
        };
      }

      if (llcId) {
        const investorImplement = await ModelLlcInvestor.find({
          llc: llcId,
        }).populate({
          path: 'investor',
          populate: {
            path: 'user',
          },
        });

        const dataInvestor = investorImplement.map((a) => {
          return {
            id: a['investor'] ? a['investor']['_id'] : null,
            name: a['investor']
              ? `${a['investor']['name']['firstName']} ${a['investor']['name']['lastName']}`
              : null,
            email: a['investor']['user']
              ? a['investor']['user']['email']
              : null,
          };
        });

        return dataInvestor;
      }

      const investors = await ModelInvestor.find(query).populate('user');
      const investorsFormat = investors.map((i) => {
        const user = i.user as IModelUser;
        return {
          id: i._id.toHexString(),
          name: `${i.name.firstName} ${i.name.lastName}`,
          email: user ? `${user.email}` : null,
        };
      });

      return investorsFormat;
    } catch (error) {
      throw new BaseError(error.message, error.code, error.name, error);
    }
  }

  public async listInvestors({
    limit,
    page,
  }: IPaginateParams): Promise<IBndReturnListInvestors> {
    try {
      const options = {
        limit: 10,
        page: 1,
        pagination: true,
      };
      if (limit === undefined || page === undefined) {
        options.pagination = false;
      } else {
        options.limit = limit;
        options.page = page;
      }
      const dataInvestors: IModelPaginate<IModelInvestor[]> =
        await ModelInvestor.paginate(
          { active: true },
          {
            limit,
            page,
          }
        );

      // TODO Change commitment and investment fakers
      return {
        pagination: {
          itemCount: dataInvestors.totalDocs,
          offset: (page - 1) * limit,
          limit: dataInvestors.limit,
          totalPages: dataInvestors.totalPages,
          page: dataInvestors.page,
          nextPage: dataInvestors.nextPage,
          prevPage: dataInvestors.prevPage,
        },
        details: dataInvestors.docs.map((investor) => {
          return {
            id: investor._id.toHexString(),
            name: `${investor.name.firstName} ${investor.name.lastName}`,
            company: investor.biographical.companyName,
            commitment: faker.datatype.number(1000),
            investment: faker.datatype.number(1000),
            lastUpdate: investor.updatedBy.dateTime,
          } as IBndListInvestors;
        }),
      };
    } catch (error) {
      throw new BaseError(error.message, error.code, error.name, error);
    }
  }
}
