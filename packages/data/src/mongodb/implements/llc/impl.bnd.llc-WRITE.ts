import { injectable } from 'inversify';
import {
  IBaseEntityLlc,
  IBndBaseLlcWrite,
  BaseError,
  IBndReturnLlc,
} from '@capa/core';
import { startSession } from 'mongoose';
import {
  IModelLlc,
  ModelLlc,
  IModelLlcProperty,
  ModelLlcProperty,
  IModelLlcInvestor,
  ModelLlcInvestor,
} from '../../models';

@injectable()
export class ImplBndBaseLlcWriteMongo implements IBndBaseLlcWrite {
  public async createNewLlc(newLlc: IBaseEntityLlc): Promise<IBndReturnLlc> {
    let session;
    const {
      name,
      usPerson,
      taxId,
      address,
      state,
      date,
      investors,
      properties,
    } = newLlc;
    try {
      session = await startSession();
      session.startTransaction();

      const createLlc: IModelLlc = new ModelLlc({
        name,
        usPerson,
        taxId,
        address,
        state,
        date,
      });
      await createLlc.save({ session });

      for (const property of properties) {
        const llcProperty: IModelLlcProperty = new ModelLlcProperty({
          llc: createLlc._id,
          property: property.id,
          share: property.share,
        });
        await llcProperty.save({ session });
      }

      for (const investorId of investors) {
        const newLlcInvestor: IModelLlcInvestor = new ModelLlcInvestor({
          investor: investorId,
          llc: createLlc._id,
        });
        await newLlcInvestor.save({ session });
      }

      await createLlc.save({ session });

      await session.commitTransaction();
      session.endSession();

      return { id: createLlc._id.toHexString() };
    } catch (error) {
      await session.abortTransaction();
      session.endSession();

      throw new BaseError(
        'An error has been ocurred.',
        error.code,
        error.name,
        error
      );
    }
  }

  public async deleteLlc(llcId: string): Promise<void> {
    let session;
    try {
      session = await startSession();
      session.startTransaction();
      const deletedLlc = await ModelLlc.findOne({ _id: llcId });

      if (!deletedLlc || !deletedLlc.active) {
        throw new BaseError(
          `LLC not found.`,
          404,
          'BaseUseCaseLlcs | deleteLlc'
        );
      }

      deletedLlc.active = false;
      await deletedLlc.save({ session });

      await session.commitTransaction();
      session.endSession();
    } catch (error) {
      await session.abortTransaction();
      session.endSession();
      throw new BaseError(
        'An error has been ocurred.',
        error.code,
        error.name,
        error
      );
    }
  }

  public async updateLlc(
    llcId: string,
    params: IBaseEntityLlc
  ): Promise<IBndReturnLlc> {
    let session;
    const {
      name,
      usPerson,
      taxId,
      address,
      state,
      date,
      investors,
      properties,
    } = params;

    try {
      session = await startSession();
      session.startTransaction();
      const updatedLlc: IModelLlc = await ModelLlc.findOne({ _id: llcId });
      if (!updatedLlc) {
        throw new BaseError(
          `There is an invalid LLC id: ${llcId}`,
          404,
          'BaseUseCasePackage | updateLlc'
        );
      }

      updatedLlc.name = name;
      updatedLlc.address = address;
      updatedLlc.usPerson = usPerson;
      updatedLlc.taxId = taxId;
      updatedLlc.state = state;
      updatedLlc.date = date;
      await updatedLlc.save({ session });

      for (const property of properties) {
        const updatedLlcProperty: IModelLlcProperty =
          await ModelLlcProperty.findOne({ llc: llcId, property: property.id });

        if (updatedLlcProperty) {
          updatedLlcProperty.llc = updatedLlc.id;
          updatedLlcProperty.property = property.id;
          updatedLlcProperty.share = property.share;
          await updatedLlcProperty.save({ session });
        } else {
          const llcProperty: IModelLlcProperty = new ModelLlcProperty({
            llc: llcId,
            property: property.id,
            share: property.share,
          });
          await llcProperty.save({ session });
        }
      }

      //TODO: Define the update flow on investors llcs

      for (const investorId of investors) {
        const updatedLlcInvestor: IModelLlcInvestor =
          await ModelLlcInvestor.findOne({ llc: llcId, investor: investorId });
        if (updatedLlcInvestor) {
          updatedLlcInvestor.investor = investorId;
          updatedLlcInvestor.llc = updatedLlc.id;
          await updatedLlcInvestor.save({ session });
        } else {
          const newLlcInvestor: IModelLlcInvestor = new ModelLlcInvestor({
            investor: investorId,
            llc: llcId,
          });
          await newLlcInvestor.save({ session });
        }
      }

      await session.commitTransaction();
      session.endSession();

      return { id: updatedLlc._id.toHexString() };
    } catch (error) {
      await session.abortTransaction();
      session.endSession();

      throw new BaseError(
        'An error has been ocurred.',
        error.code,
        error.name,
        error
      );
    }
  }
}
