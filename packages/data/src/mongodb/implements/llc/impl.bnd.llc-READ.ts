import {
  IBndBaseLlcRead,
  BaseError,
  IBaseEntityGetLlc,
  IPaginateParams,
  IBaseResponseList,
  IBaseDetailsLlc,
  IBaseResponseOverviewDetails,
  IBaseGetDetails,
  IBaseModelLlcProperty,
  IBaseModelLlcInvestor,
  IBndBaseGCloudInfra,
  IBaseResponsePreviewLLC,
} from '@capa/core';
import { injectable } from 'inversify';
import {
  IModelLlc,
  ModelLlc,
  ModelLlcInvestor,
  ModelLlcProperty,
} from '../../models';
import { populationQuery, queryDetailsList, queryDetails } from '.';

@injectable()
export class ImplBndBaseLlcReadMongo implements IBndBaseLlcRead {
  public async checkExistsTaxId(taxId: string): Promise<IBaseEntityGetLlc> {
    try {
      return await ModelLlc.findOne({ taxId });
    } catch (error) {
      throw new BaseError(
        `An error occurred while trying to get a taxId LLC. ERROR ${error}`,
        500,
        'GenerateDBError. ImplBndBaseLlcReadMongo | checkExistsTaxId',
        error
      );
    }
  }

  public async checkExistName(name: string): Promise<void> {
    try {
      const createdLlc: IModelLlc = await ModelLlc.findOne({ name });
      if (createdLlc) {
        throw new BaseError(
          `Already exist an Entity with the name: ${name}.`,
          409,
          'BaseUseCaseLLC | createNewLlc'
        );
      }
    } catch (error) {
      throw new BaseError(error.message, error.code, error.name, error);
    }
  }

  public async searchLlcs(search?: string): Promise<IBaseEntityGetLlc[]> {
    try {
      const searchString = search
        ? { name: { $regex: search, $options: 'i' } }
        : null;

      const data = await ModelLlc.find(searchString).populate('state', 'name');

      return data.map(
        ({ _id, state, name, usPerson, taxId, address, date, active }) => {
          return {
            id: _id,
            state,
            name,
            usPerson,
            taxId,
            address,
            date,
            active,
          };
        }
      );
    } catch (error) {
      throw new BaseError(
        'An error has been ocurred.',
        500,
        'GenerateDBError. ImplBndBaseLlcReadMongo | searchLlcs',
        error
      );
    }
  }

  public async getByIdLlc(llcId: string): Promise<IBaseEntityGetLlc> {
    try {
      const getLlc: IModelLlc = await ModelLlc.findOne({ _id: llcId });

      if (!getLlc) {
        throw new BaseError('Entity not found.', 404, 'getByIdLlc');
      }
      return getLlc;
    } catch (error) {
      throw new BaseError(
        `An error has been ocurred.`,
        500,
        'MongoDbUserError',
        error
      );
    }
  }

  public async llcList({
    page = 1,
    limit,
  }: IPaginateParams): Promise<IBaseResponseList> {
    try {
      const options = {
        limit,
        page,
        offset: (page - 1) * limit,
        sort: { name: -1 },
      };

      const aggregate = ModelLlc.aggregate(populationQuery);

      const data = await ModelLlc.aggregatePaginate(aggregate, options);

      const resp: IBaseResponseList = {
        details: data.docs,
        pagination: {
          itemCount: data.totalDocs,
          offset: data.offset,
          limit: data.limit,
          totalPages: data.totalPages,
          page: data.page,
          nextPage: data.nextPage,
          prevPage: data.prevPage,
        },
      };

      return this._getLlcListObject(resp);
    } catch (error) {
      throw new BaseError(
        `An error has been ocurred.`,
        500,
        'MongoDbUserError',
        error
      );
    }
  }

  public async getDetails(llcId: string): Promise<IBaseGetDetails> {
    try {
      const query = queryDetails(llcId);
      const dataResult = await ModelLlc.aggregate(query);

      if (dataResult.length > 0) {
        return this._getDetailsLlc(dataResult[0] as IBaseGetDetails);
      }
      throw new BaseError('Entity not found.', 404, 'getDetails');
    } catch (error) {
      throw new BaseError(
        `An error has been ocurred. ${error.message}`,
        error.code,
        error.name,
        error
      );
    }
  }

  public async detailsOverview(
    llcId: string,
    bndGCloudInfra: IBndBaseGCloudInfra
  ): Promise<IBaseResponseOverviewDetails> {
    try {
      const fullPath = await bndGCloudInfra.getPublicUrl();
      const query = queryDetailsList(llcId, fullPath);
      const queryResult = await ModelLlcInvestor.aggregate(query);

      return queryResult[0];
    } catch (error) {
      throw new BaseError(
        `An error has been ocurred.`,
        500,
        'MongoDbUserError',
        error
      );
    }
  }

  public async getByIdLlcInvestor(
    llcId: string
  ): Promise<IBaseModelLlcInvestor[]> {
    try {
      const getLlcInvestor: IBaseModelLlcInvestor[] =
        await ModelLlcInvestor.find({
          llc: llcId,
        });

      if (!getLlcInvestor) {
        throw new BaseError(
          'LLC Investor not found.',
          404,
          'getByIdLlcInvestor'
        );
      }
      return getLlcInvestor;
    } catch (error) {
      throw new BaseError(
        `An error has been ocurred.`,
        500,
        'MongoDbUserError',
        error
      );
    }
  }

  public async getByIdLlcProperty(
    llcId: string
  ): Promise<IBaseModelLlcProperty[]> {
    try {
      const getLlcProperty: IBaseModelLlcProperty[] =
        await ModelLlcProperty.find({
          llc: llcId,
        });

      if (!getLlcProperty) {
        throw new BaseError(
          'LLC Property not found.',
          404,
          'getByIdLlcProperty'
        );
      }
      return getLlcProperty;
    } catch (error) {
      throw new BaseError(
        `An error has been ocurred.`,
        500,
        'MongoDbUserError',
        error
      );
    }
  }

  // TODO: Implement the request params llcId and then check the activity for each llc
  public async previewLlc(llcId: string): Promise<IBaseResponsePreviewLLC> {
    const llc: IBaseEntityGetLlc = await this.getByIdLlc(llcId);

    const dataResult = {
      activity: [
        {
          date: llc.date.toISOString(),
          description:
            'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.',
        },
        {
          date: llc.date.toISOString(),
          description:
            'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.',
        },
        {
          date: llc.date.toISOString(),
          description:
            'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.',
        },
      ],
    };

    return dataResult;
  }

  private _getLlcListObject(data: IBaseResponseList): IBaseResponseList {
    try {
      const { pagination, details } = data;
      const response: IBaseResponseList = {
        details: details.map((detail): IBaseDetailsLlc => {
          return {
            id: detail.id,
            name: detail.name,
            positions: detail.positions,
            activeCommitment: detail.activeCommitment.map((e) => this._gDN(e)),
            // TODO: The contribution sum flow has yet to be defined
            contributions: this._gDN(detail.contributions) || 0,
          };
        }),
        pagination: {
          itemCount: pagination.itemCount,
          offset: pagination.offset,
          limit: pagination.limit,
          totalPages: pagination.totalPages,
          page: pagination.page,
          nextPage: pagination.nextPage,
          prevPage: pagination.prevPage,
        },
      };

      return response;
    } catch (error) {
      throw new BaseError(
        `An error has been ocurred.`,
        500,
        'MongoDbUserError',
        error
      );
    }
  }

  private _getDetailsLlc(data: IBaseGetDetails): IBaseGetDetails {
    const {
      name,
      usPerson,
      taxId,
      address,
      state,
      date,
      investors,
      properties,
      share,
    } = data;
    const response: IBaseGetDetails = {
      name,
      usPerson: Boolean(usPerson),
      taxId,
      address,
      date,
      state,
      properties: properties.map((property) => {
        const [value] = share;

        return {
          id: property.id,
          name: property.name,
          share: this._gDN(value),
        };
      }),
      investors: investors.map((investor) => {
        return {
          id: investor.id,
          name: investor.name,
        };
      }),
    };
    return response;
  }

  private _gDN(num: number): number {
    return parseFloat(`${num}`);
  }
}
