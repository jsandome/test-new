import faker from 'faker';
import { ObjectId } from 'mongodb';

// TODO: The contribution sum flow has yet to be defined
export const populationQuery = [
  {
    $match: {
      active: true,
    },
  },
  {
    $lookup: {
      from: 'llcinvestors',
      localField: '_id',
      foreignField: 'llc',
      as: 'llcinvestors',
    },
  },
  {
    $lookup: {
      from: 'fundinvestors',
      localField: 'llcinvestors.investor',
      foreignField: 'investor',
      as: 'investors',
    },
  },
  {
    $group: {
      _id: {
        id: '$_id',
        name: '$name',
        positions: {
          $cond: {
            if: { $isArray: '$investors' },
            then: { $size: '$investors' },
            else: 0,
          },
        },
        activeCommitment: '$investors.totalCommitment',
        contributions: `${faker.datatype.number()}`,
      },
    },
  },
  {
    $project: {
      _id: 0,
      id: '$_id.id',
      name: '$_id.name',
      positions: '$_id.positions',
      activeCommitment: '$_id.activeCommitment',
      contributions: '$_id.contributions',
    },
  },
];

export const queryDetailsList = (llcId, fullPath) => {
  return [
    { $match: { llc: new ObjectId(llcId) } },
    {
      $lookup: {
        from: 'investors',
        localField: 'investor',
        foreignField: '_id',
        as: 'investors',
      },
    },
    { $unwind: { path: '$investors' } },
    {
      $lookup: {
        from: 'llcs',
        localField: 'llc',
        foreignField: '_id',
        as: 'llcs',
      },
    },
    { $unwind: { path: '$llcs' } },
    {
      $lookup: {
        from: 'llcproperties',
        let: { property: '$llcs._id' },
        pipeline: [
          { $match: { $expr: { $eq: ['$llc', '$$property'] } } },
          {
            $lookup: {
              from: 'properties',
              localField: 'property',
              foreignField: '_id',
              as: 'properties',
            },
          },
          { $unwind: { path: '$properties' } },
        ],
        as: 'llcproperties',
      },
    },
    { $unwind: { path: '$llcproperties' } },
    {
      $group: {
        _id: {
          entity: {
            name: '$llcs.name',
            usPerson: '$llcs.usPerson',
            taxId: '$llcs.taxId',
            address: '$llcs.address',
            date: '$llcs.date',
          },
        },
        investors: {
          $push: {
            id: '$investors._id',
            name: {
              $concat: [
                '$investors.name.firstName',
                ' ',
                '$investors.name.lastName',
              ],
            },
          },
        },
        properties: {
          $push: {
            id: '$llcproperties.properties._id',
            name: '$llcproperties.properties.name',
            address: '$llcproperties.properties.address',
            projectedIRR: `${faker.datatype.float()}`, // TODO: change value for projectedIRR
            myShare: `${faker.datatype.float()}`, // TODO: change value for myShare
            estimatedValue: `${faker.datatype.float()}`, // TODO: change value forchange value for estimatedValue
            loanAmmount: `${faker.datatype.float()}`, // TODO: change value for loanAmmount
            estimateEquity: `${faker.datatype.float()}`, // TODO: change value for estimateEquity
            type: '$llcproperties.properties.type',
            pictures: {
              $map: {
                input: '$llcproperties.properties.pictures.url',
                as: 'url',
                in: { $concat: [`${fullPath}`, '/', '$$url'] },
              },
            },
            mainPicture: {
              $concat: [
                `${fullPath}`,
                '/',
                '$llcproperties.properties.mainPicture.url',
              ],
            },
          },
        },
      },
    },
    {
      $project: {
        _id: 0,
        entity: '$_id.entity',
        investors: '$investors',
        properties: '$properties',
      },
    },
  ];
};

export const queryDetails = (llcId) => {
  return [
    { $match: { _id: new ObjectId(llcId) } },
    {
      $lookup: {
        from: 'llcinvestors',
        localField: '_id',
        foreignField: 'llc',
        as: 'llcinvestors',
      },
    },
    {
      $lookup: {
        from: 'investors',
        localField: 'llcinvestors.investor',
        foreignField: '_id',
        as: 'investors',
      },
    },
    {
      $lookup: {
        from: 'llcproperties',
        localField: '_id',
        foreignField: 'llc',
        as: 'llcproperties',
      },
    },
    {
      $lookup: {
        from: 'properties',
        localField: 'llcproperties.property',
        foreignField: '_id',
        as: 'properties',
      },
    },
    {
      $group: {
        _id: {
          name: '$name',
          usPerson: '$usPerson',
          taxId: '$taxId',
          address: '$address',
          state: '$state',
          date: '$date',
          investors: '$investors',
          properties: '$properties',
          llcproperties: '$llcproperties',
        },
      },
    },
    {
      $project: {
        _id: 0,
        name: '$_id.name',
        usPerson: '$_id.usPerson',
        taxId: '$_id.taxId',
        address: '$_id.address',
        state: '$_id.state',
        date: '$_id.date',
        properties: {
          $map: {
            input: '$_id.properties',
            as: 'property',
            in: {
              id: '$$property._id',
              name: '$$property.name',
            }
          }
        },
        investors: {
          $map: {
            input: {
              $filter: {
                input: '$_id.investors',
                as: 'dataInvestors',
                cond: '$$dataInvestors',
              },
            },
            as: 'investor',
            in: {
              id: '$$investor._id',
              name: {
                $concat: [
                  '$$investor.name.firstName',
                  ' ',
                  '$$investor.name.lastName',
                ],
              },
            },
          },
        },
        share: '$_id.llcproperties.share',
      },
    },
  ];
};
