import { injectable } from 'inversify';
import {
  BaseError,
  EnumGCloudDocumentsStatus,
  IBndGCloudDocumentsRead,
  IGCloudDocumentList,
  IGCloudDocumentListResponse,
  IGCloudDocumentSelectResponse,
  IGCloudDocumentsQueryParams,
  IModelPaginate,
} from '@capa/core';
import { IModelGCloudDocument, ModelGCloudDocument } from '../../../models';
import { DateTime } from 'luxon';
@injectable()
export class ImplBndGCloudDocumentsRead implements IBndGCloudDocumentsRead {
  public async getGCloudDocumentsList(
    queryParams: IGCloudDocumentsQueryParams,
    publicURLGCloud: string
  ): Promise<IGCloudDocumentList> {
    try {
      const { limit, page, search } = queryParams;
      const searchObject = {
        status: { $eq: EnumGCloudDocumentsStatus.SUCCESS },
      };
      if (search) {
        searchObject['originalName'] = {
          $regex: search,
          $options: 'i',
        };
      }
      const dataDocuments: IModelPaginate<IModelGCloudDocument[]> =
        await ModelGCloudDocument.paginate(searchObject, {
          limit,
          page,
          sort: 'name',
        });
      return {
        pagination: {
          itemCount: dataDocuments.totalDocs,
          offset: (page - 1) * limit,
          limit: dataDocuments.limit,
          totalPages: dataDocuments.totalPages,
          page: dataDocuments.page,
          nextPage: dataDocuments.nextPage,
          prevPage: dataDocuments.prevPage,
        },
        details: dataDocuments.docs.map(
          (document): IGCloudDocumentListResponse => {
            return {
              id: document.id,
              name: document.originalName,
              date: DateTime.fromISO(document.createdBy.dateTime).toISODate(),
              url: `${publicURLGCloud}/${document.url}`,
              addTaxCenter: document.taxCenter,
            };
          }
        ),
      };
    } catch (error) {
      throw new BaseError(
        `An error occurred while trying to get the list of documents. ${error.message}`,
        error.code,
        'getGCloudDocumentsList',
        error
      );
    }
  }
  public async selectDocuments(
    search: string,
    publicUrlGcloud: string
  ): Promise<IGCloudDocumentSelectResponse[]> {
    try {
      const searchObject = {
        status: { $eq: EnumGCloudDocumentsStatus.SUCCESS },
      };
      if (search) {
        searchObject['originalName'] = {
          $regex: search,
          $options: 'i',
        };
      }
      const documents: IModelGCloudDocument[] = await ModelGCloudDocument.find(
        searchObject
      );
      return documents.map(
        (document: IModelGCloudDocument): IGCloudDocumentSelectResponse => {
          return {
            id: document.id,
            name: document.originalName,
          };
        }
      );
    } catch (error) {
      throw new BaseError(
        `An error occurred while trying to get the search of documents. ${error.message}`,
        error.code,
        'selectDocuments',
        error
      );
    }
  }
}
