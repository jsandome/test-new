import { injectable } from 'inversify';
import {
  BaseError,
  EnumGCloudDocumentsStatus,
  IBndBaseGCloudInfra,
  IBndGCloudDocumentsWrite,
  IGCloudDocumentCreateParams,
  IGCloudDocumentCreateResponse,
} from '@capa/core';
import { IModelGCloudDocument, ModelGCloudDocument } from '../../../models';
import { DateTime } from 'luxon';

@injectable()
export class ImplBndGCloudDocumentsWrite implements IBndGCloudDocumentsWrite {
  public async saveGClodDocument(
    gcloudInfra: IBndBaseGCloudInfra,
    params: IGCloudDocumentCreateParams
  ): Promise<IGCloudDocumentCreateResponse> {
    try {
      const documentName: string = params.path.slice(
        params.path.lastIndexOf('/') + 1,
        params.path.length
      );
      const extension: string = documentName.slice(
        documentName.lastIndexOf('.'),
        documentName.length
      );
      const newDocument: IModelGCloudDocument = new ModelGCloudDocument({
        originalName: documentName,
        url: params.path,
        taxCenter: params.addTaxCenter,
        createdBy: {
          id: params.userRequest,
          dateTime: DateTime.now().toISODate(),
        },
      });
      await newDocument.save();
      const newPathDocument = `documents/${newDocument.id}${extension}`;
      await gcloudInfra.moveFile(params.path, newPathDocument);
      newDocument.url = newPathDocument;
      newDocument.status = EnumGCloudDocumentsStatus.SUCCESS;
      newDocument.save();
      return {
        id: newDocument.id,
        name: newDocument.originalName,
      };
    } catch (error) {
      throw new BaseError(
        `An error occurred while trying to save a document.${error.message}`,
        error.code,
        'saveGClodDocument',
        error
      );
    }
  }

  public async deleteGCloudDocument(
    documentID: string,
    gcloudInfra: IBndBaseGCloudInfra
  ): Promise<void> {
    try {
      const document: IModelGCloudDocument = await ModelGCloudDocument.findOne({
        _id: documentID,
        status: { $ne: EnumGCloudDocumentsStatus.DELETED },
      });
      if (!document)
        throw new BaseError(`Document not found`, 404, 'deleteGCloudDocument');
      await gcloudInfra.deleteFile(document.url);
      document.status = EnumGCloudDocumentsStatus.DELETED;
      await document.save();
    } catch (error) {
      throw new BaseError(
        `An error occurred while trying to delete a document.${error.message}`,
        error.code,
        'deleteGCloudDocument',
        error
      );
    }
  }
}
