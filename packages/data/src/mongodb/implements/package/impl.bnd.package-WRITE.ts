import { injectable } from 'inversify';
import {
  BaseError,
  IBndPackageWrite,
  IBndReturnCreatePackage,
  IParamsCreatePackage,
  IInvestorsDetailPackage,
  IPropertiesPackage,
} from '@capa/core';
import {
  IModelPackage,
  IModelPackageInvestor,
  IModelPackageProperty,
  ModelPackage,
  ModelPackageInvestor,
  ModelPackageProperty,
} from '../../models';
import { startSession } from 'mongoose';

@injectable()
export class ImplBndBasePackageWrite implements IBndPackageWrite {
  public async createPackage(
    params: IParamsCreatePackage
  ): Promise<IBndReturnCreatePackage> {
    const { name, totalValue } = params;
    const investors: IInvestorsDetailPackage[] = params.investors;
    const properties: IPropertiesPackage[] = params.properties;
    const session = await startSession();
    session.startTransaction();

    try {
      const pack = await ModelPackage.findOne({ name });
      if (pack) {
        throw new BaseError(
          `Already exist a package with the name: ${name}.`,
          409,
          'BaseUseCasePackage | createPackage'
        );
      }

      const newPack: IModelPackage = new ModelPackage({
        name,
        totalValue,
        active: true,
      });
      await newPack.save({ session });

      for (let investor of investors) {
        const investorId = investor.id;
        const { minCommitment, initCommitment, totalCommitment, termYears } =
          investor;

        const newInvestor: IModelPackageInvestor = new ModelPackageInvestor({
          package: newPack._id,
          investor: investorId,
          minCommitment: minCommitment,
          initCommitment: initCommitment,
          totalCommitment: totalCommitment,
          termYears: termYears,
        });
        await newInvestor.save({ session });
      }

      for (let element of properties) {
        const packageProperty: IModelPackageProperty = new ModelPackageProperty(
          {
            package: newPack._id,
            property: element.id,
            share: element.share,
          }
        );

        await packageProperty.save({ session });
      }

      await session.commitTransaction();
      session.endSession();

      return { id: newPack._id.toHexString() };
    } catch (error) {
      await session.abortTransaction();
      session.endSession();

      throw new BaseError(error.message, error.code, error.name, error);
    }
  }

  public async updatePackage(
    packageId: string,
    params: IParamsCreatePackage
  ): Promise<void> {
    const { name, totalValue } = params;
    const investors: IInvestorsDetailPackage[] = params.investors;
    const properties: IPropertiesPackage[] = params.properties;
    const session = await startSession();
    session.startTransaction();

    try {
      const actualPackage = await ModelPackage.findById(packageId);
      if (!actualPackage) {
        throw new BaseError(
          `There is an invalid package id: ${packageId}`,
          404,
          'BaseUseCasePackage | updatePackage'
        );
      }

      actualPackage.name = name;
      actualPackage.totalValue = totalValue;
      await actualPackage.save({ session });

      for (let investor of investors) {
        const actualPackInvestor: IModelPackageInvestor =
          await ModelPackageInvestor.findOne({
            package: actualPackage._id,
            investor: investor.id,
          });

        if (!actualPackInvestor) {
          throw new BaseError(
            `There is an invalid Package id: ${actualPackage._id} - Investor id: ${investor.id} in PackageInvestor`,
            404,
            'BaseUseCasePackage | updatePackage'
          );
        }

        const { minCommitment, initCommitment, totalCommitment, termYears } =
          investor;

        actualPackInvestor.minCommitment = minCommitment;
        actualPackInvestor.initCommitment = initCommitment;
        actualPackInvestor.totalCommitment = totalCommitment;
        actualPackInvestor.termYears = termYears;
        await actualPackInvestor.save({ session });
      }

      for (let property of properties) {
        const actualPackageProperty: IModelPackageProperty =
          await ModelPackageProperty.findOne({
            property: property.id,
            package: actualPackage._id,
          });

        if (!actualPackageProperty) {
          throw new BaseError(
            `There is an invalid Package id: ${actualPackage._id} - Property id: ${property.id} in PackageInvestor`,
            404,
            'BaseUseCasePackage | updatePackage'
          );
        }

        actualPackageProperty.share = property.share;
        await actualPackageProperty.save({ session });
      }

      await session.commitTransaction();
      session.endSession();
    } catch (error) {
      await session.abortTransaction();
      session.endSession();

      throw new BaseError(
        `Error creating a new Package. ${error.message}`,
        error.code,
        error.name,
        error
      );
    }
  }

  public async deletePackage(packageId: string): Promise<void> {
    const session = await startSession();
    session.startTransaction();

    try {
      const actualPackage = await ModelPackage.findById(packageId);
      if (!actualPackage) {
        throw new BaseError(
          `There is an invalid package id: ${packageId}`,
          404,
          'BaseUseCasePackage | updatePackage'
        );
      }

      // Move Packages to closed section
      actualPackage.active = false;
      await actualPackage.save({ session });

      await session.commitTransaction();
      session.endSession();
    } catch (error) {
      await session.abortTransaction();
      session.endSession();

      throw new BaseError(
        'Error creating a new Package',
        error.code,
        error.name,
        error
      );
    }
  }
}
