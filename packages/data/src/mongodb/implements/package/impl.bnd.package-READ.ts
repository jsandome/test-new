import { injectable } from 'inversify';
import {
  BaseError,
  IBndPackageRead,
  IBaseEntityPackage,
  IPropertyPackageShare,
  IBndReturnDetailPackage,
  IPropertiesDetailPackage,
  IBndBaseGCloudInfra,
  IInvestorDetailPackage,
} from '@capa/core';
import {
  IModelPackage,
  ModelFundProperty,
  ModelPackage,
  ModelPackageProperty,
  ModelLlcProperty,
  IModelFundProperty,
  IModelLlcProperty,
  IModelPackageProperty,
  IModelProperty,
  ModelProperty,
  IModelPropertyFinancial,
  ModelPackageInvestor,
  IModelPackageInvestor,
  IModelPictures,
  ModelFundPackage,
  IModelFundPackage,
} from '../../models';
import Bignumber from 'bignumber.js';

@injectable()
export class ImplBndBasePackageRead implements IBndPackageRead {
  public async selectPackages(
    search: string,
    bndGCloudInfra: IBndBaseGCloudInfra
  ): Promise<IBaseEntityPackage[]> {
    try {
      const query = {};
      if (search) {
        query['name'] = {
          $regex: search,
          $options: 'i',
        };
      }

      const fullPath = await bndGCloudInfra.getPublicUrl();
      const packages: IModelPackage[] = await ModelPackage.find(query);
      let packsProperties: IBaseEntityPackage[] = [];
      for (let p of packages) {
        let pathMainImage = '';
        const packProperty = await ModelPackageProperty.find({
          package: p._id,
        }).populate({ path: 'property', select: 'mainPicture' });

        if (packProperty && packProperty.length) {
          const thisProperty: IModelProperty = packProperty[0]
            .property as IModelProperty;
          const mainPicture: IModelPictures =
            thisProperty.mainPicture as IModelPictures;
          pathMainImage = `${fullPath}/${mainPicture.url}`;
        }

        packsProperties.push({
          id: p._id.toHexString(),
          name: p.name,
          totalValue: parseFloat(`${p.totalValue}`),
          mainImage: pathMainImage,
          shareAvailable: await this._getAvailableSharePackage(
            p._id.toHexString()
          ),
        });
      }

      return packsProperties;
    } catch (error) {
      throw new BaseError(
        error.message,
        error.code,
        'BasePackageController | selectPackages',
        error
      );
    }
  }

  public async calculateTotalValue(
    properties: IPropertyPackageShare[]
  ): Promise<number> {
    try {
      let totalValue = new Bignumber(0);
      for (let property of properties) {
        let thisShare = new Bignumber(property.share);
        let totalShare = new Bignumber(0).plus(thisShare);
        // Entitites to validate available share from properties
        const funds: IModelFundProperty[] = await ModelFundProperty.find({
          property: property.id,
        }).select('share');
        const llcs: IModelLlcProperty[] = await ModelLlcProperty.find({
          property: property.id,
        }).select('share');
        const packages: IModelPackageProperty[] =
          await ModelPackageProperty.find({
            property: property.id,
          }).select('share');
        const oneProperty: IModelProperty = await ModelProperty.findById(
          property.id
        ).populate('propertyFinancial');

        for (let f of funds) totalShare = totalShare.plus(f.share);
        for (let l of llcs) totalShare = totalShare.plus(l.share);
        for (let p of packages) totalShare = totalShare.plus(p.share);

        if (totalShare.isGreaterThan(100)) {
          throw new BaseError(
            'There is a property without valid share value',
            409,
            'BasePackageController | calculateTotalValue'
          );
        }

        const inPercent = thisShare.dividedBy(100);
        const propertyValue = (
          oneProperty.propertyFinancial as IModelPropertyFinancial
        ).totalValue;
        const amountFromProperty = inPercent.multipliedBy(propertyValue);
        totalValue = totalValue.plus(amountFromProperty);
      }

      return this._numberFormat(totalValue);
    } catch (error) {
      throw new BaseError(
        `There is an error in try to calculate the total value. ${error.message}`,
        error.code,
        error.name,
        error
      );
    }
  }

  public async getTotalValuePackage(
    properties: IPropertyPackageShare[]
  ): Promise<number> {
    try {
      let totalValue = new Bignumber(0);

      for (let prop of properties) {
        await this._validateSharePropertyInPackage(prop);
        totalValue = totalValue.plus(
          await this._calculateAmountSharePropertyPackage(prop)
        );
      }

      return parseFloat(totalValue.toNumber().toFixed(2));
    } catch (error) {
      throw new BaseError(
        `There is an error in try to calculate the total value. ${error.message}`,
        error.code,
        error.name,
        error
      );
    }
  }

  private async _calculateAmountSharePropertyPackage(
    prop: IPropertyPackageShare
  ): Promise<Bignumber> {
    try {
      const property: IModelProperty = await ModelProperty.findById(
        prop.id
      ).populate({ path: 'propertyFinancial', select: 'totalValue' });
      const finantial: IModelPropertyFinancial =
        property.propertyFinancial as IModelPropertyFinancial;
      const share = new Bignumber(prop.share)
        .dividedBy(100)
        .multipliedBy(finantial.totalValue);

      return share;
    } catch (error) {
      throw new BaseError(
        `Error calculating amount share of properties. ${error.message}`,
        500,
        'execPreviewFund |_calculateAmountShareProperty',
        error
      );
    }
  }

  public async _validateSharePropertyInPackage(
    property: IPropertyPackageShare
  ): Promise<void> {
    const packProperties = await ModelPackageProperty.find({
      property: property.id,
    });

    let share = new Bignumber(0);
    for (let prop of packProperties) {
      share = share.plus(prop.share);
    }

    share = share.plus(property.share);
    if (share.isGreaterThan(100)) {
      throw new BaseError(
        'The package share value should be lees o equal than 100',
        400,
        '_validateSharePropertyInPackage'
      );
    }
  }

  public async getDetailPackage(
    packageId: string
  ): Promise<IBndReturnDetailPackage> {
    try {
      const pack: IModelPackage = await ModelPackage.findOne({
        _id: packageId,
        active: true,
      });
      if (!pack) {
        throw new BaseError(
          `There is an invalid package id: ${packageId}`,
          404,
          'BaseUseCasePackage | getDetailPackage'
        );
      }

      const packInvestors: IModelPackageInvestor[] =
        await ModelPackageInvestor.find({
          package: packageId,
        }).populate('investor', '_id name');
      const packProperties: IModelPackageProperty[] =
        await ModelPackageProperty.find({
          package: packageId,
        }).populate('property', '_id name');

      return {
        id: pack._id.toHexString(),
        name: pack.name,
        totalValue: new Bignumber(pack.totalValue).toNumber(),
        investors: !packInvestors
          ? []
          : packInvestors.map((i) => {
              let inv: unknown = i.investor;
              const investor: IInvestorDetailPackage =
                inv as IInvestorDetailPackage;

              return {
                id: investor._id,
                name: `${investor.name.firstName} ${investor.name.lastName}`,
                minCommitment: new Bignumber(i.minCommitment).toNumber(),
                initCommitment: new Bignumber(i.initCommitment).toNumber(),
                totalCommitment: new Bignumber(i.totalCommitment).toNumber(),
                termYears: i.termYears,
              };
            }),
        properties: !packProperties
          ? []
          : packProperties.map((p) => {
              let prop: unknown = p.property;
              const property: IPropertiesDetailPackage =
                prop as IPropertiesDetailPackage;

              return {
                id: property._id,
                name: property.name,
                share: new Bignumber(p.share).toNumber(),
              };
            }),
      };
    } catch (error) {
      throw new BaseError(
        `There is an error getting detail for a Package. ${error.message}`,
        error.code,
        error.name,
        error
      );
    }
  }

  private _numberFormat(number: Bignumber): number {
    return parseFloat(number.toNumber().toFixed(2));
  }

  private async _getAvailableSharePackage(packId: string): Promise<number> {
    try {
      let shareAvailable = new Bignumber(0);
      const packsProperties: IModelFundPackage[] = await ModelFundPackage.find({
        package: packId,
      });

      for (let pProp of packsProperties) {
        shareAvailable = shareAvailable.plus(pProp.share);
      }

      shareAvailable = new Bignumber(100).minus(shareAvailable);
      return parseFloat(shareAvailable.toNumber().toFixed(2));
    } catch (error) {
      throw new BaseError(
        `There is an error getting shareAvailable. ${error.message}`,
        error.code,
        error.name,
        error
      );
    }
  }
}
