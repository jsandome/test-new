import { injectable } from 'inversify';
import {
  BaseError,
  IBndValidShareData,
  IExtraPayments,
  IPropertyPackageShare,
  IPropertyCreditlineShare,
  IBaseEntityFundPackage,
} from '@capa/core';
import Bignumber from 'bignumber.js';
import { ModelProperty, ModelInvestor, ModelPackage } from '../../models';

@injectable()
export class ImplBndBaseValidShareData implements IBndValidShareData {
  /**
   * Sum all extraPayments
   * @return allPayments - sum od all extra payments
   **/
  public async sumExtraPayments(
    extraPayments: IExtraPayments[]
  ): Promise<Number> {
    let allPayments = new Bignumber(0);

    extraPayments.forEach((e) => {
      allPayments = allPayments.plus(e.amount);
    });

    return this._numberFormat(allPayments);
  }

  /**
   * Sum all shares to be equal to 100 %. Throw error if percentage > 100
   */
  public async validPercentages(
    properties: IPropertyCreditlineShare[] | IPropertyPackageShare[]
  ): Promise<void> {
    let sumPercentage = new Bignumber(0);

    properties.forEach((e) => {
      sumPercentage = sumPercentage.plus(e.share);
    });

    if (sumPercentage.isLessThan(100) || sumPercentage.isGreaterThan(100)) {
      throw new BaseError(
        'The property share value should be 100',
        400,
        'validPercentages'
      );
    }
  }

  /**
   * Valid properties exists on DB
   */
  public async validProperties(
    properties: IPropertyCreditlineShare[] | IPropertyPackageShare[]
  ): Promise<void> {
    properties.forEach(async (p) => {
      const property = await ModelProperty.findById(p.id);
      if (!property) {
        throw new BaseError(
          `Property with ID: ${p.id} was not founded`,
          404,
          'validProperties'
        );
      }
    });
  }

  /**
   * Valid packages exist on DB
   * @param packages
   */
  public async validPackages(
    packages: IBaseEntityFundPackage[]
  ): Promise<void> {
    try {
      if (packages) {
        packages.forEach(async (p) => {
          const pack = await ModelPackage.findById(p.id);
          if (!pack) {
            throw new BaseError(
              `Package with ID: ${p.id} was not founded`,
              404,
              'validPackages'
            );
          }
        });
      }
    } catch (error) {
      throw new BaseError(error.message, error.code, error.name, error);
    }
  }

  /**
   * Can similar to the properties function
   * @param investors
   * @return null | exception error 404
   */
  public async validInvestors(investors: string[]): Promise<void> {
    try {
      investors.forEach(async (inv) => {
        const investor = await ModelInvestor.findById(inv);

        if (!investor) {
          throw new BaseError(
            `Investor with ID: ${inv} was not founded`,
            404,
            'validInvestors'
          );
        }
      });
    } catch (error) {
      throw new BaseError(error.message, error.code, error.name, error);
    }
  }

  public sumActiveCommitmentLLC(activeCommitment: number[]): number {
    let allActive = new Bignumber(0);

    activeCommitment.forEach((e) => {
      allActive = allActive.plus(e);
    });
    return this._numberFormat(allActive);
  }

  public sumActiveCommitmentTotalLLC(totalActive: number[]): number {
    let allTotalActive = new Bignumber(0);

    totalActive.forEach((e) => {
      allTotalActive = allTotalActive.plus(e);
    });
    return this._numberFormat(allTotalActive);
  }

  private _numberFormat(number: Bignumber): number {
    return parseFloat(number.toNumber().toFixed(2));
  }
}
