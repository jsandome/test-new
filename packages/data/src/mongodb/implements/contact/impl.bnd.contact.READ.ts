import { injectable } from 'inversify';
import {
  BaseError,
  IBaseEntityContact,
  IBndBaseContactRead,
  IBndListContacts,
  IBndReturnListContacts,
  IBndSelectContacts,
  IModelPaginate,
  IPaginateParamsContacts,
} from '@capa/core';
import { IModelContact, ModelContact } from '../../models';

@injectable()
export class ImplBndBaseContactRead implements IBndBaseContactRead {
  public async getContact(contactId: string): Promise<IBaseEntityContact> {
    try {
      const contact: IModelContact = await ModelContact.findById(
        contactId
      ).select('-__v -createdBy -updatedBy');
      if (!contact) {
        throw new BaseError(
          `Contact not found.`,
          404,
          'BaseUseCaseContact | getContact'
        );
      }

      return {
        id: contact._id.toHexString(),
        name: contact.name,
        jobTitle: contact.jobTitle,
        companyName: contact.companyName,
        currentJob: contact.currentJob,
        email: contact.email,
        phone: contact.phone,
        address: contact.address,
        city: contact.city,
        county: contact.county,
        state: contact.state,
        zip: contact.zip,
      };
    } catch (error) {
      throw new BaseError(error.message, error.code, error.name, error);
    }
  }

  public async listContacts({
    limit = 10,
    page = 1,
  }: IPaginateParamsContacts): Promise<IBndReturnListContacts> {
    try {
      const dataContact: IModelPaginate<IModelContact[]> =
        await ModelContact.paginate(null, {
          limit,
          page,
        });

      return {
        pagination: {
          itemCount: dataContact.totalDocs,
          offset: (page - 1) * limit,
          limit: dataContact.limit,
          totalPages: dataContact.totalPages,
          page: dataContact.page,
          nextPage: dataContact.nextPage,
          prevPage: dataContact.prevPage,
        },
        details: dataContact.docs.map((contact) => {
          return {
            id: contact._id.toHexString(),
            name: contact.name,
            email: contact.email,
            company: contact.companyName,
            jobTitle: contact.jobTitle,
            lastUpdate: contact.updatedBy.dateTime,
          } as IBndListContacts;
        }),
      };
    } catch (error) {
      throw new BaseError(error.message, error.code, error.name, error);
    }
  }

  public async selectContacts(search: string): Promise<IBndSelectContacts[]> {
    try {
      const searchQ = {};
      if (search) {
        searchQ['name'] = {
          $regex: search,
          $options: 'i',
        };
      }
      const contacts: IModelContact[] = await ModelContact.find(searchQ);
      const allContacts = contacts.map((c) => {
        return {
          id: c._id.toHexString(),
          name: c.name,
          email: c.email,
        } as IBndSelectContacts;
      });

      return allContacts;
    } catch (error) {
      throw new BaseError(error.message, error.code, error.name, error);
    }
  }
}
