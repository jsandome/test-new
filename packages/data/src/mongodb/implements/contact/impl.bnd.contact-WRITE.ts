import { injectable } from 'inversify';
import {
  BaseError,
  IBndBaseContactWrite,
  IBndResponseCreateContact,
  IBndResponseUpdateContact,
  IParamsCreateContact,
  IParamsUpdateContact,
} from '@capa/core';
import { IModelContact, ModelContact } from '../../models';

@injectable()
export class ImplBndBaseContactWrite implements IBndBaseContactWrite {
  public async createContact(
    params: IParamsCreateContact
  ): Promise<IBndResponseCreateContact> {
    try {
      const { email } = params;
      const prevContact: IModelContact = await ModelContact.findOne({ email });
      if (prevContact) {
        throw new BaseError(
          `Already exist a contact with the email: ${email}`,
          409,
          'BaseUseCaseContact | createContact'
        );
      }

      const newContact: IModelContact = await new ModelContact(params);
      await newContact.save();

      return { id: newContact.id };
    } catch (error) {
      throw new BaseError(error.message, error.code, error.name, error);
    }
  }

  public async updateContact(
    contactId: string,
    params: IParamsUpdateContact
  ): Promise<IBndResponseUpdateContact> {
    try {
      const { email } = params;
      const prevContact: IModelContact = await ModelContact.findOne({
        _id: { $ne: contactId },
        email,
      });
      if (prevContact) {
        throw new BaseError(
          `Already exist a contact with the email: ${email}`,
          409,
          'BaseUseCaseContact'
        );
      }

      const oneContact: IModelContact = await ModelContact.findById(contactId);
      if (!oneContact) {
        throw new BaseError(`Contact not found.`, 404, 'BaseUseCaseContact');
      }

      oneContact.name = params.name;
      oneContact.jobTitle = params.jobTitle;
      oneContact.companyName = params.companyName;
      oneContact.currentJob = params.currentJob;
      oneContact.email = params.email;
      oneContact.phone = params.phone;
      oneContact.address = params.address;
      oneContact.city = params.city;
      oneContact.county = params.county;
      oneContact.state = params.state;
      oneContact.zip = params.zip;
      await oneContact.save();

      return {};
    } catch (error) {
      throw new BaseError(error.message, error.code, error.name, error);
    }
  }

  // TODO Validate if in Contacts apply for soft delete & hard delete
  public async deleteContact(contactId: string): Promise<void> {
    try {
      const oneContact: IModelContact = await ModelContact.findById(contactId);
      if (!oneContact) {
        throw new BaseError(
          `Contact not found.`,
          404,
          'BaseUseCaseContact | deleteContact'
        );
      }

      await oneContact.deleteOne();
    } catch (error) {
      throw new BaseError(error.message, error.code, error.name, error);
    }
  }
}
