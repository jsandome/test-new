import { injectable } from 'inversify';
import {
  IBndBasePropertyRead,
  EnumPropertyTypes,
  EnumPropertyStrategies,
  IBaseEntityProperty,
  IBaseEntityPropertyFinancialResponse,
  IBaseEntityPropertyFinancialQuarterResponse,
  IBaseQuarterDate,
  IBaseQuarterDateCheck,
  BaseError,
  IQueryEntityPropertyFinancial,
  PropertyType,
  PropertyStrategy,
  IBEPropertyGetParams,
  IBEGetPropertiesResponse,
  FundPropertyResponse,
  PackagePropertyResponse,
  IBaseEntityPropertyFundResponse,
  IBaseEntityPropertyPackageResponse,
  IBEPropertyGetResponseCategoryCheck,
  IBndBaseGCloudInfra,
  IBaseEntityPropertySelect,
  IBndBasePropertyMath,
  IBasePropertyProjectionLoan,
  IBasePropertyProjectionYear,
  IPropertyProjectionExtraExpenses,
  IBasePropertyProjectionExtraExpense,
  IPropertyProjectionExtraExpensesYear,
  IBasePropertyProjectionExtraExpenseYear,
  IPropertiesDuplicatedExcelParams,
  IBasePropertyExcelValidation,
} from '@capa/core';
import {
  ModelProperty,
  IModelProperty,
  IModelState,
  IModelCounty,
  IModelPropertyFinancialQuarter,
  ModelPropertyFinancialQuarter,
  IModelPropertyFinancial,
  ModelPropertyFinancial,
  IModelPictures,
  ModelFundProperty,
  ModelPackageProperty,
  ModelFund,
  IModelFundProperty,
  IModelPackageProperty,
  IModelPackage,
  IModelFund,
  ModelPackage,
  IModelPropertyProjection,
  IModelPropertyProjectionLoan,
  IModelPropertyProjectionYear,
} from '../../models';
import { Types } from 'mongoose';
import BigNumber from 'bignumber.js';
@injectable()
export class ImplBndBasePropertyRead implements IBndBasePropertyRead {
  public async getPropertyTypes(): Promise<PropertyType[]> {
    let propertyTypes = EnumPropertyTypes;

    let types = [];
    for (let t in propertyTypes) {
      const elem: PropertyType = {
        // Just to mantain structure to front
        id: new Types.ObjectId().toHexString(),
        label: propertyTypes[t],
      };

      types.push(elem);
    }

    return types;
  }

  public async getPropertyStrategies(): Promise<PropertyStrategy[]> {
    const propertyStr = { ...EnumPropertyStrategies };
    let strategies = [];
    for (let t in propertyStr) {
      const elem: PropertyStrategy = {
        // Just to mantain structure to front
        id: Types.ObjectId().toHexString(),
        label: propertyStr[t],
      };

      strategies.push(elem);
    }

    return strategies;
  }

  public async selectProperties(
    search: string,
    searchType: string,
    bndGCloudInfra: IBndBaseGCloudInfra,
    bndPropertyMath: IBndBasePropertyMath
  ): Promise<IBaseEntityPropertySelect[]> {
    try {
      const fullPath = await bndGCloudInfra.getPublicUrl();
      const searchObject = { isDeleted: false };
      if (search) {
        searchObject['name'] = {
          $regex: search,
          $options: 'i',
        };
      }
      if (!searchType) return [];
      const propertiesData: IModelProperty[] = await ModelProperty.find(
        searchObject
      );
      const validTypes1 = ['package', 'fund', 'llc'];
      const validTypes2 = ['creditline', 'loan'];
      const properties: IBaseEntityPropertySelect[] = [];
      for (let p of propertiesData) {
        let share = 0;
        if (validTypes1.includes(searchType)) {
          share = await bndPropertyMath.calculateSharePackageFund(
            p._id.toHexString()
          );
        } else if (validTypes2.includes(searchType)) {
          share = await bndPropertyMath.calculateShareCreditlineLoan(
            p._id.toHexString()
          );
        } else {
          throw new BaseError(
            `Error obtaining selected properties, searchType not recognized`,
            404,
            'impl.bnd.property-READ.ts | selectProperties'
          );
        }

        const main = p.mainPicture as IModelPictures;
        const propertySelect = {
          id: p._id.toHexString(),
          address: p.address,
          name: p.name,
          shareAvailable: share,
          mainImage: `${fullPath}/${main.url}`,
          type: p.type,
          isComplete: p.isComplete,
        };

        properties.push(propertySelect);
      }

      return properties;
    } catch (error) {
      throw new BaseError(error.message, error.code, error.name, error);
    }
  }

  public async getProperties(
    params: IBEPropertyGetParams,
    bndGCloudInfra: IBndBaseGCloudInfra
  ): Promise<IBEGetPropertiesResponse> {
    try {
      const fullPath = await bndGCloudInfra.getPublicUrl();
      const fundsPropertiesRelation: IModelFundProperty[] =
        await ModelFundProperty.find().select('-__v');
      const packagePropertiesRelation: IModelPackageProperty[] =
        await ModelPackageProperty.find().select('-__v');
      const propertyFindObject = {
        _id: {
          $in: fundsPropertiesRelation
            .map((p) => p.property)
            .concat(packagePropertiesRelation.map((p) => p.property)),
        },
      };
      const simplePropertyFindObject = {
        _id: {
          $nin: fundsPropertiesRelation
            .map((p) => p.property)
            .concat(packagePropertiesRelation.map((p) => p.property)),
        },
      };
      const catRes: IBEPropertyGetResponseCategoryCheck = {
        funds:
          params.category.findIndex(
            (cat) => cat.toLocaleLowerCase() === 'fund'
          ) !== -1 || params.category.length == 0
            ? true
            : false,
        package:
          params.category.findIndex(
            (cat) => cat.toLocaleLowerCase() === 'package'
          ) !== -1 || params.category.length == 0
            ? true
            : false,
        single:
          params.category.findIndex(
            (cat) => cat.toLocaleLowerCase() === 'single'
          ) !== -1 || params.category.length == 0
            ? true
            : false,
      };
      //TODO create a function to check if the params are valid
      if (params.status.length) {
        propertyFindObject['status'] = {
          $in: params.status.map((s) => s.toLocaleLowerCase()),
        };
        simplePropertyFindObject['status'] = {
          $in: params.status.map((s) => s.toLocaleLowerCase()),
        };
      }

      if (params.type.length) {
        propertyFindObject['type'] = {
          $in: params.type.map(
            (type) => EnumPropertyTypes[`${type.toUpperCase()}`]
          ),
        };
        simplePropertyFindObject['type'] = {
          $in: params.type.map(
            (type) => EnumPropertyTypes[`${type.toUpperCase()}`]
          ),
        };
      }

      const properties: IModelProperty[] = await ModelProperty.find(
        propertyFindObject
      ).populate('investors');

      const simpleProperties: IModelProperty[] = catRes.single
        ? await ModelProperty.find(simplePropertyFindObject).populate(
            'investors'
          )
        : [];

      const { fundsResponse, packagesResponse } = await this._getSelectData(
        catRes.funds ? fundsPropertiesRelation : [],
        catRes.package ? packagePropertiesRelation : [],
        properties,
        fullPath
      );
      return {
        funds: catRes.funds ? fundsResponse : null,
        packages: catRes.package ? packagesResponse : null,
        single: catRes.single
          ? simpleProperties.map((property) =>
              this._getPropertyPackageAndSingleObject(property, fullPath)
            )
          : null,
      };
    } catch (error) {
      throw new BaseError(
        error.message,
        error.code,
        'impl.bnd.property-READ.ts | getPropertiesProperties'
      );
    }
  }

  public async getProperty(
    propertyId: string,
    bndGCloudInfra: IBndBaseGCloudInfra
  ): Promise<IBaseEntityProperty> {
    try {
      const fullPath = await bndGCloudInfra.getPublicUrl();
      const property: IModelProperty = await ModelProperty.findOne({
        _id: propertyId,
        isDeleted: false,
      })
        .populate('state', 'name _id code')
        .populate('county', 'name _id fips')
        .populate({
          path: 'projection',
          populate: {
            path: 'projectionYears loans',
          },
        });
      if (!property)
        throw new BaseError('Property not found.', 404, 'execGetProperty');

      return this._getPropertyWithProjectionObject(property, fullPath);
    } catch (error) {
      throw new BaseError(
        `Error obtaining the property ${error.message}`,
        error.code,
        error.name,
        error
      );
    }
  }

  public async getFinancialInfo(
    financialParams: IQueryEntityPropertyFinancial
  ): Promise<IBaseEntityPropertyFinancialResponse> {
    const { propertyId, financialId, quarter, year } = financialParams;
    try {
      const financialData: IModelPropertyFinancial =
        await ModelPropertyFinancial.findOne({
          property: propertyId,
          _id: financialId,
        });
      if (!financialData)
        throw new BaseError(
          'Financial Data not found.',
          404,
          'execGetPropertyFinancial'
        );
      const financialQuarter: IModelPropertyFinancialQuarter =
        await this._getQuarter(financialData._id, quarter, year);
      return this._generateFinancialData(financialData, financialQuarter);
    } catch (error) {
      throw new BaseError(
        `Error obtaining the property financial data ${error.message}`,
        error.code,
        error.name,
        error
      );
    }
  }

  public async checkQuarterDate(
    year: number,
    quarter: number,
    propertyId: string
  ): Promise<boolean> {
    try {
      const financialData = await ModelPropertyFinancial.findOne({
        property: propertyId,
      }).select('_id');
      if (!financialData) return true;
      const quarters = await ModelPropertyFinancialQuarter.find({
        propertyFinancial: financialData._id,
      })
        .select('year quarter')
        .sort({ year: 1, quarter: 1 });
      if (quarters.length === 0) return true;
      const quarterExist = !!quarters.find(
        (q) => q.year === year && q.quarter === quarter
      );
      if (quarterExist) return true;
      const validDate: boolean = this._generateValidDates(
        { year: quarters[0].year, quarter: quarters[0].quarter },
        {
          year: quarters[quarters.length - 1].year,
          quarter: quarters[quarters.length - 1].quarter,
        },
        { year, quarter }
      );
      return validDate;
    } catch (error) {
      throw new BaseError(
        `Error obtaining quarter date data ${error}`,
        500,
        'impl.bnd.property-READ.ts | checkQuarterDate'
      );
    }
  }
  public async getPropertiesForExcel(
    data: IPropertiesDuplicatedExcelParams
  ): Promise<IBasePropertyExcelValidation[]> {
    try {
      const properties: IModelProperty[] = await ModelProperty.find({
        name: { $in: data.names },
        address: { $in: data.address },
        'deletedBy.dateTime': null,
      });
      return properties.map(
        (property: IModelProperty): IBasePropertyExcelValidation => {
          return {
            address: property.address,
            id: property.id,
            name: property.name,
          };
        }
      );
    } catch (error) {
      throw new BaseError(
        `Error obtaining properties for excel ${error}`,
        500,
        'impl.bnd.property-READ.ts | getPropertiesForExcel'
      );
    }
  }

  public async getPropertyIdByAddress(address: string): Promise<string> {
    try {
      const property = await ModelProperty.findOne({
        address,
        isDeleted: false,
      }).select('_id');
      if (property) {
        return property._id.toHexString();
      }
      return null;
    } catch (error) {
      throw new BaseError(
        `Error obtaining property id by address ${error}`,
        500,
        'impl.bnd.property-READ.ts | getPropertyIdByAddress'
      );
    }
  }

  public async getPropertyIdByName(name: string): Promise<string> {
    try {
      const property = await ModelProperty.findOne({
        name,
        'deletedBy.dateTime': null,
      }).select('_id');
      if (property) {
        return property.id;
      }
      return null;
    } catch (error) {
      throw new BaseError(
        `Error obtaining property id by name ${error}`,
        500,
        'impl.bnd.property-READ.ts | getPropertyIdByName'
      );
    }
  }

  private _getPropertyWithProjectionObject(
    property: IModelProperty,
    gcloudPath: string
  ): IBaseEntityProperty {
    const projection: IModelPropertyProjection =
      property.projection as IModelPropertyProjection;
    return {
      id: property.id,
      name: property.name,
      address: property.address,
      type: property.type,
      state: property.state
        ? {
            name: (property.state as IModelState).name,
            code: (property.state as IModelState).code,
            id: (property.state as IModelState).id,
          }
        : null,
      county: property.county
        ? {
            name: (property.county as IModelCounty).name,
            fips: (property.county as IModelCounty).fips,
            id: (property.county as IModelCounty).id,
          }
        : null,
      bedRooms: new BigNumber(property.bedRooms).toNumber(),
      bathRooms: new BigNumber(property.bathRooms).toNumber(),
      size: new BigNumber(property.size).toNumber(),
      parcelSize: new BigNumber(property.parcelSize).toNumber(),
      parcelNumber: new BigNumber(property.parcelNumber).toNumber(),
      strategies: property.strategies,
      propertyDescription: property.propertyDescription,
      pictures: (property.pictures as IModelPictures[]).map(
        (pic: IModelPictures): string => {
          return `${gcloudPath}/${pic.url}`;
        }
      ),
      mainPicture: `${gcloudPath}/${
        (property.mainPicture as IModelPictures).url
      }`,
      status: property.status,
      location: property.location,
      projection: !projection
        ? null
        : {
            property: property.id,
            totalValue: new BigNumber(projection.totalValue).toNumber(),
            purchasePrice: new BigNumber(projection.purchasePrice).toNumber(),
            aquisitionFee: new BigNumber(projection.aquisitionFee).toNumber(),
            dispositionFee: new BigNumber(projection.dispositionFee).toNumber(),
            amountAllocatedToLand: new BigNumber(
              projection.amountAllocatedToLand
            ).toNumber(),
            depreciation: new BigNumber(projection.depreciation).toNumber(),
            commissions: new BigNumber(projection.commissions).toNumber(),
            downPayment: new BigNumber(projection.downPayment).toNumber(),
            capitalGainsTaxPaid: new BigNumber(
              projection.capitalGainsTaxPaid
            ).toNumber(),
            extraExpenses: projection.extraExpenses.map(
              (
                extraExpense: IPropertyProjectionExtraExpenses
              ): IBasePropertyProjectionExtraExpense => {
                return {
                  name: extraExpense.name,
                  years: extraExpense.years.map(
                    (
                      extraExpenseYear: IPropertyProjectionExtraExpensesYear
                    ): IBasePropertyProjectionExtraExpenseYear => {
                      return {
                        amount: new BigNumber(
                          extraExpenseYear.amount
                        ).toNumber(),
                        year: extraExpenseYear.year,
                      };
                    }
                  ),
                };
              }
            ),
            loans: projection.loans.map(
              (
                loan: IModelPropertyProjectionLoan
              ): IBasePropertyProjectionLoan => {
                return {
                  propertyProjection: projection.id,
                  amount: new BigNumber(loan.amount).toNumber(),
                  interest: new BigNumber(loan.interest).toNumber(),
                  termYears: new BigNumber(loan.termYears).toNumber(),
                  paymentsPerYear: new BigNumber(
                    loan.paymentsPerYear
                  ).toNumber(),
                  amortizationYears: new BigNumber(
                    loan.amortizationYears
                  ).toNumber(),
                };
              }
            ),
            projectionYears: projection.projectionYears.map(
              (
                projectionYear: IModelPropertyProjectionYear
              ): IBasePropertyProjectionYear => {
                return {
                  propertyProjection: projection.id,
                  year: new BigNumber(projectionYear.year).toNumber(),
                  units: new BigNumber(projectionYear.units).toNumber(),
                  rent: new BigNumber(projectionYear.rent).toNumber(),
                  vacancy: new BigNumber(projectionYear.vacancy).toNumber(),
                  propertyManagement: new BigNumber(
                    projectionYear.propertyManagement
                  ).toNumber(),
                  assetManagement: new BigNumber(
                    projectionYear.assetManagement
                  ).toNumber(),
                  taxes: new BigNumber(projectionYear.taxes).toNumber(),
                  insurance: new BigNumber(projectionYear.insurance).toNumber(),
                  repairs: new BigNumber(projectionYear.repairs).toNumber(),
                  reserve: new BigNumber(projectionYear.reserve).toNumber(),
                  salesPrice: !projectionYear.salesPrice
                    ? null
                    : new BigNumber(projectionYear.salesPrice).toNumber(),
                };
              }
            ),
          },
    };
  }

  private _generateValidDates(
    low: IBaseQuarterDate,
    high: IBaseQuarterDate,
    current: IBaseQuarterDate
  ): boolean {
    let res: IBaseQuarterDateCheck = { high, low };
    if (high.quarter === 4) {
      res.high.year += 1;
      res.high.quarter = 1;
    } else {
      res.high.quarter += 1;
    }
    if (low.quarter === 1) {
      res.low.year -= 1;
      res.low.quarter = 4;
    } else {
      res.low.quarter -= 1;
    }
    const highValidation = Object.keys(current).every(
      (key) => res.high[key] == current[key]
    );
    const lowValidation = Object.keys(current).every(
      (key) => res.low[key] == current[key]
    );
    return highValidation || lowValidation;
  }

  private async _getQuarter(
    propertyFinancialId: Types.ObjectId,
    quarter: number,
    year: number
  ): Promise<IModelPropertyFinancialQuarter> {
    try {
      if (!quarter || !year) {
        return null;
      }
      const financialQuarter: IModelPropertyFinancialQuarter =
        await ModelPropertyFinancialQuarter.findOne({
          propertyFinancial: propertyFinancialId,
          year: year,
          quarter: quarter,
        });
      return financialQuarter;
    } catch (error) {
      throw new BaseError(
        `Error obtaining quarter data ${error}`,
        500,
        'impl.bnd.property-READ.ts | _getQuarter'
      );
    }
  }

  private _generateFinancialData(
    financialData: IModelPropertyFinancial,
    financialQuarter: IModelPropertyFinancialQuarter
  ): IBaseEntityPropertyFinancialResponse {
    return {
      totalValue: this._gDN(financialData.totalValue),
      initialInvestment: this._gDN(financialData.initialInvestment),
      purchasePrice: this._gDN(financialData.purchasePrice),
      aquisitionFee: this._gDN(financialData.aquisitionFee),
      dispositionFee: this._gDN(financialData.dispositionFee),
      amountAllocatedToLand: this._gDN(financialData.amountAllocatedToLand),
      depreciation: this._gDN(financialData.depreciation),
      commissions: this._gDN(financialData.commissions),
      downPayment: this._gDN(financialData.downPayment),
      capitalGainsTaxPaid: this._gDN(financialData.capitalGainsTaxPaid),
      salesPrice: {
        year1: this._gDN(financialData.salesPrice.year1),
        year2: this._gDN(financialData.salesPrice.year2),
        year3: this._gDN(financialData.salesPrice.year3),
        year4: this._gDN(financialData.salesPrice.year4),
        year5: this._gDN(financialData.salesPrice.year5),
      },
      quarter: this._generateQuarterResponse(financialQuarter),
    };
  }

  private _generateQuarterResponse(
    quarterData: IModelPropertyFinancialQuarter
  ): IBaseEntityPropertyFinancialQuarterResponse {
    if (!quarterData) {
      return null;
    }
    return {
      year: quarterData.year,
      quarter: quarterData.quarter,
      totalUnits: quarterData.totalUnits,
      rents: {
        month1: this._gDN(quarterData.rents.month1),
        month2: this._gDN(quarterData.rents.month2),
        month3: this._gDN(quarterData.rents.month3),
      },
      propertyManagement: this._gDN(quarterData.propertyManagement),
      assetManagement: this._gDN(quarterData.assetManagement),
      repairsMaintenance: this._gDN(quarterData.repairsMaintenance),
      taxes: this._gDN(quarterData.taxes),
      insurance: this._gDN(quarterData.insurance),
      reserve: this._gDN(quarterData.reserve),
      vacancy: this._gDN(quarterData.vacancy),
      operativeExpenses: quarterData.operativeExpenses.map(
        (operativeExpense) => {
          return {
            name: operativeExpense.name,
            total: this._gDN(operativeExpense.total),
          };
        }
      ),
    };
  }

  private _gDN(num: number): number {
    return parseFloat(`${num}`);
  }

  private async _getSelectData(
    fundsPropertiesRelation: IModelFundProperty[],
    packagePropertiesRelation: IModelPackageProperty[],
    propertiesData: IModelProperty[],
    fullPath: string
  ) {
    const funds: IModelFund[] = await ModelFund.find({
      _id: { $in: fundsPropertiesRelation.map((f) => f.fund) },
      active: true,
    });
    const packages: IModelPackage[] = await ModelPackage.find({
      _id: { $in: packagePropertiesRelation.map((p) => p.package) },
      active: true,
    });
    const propertyObject = this._getPropertiesObject(propertiesData);
    const fundsRelObject = this._getFundsRelObject(
      fundsPropertiesRelation,
      propertyObject
    );
    const packagesRelObject = this._getPackagesRelObject(
      packagePropertiesRelation,
      propertyObject
    );
    const fundsResponse: FundPropertyResponse[] = this._generateFundsResponse(
      funds,
      fundsRelObject,
      propertyObject,
      fullPath
    );
    const packagesResponse: PackagePropertyResponse[] =
      this._generatePackagesResponse(
        packages,
        packagesRelObject,
        propertyObject,
        fullPath
      );
    return { fundsResponse, packagesResponse };
  }

  private _getFundsRelObject(
    fundsPropertiesRelation: IModelFundProperty[],
    properties
  ) {
    const response = {};
    for (let i = 0; i < fundsPropertiesRelation.length; i++) {
      if (!response[`${fundsPropertiesRelation[i].fund}`]) {
        response[`${fundsPropertiesRelation[i].fund}`] = [];
      }
      if (properties[`${fundsPropertiesRelation[i].property}`]) {
        response[`${fundsPropertiesRelation[i].fund}`].push(
          fundsPropertiesRelation[i]
        );
      }
    }
    return response;
  }
  //TODO find a validation of the type in pointer objects
  private _getPackagesRelObject(
    packagesPropertyRelation: IModelPackageProperty[],
    properties
  ) {
    const response = {};
    for (let i = 0; i < packagesPropertyRelation.length; i++) {
      if (!response[`${packagesPropertyRelation[i].package}`]) {
        response[`${packagesPropertyRelation[i].package}`] = [];
      }
      if (properties[`${packagesPropertyRelation[i].property}`]) {
        response[`${packagesPropertyRelation[i].package}`].push(
          packagesPropertyRelation[i]
        );
      }
    }
    return response;
  }

  private _getPropertiesObject(properties: IModelProperty[]) {
    const response = {};
    for (let i = 0; i < properties.length; i++) {
      response[`${properties[i]._id}`] = properties[i];
    }
    return response;
  }

  private _generateFundsResponse(
    funds: IModelFund[],
    relation,
    properties,
    fullPath: string
  ): FundPropertyResponse[] {
    const response: FundPropertyResponse[] = funds.map((fund) => {
      return {
        id: fund._id.toHexString(),
        name: fund.name,
        properties: relation[`${fund._id}`].map((rel) => {
          return this._getPropertyFundObject(
            properties[`${rel.property}`],
            this._gDN(rel.share),
            fullPath
          );
        }),
      };
    });
    return response;
  }

  private _generatePackagesResponse(
    packages: IModelPackage[],
    relation,
    properties,
    fullPath: string
  ): PackagePropertyResponse[] {
    const response: PackagePropertyResponse[] = packages.map((p) => {
      return {
        id: p._id.toHexString(),
        name: p.name,
        properties: relation[`${p._id}`].map((rel) => {
          return this._getPropertyPackageAndSingleObject(
            properties[`${rel.property}`],
            fullPath
          );
        }),
      };
    });
    return response;
  }
  private _getPropertyFundObject(
    property: IModelProperty,
    fundShare: number,
    fullPath: string
  ): IBaseEntityPropertyFundResponse {
    return {
      id: property._id.toHexString(),
      name: property.name,
      address: property.address,
      type: property.type,
      location: property.location,
      estimatedValue: 5000,
      fundsShare: fundShare,
      myShare: 5000,
      fundEquity: 5000,
      myEquity: 5000,
      projectedIRR: 5000,
      projectEquityMultiple: 5000,
      state: property.status,
      pictures: (property.pictures as IModelPictures[]).map((picture) => {
        return `${fullPath}/${picture.url}`;
      }),
      mainPicture: `${fullPath}/${
        (property.mainPicture as IModelPictures).url
      }`,
    };
  }
  private _getPropertyPackageAndSingleObject(
    property: IModelProperty,
    fullPath: string
  ): IBaseEntityPropertyPackageResponse {
    return {
      id: property._id.toHexString(),
      name: property.name,
      address: property.address,
      type: property.type,
      location: property.location,
      estimatedValue: 5000,
      myShare: 5000,
      projectedIRR: 5000,
      loanAmmount: 150,
      state: property.status,
      estimateEquity: 1264,
      pictures: (property.pictures as IModelPictures[]).map((picture) => {
        return `${fullPath}/${picture.url}`;
      }),
      mainPicture: `${fullPath}/${
        (property.mainPicture as IModelPictures).url
      }`,
    };
  }
}
