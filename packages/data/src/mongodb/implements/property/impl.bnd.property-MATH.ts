import { injectable } from 'inversify';
import {
  BaseError,
  IBaseEntityPropertyPreview,
  IBndBasePropertyMath,
  IPropertyProjection,
  IBasePPOperativeInfo,
  IPropertyOperatingYear,
  IBaseDebtOverview,
  IBaseScheduleOfProspective,
  IBaseExitStrategy,
} from '@capa/core';
import BigNumber from 'bignumber.js';
import { DateTime } from 'luxon';
import { Types } from 'mongoose';
import * as FJS from '@formulajs/formulajs';
import {
  getAmortizationScheduleRawData,
  getPropertyInfoObject,
  IModelLoan,
  ModelLoanProperty,
  IModelLoanProperty,
  IExtraPaymentParam,
  IPaymentProperty,
  ILoansByProperty,
  IModelCreditlineProperty,
  ModelCreditlineProperty,
  ModelFundProperty,
  ModelLlcProperty,
  ModelPackageProperty,
  ModelProperty,
  IPropertyPreviewGenerateParams,
  IRAWPropertyPreviewInput,
  IPropertyPreviewProjection,
  IRAWPropertyOperatingYear,
  IPPOperativeResponse,
  IPPMathOperativeYear,
  ISOPCFParams,
  ISOPCSYear,
  eoyGralInfoParams,
  eoyGralInfoResponse,
  ISOPCSResponse,
  IExitStrategyRAW,
  IExitStrategyParams,
  IPropertyPreviewRAW,
  IPropertyPreviewOBjectParams,
  ILoanByPropert,
  IGralLoanByPropertyParams,
} from '../../';
import { getSchedulePayment } from '../loan';
import { IModelCounty, IModelProperty, IModelState } from '../../models';
import {
  IRAWOperatingExtraExpenseParam,
  ISOPCSLoanResponse,
} from './ientity.property-interfaces';

export const getPropertyLoanData = async (
  propertyId: Types.ObjectId
): Promise<ILoansByProperty> => {
  const loansData: IModelLoanProperty[] = await ModelLoanProperty.find({
    property: propertyId,
  }).populate('loan');
  const propertyInfoObject = await getPropertyInfoObject([propertyId]);
  const response: ILoansByProperty = {
    totalAmount: new BigNumber(0),
    loans: [],
  };
  for (const loanData of loansData) {
    const loan: IModelLoan = loanData.loan as IModelLoan;
    const data = getAmortizationScheduleRawData(
      {
        interest: new BigNumber(loan.interest),
        amortizationYears: new BigNumber(loan.interest),
        loanAmount: new BigNumber(loan.amount),
        paymentsPerYear: new BigNumber(loan.paymentsPerYear),
        beginningDate: DateTime.fromISO(loan.beginningDate),
        extraPayments: loan.extraPayments
          ? loan.extraPayments.map((extraPayment): IExtraPaymentParam => {
              return {
                amount: new BigNumber(extraPayment.amount),

                name: extraPayment.name,
                paymentNumbers: extraPayment.paymentNumbers,
              };
            })
          : [],
        propertiesShare: [
          {
            _id: loanData.property as Types.ObjectId,
            payments: loanData.payments.map((payment): IPaymentProperty => {
              return {
                finalPayment: new BigNumber(payment.finalPayment),
                firstPayment: new BigNumber(payment.firstPayment),
                share: new BigNumber(payment.share),
              };
            }),
          },
        ],
        estimatedNumberOfPayments: new BigNumber(
          loan.paymentsPerYear
        ).multipliedBy(loan.amortizationYears),
      },
      { ...propertyInfoObject, ...{} }
    );
    const propertyLoanInfo =
      data.generalPropertyInfo.length > 0 ? data.generalPropertyInfo[0] : null;
    if (!propertyLoanInfo)
      throw new BaseError(
        'The username or password is not correct.',
        401,
        'LoginError'
      );
    response.totalAmount = response.totalAmount.plus(
      propertyLoanInfo.loanAmount
    );
    response.loans.push({
      amount: propertyLoanInfo.loanAmount,
      interest: new BigNumber(loan.interest),
      schedulePayment: data.schedulePayment,
      amortizationYears: new BigNumber(loan.amortizationYears),
      loanTerm: new BigNumber(loan.termYears),
      paymentsPerYear: new BigNumber(loan.paymentsPerYear),
      ltc: null,
    });
  }
  return response;
};

const vN /* Validate Number */ = (number: BigNumber | number): BigNumber =>
  number ? new BigNumber(number) : new BigNumber(0);

export const getOperatingData = (
  operativeInfo: IRAWPropertyOperatingYear[]
): IPPOperativeResponse => {
  return operativeInfo
    .map((yearInfo: IRAWPropertyOperatingYear): IPPMathOperativeYear => {
      const extraExpensesAmount: BigNumber = yearInfo.extraExpenses
        .map((extraExpense: IRAWOperatingExtraExpenseParam): BigNumber => {
          return extraExpense.amount;
        })
        .reduce((a, b) => a.plus(b), new BigNumber(0));
      const scheduledGrossIncomeYear: IPropertyPreviewProjection = {
        year: yearInfo.year,
        value: yearInfo.rent,
      };
      const totalOperatingExpensesYear: IPropertyPreviewProjection = {
        year: yearInfo.year,
        value: yearInfo.vacancy
          .plus(yearInfo.propertyManagement)
          .plus(yearInfo.assetManagement)
          .plus(yearInfo.taxes)
          .plus(yearInfo.insurance)
          .plus(yearInfo.repairs)
          .plus(yearInfo.reserve)
          .plus(extraExpensesAmount),
      };
      const netOperatingIncomeYear: IPropertyPreviewProjection = {
        year: yearInfo.year,
        value: scheduledGrossIncomeYear.value.minus(
          totalOperatingExpensesYear.value
        ),
      };
      return {
        scheduledGrossIncomeYear,
        totalOperatingExpensesYear,
        netOperatingIncomeYear,
      };
    })
    .reduce(
      (previus, current) => {
        previus.scheduleGrossIncome.push(current.scheduledGrossIncomeYear);
        previus.totalOperatingExpenses.push(current.totalOperatingExpensesYear);
        previus.netOperatingIncome.push(current.netOperatingIncomeYear);
        return {
          scheduleGrossIncome: previus.scheduleGrossIncome,
          totalOperatingExpenses: previus.totalOperatingExpenses,
          netOperatingIncome: previus.netOperatingIncome,
        };
      },
      {
        scheduleGrossIncome: [],
        totalOperatingExpenses: [],
        netOperatingIncome: [],
      }
    );
};

export const getPV = (
  interestRate: BigNumber,
  paymentsPerYear: BigNumber,
  amortizationYears: BigNumber,
  shedulePayment: BigNumber, //Annual Debt Service,
  adjust: BigNumber
): BigNumber => {
  const rate: BigNumber = interestRate.dividedBy(paymentsPerYear);
  const periods: BigNumber = amortizationYears
    .multipliedBy(paymentsPerYear)
    .minus(adjust);
  const payment: BigNumber = shedulePayment
    .multipliedBy(-1)
    .dividedBy(paymentsPerYear);
  return new BigNumber(FJS.PV(rate, periods, payment));
};

const getCapRate = (
  netOperatingIncome: IPropertyPreviewProjection[],
  purchasePrice: BigNumber
): IPropertyPreviewProjection[] => {
  return netOperatingIncome.map(
    (
      operatingIncomeYear: IPropertyPreviewProjection
    ): IPropertyPreviewProjection => {
      return {
        year: operatingIncomeYear.year,
        value: operatingIncomeYear.value.dividedBy(purchasePrice),
      };
    }
  );
};

const getEOYGralInfo = (loanParams: eoyGralInfoParams): eoyGralInfoResponse => {
  const interest: BigNumber = loanParams.interest.dividedBy(100);
  const eoyPrincipalBalance: BigNumber = getPV(
    interest,
    loanParams.paymentsPerYear,
    loanParams.amortizationYears,
    loanParams.schedulePayment,
    loanParams.paymentsPerYear.multipliedBy(loanParams.year)
  );
  let annualEquityBuildUp: BigNumber;
  if (loanParams.year >= 3) {
    annualEquityBuildUp = getPV(
      interest,
      loanParams.paymentsPerYear,
      loanParams.amortizationYears,
      loanParams.schedulePayment,
      loanParams.paymentsPerYear.multipliedBy(
        new BigNumber(loanParams.year).minus(1)
      )
    )
      .multipliedBy(-1)
      .plus(
        getPV(
          interest,
          loanParams.paymentsPerYear,
          loanParams.amortizationYears,
          loanParams.schedulePayment,
          loanParams.paymentsPerYear.multipliedBy(loanParams.year)
        )
      );
  } else {
    annualEquityBuildUp = getPV(
      interest,
      loanParams.paymentsPerYear,
      loanParams.amortizationYears,
      loanParams.schedulePayment,
      loanParams.paymentsPerYear
    ).plus(loanParams.amount);
  }

  return {
    eoyPrincipalBalance,
    annualEquityBuildUp,
  };
};

const getLoansGralInfo = (
  loansInfo: IGralLoanByPropertyParams[]
): ILoansByProperty => {
  const loans: ILoanByPropert[] = [];
  for (const loan of loansInfo) {
    const scheduledPayment: BigNumber = getSchedulePayment(
      loan.interest.dividedBy(100),
      loan.paymentsPerYear,
      loan.amortizationYears,
      loan.amount
    ).multipliedBy(12);
    loans.push({
      amortizationYears: loan.amortizationYears,
      amount: loan.amount,
      interest: loan.interest,
      loanTerm: loan.termYears,
      paymentsPerYear: loan.paymentsPerYear,
      schedulePayment: scheduledPayment,
      ltc: loan.ltc,
    });
  }
  return {
    loans,
    totalAmount: loansInfo.reduce((a, b) => a.plus(b.amount), new BigNumber(0)),
  };
};

const getScheduleOfProspectiveCashFlow = (
  cashflowParams: ISOPCFParams
): ISOPCSResponse => {
  const response: ISOPCSResponse = cashflowParams.loans.reduce(
    (previus, currentLoanInfo) => {
      const initialEquity: BigNumber = cashflowParams.purchasePrice.minus(
        currentLoanInfo.amount
      );

      const { debtOverview, annualCashFlowTotal, eoyPrincipalBalanceTotal } =
        previus;

      const loanProspective = cashflowParams.netOperatingIncome.map(
        (netOperatingInfo): ISOPCSYear => {
          const annualDebtService: BigNumber = currentLoanInfo.schedulePayment;
          const annualCashFlow: BigNumber =
            netOperatingInfo.value.plus(annualDebtService);
          const currentAnnualCashFlow = annualCashFlowTotal.find(
            (e) => e.year == netOperatingInfo.year
          );
          currentAnnualCashFlow.value =
            currentAnnualCashFlow.value.plus(annualCashFlow);

          const annualCashOnCashReturn: BigNumber = annualCashFlow.dividedBy(
            initialEquity.dividedBy(netOperatingInfo.year == 1 ? 2 : 1)
          );
          const { eoyPrincipalBalance, annualEquityBuildUp } = getEOYGralInfo({
            interest: currentLoanInfo.interest,
            paymentsPerYear: currentLoanInfo.paymentsPerYear,
            amortizationYears: currentLoanInfo.amortizationYears,
            schedulePayment: currentLoanInfo.schedulePayment,
            year: netOperatingInfo.year,
            amount: currentLoanInfo.amount,
          });
          const currentEOYPrincipal = eoyPrincipalBalanceTotal.find(
            (e) => e.year == netOperatingInfo.year
          );
          currentEOYPrincipal.value =
            currentEOYPrincipal.value.plus(eoyPrincipalBalance);
          const annualEquityReturnOnEquityBuildIp: BigNumber =
            annualEquityBuildUp.dividedBy(initialEquity);
          const totalAnnualReturn: BigNumber =
            annualEquityReturnOnEquityBuildIp.plus(annualCashOnCashReturn);
          return {
            year: netOperatingInfo.year,
            annualDebtService,
            annualCashFlow,
            annualCashOnCashReturn,
            eoyPrincipalBalance,
            annualEquityBuildUp,
            annualEquityReturnOnEquityBuildIp,
            totalAnnualReturn,
          };
        }
      );
      debtOverview.push({
        initialEquity: initialEquity,
        interest: currentLoanInfo.interest,
        paymentsPerYear: currentLoanInfo.paymentsPerYear,
        amortizationYears: currentLoanInfo.amortizationYears,
        termYears: currentLoanInfo.loanTerm,
        schedulePayment: currentLoanInfo.schedulePayment,
        ltc: currentLoanInfo.ltc,
        amount: currentLoanInfo.amount,
        scheduleOfProspectives: loanProspective,
      });
      return {
        debtOverview,
        annualCashFlowTotal,
        eoyPrincipalBalanceTotal,
      };
    },
    {
      debtOverview: [],
      annualCashFlowTotal: cashflowParams.netOperatingIncome.map((netYear) => {
        return {
          year: netYear.year,
          value: new BigNumber(0),
        };
      }),
      eoyPrincipalBalanceTotal: cashflowParams.netOperatingIncome.map(
        (netYear) => {
          return {
            year: netYear.year,
            value: new BigNumber(0),
          };
        }
      ),
    }
  );
  return response;
};

export const getExitStrategyInfo = (
  exitStrategyParams: IExitStrategyParams
): IExitStrategyRAW[] => {
  const {
    salesPrice,
    dispositionFee,
    commissions,
    aquisitionFee,
    amountAllocatedToLand,
    depreciation,
    eoyPrincipalBalanceTotal,
    annualCashFlowTotal,
    downPayment,
    purchasePrice,
    capitalGainsTaxPaid,
  } = exitStrategyParams;
  const response: IExitStrategyRAW[] = [];
  const totalCashInvested: BigNumber = aquisitionFee.plus(downPayment);
  let oldCashFlowReceived: BigNumber = new BigNumber(0); //the initialization is important
  for (const salePrice of salesPrice) {
    //Cost of sale
    const currentCostOfSale: BigNumber = salePrice.value.multipliedBy(
      dispositionFee.plus(commissions)
    );
    //Property's Tax Basis
    const currentPropertyTaxBasis: BigNumber = purchasePrice.minus(
      purchasePrice
        .minus(amountAllocatedToLand)
        .dividedBy(depreciation)
        .multipliedBy(salePrice.year)
    );

    //Total Amount Subject to Capital Gains Tax
    const currentAmountSubjecttoCapitalGainsTax: BigNumber = salePrice.value
      .minus(currentCostOfSale)
      .minus(currentPropertyTaxBasis);

    //Capital Gains Tax Paid
    const currentCapitalGainsTaxPaid: BigNumber =
      currentAmountSubjecttoCapitalGainsTax.multipliedBy(capitalGainsTaxPaid);

    // Debt Principal Balance
    const currentDebtPrincipalBalance: BigNumber = eoyPrincipalBalanceTotal
      .find((e) => e.year == salePrice.year)
      .value.multipliedBy(-1);

    //Cash Received From Sale (Net of Taxes)
    const currentCashReceivedFromSale: BigNumber = salePrice.value
      .minus(currentCostOfSale)
      .minus(currentCapitalGainsTaxPaid)
      .minus(currentDebtPrincipalBalance);

    //Total Cash Flow Received
    const currentCashFlowReceived: BigNumber = annualCashFlowTotal
      .filter((e) =>
        salePrice.year <= 3 ? e.year <= 3 : e.year == salePrice.year
      )
      .reduce((previus, current) => {
        return new BigNumber(previus).plus(current.value);
      }, new BigNumber(0))
      .plus(oldCashFlowReceived);

    oldCashFlowReceived = currentCashFlowReceived;

    //Total Return
    const currentTotalReturn: BigNumber = currentCashFlowReceived.plus(
      currentCashReceivedFromSale
    );

    //Total Annual ROI (Net Capital Gains Tax)
    const currentTotalAnnualROI: BigNumber = currentTotalReturn
      .minus(totalCashInvested)
      .dividedBy(totalCashInvested)
      .dividedBy(salePrice.year);

    response.push({
      year: salePrice.year,
      salesPrice: salePrice.value,
      costOfSale: currentCostOfSale,
      propertyTaxBasis: currentPropertyTaxBasis,
      amountSubjecttoCapitalGainsTax: currentAmountSubjecttoCapitalGainsTax,
      capitalGainsTaxPaid: currentCapitalGainsTaxPaid,
      debtPrincipalBalance: currentDebtPrincipalBalance,
      cashReceivedFromSale: currentCashReceivedFromSale,
      cashFlowReceived: currentCashFlowReceived,
      totalReturn: currentTotalReturn,
      totalAnnualROI: currentTotalAnnualROI,
    });
  }

  return response;
};
export const getPropertyPreviewRAW = (
  params: IPropertyPreviewGenerateParams
): IPropertyPreviewRAW => {
  const {
    purchasePrice,
    commissions,
    dispositionFee,
    operativeInfo,
    amountAllocatedToLand,
    aquisitionFee,
    depreciation,
    downPayment,
    capitalGainsTaxPaid,
  } = params.data;
  const { scheduleGrossIncome, totalOperatingExpenses, netOperatingIncome } =
    getOperatingData(operativeInfo);
  const capRate: IPropertyPreviewProjection[] = getCapRate(
    netOperatingIncome,
    purchasePrice
  );
  const { debtOverview, annualCashFlowTotal, eoyPrincipalBalanceTotal } =
    getScheduleOfProspectiveCashFlow({
      loans: params.loanData.loans,
      netOperatingIncome,
      purchasePrice,
    });

  const exitStrategy: IExitStrategyRAW[] = getExitStrategyInfo({
    salesPrice: params.data.operativeInfo
      .filter((e) => e.year >= 3)
      .map((operativeYear) => {
        return {
          year: operativeYear.year,
          value: operativeYear.salesPrice,
        };
      }),
    commissions: commissions.dividedBy(100),
    dispositionFee: dispositionFee.dividedBy(100),
    amountAllocatedToLand,
    aquisitionFee,
    depreciation,
    annualCashFlowTotal: annualCashFlowTotal,
    eoyPrincipalBalanceTotal: eoyPrincipalBalanceTotal,
    downPayment: downPayment,
    purchasePrice,
    capitalGainsTaxPaid: capitalGainsTaxPaid,
  });

  return {
    scheduleGrossIncome,
    totalOperatingExpenses,
    netOperatingIncome,
    capRate,
    debtOverview,
    annualCashFlowTotal,
    eoyPrincipalBalanceTotal,
    exitStrategy,
  };
};

const validatePropertyPreviewRawParams = (
  params: IPropertyPreviewGenerateParams
): void => {
  const { data, loanData } = params;
  /*First validation, purchase price cannot be equal to loan amount total */
  if (loanData.totalAmount.isEqualTo(data.purchasePrice)) {
    throw new BaseError(
      `Purchase price cannot be equal to loan amount`,
      400,
      'BaseUseCaseProperty | validatePropertyPreviewRawParams'
    );
  }
  /*Second validation, each operative year has to have unique year */
  const validationObject = {};
  for (const operativeYear of data.operativeInfo) {
    if (validationObject[`${operativeYear.year}`]) {
      throw new BaseError(
        `The year ${operativeYear.year} is duplicated`,
        400,
        'BaseUseCaseProperty | validatePropertyPreviewRawParams'
      );
    }
    validationObject[`${operativeYear.year}`] = true;
  }
};

@injectable()
export class ImplBndBasePropertyMath implements IBndBasePropertyMath {
  constructor() {}

  public async getPropertyPreview(
    data: IPropertyProjection,
    idProperty: string
  ): Promise<IBaseEntityPropertyPreview> {
    try {
      const propertyGralInfo: IModelProperty = await ModelProperty.findOne({
        _id: idProperty,
      })
        .populate('state', 'name code')
        .populate('county', 'name fips');
      const loans = getLoansGralInfo(
        data.loans.map((loan): IGralLoanByPropertyParams => {
          return {
            amortizationYears: new BigNumber(loan.amortizationYears),
            amount: new BigNumber(loan.amount),
            interest: new BigNumber(loan.interest),
            paymentsPerYear: new BigNumber(
              loan.paymentsPerYear ? loan.paymentsPerYear : 12
            ),
            termYears: new BigNumber(loan.termYears),
            ltc: loan.ltc ? loan.ltc : null,
          };
        })
      );
      const propertyPreviewRawParams: IRAWPropertyPreviewInput =
        this._getPropertyPreviewParams(data);
      validatePropertyPreviewRawParams({
        data: propertyPreviewRawParams,
        loanData: loans,
      });
      const responseRAW = getPropertyPreviewRAW({
        data: propertyPreviewRawParams,
        loanData: loans,
      });
      return this._getPropertyPreviewResponseObject({
        financialInfo: data,
        previewInfo: responseRAW,
        propertyInfo: propertyGralInfo,
      });
    } catch (error) {
      throw new BaseError(
        `An error occurred while trying to get the property preview.${error.message}`,
        error.code,
        'ImplBndBasePropertyMath | getPropertyPreview',
        error
      );
    }
  }

  private _getPropertyPreviewResponseObject(
    params: IPropertyPreviewOBjectParams
  ): IBaseEntityPropertyPreview {
    const { financialInfo, previewInfo, propertyInfo } = params;

    const response: IBaseEntityPropertyPreview = {
      name: propertyInfo.name,
      address: propertyInfo.address,
      type: propertyInfo.type,
      state: propertyInfo.state
        ? {
            name: (propertyInfo.state as IModelState).name,
            code: (propertyInfo.state as IModelState).code,
            id: (propertyInfo.state as IModelState).id,
          }
        : null,
      county: propertyInfo.county
        ? {
            name: (propertyInfo.county as IModelCounty).name,
            fips: (propertyInfo.county as IModelCounty).fips,
            id: (propertyInfo.county as IModelCounty).id,
          }
        : null,
      bedRooms: new BigNumber(propertyInfo.bedRooms).toNumber(),
      bathRooms: new BigNumber(propertyInfo.bathRooms).toNumber(),
      size: new BigNumber(propertyInfo.size).toNumber(),
      parcelSize: new BigNumber(propertyInfo.parcelSize).toNumber(),
      parcelNumber: new BigNumber(propertyInfo.parcelNumber).toNumber(),
      strategies: propertyInfo.strategies,
      propertyDescription: propertyInfo.propertyDescription,
      aquisitionFee: financialInfo.aquisitionFee,
      commissions: financialInfo.commissions,
      dispositionFee: financialInfo.dispositionFee,
      depreciation: financialInfo.depreciation,
      amountAllocatedToLand: financialInfo.amountAllocatedToLand,
      downPayment: financialInfo.downPayment,
      aquisitionPrice: financialInfo.purchasePrice,
      extraExpenses: financialInfo.extraExpenses,
      capitalGainsTaxPaid: financialInfo.capitalGainsTaxPaid,
      operativeInfo: this._getOperativeInfoResponse(
        financialInfo.operativeInfo,
        previewInfo,
        financialInfo.purchasePrice
      ),
      debtOverviews: this._getDebtOverviewsResponse(previewInfo.debtOverview),
      exitStrategies: this._getExitStrategies(previewInfo.exitStrategy),
    };
    return response;
  }

  private _getExitStrategies(
    exitStrategyYears: IExitStrategyRAW[]
  ): IBaseExitStrategy[] {
    return exitStrategyYears.map(
      (exitStrategyYear: IExitStrategyRAW): IBaseExitStrategy => {
        return {
          year: exitStrategyYear.year,
          salesPrice: exitStrategyYear.salesPrice.toNumber(),
          costOfSale: exitStrategyYear.costOfSale.toNumber(),
          propertyTaxBasis: exitStrategyYear.propertyTaxBasis.toNumber(),
          amountSubjecttoCapitalGainsTax:
            exitStrategyYear.amountSubjecttoCapitalGainsTax.toNumber(),
          capitalGainsTaxPaid: exitStrategyYear.capitalGainsTaxPaid.toNumber(),
          debtPrincipalBalance:
            exitStrategyYear.debtPrincipalBalance.toNumber(),
          cashReceivedFromSale:
            exitStrategyYear.cashReceivedFromSale.toNumber(),
          cashFlowReceived: exitStrategyYear.cashFlowReceived.toNumber(),
          totalReturn: exitStrategyYear.totalReturn.toNumber(),
          totalAnnualROI: exitStrategyYear.totalAnnualROI
            .multipliedBy(100)
            .toNumber(),
        };
      }
    );
  }

  private _getDebtOverviewsResponse(
    debtOverviews: ISOPCSLoanResponse[]
  ): IBaseDebtOverview[] {
    return debtOverviews.map(
      (debtOverview: ISOPCSLoanResponse): IBaseDebtOverview => {
        const response: IBaseDebtOverview = {
          amortizationYears: new BigNumber(
            debtOverview.amortizationYears
          ).toNumber(),
          amount: new BigNumber(debtOverview.amount).toNumber(),
          initialEquity: new BigNumber(debtOverview.initialEquity).toNumber(),
          interestRate: new BigNumber(debtOverview.interest).toNumber(),
          loanTerm: new BigNumber(debtOverview.termYears).toNumber(),
          ltc: debtOverview.ltc,
          schedulePayment: new BigNumber(
            debtOverview.schedulePayment
          ).toNumber(),
          scheduleOfProspectives: debtOverview.scheduleOfProspectives.map(
            (scheduleOfProspective): IBaseScheduleOfProspective => {
              return {
                year: scheduleOfProspective.year,
                annualDebtService:
                  scheduleOfProspective.annualDebtService.toNumber(),
                annualCashFlow: scheduleOfProspective.annualCashFlow.toNumber(),
                annualCashOnCashReturn:
                  scheduleOfProspective.annualCashOnCashReturn
                    .multipliedBy(100)
                    .toNumber(),
                eoyPrincipalBalance:
                  scheduleOfProspective.eoyPrincipalBalance.toNumber(),
                annualEquityBuildUp:
                  scheduleOfProspective.annualEquityBuildUp.toNumber(),
                annualEquityReturnOnEquityBuildIp:
                  scheduleOfProspective.annualEquityReturnOnEquityBuildIp
                    .multipliedBy(100)
                    .toNumber(),
                totalAnnualReturn: scheduleOfProspective.totalAnnualReturn
                  .multipliedBy(100)
                  .toNumber(),
              };
            }
          ),
        };
        return response;
      }
    );
  }

  private _getOperativeInfoResponse(
    operativeInfo: IPropertyOperatingYear[],
    previewInfo: IPropertyPreviewRAW,
    purchasePrice: number
  ): IBasePPOperativeInfo[] {
    const informationObject = {};
    for (let operativeYear of operativeInfo) {
      informationObject[`${operativeYear.year}`] = {
        year: operativeYear.year,
        rentRoll: operativeYear.rent,
        scheduleGrossIncome: operativeYear.rent,
        vacancy: operativeYear.vacancy,
        propertyManagement: operativeYear.propertyManagement,
        assetManagement: operativeYear.assetManagement,
        taxes: operativeYear.taxes,
        insurance: operativeYear.insurance,
        repairs: operativeYear.repairs,
        reserve: operativeYear.reserve,
        units: operativeYear.units,
        purchasePriceAndImprovements: purchasePrice,
        totalOperatingExpenses: null,
        netOperatingIncome: null,
        capRate: null,
      };
    }
    for (const totalYearOperatingExpenses of previewInfo.totalOperatingExpenses) {
      informationObject[
        `${totalYearOperatingExpenses.year}`
      ].totalOperatingExpenses = totalYearOperatingExpenses.value.toNumber();
    }
    for (const yearNetOperatingIncome of previewInfo.netOperatingIncome) {
      informationObject[`${yearNetOperatingIncome.year}`].netOperatingIncome =
        yearNetOperatingIncome.value.toNumber();
    }
    for (const yearCapRate of previewInfo.capRate) {
      informationObject[`${yearCapRate.year}`].capRate = yearCapRate.value
        .multipliedBy(100)
        .toNumber();
    }
    const response: IBasePPOperativeInfo[] = [];
    for (const key in informationObject) {
      response.push(informationObject[`${key}`]);
    }
    return response;
  }

  private _getPropertyPreviewParams(
    data: IPropertyProjection
  ): IRAWPropertyPreviewInput {
    const extraExpensesObject = {};
    if (data.extraExpenses)
      for (const extraExpense of data.extraExpenses) {
        extraExpense.years.forEach((e) => {
          if (e) {
            const expenseElement: IRAWOperatingExtraExpenseParam = {
              name: extraExpense.name,
              amount: new BigNumber(e.amount),
            };
            if (!extraExpensesObject[`${e.year}`]) {
              extraExpensesObject[`${e.year}`] = [expenseElement];
            } else {
              extraExpensesObject[`${e.year}`].push(expenseElement);
            }
          }
        });
      }

    const response: IRAWPropertyPreviewInput = {
      operativeInfo: [],
      aquisitionFee: vN(data.aquisitionFee),
      dispositionFee: vN(data.dispositionFee),
      amountAllocatedToLand: vN(data.amountAllocatedToLand),
      depreciation: vN(data.depreciation),
      commissions: vN(data.commissions),
      downPayment: vN(data.downPayment),
      purchasePrice: vN(data.purchasePrice),
      capitalGainsTaxPaid: vN(data.capitalGainsTaxPaid).dividedBy(100),
    };
    for (let operativeYear of data.operativeInfo) {
      response.operativeInfo.push({
        year: operativeYear.year,
        rent: vN(operativeYear.rent),
        vacancy: vN(operativeYear.vacancy),
        propertyManagement: vN(operativeYear.propertyManagement),
        assetManagement: vN(operativeYear.assetManagement),
        taxes: vN(operativeYear.taxes),
        insurance: vN(operativeYear.insurance),
        repairs: vN(operativeYear.repairs),
        reserve: vN(operativeYear.reserve),
        units: vN(operativeYear.units),
        salesPrice: vN(operativeYear.salesPrice),
        extraExpenses: extraExpensesObject[`${operativeYear.year}`]
          ? extraExpensesObject[`${operativeYear.year}`]
          : [],
      });
    }
    return response;
  }

  public async calculateShareCreditlineLoan(
    propertyId: string
  ): Promise<number> {
    let totalShare = new BigNumber(0);

    const propertyCreditline: IModelCreditlineProperty[] =
      await ModelCreditlineProperty.find({
        property: propertyId,
      });
    const propertyLoan: IModelLoanProperty[] = await ModelLoanProperty.find({
      property: propertyId,
    });

    for (let pCreditline of propertyCreditline) {
      totalShare = totalShare.plus(pCreditline.share);
    }
    for (let pLoan of propertyLoan) {
      // TODO Validate this calculation related to paymennts
      for (let payment of pLoan.payments) {
        totalShare = totalShare.plus(payment.share);
      }
    }

    let shareAvailable = new BigNumber(100);
    shareAvailable = shareAvailable.minus(totalShare);

    return parseFloat(`${shareAvailable}`);
  }

  public async calculateSharePackageFund(propertyId: string): Promise<number> {
    let totalShare = new BigNumber(0);

    const propertyPackage = await ModelPackageProperty.find({
      property: propertyId,
    });
    const propertyFund = await ModelFundProperty.find({
      property: propertyId,
    });
    const propertyEntity = await ModelLlcProperty.find({
      property: propertyId,
    });

    for (let pPackage of propertyPackage) {
      totalShare = totalShare.plus(pPackage.share);
    }
    for (let pFund of propertyFund) {
      totalShare = totalShare.plus(pFund.share);
    }
    for (let pEntity of propertyEntity) {
      totalShare = totalShare.plus(pEntity.share);
    }

    let shareAvailable = new BigNumber(100);
    shareAvailable = shareAvailable.minus(totalShare);

    return parseFloat(`${shareAvailable}`);
  }
}
