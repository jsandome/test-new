import { injectable } from 'inversify';
import { Types, startSession } from 'mongoose';
import { DateTime } from 'luxon';
import {
  IBndBasePropertyWrite,
  BaseError,
  IBaseEntityPropertyResponse,
  IBaseEntityPropertyFinancialCreateResponse,
  IBaseEntityProperty,
  IBaseEntityPropertyFinancialCreate,
  IBndBaseGCloudInfra,
  IPropertyProjectionParams,
  IPropertyCreateProjectionResponse,
  IExcelPropertiesAndProjectionsSaveResponse,
  IPropertySaveExcelParams,
  IBaseEntityStates,
  IBaseEntityCounties,
  IPropertyOperatingYear,
  ILoansByPropertyParams,
  IPropertyProfitsAndLossExcelInfo,
  IExcelPropertiesProfitsAndLossResponse,
} from '@capa/core';
import {
  ModelProperty,
  IModelProperty,
  IModelPropertyFinancial,
  ModelPropertyFinancial,
  ModelPropertyFinancialQuarter,
  IModelPropertyFinancialQuarter,
  ModelFundProperty,
  ModelCreditlineProperty,
  IModelPictures,
  IModelPropertyProjection,
  ModelPropertyProjection,
  IModelPropertyProjectionYear,
  ModelPropertyProjectionYear,
  IModelPropertyProjectionLoan,
  ModelPropertyProjectionLoan,
  ModelPackageProperty,
} from '../../models';

@injectable()
export class ImplBndBasePropertyWrite implements IBndBasePropertyWrite {
  public async createNewProperty(
    property: IBaseEntityProperty,
    bndGCloudInfra: IBndBaseGCloudInfra
  ): Promise<IBaseEntityPropertyResponse> {
    const pictures = property.pictures as string[];
    const mainPicture = property.mainPicture as string;
    const session = await startSession();
    session.startTransaction();

    try {
      const newProperty = this._setPropertyValues(
        new ModelProperty({ session }),
        property
      );
      const propertyImagePath = `properties/${newProperty.id}`;
      const nameMainPicture = this._getCreatedImageName(mainPicture);
      const newPathPictures = [];

      for (let pic of pictures) {
        const namePicture = this._getCreatedImageName(pic);
        const destPathPicture = `${propertyImagePath}/${namePicture}`;
        await bndGCloudInfra.moveFile(pic, destPathPicture);
        newPathPictures.push({ url: destPathPicture });
        if (namePicture == nameMainPicture) {
          newProperty.mainPicture = { url: destPathPicture };
        }
      }
      newProperty.pictures = newPathPictures;
      newProperty.investors = [];
      await newProperty.save({ session });
      await session.commitTransaction();
      session.endSession();

      return { id: newProperty._id.toHexString() };
    } catch (error) {
      await session.abortTransaction();
      session.endSession();

      throw new BaseError(
        `An error occurred while trying to create a property. ERROR ${error}`,
        500,
        'GenerateDBError',
        error
      );
    }
  }

  public async updateProperty(
    propertyData: IBaseEntityProperty,
    propertyId: string,
    bndGCloudInfra: IBndBaseGCloudInfra
  ): Promise<IBaseEntityPropertyResponse> {
    const newPictures: string[] = propertyData.newPictures as string[];
    const deletePictures: string[] = propertyData.deletePictures as string[];
    const newMainPicture: string = propertyData.newMainPicture as string;
    const session = await startSession();
    session.startTransaction();

    try {
      let oldProperty: IModelProperty = await ModelProperty.findOne({
        _id: propertyId,
      });
      if (!oldProperty)
        throw new BaseError('Property not found.', 404, 'updateProperty');

      let actualPictures: IModelPictures[] =
        oldProperty.pictures as IModelPictures[];
      const propertyImagePath = `properties/${oldProperty.id}`;
      // Work with deleted pictures from bucket
      if (deletePictures && deletePictures.length) {
        for (let toDeletePic of deletePictures) {
          const relativePath =
            bndGCloudInfra.getRelativePathFromFull(toDeletePic);
          await bndGCloudInfra.deleteFile(relativePath);
          actualPictures = actualPictures.filter((e) => {
            return e.url !== relativePath;
          });
        }
      }
      // Working with new pictures in the bucket
      if (newPictures && newPictures.length) {
        for (let newPicture of newPictures) {
          const namePicture = this._getCreatedImageName(newPicture);
          const destPathPicture = `${propertyImagePath}/${namePicture}`;
          await bndGCloudInfra.moveFile(newPicture, destPathPicture);
          actualPictures.push({ url: destPathPicture });
        }
      }
      // Working with new main pictures in the bucket
      if (newMainPicture) {
        const relativePath =
          bndGCloudInfra.getRelativePathFromFull(newMainPicture);
        const splitedPath = relativePath.split('/');
        let nameNewMainPicture = '';

        if (splitedPath[0] === 'tmp')
          nameNewMainPicture = this._getCreatedImageName(
            splitedPath[splitedPath.length - 1]
          );
        else
          nameNewMainPicture = this._getUpdatedImageName(
            splitedPath[splitedPath.length - 1]
          );

        const destPathMainPicture = `${propertyImagePath}/${nameNewMainPicture}`;
        oldProperty.mainPicture = { url: destPathMainPicture };
      }
      oldProperty.pictures = actualPictures;
      await oldProperty.save({ session });

      const updatedProperty = this._setPropertyValues(
        oldProperty,
        propertyData
      );

      await updatedProperty.save({ session });
      await session.commitTransaction();
      session.endSession();

      return { id: updatedProperty._id.toHexString() };
    } catch (error) {
      await session.abortTransaction();
      session.endSession();

      throw new BaseError(error.message, error.code, error.name, error);
    }
  }

  public async createPropertyFinancialInformation(
    PropertyfinancialInfo: IBaseEntityPropertyFinancialCreate,
    propertyId: string
  ): Promise<IBaseEntityPropertyFinancialCreateResponse> {
    try {
      const property: IModelProperty = await ModelProperty.findOne({
        _id: Types.ObjectId(propertyId),
      }).select('propertyFinancial');
      if (!property)
        throw new BaseError(
          'Property not found.',
          404,
          'execGetPropertyFinancial'
        );
      const session = await startSession();
      session.startTransaction();
      const propertyFinancial: IModelPropertyFinancial =
        await this._createOrUpdateFinancialInfo(
          PropertyfinancialInfo,
          property
        );
      await propertyFinancial.save({ session });

      if (!property.propertyFinancial || !property.isComplete) {
        property.propertyFinancial = propertyFinancial._id;
        property.isComplete = true;
        property.save({ session });
      }
      const financialQuarter: IModelPropertyFinancialQuarter =
        await this._createOrUpdateFinancialQuarter(
          PropertyfinancialInfo,
          propertyFinancial
        );
      await financialQuarter.save({ session });
      await session.commitTransaction();
      session.endSession();
      const quarterIds = await ModelPropertyFinancialQuarter.find({
        propertyFinancial: propertyFinancial._id,
      }).select('_id');
      return {
        financialId: propertyFinancial._id.toHexString(),
        propertyId: propertyId,
        financialQuarterId: quarterIds.map((q) => q._id.toHexString()),
      };
    } catch (error) {
      throw new BaseError(
        `An error occurred while trying to create a user: ${error.message}`,
        error.code,
        error.name,
        error
      );
    }
  }

  private _getCreatedImageName(imageName: string): string {
    let splitedName: string[] = imageName.split('/');
    const tmpName: string[] = splitedName[splitedName.length - 1].split('-');
    tmpName.splice(0, 1);
    return tmpName.join('-');
  }

  private _getUpdatedImageName(imageName: string): string {
    const posPoint = imageName.lastIndexOf('.');
    const justFormat = imageName.substring(posPoint); // string  ".jpg", ".png" ...
    const justName = imageName.substring(0, posPoint);

    return `${justName}${justFormat}`;
  }

  private _setPropertyValues(
    property: IModelProperty,
    propertyData: IBaseEntityProperty
  ): IModelProperty {
    property.name = propertyData.name;
    property.address = propertyData.address;
    property.type = propertyData.type;
    property.state = Types.ObjectId(propertyData.state as string);
    property.county = Types.ObjectId(propertyData.county as string);
    property.bedRooms = propertyData.bedRooms;
    property.bathRooms = propertyData.bathRooms;
    property.size = propertyData.size;
    property.parcelSize = propertyData.parcelSize;
    property.parcelNumber = propertyData.parcelNumber;
    property.strategies = propertyData.strategies;
    property.propertyDescription = propertyData.propertyDescription;
    property.location = propertyData.location;
    return property;
  }

  private async _createOrUpdateFinancialInfo(
    PropertyfinancialInfo: IBaseEntityPropertyFinancialCreate,
    property: IModelProperty
  ): Promise<IModelPropertyFinancial> {
    try {
      let propertyFinancial: IModelPropertyFinancial =
        await ModelPropertyFinancial.findOne({
          property: property._id,
        });
      if (!propertyFinancial) {
        propertyFinancial = new ModelPropertyFinancial();
        propertyFinancial.property = property._id;
      }
      propertyFinancial.totalValue = PropertyfinancialInfo.totalValue;
      propertyFinancial.purchasePrice = PropertyfinancialInfo.purchasePrice;
      propertyFinancial.initialInvestment =
        PropertyfinancialInfo.initialInvestment;
      propertyFinancial.aquisitionFee = PropertyfinancialInfo.aquisitionFee;
      propertyFinancial.dispositionFee = PropertyfinancialInfo.dispositionFee;
      propertyFinancial.amountAllocatedToLand =
        PropertyfinancialInfo.amountAllocatedToLand;
      propertyFinancial.depreciation = PropertyfinancialInfo.depreciation;
      propertyFinancial.commissions = PropertyfinancialInfo.commissions;
      propertyFinancial.downPayment = PropertyfinancialInfo.downPayment;
      propertyFinancial.capitalGainsTaxPaid =
        PropertyfinancialInfo.capitalGainsTaxPaid;
      propertyFinancial.salesPrice = PropertyfinancialInfo.salesPrice;
      return propertyFinancial;
    } catch (error) {
      throw new BaseError(
        `An error occurred while trying to get PropertyFinancialData.  ${error}`,
        500,
        'GenerateDBError | _createOrUpdateFinancialInfo',
        error
      );
    }
  }
  private async _createOrUpdateFinancialQuarter(
    financialQuarterInfo: IBaseEntityPropertyFinancialCreate,
    propertyFinancial: IModelPropertyFinancial
  ): Promise<IModelPropertyFinancialQuarter> {
    try {
      let financialQuarter: IModelPropertyFinancialQuarter =
        await ModelPropertyFinancialQuarter.findOne({
          propertyFinancial: propertyFinancial._id,
          quarter: financialQuarterInfo.quarter,
          year: financialQuarterInfo.year,
        });
      if (!financialQuarter) {
        financialQuarter = new ModelPropertyFinancialQuarter();
        financialQuarter.propertyFinancial = propertyFinancial._id;
        (propertyFinancial.quarters as Types.ObjectId[]).push(
          financialQuarter._id as Types.ObjectId
        );
      }
      financialQuarter.year = financialQuarterInfo.year;
      financialQuarter.quarter = financialQuarterInfo.quarter;
      financialQuarter.totalUnits = financialQuarterInfo.totalUnits;
      financialQuarter.rents = financialQuarterInfo.rents;
      financialQuarter.propertyManagement =
        financialQuarterInfo.propertyManagement;
      financialQuarter.assetManagement = financialQuarterInfo.assetManagement;
      financialQuarter.repairsMaintenance =
        financialQuarterInfo.repairsMaintenance;
      financialQuarter.taxes = financialQuarterInfo.taxes;
      financialQuarter.insurance = financialQuarterInfo.insurance;
      financialQuarter.reserve = financialQuarterInfo.reserve;
      financialQuarter.vacancy = financialQuarterInfo.vacancy;
      financialQuarter.operativeExpenses =
        financialQuarterInfo.operativeExpenses;

      return financialQuarter;
    } catch (error) {
      throw new BaseError(
        `An error occurred while trying to get PropertyFinancialData.  ${error}`,
        500,
        'GenerateDBError | createOrUpdateFinancialQuarter',
        error
      );
    }
  }

  public async deleteProperties(
    property: string
  ): Promise<IBaseEntityPropertyResponse> {
    try {
      const properties: IModelProperty = await ModelProperty.findOne({
        _id: property,
      });

      if (!properties) {
        throw new BaseError(
          `Property not found`,
          404,
          'BaseUseCaseProperty | deleteProperties'
        );
      }

      const propertysFunds = await ModelFundProperty.findOne({ property });
      const propertysPackages = await ModelPackageProperty.findOne({
        property,
      });
      const creditProperties = await ModelCreditlineProperty.findOne({
        property,
      });
      if (!propertysFunds && !propertysPackages && !creditProperties) {
        const deletedProperty = await ModelProperty.findOneAndDelete({
          _id: property,
        });

        return {
          id: deletedProperty._id.toHexString(),
        };
      }
      properties.isDeleted = true;
      properties.deletedBy.dateTime = new Date();
      await properties.save();
      return {
        id: properties._id.toHexString(),
        isDeleted: properties.isDeleted,
      };
    } catch (error) {
      throw new BaseError(
        `An error occurred while trying to deleted Properties.${error}`,
        500,
        'GenerateDBError | deleteProperties',
        error
      );
    }
  }

  public async updatePicturesProperty(
    propertyId: string,
    mainPicture: string,
    pictures: string[]
  ) {
    try {
      const property: IModelProperty = await ModelProperty.findById(propertyId);
      property.mainPicture = { url: mainPicture };
      property.pictures = pictures;
      await property.save();
    } catch (error) {
      throw new BaseError(
        `An error occurred while trying to update pictures from Properties.${error}`,
        500,
        'GenerateDBError | updatePicturesProperty',
        error
      );
    }
  }

  public async savePropertyProjection(
    params: IPropertyProjectionParams
  ): Promise<IPropertyCreateProjectionResponse> {
    const session = await startSession();
    session.startTransaction();
    try {
      const { idProperty, projectionInformation } = params;
      const property: IModelProperty = await ModelProperty.findById(idProperty);
      const propertyProjection: IModelPropertyProjection =
        new ModelPropertyProjection({
          property: property._id,
          totalValue: projectionInformation.totalValue,
          purchasePrice: projectionInformation.purchasePrice,
          aquisitionFee: projectionInformation.aquisitionFee,
          dispositionFee: projectionInformation.dispositionFee,
          amountAllocatedToLand: projectionInformation.amountAllocatedToLand,
          depreciation: projectionInformation.depreciation,
          commissions: projectionInformation.commissions,
          downPayment: projectionInformation.downPayment,
          capitalGainsTaxPaid: projectionInformation.capitalGainsTaxPaid,
          extraExpenses: projectionInformation.extraExpenses,
          projectionYears: [],
        });
      property.projection = propertyProjection._id;
      property.isComplete = true;
      const projectionLoans: IModelPropertyProjectionLoan[] =
        projectionInformation.loans.map(
          (loan): IModelPropertyProjectionLoan => {
            const loanProjection: IModelPropertyProjectionLoan =
              new ModelPropertyProjectionLoan({
                propertyProjection: propertyProjection._id,
                amount: loan.amount,
                interest: loan.interest,
                termYears: loan.termYears,
                paymentsPerYear: loan.paymentsPerYear
                  ? loan.paymentsPerYear
                  : 12,
                amortizationYears: loan.amortizationYears,
                ltc: loan.ltc ? loan.ltc : null,
              });
            propertyProjection.loans.push(loanProjection._id);
            return loanProjection;
          }
        );
      const projectedYears: IModelPropertyProjectionYear[] =
        projectionInformation.operativeInfo.map(
          (operativeYear): IModelPropertyProjectionYear => {
            const projectedYear = new ModelPropertyProjectionYear({
              propertyProjection: propertyProjection._id,
              year: operativeYear.year,
              units: operativeYear.units,
              rent: operativeYear.rent,
              vacancy: operativeYear.vacancy,
              propertyManagement: operativeYear.propertyManagement,
              assetManagement: operativeYear.assetManagement,
              taxes: operativeYear.taxes,
              insurance: operativeYear.insurance,
              repairs: operativeYear.repairs,
              reserve: operativeYear.reserve,
              salesPrice: operativeYear.salesPrice,
            });
            propertyProjection.projectionYears.push(projectedYear._id);
            return projectedYear;
          }
        );
      await property.save({ session });
      await propertyProjection.save({ session });
      for (const projectedYear of projectedYears) {
        await projectedYear.save({ session });
      }
      for (const loanProjection of projectionLoans) {
        await loanProjection.save({ session });
      }
      await session.commitTransaction();
      session.endSession();
      return {
        idProjection: propertyProjection.id,
      };
    } catch (error) {
      await session.abortTransaction();
      session.endSession();
      throw new BaseError(
        `An error occurred while trying to save the property projection.${error}`,
        error.code,
        'GenerateDBError | savePropertyProjection',
        error
      );
    }
  }

  public async savePropertiesAndProjectionsByExcel(
    params: IPropertySaveExcelParams
  ): Promise<IExcelPropertiesAndProjectionsSaveResponse> {
    const { propertiesAndProjections, userRequest, bndGCloudInfra } = params;
    const { projections, propertiesInfo } = propertiesAndProjections;
    const { properties } = propertiesInfo;
    const session = await startSession();
    session.startTransaction();
    const propertiesPicturesImages: string[] = [];
    const propertiesIdByName = {};
    try {
      /* Save properties */
      const newProperties: IModelProperty[] = !properties
        ? []
        : properties.map(
            (excelProperty: IBaseEntityProperty): IModelProperty => {
              return new ModelProperty({
                name: excelProperty.name,
                address: excelProperty.address,
                type: excelProperty.type,
                state: (excelProperty.state as IBaseEntityStates).id,
                county: (excelProperty.county as IBaseEntityCounties).id,
                bedRooms: excelProperty.bedRooms,
                bathRooms: excelProperty.bathRooms,
                size: excelProperty.size,
                parcelSize: excelProperty.parcelSize,
                parcelNumber: excelProperty.parcelNumber,
                strategies: excelProperty.strategies,
                propertyDescription: excelProperty.propertyDescription,
                pictures: null,
                mainPicture: null,
                location: {
                  lat: `${excelProperty.location.lat}`,
                  lng: `${excelProperty.location.lng}`,
                },
                excelImportInformation: {
                  column: excelProperty.excelImportInformation.column,
                  excelName: excelProperty.excelImportInformation.excelName,
                  importDate: excelProperty.excelImportInformation.importDate,
                  isValid: excelProperty.excelImportInformation.isValid,
                },
                createdBy: {
                  id: userRequest,
                  dateTime: DateTime.now(),
                },
              });
            }
          );
      for (const property of newProperties) {
        propertiesIdByName[`${property.name}`] = property.id;
        const imageURL: string = `properties/${property.id}/defaultImage.png`;
        propertiesPicturesImages.push(imageURL);
        await bndGCloudInfra.copyFile('excel/images/Imagen Base.png', imageURL);
        property.pictures = [{ url: imageURL }];
        property.mainPicture = { url: imageURL };
        await property.save({ session });
      }
      /* Save projections */
      const newProjections: IModelPropertyProjection[] = [];
      const newProjectionsYears: IModelPropertyProjectionYear[] = [];
      const newProjectionsLoans: IModelPropertyProjectionLoan[] = [];
      const oldProperties: IModelProperty[] = [];
      for (const projectionInfo of !projections ? [] : projections) {
        const { projection } = projectionInfo;
        const newProjection: IModelPropertyProjection =
          new ModelPropertyProjection({
            property: projection.propertyId
              ? projection.propertyId
              : propertiesIdByName[`${projection.propertyName}`],
            totalValue: projection.totalValue,
            purchasePrice: projection.purchasePrice,
            aquisitionFee: projection.aquisitionFee,
            dispositionFee: projection.dispositionFee,
            amountAllocatedToLand: projection.amountAllocatedToLand,
            depreciation: projection.depreciation,
            commissions: projection.commissions,
            downPayment: projection.downPayment,
            capitalGainsTaxPaid: projection.capitalGainsTaxPaid,
            extraExpenses: [],
            loans: [],
            projectionYears: [],
          });
        if (projection.propertyId) {
          const oldProperty: IModelProperty = await ModelProperty.findOne({
            _id: projection.propertyId,
          });
          oldProperty.projection = newProjection.id;
          oldProperties.push(oldProperty);
        } else {
          const indexProperty: number = newProperties.findIndex(
            (e) => e.name == projection.propertyName
          );
          newProperties[indexProperty].projection = newProjection.id;
        }
        const newProjectionYears: IModelPropertyProjectionYear[] =
          projection.operativeInfo.map(
            (
              operativeYear: IPropertyOperatingYear
            ): IModelPropertyProjectionYear => {
              return new ModelPropertyProjectionYear({
                propertyProjection: newProjection.id,
                year: operativeYear.year,
                units: operativeYear.units,
                rent: operativeYear.rent,
                vacancy: operativeYear.vacancy,
                propertyManagement: operativeYear.propertyManagement,
                assetManagement: operativeYear.assetManagement,
                taxes: operativeYear.taxes,
                insurance: operativeYear.insurance,
                repairs: operativeYear.repairs,
                reserve: operativeYear.reserve,
                salesPrice: operativeYear.salesPrice,
              });
            }
          );
        newProjection.projectionYears = newProjectionYears.map((e) => e.id);
        const newProjectionLoans: IModelPropertyProjectionLoan[] =
          projection.loans.map(
            (loan: ILoansByPropertyParams): IModelPropertyProjectionLoan => {
              return new ModelPropertyProjectionLoan({
                propertyProjection: newProjection.id,
                amount: loan.amortizationYears,
                interest: loan.interest,
                termYears: loan.termYears,
                paymentsPerYear: !loan.paymentsPerYear
                  ? 12
                  : loan.paymentsPerYear,
                amortizationYears: loan.amortizationYears,
              });
            }
          );
        newProjection.loans = newProjectionLoans.map((e) => e.id);
        newProjections.push(newProjection);
        newProjectionsYears.push(...newProjectionYears);
        newProjectionsLoans.push(...newProjectionLoans);
      }
      for (const tmpProjections of newProjections) {
        await tmpProjections.save({ session });
      }
      for (const tmpProjectionsYears of newProjectionsYears) {
        await tmpProjectionsYears.save({ session });
      }
      for (const tmpProjectionsLoans of newProjectionsLoans) {
        await tmpProjectionsLoans.save({ session });
      }
      for (const oldProperty of oldProperties) {
        await oldProperty.save({ session });
      }
      await session.commitTransaction();
      session.endSession();
      return {
        projections: newProjections.map((p: IModelPropertyProjection) => {
          return {
            id: p.id,
          };
        }),
        properties: newProperties.map((p: IModelProperty) => {
          return {
            id: p.id,
            name: p.name,
          };
        }),
      };
    } catch (error) {
      for (const imageURL of propertiesPicturesImages) {
        await bndGCloudInfra.deleteFile(imageURL);
      }
      await session.abortTransaction();
      session.endSession();

      throw new BaseError(
        `An error occurred while trying to save properties by excel. ${error}`,
        error.code,
        'GenerateDBError | updatePicturesProperty',
        error
      );
    }
  }

  public async saveProfitsAndLoss(
    profitsAndLoss: IPropertyProfitsAndLossExcelInfo[]
  ): Promise<IExcelPropertiesProfitsAndLossResponse> {
    const session = await startSession();
    session.startTransaction();
    try {
      const profitsAndLossIds: string[] = [];
      //TODO addapt the new structure of profits and loss to save it
      // for (const profitsAndLossData of profitsAndLoss) {
      //   const property: IModelProperty = await ModelProperty.findOne({
      //     _id: profitsAndLossData.propertyId,
      //     'deletedBy.dateTime': null,
      //   });
      //   if (!property)
      //     throw new BaseError(
      //       `Property not found ${profitsAndLossData.propertyName} | ${profitsAndLossData.propertyId}`,
      //       404,
      //       'BaseUseCaseProperty | saveProfitsAndLoss'
      //     );
      //   let propertyActual: IModelPropertyActual;

      //   if (!property.actual) {
      //     propertyActual = new ModelPropertyActual({
      //       property: property.id,
      //       profitsAndLoss: [],
      //     });
      //     property.actual = propertyActual.id;
      //     await property.save({ session });
      //   } else {
      //     propertyActual = await ModelPropertyActual.findOne({
      //       _id: property.actual,
      //     });
      //   }
      //   const newProfitAndLoss: IModelPropertyActualProfitsAndLoss =
      //     new ModelPropertyActualProfitsAndLoss({
      //       propertyActual: propertyActual.id,
      //       dateInfo: profitsAndLossData.dateInfo,
      //       income: profitsAndLossData.income,
      //       grossProfit: profitsAndLossData.grossProfit,
      //       expenses: profitsAndLossData.expenses,
      //       netOperatingIncome: profitsAndLossData.netIncome,
      //       otherIncome: profitsAndLossData.otherIncome,
      //       gainOnSaleOfProperty: profitsAndLossData.gainOnSaleOfProperty,
      //       totalOtherIncome: profitsAndLossData.totalOtherIncome,
      //       netOtherIncome: profitsAndLossData.netOtherIncome,
      //       netIncome: profitsAndLossData.netIncome,
      //     });
      //   propertyActual.profitsAndLoss.push(newProfitAndLoss.id);
      //   profitsAndLossIds.push(newProfitAndLoss.id);
      //   await newProfitAndLoss.save({ session });
      //   await propertyActual.save({ session });
      // }
      await session.commitTransaction();
      session.endSession();
      return { profitsAndLossIds };
    } catch (error) {
      await session.abortTransaction();
      session.endSession();
      throw new BaseError(
        `An error occurred while trying to save the profits and lost`,
        error.code,
        'GenerateDBError | saveProfitsAndLoss',
        error
      );
    }
  }
}
