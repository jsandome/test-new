import { IPropertyProjection } from '@capa/core';
import BigNumber from 'bignumber.js';
import { IModelProperty } from '../../models';

export interface ILoanByPropert {
  amount: BigNumber;
  interest: BigNumber;
  schedulePayment: BigNumber;
  amortizationYears: BigNumber;
  loanTerm: BigNumber;
  paymentsPerYear: BigNumber;
  ltc: number;
}

export interface ILoansByProperty {
  totalAmount: BigNumber;
  loans: ILoanByPropert[];
}

export interface IPropertyPreviewResponseData {
  loans: ILoansByProperty;
}

export interface IRAWOperatingExtraExpenseParam {
  name: string;
  amount: BigNumber;
}

export interface IRAWPropertyOperatingYear {
  year: number;
  rent: BigNumber;
  vacancy: BigNumber;
  propertyManagement: BigNumber;
  assetManagement: BigNumber;
  taxes: BigNumber;
  insurance: BigNumber;
  repairs: BigNumber;
  reserve: BigNumber;
  units: BigNumber;
  salesPrice: BigNumber;
  extraExpenses: IRAWOperatingExtraExpenseParam[];
}

interface IRAWPropertyProjectionExtraExpensesYear {
  year: number;
  amount: BigNumber;
}

export interface IRAWPropertyProjectionExtraExpenses {
  name: String;
  years: IRAWPropertyProjectionExtraExpensesYear[];
}

export interface IRAWPropertyPreviewInput {
  operativeInfo: IRAWPropertyOperatingYear[];
  aquisitionFee: BigNumber;
  dispositionFee: BigNumber;
  amountAllocatedToLand: BigNumber;
  depreciation: BigNumber;
  commissions: BigNumber;
  downPayment: BigNumber;
  purchasePrice: BigNumber;
  capitalGainsTaxPaid: BigNumber;
}

export interface IPropertyPreviewGenerateParams {
  data: IRAWPropertyPreviewInput;
  loanData: ILoansByProperty;
}

export interface IPropertyPreviewProjection {
  year: number;
  value: BigNumber;
}

export interface IPPOperativeResponse /*Property preview operating response*/ {
  scheduleGrossIncome: IPropertyPreviewProjection[];
  totalOperatingExpenses: IPropertyPreviewProjection[];
  netOperatingIncome: IPropertyPreviewProjection[];
}

export interface IPPMathOperativeYear {
  scheduledGrossIncomeYear: IPropertyPreviewProjection;
  totalOperatingExpensesYear: IPropertyPreviewProjection;
  netOperatingIncomeYear: IPropertyPreviewProjection;
}

export interface ISOPCFParams /*Schedule of prospective cash flow params */ {
  loans: ILoanByPropert[];
  netOperatingIncome: IPropertyPreviewProjection[];
  purchasePrice: BigNumber;
}

export interface eoyGralInfoParams {
  interest: BigNumber;
  paymentsPerYear: BigNumber;
  amortizationYears: BigNumber;
  schedulePayment: BigNumber;
  year: number;
  amount: BigNumber;
}

export interface eoyGralInfoResponse {
  eoyPrincipalBalance: BigNumber;
  annualEquityBuildUp: BigNumber;
}

export interface ISOPCSYear /*Schedule of prospective cash flow params */ {
  year: number;
  annualDebtService: /* Scheduled Payment */ BigNumber;
  annualCashFlow: BigNumber;
  annualCashOnCashReturn: BigNumber;
  eoyPrincipalBalance: BigNumber;
  annualEquityBuildUp: BigNumber;
  annualEquityReturnOnEquityBuildIp: BigNumber;
  totalAnnualReturn: BigNumber;
}

export interface ISOPCSLoanResponse {
  amount: BigNumber;
  initialEquity: BigNumber;
  interest: BigNumber;
  paymentsPerYear: BigNumber;
  amortizationYears: BigNumber;
  termYears: BigNumber;
  schedulePayment: BigNumber;
  ltc: number;
  scheduleOfProspectives: ISOPCSYear[];
}

export interface ISOPCSResponse /*Schedule of prospective cash flow params */ {
  debtOverview: ISOPCSLoanResponse[];
  annualCashFlowTotal: IPropertyPreviewProjection[];
  eoyPrincipalBalanceTotal: IPropertyPreviewProjection[];
}

export interface IExitStrategyParams {
  salesPrice: IPropertyPreviewProjection[];
  dispositionFee: BigNumber;
  commissions: BigNumber;
  aquisitionFee: BigNumber;
  amountAllocatedToLand: BigNumber;
  depreciation: BigNumber;
  annualCashFlowTotal: IPropertyPreviewProjection[];
  eoyPrincipalBalanceTotal: IPropertyPreviewProjection[];
  downPayment: BigNumber;
  purchasePrice: BigNumber;
  capitalGainsTaxPaid: BigNumber;
}

export interface IExitStrategyRAW {
  year: number;
  salesPrice: BigNumber;
  costOfSale: BigNumber;
  propertyTaxBasis: BigNumber;
  amountSubjecttoCapitalGainsTax: BigNumber;
  capitalGainsTaxPaid: BigNumber;
  debtPrincipalBalance: BigNumber;
  cashReceivedFromSale: BigNumber;
  cashFlowReceived: BigNumber;
  totalReturn: BigNumber;
  totalAnnualROI: BigNumber;
}

export interface IPropertyPreviewRAW {
  scheduleGrossIncome: IPropertyPreviewProjection[];
  totalOperatingExpenses: IPropertyPreviewProjection[];
  netOperatingIncome: IPropertyPreviewProjection[];
  capRate: IPropertyPreviewProjection[];
  debtOverview: ISOPCSLoanResponse[];
  annualCashFlowTotal: IPropertyPreviewProjection[];
  eoyPrincipalBalanceTotal: IPropertyPreviewProjection[];
  exitStrategy: IExitStrategyRAW[];
}

export interface IPropertyPreviewOBjectParams {
  financialInfo: IPropertyProjection;
  propertyInfo: IModelProperty;
  previewInfo: IPropertyPreviewRAW;
}

export interface IGralLoanByPropertyParams {
  amount: BigNumber;
  interest: BigNumber;
  termYears: BigNumber;
  paymentsPerYear: BigNumber;
  amortizationYears: BigNumber;
  ltc: number;
}
