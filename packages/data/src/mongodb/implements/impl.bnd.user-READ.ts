import { injectable } from 'inversify';
import { BaseError, IBaseEntityUser, IBndBaseUserRead } from '@capa/core';
import { IModelUser, ModelUser } from '../models';

@injectable()
export class ImplBndBaseUserReadMongo implements IBndBaseUserRead {
  public async getUserByEmailGETPASS(email: string): Promise<IBaseEntityUser> {
    try {
      const user: IModelUser = await ModelUser.findOne({
        $expr: {
          $eq: [{ $toLower: '$email' }, email.toLowerCase()],
        },
      }).select('_id admin investor email password twoFactor type active');
      if (!user) return null;

      const res = user.toObject();
      return {
        id: res._id,
        email: res.email,
        password: res.password,
        admin: res.admin ? res.admin.toHexString() : null,
        investor: res.investor ? res.investor.toHexString() : null,
        twoFactor: res.twoFactor,
        type: res.type,
        active: res.active,
      };
    } catch (error) {
      throw new BaseError(
        `An error ocurred while trying to get a user: ${email}.`,
        500,
        'MongoDbUserError',
        error as Error
      );
    }
  }
  public async get2FASECRET(id: string): Promise<string> {
    try {
      const user = await ModelUser.findById(id).select('_id twoFactor');
      if (!user) return null;

      const res = user.toObject();
      return res.twoFactor.secret;
    } catch (error) {
      throw new BaseError(
        `An error ocurred while trying to get a 2FA secret.`,
        500,
        'MongoDbUserError',
        error as Error
      );
    }
  }
  public async getUserIdByEmail(email: string): Promise<string> {
    try {
      const user = await ModelUser.findOne({
        $expr: {
          $eq: [{ $toLower: '$email' }, email.toLowerCase()],
        },
      }).select('_id');
      return user.id;
    } catch (error) {
      throw new BaseError(
        `An error occurred while trying to get a user _id. ERROR ${error}`,
        500,
        'GenerateDBError. ImplBndBaseUserReadMongo | getUserIdByEmail',
        error as Error
      );
    }
  }
  public async getById(id: string): Promise<IBaseEntityUser> {
    try {
      const user: IModelUser = await ModelUser.findById(id);
      if (!user) return null;

      const res = user.toObject();
      return {
        id: res._id,
        admin: String(res.admin),
        investor: String(res.investor),
        type: res.type,
        email: res.email,
        active: res.active,
        password: res.password,
      };
    } catch (error) {
      throw new BaseError(
        `An error occurred while trying to get a user. ERROR ${error}`,
        500,
        'GenerateDBError',
        error as Error
      );
    }
  }

  public async getLoginDataByEmail(email: string): Promise<IBaseEntityUser> {
    try {
      const user: IModelUser = await ModelUser.findOne({ email: email });
      if (!user) return null;
      const res = user.toObject();
      return {
        active: res.active,
        admin: res.admin ? null : `${res.admin}`,
        email: res.email,
        investor: res.investor ? null : `${res.investor}`,
        type: res.type,
        id: res._id,
        password: res.password,
      };
    } catch (error) {
      throw new BaseError(
        `An error occurred while trying to get a user. ERROR ${error}`,
        500,
        'GenerateDBError',
        error as Error
      );
    }
  }

  public async getUserByToken(token: string): Promise<IBaseEntityUser> {
    const user: IModelUser = await ModelUser.findOne({
      'expirePass.token': token,
    });
    if (!user) return null;

    return {
      email: user.email,
      active: user.active,
      admin: user.admin ? null : `${user.admin}`,
      investor: user.investor ? null : `${user.investor}`,
      type: user.type,
      expirePass: user.expirePass,
    };
  }

  public async getUserBySecure(
    email: string,
    secure: string
  ): Promise<IBaseEntityUser> {
    try {
      const user: IModelUser = await ModelUser.findOne({
        email,
        'twoFactor.secure': secure,
      });

      if (!user) return null;

      const res = user.toObject();
      const response: IBaseEntityUser = {
        email: res.email,
        active: res.active,
        admin: res.admin !== null ? `${res.admin}` : null,
        investor: res.investor !== null ? `${res.investor}` : null,
        type: res.type,
        expirePass: res.expirePass,
        twoFactor: res.twoFactor,
      };

      return response;
    } catch (error) {
      throw new BaseError(
        `An error occurred while trying to get a user. ERROR ${error}`,
        500,
        'GenerateDBError | getUserByToken',
        error as Error
      );
    }
  }

  public async validateSessionToken(
    token: string,
    id: string
  ): Promise<boolean> {
    try {
      const user: IModelUser = await ModelUser.findById(id);
      return user.tokenJwt === token;
    } catch (error) {
      throw new BaseError(
        `An error occurred while trying to get a user. ERROR ${error}`,
        500,
        'GenerateDBError',
        error as Error
      );
    }
  }
}
