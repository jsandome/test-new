import { injectable } from 'inversify';
import {
  BaseError,
  IBndBaseCapitalCallRead,
  IBndReturnGetCapitallCall,
  IBndReturnListCapitalCall,
  IBndReturnSelectCapitallCall,
  ReponseAggregateCapitalCall,
  IPaginateParams,
} from '@capa/core';
import {
  IModelCapitalCall,
  IModelCapitalCallInvestor,
  IModelLlc,
  ModelCapitalCall,
  ModelLlc,
  ModelCapitalCallInvestor,
  IModelContribution,
  ModelContribution,
} from '../../models';
import Bignumber from 'bignumber.js';
import {
  queryDetailsCapitalCall,
  queryDetailsByCapitalCall,
} from './querys.capitalCall';
@injectable()
export class ImplBndCapitalCallRead implements IBndBaseCapitalCallRead {
  public async detailCapitalCall(
    llcId: string,
    capitalCallId: string
  ): Promise<IBndReturnGetCapitallCall> {
    try {
      const llc: IModelLlc = await ModelLlc.findById(llcId);
      if (!llc) {
        throw new BaseError(
          'Entity not found.',
          404,
          'BaseUseCaseCapitalCall | detailCapitalCall'
        );
      }

      const query = queryDetailsByCapitalCall(llcId, capitalCallId);
      const dataCapitalCall = await ModelCapitalCall.aggregate(query);
      const array = [];

      for (let i = 0; i < dataCapitalCall.length; i++) {
        const details = dataCapitalCall[i].details;
        const amount = dataCapitalCall[i].totals.totalAmount;
        const totals = dataCapitalCall[i].totals;
        const receivedAccounts =
          dataCapitalCall[i].totals.receivedAccounts.length;

        let totalAmount = 0;
        amount.map((e) => (totalAmount += new Bignumber(e).toNumber()));

        const totalsReceived: number[] = totals.received.map((i) => {
          let totalReceived = 0;

          i.forEach((res) => {
            totalReceived += new Bignumber(res).toNumber();
          });

          return totalReceived;
        });

        const received = totalsReceived.reduce(
          (a, b) => new Bignumber(a).toNumber() + new Bignumber(b).toNumber(),
          0
        );

        const data: IBndReturnGetCapitallCall = {
          totals: {
            dueDate: totals.dueDate,
            totalAmount,
            totalAccounts: totals.totalAccounts,
            received,
            receivedAccounts,
            outstandingAccounts: totals.totalAccounts - receivedAccounts,
            outstanding: new Bignumber(totalAmount).minus(received).toNumber(),
          },
          details: details.map((i) => {
            let received = 0;
            i.received.map((e) => (received += new Bignumber(e).toNumber()));
            return {
              account: i.account,
              commitment: i.commitment,
              called: new Bignumber(i.called).toNumber(),
              lifetimeCalled: new Bignumber(i.lifetimeCalled).toNumber(),
              investorId: i.investorId,
              received,
              receivedDate: i.receivedDate[0],
            };
          }),
        };

        array.push(data);
      }

      return array[0];
    } catch (error) {
      throw new BaseError(error.message, error.code, error.name, error);
    }
  }

  public async listCapitalCall(
    llcId: string,
    { limit = 10, page = 1 }: IPaginateParams,
    search: string
  ): Promise<IBndReturnListCapitalCall> {
    try {
      const llc: IModelLlc = await ModelLlc.findOne({
        _id: llcId,
        active: true,
      });
      if (!llc) {
        throw new BaseError(
          'Entity not found.',
          404,
          'BaseUseCaseCapitalCall | listCapitalCall'
        );
      }
      const options = {
        limit,
        page,
        offset: (page - 1) * limit,
      };

      const query = queryDetailsCapitalCall(llcId, search);

      const aggregate: ReponseAggregateCapitalCall =
        ModelCapitalCall.aggregate(query);

      const capitalCallData = await ModelCapitalCall.aggregatePaginate(
        aggregate,
        options
      );

      const amountCalled: number[] =
        capitalCallData.docs[0].totals.amountCalled.map((i) => {
          let totalAmountCalled = 0;

          i.forEach((totaAm) => {
            totalAmountCalled += new Bignumber(totaAm).toNumber();
          });

          return totalAmountCalled;
        });
      const result: number[] = capitalCallData.docs[0].totals.result.map(
        (i) => {
          let totalResult = 0;

          i.forEach((res) => {
            totalResult += new Bignumber(res).toNumber();
          });

          return totalResult;
        }
      );

      return {
        pagination: {
          itemCount: capitalCallData.totalDocs,
          offset: (page - 1) * limit,
          limit: capitalCallData.limit,
          totalPages: capitalCallData.totalPages,
          page: capitalCallData.page,
          nextPage: capitalCallData.nextPage,
          prevPage: capitalCallData.prevPage,
        },

        details: capitalCallData.docs[0].details.map((e) => {
          let amountCalled = 0;
          e.amountCalled.forEach((i) => {
            amountCalled += new Bignumber(i).toNumber();
          });

          let result = 0;
          e.result.forEach((a) => {
            result += new Bignumber(a).toNumber();
          });

          return {
            id: e.id,
            dueDate: e.dueDate,
            description: e.description,
            parties: e.parties,
            amountCalled: new Bignumber(amountCalled).toNumber() ?? 0,
            result: new Bignumber(result).toNumber() ?? 0,
          };
        }),
        totals: {
          amountCalled: amountCalled.reduce(
            (a, b) => new Bignumber(a).toNumber() + new Bignumber(b).toNumber(),
            0
          ),
          result: result.reduce(
            (a, b) => new Bignumber(a).toNumber() + new Bignumber(b).toNumber(),
            0
          ),
        },
      };
    } catch (error) {
      throw new BaseError(error.message, error.code, error.name, error);
    }
  }

  public async selectCapitalCall(
    llcId: string,
    search: string
  ): Promise<IBndReturnSelectCapitallCall[]> {
    try {
      const llc: IModelLlc = await ModelLlc.findById(llcId);
      if (!llc)
        throw new BaseError(
          'Entity not found.',
          404,
          'BaseUseCaseCapitalCall | selectCapitalCall'
        );
      const query = {
        llc: llcId,
        'deletedBy.dateTime': null,
      };
      const result: IBndReturnSelectCapitallCall[] = [];
      if (search) {
        query['description'] = {
          $regex: search,
          $options: 'i',
        };
      }

      const capitalCalls: IModelCapitalCall[] = await ModelCapitalCall.find(
        query
      );
      let contributionsAmount = new Bignumber(0);
      let capitalAmount = new Bignumber(0);
      for (let cc of capitalCalls) {
        const ccInvestors: IModelCapitalCallInvestor[] =
          await ModelCapitalCallInvestor.find({
            capitalCall: cc.id,
          });

        for (let ccInvestor of ccInvestors) {
          const contributions: IModelContribution[] =
            await ModelContribution.find({
              capitalCallInvestor: ccInvestor.id,
            });

          for (let contr of contributions)
            contributionsAmount = contributionsAmount.plus(contr.paymentAmount);
          capitalAmount = capitalAmount.plus(ccInvestor.capitalAmount);
        }
        result.push({
          id: cc.id,
          description: cc.description,
          amountCalled: capitalAmount.toNumber(),
          result: contributionsAmount.toNumber(),
        });
      }

      return result;
    } catch (error) {
      throw new BaseError(error.message, error.code, error.name, error);
    }
  }
}
