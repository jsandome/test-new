import { injectable } from 'inversify';
import {
  BaseError,
  IBndBaseCapitalCallWrite,
  IBndReturnCreateCapitallCall,
  IParamsCreateCapitalCall,
  IParamsUpdateCapitalCall,
} from '@capa/core';
import { startSession } from 'mongoose';
import {
  IModelCapitalCall,
  IModelCapitalCallInvestor,
  IModelLlc,
  ModelCapitalCall,
  ModelCapitalCallInvestor,
  ModelLlc,
} from '../../models';

@injectable()
export class ImplBndCapitalCallWrite implements IBndBaseCapitalCallWrite {
  public async createNewCapitalCall(
    newCapitalCall: IParamsCreateCapitalCall,
    llcId: string
  ): Promise<IBndReturnCreateCapitallCall> {
    const session = await startSession();
    session.startTransaction();
    const { description, dueDate, investors } = newCapitalCall;

    try {
      const createCapitalCall: IModelCapitalCall = new ModelCapitalCall({
        description,
        dueDate,
        llc: llcId,
      });

      for (let capitals of investors) {
        const capitalCallInvestor: IModelCapitalCallInvestor =
          new ModelCapitalCallInvestor({
            investor: capitals.id,
            capitalAmount: capitals.capitalAmount,
            capitalCall: createCapitalCall._id,
          });
        await capitalCallInvestor.save({ session });
      }

      await createCapitalCall.save({ session });

      await session.commitTransaction();
      session.endSession();

      return { id: createCapitalCall._id.toHexString() };
    } catch (error) {
      await session.abortTransaction();
      session.endSession();
      throw new BaseError(
        `Error creating a Capital Call. ${error.message}`,
        error.code,
        error.name,
        error
      );
    }
  }

  public async updateCapitalCall(
    params: IParamsUpdateCapitalCall,
    llcId: string,
    capitalCallId: string
  ): Promise<void> {
    try {
      const { description, dueDate } = params;
      const llc: IModelLlc = await ModelLlc.findById(llcId);
      if (!llc) {
        throw new BaseError(
          'Entity not found.',
          404,
          'BaseUseCaseCapitalCall | updateCapitalCall'
        );
      }

      const capitalcall: IModelCapitalCall = await ModelCapitalCall.findOne({
        'deletedBy.dateTime': null,
        _id: capitalCallId,
        llc: llcId,
      });
      if (!capitalcall) {
        throw new BaseError(
          'Capital Call not found.',
          404,
          'BaseUseCaseCapitalCall | updateCapitalCall'
        );
      }

      capitalcall.description = description;
      capitalcall.dueDate = dueDate;
      await capitalcall.save();
    } catch (error) {
      throw new BaseError(error.message, error.code, error.name, error);
    }
  }

  public async deleteCapitalCall(
    llcId: string,
    capitalCallId: string,
    userId: string
  ): Promise<void> {
    try {
      const llc: IModelLlc = await ModelLlc.findById(llcId);
      if (!llc) {
        throw new BaseError(
          'Entity not found.',
          404,
          'BaseUseCaseCapitalCall | deleteCapitalCall'
        );
      }

      const capitalcall: IModelCapitalCall = await ModelCapitalCall.findOne({
        'deletedBy.dateTime': null,
        _id: capitalCallId,
        llc: llcId,
      });
      if (!capitalcall) {
        throw new BaseError(
          'Capital Call not found.',
          404,
          'BaseUseCaseCapitalCall | deleteCapitalCall'
        );
      }
      capitalcall.deletedBy.id = userId;
      capitalcall.deletedBy.dateTime = new Date();
      await capitalcall.save();
    } catch (error) {
      throw new BaseError(error.message, error.code, error.name, error);
    }
  }
}
