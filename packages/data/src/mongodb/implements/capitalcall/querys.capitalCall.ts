import { ObjectId } from 'mongodb';

export const queryDetailsCapitalCall = (llcId: string, search: string) => {
  return [
    {
      $match: {
        llc: new ObjectId(llcId.toString()),
        'deletedBy.dateTime': null,
        description: { $regex: search ?? '', $options: 'i' },
      },
    },
    {
      $lookup: {
        from: 'capitalcallinvestors',
        let: { capitalCall: '$_id' },
        pipeline: [
          { $match: { $expr: { $eq: ['$capitalCall', '$$capitalCall'] } } },
          {
            $lookup: {
              from: 'investors',
              localField: 'investor',
              foreignField: '_id',
              as: 'investors',
            },
          },
          { $unwind: { path: '$investors' } },
        ],
        as: 'capitalcallinvestors',
      },
    },
    {
      $lookup: {
        from: 'contributions',
        localField: 'capitalcallinvestors._id',
        foreignField: 'capitalCallInvestor',
        as: 'contributions',
      },
    },
    {
      $group: {
        _id: null,
        details: {
          $push: {
            id: '$_id',
            dueDate: '$dueDate',
            description: '$description',
            parties: { $size: '$capitalcallinvestors.investors' },
            amountCalled: '$capitalcallinvestors.capitalAmount',
            result: '$contributions.paymentAmount',
          },
        },
      },
    },
    {
      $project: {
        _id: 0,
        details: '$details',
        totals: {
          amountCalled: '$details.amountCalled',
          result: '$details.result',
        },
      },
    },
  ];
};

export const queryDetailsByCapitalCall = (llcId: string, _id: string) => {
  return [
    {
      $match: {
        llc: new ObjectId(llcId.toString()),
        'deletedBy.dateTime': null,
        _id: new ObjectId(_id.toString()),
      },
    },
    {
      $lookup: {
        from: 'capitalcallinvestors',
        localField: '_id',
        foreignField: 'capitalCall',
        as: 'capitalcallinvestors',
      },
    },
    { $unwind: { path: '$capitalcallinvestors' } },
    {
      $lookup: {
        from: 'contributions',
        localField: 'capitalcallinvestors._id',
        foreignField: 'capitalCallInvestor',
        as: 'contributions',
      },
    },
    {
      $lookup: {
        from: 'investors',
        localField: 'capitalcallinvestors.investor',
        foreignField: '_id',
        as: 'investors',
      },
    },
    { $unwind: { path: '$investors' } },
    {
      $group: {
        _id: {
          dueDate: '$dueDate',
        },
        totalAmount: {
          $push: '$capitalcallinvestors.capitalAmount',
        },
        totalAccounts: {
          $push: '$investors',
        },
        received: {
          $push: '$contributions.paymentAmount',
        },
        details: {
          $push: {
            account: {
              $concat: [
                '$investors.name.firstName',
                ' ',
                '$investors.name.lastName',
              ],
            },
            commitment: 500000,
            called: '$capitalcallinvestors.capitalAmount',
            lifetimeCalled: '$capitalcallinvestors.capitalAmount',
            investorId: '$investors._id',
            received: '$contributions.paymentAmount',
            receivedDate: { $slice: ['$contributions.receivedDate', -1] },
          },
        },
      },
    },
    {
      $project: {
        _id: 0,
        totals: {
          dueDate: '$_id.dueDate',
          totalAmount: '$totalAmount',
          totalAccounts: { $size: '$totalAccounts' },
          receivedAccounts: {
            $filter: {
              input: '$received',
              as: 'accounts',
              cond: { $gt: [{ $size: '$$accounts' }, 0] },
            },
          },
          received: '$received',
        },
        details: '$details',
      },
    },
  ];
};
