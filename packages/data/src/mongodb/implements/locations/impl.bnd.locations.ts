import { injectable } from 'inversify';
import {
  BaseError,
  IBaseEntityCounties,
  IBaseEntityCountries,
  IBaseEntityStates,
  IBndBaseLocations,
  IBaseEntityCountyParam,
} from '@capa/core';
import {
  ModelCountry,
  ModelCounty,
  ModelState,
  IModelCounty,
  IModelState,
} from '../../models';
import { Types } from 'mongoose';

@injectable()
export class ImplBndBaseLocations implements IBndBaseLocations {
  public async getCountries(search: string): Promise<IBaseEntityCountries[]> {
    try {
      const searchObject = {};
      if (search) {
        searchObject['name'] = {
          $regex: search,
          $options: 'i',
        };
      }
      const countries = await ModelCountry.find(searchObject).select({
        name: 1,
        _id: 1,
        code: 1,
      });
      return countries.map((c) => {
        return {
          id: c._id.toHexString(),
          name: c.name,
          code: c.code,
        };
      });
    } catch (error) {
      throw new BaseError(
        `An error occurred while trying to get the countries. ERROR ${error}`,
        500,
        'GenerateDBError. ImplBndBaseLocations | getCountries',
        error
      );
    }
  }
  public async getStates(search: string): Promise<IBaseEntityStates[]> {
    try {
      const searchObject = {};
      if (search) {
        searchObject['name'] = {
          $regex: search,
          $options: 'i',
        };
      }
      const states = await ModelState.find(searchObject)
        .select({ name: 1, _id: 1, code: 1 })
        .sort('name');

      return states.map((s) => {
        return {
          id: s._id,
          name: s.name,
          code: s.code,
        };
      });
    } catch (error) {
      throw new BaseError(
        `An error occurred while trying to get the States. ERROR ${error}`,
        500,
        'GenerateDBError. ImplBndBaseLocations | getStates',
        error
      );
    }
  }
  public async getCountiesByState(
    searchData: IBaseEntityCountyParam
  ): Promise<IBaseEntityCounties[]> {
    try {
      const stateExist = !!(await ModelState.countDocuments({
        _id: searchData.stateId,
      }));
      if (!stateExist) {
        throw new BaseError(
          'ID State Not Found',
          404,
          `GenerateDBError. ImplBndBaseLocations | getCountiesByState`
        );
      }
      const countySearchObjet = {
        state: Types.ObjectId(searchData.stateId),
      };
      if (searchData.search) {
        countySearchObjet['name'] = {
          $regex: searchData.search,
          $options: 'i',
        };
      }
      const counties = await ModelCounty.find(countySearchObjet)
        .select({ name: 1, _id: 1, fips: 1 })
        .sort('name');

      return counties.map((c) => {
        return {
          id: c._id,
          name: c.name,
          fips: c.fips,
        };
      });
    } catch (error) {
      throw new BaseError(error.message, error.code, error.name, error);
    }
  }
  public async getCountyByNameAndState(
    name: string,
    state: string
  ): Promise<IBaseEntityCounties> {
    try {
      const county: IModelCounty = await ModelCounty.findOne({ name, state });
      if (!county) {
        return null;
      }
      return {
        name: county.name,
        fips: county.fips,
        state: `${county.state}`,
        id: county.id,
      };
    } catch (error) {
      throw new BaseError(error.message, error.code, error.name, error);
    }
  }

  public async getStateByName(name: string): Promise<IBaseEntityStates> {
    try {
      const state: IModelState = await ModelState.findOne({ name });
      if (!state) {
        return null;
      }
      return {
        id: state.id,
        name: state.name,
        code: state.code,
      };
    } catch (error) {
      throw new BaseError(error.message, error.code, error.name, error);
    }
  }
}
