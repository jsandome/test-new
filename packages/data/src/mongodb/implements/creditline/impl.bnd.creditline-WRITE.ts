import { injectable } from 'inversify';
import {
  BaseError,
  IBndCreditlineWrite,
  IBndReturnCreate,
  IExtraPayments,
  IParamsCreateCreditline,
  IParamsUpdateCreditline,
  IPropertyCreditlineShare,
} from '@capa/core';
import { ModelProperty } from '../../models';
import { startSession } from 'mongoose';
import {
  IModelCreditline,
  IModelCreditlineProperty,
  ModelCreditline,
  ModelCreditlineProperty,
} from '../../models';
import Bignumber from 'bignumber.js';

@injectable()
export class ImplBndBaseCreditlineWrite implements IBndCreditlineWrite {
  public async createCreditLine(
    params: IParamsCreateCreditline
  ): Promise<IBndReturnCreate> {
    const { name, principal, interest, extraPayments, properties } = params;
    const session = await startSession();
    session.startTransaction();

    try {
      await this._validPercentagesCreditline(properties);
      const createdCreditine: IModelCreditline = await ModelCreditline.findOne({
        name,
        active: true,
      });
      if (createdCreditine) {
        throw new BaseError(
          `Error, Credit Line already exists`,
          409,
          'BaseUseCaseCreditline | updateCreditline'
        );
      }

      const creditline: IModelCreditline = new ModelCreditline({
        name,
        principal,
        interest,
        extraPayments,
        active: true,
      });
      await creditline.save({ session });

      for (let element of properties) {
        const property = await ModelProperty.findById(element.id);
        const creditlineProperty: IModelCreditlineProperty =
          new ModelCreditlineProperty({
            creditline: creditline._id,
            property: property._id,
            share: element.share,
          });

        await creditlineProperty.save({ session });
      }

      await session.commitTransaction();
      session.endSession();

      return { id: creditline._id };
    } catch (error) {
      await session.abortTransaction();
      session.endSession();

      throw new BaseError(error.message, error.code, error.name, error);
    }
  }

  private async _validPercentagesCreditline(
    properties: IPropertyCreditlineShare[]
  ): Promise<boolean> {
    try {
      let sumPercentage = new Bignumber(0);

      for (let e of properties) {
        sumPercentage = sumPercentage.plus(e.share);

        const creditlinesProperty: IModelCreditlineProperty[] =
          await ModelCreditlineProperty.find({
            property: e.id,
          });

        let shareByProperty = new Bignumber(0);
        for (let clp of creditlinesProperty) {
          shareByProperty = shareByProperty.plus(clp.share);
        }
        shareByProperty = shareByProperty.plus(e.share);
        if (shareByProperty.isGreaterThan(100)) {
          throw new BaseError(
            'The share for this Property exceeds the 100% of percentage',
            409,
            'validPercentages'
          );
        }
      }

      if (sumPercentage.isLessThan(100) || sumPercentage.isGreaterThan(100)) {
        throw new BaseError(
          'The property share value should be 100',
          409,
          'validPercentages'
        );
      }

      return true;
    } catch (error) {
      throw new BaseError(error.message, error.code, error.name, error);
    }
  }

  public async updateCreditline(
    creditlineId: string,
    params: IParamsUpdateCreditline
  ): Promise<void> {
    const { name, principal, interest } = params;
    const extraPayments: IExtraPayments[] = params.extraPayments;
    const properties: IPropertyCreditlineShare[] = params.properties;
    const session = await startSession();
    session.startTransaction();

    try {
      const creditline: IModelCreditline = await ModelCreditline.findById(
        creditlineId
      );
      if (!creditline) {
        throw new BaseError(
          `Credit line not found.`,
          404,
          'BaseUseCaseCreditline | updateCreditline'
        );
      }

      const createdCreditline: IModelCreditline = await ModelCreditline.findOne(
        {
          _id: { $ne: creditlineId },
          name,
          active: true,
        }
      );

      if (createdCreditline) {
        throw new BaseError(
          `You can't update a Credit line with same name: ${name}`,
          404,
          'BaseUseCaseCreditline | updateCreditline'
        );
      }

      const creditlineProperties: IModelCreditlineProperty[] =
        await ModelCreditlineProperty.find({
          creditline: creditline._id,
        });
      const deletedCreditlineProperties: string[] = creditlineProperties.map(
        (e) => {
          if (properties.indexOf(e.id) === -1) {
            return e.id;
          }
        }
      );

      for (let p of properties) {
        const clp: IModelCreditlineProperty =
          await ModelCreditlineProperty.findOne({
            creditline: creditline._id,
            property: p.id,
          });

        if (clp) {
          clp.share = p.share;
          await clp.save({ session });
        }
      }

      await ModelCreditlineProperty.deleteMany(
        {
          in: deletedCreditlineProperties,
        },
        { session }
      );

      creditline.name = name;
      creditline.principal = principal;
      creditline.interest = interest;
      creditline.extraPayments = extraPayments;
      await creditline.save({ session });

      await session.commitTransaction();
      session.endSession();
    } catch (error) {
      await session.abortTransaction();
      session.endSession();
      throw new BaseError(error.message, error.code, error.name, error);
    }
  }

  public async deleteCreditline(creditlineId: string): Promise<void> {
    const session = await startSession();
    session.startTransaction();

    try {
      const creditline: IModelCreditline = await ModelCreditline.findById(
        creditlineId
      );

      if (!creditline) {
        throw new BaseError(
          'Credit line not found.',
          404,
          'BaseUseCaseCreditline | deleteCreditline'
        );
      }

      const creditlineProperty: IModelCreditlineProperty =
        await ModelCreditlineProperty.findOne({
          creditline: creditline._id,
        });

      if (!creditlineProperty) {
        throw new BaseError(
          'Credit line not found.',
          404,
          'BaseUseCaseCreditline | deleteCreditline'
        );
      }

      await creditlineProperty.deleteOne({ session });
      await creditline.deleteOne({ session });

      await session.commitTransaction();
      session.endSession();
    } catch (error) {
      await session.abortTransaction();
      session.endSession();

      throw new BaseError(error.message, error.code, error.name, error);
    }
  }
}
