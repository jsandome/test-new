import {
  IBndCreditlineRead,
  BaseError,
  IBaseEntityCreditline,
} from '@capa/core';
import { injectable } from 'inversify';

import { IModelCreditline, ModelCreditline } from '../../models';

@injectable()
export class ImplBndBaseCreditlineRead implements IBndCreditlineRead {
  public async selectCreditlines(
    search: string
  ): Promise<IBaseEntityCreditline[]> {
    try {
      const query = {};
      if (search) {
        query['name'] = {
          $regex: search,
          $options: 'i',
        };
      }

      const creditline: IModelCreditline[] = await ModelCreditline.find(query);
      const cls = creditline.map((e) => {
        return {
          id: e._id,
          name: e.name,
          principal: e.principal,
          interest: e.interest,
          extraPayments: e.extraPayments,
          active: e.active,
        } as IBaseEntityCreditline;
      });

      return cls;
    } catch (error) {
      throw new BaseError(
        `Error obtaining the creditline.${error.message} `,
        error.code,
        'BaseCreditLineController | selectCreditlines',
        error
      );
    }
  }
}
