import {
  BaseError,
  IBndBaseDistributionWrite,
  IBndReturnCreateDistribution,
  IBndReturnDeleteDistribution,
  IBndReturnUpdateDistribution,
  IDistributionAccount,
  IParamsCreateDistribution,
  IParamsUpdateDistribution,
} from '@capa/core';
import { injectable } from 'inversify';
import { startSession } from 'mongoose';
import {
  IModelDistribution,
  IModelDistributionInvestor,
  IModelLlc,
  ModelDistribution,
  ModelDistributionInvestor,
  ModelLlc,
} from '../../models';
import Bignumber from 'bignumber.js';

@injectable()
export class ImplBndDistributionWrite implements IBndBaseDistributionWrite {
  public async createNewDistribution(
    llcId: string,
    dataDistribution: IParamsCreateDistribution
  ): Promise<IBndReturnCreateDistribution> {
    const session = await startSession();
    session.startTransaction();

    try {
      const { description, distributionDate, yieldPeriod, account } =
        dataDistribution;
      const llc: IModelLlc = await ModelLlc.findOne({
        _id: llcId,
        active: true,
      });
      if (!llc) {
        throw new BaseError(
          'Entity not found.',
          404,
          'BaseUseCaseDistribution | createNewDistribution'
        );
      }

      const newDistribution: IModelDistribution = new ModelDistribution({
        llc: llcId,
        description,
        distributionDate,
        yieldPeriod,
      });
      await newDistribution.save({ session });

      for (let inv of account) {
        const newDistributionInvestor: IModelDistributionInvestor =
          new ModelDistributionInvestor({
            paymentType: (inv as IDistributionAccount).paymentType,
            paymentNumber: (inv as IDistributionAccount).paymentNumber,
            amount: (inv as IDistributionAccount).amount,
            investor: (inv as IDistributionAccount).id,
            distribution: newDistribution.id,
          });
        await newDistributionInvestor.save({ session });
      }
      await session.commitTransaction();
      session.endSession();

      return { id: newDistribution.id };
    } catch (error) {
      await session.abortTransaction();
      session.endSession();
      throw new BaseError(error.message, error.code, error.name, error);
    }
  }

  public async updateDistribution(
    llcId: string,
    distributionId: string,
    dataDistribution: IParamsUpdateDistribution
  ): Promise<IBndReturnUpdateDistribution> {
    const session = await startSession();
    session.startTransaction();

    try {
      const { description, distributionDate, yieldPeriod, account } =
        dataDistribution;
      const llc: IModelLlc = await ModelLlc.findOne({
        _id: llcId,
        active: true,
      });
      if (!llc) {
        throw new BaseError(
          'Entity not found.',
          404,
          'BaseUseCaseContribution | updateDistribution'
        );
      }

      const distribution: IModelDistribution = await ModelDistribution.findOne({
        _id: distributionId,
        'deletedBy.dateTime': null,
      });
      if (!distribution) {
        throw new BaseError(
          'Distribution not found.',
          404,
          'BaseUseCaseDistribution | updateDistribution'
        );
      }

      distribution.description = description;
      distribution.distributionDate = distributionDate;
      distribution.yieldPeriod = yieldPeriod;
      await distribution.save({ session });

      if (account && account.length) {
        for (let newInvestor of account) {
          const distributionInv: IModelDistributionInvestor =
            await ModelDistributionInvestor.findOne({
              distribution: distribution.id,
              investor: newInvestor.id,
              'deletedBy.dateTime': null,
            });

          if (distributionInv) {
            distributionInv.paymentType = newInvestor.paymentType;
            distributionInv.paymentNumber = newInvestor.paymentNumber;
            distributionInv.amount = newInvestor.amount;
            await distributionInv.save({ session });
          } else {
            const newDistributionInv: IModelDistributionInvestor =
              new ModelDistributionInvestor({
                distribution: distribution.id,
                investor: newInvestor.id,
                paymentType: newInvestor.paymentType,
                paymentNumber: newInvestor.paymentNumber,
                amount: newInvestor.amount,
              });
            await newDistributionInv.save({ session });
          }
        }

        const toDeleteDistrs: IModelDistributionInvestor[] =
          await ModelDistributionInvestor.find({
            distribution: distribution.id,
            investor: { $nin: account.map((e) => e.id) },
            'deletedBy.dateTime': null,
          });

        for (let toDelete of toDeleteDistrs)
          await toDelete.deleteOne({ session });
      }

      await session.commitTransaction();
      session.endSession();

      return { id: distribution.id };
    } catch (error) {
      await session.abortTransaction();
      session.endSession();
      throw new BaseError(error.message, error.code, error.name, error);
    }
  }

  public async deleteDistribution(
    llcId: string,
    distributionId: string,
    userId: string
  ): Promise<IBndReturnDeleteDistribution> {
    const session = await startSession();
    session.startTransaction();

    try {
      const llc: IModelLlc = await ModelLlc.findOne({
        _id: llcId,
        active: true,
      });
      if (!llc) {
        throw new BaseError(
          'Entity not found.',
          404,
          'BaseUseCaseContribution | deleteDistribution'
        );
      }

      const distribution: IModelDistribution = await ModelDistribution.findOne({
        _id: distributionId,
        active: true,
      });

      const distributionInvestor: IModelDistributionInvestor =
        await ModelDistributionInvestor.findOne({
          distribution: distributionId,
        });

      if (!distribution || !distributionInvestor) {
        throw new BaseError(
          'Distribution not found.',
          404,
          'BaseUseCaseDistribution | deleteDistribution'
        );
      }

      distribution.deletedBy.dateTime = new Date();
      distribution.deletedBy.id = userId;
      distribution.active = false;
      await distribution.save({ session });

      const distributionInvs: IModelDistributionInvestor[] =
        await ModelDistributionInvestor.find({
          distribution: distribution.id,
          'deletedBy.dateTime': null,
        });
      for (let distr of distributionInvs) {
        distr.deletedBy.dateTime = new Date();
        distr.deletedBy.id = userId;
        distr.amount = new Bignumber(distr.amount).toNumber();
        await distr.save({ session });
      }

      await session.commitTransaction();
      session.endSession();

      return {};
    } catch (error) {
      await session.abortTransaction();
      session.endSession();
      throw new BaseError(error.message, error.code, error.name, error);
    }
  }
}
