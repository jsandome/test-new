import {
  BaseError,
  IBndBaseDistributionRead,
  IBndReturnDetailDistribution,
  IBndReturnListDistribution,
  IBndReturnListDistributionDetail,
  IModelPaginate,
  IPaginateParams,
} from '@capa/core';
import {
  IModelInvestor,
  IModelDistribution,
  IModelDistributionInvestor,
  IModelLlc,
  ModelDistribution,
  ModelDistributionInvestor,
  ModelLlc,
  IModelPayment,
  ModelPayment,
} from '../../models';
import { injectable } from 'inversify';
import Bignumber from 'bignumber.js';

@injectable()
export class ImplBndDistributionRead implements IBndBaseDistributionRead {
  public async listDistributions(
    llcId: string,
    { limit = 10, page = 1 }: IPaginateParams,
    search: string
  ): Promise<IBndReturnListDistribution> {
    try {
      const llc: IModelLlc = await ModelLlc.findOne({
        _id: llcId,
        active: true,
      });
      if (!llc) {
        throw new BaseError(
          'Entity not found.',
          404,
          'BaseUseCaseDistribution | getListDistributions'
        );
      }

      const query = {
        'deletedBy.dateTime': null,
      };
      if (search) {
        query['description'] = {
          $regex: search,
          $options: 'i',
        };
      }

      const distributions: IModelPaginate<IModelDistribution[]> =
        await ModelDistribution.paginate(
          {
            llc: llcId,
            active: true,
            'deletedBy.dateTime': null,
          },
          {
            limit,
            page,
          }
        );

      let distributionsData: IBndReturnListDistributionDetail[] = [];
      for (let d of distributions.docs) {
        let parties = 0;
        let totalDistribution = new Bignumber(0);
        let totalPayment = new Bignumber(0);
        const distributionInvs: IModelDistributionInvestor[] =
          await ModelDistributionInvestor.find({
            distribution: d.id,
            'deletedBy.dateTime': null,
          });

        for (let distr of distributionInvs) {
          parties += 1;
          totalDistribution = totalDistribution.plus(distr.amount);

          const payments: IModelPayment[] = await ModelPayment.find({
            distributionInvestor: distr.id,
            'deletedBy.dateTime': null,
          });
          for (let p of payments) totalPayment = totalPayment.plus(p.amount);
        }

        distributionsData.push({
          id: d.id,
          distributionDate: d.distributionDate,
          description: d.description,
          yieldPeriod: d.yieldPeriod,
          parties,
          totalDistribution: totalDistribution.toNumber(),
          totalPaymentAmount: totalPayment.toNumber(),
          totalOutstanding: totalDistribution.minus(totalPayment).toNumber(),
        });
      }

      return {
        pagination: {
          itemCount: distributions.totalDocs,
          offset: (page - 1) * limit,
          limit: distributions.limit,
          totalPages: distributions.totalPages,
          page: distributions.page,
          nextPage: distributions.nextPage,
          prevPage: distributions.prevPage,
        },
        details: distributionsData,
      };
    } catch (error) {
      throw new BaseError(error.message, error.code, error.name, error);
    }
  }

  public async detailDistributions(
    llcId: string,
    distributionId: string
  ): Promise<IBndReturnDetailDistribution> {
    try {
      const llc: IModelLlc = await ModelLlc.findOne({
        _id: llcId,
        active: true,
      });
      if (!llc) {
        throw new BaseError(
          'Entity not found.',
          404,
          'BaseUseCaseContribution | detailDistribution'
        );
      }

      const distribution: IModelDistribution = await ModelDistribution.findOne({
        _id: distributionId,
        'deletedBy.dateTime': null,
      });
      if (!distribution) {
        throw new BaseError(
          'Distribution not found.',
          404,
          'BaseUseCaseDistribution | detailDistribution'
        );
      }

      const distributionInv: IModelDistributionInvestor[] =
        await ModelDistributionInvestor.find({
          distribution: distribution.id,
          'deletedBy.dateTime': null,
        }).populate({ path: 'investor' });

      return {
        description: distribution.description,
        distributionDate: distribution.distributionDate,
        yieldPeriod: distribution.yieldPeriod,
        account: distributionInv.map((e) => {
          return {
            id: e.id,
            name: `${(e.investor as IModelInvestor).name.firstName} ${
              (e.investor as IModelInvestor).name.lastName
            }`,
            amount: new Bignumber(e.amount).toNumber(),
            paymentNumber: e.paymentNumber,
            paymentType: e.paymentType,
          };
        }),
      };
    } catch (error) {
      throw new BaseError(error.message, error.code, error.name, error);
    }
  }
}
