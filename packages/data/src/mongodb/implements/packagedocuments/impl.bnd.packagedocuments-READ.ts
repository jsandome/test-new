import { injectable } from 'inversify';
import {
  BaseError,
  IBndPackageDocumentsRead,
  IBndResponseListPackageDocuments
} from '@capa/core';
import { IModelPackageDocuments, ModelPackageDocuments } from '../../models';
import { isEmpty, isNil, isNaN, pick } from 'lodash';

@injectable()
export class ImplBndBasePackageDocumentsRead implements IBndPackageDocumentsRead {
  public async checkIfNameExists(
    name: string,
    packageId: string
  ): Promise<Boolean> {
    try {
      const packagedocuments = await ModelPackageDocuments.findOne( { "name": name } ).collation({locale: "en", strength: 2});
      if(packageId === "") {
        return !isEmpty(packagedocuments);
      }
      return !(isEmpty(packagedocuments) || isNil(packagedocuments) || isNaN(packagedocuments))
      && packagedocuments._id.toHexString() !== packageId;
    } catch (error) {
      throw new BaseError(
        error.message,
        error.code,
        error.name,
        error as Error
      );
    }
  }
  public async listPackageDocuments(): Promise<IBndResponseListPackageDocuments[]>{
    try {
      const packagedocuments: IModelPackageDocuments[] = await ModelPackageDocuments.find({ deletedBy: null });
      return packagedocuments.map(result => {
        return {
          id: result.id,
          name: result.name,
          templates: result.templates.map(element => pick(element, ['templateId', 'name', 'date'])),
        };
      });
    } catch (error) {
      throw new BaseError(
        error.message,
        error.code,
        error.name,
        error as Error
      );
    }
  }
  public async getPackageDocumentsById(packageId: string): Promise<IBndResponseListPackageDocuments> {
    try {
      const packagedocuments: IModelPackageDocuments = await ModelPackageDocuments.findOne({ _id: packageId });
      if (isEmpty(packagedocuments) || isNil(packagedocuments) || isNaN(packagedocuments))
        throw new BaseError(
          'Package not found.',
          404,
          'ImplBndBasePackageDocumentsRead | getPackageDocumentsById'
        );
      return {
        id: packagedocuments._id.toHexString(),
        name: packagedocuments.name,
        templates: packagedocuments.templates.map(element => pick(element, ['templateId', 'name', 'date'])),
      };
    } catch (error) {
      throw new BaseError(
        error.message,
        error.code,
        error.name,
        error as Error
      );
    }
  }
  public async checkIfExistsPackageDocuments(packageId: string): Promise<Boolean> {
    try {
      const packagedocuments = await ModelPackageDocuments.findById(packageId);
      return isEmpty(packagedocuments) || isNil(packagedocuments) || isNaN(packagedocuments);
    } catch (error) {
      throw new BaseError(
        error.message,
        error.code,
        error.name,
        error as Error
      );
    }
  }
}
