import { injectable, inject } from 'inversify';
import {
  IBndPackageDocumentsWrite,
  IBndResponseCreatePackageDocuments,
  IParamsCreatePackageDocuments,
  BaseError,
  BaseUseCaseDocuSign,
  IParamsUpdatePackageDocuments,
  ListTemplates,
  IBaseTemplateDocusing,
  EnumTypeStepOnboarding
} from '@capa/core';
import { IModelPackageDocuments, 
  ModelPackageDocuments,
  ModelOnboarding } from '../../models';
import { startSession } from 'mongoose';
import { DateTime } from 'luxon';
import { isEmpty, isNil, isNaN, get as getPathOr } from 'lodash';


@injectable()
export class ImplBndBasePackageDocumentsWrite implements IBndPackageDocumentsWrite {
  @inject('BaseUseCaseDocuSign') private ucDocuSign: BaseUseCaseDocuSign;
  public async createPackageDocuments(
    params: IParamsCreatePackageDocuments
  ):Promise<IBndResponseCreatePackageDocuments> {
    const { name, templates, userRequest } = params;
    const session = await startSession();
    session.startTransaction();
    try {
      const templatesInDoc = await this.ucDocuSign.listTemplates();
      const templetesWithExtraData = [];
      for (let index = 0; index < templatesInDoc.length; index++) {
        const element = templatesInDoc[index];
        if(templates.includes(element['templateId'])) {
          templetesWithExtraData.push({
            name: element['name'],
            templateId: element['templateId'],
            date: element['date']
          });
        }
      }
      if(templetesWithExtraData.length !== templates.length) {
        throw new BaseError(
          'Error creating a new PackageDocuments',
          404,
          'Docusing Templete not Found'
        );
      }
      const newPackDocument: IModelPackageDocuments = new ModelPackageDocuments({
        name,
        templates: templetesWithExtraData,
        createdBy: {
          id: userRequest,
          dateTime: DateTime.now().toISODate(),
        },
        updatedBy: {
          id: userRequest,         
          dateTime: DateTime.now().toISODate(),
        }
      });
      await newPackDocument.save({ session });
      await session.commitTransaction();
      session.endSession();
      return { id: newPackDocument._id.toHexString() };
    } catch (error) {
      await session.abortTransaction();
      session.endSession();
      throw new BaseError(
        'Error creating a new PackageDocuments',
        error.code,
        error.name,
        error as Error
      );
    }
  }
  public async deletePackageDocuments(packageDocumentId: string, userDeleteId: string): Promise<void> {
    const session = await startSession();
    session.startTransaction();
    try {
      //TODO add funtion to check and allow or not the delete action
      const packagedocuments: IModelPackageDocuments = await ModelPackageDocuments.findOne({ _id: packageDocumentId });
      if (isEmpty(packagedocuments) || isNil(packagedocuments) || isNaN(packagedocuments)) {
        throw new BaseError(
          'PackageDocuments not found.',
          404,
          'ImplBndBasePackageDocumentsWrite | deletePackageDocuments'
        );
      }
      const countElemets = await ModelOnboarding.countDocuments({packageDocumentId: packageDocumentId});
      if(countElemets > 0) {
        const onProccessOnboarding = await ModelOnboarding.countDocuments({packageDocumentId: packageDocumentId,
          currentStep: { $nin: [ EnumTypeStepOnboarding.FINISHED ] }});
        if(onProccessOnboarding > 0) {
          throw new BaseError(
            'Exist Onboarding in process.',
            409,
            'ImplBndBasePackageDocumentsWrite | deletePackageDocuments'
          );
        } else {
          const existPreviousDelete = getPathOr(packagedocuments, ["deletedBy", "id"], null);
          if(isEmpty(existPreviousDelete)) {
            packagedocuments.deletedBy = {
              id: userDeleteId,
              dateTime: DateTime.now().toJSDate(),
            };
            await packagedocuments.save({ session });
          }
        }
      } else {
        await packagedocuments.deleteOne({ session });
      }
      await session.commitTransaction();
      session.endSession();
    } catch (error) {
      await session.abortTransaction();
      session.endSession();
      throw new BaseError(
        error.message,
        error.code,
        error.name,
        error
      );
    }
  }
  private checkIsInTemplateDocusing(origin: string, templatesInDoc: ListTemplates[], templates: string[]): IBaseTemplateDocusing[] {
    let templatesWithExtraData = [];
    for (let index = 0; index < templatesInDoc.length; index++) {
      const element = templatesInDoc[index];
      if(templates.includes(element['templateId'])) {
        templatesWithExtraData.push({
          name: element['name'],
          templateId: element['templateId'],
          date: element['date']
        });
      }
    }
    if(templatesWithExtraData.length !== templates.length) {
      throw new BaseError(
        `Error ${origin} PackageDocuments`,
        404,
        'Docusing Templete not Found'
      );
    }
    return templatesWithExtraData;
  }
  public async updatePackageDocuments(packageDocumentId: string, updateParams: IParamsUpdatePackageDocuments): Promise<void> {
    const session = await startSession();
    session.startTransaction();
    try {
      const templatesInDoc = await this.ucDocuSign.listTemplates();
      const review = await ModelPackageDocuments.findById(packageDocumentId);
      const arrayOfId = review.templates.map(elements => elements.templateId);
      let notDuplicateArray = [];
      if(updateParams.newTemplates) {
        notDuplicateArray = updateParams.newTemplates.filter( ( el ) => !arrayOfId.includes( el ) );
        if(notDuplicateArray.length > 0) {
          const respCheck = this.checkIsInTemplateDocusing('update a', templatesInDoc, notDuplicateArray);
          await ModelPackageDocuments.findByIdAndUpdate(packageDocumentId, { "$addToSet": { "templates": respCheck } }, { new: true, upsert: true, session });
        }
      }
      if(updateParams.deleteTemplates) {
        const totalIds = arrayOfId.concat(notDuplicateArray);
        const afterRemove = totalIds.filter( ( el ) => updateParams.deleteTemplates.includes( el ) );
        if(afterRemove.length === totalIds.length) {
          throw new BaseError(
            'PackageDocusing Templetes could not be Empty',
            409,
            'Error in update a PackageDocuments'
          );
        } else {
          await ModelPackageDocuments.findOneAndUpdate({_id:packageDocumentId}, { $pull: {templates: { templateId:{ $in: updateParams.deleteTemplates } } } }, { session } );
        }
      }
      await ModelPackageDocuments.updateOne({ _id: packageDocumentId }, { "name":  updateParams.name}, { session });      
      await session.commitTransaction();
      session.endSession();
    } catch (error) {
      await session.abortTransaction();
      session.endSession();
      throw new BaseError(
        `An error occurred while trying to update a packagedocuments: ${error.message}`,
        error.code,
        error.name,
        error
      );
    }
  }
}