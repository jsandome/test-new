import { injectable } from 'inversify';
import { BaseError, IBndInvestorRead, IEntityInvestor } from '@capa/core';
import { IModelInvestor, IModelUser, ModelInvestor } from '../models';
import { ModelUser } from '../models/impl.user';

@injectable()
export class ImplBndInvestorReadMongo implements IBndInvestorRead {
  public async getById(id: string): Promise<IEntityInvestor> {
    try {
      const investor: IModelInvestor = await ModelInvestor.findById(id).exec();
      const user: IModelUser = await ModelUser.findOne({ _id: investor.user });

      if (!investor || !user) return null;

      const res = investor.toObject();
      return {
        id: res._id,
        name: res.name,
        phone: res.phone,
        active: res.active,
        type: user.type,
      };
    } catch (error) {
      throw new BaseError(
        `An error occurred while trying to get an investor. ERROR ${error}`,
        500,
        'GenerateDBError',
        error
      );
    }
  }
}
