import { inject, injectable } from 'inversify';
import mongoose from 'mongoose';
import { BASETYPES, IBaseLogger, IMongoEnvironment } from '@capa/core';

@injectable()
export class MongoConfig {
  constructor(
    @inject(BASETYPES.Log) private _log: IBaseLogger,
    @inject(BASETYPES.IMongoConfig) private _config: IMongoEnvironment
  ) {}

  public async initConnection(): Promise<typeof mongoose> {
    const { dbUser, dbPass, dbName, dbUrl } = this._config;
    try {
      if (dbUser && dbPass && dbName) {
        const conn = await mongoose.connect(dbUrl, {
          useNewUrlParser: true,
          useFindAndModify: false,
          useCreateIndex: true,
          useUnifiedTopology: true,
          connectTimeoutMS: 15000,
          socketTimeoutMS: 10000,
          poolSize: 10,
          user: dbUser,
          pass: dbPass,
          dbName,
        });
        this._log.info(
          `the database connection to ${dbUrl} ${dbName} has been successfully`
        );
        return conn;
      } else {
        const conn = await mongoose.connect(dbUrl, {
          useNewUrlParser: true,
          useFindAndModify: false,
          useCreateIndex: true,
          useUnifiedTopology: true,
          connectTimeoutMS: 15000,
          socketTimeoutMS: 10000,
          poolSize: 10,
        });
        this._log.info(
          `the database connection to ${dbUrl} ${dbName} has been successfully`
        );
        return conn;
      }
    } catch (err) {
      this._log.error(err);
      this._log.error(`Error connecting with database ${dbUrl}`);
      return process.exit(1);
    }
  }

  public setAutoReconnect() {
    mongoose.connection.on('disconnected', () => {
      this.initConnection();
    });
  }

  public setEvents() {
    mongoose.connection.on('connected', () => {
      this._log.info(
        `Mongoose default connection is open to ${this._config.dbUrl}`
      );
    });

    mongoose.connection.on('error', (err) => {
      this._log.info(`Mongoose default connection has occured ${err} error`);
    });

    mongoose.connection.on('disconnected', () => {
      this._log.info('Mongoose default connection is disconnected');
    });

    process.on('SIGINT', () => {
      mongoose.connection.close(() => {
        this._log.info(
          'Mongoose default connection is disconnected due to application termination'
        );
        process.exit(0);
      });
    });
  }

  public async disconnect() {
    await mongoose.connection.close();
  }
}
