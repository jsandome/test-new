import supertest from 'supertest';
import FactoryGirl from 'factory-girl';

export {};
declare global {
  type Logger = {
    info: (msg: any) => void;
    error: (msg: any) => void;
    debug: (msg: any) => void;
    warning: (msg: any) => void;
  };
  namespace NodeJS {
    interface Global {
      logger: Logger;
      testServer: supertest.SuperAgentTest;
      adminPass: string;
      adminPassHash: string;
      cookieSessionId: string;
      cookieSecure: boolean;
      investorPass: string;
      investorPassHash: string;
      factory: FactoryGirl;
    }
  }
}
