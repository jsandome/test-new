import {
  ModelCapitalCallInvestor,
  IModelCapitalCallInvestor,
} from '@capa/data';
import faker from 'faker';

export const FCapitalCallInvestor = async (
  capitalCall: string,
  investor: string
): Promise<IModelCapitalCallInvestor> => {
  const capitalcallInvestor: IModelCapitalCallInvestor =
    new ModelCapitalCallInvestor({
      capitalAmount: faker.datatype.number(10000),
      capitalCall,
      investor,
    });

  return await capitalcallInvestor.save();
};
