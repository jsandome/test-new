import { EnumPropertyTypes } from '@capa/core';
import {
  ModelProperty,
  ModelPropertyFinancial,
  ModelPropertyFinancialQuarter,
  IModelProperty,
  IModelFund,
  IModelPropertyFinancial,
  IModelPropertyFinancialQuarter,
  ModelFund,
  IModelFundProperty,
  ModelFundProperty,
} from '@capa/data';
import { FPackage, FPackageProperty } from './';
import faker from 'faker';
import { DateTime } from 'luxon';

export const between = (min, max) => {
  return Math.floor(Math.random() * (max - min) + min);
};

export const getEnumKey = (obj): string => {
  const keys = Object.keys(obj);
  const index = between(0, keys.length - 1);
  return keys[index];
};

/**
 * Generate random Property data to use in unit testing
 * @returns Return an IModelProperty
 * @returns Return an IModelPropertyFinancial
 * @returns Return an IModelPropertyQuarter
 */
export const FProperty = async (
  state = '60c7bd3e9af6ecdf0eed781d',
  county = '60c7bd3e9af6ecdf0eed781e'
): Promise<
  [IModelProperty, IModelPropertyFinancial, IModelPropertyFinancialQuarter]
> => {
  const dateNow = DateTime.now();
  const property: IModelProperty = new ModelProperty({
    name: 'String',
    address:
      'Amsterdam 173-int. 3, Hipódromo, Cuauhtémoc, 06100 Ciudad de México, CDMX',
    type: 'Commercial',
    state,
    county,
    bedRooms: 1,
    bathRooms: 1,
    size: 1,
    parcelSize: 1,
    parcelNumber: 1,
    strategies: {
      buyAndHold: faker.datatype.boolean(),
      developAndHold: faker.datatype.boolean(),
      developAndSell: faker.datatype.boolean(),
      inmediateSell: faker.datatype.boolean(),
      repositionAndHold: faker.datatype.boolean(),
      repositionAndSell: faker.datatype.boolean(),
    },
    propertyDescription: faker.datatype.string(25),
    pictures: [],
    mainPicture: { url: faker.image.imageUrl() },
    location: {
      lat: faker.address.latitude(),
      lng: faker.address.longitude(),
    },
    isComplete: true,
    investors: [],
  });
  await property.save();

  const propertyFinancial: IModelPropertyFinancial = new ModelPropertyFinancial(
    {
      property: property._id,
      aquisitionFee: faker.datatype.number(100),
      dispositionFee: faker.datatype.number(100),
      amountAllocatedToLand: faker.datatype.number(100),
      depreciation: faker.datatype.number(100),
      commissions: faker.datatype.number(100),
      downPayment: faker.datatype.number(100),
      capitalGainsTaxPaid: faker.datatype.number(100),
      salesPrice: {
        year1: faker.datatype.number(100),
        year2: faker.datatype.number(100),
        year3: faker.datatype.number(100),
        year4: faker.datatype.number(100),
        year5: faker.datatype.number(100),
      },
      year: dateNow.year,
      quarter: dateNow.quarter,
      totalValue: faker.datatype.number(100),
      initialInvestment: faker.datatype.number(100),
      purchasePrice: faker.datatype.number(100),
      totalUnits: faker.datatype.number(5),
    }
  );
  await propertyFinancial.save();

  const propertyQuarter: IModelPropertyFinancialQuarter =
    new ModelPropertyFinancialQuarter({
      year: dateNow.year,
      quarter: dateNow.quarter,
      totalUnits: 1,
      rents: {
        month1: 1,
        month2: 1,
        month3: 1,
      },
      propertyManagement: 0,
      assetManagement: 0,
      repairsMaintenance: 0,
      taxes: 0,
      insurance: 0,
      reserve: 0,
      vacancy: 0,
      operativeExpenses: [
        {
          name: 'String',
          total: 0,
        },
      ],
    });

  property.propertyFinancial = propertyFinancial._id;
  propertyFinancial.property = property._id;

  propertyQuarter.propertyFinancial = propertyFinancial._id;
  await property.save();
  await propertyFinancial.save();
  await propertyQuarter.save();

  return [property, propertyFinancial, propertyQuarter];
};

/**
 * Generate random Property data to use in unit testing
 * @returns Return an IModelProperty
 */
export const insertPropertyWithoutFinancial = async (name?: string) => {
  const property: IModelProperty = new ModelProperty({
    name: name ? name : faker.datatype.string(),
    address: `Amsterdam 173-int. ${faker.datatype.number({
      min: 0,
      max: 300,
    })}, Hipódromo, Cuauhtémoc, 06100 Ciudad de México, CDMX`,
    type: EnumPropertyTypes[getEnumKey(EnumPropertyTypes)],
    state: '60c7bd3e9af6ecdf0eed781d',
    county: '60c7bd3e9af6ecdf0eed781e',
    bedRooms: 1,
    bathRooms: 1,
    size: 1,
    parcelSize: 1,
    parcelNumber: 1,
    strategies: {
      buyAndHold: faker.datatype.boolean(),
      developAndHold: faker.datatype.boolean(),
      developAndSell: faker.datatype.boolean(),
      inmediateSell: faker.datatype.boolean(),
      repositionAndHold: faker.datatype.boolean(),
      repositionAndSell: faker.datatype.boolean(),
    },
    propertyDescription: faker.datatype.string(25),
    pictures: [{ url: faker.image.imageUrl() }],
    mainPicture: { url: faker.image.imageUrl() },
    location: {
      lat: faker.address.latitude(),
      lng: faker.address.longitude(),
    },
    isComplete: false,
    investors: [],
  });
  await property.save();

  return property as IModelProperty;
};

export const insertPropertyWithPackage = async () => {
  const newProperty: IModelProperty = await insertPropertyWithoutFinancial();
  const newPackage = await FPackage();
  const packageProperty = await FPackageProperty(
    newPackage._id.toHexString(),
    newProperty._id.toHexString()
  );
  return [newProperty, newPackage, packageProperty];
};

export const insertPropertyWithFund = async () => {
  const newProperty: IModelProperty = await insertPropertyWithoutFinancial();
  const newFund: IModelFund = new ModelFund({
    name: faker.name.firstName(),
    active: true,
    cashHand: faker.datatype.number(100),
    investors: [],
    properties: [newProperty._id.toHexString()],
    packages: [],
  });
  const fundsPrperty: IModelFundProperty = new ModelFundProperty({
    fund: newFund.id,
    property: newProperty.id,
    share: faker.datatype.number(100),
  });
  await newProperty.save();
  await newFund.save();
  await fundsPrperty.save();

  return [newProperty, newFund, fundsPrperty];
};

//Property preview

export const expectedEstrategiesResultPreview1 = [
  {
    year: 3,
    totalAnnualROI: 41.07855079805669,
  },
  {
    year: 4,
    totalAnnualROI: 35.42958824125817,
  },
  {
    year: 5,
    totalAnnualROI: 32.331899708097986,
  },
  {
    year: 6,
    totalAnnualROI: 30.515711037135734,
  },
  {
    year: 7,
    totalAnnualROI: 29.439749644622612,
  },
];

export const propertyPreviewBody1 = {
  operativeInfo: [
    {
      year: 1,
      units: 1,
      rent: 12000,
      vacancy: 0,
      propertyManagement: 1200,
      assetManagement: 1800,
      taxes: 2620,
      insurance: 717,
      repairs: 500,
      reserve: 500,
      salesPrice: 0,
    },
    {
      year: 2,
      units: 1,
      rent: 12600,
      vacancy: 315,
      propertyManagement: 1260,
      assetManagement: 1800,
      taxes: 2620,
      insurance: 717,
      repairs: 500,
      reserve: 500,
      salesPrice: 0,
    },
    {
      year: 3,
      units: 1,
      rent: 13200,
      vacancy: 330,
      propertyManagement: 1320,
      assetManagement: 1800,
      taxes: 2699,
      insurance: 739,
      repairs: 500,
      reserve: 500,
      salesPrice: 150000,
    },
    {
      year: 4,
      units: 1,
      rent: 13596,
      vacancy: 340,
      propertyManagement: 1360,
      assetManagement: 1800,
      taxes: 2780,
      insurance: 761,
      repairs: 500,
      reserve: 500,
      salesPrice: 154500,
    },
    {
      year: 5,
      units: 1,
      rent: 14004,
      vacancy: 350,
      propertyManagement: 1400,
      assetManagement: 1800,
      taxes: 2863,
      insurance: 783,
      repairs: 500,
      reserve: 500,
      salesPrice: 159135,
    },
    {
      year: 6,
      units: 1,
      rent: 14424,
      vacancy: 361,
      propertyManagement: 1442,
      assetManagement: 1800,
      taxes: 2949,
      insurance: 807,
      repairs: 500,
      reserve: 500,
      salesPrice: 163909,
    },
    {
      year: 7,
      units: 1,
      rent: 14857,
      vacancy: 371,
      propertyManagement: 1486,
      assetManagement: 1800,
      taxes: 3037,
      insurance: 831,
      repairs: 500,
      reserve: 500,
      salesPrice: 168826,
    },
  ],
  aquisitionFee: 3175,
  dispositionFee: 1,
  amountAllocatedToLand: 10000,
  depreciation: 27.5,
  commissions: 7,
  downPayment: 27000,
  purchasePrice: 90000,
  totalValue: 100000,
  extraExpenses: [],
  capitalGainsTaxPaid: 27,
  loans: [
    {
      amount: 63000,
      interest: 4,
      termYears: 10,
      paymentsPerYear: 12,
      amortizationYears: 20,
      ltc: 70,
    },
  ],
};
