import { ModelCreditlineProperty, IModelCreditlineProperty } from '@capa/data';
import { Types } from 'mongoose';

export const FCreditlineProperty = async (
  properties: Types.ObjectId[],
  creditline: Types.ObjectId,
  share: number
): Promise<IModelCreditlineProperty[]> => {
  const allCreditlineProperties: IModelCreditlineProperty[] = [];

  for (let p of properties) {
    const creditlineProperty: IModelCreditlineProperty =
      new ModelCreditlineProperty({
        creditline,
        property: p,
        share,
      });
    const clp = await creditlineProperty.save();
    allCreditlineProperties.push(clp);
  }

  return allCreditlineProperties;
};
