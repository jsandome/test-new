import { IModelContribution, ModelContribution } from '@capa/data';
import faker from 'faker';
import { IEnumTypesPayment } from '@capa/core';

export const FContribution = async (
  capitalCallInvestor: string
): Promise<IModelContribution> => {
  const contribution: IModelContribution = new ModelContribution({
    receivedDate: new Date().toISOString(),
    paymentAmount: faker.datatype.number(1000),
    paymentMethod: IEnumTypesPayment.WIRE,
    isCompleted: false,
    capitalCallInvestor,
    checkNumber: faker.datatype.number(100),
    notes: faker.name.firstName(),
  });

  return await contribution.save();
};
