import { ModelContact, IModelContact } from '@capa/data';
import faker from 'faker';

export const FContact = async (state: string, county: string) => {
  const contact: IModelContact = new ModelContact({
    name: faker.name.firstName(),
    jobTitle: faker.name.jobDescriptor(),
    companyName: faker.name.jobType(),
    currentJob: faker.datatype.boolean(),
    email: faker.internet.email(),
    phone: faker.phone.phoneNumberFormat(),
    address: faker.address.direction(),
    city: faker.address.city(),
    state,
    county,
    zip: faker.address.zipCode(),
  });

  return await contact.save();
};
