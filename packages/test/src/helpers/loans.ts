import { IModelLoan, ModelLoan, ModelLoanProperty } from '@capa/data';
import faker from 'faker';
import { insertPropertyWithoutFinancial } from './';
import Bignumber from 'bignumber.js';

export const insertLoan = async (name?: string) => {
  const property = await insertPropertyWithoutFinancial();
  const loan: IModelLoan = new ModelLoan({
    name: name ? name : faker.datatype.string(),
    amount: faker.datatype.number({ max: 10000, min: 1, precision: 0.11 }),
    interest: faker.datatype.number({ max: 100, min: 0.1, precision: 0.1 }),
    amortizationYears: faker.datatype.number({ max: 50, min: 1, precision: 1 }),
    termYears: faker.datatype.number({ max: 50, min: 1, precision: 1 }),
    beginningDate: '2021-03-31T22:25:39+0000',
    paymentsPerYear: faker.datatype.number({ max: 50, min: 1, precision: 1 }),
    extraPayments: [
      {
        name: 'Extra payment',
        amount: faker.datatype.number({ max: 10000, min: 1, precision: 0.1 }),
        paymentNumbers: [1],
      },
    ],
    properties: null,
  });
  const loanProperty = new ModelLoanProperty({
    property: property._id,
    loan: loan._id,
    payments: [
      {
        firstPayment: 1,
        finalPayment: new Bignumber(loan.amortizationYears).multipliedBy(
          loan.paymentsPerYear
        ),
        share: 100,
      },
    ],
  });
  loan.properties = [loanProperty._id];
  loanProperty.save();
  await loan.save();
  return [loan, property];
};
