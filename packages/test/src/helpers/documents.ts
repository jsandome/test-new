import { EnumGCloudDocumentsStatus } from '@capa/core';
import { IModelGCloudDocument, ModelGCloudDocument } from '@capa/data';
import { DateTime } from 'luxon';

export const createDocument = async (
  id: string
): Promise<IModelGCloudDocument> => {
  const document: IModelGCloudDocument = ModelGCloudDocument({
    originalName: 'Letter Test.pdf',
    status: EnumGCloudDocumentsStatus.SUCCESS,
    url: '/documents',
    taxCenter: true,
    createdBy: {
      id,
      dateTime: DateTime.now(),
    },
  });
  document.url = `${document.url}/${document.id}.pdf`;
  await document.save();
  return document;
};
