import {
  ModelProperty,
  ModelPropertyFinancial,
  ModelPropertyFinancialQuarter,
  ModelPropertyInvestor,
  ModelFund,
  ModelFundProperty,
  ModelPackage,
  ModelPackageProperty,
  ModelPackageInvestor,
  ModelCountry,
  ModelCounty,
  ModelState,
  ModelLoan,
  ModelLoanProperty,
  ModelLlc,
  ModelLlcInvestor,
  ModelLlcProperty,
  ModelGCloudDocument,
  ModelPackageDocuments
} from '@capa/data';

export const cleanAllModels = async () => {
  await cleanProperty();
  await cleanFunds();
  await ModelGCloudDocument.deleteMany();
};

export const cleanProperty = async () => {
  await ModelProperty.deleteMany();
  await ModelPropertyFinancial.deleteMany();
  await ModelPropertyFinancialQuarter.deleteMany();
  await ModelPropertyInvestor.deleteMany();
};

export const cleanFunds = async () => {
  await ModelFund.deleteMany();
  await ModelFundProperty.deleteMany();
};

export const cleanPackage = async () => {
  await ModelPackage.deleteMany();
  await ModelPackageProperty.deleteMany();
  await ModelPackageInvestor.deleteMany();
};

export const cleanLocationsData = async () => {
  await ModelCountry.remove({});
  await ModelCounty.remove({});
  await ModelState.remove({});
};

export const cleanLoans = async () => {
  await ModelLoan.deleteMany();
  await ModelLoanProperty.deleteMany();
};

export const cleanLLC = async () => {
  await ModelLlc.deleteMany();
  await ModelLlcInvestor.deleteMany({});
  await ModelLlcProperty.deleteMany({});
};

export const cleanDocumentsPackage = async () => {
  await ModelPackageDocuments.deleteMany({});
};
