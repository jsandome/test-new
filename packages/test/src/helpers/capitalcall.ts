import { ModelCapitalCall, IModelCapitalCall } from '@capa/data';
import faker from 'faker';

export const FCapitalCall = async (llc: string): Promise<IModelCapitalCall> => {
  const capitalcall: IModelCapitalCall = new ModelCapitalCall({
    description: faker.name.firstName(),
    dueDate: new Date(),
    companyName: faker.name.jobType(),
    llc,
  });

  return await capitalcall.save();
};
