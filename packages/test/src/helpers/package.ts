import {
  ModelPackage,
  ModelPackageInvestor,
  ModelPackageProperty,
  IModelPackage,
  IModelPackageInvestor,
  IModelPackageProperty,
} from '@capa/data';
import faker from 'faker';

export const FPackage = async () => {
  const pack: IModelPackage = new ModelPackage({
    name: faker.random.words(20),
    totalValue: faker.datatype.number(1000),
    active: true,
  });

  return await pack.save();
}

export const FPackageProperty = async (packageId: string, property: string) => {
  const packProperty: IModelPackageProperty = new ModelPackageProperty({
    package: packageId,
    property,
    share: 100,
  });

  return await packProperty.save();
}

export const FPackageInvestor = async (packageId: string, investor: string) => {
  const packInvestor: IModelPackageInvestor = new ModelPackageInvestor({
    package: packageId,
    investor,
    minCommitment: faker.datatype.number(1000),
    initCommitment: faker.datatype.number(1000),
    totalCommitment: faker.datatype.number(1000),
    termYears: faker.datatype.number(10),
  });

  return await packInvestor.save();
}
  