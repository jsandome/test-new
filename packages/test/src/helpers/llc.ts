import {
  ModelProperty,
  IModelProperty,
  IModelLlcInvestor,
  IModelLlcProperty,
  ModelUser,
  ModelInvestor,
  IModelInvestor,
  IModelUser,
  IModelLlc,
  ModelLlc,
  ModelLlcInvestor,
  ModelLlcProperty,
} from '@capa/data';
import faker from 'faker';
import { EnumUserTypes } from '@capa/core';

export const factoryLlcs = async () => {
  const email = faker.internet.email();

  const user: IModelUser = new ModelUser({
    admin: null,
    investor: null,
    password: global.adminPassHash,
    type: EnumUserTypes.INVESTOR,
    email,
    active: true,
  });
  await user.save();
  const investor: IModelInvestor = new ModelInvestor({
    user: user.id,
    name: {
      firstName: faker.name.firstName(),
      lastName: faker.name.lastName(),
    },
    type: EnumUserTypes.INVESTOR,
  });
  await investor.save();
  user.investor = investor.id;
  await user.save();

  const property: IModelProperty = new ModelProperty({
    name: 'String',
    address:
      'Amsterdam 173-int. 3, Hipódromo, Cuauhtémoc, 06100 Ciudad de México, CDMX',
    type: 'Commercial',
    state: '60c7bd3e9af6ecdf0eed781d',
    county: '60c7bd3e9af6ecdf0eed781e',
    bedRooms: 1,
    bathRooms: 1,
    size: 1,
    parcelSize: 1,
    parcelNumber: 1,
    strategies: {
      buyAndHold: faker.datatype.boolean(),
      developAndHold: faker.datatype.boolean(),
      developAndSell: faker.datatype.boolean(),
      inmediateSell: faker.datatype.boolean(),
      repositionAndHold: faker.datatype.boolean(),
      repositionAndSell: faker.datatype.boolean(),
    },
    propertyDescription: faker.datatype.string(25),
    pictures: [],
    mainPicture: faker.image.imageUrl(),
    location: {
      lat: faker.address.latitude(),
      lng: faker.address.longitude(),
    },
    isComplete: true,
    investors: [],
  });
  await property.save();

  const llc: IModelLlc = new ModelLlc({
    name: `${faker.name.firstName()} ${faker.name.lastName()}`,
    usPerson: faker.datatype.boolean(),
    taxId: faker.datatype.uuid(),
    address: faker.address.direction(),
    state: '60c7bd3e9af6ecdf0eed7854',
    date: new Date(),
  });
  await llc.save();

  const llcsInvestor: IModelLlcInvestor = new ModelLlcInvestor({
    llc: llc._id,
    investor: investor.id,
  });
  await llcsInvestor.save();

  const llcsProperty: IModelLlcProperty = new ModelLlcProperty({
    llc: llc._id,
    property: property.id,
    share: faker.datatype.number(100),
  });

  llcsInvestor.investor = investor.id;
  await llcsInvestor.save();

  llcsProperty.llc = llc._id;
  llcsProperty.property = property.id;
  await llcsProperty.save();

  return llc;
};

export const factoryLLCProperty = async (llcId: string, property: string) => {
  const llcProperty: IModelLlcProperty = new ModelLlcProperty({
    llc: llcId,
    property,
    share: 100,
  });

  return await llcProperty.save();
};

export const factoryLLCInvestor = async (llcId: string, investor: string) => {
  const llcInvestor: IModelLlcInvestor = new ModelLlcInvestor({
    llc: llcId,
    investor,
  });

  return await llcInvestor.save();
};
