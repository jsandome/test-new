import {
  IModelState,
  IModelCounty,
  ModelCountry,
  ModelCounty,
  ModelState,
} from '@capa/data';
import faker from 'faker';

export const Countries = [
  new ModelCountry({
    _id: '60de25148a635bc4febe6618',
    name: 'Estados Unidos Mexicanos',
    code: 'EM',
  }),
  new ModelCountry({
    _id: '60de25148a635bc4febe6619',
    name: 'United States Of America',
    code: 'AM',
  }),
  new ModelCountry({
    _id: '60de25148a635bc4febe661a',
    name: 'Alemania',
    code: 'AL',
  }),
  new ModelCountry({
    _id: '60de25148a635bc4febe661b',
    name: 'State of Japon',
    code: 'SJ',
  }),
];
export const States = [
  new ModelState({
    _id: '60de259053bf64c56cc9a7a3',
    name: 'State of California',
    code: 'TC',
  }),
  new ModelState({
    _id: '60de259053bf64c56cc9a7a4',
    name: 'State of New York',
    code: 'TY',
  }),
  new ModelState({
    _id: '60de259053bf64c56cc9a7a5',
    name: 'State of New Mexico',
    code: 'TM',
  }),
  new ModelState({
    _id: '60de259053bf64c56cc9a7a6',
    name: 'State of Washington',
    code: 'TW',
  }),
];

export const Counties = [
  new ModelCounty({
    _id: '60c7bd3e9af6ecdf0eed781c',
    name: 'County Los Angeles',
    fips: 7038,
    state: '60de259053bf64c56cc9a7a3',
  }),
  new ModelCounty({
    _id: '60c7bd3e9af6ecdf0eed7823',
    name: 'County of San Diego',
    fips: 7073,
    state: '60de259053bf64c56cc9a7a3',
  }),
  new ModelCounty({
    _id: '60c7bd3e9af6ecdf0eed7824',
    name: 'County Orange',
    fips: 65659,
    state: '60de259053bf64c56cc9a7a3',
  }),
  new ModelCounty({
    _id: '60c7bd3e9af6ecdf0eed782a',
    name: 'County Riverside',
    fips: 760665,
    state: '60de259053bf64c56cc9a7a3',
  }),
  new ModelCounty({
    _id: '60c7bd3e9af6ecdf0eed7829',
    name: 'County Kings',
    fips: 63047,
    state: '60de259053bf64c56cc9a7a4',
  }),
  new ModelCounty({
    _id: '60c7bd3e9af6ecdf0eed782b',
    name: 'County Queens',
    fips: 63081,
    state: '60de259053bf64c56cc9a7a4',
  }),
  new ModelCounty({
    _id: '60c7bd3e9af6ecdf0eed7838',
    name: 'County New York',
    fips: 63061,
    state: '60de259053bf64c56cc9a7a4',
  }),
  new ModelCounty({
    _id: '60c7bd3e9af6ecdf0eed783e',
    name: 'County Suffolk',
    fips: 63103,
    state: '60de259053bf64c56cc9a7a4',
  }),
  new ModelCounty({
    _id: '60c7bd3e9af6ecdf0eed7a06',
    name: 'County Santa Fe',
    fips: 53049,
    state: '60de259053bf64c56cc9a7a5',
  }),
  new ModelCounty({
    _id: '60c7bd3e9af6ecdf0eed7a11',
    name: 'County Sandoval',
    fips: 53043,
    state: '60de259053bf64c56cc9a7a5',
  }),
  new ModelCounty({
    _id: '60c7bd3e9af6ecdf0eed7a3d',
    name: 'County San Juan',
    fips: 53045,
    state: '60de259053bf64c56cc9a7a5',
  }),
  new ModelCounty({
    _id: '60c7bd3e9af6ecdf0eed7b26',
    name: 'County Valencia',
    fips: 53061,
    state: '60de259053bf64c56cc9a7a5',
  }),
  new ModelCounty({
    _id: '60c7bd3e9af6ecdf0eed7a7f',
    name: 'County Cowlitz',
    fips: 35015,
    state: '60de259053bf64c56cc9a7a6',
  }),
  new ModelCounty({
    _id: '60c7bd3e9af6ecdf0eed7ab8',
    name: 'County Grant',
    fips: 35025,
    state: '60de259053bf64c56cc9a7a6',
  }),
  new ModelCounty({
    _id: '60c7bd3e9af6ecdf0eed7acb',
    name: 'County Franklin',
    fips: 35021,
    state: '60de259053bf64c56cc9a7a6',
  }),
  new ModelCounty({
    _id: '60c7bd3e9af6ecdf0eed7afa',
    name: 'County Island',
    fips: 35029,
    state: '60de259053bf64c56cc9a7a6',
  }),
];

export const insertLocations = async () => {
  await ModelCountry.insertMany(Countries);
  await ModelState.insertMany(States);
  await ModelCounty.insertMany(Counties);
};

export const insertStateAndCounty = async () => {
  const state: IModelState = new ModelState({
    name: faker.address.state(),
    code: faker.address.stateAbbr(),
  });
  const county: IModelCounty = new ModelCounty({
    name: faker.address.county(),
    fips: faker.datatype.number({ min: 1000, max: 9999, precision: 1 }),
    state: state._id,
  });
  await state.save();
  await county.save();
  return { state, county };
};
