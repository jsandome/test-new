import {
  ModelProperty,
  ModelFund,
  ModelFundInvestor,
  ModelFundProperty,
  IModelProperty,
  IModelFund,
  IModelFundProperty,
  IModelFundInvestor,
  ModelUser,
  ModelInvestor,
  IModelInvestor,
  IModelUser,
  ModelPackage,
  IModelPackage,
  IModelFundPackage,
  ModelFundPackage,
} from '@capa/data';
import faker from 'faker';
import { EnumUserTypes } from '@capa/core';

export const Ffunds = async () => {
  const email = faker.internet.email();

  const user: IModelUser = new ModelUser({
    admin: null,
    investor: null,
    password: global.adminPassHash,
    type: EnumUserTypes.INVESTOR,
    email,
    active: true,
  });
  await user.save();
  const investor: IModelInvestor = new ModelInvestor({
    user: user.id,
    name: {
      firstName: faker.name.firstName(),
      lastName: faker.name.lastName(),
    },
    type: EnumUserTypes.INVESTOR,
  });
  await investor.save();
  user.investor = investor.id;
  await user.save();

  const property: IModelProperty = new ModelProperty({
    name: 'String',
    address:
      'Amsterdam 173-int. 3, Hipódromo, Cuauhtémoc, 06100 Ciudad de México, CDMX',
    type: 'Commercial',
    state: '60c7bd3e9af6ecdf0eed781d',
    county: '60c7bd3e9af6ecdf0eed781e',
    bedRooms: 1,
    bathRooms: 1,
    size: 1,
    parcelSize: 1,
    parcelNumber: 1,
    strategies: {
      buyAndHold: faker.datatype.boolean(),
      developAndHold: faker.datatype.boolean(),
      developAndSell: faker.datatype.boolean(),
      inmediateSell: faker.datatype.boolean(),
      repositionAndHold: faker.datatype.boolean(),
      repositionAndSell: faker.datatype.boolean(),
    },
    propertyDescription: faker.datatype.string(25),
    pictures: [],
    mainPicture: {url:faker.image.imageUrl()},
    location: {
      lat: faker.address.latitude(),
      lng: faker.address.longitude(),
    },
    isComplete: true,
    investors: [],
  });
  await property.save();

  const fund: IModelFund = new ModelFund({
    name: faker.name.firstName(),
    active: true,
    cashHand: faker.datatype.number(100),
    investors: [],
    properties: [],
    packages: [],
  });
  await fund.save();

  const pack: IModelPackage = new ModelPackage({
    name: faker.name.firstName(),
    totalValue: faker.datatype.number(1000),
    active: true,
  });
  await pack.save();

  const fundsInvestor: IModelFundInvestor = new ModelFundInvestor({
    fund: fund.id,
    investor: investor.id,
    minCommitment: faker.datatype.number(100),
    initCommitment: faker.datatype.number(100),
    totalCommitment: faker.datatype.number(100),
    termYears: faker.datatype.number(),
  });
  await fundsInvestor.save();

  const fundsPrperty: IModelFundProperty = new ModelFundProperty({
    fund: fund.id,
    property: property.id,
    share: faker.datatype.number(100),
  });

  const fundPackage: IModelFundPackage = new ModelFundPackage({
    fund: fund.id,
    package: pack.id,
    share: faker.datatype.number(100),
  });
  await fundPackage.save();

  fundsInvestor.fund = fund.id;
  fundsInvestor.investor = investor.id;
  await fundsInvestor.save();

  fundsPrperty.fund = fund.id;
  fundsPrperty.property = property.id;
  await fundsPrperty.save();

  return fund;
};
