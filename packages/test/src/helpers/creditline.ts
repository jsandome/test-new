import { ModelCreditline, IModelCreditline } from '@capa/data';
import faker from 'faker';

export const FCreditline = async () => {
  const creditline: IModelCreditline = new ModelCreditline({
    name: faker.name.firstName(),
    principal: faker.datatype.number({ max: 10000 }),
    interest: faker.datatype.number({ max: 10000 }),
    extraPayments: [
      {
        name: faker.name.firstName(),
        date: new Date(),
        amount: faker.datatype.number({ max: 10000 }),
      },
    ],
    active: true,
  });

  return await creditline.save();
};
