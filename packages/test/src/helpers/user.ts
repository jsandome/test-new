import { EnumUserTypes, EnumEmailType, EnumPhoneType } from '@capa/core';
import {
  ModelUser,
  ModelAdministrator,
  ModelInvestor,
  IModelAdministrator,
  IModelInvestor,
  IModelUser,
} from '@capa/data';
import faker from 'faker';

// Create a random admin on DB
export const FUserAdmin = async (
  email: string
): Promise<[IModelUser, IModelAdministrator]> => {
  const user: IModelUser = new ModelUser({
    admin: null,
    investor: null,
    password: global.adminPassHash,
    type: EnumUserTypes.ADMIN,
    email,
    active: true,
  });
  await user.save();
  const admin: IModelAdministrator = new ModelAdministrator({
    user: user.id,
    name: {
      firstName: faker.name.firstName(),
      lastName: faker.name.lastName(),
    },
    type: EnumUserTypes.ADMIN,
  });
  await admin.save();
  user.admin = admin.id;
  await user.save();

  return [user, admin];
};

// Create a random Investor on DB
export const FUserInvestor = async (
  email: string
): Promise<[IModelUser, IModelInvestor]> => {
  const user: IModelUser = new ModelUser({
    admin: null,
    investor: null,
    password: global.adminPassHash,
    type: EnumUserTypes.INVESTOR,
    email,
    active: true,
  });
  await user.save();
  const investor: IModelInvestor = new ModelInvestor({
    user: user.id,
    name: {
      firstName: faker.name.firstName(),
      lastName: faker.name.lastName(),
    },
    phone: faker.phone.phoneNumber(),
    phoneType: EnumPhoneType.WORK,
    email: faker.phone.phoneNumber(),
    emailType: EnumEmailType.WORK,
    type: EnumUserTypes.INVESTOR,
    addresses: [],
    biographical: {},
  });
  await investor.save();
  user.investor = investor.id;
  await user.save();

  return [user, investor];
};
