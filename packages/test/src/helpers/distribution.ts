import { IEnumTypesPayment } from '@capa/core';
import {
  IModelDistribution,
  IModelDistributionInvestor,
  ModelDistribution,
  ModelDistributionInvestor,
} from '@capa/data';
import faker from 'faker';

export const FDistribution = async (
  llcId: string
): Promise<IModelDistribution> => {
  const distribution: IModelDistribution = new ModelDistribution({
    llc: llcId,
    description: faker.lorem.paragraph(1),
    distributionDate: new Date(),
    yieldPeriod: 'January 2021',
  });

  return await distribution.save();
};

export const FDistributionInvestor = async (
  distributionId: string,
  investorId: string
): Promise<IModelDistributionInvestor> => {
  const distributionInvestor: IModelDistributionInvestor =
    new ModelDistributionInvestor({
      distribution: distributionId,
      investor: investorId,
      paymentType: IEnumTypesPayment.WIRE,
      paymentNumber: 'Number-1234.',
      amount: faker.datatype.number(1000),
    });

  return await distributionInvestor.save();
};
