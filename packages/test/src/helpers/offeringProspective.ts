import {
  ModelOfferingProspective,
  IModelOfferingProspective,
} from '@capa/data';
import faker from 'faker';

export const FOfferingProspective = async (
  investor: string,
  offering: string,
  offeringType: string
): Promise<IModelOfferingProspective> => {
  const offeringProsp: IModelOfferingProspective = new ModelOfferingProspective(
    {
      investor,
      offeringType,
      offering,
      status: 'Contacted',
      expectedAmount: faker.datatype.number(1000),
      likeliHood: faker.datatype.number(1000),
      additionalProspect: [],
    }
  );

  return await offeringProsp.save();
};
