import 'reflect-metadata';
import 'mocha';
import supertest from 'supertest';
import FactoryGirl from 'factory-girl';

import { ServerApp } from '@capa/api';
import { EnumEnvironments } from '@capa/core';

before(async () => {
  const server = new ServerApp();
  const app = await server.listen();
  (
    global as {
      testServer: supertest.SuperAgentTest;
    }
  ).testServer = supertest.agent(app);
  while (!(global as { testServer?: any }).testServer)
    await new Promise((resolve) => setTimeout(resolve, 50));
  (global as { adminPass: string }).adminPass = 'Admin123.';
  (global as { adminPassHash: string }).adminPassHash =
    '$2b$10$ykcLvzgs96afDi9dFSfX.Oi4B2j4gufNVZQRlT3ryp79VxODZRgVq';
  (global as { cookieSessionId: string }).cookieSessionId = 'capaSession';
  (global as { cookieSecure: boolean }).cookieSecure =
    process.env.NODE_ENV === EnumEnvironments.TEST ? false : true;
  (global as { investorPass: string }).investorPass = 'Investor123.';
  (global as { investorPassHash: string }).investorPassHash =
    '$2b$10$1/4BQTesiDVYX1gCsXnjoukzWwrLEwTMC7JpRo1MbJVz8bqxuhsUO';

  (global as { factory: FactoryGirl }).factory = FactoryGirl;
  console.info('Starting API tests.');
});

after(async () => {
  console.info('Ending API tests.');
});
