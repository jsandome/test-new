import 'mocha';
import { expect } from 'chai';
import faker from 'faker';
import {
  IModelGCloudDocument,
  ModelAdministrator,
  ModelGCloudDocument,
  ModelUser,
} from '@capa/data';
import { EnumGCloudDocumentsStatus, EnumUserTypes } from '@capa/core';
import { createDocument } from '../helpers';

describe('Test DocuSign', async () => {
  it('should return list templates DocuSign ', async () => {
    const email = faker.internet.email();
    const user = new ModelUser({
      admin: null,
      investor: null,
      password: global.adminPassHash,
      type: EnumUserTypes.ADMIN,
      email,
      active: true,
    });
    await user.save();
    const admin = new ModelAdministrator({
      user: user.id,
      name: {
        firstName: faker.name.firstName(),
        lastName: faker.name.lastName(),
      },
      type: EnumUserTypes.ADMIN,
    });
    await admin.save();
    user.admin = admin.id;
    await user.save();

    await global.testServer
      .post(`/admin/login`)
      .send({
        user: email,
        password: global.adminPass,
      })
      .expect(200);

    const { body } = await global.testServer
      .get('/admin/document/docusign/list')
      .send()
      .expect(200);

    const { response } = body;
    expect(body).to.have.all.keys('response');
    response.forEach((a) => {
      expect(a).to.have.all.keys(
        'date',
        'description',
        'id',
        'templateId',
        'name'
      );
    });
  });
});

describe('Test Documents', async () => {
  describe('Should create document', async () => {
    it('POST /admin/document', async () => {
      const email = faker.internet.email();
      const user = new ModelUser({
        admin: null,
        investor: null,
        password: global.adminPassHash,
        type: EnumUserTypes.ADMIN,
        email,
        active: true,
      });
      await user.save();
      const admin = new ModelAdministrator({
        user: user.id,
        name: {
          firstName: faker.name.firstName(),
          lastName: faker.name.lastName(),
        },
        type: EnumUserTypes.ADMIN,
      });
      await admin.save();
      user.admin = admin.id;
      await user.save();

      await global.testServer
        .post(`/admin/login`)
        .send({
          user: email,
          password: global.adminPass,
        })
        .expect(200);

      const { body } = await global.testServer
        .post('/admin/document')
        .send({
          path: 'tmp/documents/Carta firmada Fernando.pdf',
          addTaxCenter: true,
        })
        .expect(200);

      expect(body).to.have.all.keys('response', 'message');
      const { response } = body;
      expect(response).to.have.all.keys('id', 'name');

      const createdDocument: IModelGCloudDocument =
        await ModelGCloudDocument.findOne({ _id: response.id });
      expect(createdDocument).to.not.be.equal(null);
      await createdDocument.deleteOne();
      await user.deleteOne();
      await admin.deleteOne();
    });
  });
  describe('Should delete a document', async () => {
    it('DELETE /admin/document', async () => {
      const email = faker.internet.email();
      const user = new ModelUser({
        admin: null,
        investor: null,
        password: global.adminPassHash,
        type: EnumUserTypes.ADMIN,
        email,
        active: true,
      });
      await user.save();
      const admin = new ModelAdministrator({
        user: user.id,
        name: {
          firstName: faker.name.firstName(),
          lastName: faker.name.lastName(),
        },
        type: EnumUserTypes.ADMIN,
      });
      await admin.save();
      user.admin = admin.id;
      await user.save();

      await global.testServer
        .post(`/admin/login`)
        .send({
          user: email,
          password: global.adminPass,
        })
        .expect(200);
      const newDocument: IModelGCloudDocument = await createDocument(user.id);
      const { body } = await global.testServer
        .delete(`/admin/document/${newDocument.id}`)
        .expect(200);

      expect(body).to.have.all.keys('response', 'message');
      const deletedDocument: IModelGCloudDocument =
        await ModelGCloudDocument.findOne({ _id: newDocument.id });
      expect(deletedDocument.status).to.be.equal(
        EnumGCloudDocumentsStatus.DELETED
      );

      await deletedDocument.deleteOne();
      await user.deleteOne();
      await admin.deleteOne();
    });
  });
  describe('Should select documents', async () => {
    it('GET /admin/document/select', async () => {
      const email = faker.internet.email();
      const user = new ModelUser({
        admin: null,
        investor: null,
        password: global.adminPassHash,
        type: EnumUserTypes.ADMIN,
        email,
        active: true,
      });
      await user.save();
      const admin = new ModelAdministrator({
        user: user.id,
        name: {
          firstName: faker.name.firstName(),
          lastName: faker.name.lastName(),
        },
        type: EnumUserTypes.ADMIN,
      });
      await admin.save();
      user.admin = admin.id;
      await user.save();

      await global.testServer
        .post(`/admin/login`)
        .send({
          user: email,
          password: global.adminPass,
        })
        .expect(200);

      const newDocument: IModelGCloudDocument = await createDocument(user.id);
      const { body } = await global.testServer
        .get(`/admin/document/select?search=${newDocument.originalName}`)
        .expect(200);
      expect(body).to.have.all.keys('response');
      const documents = body.response;
      for (const document of documents) {
        expect(document).to.have.all.keys('id', 'name');
      }
      await newDocument.deleteOne();
      await user.deleteOne();
      await admin.deleteOne();
    });
  });
  describe('Should list documents', async () => {
    it('GET /admin/document/list', async () => {
      const email = faker.internet.email();
      const user = new ModelUser({
        admin: null,
        investor: null,
        password: global.adminPassHash,
        type: EnumUserTypes.ADMIN,
        email,
        active: true,
      });
      await user.save();
      const admin = new ModelAdministrator({
        user: user.id,
        name: {
          firstName: faker.name.firstName(),
          lastName: faker.name.lastName(),
        },
        type: EnumUserTypes.ADMIN,
      });
      await admin.save();
      user.admin = admin.id;
      await user.save();

      await global.testServer
        .post(`/admin/login`)
        .send({
          user: email,
          password: global.adminPass,
        })
        .expect(200);

      await createDocument(user.id);
      await createDocument(user.id);
      let apiResponse = await global.testServer
        .get(`/admin/document/list`)
        .expect(200);
      let body = apiResponse.body;
      expect(body).to.have.all.keys('response');
      let response = body.response;
      expect(response).to.have.all.keys('pagination', 'details');
      expect(response.pagination).to.have.all.keys(
        'itemCount',
        'offset',
        'limit',
        'totalPages',
        'page',
        'nextPage',
        'prevPage'
      );
      const count = response.pagination.itemCount;
      for (var document of response.details) {
        expect(document).to.have.all.keys(
          'id',
          'name',
          'date',
          'url',
          'addTaxCenter'
        );
      }
      apiResponse = await global.testServer
        .get(`/admin/document/list?page=1&limit=1`)
        .expect(200);
      body = apiResponse.body;
      expect(body).to.have.all.keys('response');
      response = body.response;
      expect(response).to.have.all.keys('pagination', 'details');
      expect(response.pagination).to.have.all.keys(
        'itemCount',
        'offset',
        'limit',
        'totalPages',
        'page',
        'nextPage',
        'prevPage'
      );
      expect(count).to.be.equal(response.pagination.itemCount);
      await ModelGCloudDocument.deleteMany();
      await user.deleteOne();
      await admin.deleteOne();
    });
  });
});
