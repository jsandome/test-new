import {
  ModelCreditline,
  ModelUser,
  ModelAdministrator,
  ModelCreditlineProperty,
} from '@capa/data';
import {
  FProperty,
  FCreditline,
  FCreditlineProperty,
  FUserAdmin,
  insertPropertyWithoutFinancial,
} from '../helpers/';
import { expect } from 'chai';
import faker from 'faker';
import { EnumUserTypes } from '@capa/core';
import Bignumber from 'bignumber.js';

describe('Creditline Controller Test', () => {
  describe('POST /admin/credit-line', () => {
    it('should return ID of the created line', async () => {
      const email = faker.internet.email();
      const user = new ModelUser({
        admin: null,
        investor: null,
        password: global.adminPassHash,
        type: EnumUserTypes.ADMIN,
        email,
        active: true,
      });
      await user.save();
      const admin = new ModelAdministrator({
        user: user.id,
        name: {
          firstName: faker.name.firstName(),
          lastName: faker.name.lastName(),
        },
        type: EnumUserTypes.ADMIN,
      });
      await admin.save();
      user.admin = admin.id;
      await user.save();

      await global.testServer
        .post(`/admin/login`)
        .send({
          user: email,
          password: global.adminPass,
        })
        .expect(200);

      // Create property data and resolve promises
      const data = await FProperty();
      const [property, propertyFinancial, propertyQuarter] = await Promise.all(
        data
      );

      const name = faker.datatype.string(25);
      const { body } = await global.testServer
        .post(`/admin/credit-line`)
        .send({
          name,
          principal: faker.datatype.number(100),
          interest: faker.datatype.number(100),
          extraPayments: [
            {
              name: faker.datatype.string(25),
              date: new Date(),
              amount: faker.datatype.number(100),
            },
          ],
          properties: [
            {
              id: property._id,
              share: 100,
            },
          ],
        })
        .expect(200);
      const { response } = body;
      expect(response).to.have.all.keys('id');
      const creditline = await ModelCreditline.findById(response.id);
      const creditlineProperty = await ModelCreditlineProperty.findOne({
        creditline: creditline._id,
      });

      expect(creditline.name).to.equal(name);
      expect(creditlineProperty.property.toHexString()).to.equal(
        property._id.toHexString()
      );
      expect(creditlineProperty.creditline.toHexString()).to.equal(
        creditline._id.toHexString()
      );

      await ModelCreditline.deleteMany();
      await ModelCreditlineProperty.deleteMany();
      await propertyQuarter.deleteOne();
      await propertyFinancial.deleteOne();
      await property.deleteOne();
      await user.deleteOne();
      await admin.deleteOne();
    });

    it('should throw error (Percentage is not 100%)', async () => {
      const email = faker.internet.email();
      const user = new ModelUser({
        admin: null,
        investor: null,
        password: global.adminPassHash,
        type: EnumUserTypes.ADMIN,
        email,
        active: true,
      });
      await user.save();
      const admin = new ModelAdministrator({
        user: user.id,
        name: {
          firstName: faker.name.firstName(),
          lastName: faker.name.lastName(),
        },
        type: EnumUserTypes.ADMIN,
      });
      await admin.save();
      user.admin = admin.id;
      await user.save();

      await global.testServer
        .post(`/admin/login`)
        .send({
          user: email,
          password: global.adminPass,
        })
        .expect(200);

      // Create property data and resolve promises
      const data = await FProperty();
      const [property, propertyFinancial, propertyQuarter] = await Promise.all(
        data
      );

      const name = faker.datatype.string(25);
      await global.testServer
        .post(`/admin/credit-line`)
        .send({
          name,
          principal: faker.datatype.number(100),
          interest: faker.datatype.number(100),
          extraPayments: [
            {
              name: faker.datatype.string(25),
              date: new Date(),
              amount: faker.datatype.number(100),
            },
          ],
          properties: [
            {
              id: property._id,
              share: 75,
            },
          ],
        })
        .expect(409);

      await propertyQuarter.deleteOne();
      await propertyFinancial.deleteOne();
      await property.deleteOne();
      await user.deleteOne();
      await admin.deleteOne();
    });

    it('should throw error (Property share its used in other creditline)', async () => {
      const email = faker.internet.email();
      const anAdmin = await FUserAdmin(email);
      const [userAdmin, admin] = await Promise.all(anAdmin);
      const property1 = await insertPropertyWithoutFinancial();
      const [creditline] = await Promise.all([FCreditline()]);
      const [creditlineProperty] = await Promise.all([
        FCreditlineProperty([property1._id], creditline._id, 100),
      ]);

      await global.testServer
        .post(`/admin/login`)
        .send({
          user: email,
          password: global.adminPass,
        })
        .expect(200);

      const name = faker.datatype.string(25);
      await global.testServer
        .post(`/admin/credit-line`)
        .send({
          name,
          principal: faker.datatype.number(100),
          interest: faker.datatype.number(100),
          extraPayments: [
            {
              name: faker.datatype.string(25),
              date: new Date(),
              amount: faker.datatype.number(100),
            },
          ],
          properties: [
            {
              id: property1._id,
              share: 100,
            },
          ],
        })
        .expect(409);

      await userAdmin.deleteOne();
      await admin.deleteOne();
      await property1.deleteOne();
      await creditline.deleteOne();
      await creditlineProperty[0].deleteOne();
    });
  });

  describe('POST /admin/credit-line/preview', () => {
    it('should return preview of total extra payments for a created line', async () => {
      const email = faker.internet.email();
      const user = new ModelUser({
        admin: null,
        investor: null,
        password: global.adminPassHash,
        type: EnumUserTypes.ADMIN,
        email,
        active: true,
      });
      await user.save();
      const admin = new ModelAdministrator({
        user: user.id,
        name: {
          firstName: faker.name.firstName(),
          lastName: faker.name.lastName(),
        },
        type: EnumUserTypes.ADMIN,
      });
      await admin.save();
      user.admin = admin.id;
      await user.save();

      const [property, propertyFinancial, propertyQuarter] = await Promise.all(
        await FProperty()
      );

      await global.testServer
        .post(`/admin/login`)
        .send({
          user: email,
          password: global.adminPass,
        })
        .expect(200);

      const { body } = await global.testServer
        .post('/admin/credit-line/preview')
        .send({
          properties: [
            {
              id: property._id,
              share: 100,
            },
          ],
          extraPayments: [],
        })
        .expect(200);
      expect(body.response).to.have.all.keys('totalExtraPayments');

      await propertyQuarter.deleteOne();
      await propertyFinancial.deleteOne();
      await property.deleteOne();
      await user.deleteOne();
      await admin.deleteOne();
    });

    it('should return preview of total extra payments for a created line and some properties', async () => {
      const email = faker.internet.email();
      const user = new ModelUser({
        admin: null,
        investor: null,
        password: global.adminPassHash,
        type: EnumUserTypes.ADMIN,
        email,
        active: true,
      });
      await user.save();
      const admin = new ModelAdministrator({
        user: user.id,
        name: {
          firstName: faker.name.firstName(),
          lastName: faker.name.lastName(),
        },
        type: EnumUserTypes.ADMIN,
      });
      await admin.save();
      user.admin = admin.id;
      await user.save();

      const [property1, propertyFinancial1, propertyQuarter1] =
        await Promise.all(await FProperty());
      const [property2, propertyFinancial2, propertyQuarter2] =
        await Promise.all(await FProperty());

      await global.testServer
        .post(`/admin/login`)
        .send({
          user: email,
          password: global.adminPass,
        })
        .expect(200);

      const extra1 = faker.datatype.number(100);
      const extra2 = faker.datatype.number(100);

      const { body } = await global.testServer
        .post('/admin/credit-line/preview')
        .send({
          properties: [
            {
              id: property1._id,
              share: 30,
            },
            {
              id: property2._id,
              share: 70,
            },
          ],
          extraPayments: [
            {
              name: faker.name.firstName(),
              date: new Date(),
              amount: extra1,
            },
            {
              name: faker.name.firstName(),
              date: new Date(),
              amount: extra2,
            },
          ],
        })
        .expect(200);

      expect(body.response).to.have.all.keys('totalExtraPayments');
      expect(body.response.totalExtraPayments).to.equal(extra1 + extra2);

      await propertyQuarter1.deleteOne();
      await propertyFinancial1.deleteOne();
      await property1.deleteOne();
      await propertyQuarter2.deleteOne();
      await propertyFinancial2.deleteOne();
      await property2.deleteOne();
      await user.deleteOne();
      await admin.deleteOne();
    });
  });

  describe('GET /credit-line/select', () => {
    it('should return credit lines from the user', async () => {
      const email = faker.internet.email();
      const user = new ModelUser({
        admin: null,
        investor: null,
        password: global.adminPassHash,
        type: EnumUserTypes.ADMIN,
        email,
        active: true,
      });
      await user.save();
      const admin = new ModelAdministrator({
        user: user.id,
        name: {
          firstName: faker.name.firstName(),
          lastName: faker.name.lastName(),
        },
        type: EnumUserTypes.ADMIN,
      });
      await admin.save();
      user.admin = admin.id;
      await user.save();

      await global.testServer
        .post(`/admin/login`)
        .send({
          user: email,
          password: global.adminPass,
        })
        .expect(200);

      const [property, propertyFinancial, propertyQuarter] = await Promise.all(
        await FProperty()
      );

      const [creditline] = await Promise.all([FCreditline()]);
      const [creditlineProperty] = await Promise.all([
        FCreditlineProperty([property._id], creditline._id, 100),
      ]);

      const { body } = await global.testServer
        .get(`/credit-line/select`)
        .expect(200);
      const { response } = body;

      response.forEach((element) => {
        expect(element).to.have.all.keys(
          'id',
          'name',
          'principal',
          'interest',
          'extraPayments',
          'active'
        );
      });

      await creditlineProperty[0].deleteOne();
      await creditline.deleteOne();
      await propertyQuarter.deleteOne();
      await propertyFinancial.deleteOne();
      await property.deleteOne();
      await user.deleteOne();
      await admin.deleteOne();
    });
  });

  describe('PUT /admin/credit-line/:creditlineId', () => {
    it('should update credit line and add two properties', async () => {
      const email = faker.internet.email();
      const anAdmin = await FUserAdmin(email);
      const [userAdmin, admin] = await Promise.all(anAdmin);
      const property1 = await insertPropertyWithoutFinancial();
      const property2 = await insertPropertyWithoutFinancial();
      const property3 = await insertPropertyWithoutFinancial();

      await global.testServer
        .post(`/admin/login`)
        .send({
          user: email,
          password: global.adminPass,
        })
        .expect(200);

      const [creditline] = await Promise.all([FCreditline()]);
      const [creditlineProperty] = await Promise.all([
        FCreditlineProperty([property1._id], creditline._id, 100),
      ]);

      const dataUpdateCreditline = {
        name: faker.name.title(),
        principal: faker.datatype.number(100),
        interest: faker.datatype.number(100),
        extraPayments: [
          {
            name: faker.datatype.string(25),
            date: new Date(),
            amount: faker.datatype.number(100),
          },
        ],
        properties: [
          {
            id: property1._id,
            share: 40,
          },
          {
            id: property2._id,
            share: 30,
          },
          {
            id: property3._id,
            share: 30,
          },
        ],
      };

      await global.testServer
        .put(`/admin/credit-line/${creditline._id}`)
        .send(dataUpdateCreditline)
        .expect(200);

      const updatedCreditline = await ModelCreditline.findById(creditline._id);
      const updatedCreditlineProperty = await ModelCreditlineProperty.findById(
        creditlineProperty[0]._id
      );

      expect(updatedCreditline.name).to.equal(dataUpdateCreditline.name);
      expect(updatedCreditline.interest).to.equal(
        dataUpdateCreditline.interest
      );

      const principalBig = new Bignumber(updatedCreditline.principal);
      const principalF = parseFloat(principalBig.toNumber().toFixed(2));
      const shareBig = new Bignumber(updatedCreditlineProperty.share);
      const shareF = parseFloat(shareBig.toNumber().toFixed(2));

      expect(principalF).to.equal(dataUpdateCreditline.principal);
      expect(shareF).to.equal(dataUpdateCreditline.properties[0].share);

      // Remove two properties to the creditline
      dataUpdateCreditline.properties = [
        {
          id: property1._id,
          share: 100,
        },
      ];

      await global.testServer
        .put(`/admin/credit-line/${creditline._id}`)
        .send(dataUpdateCreditline)
        .expect(200);

      const newUpdatedCreditlineProperty1 =
        await ModelCreditlineProperty.findOne({
          creditline: creditline._id,
          property: property1._id,
        });
      const newUpdatedCreditlineProperty2 =
        await ModelCreditlineProperty.findOne({
          creditline: creditline._id,
          property: property2._id,
        });
      const newUpdatedCreditlineProperty3 =
        await ModelCreditlineProperty.findOne({
          creditline: creditline._id,
          property: property3._id,
        });

      expect(newUpdatedCreditlineProperty2).to.be.equal(null);
      expect(newUpdatedCreditlineProperty3).to.be.equal(null);

      const shareBig1 = new Bignumber(newUpdatedCreditlineProperty1.share);
      const shareF1 = parseFloat(shareBig1.toNumber().toFixed(2));
      expect(shareF1).to.be.equal(100);

      await creditlineProperty[0].deleteOne();
      await creditline.deleteOne();
      await property1.deleteOne();
      await property2.deleteOne();
      await property3.deleteOne();
      await userAdmin.deleteOne();
      await admin.deleteOne();
    });
  });

  describe('DELETE /admin/credit-line/:creditlineId', () => {
    it('should delete a creditline', async () => {
      const email = faker.internet.email();
      const user = new ModelUser({
        admin: null,
        investor: null,
        password: global.adminPassHash,
        type: EnumUserTypes.ADMIN,
        email,
        active: true,
      });
      await user.save();
      const admin = new ModelAdministrator({
        user: user.id,
        name: {
          firstName: faker.name.firstName(),
          lastName: faker.name.lastName(),
        },
        type: EnumUserTypes.ADMIN,
      });
      await admin.save();
      user.admin = admin.id;
      await user.save();

      await global.testServer
        .post(`/admin/login`)
        .send({
          user: email,
          password: global.adminPass,
        })
        .expect(200);

      const [property, propertyFinancial, propertyQuarter] = await Promise.all(
        await FProperty()
      );

      const [creditline] = await Promise.all([FCreditline()]);
      const [creditlineProperty] = await Promise.all([
        FCreditlineProperty([property._id], creditline._id, 100),
      ]);

      await global.testServer
        .delete(`/admin/credit-line/${creditline._id}`)
        .expect(200);

      const deletedCreditline = await ModelCreditline.findById(creditline._id);
      const deletedCreditlineProperty = await ModelCreditlineProperty.findById(
        creditlineProperty[0]._id
      );

      expect(deletedCreditline).to.equal(null);
      expect(deletedCreditlineProperty).to.equal(null);

      await user.deleteOne();
      await admin.deleteOne();
      await creditline.deleteOne();
      await creditlineProperty[0].deleteOne();
      await propertyQuarter.deleteOne();
      await propertyFinancial.deleteOne();
      await property.deleteOne();
    });

    it('throw exception try to delete an non-existent creditline', async () => {
      const email = faker.internet.email();
      const user = new ModelUser({
        admin: null,
        investor: null,
        password: global.adminPassHash,
        type: EnumUserTypes.ADMIN,
        email,
        active: true,
      });
      await user.save();
      const admin = new ModelAdministrator({
        user: user.id,
        name: {
          firstName: faker.name.firstName(),
          lastName: faker.name.lastName(),
        },
        type: EnumUserTypes.ADMIN,
      });
      await admin.save();
      user.admin = admin.id;
      await user.save();

      await global.testServer
        .post(`/admin/login`)
        .send({
          user: email,
          password: global.adminPass,
        })
        .expect(200);
      const creditlineId = '60f739a11f728f4d0083577d';

      await global.testServer
        .delete(`/admin/credit-line/${creditlineId}`)
        .expect(404);
    });
  });
});
