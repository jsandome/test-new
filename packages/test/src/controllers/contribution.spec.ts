import {
  factoryLlcs,
  FCapitalCall,
  FCapitalCallInvestor,
  FContribution,
  FUserAdmin,
  FUserInvestor,
} from '../helpers';
import faker from 'faker';
import {
  IModelCapitalCall,
  IModelCapitalCallInvestor,
  IModelContribution,
  ModelCapitalCallInvestor,
  ModelContribution,
} from '@capa/data';
import { IEnumTypesPayment } from '@capa/core';
import { expect } from 'chai';
import Bignumber from 'bignumber.js';

describe('Contributions Controller Test', () => {
  describe('POST /admin/llc/:llcId/contribution', () => {
    it('should create a new Contribution', async () => {
      const email = faker.internet.email();
      const emailInvestor = faker.internet.email();
      const anAdmin = await FUserAdmin(email);
      const anInvestor = await FUserInvestor(emailInvestor);
      const [userAdmin, admin] = await Promise.all(anAdmin);
      const [user, investor] = await Promise.all(anInvestor);
      const llc = await factoryLlcs();
      const capitalcall: IModelCapitalCall = await FCapitalCall(llc.id);
      const capitalcallInvestor: IModelCapitalCallInvestor =
        await FCapitalCallInvestor(capitalcall.id, investor.id);

      await global.testServer
        .post(`/admin/login`)
        .send({
          user: email,
          password: global.adminPass,
        })
        .expect(200);

      const dataContribution = {
        investor: investor.id,
        dateReceived: new Date(),
        capitalCall: {
          id: capitalcall.id,
          amount: faker.datatype.number(1000),
          paymentType: IEnumTypesPayment.WIRE,
          paymentNumber: faker.datatype.number(10),
          notes: faker.datatype.string(10),
        },
      };
      const { body } = await global.testServer
        .post(`/admin/llc/${llc.id}/contribution`)
        .send(dataContribution)
        .expect(200);
      expect(body.response).to.have.property('id');
      const newContribution = await ModelContribution.findById(
        body.response.id
      );
      expect(newContribution).not.to.be.null;

      await userAdmin.deleteOne();
      await admin.deleteOne();
      await user.deleteOne();
      await investor.deleteOne();
      await llc.deleteOne();
      await capitalcall.deleteOne();
      await capitalcallInvestor.deleteOne();
    });
  });

  describe('POST /admin/llc/:llcId/capital-call/:capitalCallId/contribution', () => {
    it('should record a new Contribution', async () => {
      const email = faker.internet.email();
      const emailInvestor = faker.internet.email();
      const anAdmin = await FUserAdmin(email);
      const anInvestor = await FUserInvestor(emailInvestor);
      const [userAdmin, admin] = await Promise.all(anAdmin);
      const [user, investor] = await Promise.all(anInvestor);
      const llc = await factoryLlcs();
      const capitalcall: IModelCapitalCall = await FCapitalCall(llc.id);
      const capitalcallInvestor: IModelCapitalCallInvestor =
        await FCapitalCallInvestor(capitalcall.id, investor.id);

      await global.testServer
        .post(`/admin/login`)
        .send({
          user: email,
          password: global.adminPass,
        })
        .expect(200);

      const dataContribution = {
        investorId: investor.id,
        amountContribution: faker.datatype.number(1000),
        paymentDate: new Date(),
        paymentType: IEnumTypesPayment.WIRE,
        paymentNumber: 'NUMBER-1234.',
        notes: faker.datatype.string(10),
      };
      await global.testServer
        .post(
          `/admin/llc/${llc.id}/capital-call/${capitalcall.id}/contribution`
        )
        .send(dataContribution)
        .expect(200);

      const capitalCallInvestor: IModelCapitalCallInvestor =
        await ModelCapitalCallInvestor.findOne({
          investor: investor.id,
          capitalCall: capitalcall.id,
        });
      const newContribution: IModelContribution =
        await ModelContribution.findOne({
          capitalCallInvestor: capitalCallInvestor.id,
        });
      expect(newContribution).not.to.be.null;

      await userAdmin.deleteOne();
      await admin.deleteOne();
      await user.deleteOne();
      await investor.deleteOne();
      await llc.deleteOne();
      await capitalcall.deleteOne();
      await capitalcallInvestor.deleteOne();
    });
  });

  describe('GET /admin/llc/:llcId/transactions/contribution', () => {
    it('should list all Contributions for a llc', async () => {
      const email = faker.internet.email();
      const emailInvestor = faker.internet.email();
      const anAdmin = await FUserAdmin(email);
      const anInvestor = await FUserInvestor(emailInvestor);
      const [userAdmin, admin] = await Promise.all(anAdmin);
      const [user, investor] = await Promise.all(anInvestor);
      const llc = await factoryLlcs();
      const capitalcall: IModelCapitalCall = await FCapitalCall(llc.id);
      const capitalcallInvestor: IModelCapitalCallInvestor =
        await FCapitalCallInvestor(capitalcall.id, investor.id);
      await FContribution(capitalcallInvestor.id);

      await global.testServer
        .post(`/admin/login`)
        .send({
          user: email,
          password: global.adminPass,
        })
        .expect(200);

      const { body } = await global.testServer
        .get(`/admin/llc/${llc.id}/transactions/contribution`)
        .expect(200);
      expect(body.response).to.have.all.keys('pagination', 'details');
      expect(body.response.pagination).to.have.all.keys(
        'itemCount',
        'offset',
        'limit',
        'totalPages',
        'page',
        'nextPage',
        'prevPage'
      );
      body.response.details.forEach((e) => {
        expect(e).to.have.all.keys(
          'id',
          'contributionDate',
          'investor',
          'description',
          'amountContributed'
        );
      });

      await userAdmin.deleteOne();
      await admin.deleteOne();
      await user.deleteOne();
      await investor.deleteOne();
      await llc.deleteOne();
      await capitalcall.deleteOne();
      await capitalcallInvestor.deleteOne();
    });
  });

  describe('GET /admin/llc/:llcId/contribution/:contributionId', () => {
    it('should get detail for a Contribution', async () => {
      const email = faker.internet.email();
      const emailInvestor = faker.internet.email();
      const anAdmin = await FUserAdmin(email);
      const anInvestor = await FUserInvestor(emailInvestor);
      const [userAdmin, admin] = await Promise.all(anAdmin);
      const [user, investor] = await Promise.all(anInvestor);
      const llc = await factoryLlcs();
      const capitalcall: IModelCapitalCall = await FCapitalCall(llc.id);
      const capitalcallInvestor: IModelCapitalCallInvestor =
        await FCapitalCallInvestor(capitalcall.id, investor.id);
      const contribution = await FContribution(capitalcallInvestor.id);

      await global.testServer
        .post(`/admin/login`)
        .send({
          user: email,
          password: global.adminPass,
        })
        .expect(200);

      const { body } = await global.testServer
        .get(`/admin/llc/${llc.id}/contribution/${contribution.id}`)
        .expect(200);

      expect(body.response).to.have.all.keys(
        'account',
        'contributionDate',
        'capitalCall'
      );
      expect(body.response.account).to.have.all.keys('id', 'name');
      expect(body.response.capitalCall).to.have.all.keys(
        'id',
        'description',
        'amount',
        'paymentType',
        'paymentNumber',
        'notes'
      );

      await userAdmin.deleteOne();
      await admin.deleteOne();
      await user.deleteOne();
      await investor.deleteOne();
      await llc.deleteOne();
      await capitalcall.deleteOne();
      await capitalcallInvestor.deleteOne();
    });
  });

  describe('PUT /admin/llc/:llcId/contribution/:contributionId', () => {
    it('should update a created Contribution', async () => {
      const email = faker.internet.email();
      const emailInvestor = faker.internet.email();
      const emailInvestor1 = faker.internet.email();
      const anAdmin = await FUserAdmin(email);
      const anInvestor = await FUserInvestor(emailInvestor);
      const anInvestor1 = await FUserInvestor(emailInvestor1);
      const [userAdmin, admin] = await Promise.all(anAdmin);
      const [user, investor] = await Promise.all(anInvestor);
      const [user1, investor1] = await Promise.all(anInvestor1);
      const llc = await factoryLlcs();
      const capitalcall: IModelCapitalCall = await FCapitalCall(llc.id);
      const capitalcallInvestor: IModelCapitalCallInvestor =
        await FCapitalCallInvestor(capitalcall.id, investor.id);
      const contribution: IModelContribution = await FContribution(
        capitalcallInvestor.id
      );

      await global.testServer
        .post(`/admin/login`)
        .send({
          user: email,
          password: global.adminPass,
        })
        .expect(200);

      const dataContribution = {
        account: investor1.id,
        dateReceived: new Date(),
        capitalCall: {
          amount: faker.datatype.number(1000),
          paymentType: IEnumTypesPayment.WIRE,
          paymentNumber: faker.datatype.number(10),
          notes: faker.datatype.string(10),
        },
      };
      const { body } = await global.testServer
        .put(`/admin/llc/${llc.id}/contribution/${contribution.id}`)
        .send(dataContribution)
        .expect(200);

      expect(body.response).to.have.property('id');
      const updatedContribution: IModelContribution =
        await ModelContribution.findById(contribution.id);
      const amountBig = new Bignumber(updatedContribution.paymentAmount);
      const amountF = parseFloat(amountBig.toNumber().toFixed(2));
      expect(amountF).to.be.equal(dataContribution.capitalCall.amount);

      await userAdmin.deleteOne();
      await admin.deleteOne();
      await user.deleteOne();
      await investor.deleteOne();
      await user1.deleteOne();
      await investor1.deleteOne();
      await llc.deleteOne();
      await capitalcall.deleteOne();
      await capitalcallInvestor.deleteOne();
    });
  });

  describe('DELETE /admin/llc/:llcId/contribution/:contributionId', () => {
    it('should delete a created Contribution', async () => {
      const email = faker.internet.email();
      const emailInvestor = faker.internet.email();
      const anAdmin = await FUserAdmin(email);
      const anInvestor = await FUserInvestor(emailInvestor);
      const [userAdmin, admin] = await Promise.all(anAdmin);
      const [user, investor] = await Promise.all(anInvestor);
      const llc = await factoryLlcs();
      const capitalcall: IModelCapitalCall = await FCapitalCall(llc.id);
      const capitalcallInvestor: IModelCapitalCallInvestor =
        await FCapitalCallInvestor(capitalcall.id, investor.id);
      const contribution: IModelContribution = await FContribution(
        capitalcallInvestor.id
      );

      await global.testServer
        .post(`/admin/login`)
        .send({
          user: email,
          password: global.adminPass,
        })
        .expect(200);

      await global.testServer
        .delete(`/admin/llc/${llc.id}/contribution/${contribution.id}`)
        .expect(200);

      const deletedContribution: IModelContribution =
        await ModelContribution.findById(contribution.id);
      expect(deletedContribution.deletedBy.dateTime).not.to.be.null;

      await userAdmin.deleteOne();
      await admin.deleteOne();
      await user.deleteOne();
      await investor.deleteOne();
      await llc.deleteOne();
      await capitalcall.deleteOne();
      await capitalcallInvestor.deleteOne();
    });
  });
});
