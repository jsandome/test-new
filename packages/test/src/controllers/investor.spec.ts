import faker from 'faker';
import {
  FUserInvestor,
  FUserAdmin,
  insertPropertyWithoutFinancial,
  FOfferingProspective,
  insertStateAndCounty,
} from '../helpers';
import {
  EnumEmailType,
  EnumOfferingStatus,
  EnumOfferingType,
  EnumPhoneType,
} from '@capa/core';
import {
  IModelInvestor,
  IModelOfferingProspective,
  ModelInvestor,
  ModelOfferingProspective,
} from '@capa/data';
import { expect } from 'chai';
import Bignumber from 'bignumber.js';

describe('Investor Controller Test', () => {
  describe('POST /admin/investor/{investorId}/prospective', () => {
    it('should create a new offering prospective', async () => {
      const emailAdmin = faker.internet.email();
      const emailInv1 = faker.internet.email();
      const emailInv2 = faker.internet.email();
      const anAdmin = await FUserAdmin(emailAdmin);
      const anInvestor1 = await FUserInvestor(emailInv1);
      const anInvestor2 = await FUserInvestor(emailInv2);
      const [userAdmin, admin] = await Promise.all(anAdmin);
      const [user1, investor1] = await Promise.all(anInvestor1);
      const [user2, investor2] = await Promise.all(anInvestor2);
      const property = await insertPropertyWithoutFinancial();

      await global.testServer
        .post(`/admin/login`)
        .send({
          user: emailAdmin,
          password: global.adminPass,
        })
        .expect(200);

      const dataNewOffering = {
        offeringType: EnumOfferingType.PROPERTY,
        offering: property.id,
        status: EnumOfferingStatus.CONTACTED,
        expectedAmount: faker.datatype.number(10000),
        likeliHood: faker.datatype.number(100),
        additionalProspect: [
          {
            id: investor2.id,
            primary: true,
          },
        ],
      };

      const { body } = await global.testServer
        .post(`/admin/investor/${investor1.id}/prospective`)
        .send(dataNewOffering)
        .expect(200);

      expect(body).to.have.all.keys('response', 'message');
      const newOffering: IModelOfferingProspective =
        await ModelOfferingProspective.findById(body.response.id);

      expect(newOffering.offering.toHexString()).to.be.equal(
        property._id.toHexString()
      );
      expect(newOffering.status).to.be.equal(dataNewOffering.status);

      const expectedAmountBig = new Bignumber(newOffering.expectedAmount);
      const expectedAmountF = parseFloat(
        expectedAmountBig.toNumber().toFixed(2)
      );
      expect(expectedAmountF).to.be.equal(dataNewOffering.expectedAmount);

      const likeliHoodBig = new Bignumber(newOffering.likeliHood);
      const likeliHoodBigF = parseFloat(likeliHoodBig.toNumber().toFixed(2));
      expect(likeliHoodBigF).to.be.equal(dataNewOffering.likeliHood);

      await userAdmin.deleteOne();
      await admin.deleteOne();
      await user1.deleteOne();
      await user2.deleteOne();
      await investor1.deleteOne();
      await investor2.deleteOne();
    });
  });

  describe('GET /admin/investor/select', () => {
    it('should select investors', async () => {
      const emailAdmin = faker.internet.email();
      const emailInv1 = faker.internet.email();
      const anAdmin = await FUserAdmin(emailAdmin);
      const anInvestor1 = await FUserInvestor(emailInv1);
      const [userAdmin, admin] = await Promise.all(anAdmin);
      const [user1, investor1] = await Promise.all(anInvestor1);

      await global.testServer
        .post(`/admin/login`)
        .send({
          user: emailAdmin,
          password: global.adminPass,
        })
        .expect(200);

      const { body } = await global.testServer
        .get(`/admin/investor/select`)
        .expect(200);
      expect(body).to.have.property('response');

      body.response.forEach((element) => {
        expect(element).to.have.all.keys('id', 'name', 'email');
      });

      await userAdmin.deleteOne();
      await admin.deleteOne();
      await user1.deleteOne();
      await investor1.deleteOne();
    });
  });

  describe('GET /admin/investor/list', () => {
    it('should get list of investors', async () => {
      const emailAdmin = faker.internet.email();
      const emailInv1 = faker.internet.email();
      const emailInv2 = faker.internet.email();
      const anAdmin = await FUserAdmin(emailAdmin);
      const anInvestor1 = await FUserInvestor(emailInv1);
      const anInvestor2 = await FUserInvestor(emailInv2);
      const [userAdmin, admin] = await Promise.all(anAdmin);
      const [user1, investor1] = await Promise.all(anInvestor1);
      const [user2, investor2] = await Promise.all(anInvestor2);

      await global.testServer
        .post(`/admin/login`)
        .send({
          user: emailAdmin,
          password: global.adminPass,
        })
        .expect(200);

      const { body } = await global.testServer
        .get(`/admin/investor/list`)
        .expect(200);
      expect(body).to.have.property('response');
      const {response} = body;
      expect(response).to.have.all.keys(
        'pagination',
        'details'
      );
      expect(response.pagination).to.have.all.keys('itemCount', 'offset', 'limit' , 'totalPages', 'page', 'nextPage', 'prevPage');
      response.details.forEach(e => {
        expect(e).to.have.all.keys('name', 'company', 'commitment', 'investment', 'lastUpdate', 'id');
      });

      await userAdmin.deleteOne();
      await admin.deleteOne();
      await user1.deleteOne();
      await user2.deleteOne();
      await investor1.deleteOne();
      await investor2.deleteOne();
    });
  });

  describe('PATCH /admin/investor/:investorId/prospective/:prospectiveId', () => {
    it('should update a created Offering prospective', async () => {
      const emailAdmin = faker.internet.email();
      const emailInv1 = faker.internet.email();
      const anAdmin = await FUserAdmin(emailAdmin);
      const anInvestor1 = await FUserInvestor(emailInv1);
      const [userAdmin, admin] = await Promise.all(anAdmin);
      const [user1, investor1] = await Promise.all(anInvestor1);
      const property = await insertPropertyWithoutFinancial();
      const offeringProsp = await FOfferingProspective(
        investor1._id.toHexString(),
        property._id.toHexString(),
        'Property'
      );

      await global.testServer
        .post(`/admin/login`)
        .send({
          user: emailAdmin,
          password: global.adminPass,
        })
        .expect(200);

      const dataOffering = {
        status: EnumOfferingStatus.SOFTCIRCLED,
        likeliHood: faker.datatype.number(100),
        expectedAmount: faker.datatype.number(1000),
      };
      await global.testServer
        .patch(
          `/admin/investor/${investor1._id.toHexString()}/prospective/${offeringProsp._id.toHexString()}`
        )
        .send(dataOffering)
        .expect(200);

      const updatedOffering = await ModelOfferingProspective.findById(
        offeringProsp._id.toHexString()
      );
      expect(updatedOffering.status).to.be.equal(
        EnumOfferingStatus.SOFTCIRCLED
      );

      const likeliHoodBig = new Bignumber(updatedOffering.likeliHood);
      const likeliHoodF = parseFloat(likeliHoodBig.toNumber().toFixed(2));
      expect(likeliHoodF).to.be.equal(dataOffering.likeliHood);

      const expectedAmountBig = new Bignumber(updatedOffering.expectedAmount);
      const expectedAmountF = parseFloat(
        expectedAmountBig.toNumber().toFixed(2)
      );
      expect(expectedAmountF).to.be.equal(dataOffering.expectedAmount);

      await userAdmin.deleteOne();
      await admin.deleteOne();
      await user1.deleteOne();
      await investor1.deleteOne();
      await property.deleteOne();
      await offeringProsp.deleteOne();
    });
  });

  describe('PUT /admin/investor/:investorId/general', () => {
    it('should update an investor', async () => {
      const emailAdmin = faker.internet.email();
      const emailInv1 = faker.internet.email();
      const anAdmin = await FUserAdmin(emailAdmin);
      const anInvestor1 = await FUserInvestor(emailInv1);
      const [userAdmin, admin] = await Promise.all(anAdmin);
      const [user1, investor1] = await Promise.all(anInvestor1);
      const { state, county } = await insertStateAndCounty();

      await global.testServer
        .post(`/admin/login`)
        .send({
          user: emailAdmin,
          password: global.adminPass,
        })
        .expect(200);

      const dataToUpdateInvestor = {
        email: faker.internet.email(),
        emailType: EnumEmailType.WORK,
        phone: faker.phone.phoneNumber('5511223344'),
        phoneType: EnumPhoneType.WORK,
        addresses: [
          {
            address: faker.address.streetAddress(),
            city: faker.address.cityName(),
            zipCode: faker.address.zipCode('12345-1234'),
            county: county._id,
            state: state._id,
            isPrincipal: true,
          },
        ],
        biographical: {
          name: faker.name.firstName(),
          city: faker.address.cityName(),
          jobTitle: faker.name.jobDescriptor(),
          companyName: faker.name.jobArea(),
          referrer: faker.name.firstName(),
          assignee: faker.name.firstName(),
        },
        additionalDetails: faker.datatype.string(10),
      };

      await global.testServer
        .put(`/admin/investor/${investor1._id.toHexString()}/general`)
        .send(dataToUpdateInvestor)
        .expect(200);

      const investor: IModelInvestor = await ModelInvestor.findById(
        investor1._id.toHexString()
      );
      expect(investor.email).to.be.equal(dataToUpdateInvestor.email);
      expect(investor.emailType).to.be.equal(dataToUpdateInvestor.emailType);
      expect(investor.phone).to.be.equal(dataToUpdateInvestor.phone);
      expect(investor.phoneType).to.be.equal(dataToUpdateInvestor.phoneType);
      expect(investor.emailType).to.be.equal(dataToUpdateInvestor.emailType);
      expect(investor.biographical.name).to.be.equal(
        dataToUpdateInvestor.biographical.name
      );
      expect(investor.biographical.city).to.be.equal(
        dataToUpdateInvestor.biographical.city
      );
      expect(investor.biographical.jobTitle).to.be.equal(
        dataToUpdateInvestor.biographical.jobTitle
      );
      expect(investor.biographical.companyName).to.be.equal(
        dataToUpdateInvestor.biographical.companyName
      );
      expect(investor.biographical.referrer).to.be.equal(
        dataToUpdateInvestor.biographical.referrer
      );
      expect(investor.biographical.assignee).to.be.equal(
        dataToUpdateInvestor.biographical.assignee
      );

      await userAdmin.deleteOne();
      await admin.deleteOne();
      await user1.deleteOne();
      await investor1.deleteOne();
      await state.deleteOne();
      await county.deleteOne();
    });
  });
});
