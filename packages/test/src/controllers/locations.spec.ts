import 'mocha';
import { expect } from 'chai';

import {
  Countries,
  States,
  insertLocations,
  cleanLocationsData,
} from '../helpers';
const randomStringExtract = (string) => {
  let indexName = parseInt((Math.random() * string.length - 1).toFixed());
  if (indexName === 0) indexName += 1;
  return string.slice(0, indexName);
};

describe('Locations', async () => {
  before(insertLocations);
  after(cleanLocationsData);

  it('Countries', async () => {
    const indexDocument = parseInt(
      (Math.random() * (Countries.length - 1)).toFixed()
    );
    const currentDocument = Countries[indexDocument];
    const stringExtract = randomStringExtract(currentDocument.name);
    const { body } = await global.testServer
      .get(`/country?search=${stringExtract}`)
      .expect(200);
    const response = body.response;
    expect(response.length).to.be.greaterThan(0);
    const totalMatchs = response.filter(
      (d) => d.id == currentDocument.id
    ).length;
    expect(totalMatchs).to.be.equal(1);
    response.forEach((country) => {
      expect(country).to.have.all.keys('id', 'name', 'code');
    });
  });
  it('States', async () => {
    const indexDocument = parseInt(
      (Math.random() * (States.length - 1)).toFixed()
    );
    const currentDocument = States[indexDocument];
    const stringExtract = randomStringExtract(currentDocument.name);
    const { body } = await global.testServer
      .get(`/state?search=${stringExtract}`)
      .expect(200);
    const states = body.response;
    expect(states.length).to.be.greaterThan(0);
    const totalMatchs = states.filter((d) => d.id == currentDocument.id).length;
    expect(totalMatchs).to.be.equal(1);
    states.forEach((state) => {
      expect(state).to.have.all.keys('id', 'name', 'code');
    });
  });
  it('Counties', async () => {
    const randomState =
      States[parseInt((Math.random() * (States.length - 1)).toFixed())];
    const countiesQuerySearch = await global.testServer
      .get(`/state/${randomState._id}/county`)
      .expect(200);
    const countiesWithSearch = countiesQuerySearch.body.response;
    expect(countiesWithSearch.length).to.be.greaterThan(
      0,
      'No counties find with the search'
    );
    countiesWithSearch.forEach((country) => {
      expect(country).to.have.all.keys('id', 'name', 'fips');
    });
  });
});
