import 'mocha';
import { expect } from 'chai';
import faker from 'faker';
import { FPackage, FProperty, FUserAdmin, FUserInvestor } from '../helpers';
import { ModelOnboarding,
  ModelPackageDocuments,
  ModelFund, 
  ModelFundInvestor,
  ModelFundProperty,
  ModelFundPackage} from '@capa/data';

let user = null;
let adminC = null;
let capaSession = '';
let templatesName = null;
const test1 = faker.lorem.words();
const test2 = faker.lorem.words();
const emailTest = faker.internet.email();
const emailTest2 = faker.internet.email();
let tokenToUse = null;

const individualBody = {
  "investmentAmount": 100,
  "mailingAddres": {
    "streetLineOne": faker.lorem.words(),
    "streetLineTwo": faker.lorem.words(),
    "city": faker.lorem.words(),
    "state": faker.lorem.words(),
    "zip": faker.address.zipCode(),
    "country": faker.lorem.words(),
  },
  "sameMailingAddress": false,
  "domicileAddress": {
    "streetLineOne": faker.lorem.words(),
    "streetLineTwo": faker.lorem.words(),
    "city": faker.lorem.words(),
    "state": faker.lorem.words(),
    "zip": faker.address.zipCode(),
    "country": faker.lorem.words(),
  },
  "typeFormOnboarding": "Individual",
  "individualIdentity": {
    "firstName": faker.lorem.words(),
    "middleName": faker.lorem.words(),
    "lastName": faker.lorem.words(),
    "phoneNumber": faker.lorem.words(),
    "email": faker.lorem.words(),
    "country": faker.lorem.words(),
    "employer": faker.lorem.words(),
    "jobTitle": faker.lorem.words(),
    "entityTaxPurpose": true
  }
};
const individualBodyBadRequest = {
  "investmentAmount": 100,
  "mailingAddres": {
    "streetLineOne": faker.lorem.words(),
    "streetLineTwo": faker.lorem.words(),
    "city": faker.lorem.words(),
    "state": faker.lorem.words(),
    "zip": faker.address.zipCode(),
    "country": faker.lorem.words(),
  },
  "sameMailingAddress": true,
    "domicileAddress": {
    "streetLineOne": faker.lorem.words(),
    "streetLineTwo": faker.lorem.words(),
    "city": faker.lorem.words(),
    "state": faker.lorem.words(),
    "zip": faker.address.zipCode(),
    "country": faker.lorem.words(),
  },
    "typeFormOnboarding": "Individual",
    "individualIdentity": {
    "firstName": faker.lorem.words(),
    "middleName": faker.lorem.words(),
    "lastName": faker.lorem.words(),
    "phoneNumber": faker.lorem.words(),
    "email": faker.lorem.words(),
    "country": faker.lorem.words(),
    "employer": faker.lorem.words(),
    "jobTitle": faker.lorem.words(),
    "entityTaxPurpose": true
  }
};

const jointTenancyBody = {
  "investmentAmount": 100,
	"mailingAddres": {
		"streetLineOne": faker.lorem.words(),
		"streetLineTwo": faker.lorem.words(),
		"city": faker.lorem.words(),
		"state": faker.lorem.words(),
    "zip": faker.address.zipCode(),
  	"country": faker.lorem.words(),
	},
	"sameMailingAddress": false,
  "domicileAddress": {
  "streetLineOne": faker.lorem.words(),
  "streetLineTwo": faker.lorem.words(),
  "city": faker.lorem.words(),
  "state": faker.lorem.words(),
  "zip": faker.address.zipCode(),
  "country": faker.lorem.words(),
	},
  "typeFormOnboarding": "Joint tenancy with right of survivorship",
  "jointTenancy": {
    "subscriberLegalName": faker.lorem.words(),
    "relationshipOwners": faker.lorem.words(),
    "entityTaxPurpose": false,
    "owners": [{
        "firstName": faker.lorem.words(),
        "middleName": faker.lorem.words(),
        "lastName": faker.lorem.words(),
        "phoneNumber": faker.lorem.words(),
        "email": faker.lorem.words(),
        "country": faker.lorem.words(),
        "usPerson": true
    }]
  }
};
const jointTenancyBody2 = {
  "investmentAmount": 100,
	"mailingAddres": {
		"streetLineOne": faker.lorem.words(),
		"streetLineTwo": faker.lorem.words(),
		"city": faker.lorem.words(),
		"state": faker.lorem.words(),
    "zip": faker.address.zipCode(),
  	"country": faker.lorem.words(),
	},
	"sameMailingAddress": false,
  "domicileAddress": {
  "streetLineOne": faker.lorem.words(),
  "streetLineTwo": faker.lorem.words(),
  "city": faker.lorem.words(),
  "state": faker.lorem.words(),
  "zip": faker.address.zipCode(),
  "country": faker.lorem.words(),
	},
  "typeFormOnboarding": "Joint tenancy with right of survivorship",
  "jointTenancy": {
    "subscriberLegalName": faker.lorem.words(),
    "relationshipOwners": faker.lorem.words(),
    "entityTaxPurpose": true,
    "ultimateBeneficial": faker.lorem.words(),
    "ultimateBeneficialTaxId": faker.lorem.words(),
    "owners": [{
        "firstName": faker.lorem.words(),
        "middleName": faker.lorem.words(),
        "lastName": faker.lorem.words(),
        "phoneNumber": faker.lorem.words(),
        "email": faker.lorem.words(),
        "country": faker.lorem.words(),
        "usPerson": true
    }]
  }
};
const jointTenancyBodyBadRequest = {
  "investmentAmount": 100,
	"mailingAddres": {
		"streetLineOne": faker.lorem.words(),
		"streetLineTwo": faker.lorem.words(),
		"city": faker.lorem.words(),
		"state": faker.lorem.words(),
		"zip": faker.lorem.words(),
  	"country": faker.lorem.words(),
	},
	"sameMailingAddress": false,
  "domicileAddress": {
  "streetLineOne": faker.lorem.words(),
  "streetLineTwo": faker.lorem.words(),
  "city": faker.lorem.words(),
  "state": faker.lorem.words(),
  "zip": faker.lorem.words(),
  "country": faker.lorem.words(),
	},
  "typeFormOnboarding": "Joint tenancy with right of survivorship",
  "jointTenancy": {
    "subscriberLegalName": faker.lorem.words(),
    "relationshipOwners": faker.lorem.words(),
    "owners": [
      {
        "firstName": faker.lorem.words(),
        "middleName": faker.lorem.words(),
        "lastName": faker.lorem.words(),
        "phoneNumber": faker.lorem.words(),
        "email": faker.lorem.words(),
        "country": faker.lorem.words(),
        "usPerson": true
      }
    ]
  }
};

const trustBody = {
  "investmentAmount": 100,
	"mailingAddres": {
		"streetLineOne": faker.lorem.words(),
		"streetLineTwo": faker.lorem.words(),
		"city": faker.lorem.words(),
		"state": faker.lorem.words(),
    "zip": faker.address.zipCode(),
  	"country": faker.lorem.words(),
	},
	"sameMailingAddress": false,
  "domicileAddress": {
  "streetLineOne": faker.lorem.words(),
  "streetLineTwo": faker.lorem.words(),
  "city": faker.lorem.words(),
  "state": faker.lorem.words(),
  "zip": faker.address.zipCode(),
  "country": faker.lorem.words(),
	},
  "typeFormOnboarding": "Trust (except employee benefit trust or IRA)",
  "trust": {
    "subscriberLegalName": faker.lorem.words(),
    "taxID": faker.lorem.words(),
    "entityTaxPurpose": true,
    "ultimateBeneficial": faker.lorem.words(),
    "ultimateBeneficialOwnerTaxId": faker.lorem.words(),
    "grantorTrust": true,
    "authorized": true,
    "owners":[
      {
        "naturalPerson": true,
        "person": {
          "firstName": faker.lorem.words(),
          "middleName": faker.lorem.words(),
          "lastName": faker.lorem.words(),
          "phoneNumber": faker.lorem.words(),
          "email": faker.lorem.words(),
          "title": faker.lorem.words(),
          "country": faker.lorem.words(),
          "dateBirthday": "2000-10-15",
          "taxID": faker.lorem.words(),
        }
      },
      {
        "naturalPerson": false,
        "entity": {
          "entityName": faker.lorem.words(),
          "entityTitle": faker.lorem.words(),
          "basicSignatureBlock": true
        }
      }
    ]
  }
};
const trustBody2 = {
  "investmentAmount": 100,
	"mailingAddres": {
		"streetLineOne": faker.lorem.words(),
		"streetLineTwo": faker.lorem.words(),
		"city": faker.lorem.words(),
		"state": faker.lorem.words(),
    "zip": faker.address.zipCode(),
  	"country": faker.lorem.words(),
	},
	"sameMailingAddress": false,
  "domicileAddress": {
  "streetLineOne": faker.lorem.words(),
  "streetLineTwo": faker.lorem.words(),
  "city": faker.lorem.words(),
  "state": faker.lorem.words(),
  "zip": faker.address.zipCode(),
  "country": faker.lorem.words(),
	},
  "typeFormOnboarding": "Trust (except employee benefit trust or IRA)",
  "trust": {
    "subscriberLegalName": faker.lorem.words(),
    "taxID": faker.lorem.words(),
    "entityTaxPurpose": false,
    "grantorTrust": true,
    "authorized": true,
    "owners":[
      {
        "naturalPerson": true,
        "person": {
          "firstName": faker.lorem.words(),
          "middleName": faker.lorem.words(),
          "lastName": faker.lorem.words(),
          "phoneNumber": faker.lorem.words(),
          "email": faker.lorem.words(),
          "title": faker.lorem.words(),
          "country": faker.lorem.words(),
          "dateBirthday": "2000-10-15",
          "taxID": faker.lorem.words(),
        }
      },
      {
        "naturalPerson": false,
        "entity": {
          "entityName": faker.lorem.words(),
          "entityTitle": faker.lorem.words(),
          "basicSignatureBlock": true
        }
      }
    ]
  }
};
const trustBodyBadRequest2 = {
  "investmentAmount": 100,
	"mailingAddres": {
		"streetLineOne": faker.lorem.words(),
		"streetLineTwo": faker.lorem.words(),
		"city": faker.lorem.words(),
		"state": faker.lorem.words(),
    "zip": faker.address.zipCode(),
  	"country": faker.lorem.words(),
	},
	"sameMailingAddress": false,
  "domicileAddress": {
  "streetLineOne": faker.lorem.words(),
  "streetLineTwo": faker.lorem.words(),
  "city": faker.lorem.words(),
  "state": faker.lorem.words(),
  "zip": faker.address.zipCode(),
  "country": faker.lorem.words(),
	},
  "typeFormOnboarding": "Trust (except employee benefit trust or IRA)",
  "trust": {
    "subscriberLegalName": faker.lorem.words(),
    "taxID": faker.lorem.words(),
    "entityTaxPurpose": true,
    "ultimateBeneficial": faker.lorem.words(),
    "ultimateBeneficialOwnerTaxId": faker.lorem.words(),
    "grantorTrust": true,
    "authorized": true,
    "owners":[
      {
        "naturalPerson": false,
        "person": {
          "firstName": faker.lorem.words(),
          "middleName": faker.lorem.words(),
          "lastName": faker.lorem.words(),
          "phoneNumber": faker.lorem.words(),
          "email": faker.lorem.words(),
          "title": faker.lorem.words(),
          "country": faker.lorem.words(),
          "dateBirthday": "2000-10-15",
          "taxID": faker.lorem.words(),
        }
      },
      {
        "naturalPerson": false,
        "entity": {
          "entityName": faker.lorem.words(),
          "entityTitle": faker.lorem.words(),
          "basicSignatureBlock": true
        }
      }
    ]
  }
};
const trustBodyBadRequest3 = {
  "investmentAmount": 100,
	"mailingAddres": {
		"streetLineOne": faker.lorem.words(),
		"streetLineTwo": faker.lorem.words(),
		"city": faker.lorem.words(),
		"state": faker.lorem.words(),
    "zip": faker.address.zipCode(),
  	"country": faker.lorem.words(),
	},
	"sameMailingAddress": false,
  "domicileAddress": {
  "streetLineOne": faker.lorem.words(),
  "streetLineTwo": faker.lorem.words(),
  "city": faker.lorem.words(),
  "state": faker.lorem.words(),
  "zip": faker.address.zipCode(),
  "country": faker.lorem.words(),
	},
  "typeFormOnboarding": "Trust (except employee benefit trust or IRA)",
  "trust": {
    "subscriberLegalName": faker.lorem.words(),
    "taxID": faker.lorem.words(),
    "entityTaxPurpose": true,
    "ultimateBeneficial": faker.lorem.words(),
    "ultimateBeneficialOwnerTaxId": faker.lorem.words(),
    "grantorTrust": true,
    "authorized": true,
    "owners":[
      {
        "naturalPerson": true,
        "person": {
          "firstName": faker.lorem.words(),
          "middleName": faker.lorem.words(),
          "lastName": faker.lorem.words(),
          "phoneNumber": faker.lorem.words(),
          "email": faker.lorem.words(),
          "title": faker.lorem.words(),
          "country": faker.lorem.words(),
          "dateBirthday": "2000-10-15",
          "taxID": faker.lorem.words(),
        }
      },
      {
        "naturalPerson": true,
        "entity": {
          "entityName": faker.lorem.words(),
          "entityTitle": faker.lorem.words(),
          "basicSignatureBlock": true
        }
      }
    ]
  }
};
const trustBodyBadRequest4 = {
  "investmentAmount": 100,
	"mailingAddres": {
		"streetLineOne": faker.lorem.words(),
		"streetLineTwo": faker.lorem.words(),
		"city": faker.lorem.words(),
		"state": faker.lorem.words(),
    "zip": faker.address.zipCode(),
  	"country": faker.lorem.words(),
	},
	"sameMailingAddress": false,
  "domicileAddress": {
  "streetLineOne": faker.lorem.words(),
  "streetLineTwo": faker.lorem.words(),
  "city": faker.lorem.words(),
  "state": faker.lorem.words(),
  "zip": faker.address.zipCode(),
  "country": faker.lorem.words(),
	},
  "typeFormOnboarding": "Trust (except employee benefit trust or IRA)",
  "trust": {
    "subscriberLegalName": faker.lorem.words(),
    "taxID": faker.lorem.words(),
    "entityTaxPurpose": false,
    "ultimateBeneficial": faker.lorem.words(),
    "ultimateBeneficialOwnerTaxId": faker.lorem.words(),
    "grantorTrust": true,
    "authorized": true,
    "owners":[
      {
        "naturalPerson": true,
        "person": {
          "firstName": faker.lorem.words(),
          "middleName": faker.lorem.words(),
          "lastName": faker.lorem.words(),
          "phoneNumber": faker.lorem.words(),
          "email": faker.lorem.words(),
          "title": faker.lorem.words(),
          "country": faker.lorem.words(),
          "dateBirthday": "2000-10-15",
          "taxID": faker.lorem.words(),
        }
      },
      {
        "naturalPerson": false,
        "entity": {
          "entityName": faker.lorem.words(),
          "entityTitle": faker.lorem.words(),
          "basicSignatureBlock": true
        }
      }
    ]
  }
};

const llcBody = {
  "investmentAmount": 100,
	"mailingAddres": {
		"streetLineOne": faker.lorem.words(),
		"streetLineTwo": faker.lorem.words(),
		"city": faker.lorem.words(),
		"state": faker.lorem.words(),
    "zip": faker.address.zipCode(),
  	"country": faker.lorem.words(),
	},
	"sameMailingAddress": false,
  "domicileAddress": {
  "streetLineOne": faker.lorem.words(),
  "streetLineTwo": faker.lorem.words(),
  "city": faker.lorem.words(),
  "state": faker.lorem.words(),
  "zip": faker.address.zipCode(),
  "country": faker.lorem.words(),
	},
  "typeFormOnboarding": "LLC",
  "llc": {
    "subscriberLegalName": faker.lorem.words(),
    "taxID": faker.lorem.words(),
    "usPerson": false,
    "organizationalJurisdiction": faker.lorem.words(),
    "entityTaxPurpose": true,
    "owners": [
      {
          "naturalPerson": true,
          "person": {
            "firstName": faker.lorem.words(),            
            "middleName": faker.lorem.words(),
            "lastName": faker.lorem.words(),
            "phoneNumber": faker.lorem.words(),
            "email": faker.lorem.words(),
            "title": faker.lorem.words(),
            "country": faker.lorem.words(),
            "dateBirthday": "2000-10-15",
            "taxID": faker.lorem.words(),
          }
        },
        {
          "naturalPerson": false,
          "entity": {
            "entityName": faker.lorem.words(),
            "entityTitle": faker.lorem.words(),
            "basicSignatureBlock": true
          }
        }
    ]
  }
};
const llcBody2 = {
  "investmentAmount": 100,
	"mailingAddres": {
		"streetLineOne": faker.lorem.words(),
		"streetLineTwo": faker.lorem.words(),
		"city": faker.lorem.words(),
		"state": faker.lorem.words(),
    "zip": faker.address.zipCode(),
  	"country": faker.lorem.words(),
	},
	"sameMailingAddress": false,
  "domicileAddress": {
  "streetLineOne": faker.lorem.words(),
  "streetLineTwo": faker.lorem.words(),
  "city": faker.lorem.words(),
  "state": faker.lorem.words(),
  "zip": faker.address.zipCode(),
  "country": faker.lorem.words(),
	},
  "typeFormOnboarding": "LLC",
  "llc": {
    "subscriberLegalName": faker.lorem.words(),
    "taxID": faker.lorem.words(),
    "usPerson": true,
    "ultimateBeneficial": faker.lorem.words(),
    "ultimateBeneficialOwnerTaxId": faker.lorem.words(),
    "country": faker.lorem.words(),
    "owners": [
      {
          "naturalPerson": true,
          "person": {
            "firstName": faker.lorem.words(),            
            "middleName": faker.lorem.words(),
            "lastName": faker.lorem.words(),
            "phoneNumber": faker.lorem.words(),
            "email": faker.lorem.words(),
            "title": faker.lorem.words(),
            "country": faker.lorem.words(),
            "dateBirthday": "2000-10-15",
            "taxID": faker.lorem.words(),
          }
        },
        {
          "naturalPerson": false,
          "entity": {
            "entityName": faker.lorem.words(),
            "entityTitle": faker.lorem.words(),
            "basicSignatureBlock": true
          }
        }
    ]
  }
};
const llcBodyBadRequest = {
  "investmentAmount": 100,
	"mailingAddres": {
		"streetLineOne": faker.lorem.words(),
		"streetLineTwo": faker.lorem.words(),
		"city": faker.lorem.words(),
		"state": faker.lorem.words(),
    "zip": faker.address.zipCode(),
  	"country": faker.lorem.words(),
	},
	"sameMailingAddress": false,
  "domicileAddress": {
  "streetLineOne": faker.lorem.words(),
  "streetLineTwo": faker.lorem.words(),
  "city": faker.lorem.words(),
  "state": faker.lorem.words(),
  "zip": faker.address.zipCode(),
  "country": faker.lorem.words(),
	},
  "typeFormOnboarding": "LLC",
  "llc": {
    "subscriberLegalName": faker.lorem.words(),
    "taxID": faker.lorem.words(),
    "usPerson": true,
    "ultimateBeneficial": faker.lorem.words(),
    "ultimateBeneficialOwnerTaxId": faker.lorem.words(),
    "country": faker.lorem.words(),
    "owners": [
      {
          "naturalPerson": false,
          "person": {
            "firstName": faker.lorem.words(),            
            "middleName": faker.lorem.words(),
            "lastName": faker.lorem.words(),
            "phoneNumber": faker.lorem.words(),
            "email": faker.lorem.words(),
            "title": faker.lorem.words(),
            "country": faker.lorem.words(),
            "dateBirthday": "2000-10-15",
            "taxID": faker.lorem.words(),
          }
        },
        {
          "naturalPerson": false,
          "entity": {
            "entityName": faker.lorem.words(),
            "entityTitle": faker.lorem.words(),
            "basicSignatureBlock": true
          }
        }
    ]
  }
};
const llcBodyBadRequest2 = {
  "investmentAmount": 100,
	"mailingAddres": {
		"streetLineOne": faker.lorem.words(),
		"streetLineTwo": faker.lorem.words(),
		"city": faker.lorem.words(),
		"state": faker.lorem.words(),
    "zip": faker.address.zipCode(),
  	"country": faker.lorem.words(),
	},
	"sameMailingAddress": false,
  "domicileAddress": {
  "streetLineOne": faker.lorem.words(),
  "streetLineTwo": faker.lorem.words(),
  "city": faker.lorem.words(),
  "state": faker.lorem.words(),
  "zip": faker.address.zipCode(),
  "country": faker.lorem.words(),
	},
  "typeFormOnboarding": "LLC",
  "llc": {
    "subscriberLegalName": faker.lorem.words(),
    "taxID": faker.lorem.words(),
    "usPerson": true,
    "ultimateBeneficial": faker.lorem.words(),
    "ultimateBeneficialOwnerTaxId": faker.lorem.words(),
    "country": faker.lorem.words(),
    "owners": [
      {
          "naturalPerson": true,
          "person": {
            "firstName": faker.lorem.words(),            
            "middleName": faker.lorem.words(),
            "lastName": faker.lorem.words(),
            "phoneNumber": faker.lorem.words(),
            "email": faker.lorem.words(),
            "title": faker.lorem.words(),
            "country": faker.lorem.words(),
            "dateBirthday": "2000-10-15",
            "taxID": faker.lorem.words(),
          }
        },
        {
          "naturalPerson": true,
          "entity": {
            "entityName": faker.lorem.words(),
            "entityTitle": faker.lorem.words(),
            "basicSignatureBlock": true
          }
        }
    ]
  }
};
const llcBodyBadRequest3 = {
  "investmentAmount": 100,
	"mailingAddres": {
		"streetLineOne": faker.lorem.words(),
		"streetLineTwo": faker.lorem.words(),
		"city": faker.lorem.words(),
		"state": faker.lorem.words(),
    "zip": faker.address.zipCode(),
  	"country": faker.lorem.words(),
	},
	"sameMailingAddress": false,
  "domicileAddress": {
  "streetLineOne": faker.lorem.words(),
  "streetLineTwo": faker.lorem.words(),
  "city": faker.lorem.words(),
  "state": faker.lorem.words(),
  "zip": faker.address.zipCode(),
  "country": faker.lorem.words(),
	},
  "typeFormOnboarding": "LLC",
  "llc": {
    "subscriberLegalName": faker.lorem.words(),
    "taxID": faker.lorem.words(),
    "usPerson": false,
    "ultimateBeneficial": faker.lorem.words(),
    "ultimateBeneficialOwnerTaxId": faker.lorem.words(),
    "country": faker.lorem.words(),
    "owners": [
      {
          "naturalPerson": true,
          "person": {
            "firstName": faker.lorem.words(),            
            "middleName": faker.lorem.words(),
            "lastName": faker.lorem.words(),
            "phoneNumber": faker.lorem.words(),
            "email": faker.lorem.words(),
            "title": faker.lorem.words(),
            "country": faker.lorem.words(),
            "dateBirthday": "2000-10-15",
            "taxID": faker.lorem.words(),
          }
        },
        {
          "naturalPerson": false,
          "entity": {
            "entityName": faker.lorem.words(),
            "entityTitle": faker.lorem.words(),
            "basicSignatureBlock": true
          }
        }
    ]
  }
};
const llcBodyBadRequest4 = {
  "investmentAmount": 100,
	"mailingAddres": {
		"streetLineOne": faker.lorem.words(),
		"streetLineTwo": faker.lorem.words(),
		"city": faker.lorem.words(),
		"state": faker.lorem.words(),
    "zip": faker.address.zipCode(),
  	"country": faker.lorem.words(),
	},
	"sameMailingAddress": false,
  "domicileAddress": {
  "streetLineOne": faker.lorem.words(),
  "streetLineTwo": faker.lorem.words(),
  "city": faker.lorem.words(),
  "state": faker.lorem.words(),
  "zip": faker.address.zipCode(),
  "country": faker.lorem.words(),
	},
  "typeFormOnboarding": "LLC",
  "llc": {
    "subscriberLegalName": faker.lorem.words(),
    "taxID": faker.lorem.words(),
    "usPerson": true,
    "country": faker.lorem.words(),
    "owners": [
      {
          "naturalPerson": true,
          "person": {
            "firstName": faker.lorem.words(),            
            "middleName": faker.lorem.words(),
            "lastName": faker.lorem.words(),
            "phoneNumber": faker.lorem.words(),
            "email": faker.lorem.words(),
            "title": faker.lorem.words(),
            "country": faker.lorem.words(),
            "dateBirthday": "2000-10-15",
            "taxID": faker.lorem.words(),
          }
        },
        {
          "naturalPerson": false,
          "entity": {
            "entityName": faker.lorem.words(),
            "entityTitle": faker.lorem.words(),
            "basicSignatureBlock": true
          }
        }
    ]
  }
};

const otherJointBody = {
  "investmentAmount": 100,
	"mailingAddres": {
		"streetLineOne": faker.lorem.words(),
		"streetLineTwo": faker.lorem.words(),
		"city": faker.lorem.words(),
		"state": faker.lorem.words(),
    "zip": faker.address.zipCode(),
  	"country": faker.lorem.words(),
	},
	"sameMailingAddress": false,
  "domicileAddress": {
  "streetLineOne": faker.lorem.words(),
  "streetLineTwo": faker.lorem.words(),
  "city": faker.lorem.words(),
  "state": faker.lorem.words(),
  "zip": faker.address.zipCode(),
  "country": faker.lorem.words(),
	},
  "typeFormOnboarding": "Other joint ownership",
  "otherJoint": {
    "subscriberLegalName": faker.lorem.words(),
	  "taxID": faker.lorem.words(),
    "entityTaxPurpose": true,
		"owners": [
        {
				  "usPerson": true,
				  "person": {
					  "firstName": faker.lorem.words(), 
            "middleName": faker.lorem.words(),
            "lastName": faker.lorem.words(),
            "phoneNumber": faker.lorem.words(),
            "email": faker.lorem.words(),
            "citizenship": faker.lorem.words(),
            "title": faker.lorem.words(),
            "dateBirthday": "2000-10-15",
            "taxID": faker.lorem.words(),
				}
			},
			{
				"usPerson": false,
        "person": {
          "firstName": faker.lorem.words(), 
          "middleName": faker.lorem.words(),
          "lastName": faker.lorem.words(),
          "phoneNumber": faker.lorem.words(),
          "email": faker.lorem.words(),
          "citizenship": faker.lorem.words(),
          "title": faker.lorem.words(),
          "dateBirthday": "2000-10-15",
          "taxID": faker.lorem.words(),
        }
			}
    ]
  }
};
const otherJointBody2 = {
  "investmentAmount": 100,
	"mailingAddres": {
		"streetLineOne": faker.lorem.words(),
		"streetLineTwo": faker.lorem.words(),
		"city": faker.lorem.words(),
		"state": faker.lorem.words(),
    "zip": faker.address.zipCode(),
  	"country": faker.lorem.words(),
	},
	"sameMailingAddress": false,
  "domicileAddress": {
  "streetLineOne": faker.lorem.words(),
  "streetLineTwo": faker.lorem.words(),
  "city": faker.lorem.words(),
  "state": faker.lorem.words(),
  "zip": faker.address.zipCode(),
  "country": faker.lorem.words(),
	},
  "typeFormOnboarding": "Other joint ownership",
  "otherJoint": {
    "subscriberLegalName": faker.lorem.words(),
	  "taxID": faker.lorem.words(),
    "entityTaxPurpose": true,
		"owners": [
			{
				"usPerson": false
			}
    ]
  }
};

const otherJointBodyBadRequest = {
  "investmentAmount": 100,
	"mailingAddres": {
		"streetLineOne": faker.lorem.words(),
		"streetLineTwo": faker.lorem.words(),
		"city": faker.lorem.words(),
		"state": faker.lorem.words(),
    "zip": faker.address.zipCode(),
  	"country": faker.lorem.words(),
	},
	"sameMailingAddress": false,
  "domicileAddress": {
  "streetLineOne": faker.lorem.words(),
  "streetLineTwo": faker.lorem.words(),
  "city": faker.lorem.words(),
  "state": faker.lorem.words(),
  "zip": faker.address.zipCode(),
  "country": faker.lorem.words(),
	},
  "typeFormOnboarding": "Other joint ownership",
  "otherJoint": {
    "subscriberLegalName": faker.lorem.words(),
	  "taxID": faker.lorem.words(),
    "entityTaxPurpose": true,
		"owners": [
			{
        "person": {
          "firstName": faker.lorem.words(), 
          "middleName": faker.lorem.words(),
          "lastName": faker.lorem.words(),
          "phoneNumber": faker.lorem.words(),
          "email": faker.lorem.words(),
          "citizenship": faker.lorem.words(),
          "title": faker.lorem.words(),
          "dateBirthday": "2000-10-15",
          "taxID": faker.lorem.words(),
        }
			}
    ]
  }
};

describe('Onboarding test', () => {
  let idCorrect = null;
  let foundID = null;
  before(async () =>{
    const email = faker.internet.email();
    const anAdmin = await FUserAdmin(email);
    const [userAdmin, admin] = await Promise.all(anAdmin);
    user = userAdmin;
    adminC = admin;
    await global.testServer
      .post(`/admin/login`)
      .send({
        user: email,
        password: global.adminPass,
      })
      .expect(200);
    const authCookie = global.testServer.jar.getCookie(
      global.cookieSessionId,
      {
        domain: '',
        path: '/',
        secure: global.cookieSecure,
        script: false,
      }
    );
    expect(authCookie).to.exist;
    capaSession = authCookie.value;
    const getNames = await global.testServer
    .get(`/admin/document/docusign/list`)
    .expect(200);
    templatesName = getNames.body["response"].reduce((pre, value) => {
      if(value.name === "") return pre;
      pre.push(value);
      return pre;
    }, []);
  });

  it('Needed in process, create packageDocusing return a 200 response', async () => {
    const res = await global.testServer
    .post(`/admin/document/docusign/package`)
    .set('Cookie', `capaSession=${capaSession};`)
    .send({
      name: test1,
      templates: [
        templatesName[0]["templateId"] || test2
      ]
    }).expect(200);
    const { body } = res;
    expect(body).to.have.property('response');
    idCorrect = body.response.id;
  });

  it('should create a fund', async () => {
    const email = faker.internet.email();
    const anInvestor = await FUserInvestor(email);
    const [user, investor] = await Promise.all(anInvestor);
    const thisProperty = await FProperty();
    const [property] = await Promise.all(thisProperty);
    const newPack = await FPackage();


    const dataFund = {
      name: faker.name.firstName(),
      cashHand: faker.datatype.number(10000),
      investors: [
        {
          id: investor._id,
          minCommitment: faker.datatype.number(10000),
          initCommitment: faker.datatype.number(10000),
          totalCommitment: faker.datatype.number(10000),
          termYears: faker.datatype.number(10),
        },
      ],
      packages: [
        {
          id: newPack._id,
          share: 100,
        },
      ],
      properties: [
        {
          id: property._id,
          share: 100,
        },
      ],
    };

    const { body } = await global.testServer
      .post(`/admin/fund`)
      .set('Cookie', `capaSession=${capaSession};`)
      .send(dataFund)
      .expect(200);
    expect(body.response).to.have.all.keys('id');
    foundID = body.response.id;
    expect(body).to.have.property('response');
    expect(body).to.have.property('message');
    await user.deleteOne();
    await investor.deleteOne();
  });

  it('Create Onboarding return a 200 response', async () => {
    const res = await global.testServer
    .post(`/admin/onboarding/create`)
    .set('Cookie', `capaSession=${capaSession};`)
    .send({
      "packageDocumentId": idCorrect,
      "email": emailTest,
      "fundId": foundID
    }).expect(200);
    const { body } = res;
    expect(body).to.have.property('response');
    const onboardings = await ModelOnboarding.find({}).sort({ tokenDate : 'desc' }).limit(1);
    tokenToUse = onboardings[0].token
  });

  it('Create Onboarding return a 400 response', async () => {
    const res = await global.testServer
    .post(`/admin/onboarding/create`)
    .set('Cookie', `capaSession=${capaSession};`)
    .send({
      "packageDocumentId": idCorrect,
      "email": emailTest2,
      "fundId": test1
    }).expect(400);
    const { body } = res;
    expect(body).to.have.property('response');
  });

  it('Update Onboarding step-One Individual return a 200 response', async () => {
    const res = await global.testServer
    .put(`/onboarding/${tokenToUse}/basic-information`)
    .send(individualBody).expect(200);
    const { body } = res;
    expect(body).to.have.property('message');
    const message = body.message;
    expect(message).to.be.equal('The information has been saved successfully.');
  });

  it('Update Onboarding step-One Individual return a 400 response', async () => {
    const res = await global.testServer
    .put(`/onboarding/${tokenToUse}/basic-information`)
    .send(individualBodyBadRequest).expect(400);
    const { body } = res;
    expect(body).to.have.property('message');
    const message = body.message;
    expect(message).to.be.equal('Domicile Address is not required.');
  });

  it('Update Onboarding step-One Joint Tenancy return a 200 response', async () => {
    const res = await global.testServer
    .put(`/onboarding/${tokenToUse}/basic-information`)
    .send(jointTenancyBody).expect(200);
    const { body } = res;
    expect(body).to.have.property('message');
    const message = body.message;
    expect(message).to.be.equal('The information has been saved successfully.');
  });
  
  it('Update Onboarding step-One Joint Tenancy with entityTaxPurpose = true, return a 200 response', async () => {
    const res = await global.testServer
    .put(`/onboarding/${tokenToUse}/basic-information`)
    .send(jointTenancyBody2).expect(200);
    const { body } = res;
    expect(body).to.have.property('message');
    const message = body.message;
    expect(message).to.be.equal('The information has been saved successfully.');
  });

  it('Update Onboarding step-One Joint Tenancy  return a 400 response', async () => {
    const res = await global.testServer
    .put(`/onboarding/${tokenToUse}/basic-information`)
    .send(jointTenancyBodyBadRequest).expect(400);
    const { body } = res;
    expect(body).to.have.property('message');
    const message = body.message;
    expect(message).to.be.equal('Joint Tenancy is not valid.');
  });

  it('Update Onboarding step-One Trust return a 200 response', async () => {
    const res = await global.testServer
    .put(`/onboarding/${tokenToUse}/basic-information`)
    .send(trustBody).expect(200);
    const { body } = res;
    expect(body).to.have.property('message');
    const message = body.message;
    expect(message).to.be.equal('The information has been saved successfully.');
  });

  it('Update Onboarding step-One Trust return a 200 response', async () => {
    const res = await global.testServer
    .put(`/onboarding/${tokenToUse}/basic-information`)
    .send(trustBody2).expect(200);
    const { body } = res;
    expect(body).to.have.property('message');
    const message = body.message;
    expect(message).to.be.equal('The information has been saved successfully.');
  });

  it('Update Onboarding step-One Trust return a 400 response', async () => {
    const res = await global.testServer
    .put(`/onboarding/${tokenToUse}/basic-information`)
    .send(trustBodyBadRequest2).expect(400);
    const { body } = res;
    expect(body).to.have.property('message');
    const message = body.message;
    expect(message).to.be.equal('Trust Owners is invalid');
  });

  it('Update Onboarding step-One Trust return a 400 response', async () => {
    const res = await global.testServer
    .put(`/onboarding/${tokenToUse}/basic-information`)
    .send(trustBodyBadRequest3).expect(400);
    const { body } = res;
    expect(body).to.have.property('message');
    const message = body.message;
    expect(message).to.be.equal('Trust Owners is invalid');
  });

  it('Update Onboarding step-One Trust return a 400 response', async () => {
    const res = await global.testServer
    .put(`/onboarding/${tokenToUse}/basic-information`)
    .send(trustBodyBadRequest4).expect(400);
    const { body } = res;
    expect(body).to.have.property('message');
    const message = body.message;
    expect(message).to.be.equal('Ultimate Beneficial is not required., Ultimate Beneficial Owner TaxId is not required.');
  });
 
  it('Update Onboarding step-One LLC return a 200 response', async () => {
    const res = await global.testServer
    .put(`/onboarding/${tokenToUse}/basic-information`)
    .send(llcBody).expect(200);
    const { body } = res;
    expect(body).to.have.property('message');
    const message = body.message;
    expect(message).to.be.equal('The information has been saved successfully.');
  });

  it('Update Onboarding step-One LLC return a 200 response', async () => {
    const res = await global.testServer
    .put(`/onboarding/${tokenToUse}/basic-information`)
    .send(llcBody2).expect(200);
    const { body } = res;
    expect(body).to.have.property('message');
    const message = body.message;
    expect(message).to.be.equal('The information has been saved successfully.');
  });

  it('Update Onboarding step-One LLC return a 400 response', async () => {
    const res = await global.testServer
    .put(`/onboarding/${tokenToUse}/basic-information`)
    .send(llcBodyBadRequest).expect(400);
    const { body } = res;
    expect(body).to.have.property('message');
    const message = body.message;
    expect(message).to.be.equal('LLC Owners is invalid');
  });

  it('Update Onboarding step-One LLC return a 400 response', async () => {
    const res = await global.testServer
    .put(`/onboarding/${tokenToUse}/basic-information`)
    .send(llcBodyBadRequest2).expect(400);
    const { body } = res;
    expect(body).to.have.property('message');
    const message = body.message;
    expect(message).to.be.equal('LLC Owners is invalid');
  });

  it('Update Onboarding step-One LLC return a 400 response', async () => {
    const res = await global.testServer
    .put(`/onboarding/${tokenToUse}/basic-information`)
    .send(llcBodyBadRequest3).expect(400);
    const { body } = res;
    expect(body).to.have.property('message');
    const message = body.message;
    expect(message).to.be.equal('LLC is not valid., Ultimate Beneficial is not required., Ultimate Beneficial Owner TaxId is not required., Country is not required.');
  });

  it('Update Onboarding step-One LLC return a 400 response', async () => {
    const res = await global.testServer
    .put(`/onboarding/${tokenToUse}/basic-information`)
    .send(llcBodyBadRequest4).expect(400);
    const { body } = res;
    expect(body).to.have.property('message');
    const message = body.message;
    expect(message).to.be.equal('LLC is not valid.');
  });

  it('Update Onboarding step-One Other joint ownership return a 200 response', async () => {
    const res = await global.testServer
    .put(`/onboarding/${tokenToUse}/basic-information`)
    .send(otherJointBody).expect(200);
    const { body } = res;
    expect(body).to.have.property('message');
    const message = body.message;
    expect(message).to.be.equal('The information has been saved successfully.');
  });

  it('Update Onboarding step-One Other joint ownership return a 200 response', async () => {
    const res = await global.testServer
    .put(`/onboarding/${tokenToUse}/basic-information`)
    .send(otherJointBody2).expect(200);
    const { body } = res;
    expect(body).to.have.property('message');
    const message = body.message;
    expect(message).to.be.equal('The information has been saved successfully.');
  });

  it('Update Onboarding step-One LLC return a 400 response', async () => {
    const res = await global.testServer
    .put(`/onboarding/${tokenToUse}/basic-information`)
    .send(otherJointBodyBadRequest).expect(400);
    const { body } = res;
    expect(body).to.have.property('message');
    const message = body.message;
    expect(message).to.be.equal('Other joint ownership Owners is invalid');
  });

  after(async () => {
    await user.deleteOne();
    await adminC.deleteOne();
    await ModelFund.deleteMany({});
    await ModelPackageDocuments.deleteMany({});
    await ModelOnboarding.deleteMany({});

    await ModelFundInvestor.deleteMany();
    await ModelFundProperty.deleteMany();
    await ModelFundPackage.deleteMany();
  });

})