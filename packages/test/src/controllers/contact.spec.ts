import { FUserAdmin, insertStateAndCounty, FContact } from '../helpers';
import faker from 'faker';
import { expect } from 'chai';
import { IModelContact, ModelContact } from '@capa/data';

describe('Contact Controller Test', () => {
  describe('POST /admin/contact', () => {
    it('should return ID of a created contact', async () => {
      const { state, county } = await insertStateAndCounty();
      const email = faker.internet.email();
      const anAdmin = await FUserAdmin(email);
      const [userAdmin, admin] = await Promise.all(anAdmin);

      await global.testServer
        .post(`/admin/login`)
        .send({
          user: email,
          password: global.adminPass,
        })
        .expect(200);

      const emailContact = faker.internet.email();
      const { body } = await global.testServer
        .post(`/admin/contact`)
        .send({
          name: faker.name.firstName(),
          jobTitle: faker.name.jobDescriptor(),
          companyName: faker.name.jobType(),
          currentJob: faker.datatype.boolean(),
          email: emailContact,
          phone: faker.phone.phoneNumberFormat(),
          address: faker.address.direction(),
          city: faker.address.city(),
          state: state._id,
          county: county._id,
          zip: faker.address.zipCode(),
        })
        .expect(200);

      const newContact: IModelContact = await ModelContact.findById(
        body.response.id
      );
      expect(newContact.email).to.be.equal(emailContact);

      await admin.deleteOne();
      await userAdmin.deleteOne();
      await state.deleteOne();
      await county.deleteOne();
    });
  });

  describe('GET /admin/contact/:contactId', () => {
    it('should get the requested contact', async () => {
      const { state, county } = await insertStateAndCounty();
      const email = faker.internet.email();
      const anAdmin = await FUserAdmin(email);
      const [userAdmin, admin] = await Promise.all(anAdmin);
      const contact = await FContact(state._id, county._id);

      await global.testServer
        .post(`/admin/login`)
        .send({
          user: email,
          password: global.adminPass,
        })
        .expect(200);

      const { body } = await global.testServer
        .get(`/admin/contact/${contact._id}`)
        .expect(200);

      expect(body.response).to.have.all.keys(
        'id',
        'name',
        'jobTitle',
        'companyName',
        'currentJob',
        'email',
        'phone',
        'address',
        'city',
        'state',
        'county',
        'zip'
      );

      await admin.deleteOne();
      await userAdmin.deleteOne();
      await contact.deleteOne();
      await state.deleteOne();
      await county.deleteOne();
    });
  });

  describe('GET /contact/list', () => {
    it('should get the requested contact', async () => {
      const { state, county } = await insertStateAndCounty();
      const email = faker.internet.email();
      const anAdmin = await FUserAdmin(email);
      const [userAdmin, admin] = await Promise.all(anAdmin);
      const contact1 = await FContact(state._id, county._id);
      const contact2 = await FContact(state._id, county._id);

      await global.testServer
        .post(`/admin/login`)
        .send({
          user: email,
          password: global.adminPass,
        })
        .expect(200);

      const { body } = await global.testServer.get(`/contact/list`).expect(200);
      const { response } = body;

      expect(response).to.have.all.keys('pagination', 'details');
      expect(response.pagination).to.have.all.keys(
        'itemCount',
        'offset',
        'limit',
        'totalPages',
        'page',
        'nextPage',
        'prevPage'
      );
      response.details.forEach((e) => {
        expect(e).to.have.all.keys(
          'name',
          'company',
          'jobTitle',
          'lastUpdate',
          'id',
          'email'
        );
      });

      await admin.deleteOne();
      await userAdmin.deleteOne();
      await contact1.deleteOne();
      await contact2.deleteOne();
      await state.deleteOne();
      await county.deleteOne();
    });
  });

  describe('GET /contact/select', () => {
    it('should search the selected contact', async () => {
      const { state, county } = await insertStateAndCounty();
      const email = faker.internet.email();
      const anAdmin = await FUserAdmin(email);
      const [userAdmin, admin] = await Promise.all(anAdmin);
      const contact1 = await FContact(state._id, county._id);
      const contact2 = await FContact(state._id, county._id);
      const contact3 = await FContact(state._id, county._id);

      await global.testServer
        .post(`/admin/login`)
        .send({
          user: email,
          password: global.adminPass,
        })
        .expect(200);

      const { body } = await global.testServer
        .get(`/contact/select`)
        .expect(200);

      body.response.forEach((element) => {
        expect(element).to.be.all.keys('id', 'name', 'email');
      });

      await admin.deleteOne();
      await userAdmin.deleteOne();
      await contact1.deleteOne();
      await contact2.deleteOne();
      await contact3.deleteOne();
      await state.deleteOne();
      await county.deleteOne();
    });
  });

  describe('UPDATE /admin/contact/:contactId', () => {
    it('should update the selected contact', async () => {
      const { state, county } = await insertStateAndCounty();
      const email = faker.internet.email();
      const anAdmin = await FUserAdmin(email);
      const [userAdmin, admin] = await Promise.all(anAdmin);
      const contact1 = await FContact(state._id, county._id);

      await global.testServer
        .post(`/admin/login`)
        .send({
          user: email,
          password: global.adminPass,
        })
        .expect(200);

      const dataToUpdate = {
        name: faker.name.firstName(),
        jobTitle: faker.name.jobDescriptor(),
        companyName: faker.name.jobType(),
        currentJob: faker.datatype.boolean(),
        email: faker.internet.email(),
        phone: faker.phone.phoneNumberFormat(),
        address: faker.address.direction(),
        city: faker.address.city(),
        state: state._id,
        county: county._id,
        zip: faker.address.zipCode(),
      };

      await global.testServer
        .put(`/admin/contact/${contact1._id}`)
        .send(dataToUpdate)
        .expect(200);

      const updatedContact: IModelContact = await ModelContact.findById(
        contact1._id
      );
      expect(updatedContact.name).to.be.equal(dataToUpdate.name);
      expect(updatedContact.jobTitle).to.be.equal(dataToUpdate.jobTitle);
      expect(updatedContact.companyName).to.be.equal(dataToUpdate.companyName);
      expect(updatedContact.currentJob).to.be.equal(dataToUpdate.currentJob);
      expect(updatedContact.email).to.be.equal(dataToUpdate.email);
      expect(updatedContact.phone).to.be.equal(dataToUpdate.phone);
      expect(updatedContact.address).to.be.equal(dataToUpdate.address);
      expect(updatedContact.city).to.be.equal(dataToUpdate.city);
      expect(updatedContact.city).to.be.equal(dataToUpdate.city);
      expect(updatedContact.zip).to.be.equal(dataToUpdate.zip);

      await admin.deleteOne();
      await userAdmin.deleteOne();
      await contact1.deleteOne();
      await state.deleteOne();
      await county.deleteOne();
    });
  });

  describe('DELETE /admin/contact/:contactId', () => {
    it('should update the selected contact', async () => {
      const { state, county } = await insertStateAndCounty();
      const email = faker.internet.email();
      const anAdmin = await FUserAdmin(email);
      const [userAdmin, admin] = await Promise.all(anAdmin);
      const contact1 = await FContact(state._id, county._id);

      await global.testServer
        .post(`/admin/login`)
        .send({
          user: email,
          password: global.adminPass,
        })
        .expect(200);

      await global.testServer
        .delete(`/admin/contact/${contact1._id}`)
        .expect(200);

      const updatedContact: IModelContact = await ModelContact.findById(
        contact1._id
      );
      expect(updatedContact).to.be.equal(null);

      await state.deleteOne();
      await county.deleteOne();
      await admin.deleteOne();
      await userAdmin.deleteOne();
      await contact1.deleteOne();
    });
  });
});
