import {
  FUserInvestor,
  FUserAdmin,
  FProperty,
  insertPropertyWithoutFinancial,
  FPackage,
  FPackageInvestor,
  FPackageProperty,
} from '../helpers';
import faker from 'faker';
import {
  ModelPackage,
  ModelPackageInvestor,
  ModelPackageProperty,
} from '@capa/data';
import { expect } from 'chai';
import Bignumber from 'bignumber.js';
import { Types } from 'mongoose';

describe('Package Controller Test', () => {
  describe('POST /admin/package', () => {
    it('should create a package', async () => {
      const emailAdmin = faker.internet.email();
      const email = faker.internet.email();
      const anAdmin = await FUserAdmin(emailAdmin);
      const anInvestor = await FUserInvestor(email);
      const [userAdmin, admin] = await Promise.all(anAdmin);
      const [user, investor] = await Promise.all(anInvestor);
      const thisProperty = await FProperty();
      const [property, propertyFinancial, propertyQuarter] = await Promise.all(
        thisProperty
      );

      await global.testServer
        .post(`/admin/login`)
        .send({
          user: emailAdmin,
          password: global.adminPass,
        })
        .expect(200);

      const name = faker.name.firstName();
      const properties = [
        {
          id: property._id,
          share: 100,
        },
      ];
      const investors = [
        {
          id: investor._id,
          minCommitment: faker.datatype.number(10000),
          initCommitment: faker.datatype.number(10000),
          totalCommitment: faker.datatype.number(10000),
          termYears: faker.datatype.number(10),
        },
      ];

      const { body } = await global.testServer
        .post(`/admin/package`)
        .send({
          name,
          totalValue: faker.datatype.number(10000),
          investors,
          properties,
        })
        .expect(200);

      const newPack = await ModelPackage.findById(body.response.id);
      const packInvestor = await ModelPackageInvestor.findOne({
        package: newPack._id,
      });
      const packProperty = await ModelPackageProperty.findOne({
        package: newPack._id,
      });

      expect(newPack.name).to.equal(name);
      expect(packInvestor.investor.toHexString()).to.equal(
        investor._id.toHexString()
      );
      expect((packProperty.property as Types.ObjectId).toHexString()).to.equal(
        property._id.toHexString()
      );

      await userAdmin.deleteOne();
      await user.deleteOne();
      await admin.deleteOne();
      await investor.deleteOne();
      await property.deleteOne();
      await propertyFinancial.deleteOne();
      await propertyQuarter.deleteOne();
    });
  });

  describe('POST /admin/package/preview', () => {
    it('should return a preview from the properties', async () => {
      const emailAdmin = faker.internet.email();
      const email = faker.internet.email();
      const anAdmin = await FUserAdmin(emailAdmin);
      const anInvestor = await FUserInvestor(email);
      const [userAdmin, admin] = await Promise.all(anAdmin);
      const [user, investor] = await Promise.all(anInvestor);
      const thisProperty = await FProperty();
      const [property, propertyFinancial, propertyQuarter] = await Promise.all(
        thisProperty
      );

      await global.testServer
        .post(`/admin/login`)
        .send({
          user: emailAdmin,
          password: global.adminPass,
        })
        .expect(200);

      const share = 40;
      const properties = [
        {
          id: property._id,
          share,
        },
      ];

      const { body } = await global.testServer
        .post(`/admin/package/preview`)
        .send({ properties })
        .expect(200);

      const { totalValue } = body.response;
      const shareBig = new Bignumber(share);
      const percentageBig = shareBig.dividedBy(100);
      const totalValueBig = new Bignumber(propertyFinancial.totalValue);
      const propertyValueBig = percentageBig.multipliedBy(totalValueBig);

      expect(totalValue).to.be.equal(
        parseFloat(propertyValueBig.toNumber().toFixed(2))
      );

      await userAdmin.deleteOne();
      await user.deleteOne();
      await admin.deleteOne();
      await investor.deleteOne();
      await property.deleteOne();
      await propertyFinancial.deleteOne();
      await propertyQuarter.deleteOne();
    });
  });

  describe('GET /package/select', () => {
    it('should return packages', async () => {
      await ModelPackage.deleteMany();
      await ModelPackageInvestor.deleteMany();
      await ModelPackageProperty.deleteMany();

      const emailAdmin = faker.internet.email();
      const email = faker.internet.email();
      const anAdmin = await FUserAdmin(emailAdmin);
      const anInvestor = await FUserInvestor(email);
      const [userAdmin, admin] = await Promise.all(anAdmin);
      const [user, investor] = await Promise.all(anInvestor);
      const thisProperty = await FProperty();
      const [property, propertyFinancial, propertyQuarter] = await Promise.all(
        thisProperty
      );

      await global.testServer
        .post(`/admin/login`)
        .send({
          user: emailAdmin,
          password: global.adminPass,
        })
        .expect(200);

      const pack = await FPackage();
      const packInvestor = await FPackageInvestor(
        pack._id.toHexString(),
        investor._id
      );
      const packProperty = await FPackageProperty(
        pack._id.toHexString(),
        property._id.toHexString()
      );

      const { body } = await global.testServer
        .get(`/package/select`)
        .expect(200);
      const { response } = body;

      response.forEach((e) => {
        expect(e).to.have.all.keys(
          'id',
          'name',
          'totalValue',
          'shareAvailable',
          'mainImage'
        );
      });

      await userAdmin.deleteOne();
      await user.deleteOne();
      await admin.deleteOne();
      await investor.deleteOne();
      await property.deleteOne();
      await propertyFinancial.deleteOne();
      await propertyQuarter.deleteOne();
      await pack.deleteOne();
      await packInvestor.deleteOne();
      await packProperty.deleteOne();
    });
  });

  describe('GET /admin/package/:packageId', () => {
    it('should get detail for a package', async () => {
      const emailAdmin = faker.internet.email();
      const email = faker.internet.email();
      const anAdmin = await FUserAdmin(emailAdmin);
      const anInvestor = await FUserInvestor(email);
      const [userAdmin, admin] = await Promise.all(anAdmin);
      const [user, investor] = await Promise.all(anInvestor);
      const property = await insertPropertyWithoutFinancial();

      await global.testServer
        .post(`/admin/login`)
        .send({
          user: emailAdmin,
          password: global.adminPass,
        })
        .expect(200);

      const pack = await FPackage();
      const packInvestor = await FPackageInvestor(
        pack._id.toHexString(),
        investor._id
      );
      const packProperty = await FPackageProperty(
        pack._id.toHexString(),
        property._id.toHexString()
      );

      const { body } = await global.testServer
        .get(`/admin/package/${pack._id.toHexString()}`)
        .expect(200);
      const { response } = body;

      expect(response).to.have.all.keys(
        'id',
        'name',
        'totalValue',
        'investors',
        'properties'
      );

      response.investors.forEach((inv) => {
        expect(inv).to.have.all.keys(
          'id',
          'name',
          'minCommitment',
          'initCommitment',
          'totalCommitment',
          'termYears'
        );
      });
      response.properties.forEach((prop) => {
        expect(prop).to.have.all.keys('id', 'name', 'share');
      });

      await userAdmin.deleteOne();
      await user.deleteOne();
      await admin.deleteOne();
      await investor.deleteOne();
      await property.deleteOne();
      await pack.deleteOne();
      await packInvestor.deleteOne();
      await packProperty.deleteOne();
    });
  });

  describe('UPDATE /admin/package/:packageId', () => {
    it('should update a created package', async () => {
      const emailAdmin = faker.internet.email();
      const email = faker.internet.email();
      const anAdmin = await FUserAdmin(emailAdmin);
      const anInvestor = await FUserInvestor(email);
      const [userAdmin, admin] = await Promise.all(anAdmin);
      const [user, investor] = await Promise.all(anInvestor);
      const property = await insertPropertyWithoutFinancial();
      const pack = await FPackage();
      const packInvestor = await FPackageInvestor(
        pack._id.toHexString(),
        investor._id
      );
      const packProperty = await FPackageProperty(
        pack._id.toHexString(),
        property._id.toHexString()
      );

      await global.testServer
        .post(`/admin/login`)
        .send({
          user: emailAdmin,
          password: global.adminPass,
        })
        .expect(200);

      const name = faker.name.firstName();
      const totalValue = faker.datatype.number(1000);
      const properties = [
        {
          id: property._id,
          share: 100,
        },
      ];
      const investors = [
        {
          id: investor._id,
          minCommitment: faker.datatype.number(10000),
          initCommitment: faker.datatype.number(10000),
          totalCommitment: faker.datatype.number(10000),
          termYears: faker.datatype.number(10),
        },
      ];

      await global.testServer
        .put(`/admin/package/${pack._id}`)
        .send({
          name,
          totalValue,
          investors,
          properties,
        })
        .expect(200);

      const updatedPack = await ModelPackage.findById(pack._id);
      const updatedPackInvestor = await ModelPackageInvestor.findOne({
        package: updatedPack._id,
      });
      const updatedPackProperty = await ModelPackageProperty.findOne({
        package: updatedPack._id,
      });

      expect(updatedPack.name).to.equal(name);
      expect(updatedPackInvestor.investor.toHexString()).to.equal(
        investor._id.toHexString()
      );
      expect(
        (updatedPackProperty.property as Types.ObjectId).toHexString()
      ).to.equal(property._id.toHexString());

      await userAdmin.deleteOne();
      await user.deleteOne();
      await admin.deleteOne();
      await investor.deleteOne();
      await property.deleteOne();
      await pack.deleteOne();
      await packInvestor.deleteOne();
      await packProperty.deleteOne();
    });
  });

  describe('DELETE /admin/package/:packageId', () => {
    it('should delete a created package', async () => {
      const emailAdmin = faker.internet.email();
      const email = faker.internet.email();
      const anAdmin = await FUserAdmin(emailAdmin);
      const anInvestor = await FUserInvestor(email);
      const [userAdmin, admin] = await Promise.all(anAdmin);
      const [user, investor] = await Promise.all(anInvestor);
      const property = await insertPropertyWithoutFinancial();
      const pack = await FPackage();
      const packInvestor = await FPackageInvestor(
        pack._id.toHexString(),
        investor._id
      );
      const packProperty = await FPackageProperty(
        pack._id.toHexString(),
        property._id.toHexString()
      );

      await global.testServer
        .post(`/admin/login`)
        .send({
          user: emailAdmin,
          password: global.adminPass,
        })
        .expect(200);

      await global.testServer.delete(`/admin/package/${pack._id}`).expect(200);

      const updatedPack = await ModelPackage.findById(pack._id);
      expect(updatedPack.active).to.be.equal(false);

      await userAdmin.deleteOne();
      await user.deleteOne();
      await admin.deleteOne();
      await investor.deleteOne();
      await property.deleteOne();
      await pack.deleteOne();
      await packInvestor.deleteOne();
      await packProperty.deleteOne();
      await updatedPack.deleteOne();
    });
  });
});
