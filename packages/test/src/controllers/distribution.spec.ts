import {
  FDistribution,
  FDistributionInvestor,
  factoryLlcs,
  FUserAdmin,
  FUserInvestor,
} from '../helpers';
import faker from 'faker';
import { IEnumTypesPayment } from '@capa/core';
import { expect } from 'chai';
import {
  IModelDistributionInvestor,
  ModelDistribution,
  ModelDistributionInvestor,
} from '@capa/data';

describe('Distributions Controller Test', () => {
  describe('POST /admin/llc/:llcId/distribution', () => {
    it('should create a new Distribution', async () => {
      const email = faker.internet.email();
      const emailInvestor = faker.internet.email();
      const anAdmin = await FUserAdmin(email);
      const anInvestor = await FUserInvestor(emailInvestor);
      const [userAdmin, admin] = await Promise.all(anAdmin);
      const [user, investor] = await Promise.all(anInvestor);
      const llc = await factoryLlcs();

      await global.testServer
        .post(`/admin/login`)
        .send({
          user: email,
          password: global.adminPass,
        })
        .expect(200);

      const dataDistribution = {
        description: faker.lorem.paragraph(1),
        distributionDate: new Date().toISOString(),
        yieldPeriod: 'January 2021',
        account: [
          {
            id: investor.id,
            paymentType: IEnumTypesPayment.WIRE,
            paymentNumber: 'number-1234.',
            amount: faker.datatype.number(1000),
          },
        ],
      };
      const { body } = await global.testServer
        .post(`/admin/llc/${llc.id}/distribution`)
        .send(dataDistribution)
        .expect(200);
      expect(body.response).to.have.property('id');
      const newDistribution = await ModelDistribution.findById(
        body.response.id
      );
      expect(newDistribution).not.to.be.null;

      await userAdmin.deleteOne();
      await admin.deleteOne();
      await user.deleteOne();
      await investor.deleteOne();
      await llc.deleteOne();
      await ModelDistribution.deleteMany();
    });
  });

  describe('GET /admin/llc/:llcId/transactions/distribution', () => {
    it('should list Distributions', async () => {
      const email = faker.internet.email();
      const emailInvestor = faker.internet.email();
      const anAdmin = await FUserAdmin(email);
      const anInvestor = await FUserInvestor(emailInvestor);
      const [userAdmin, admin] = await Promise.all(anAdmin);
      const [user, investor] = await Promise.all(anInvestor);
      const llc = await factoryLlcs();
      const distribution = await FDistribution(llc.id);
      await FDistributionInvestor(distribution.id, investor.id);

      await global.testServer
        .post(`/admin/login`)
        .send({
          user: email,
          password: global.adminPass,
        })
        .expect(200);

      const { body } = await global.testServer
        .get(`/admin/llc/${llc.id}/transactions/distribution`)
        .expect(200);

      expect(body.response).to.have.all.keys('pagination', 'details');
      expect(body.response.pagination).to.have.all.keys(
        'itemCount',
        'offset',
        'limit',
        'totalPages',
        'page',
        'nextPage',
        'prevPage'
      );
      body.response.details.forEach((e) => {
        expect(e).to.have.all.keys(
          'id',
          'distributionDate',
          'totalDistribution',
          'totalPaymentAmount',
          'totalOutstanding',
          'description',
          'parties',
          'yieldPeriod'
        );
      });

      await userAdmin.deleteOne();
      await admin.deleteOne();
      await user.deleteOne();
      await investor.deleteOne();
      await llc.deleteOne();
      await distribution.deleteOne();
      await ModelDistributionInvestor.deleteMany();
    });
  });

  describe('GET /admin/llc/:llcId/distribution/:distributionId', () => {
    it('should get details for a Distribution', async () => {
      const email = faker.internet.email();
      const emailInvestor = faker.internet.email();
      const anAdmin = await FUserAdmin(email);
      const anInvestor = await FUserInvestor(emailInvestor);
      const [userAdmin, admin] = await Promise.all(anAdmin);
      const [user, investor] = await Promise.all(anInvestor);
      const llc = await factoryLlcs();
      const distribution = await FDistribution(llc.id);
      await FDistributionInvestor(distribution.id, investor.id);

      await global.testServer
        .post(`/admin/login`)
        .send({
          user: email,
          password: global.adminPass,
        })
        .expect(200);

      const { body } = await global.testServer
        .get(`/admin/llc/${llc.id}/distribution/${distribution.id}`)
        .expect(200);

      expect(body.response).to.have.all.keys(
        'description',
        'distributionDate',
        'yieldPeriod',
        'account'
      );
      body.response.account.forEach((e) => {
        expect(e).to.have.all.keys(
          'id',
          'name',
          'amount',
          'paymentType',
          'paymentNumber'
        );
      });

      await userAdmin.deleteOne();
      await admin.deleteOne();
      await user.deleteOne();
      await investor.deleteOne();
      await llc.deleteOne();
      await distribution.deleteOne();
      await ModelDistributionInvestor.deleteMany();
    });
  });

  describe('GET /admin/llc/:llcId/distribution/:distributionId', () => {
    it('should get details for a Distribution', async () => {
      const email = faker.internet.email();
      const emailInvestor = faker.internet.email();
      const anAdmin = await FUserAdmin(email);
      const anInvestor = await FUserInvestor(emailInvestor);
      const [userAdmin, admin] = await Promise.all(anAdmin);
      const [user, investor] = await Promise.all(anInvestor);
      const llc = await factoryLlcs();
      const distribution = await FDistribution(llc.id);
      await FDistributionInvestor(distribution.id, investor.id);

      await global.testServer
        .post(`/admin/login`)
        .send({
          user: email,
          password: global.adminPass,
        })
        .expect(200);

      const { body } = await global.testServer
        .get(`/admin/llc/${llc.id}/distribution/${distribution.id}`)
        .expect(200);

      expect(body.response).to.have.all.keys(
        'description',
        'distributionDate',
        'yieldPeriod',
        'account'
      );
      body.response.account.forEach((e) => {
        expect(e).to.have.all.keys(
          'id',
          'name',
          'amount',
          'paymentType',
          'paymentNumber'
        );
      });

      await userAdmin.deleteOne();
      await admin.deleteOne();
      await user.deleteOne();
      await investor.deleteOne();
      await llc.deleteOne();
      await distribution.deleteOne();
      await ModelDistributionInvestor.deleteMany();
    });
  });

  describe('PUT /admin/llc/:llcId/distribution/:distributionId', () => {
    it('should update a created Distribution', async () => {
      const email = faker.internet.email();
      const emailInvestor1 = faker.internet.email();
      const emailInvestor2 = faker.internet.email();
      const anAdmin = await FUserAdmin(email);
      const anInvestor1 = await FUserInvestor(emailInvestor1);
      const anInvestor2 = await FUserInvestor(emailInvestor2);
      const [userAdmin, admin] = await Promise.all(anAdmin);
      const [user1, investor1] = await Promise.all(anInvestor1);
      const [user2, investor2] = await Promise.all(anInvestor2);
      const llc = await factoryLlcs();
      const distribution = await FDistribution(llc.id);
      await FDistributionInvestor(distribution.id, investor1.id);

      await global.testServer
        .post(`/admin/login`)
        .send({
          user: email,
          password: global.adminPass,
        })
        .expect(200);

      const dataUpdateDistr = {
        description: 'New distribution',
        distributionDate: new Date(),
        yieldPeriod: 'February 2021',
        account: [
          {
            id: investor2.id,
            paymentType: IEnumTypesPayment.WIRE,
            paymentNumber: 'number-1234.',
            amount: faker.datatype.number(1000),
          },
        ],
      };

      const { body } = await global.testServer
        .put(`/admin/llc/${llc.id}/distribution/${distribution.id}`)
        .send(dataUpdateDistr)
        .expect(200);

      expect(body.response).to.have.property('id');
      const updateDistribution = await ModelDistribution.findById(
        distribution.id
      );
      expect(updateDistribution.description).to.be.equal(
        dataUpdateDistr.description
      );
      expect(updateDistribution.distributionDate.toISOString()).to.be.equal(
        dataUpdateDistr.distributionDate.toISOString()
      );
      expect(updateDistribution.yieldPeriod).to.be.equal(
        dataUpdateDistr.yieldPeriod
      );

      const distributionInv1: IModelDistributionInvestor =
        await ModelDistributionInvestor.findOne({
          distribution: distribution.id,
          investor: investor1.id,
          'deletedBy.dateTime': null,
        });
      expect(distributionInv1).to.be.null;

      const distributionInv2: IModelDistributionInvestor =
        await ModelDistributionInvestor.findOne({
          distribution: distribution.id,
          investor: investor2.id,
          'deletedBy.dateTime': null,
        });
      expect(distributionInv2).not.to.be.null;

      await userAdmin.deleteOne();
      await admin.deleteOne();
      await user1.deleteOne();
      await user2.deleteOne();
      await investor1.deleteOne();
      await investor2.deleteOne();
      await llc.deleteOne();
      await distribution.deleteOne();
      await ModelDistributionInvestor.deleteMany();
    });
  });

  describe('DELETE /admin/llc/:llcId/distribution/:distributionId', () => {
    it('should delete a created Distribution', async () => {
      const email = faker.internet.email();
      const emailInvestor1 = faker.internet.email();
      const anAdmin = await FUserAdmin(email);
      const anInvestor1 = await FUserInvestor(emailInvestor1);
      const [userAdmin, admin] = await Promise.all(anAdmin);
      const [user1, investor1] = await Promise.all(anInvestor1);
      const llc = await factoryLlcs();
      const distribution = await FDistribution(llc.id);
      await FDistributionInvestor(distribution.id, investor1.id);

      await global.testServer
        .post(`/admin/login`)
        .send({
          user: email,
          password: global.adminPass,
        })
        .expect(200);

      const { body } = await global.testServer
        .delete(`/admin/llc/${llc.id}/distribution/${distribution.id}`)
        .expect(200);
      expect(body).to.have.property('response');

      const deleteDistribution = await ModelDistribution.findOne({
        llc: llc.id,
        'deletedBy.dateTime': null,
      });
      expect(deleteDistribution).to.be.null;

      const deletedDistributionInv: IModelDistributionInvestor =
        await ModelDistributionInvestor.findOne({
          distribution: distribution.id,
          investor: investor1.id,
          'deletedBy.dateTime': null,
        });
      expect(deletedDistributionInv).to.be.null;

      await userAdmin.deleteOne();
      await admin.deleteOne();
      await user1.deleteOne();
      await investor1.deleteOne();
      await llc.deleteOne();
      await distribution.deleteOne();
      await ModelDistributionInvestor.deleteMany();
    });
  });
});
