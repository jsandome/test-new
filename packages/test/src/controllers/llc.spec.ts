import 'mocha';
import { expect } from 'chai';
import { ModelLlc, ModelLlcInvestor, ModelLlcProperty } from '@capa/data';
import faker from 'faker';
import {
  FUserAdmin,
  FUserInvestor,
  FProperty,
  insertStateAndCounty,
  factoryLlcs,
  factoryLLCProperty,
  factoryLLCInvestor,
  cleanLLC,
} from '../helpers';

describe('LLC´S -  Entities Testing API', async () => {
  before(cleanLLC);

  describe('POST /admin/llc', async () => {
    it('should create a LLC', async () => {
      const { state } = await insertStateAndCounty();

      const emailAdmin = faker.internet.email();
      const email = faker.internet.email();
      const anAdmin = await FUserAdmin(emailAdmin);
      const anInvestor = await FUserInvestor(email);
      const [userAdmin, admin] = await Promise.all(anAdmin);
      const [user, investor] = await Promise.all(anInvestor);
      const thisProperty = await FProperty();
      const [property] = await Promise.all(thisProperty);

      await global.testServer
        .post(`/admin/login`)
        .send({
          user: emailAdmin,
          password: global.adminPass,
        })
        .expect(200);

      const properties = [
        {
          id: property._id,
          share: 100,
        },
      ];
      const investors = [investor._id];
      const date = new Date();

      let month = '' + (date.getMonth() + 1);
      let day = '' + date.getDate();
      let year = date.getFullYear();

      if (month.length < 2) month = '0' + month;
      if (day.length < 2) day = '0' + day;

      const { body } = await global.testServer
        .post(`/admin/llc`)
        .send({
          name: faker.name.lastName(),
          usPerson: faker.datatype.boolean(),
          taxId: faker.datatype.uuid(),
          address: faker.address.direction(),
          state: state._id,
          date: [year, month, day].join('-'),
          properties,
          investors,
        })
        .expect(200);

      expect(body).to.have.property('response');
      expect(body).to.have.property('message');
      expect(body.response).to.have.all.keys('id');

      await userAdmin.deleteOne();
      await user.deleteOne();
      await admin.deleteOne();
      await investor.deleteOne();
      await property.deleteOne();
      await state.deleteOne();
      await ModelLlc.deleteMany();
      await ModelLlcInvestor.deleteMany();
      await ModelLlcProperty.deleteMany();
    });

    it('throw exception creating a new Entity with same name', async () => {
      const { state } = await insertStateAndCounty();
      const emailAdmin = faker.internet.email();
      const email = faker.internet.email();
      const anAdmin = await FUserAdmin(emailAdmin);
      const anInvestor = await FUserInvestor(email);
      const [userAdmin, admin] = await Promise.all(anAdmin);
      const [user, investor] = await Promise.all(anInvestor);
      const thisProperty = await FProperty();
      const [property] = await Promise.all(thisProperty);
      const llcs = await factoryLlcs();

      await global.testServer
        .post(`/admin/login`)
        .send({
          user: emailAdmin,
          password: global.adminPass,
        })
        .expect(200);

      const properties = [
        {
          id: property._id,
          share: 100,
        },
      ];
      const investors = [investor._id];
      const date = new Date();

      let month = '' + (date.getMonth() + 1);
      let day = '' + date.getDate();
      let year = date.getFullYear();

      if (month.length < 2) month = '0' + month;
      if (day.length < 2) day = '0' + day;

      await global.testServer
        .post(`/admin/llc`)
        .send({
          name: llcs.name,
          usPerson: faker.datatype.boolean(),
          taxId: faker.datatype.uuid(),
          address: faker.address.direction(),
          state: state._id,
          date: [year, month, day].join('-'),
          properties,
          investors,
        })
        .expect(409);

      await userAdmin.deleteOne();
      await user.deleteOne();
      await admin.deleteOne();
      await investor.deleteOne();
      await property.deleteOne();
      await state.deleteOne();
      await ModelLlc.deleteMany();
      await ModelLlcInvestor.deleteMany();
      await ModelLlcProperty.deleteMany();
    });
  });

  describe('GET /llc/select', async () => {
    it('should return LLC', async () => {
      const emailAdmin = faker.internet.email();
      const email = faker.internet.email();
      const anAdmin = await FUserAdmin(emailAdmin);
      const anInvestor = await FUserInvestor(email);
      const [userAdmin, admin] = await Promise.all(anAdmin);
      const [user, investor] = await Promise.all(anInvestor);
      const thisProperty = await FProperty();
      const [property] = await Promise.all(thisProperty);
      const llcs = await factoryLlcs();

      await global.testServer
        .post(`/admin/login`)
        .send({
          user: emailAdmin,
          password: global.adminPass,
        })
        .expect(200);

      const { body } = await global.testServer
        .get(`/llc/select?search=${llcs.name}`)
        .expect(200);
      const { response } = body;

      response.forEach((element) => {
        expect(element).to.have.all.keys(
          'id',
          'state',
          'name',
          'usPerson',
          'taxId',
          'address',
          'date',
          'active'
        );
      });
      await userAdmin.deleteOne();
      await user.deleteOne();
      await admin.deleteOne();
      await investor.deleteOne();
      await property.deleteOne();
      await ModelLlc.deleteMany();
      await ModelLlcInvestor.deleteMany();
      await ModelLlcProperty.deleteMany();
    });
  });

  describe('DELETE /admin/llc', async () => {
    it('should delete an LLC ', async () => {
      const emailAdmin = faker.internet.email();
      const email = faker.internet.email();
      const anAdmin = await FUserAdmin(emailAdmin);
      const anInvestor = await FUserInvestor(email);
      const [userAdmin, admin] = await Promise.all(anAdmin);
      const [user, investor] = await Promise.all(anInvestor);
      const thisProperty = await FProperty();
      const [property] = await Promise.all(thisProperty);
      const llcs = await factoryLlcs();

      await global.testServer
        .post(`/admin/login`)
        .send({
          user: emailAdmin,
          password: global.adminPass,
        })
        .expect(200);

      const { body } = await global.testServer
        .delete(`/admin/llc/${llcs._id}`)
        .expect(200);

      const updatedLlc = await ModelLlc.findOne({ _id: llcs._id });
      expect(updatedLlc.active).to.be.equal(false);
      expect(body).to.have.all.keys('message', 'response');

      await userAdmin.deleteOne();
      await user.deleteOne();
      await admin.deleteOne();
      await investor.deleteOne();
      await property.deleteOne();
      await ModelLlc.deleteMany();
      await ModelLlcInvestor.deleteMany();
      await ModelLlcProperty.deleteMany();
    });
  });

  describe('PUT /admin/llc', async () => {
    it('should update LLC', async () => {
      const { state } = await insertStateAndCounty();

      const emailAdmin = faker.internet.email();
      const email = faker.internet.email();
      const anAdmin = await FUserAdmin(emailAdmin);
      const anInvestor = await FUserInvestor(email);
      const [userAdmin, admin] = await Promise.all(anAdmin);
      const [user, investor] = await Promise.all(anInvestor);
      const thisProperty = await FProperty();
      const [property] = await Promise.all(thisProperty);
      const llcs = await factoryLlcs();

      const llcInvestor = await factoryLLCInvestor(
        llcs._id.toHexString(),
        investor._id
      );
      const llcProperty = await factoryLLCProperty(
        llcs._id.toHexString(),
        property._id.toHexString()
      );

      await global.testServer
        .post(`/admin/login`)
        .send({
          user: emailAdmin,
          password: global.adminPass,
        })
        .expect(200);

      const properties = [
        {
          id: property._id,
          share: 100,
        },
      ];
      const investors = [investor._id];
      const date = new Date();

      let month = '' + (date.getMonth() + 1);
      let day = '' + date.getDate();
      let year = date.getFullYear();

      if (month.length < 2) month = '0' + month;
      if (day.length < 2) day = '0' + day;

      const { body } = await global.testServer
        .put(`/admin/llc/${llcs.id}`)
        .send({
          name: llcs.name,
          usPerson: faker.datatype.boolean(),
          taxId: llcs.taxId,
          address: faker.address.streetAddress(),
          state: state._id,
          date: [year, month, day].join('-'),
          properties,
          investors,
        })
        .expect(200);

      expect(body).to.have.all.keys('message', 'response');

      await userAdmin.deleteOne();
      await user.deleteOne();
      await admin.deleteOne();
      await investor.deleteOne();
      await property.deleteOne();
      await llcs.deleteOne();
      await llcInvestor.deleteOne();
      await llcProperty.deleteOne();
      await state.deleteOne();
    });
  });

  describe('GET /admin/llc/list', async () => {
    it('should return list LLC with pagination ', async () => {
      const emailAdmin = faker.internet.email();
      const email = faker.internet.email();
      const anAdmin = await FUserAdmin(emailAdmin);
      const anInvestor = await FUserInvestor(email);
      const [userAdmin, admin] = await Promise.all(anAdmin);
      const [user, investor] = await Promise.all(anInvestor);
      const thisProperty = await FProperty();
      const [property] = await Promise.all(thisProperty);
      await factoryLlcs();

      await global.testServer
        .post(`/admin/login`)
        .send({
          user: emailAdmin,
          password: global.adminPass,
        })
        .expect(200);

      const { body } = await global.testServer
        .get(`/admin/llc/list`)
        .expect(200);

      expect(body).to.have.all.keys('response');

      await userAdmin.deleteOne();
      await user.deleteOne();
      await admin.deleteOne();
      await investor.deleteOne();
      await property.deleteOne();
      await ModelLlc.deleteMany();
      await ModelLlcInvestor.deleteMany();
      await ModelLlcProperty.deleteMany();
    });
  });

  describe('GET /admin/llc/:llcId/overview', async () => {
    it('should return details overview llc', async () => {
      const emailAdmin = faker.internet.email();
      const email = faker.internet.email();
      const anAdmin = await FUserAdmin(emailAdmin);
      const anInvestor = await FUserInvestor(email);
      const [userAdmin, admin] = await Promise.all(anAdmin);
      const [user, investor] = await Promise.all(anInvestor);
      const thisProperty = await FProperty();
      const [property] = await Promise.all(thisProperty);
      const llcs = await factoryLlcs();

      await global.testServer
        .post(`/admin/login`)
        .send({
          user: emailAdmin,
          password: global.adminPass,
        })
        .expect(200);

      const { body } = await global.testServer
        .get(`/admin/llc/${llcs.id}/overview`)
        .expect(200);

      expect(body).to.have.all.keys('response');
      expect(body).to.have.keys({
        response: {
          entity: {
            name: `${faker.name.findName()}`,
            usPerson: `${faker.datatype.float()}`,
            taxId: `${faker.datatype.uuid()}`,
            address: `${faker.address.streetAddress()}`,
            date: `${faker.datatype.datetime()}`,
          },
          investors: [
            {
              id: `${faker.datatype.uuid()}`,
              name: `${faker.name.findName()}`,
            },
          ],
          properties: [
            {
              id: `${faker.datatype.uuid()}`,
              name: `${faker.name.firstName()}`,
              address: `${faker.address.state()}`,
              projectedIRR: `${faker.datatype.float()}`,
              myShare: `${faker.datatype.float()}`,
              estimatedValue: `${faker.datatype.float()}`,
              loanAmmount: `${faker.datatype.float()}`,
              estimateEquity: `${faker.datatype.float()}`,
              type: `${faker.name.firstName()}`,
              pictures: `${faker.datatype.array()}`,
              mainPicture: `${faker.name.title()}`,
            },
          ],
        },
      });

      await userAdmin.deleteOne();
      await user.deleteOne();
      await admin.deleteOne();
      await investor.deleteOne();
      await property.deleteOne();
      await ModelLlc.deleteMany();
      await ModelLlcInvestor.deleteMany();
      await ModelLlcProperty.deleteMany();
    });
  });

  describe('GET /admin/llc/:llcId', async () => {
    it('should return details LLC', async () => {
      const emailAdmin = faker.internet.email();
      const email = faker.internet.email();
      const anAdmin = await FUserAdmin(emailAdmin);
      const anInvestor = await FUserInvestor(email);
      const [userAdmin, admin] = await Promise.all(anAdmin);
      const [user, investor] = await Promise.all(anInvestor);
      const thisProperty = await FProperty();
      const [property] = await Promise.all(thisProperty);
      const llcs = await factoryLlcs();

      await global.testServer
        .post(`/admin/login`)
        .send({
          user: emailAdmin,
          password: global.adminPass,
        })
        .expect(200);

      const { body } = await global.testServer
        .get(`/admin/llc/${llcs.id}`)
        .expect(200);

      expect(body).to.have.all.keys('response');
      expect(body).to.have.keys({
        response: {
          name: `${faker.name.findName()}`,
          usPerson: `${faker.datatype.float()}`,
          taxId: `${faker.datatype.uuid()}`,
          address: `${faker.address.streetAddress()}`,
          date: `${faker.datatype.datetime()}`,
          state: `${faker.datatype.uuid()}`,
          investors: [
            {
              id: `${faker.datatype.uuid()}`,
              name: `${faker.name.findName()}`,
            },
          ],
          properties: [
            {
              id: `${faker.datatype.uuid()}`,
              name: `${faker.name.firstName()}`,
              share: `${faker.datatype.float()}`,
            },
          ],
        },
      });

      await userAdmin.deleteOne();
      await user.deleteOne();
      await admin.deleteOne();
      await investor.deleteOne();
      await property.deleteOne();
      await ModelLlc.deleteMany();
      await ModelLlcInvestor.deleteMany();
      await ModelLlcProperty.deleteMany();
    });
  });

  describe('GET /admin/llc/:llcId/preview', async () => {
    it('should return preview LLC ', async () => {
      const emailAdmin = faker.internet.email();
      const email = faker.internet.email();
      const anAdmin = await FUserAdmin(emailAdmin);
      const anInvestor = await FUserInvestor(email);
      const [userAdmin, admin] = await Promise.all(anAdmin);
      const [user, investor] = await Promise.all(anInvestor);
      const thisProperty = await FProperty();
      const [property] = await Promise.all(thisProperty);
      const llcs = await factoryLlcs();

      await global.testServer
        .post(`/admin/login`)
        .send({
          user: emailAdmin,
          password: global.adminPass,
        })
        .expect(200);

      const { body } = await global.testServer
        .get(`/admin/llc/${llcs.id}/preview`)
        .expect(200);
      const { response } = body;
      expect(body).to.have.all.keys('response');
      response.activity.forEach((a) => {
        expect(a).to.have.all.keys('date', 'description');
      });

      await userAdmin.deleteOne();
      await user.deleteOne();
      await admin.deleteOne();
      await investor.deleteOne();
      await property.deleteOne();
      await ModelLlc.deleteMany();
      await ModelLlcInvestor.deleteMany();
      await ModelLlcProperty.deleteMany();
      await llcs.deleteOne();
    });
  });
});
