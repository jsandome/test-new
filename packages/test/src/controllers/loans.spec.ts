import {
  IModelLoan,
  IModelUser,
  ModelLoan,
  ModelLoanProperty,
} from '@capa/data';
import { expect } from 'chai';
import faker from 'faker';
import {
  insertLoan,
  cleanLoans,
  insertPropertyWithoutFinancial,
  FUserAdmin,
} from '../helpers';

describe('Loan Controller Test', () => {
  before(cleanLoans);
  describe('POST /admin/loan', () => {
    it('should return ID of the created Loan', async () => {
      const [user, admin] = await FUserAdmin(faker.internet.email());
      await global.testServer
        .post(`/admin/login`)
        .send({
          user: (user as IModelUser).email,
          password: global.adminPass,
        })
        .expect(200);
      const property = await insertPropertyWithoutFinancial();
      const newLoan = await global.testServer
        .post(`/admin/loan`)
        .send({
          name: faker.datatype.string(10),
          amount: faker.datatype.number({
            precision: 0.01,
            max: 499999,
            min: 0.1,
          }),
          interest: faker.datatype.number({
            precision: 0.01,
            max: 99,
            min: 0.1,
          }),
          termYears: faker.datatype.number({ max: 49, min: 1 }),
          amortizationYears: faker.datatype.number({ max: 49, min: 1 }),
          beginningDate: '2021-03-31T22:25:39+0000',
          extraPayments: [
            {
              name: 'Extra payment 1',
              amount: 1005.56,
              paymentNumbers: [1, 5, 90],
            },
          ],
          properties: [
            {
              id: property._id,
              share: 100,
            },
          ],
        })
        .expect(200);
      const { body } = newLoan;
      expect(body).to.have.all.keys('response', 'message', 'details');
      expect(body.response).to.have.key('id');
      expect(body.response.id).to.be.string;
      const loan = await ModelLoan.findOne({ _id: body.response.id });
      await property.deleteOne();
      await loan.deleteOne();
      await user.deleteOne();
      await admin.deleteOne();
    });
  });

  describe('POST /admin/loan/preview/amortization-schedule', () => {
    it('should return amortization schedule preview', async () => {
      const [user, admin] = await FUserAdmin(faker.internet.email());
      await global.testServer
        .post(`/admin/login`)
        .send({
          user: (user as IModelUser).email,
          password: global.adminPass,
        })
        .expect(200);
      const scheduleAmortization = await global.testServer
        .post(`/admin/loan/preview/amortization-schedule`)
        .send({
          name: faker.datatype.string(),
          interest: 8.5,
          amortizationYears: 30,
          termYears: 30,
          amount: 200000,
          scheduleExtraPayment: 600,
          paymentsPerYear: 12,
          beginningDate: '2021-03-31T22:25:39+0000',
          extraPayments: [
            {
              name: faker.datatype.string(),
              amount: 500,
              paymentNumbers: [1, 2, 3, 4],
            },
          ],
          properties: [],
        })
        .expect(200);
      const { body } = scheduleAmortization;
      expect(body).to.have.key('response');
      expect(body.response).to.have.all.keys(
        'interestRate',
        'paymentsPerYear',
        'amortizationYears',
        'amount',
        'schedulePayment',
        'programedNumberPayments',
        'totalNumberPayments',
        'totalEarlyPayments',
        'totalInterest',
        'generalPropertyInfo',
        'amortizationSchedule'
      );
      const {
        schedulePayment,
        programedNumberPayments,
        totalNumberPayments,
        totalEarlyPayments,
        totalInterest,
      } = body.response;

      expect(schedulePayment).to.be.equal(1537.8269671686671);
      expect(programedNumberPayments).to.be.equal(360);
      expect(totalNumberPayments).to.be.equal(152);
      expect(totalEarlyPayments).to.be.equal(92600);
      expect(totalInterest).to.be.equal(125344.327819367);
      await admin.deleteOne();
      await user.deleteOne();
    });
  });

  describe('DELETE /admin/loan/:loanId', () => {
    it('should delete a Loan', async () => {
      const [user, admin] = await FUserAdmin(faker.internet.email());
      await global.testServer
        .post(`/admin/login`)
        .send({
          user: (user as IModelUser).email,
          password: global.adminPass,
        })
        .expect(200);
      const [loan, property] = await insertLoan();
      const deleteLoan = await global.testServer
        .delete(`/admin/loan/${loan._id}`)
        .expect(200);
      const { body } = deleteLoan;
      expect(body).to.have.all.keys('message', 'response');
      const deletedLoan = await ModelLoan.findOne({ _id: loan._id });
      expect(deletedLoan).to.be.equal(null);
      await loan.deleteOne();
      await property.deleteOne();
      await property.deleteOne();
      await admin.deleteOne();
      await user.deleteOne();
    });
  });

  describe('GET /admin/loan', () => {
    it('should get list of loans', async () => {
      await ModelLoan.deleteMany();
      const [user, admin] = await FUserAdmin(faker.internet.email());
      await global.testServer
        .post(`/admin/login`)
        .send({
          user: (user as IModelUser).email,
          password: global.adminPass,
        })
        .expect(200);
      const [loan, property] = await insertLoan();
      const getLoans = await global.testServer.get(`/loan/select`).expect(200);
      expect(getLoans.body).to.have.all.keys('response');
      const loans = getLoans.body.response;
      expect(loans).to.have.length.greaterThan(0);
      loans.forEach((loan) => {
        expect(loan).to.have.all.keys(
          'id',
          'name',
          'amount',
          'interest',
          'amortizationYears',
          'termYears',
          'beginningDate',
          'extraPayments',
          'properties'
        );
        expect(loan.id).to.be.a.string;
        expect(loan.name).to.be.a.string;
        expect(loan.amount).to.be.greaterThan(0);
        expect(loan.interest).to.be.a.greaterThan(0);
        expect(loan.amortizationYears).to.be.a.greaterThan(0);
        expect(loan.termYears).to.be.a.greaterThan(0);
        expect(loan.beginningDate).to.be.a.string;
      });
      await ModelLoanProperty.deleteMany({
        _id: {
          $in: (loan as IModelLoan).properties.map((p) => p),
        },
      });
      await loan.deleteOne();
      await property.deleteOne();
      await admin.deleteOne();
      await user.deleteOne();
    });
  });
  describe('GET /admin/loan/:loanId', () => {
    it('should update a loan', async () => {
      await ModelLoan.deleteMany();
      const [loan, property] = await insertLoan();
      const [user, admin] = await FUserAdmin(faker.internet.email());
      await global.testServer
        .post(`/admin/login`)
        .send({
          user: (user as IModelUser).email,
          password: global.adminPass,
        })
        .expect(200);
      const updateLoan = await global.testServer
        .put(`/admin/loan/${loan._id}`)
        .send({
          name: 'Updated',
          amount: 10000,
          interest: 5,
          termYears: 12,
          amortizationYears: 13,
          beginningDate: '2021-07-20T22:25:39+0000',
          extraPayments: [
            {
              name: 'Extra payment Updated 1',
              amount: 100,
              paymentNumbers: [15],
            },
          ],
          properties: [
            {
              id: property._id,
              share: 100,
            },
          ],
        })
        .expect(200);
      const { body } = updateLoan;
      expect(body).to.have.all.keys('message', 'response');
      await loan.deleteOne();
      await property.deleteOne();
      await admin.deleteOne();
      await user.deleteOne();
    });
  });
});

describe('Fail Loan Tests', () => {
  before(cleanLoans);
  it('should fail when create a loan with a existing name', async () => {
    const loanName = 'Loan duplicate Name';
    const [loan, property] = await insertLoan(loanName);
    const [user, admin] = await FUserAdmin(faker.internet.email());
    await global.testServer
      .post(`/admin/login`)
      .send({
        user: (user as IModelUser).email,
        password: global.adminPass,
      })
      .expect(200);
    await global.testServer
      .post(`/admin/loan`)
      .send({
        name: loanName,
        amount: faker.datatype.number({
          precision: 0.01,
          max: 499999,
          min: 0.1,
        }),
        interest: faker.datatype.number({
          precision: 0.01,
          max: 99,
          min: 0.1,
        }),
        termYears: faker.datatype.number({ max: 49, min: 1 }),
        amortizationYears: faker.datatype.number({ max: 49, min: 1 }),
        beginningDate: '2021-03-31T22:25:39+0000',
        extraPayments: [
          {
            name: 'Extra payment 1',
            amount: 1005.56,
            paymentNumbers: [1, 5, 90],
          },
        ],
        properties: [
          {
            id: property._id,
            share: 100,
          },
        ],
      })
      .expect(409);
    await loan.deleteOne();
    await property.deleteOne();
    await admin.deleteOne();
    await user.deleteOne();
  });
  it('should fail when update a loan with a existing name', async () => {
    const loanName = 'Loan original Name';
    const loanName2 = 'Loan duplicate Name';

    const [loan, property] = await insertLoan(loanName);
    const [loan2, property2] = await insertLoan(loanName2);
    const [user, admin] = await FUserAdmin(faker.internet.email());
    await global.testServer
      .post(`/admin/login`)
      .send({
        user: (user as IModelUser).email,
        password: global.adminPass,
      })
      .expect(200);
    await global.testServer
      .put(`/admin/loan/${loan._id}`)
      .send({
        name: loan2.name,
        amount: 10000,
        interest: 5,
        termYears: 12,
        amortizationYears: 13,
        beginningDate: '2021-07-20T22:25:39+0000',
        extraPayments: [
          {
            name: 'Extra payment Updated 1',
            amount: 100,
            paymentNumbers: [15],
          },
        ],
        properties: [
          {
            id: property._id,
            share: 100,
          },
        ],
      })
      .expect(409);
    await loan.deleteOne();
    await property.deleteOne();
    await admin.deleteOne();
    await user.deleteOne();
    await loan.deleteOne();
    await property.deleteOne();
    await loan2.deleteOne();
    await property2.deleteOne();
  });
});
