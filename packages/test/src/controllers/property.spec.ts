import 'mocha';
import {
  EnumPropertyTypes,
  IBaseEntityPropertyFinancialCreate,
  IBaseEntityProperty,
} from '@capa/core';
import { expect } from 'chai';
import {
  ModelProperty,
  ModelPropertyFinancial,
  IModelUser,
  IModelProperty,
} from '@capa/data';
import faker from 'faker';
import {
  insertPropertyWithoutFinancial,
  insertStateAndCounty,
  FProperty,
  FUserAdmin,
  between,
  getEnumKey,
  insertPropertyWithPackage,
  insertPropertyWithFund,
  cleanProperty,
  cleanFunds,
  cleanPackage,
  propertyPreviewBody1,
  expectedEstrategiesResultPreview1,
  Counties,
  States,
} from '../helpers';
import Bignumber from 'bignumber.js';
import { DateTime } from 'luxon';

const checkGetPropertiesFundProperty = (property) => {
  expect(property).to.have.all.keys(
    'id',
    'name',
    'address',
    'type',
    'location',
    'estimatedValue',
    'fundsShare',
    'myShare',
    'fundEquity',
    'myEquity',
    'projectedIRR',
    'projectEquityMultiple',
    'state',
    'pictures',
    'mainPicture'
  );
};

const checkGetPropertiesPackageAndSingleProperty = (property) => {
  expect(property).to.have.all.keys(
    'id',
    'name',
    'address',
    'type',
    'location',
    'estimatedValue',
    'myShare',
    'projectedIRR',
    'loanAmmount',
    'state',
    'estimateEquity',
    'pictures',
    'mainPicture'
  );
};

describe('Property test', () => {
  describe('GET /property/types', () => {
    it('should return types of property', async () => {
      const { body } = await global.testServer
        .get(`/property/types`)
        .expect(200);
      const { response } = body;

      response.forEach((e) => {
        expect(e).to.have.all.keys('id', 'label');
      });
    });
  });

  describe('GET /property/strategies', () => {
    it('should return strategies of property', async () => {
      const { body } = await global.testServer
        .get(`/property/strategies`)
        .expect(200);
      const { response } = body;

      response.forEach((e) => {
        expect(e).to.have.all.keys('id', 'label');
      });
    });
  });
});

describe('Property CRUD', () => {
  it('POST /admin/property', async () => {
    const { state, county } = await insertStateAndCounty();
    const propertyData: IBaseEntityProperty = {
      name: `Test: ${new Date().getTime()}`,
      address: `Amsterdam 173-int. ${new Date().getTime()}, Hipódromo, Cuauhtémoc, 06100 Ciudad de México, CDMX`,
      type: EnumPropertyTypes[getEnumKey(EnumPropertyTypes)],
      state: state._id,
      county: county._id,
      bedRooms: faker.datatype.number(10),
      bathRooms: faker.datatype.number(10),
      size: faker.datatype.number(500),
      parcelSize: faker.datatype.number(100),
      parcelNumber: faker.datatype.number(100),
      strategies: {
        buyAndHold: faker.datatype.boolean(),
        developAndHold: faker.datatype.boolean(),
        developAndSell: faker.datatype.boolean(),
        inmediateSell: faker.datatype.boolean(),
        repositionAndHold: faker.datatype.boolean(),
        repositionAndSell: faker.datatype.boolean(),
      },
      propertyDescription: faker.datatype.string(25),
      pictures: [faker.image.imageUrl(), faker.image.imageUrl()],
      mainPicture: faker.image.imageUrl(),
      location: {
        lat: faker.address.latitude(),
        lng: faker.address.longitude(),
      },
      investors: [],
    };
    const [user, admin] = await FUserAdmin(faker.internet.email());
    await global.testServer
      .post(`/admin/login`)
      .send({
        user: (user as IModelUser).email,
        password: global.adminPass,
      })
      .expect(200);
    const response = await global.testServer
      .post('/admin/property')
      .send(propertyData)
      .expect(200);
    const { body } = response;
    expect(body).to.have.property('response');
    expect(body.response).to.have.all.keys('id');

    await state.deleteOne();
    await county.deleteOne();
    await ModelProperty.deleteOne({ _id: body.response.id });
    await user.deleteOne();
    await admin.deleteOne();
  });

  it('POST /admin/property/:propertyId/financial', async () => {
    const year = new Date().getFullYear();
    const quarter = between(1, 4);
    const property = await insertPropertyWithoutFinancial();
    const propertyFinancialData: IBaseEntityPropertyFinancialCreate = {
      totalValue: faker.datatype.number(100),
      initialInvestment: faker.datatype.number(100),
      purchasePrice: faker.datatype.number(100),
      aquisitionFee: faker.datatype.number(100),
      dispositionFee: faker.datatype.number(100),
      amountAllocatedToLand: faker.datatype.number(100),
      depreciation: faker.datatype.number(100),
      commissions: faker.datatype.number(100),
      downPayment: faker.datatype.number(100),
      capitalGainsTaxPaid: faker.datatype.number(100),
      salesPrice: {
        year1: faker.datatype.number(100),
        year2: faker.datatype.number(100),
        year3: faker.datatype.number(100),
        year4: faker.datatype.number(100),
        year5: faker.datatype.number(100),
      },
      year: year,
      quarter: quarter,
      totalUnits: faker.datatype.number(5),
      rents: {
        month1: faker.datatype.number(150),
        month2: faker.datatype.number(150),
        month3: faker.datatype.number(150),
      },
      propertyManagement: faker.datatype.number(150),
      assetManagement: faker.datatype.number(150),
      repairsMaintenance: faker.datatype.number(150),
      taxes: faker.datatype.number(150),
      insurance: faker.datatype.number(150),
      reserve: faker.datatype.number(150),
      vacancy: faker.datatype.number(150),
      operativeExpenses: [],
    };
    const [user, admin] = await FUserAdmin(faker.internet.email());
    await global.testServer
      .post(`/admin/login`)
      .send({
        user: (user as IModelUser).email,
        password: global.adminPass,
      })
      .expect(200);
    const { body } = await global.testServer
      .post(`/admin/property/${property._id}/financial`)
      .send(propertyFinancialData)
      .expect(200);

    expect(body).to.have.property('response');
    expect(body.response).to.have.all.keys(
      'financialId',
      'financialQuarterId',
      'propertyId'
    );

    await property.deleteOne();
    await ModelPropertyFinancial.deleteOne({ _id: body.response.financialId });
    await user.deleteOne();
    await admin.deleteOne();
  });

  it('GET /property/select', async () => {
    await cleanProperty();
    const property = await insertPropertyWithoutFinancial('search test');
    const [user, admin] = await FUserAdmin(faker.internet.email());
    await global.testServer
      .post(`/admin/login`)
      .send({
        user: (user as IModelUser).email,
        password: global.adminPass,
      })
      .expect(200);
    const { body } = await global.testServer
      .get(`/property/select?search=${property.name}&searchType=package`)
      .expect(200);

    const { response } = body;
    expect(response.length).to.be.greaterThan(
      0,
      'No properties find with the search'
    );
    response.forEach((prop) => {
      expect(prop).to.have.all.keys(
        'id',
        'address',
        'name',
        'shareAvailable',
        'mainImage',
        'type',
        'isComplete'
      );
    });

    await property.deleteOne();
    await user.deleteOne();
    await admin.deleteOne();
  });

  it('Get /admin/property/:propertyId', async () => {
    const { state, county } = await insertStateAndCounty();
    const data = await FProperty(state._id, county._id);
    const [property, propertyFinancial, propertyQuarter] = await Promise.all(
      data
    );
    const [user, admin] = await FUserAdmin(faker.internet.email());
    await global.testServer
      .post(`/admin/login`)
      .send({
        user: (user as IModelUser).email,
        password: global.adminPass,
      })
      .expect(200);
    await global.testServer.get(`/admin/property/${property._id}`).expect(200);
    //TODO create the unit test

    await state.deleteOne();
    await county.deleteOne();
    await propertyFinancial.deleteOne();
    await propertyQuarter.deleteOne();
    await property.deleteOne();
    await user.deleteOne();
    await admin.deleteOne();
  });

  it(`Get /admin/property/:propertyId/financial/:propertyId?year=:year&quarter=:quarter`, async () => {
    const dateNow = DateTime.now();
    const year = dateNow.year;
    const quarter = dateNow.quarter;
    const { state, county } = await insertStateAndCounty();
    const data = await FProperty(state._id, county._id);
    const [property, propertyFinancial, propertyQuarter] = await Promise.all(
      data
    );
    const [user, admin] = await FUserAdmin(faker.internet.email());
    await global.testServer
      .post(`/admin/login`)
      .send({
        user: (user as IModelUser).email,
        password: global.adminPass,
      })
      .expect(200);
    const { body } = await global.testServer
      .get(
        `/admin/property/${property._id}/financial/${propertyFinancial._id}?year=${year}&quarter=${quarter}`
      )
      .expect(200);

    expect(body).to.have.property('response');
    expect(body.response).to.have.all.keys(
      'totalValue',
      'initialInvestment',
      'purchasePrice',
      'aquisitionFee',
      'dispositionFee',
      'amountAllocatedToLand',
      'depreciation',
      'commissions',
      'downPayment',
      'capitalGainsTaxPaid',
      'salesPrice',
      'quarter'
    );
    expect(body.response.quarter).to.have.all.keys(
      'year',
      'quarter',
      'totalUnits',
      'rents',
      'propertyManagement',
      'assetManagement',
      'repairsMaintenance',
      'taxes',
      'insurance',
      'reserve',
      'vacancy',
      'operativeExpenses'
    );

    await state.deleteOne();
    await county.deleteOne();
    await propertyQuarter.deleteOne();
    await propertyFinancial.deleteOne();
    await property.deleteOne();
    await user.deleteOne();
    await admin.deleteOne();
  });

  it(`PUT /admin/property/:propertyId`, async () => {
    const property = await insertPropertyWithoutFinancial();
    const { state, county } = await insertStateAndCounty();
    const name = 'Property Update Test';
    const type = EnumPropertyTypes.COMMERCIAL;
    const address =
      faker.address.streetAddress() +
      faker.address.state() +
      faker.address.zipCode();
    const bedRooms = faker.datatype.number({ max: 10, min: 1, precision: 1 });
    const bathRooms = faker.datatype.number({ max: 10, min: 1, precision: 1 });
    const size = faker.datatype.number({ max: 10, min: 1, precision: 1 });
    const parcelSize = faker.datatype.number({ max: 10, min: 1, precision: 1 });
    const parcelNumber = faker.datatype.number({
      max: 10,
      min: 1,
      precision: 1,
    });
    const strategies = {
      buyAndHold: faker.datatype.boolean(),
      repositionAndHold: faker.datatype.boolean(),
      repositionAndSell: faker.datatype.boolean(),
      developAndHold: faker.datatype.boolean(),
      developAndSell: faker.datatype.boolean(),
      inmediateSell: faker.datatype.boolean(),
    };
    const propertyDescription = 'Updated Property Description';
    const [user, admin] = await FUserAdmin(faker.internet.email());
    await global.testServer
      .post(`/admin/login`)
      .send({
        user: (user as IModelUser).email,
        password: global.adminPass,
      })
      .expect(200);
    const { body } = await global.testServer
      .put(`/admin/property/${property._id}`)
      .send({
        name,
        type,
        state: state._id,
        county: county._id,
        address,
        bedRooms,
        bathRooms,
        size,
        parcelSize,
        parcelNumber,
        strategies,
        propertyDescription,
        newPictures: [
          'https://images.homify.com/c_fill,f_auto,q_0,w_740/v1448454429/p/photo/image/1143801/5.jpg',
          'https://images.adsttc.com/media/images/5721/9e81/e58e/ced9/f100/001f/medium_jpg/Parque_Humano_-_Casa_en_el_bosque_10.jpg?1461821043',
        ],
        deletePictures: [
          'https://images.homify.com/c_fill,f_auto,q_0,w_740/v1448454429/p/photo/image/1143801/5.jpg',
          'https://images.adsttc.com/media/images/5721/9e81/e58e/ced9/f100/001f/medium_jpg/Parque_Humano_-_Casa_en_el_bosque_10.jpg?1461821043',
        ],
        newMainPicture:
          'https://i.pinimg.com/474x/ee/66/d3/ee66d372c770dd38932671c27e3b7228.jpg',
        location: {
          lat: '19.40956975707746',
          lng: '-99.17181159448604',
        },
        investors: [],
      })
      .expect(200);
    expect(body).to.have.all.keys('response', 'message');
    expect(body.response).to.have.all.keys('id');
    expect(body.response.id).to.be.equal(property._id.toHexString());
    const updatedProperty = await ModelProperty.findOne({ _id: property._id });

    expect(updatedProperty.name).to.be.equal(name);
    expect(updatedProperty.type).to.be.equal(type);
    expect(updatedProperty.state.toString()).to.be.equal(state._id.toString());
    expect(updatedProperty.county.toString()).to.be.equal(
      county._id.toString()
    );
    expect(updatedProperty.address).to.be.equal(address);
    expect(
      new Bignumber(updatedProperty.bedRooms).isEqualTo(bedRooms)
    ).to.be.equal(true);
    expect(
      new Bignumber(updatedProperty.bathRooms).isEqualTo(bathRooms)
    ).to.be.equal(true);
    expect(new Bignumber(updatedProperty.size).isEqualTo(size)).to.be.equal(
      true
    );
    expect(
      new Bignumber(updatedProperty.parcelSize).isEqualTo(parcelSize)
    ).to.be.equal(true);
    expect(
      new Bignumber(updatedProperty.parcelNumber).isEqualTo(parcelNumber)
    ).to.be.equal(true);
    expect(updatedProperty.strategies.buyAndHold).to.be.equal(
      strategies.buyAndHold
    );
    expect(updatedProperty.strategies.repositionAndHold).to.be.equal(
      strategies.repositionAndHold
    );
    expect(updatedProperty.strategies.repositionAndSell).to.be.equal(
      strategies.repositionAndSell
    );
    expect(updatedProperty.strategies.developAndHold).to.be.equal(
      strategies.developAndHold
    );
    expect(updatedProperty.strategies.developAndSell).to.be.equal(
      strategies.developAndSell
    );
    expect(updatedProperty.strategies.inmediateSell).to.be.equal(
      strategies.inmediateSell
    );
    expect(updatedProperty.propertyDescription).to.be.equal(
      propertyDescription
    );

    await property.deleteOne();
    await state.deleteOne();
    await county.deleteOne();
    await user.deleteOne();
    await admin.deleteOne();
  });
  it('DELETE /admin/property/:propertyId ', async () => {
    const [property] = await FProperty();
    const [user, admin] = await FUserAdmin(faker.internet.email());
    await global.testServer
      .post(`/admin/login`)
      .send({
        user: (user as IModelUser).email,
        password: global.adminPass,
      })
      .expect(200);
    const { body } = await global.testServer
      .delete(`/admin/property/${property._id}`)
      .expect(200);

    expect(body).to.have.all.keys('message', 'response');
    const deletedProperty = await ModelProperty.findOne({ _id: property._id });
    expect(deletedProperty).to.be.equal(null);
    await property.deleteOne();
    await user.deleteOne();
    await admin.deleteOne();
  });
});

describe('GET /property', () => {
  before(cleanProperty);
  before(cleanFunds);
  before(cleanPackage);
  it('should return properties categorized by funds, singles and packages', async () => {
    const [propertyPackage, newPackage, packageProperty] =
      await insertPropertyWithPackage();
    const [propertyFund, newFund, FundProperty] =
      await insertPropertyWithFund();
    const singleProperty = await insertPropertyWithoutFinancial();
    const [user, admin] = await FUserAdmin(faker.internet.email());
    await global.testServer
      .post(`/admin/login`)
      .send({
        user: (user as IModelUser).email,
        password: global.adminPass,
      })
      .expect(200);

    const { body } = await global.testServer.get(`/property`).expect(200);

    expect(body).to.have.all.keys('response');
    expect(body.response).to.have.all.keys('funds', 'packages', 'single');
    const { funds, packages, single } = body.response;
    for (let fund of funds) {
      expect(fund).to.have.all.keys('id', 'name', 'properties');
      expect(fund.properties.length).to.be.greaterThan(0);
      fund.properties.forEach(checkGetPropertiesFundProperty);
    }
    for (let currentPackage of packages) {
      expect(currentPackage).to.have.all.keys('id', 'name', 'properties');
      expect(currentPackage.properties.length).to.be.greaterThan(0);
      currentPackage.properties.forEach(
        checkGetPropertiesPackageAndSingleProperty
      );
    }
    expect(single.length).to.be.greaterThan(0);
    for (let property of single) {
      checkGetPropertiesPackageAndSingleProperty(property);
    }
    const fundResponse = await global.testServer
      .get(`/property?category=FUND`)
      .expect(200);
    expect(fundResponse.body).to.exist;
    expect(fundResponse.body.response).to.exist;
    expect(fundResponse.body.response).to.have.all.keys(
      'funds',
      'packages',
      'single'
    );
    expect(fundResponse.body.response.packages).to.be.equal(null);
    expect(fundResponse.body.response.single).to.be.equal(null);

    const packageResponse = await global.testServer
      .get(`/property?category=PACKAGE`)
      .expect(200);
    expect(packageResponse.body).to.exist;
    expect(packageResponse.body.response).to.exist;
    expect(packageResponse.body.response).to.have.all.keys(
      'funds',
      'packages',
      'single'
    );
    expect(packageResponse.body.response.funds).to.be.equal(null);
    expect(packageResponse.body.response.single).to.be.equal(null);

    const singleResponse = await global.testServer
      .get(`/property?category=SINGLE`)
      .expect(200);
    expect(singleResponse.body).to.exist;
    expect(singleResponse.body.response).to.exist;
    expect(singleResponse.body.response).to.have.all.keys(
      'funds',
      'packages',
      'single'
    );
    expect(singleResponse.body.response.funds).to.be.equal(null);
    expect(singleResponse.body.response.packages).to.be.equal(null);

    const mixResponse = await global.testServer
      .get(`/property?category=SINGLE|FUND`)
      .expect(200);
    expect(mixResponse.body).to.exist;
    expect(mixResponse.body.response).to.exist;
    expect(mixResponse.body.response).to.have.all.keys(
      'funds',
      'packages',
      'single'
    );
    expect(mixResponse.body.response.packages).to.be.equal(null);
    expect(mixResponse.body.response.funds.length).to.be.greaterThan(0);
    expect(mixResponse.body.response.single.length).to.be.greaterThan(0);

    await propertyPackage.deleteOne();
    await propertyFund.deleteOne();
    await singleProperty.deleteOne();
    await newPackage.deleteOne();
    await packageProperty.deleteOne();
    await newFund.deleteOne();
    await FundProperty.deleteOne();
    await user.deleteOne();
    await admin.deleteOne();
  });

  describe('Property address validations', () => {
    it('Should fail when create a property with duplicated address', async () => {
      const property = await insertPropertyWithoutFinancial();
      const { state, county } = await insertStateAndCounty();
      const propertyData: IBaseEntityProperty = {
        name: `Test: ${new Date().getTime()}`,
        address: property.address,
        type: EnumPropertyTypes[getEnumKey(EnumPropertyTypes)],
        state: state._id,
        county: county._id,
        bedRooms: faker.datatype.number(10),
        bathRooms: faker.datatype.number(10),
        size: faker.datatype.number(500),
        parcelSize: faker.datatype.number(100),
        parcelNumber: faker.datatype.number(100),
        strategies: {
          buyAndHold: faker.datatype.boolean(),
          developAndHold: faker.datatype.boolean(),
          developAndSell: faker.datatype.boolean(),
          inmediateSell: faker.datatype.boolean(),
          repositionAndHold: faker.datatype.boolean(),
          repositionAndSell: faker.datatype.boolean(),
        },
        propertyDescription: faker.datatype.string(25),
        pictures: [faker.image.imageUrl(), faker.image.imageUrl()],
        mainPicture: faker.image.imageUrl(),
        location: {
          lat: faker.address.latitude(),
          lng: faker.address.longitude(),
        },
        investors: [],
      };
      const [user, admin] = await FUserAdmin(faker.internet.email());
      await global.testServer
        .post(`/admin/login`)
        .send({
          user: (user as IModelUser).email,
          password: global.adminPass,
        })
        .expect(200);
      await global.testServer
        .post(`/admin/property`)
        .send(propertyData)
        .expect(409);
      await property.deleteOne();
      await user.deleteOne();
      await admin.deleteOne();
      await state.deleteOne();
      await county.deleteOne();
    });

    it('Should fail when update a property with duplicated address', async () => {
      const property1 = await insertPropertyWithoutFinancial();
      const property2 = await insertPropertyWithoutFinancial();
      const { state, county } = await insertStateAndCounty();
      const propertyData: IBaseEntityProperty = {
        name: `Test: ${new Date().getTime()}`,
        address: property2.address,
        type: EnumPropertyTypes[getEnumKey(EnumPropertyTypes)],
        state: state._id,
        county: county._id,
        bedRooms: faker.datatype.number(10),
        bathRooms: faker.datatype.number(10),
        size: faker.datatype.number(500),
        parcelSize: faker.datatype.number(100),
        parcelNumber: faker.datatype.number(100),
        strategies: {
          buyAndHold: faker.datatype.boolean(),
          developAndHold: faker.datatype.boolean(),
          developAndSell: faker.datatype.boolean(),
          inmediateSell: faker.datatype.boolean(),
          repositionAndHold: faker.datatype.boolean(),
          repositionAndSell: faker.datatype.boolean(),
        },
        propertyDescription: faker.datatype.string(25),
        pictures: [faker.image.imageUrl(), faker.image.imageUrl()],
        mainPicture: faker.image.imageUrl(),
        location: {
          lat: faker.address.latitude(),
          lng: faker.address.longitude(),
        },
        investors: [],
      };
      const [user, admin] = await FUserAdmin(faker.internet.email());
      await global.testServer
        .post(`/admin/login`)
        .send({
          user: (user as IModelUser).email,
          password: global.adminPass,
        })
        .expect(200);
      await global.testServer
        .put(`/admin/property/${property1._id}`)
        .send(propertyData)
        .expect(409);
      await property1.deleteOne();
      await property2.deleteOne();
      await user.deleteOne();
      await admin.deleteOne();
      await state.deleteOne();
      await county.deleteOne();
    });
    it('Should sucess when update a property with the same address', async () => {
      const property = await insertPropertyWithoutFinancial();

      const { state, county } = await insertStateAndCounty();
      const propertyData: IBaseEntityProperty = {
        name: `Test: ${new Date().getTime()}`,
        address: property.address,
        type: EnumPropertyTypes[getEnumKey(EnumPropertyTypes)],
        state: state._id,
        county: county._id,
        bedRooms: faker.datatype.number(10),
        bathRooms: faker.datatype.number(10),
        size: faker.datatype.number(500),
        parcelSize: faker.datatype.number(100),
        parcelNumber: faker.datatype.number(100),
        strategies: {
          buyAndHold: faker.datatype.boolean(),
          developAndHold: faker.datatype.boolean(),
          developAndSell: faker.datatype.boolean(),
          inmediateSell: faker.datatype.boolean(),
          repositionAndHold: faker.datatype.boolean(),
          repositionAndSell: faker.datatype.boolean(),
        },
        propertyDescription: faker.datatype.string(25),
        pictures: [faker.image.imageUrl(), faker.image.imageUrl()],
        mainPicture: faker.image.imageUrl(),
        location: {
          lat: faker.address.latitude(),
          lng: faker.address.longitude(),
        },
        investors: [],
      };
      const [user, admin] = await FUserAdmin(faker.internet.email());
      await global.testServer
        .post(`/admin/login`)
        .send({
          user: (user as IModelUser).email,
          password: global.adminPass,
        })
        .expect(200);
      await global.testServer
        .put(`/admin/property/${property._id}`)
        .send(propertyData)
        .expect(200);
      await property.deleteOne();
      await user.deleteOne();
      await admin.deleteOne();
      await state.deleteOne();
      await county.deleteOne();
    });
  });
});

describe('/admin/property/preview/:idProperty', () => {
  it('Test Property Preview', async () => {
    const [property, IModelPropertyFinancial, IModelPropertyFinancialQuarter] =
      await FProperty(States[0].id, Counties[0].id);
    const [user, admin] = await FUserAdmin(faker.internet.email());
    await global.testServer
      .post(`/admin/login`)
      .send({
        user: (user as IModelUser).email,
        password: global.adminPass,
      })
      .expect(200);
    const serverResponse = await global.testServer
      .post(`/admin/property/preview/${property._id}`)
      .send(propertyPreviewBody1)
      .expect(200);
    expect(serverResponse.body).to.have.all.keys('response');
    const { exitStrategies } = serverResponse.body.response;
    expect(expectedEstrategiesResultPreview1.length).to.be.equal(
      exitStrategies.length
    );
    for (let expectedResult of expectedEstrategiesResultPreview1) {
      const estrategyResult = exitStrategies.find(
        (e) => e.year == expectedResult.year
      );
      expect(estrategyResult).to.be.not.equal(null);
      expect(estrategyResult.totalAnnualROI).to.be.equal(
        expectedResult.totalAnnualROI
      );
    }

    await property.deleteOne();
    await user.deleteOne();
    await admin.deleteOne();
    await IModelPropertyFinancial.deleteOne();
    await IModelPropertyFinancialQuarter.deleteOne();
  });

  describe('/admin/property/projection/:idProperty', () => {
    it('Test Property projectionSave', async () => {
      const property: IModelProperty = await insertPropertyWithoutFinancial();
      const [user, admin] = await FUserAdmin(faker.internet.email());
      await global.testServer
        .post(`/admin/login`)
        .send({
          user: (user as IModelUser).email,
          password: global.adminPass,
        })
        .expect(200);
      const serverResponse = await global.testServer
        .post(`/admin/property/projection/${property.id}`)
        .send(propertyPreviewBody1)
        .expect(200);
      expect(serverResponse.body).to.have.all.keys('response');
      expect(serverResponse.body.response).to.have.all.keys('idProjection');
      await property.deleteOne();
      await user.deleteOne();
      await admin.deleteOne();
    });
  });
});
