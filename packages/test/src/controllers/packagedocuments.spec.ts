import 'mocha';
import { expect } from 'chai';
import faker from 'faker';
import { FUserAdmin } from '../helpers';
import { ModelPackageDocuments } from '@capa/data';

let user = null;
let adminC = null;
let capaSession = '';
let templatesName = null;
const test1 = faker.lorem.words();
let idCorrect = null;
let idDeleted = null
const test2 = faker.lorem.words();
const test3 = faker.lorem.words();
describe('PackageDocuments test', () => { 
  before(async () =>{
      const email = faker.internet.email();
      const anAdmin = await FUserAdmin(email);
      const [userAdmin, admin] = await Promise.all(anAdmin);
      user = userAdmin;
      adminC = admin;
      await global.testServer
        .post(`/admin/login`)
        .send({
          user: email,
          password: global.adminPass,
        })
        .expect(200);
      const authCookie = global.testServer.jar.getCookie(
        global.cookieSessionId,
        {
          domain: '',
          path: '/',
          secure: global.cookieSecure,
          script: false,
        }
      );
      expect(authCookie).to.exist;
      capaSession = authCookie.value;
      const getNames = await global.testServer
      .get(`/admin/document/docusign/list`)
      .expect(200);
      templatesName = getNames.body["response"].reduce((pre, value) => {
        if(value.name === "") return pre;
        pre.push(value);
        return pre;
      }, []);
  })
  it('CREATE should return a 200 response', async () => {
    const randomString = (Math.random() + 1).toString(36).substring(7);
    const res = await global.testServer
    .post(`/admin/document/docusign/package`)
    .set('Cookie', `capaSession=${capaSession};`)
    .send({
      name: test1,
      templates: [
        templatesName[0]["templateId"] || randomString
      ]
    }).expect(200);
    const { body } = res;
    expect(body).to.have.property('response');
    idCorrect = body.response.id;
  });
  it('CREATE should return a 404 response', async () => {
    const randomString = (Math.random() + 1).toString(36).substring(7);
    const res = await global.testServer
    .post(`/admin/document/docusign/package`)
    .set('Cookie', `capaSession=${capaSession};`)
    .send({
      name: test2,
      templates: [
        randomString
      ]
    }).expect(404);
    const { body } = res;
    expect(body).to.have.property('response');
  });
  it('CREATE should return a 409 response', async () => {
    const randomString = (Math.random() + 1).toString(36).substring(7);
    const res = await global.testServer
    .post(`/admin/document/docusign/package`)
    .set('Cookie', `capaSession=${capaSession};`)
    .send({
      name: test1,
      templates: [
        templatesName[0]["templateId"] || randomString
      ]
    }).expect(409);
    const { body } = res;
    expect(body).to.have.property('response');
  });
  it('CREATE should return a 409 response, Case Insensitive', async () => {
    const randomString = (Math.random() + 1).toString(36).substring(7);
    const res = await global.testServer
    .post(`/admin/document/docusign/package`)
    .set('Cookie', `capaSession=${capaSession};`)
    .send({
      name: test1.toUpperCase(),
      templates: [
        templatesName[0]["templateId"] || randomString
      ]
    }).expect(409);
    const { body } = res;
    expect(body).to.have.property('response');
  });
  it('DELETE Should return a 200 response', async () => {
    idDeleted = idCorrect;
    const res = await global.testServer
    .delete(`/admin/document/docusign/package/${idCorrect}`)
    .set('Cookie', `capaSession=${capaSession};`)
    .expect(200);
    const { body } = res;
    expect(body).to.have.property('message');
    const message = body.message;
    expect(message).to.be.equal('Package has been deleted succesfully.');
  });
  it('DELETE Should return a 404 response', async () => {
    const res = await global.testServer
    .delete(`/admin/document/docusign/package/${idCorrect}`)
    .set('Cookie', `capaSession=${capaSession};`)
    .expect(404);
    const { body } = res;
    expect(body).to.have.property('message');
    const message = body.message;
    expect(message).to.be.equal('An error occurred while trying to delete a packagedocuments: PackageDocuments not found.');
  });
  it('CREATE should return a 200 response', async () => {
    const randomString = faker.lorem.words();
    const res = await global.testServer
    .post(`/admin/document/docusign/package`)
    .set('Cookie', `capaSession=${capaSession};`)
    .send({
      name: test2,
      templates: [
        templatesName[0]["templateId"] || randomString
      ]
    }).expect(200);
    const { body } = res;
    expect(body).to.have.property('response');
    idCorrect = body.response.id;
    const res2 = await global.testServer
    .post(`/admin/document/docusign/package`)
    .set('Cookie', `capaSession=${capaSession};`)
    .send({
      name: test1,
      templates: [
        templatesName[0]["templateId"] || randomString
      ]
    }).expect(200);
    const { body: body2 } = res2;
    expect(body2).to.have.property('response');
  });
  it('GET ALL DOCUMENTS should return a 200 response', async () => {
    const res = await global.testServer
    .get(`/admin/document/docusign/package/list`)
    .set('Cookie', `capaSession=${capaSession};`)
    .expect(200);
    const { body } = res;
    expect(body).to.have.property('response');
    body.response.forEach(element => {
      expect(element).to.have.all.keys('id', 'name', 'templates');
      element.templates.forEach(deepElement => {
        expect(deepElement).to.contain.keys('templateId', 'name', 'date');
      });
    });
  });
  it('GET DOCUMENT BY ID should return a 200 response', async () => {
    const res = await global.testServer
    .get(`/admin/document/docusign/package/${idCorrect}`)
    .set('Cookie', `capaSession=${capaSession};`)
    .expect(200);
    const { body } = res;
    expect(body).to.have.property('response');
    expect(body.response).to.have.all.keys('id', 'name', 'templates');
    body.response.templates.forEach(element => {
      expect(element).to.have.all.keys('templateId', 'name', 'date');
    });
    const resp1 = await global.testServer
    .delete(`/admin/document/docusign/package/${idCorrect}`)
    .set('Cookie', `capaSession=${capaSession};`)
    .expect(200);
    const { body: body2 } = resp1;
    expect(body2).to.have.property('response');
  });
  it('GET DOCUMENT BY ID should return a 404 response', async () => {
    const res = await global.testServer
    .get(`/admin/document/docusign/package/${idCorrect}`)
    .set('Cookie', `capaSession=${capaSession};`)
    .expect(404);
    const { body } = res;
    expect(body).to.have.property('response');
  });
  it('CREATE should return a 200 response to test', async () => {
    const randomString = faker.lorem.words();
    const randomString2 = faker.lorem.words();
    const res = await global.testServer
    .post(`/admin/document/docusign/package`)
    .set('Cookie', `capaSession=${capaSession};`)
    .send({
      name: test3,
      templates: [
        templatesName[0]["templateId"] || randomString,
        templatesName[1]["templateId"] || randomString2,
      ]
    }).expect(200);
    const { body } = res;
    expect(body).to.have.property('response');
    idCorrect = body.response.id;
  });
  it('Update DOCUMENT BY ID should return a 200 response', async () => {
    const randomString = faker.lorem.words();
    const res = await global.testServer
    .put(`/admin/document/docusign/package/${idCorrect}`)
    .set('Cookie', `capaSession=${capaSession};`)
    .send({
      name: randomString
    }).expect(200);
    const { body } = res;
    const message = body.message;
    expect(message).to.be.equal('Package has been updated succesfully.');
  });
  it('Update DOCUMENT BY ID should return a 404 response not document found', async () => {
    const randomString = faker.lorem.words();
    const res = await global.testServer
    .put(`/admin/document/docusign/package/${idDeleted}`)
    .set('Cookie', `capaSession=${capaSession};`)
    .send({
      name: randomString
    }).expect(404);
    const { body } = res;
    const message = body.message;
    expect(message).to.be.equal(`No exist a DocuSign Package with te id: ${idDeleted}.`);
  });
  it('Update DOCUMENT BY ID should return a 404 response for templateID Not found', async () => {
    const randomString = faker.lorem.words();
    const res = await global.testServer
    .put(`/admin/document/docusign/package/${idCorrect}`)
    .set('Cookie', `capaSession=${capaSession};`)
    .send({
      name: randomString,
      newTemplates: [randomString]
    }).expect(404);
    const { body } = res;
    const message = body.message;
    expect(message).to.be.equal(`An error occurred while trying to update a packagedocuments: Error update a PackageDocuments`);
  });
  it('Update DOCUMENT BY ID should return a 409 response for not empty templete', async () => {
    const randomString = faker.lorem.words();
    const res = await global.testServer
    .put(`/admin/document/docusign/package/${idCorrect}`)
    .set('Cookie', `capaSession=${capaSession};`)
    .send({
      name: randomString,
      deleteTemplates: [
        templatesName[0]["templateId"] || randomString,
        templatesName[1]["templateId"] || randomString,
      ]
    }).expect(409);
    const { body } = res;
    const message = body.message;
    expect(message).to.be.equal(`An error occurred while trying to update a packagedocuments: PackageDocusing Templetes could not be Empty`);
  });
  it('Update DOCUMENT BY ID should return a 200 delete a templete in document', async () => {
    const randomString = faker.lorem.words();
    const res = await global.testServer
    .put(`/admin/document/docusign/package/${idCorrect}`)
    .set('Cookie', `capaSession=${capaSession};`)
    .send({
      name: randomString,
      deleteTemplates: [
        templatesName[0]["templateId"] || randomString
      ]
    }).expect(200);
    const { body } = res;
    const message = body.message;
    expect(message).to.be.equal('Package has been updated succesfully.');
  });
  it('GET DOCUMENT BY ID should return a 200 response, validate previous test', async () => {
    const res = await global.testServer
    .get(`/admin/document/docusign/package/${idCorrect}`)
    .set('Cookie', `capaSession=${capaSession};`)
    .expect(200);
    const { body } = res;
    expect(body).to.have.property('response');
    expect(body.response).to.have.all.keys('id', 'name', 'templates');
    expect(body.response.templates.length).to.be.equal(1);
    expect(body.response.templates[0].templateId).to.be.equal(templatesName[1]["templateId"]);
  });
  it('Update DOCUMENT BY ID should return a 200 new templete in document', async () => {
    const randomString = faker.lorem.words();
    const res = await global.testServer
    .put(`/admin/document/docusign/package/${idCorrect}`)
    .set('Cookie', `capaSession=${capaSession};`)
    .send({
      name: randomString,
      newTemplates: [
        templatesName[2]["templateId"] || randomString
      ]
    }).expect(200);
    const { body } = res;
    const message = body.message;
    expect(message).to.be.equal('Package has been updated succesfully.');
  });
  it('GET DOCUMENT BY ID should return a 200 response, validate previous test', async () => {
    const res = await global.testServer
    .get(`/admin/document/docusign/package/${idCorrect}`)
    .set('Cookie', `capaSession=${capaSession};`)
    .expect(200);
    const { body } = res;
    expect(body).to.have.property('response');
    expect(body.response).to.have.all.keys('id', 'name', 'templates');
    expect(body.response.templates.length).to.be.equal(2);
    expect(body.response.templates[0].templateId).to.be.equal(templatesName[1]["templateId"]);
    expect(body.response.templates[1].templateId).to.be.equal(templatesName[2]["templateId"]);
  });
  let newRandonString = null;
  it('Update DOCUMENT BY ID should return a 200 not add templete', async () => {
    const randomString = faker.lorem.words();
    newRandonString = randomString;
    const res = await global.testServer
    .put(`/admin/document/docusign/package/${idCorrect}`)
    .set('Cookie', `capaSession=${capaSession};`)
    .send({
      name: randomString,
      newTemplates: [
        templatesName[2]["templateId"] || randomString
      ]
    }).expect(200);
    const { body } = res;
    const message = body.message;
    expect(message).to.be.equal('Package has been updated succesfully.');
  });
  it('GET DOCUMENT BY ID should return a 200 response, validate previous test No.2', async () => {
    const res = await global.testServer
    .get(`/admin/document/docusign/package/${idCorrect}`)
    .set('Cookie', `capaSession=${capaSession};`)
    .expect(200);
    const { body } = res;
    expect(body).to.have.property('response');
    expect(body.response).to.have.all.keys('id', 'name', 'templates');
    expect(body.response.templates.length).to.be.equal(2);
    expect(body.response.name).to.be.equal(newRandonString);
    expect(body.response.templates[0].templateId).to.be.equal(templatesName[1]["templateId"]);
    expect(body.response.templates[1].templateId).to.be.equal(templatesName[2]["templateId"]);
  });

  after(async () => {
    await user.deleteOne();
    await adminC.deleteOne();
    await ModelPackageDocuments.deleteMany({});
  });
});
