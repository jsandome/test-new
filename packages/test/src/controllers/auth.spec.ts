import 'mocha';
import { expect } from 'chai';
import faker from 'faker';

import { EnumUserTypes } from '@capa/core';
import { ModelAdministrator, ModelInvestor, ModelUser } from '@capa/data';

describe('Auth test', () => {
  describe('LOGIN', () => {
    it('should do login like admin', async () => {
      const email = faker.internet.email();
      const user = new ModelUser({
        admin: null,
        investor: null,
        password: global.adminPassHash,
        type: EnumUserTypes.ADMIN,
        email,
        active: true,
      });
      await user.save();
      const admin = new ModelAdministrator({
        user: user.id,
        name: {
          firstName: faker.name.firstName(),
          lastName: faker.name.lastName(),
        },
        type: EnumUserTypes.ADMIN,
      });
      await admin.save();
      user.admin = admin.id;
      await user.save();

      const res = await global.testServer
        .post(`/admin/login`)
        .send({
          user: email,
          password: global.adminPass,
        })
        .expect(200);
      const authCookie = global.testServer.jar.getCookie(
        global.cookieSessionId,
        {
          domain: '',
          path: '/',
          secure: global.cookieSecure,
          script: false,
        }
      );
      expect(authCookie).to.exist;

      const { body } = res;
      expect(body).to.have.property('response');
      expect(body).to.have.property('message');
      expect(body.response).to.have.all.keys(
        'twoFactor',
        'name',
        'customToken',
        'id',
        'type'
      );

      await user.deleteOne();
      await admin.deleteOne();
    });
    it('should do login like investor', async () => {
      const email = faker.internet.email();
      const user = new ModelUser({
        admin: null,
        investor: null,
        password: global.investorPassHash,
        type: EnumUserTypes.INVESTOR,
        email,
        active: true,
      });
      await user.save();
      const investor = new ModelInvestor({
        user: user.id,
        name: {
          firstName: faker.name.firstName(),
          lastName: faker.name.lastName(),
        },
      });
      await investor.save();
      user.investor = investor.id;
      await user.save();

      const res = await global.testServer
        .post(`/investor/login`)
        .send({
          user: email,
          password: global.investorPass,
        })
        .expect(200);
      const authCookie = global.testServer.jar.getCookie(
        global.cookieSessionId,
        {
          domain: '',
          path: '/',
          secure: global.cookieSecure,
          script: false,
        }
      );
      expect(authCookie).to.exist;

      const { body } = res;
      expect(body).to.have.property('response');
      expect(body).to.have.property('message');
      expect(body.response).to.have.all.keys(
        'twoFactor',
        'name',
        'customToken',
        'id',
        'type'
      );

      await user.deleteOne();
      await investor.deleteOne();
    });
  });
  describe('LOGOUT', () => {
    it('should do logout like admin', async () => {
      const email = faker.internet.email();
      const user = new ModelUser({
        admin: null,
        investor: null,
        password: global.adminPassHash,
        type: EnumUserTypes.ADMIN,
        email,
        active: true,
      });
      await user.save();
      const admin = new ModelAdministrator({
        user: user.id,
        name: {
          firstName: faker.name.firstName(),
          lastName: faker.name.lastName(),
        },
        type: EnumUserTypes.ADMIN,
      });
      await admin.save();
      user.admin = admin.id;
      await user.save();

      await global.testServer
        .post(`/admin/login`)
        .send({
          user: email,
          password: global.adminPass,
        })
        .expect(200);
      let authCookie = global.testServer.jar.getCookie(global.cookieSessionId, {
        domain: '',
        path: '/',
        secure: global.cookieSecure,
        script: false,
      });
      expect(authCookie).to.exist;

      await global.testServer
        .delete('/logout')
        .set(global.cookieSessionId, authCookie.value)
        .expect(200);
      authCookie = global.testServer.jar.getCookie(global.cookieSessionId, {
        domain: '',
        path: '/',
        secure: global.cookieSecure,
        script: false,
      });
      expect(authCookie).to.not.exist;

      await user.deleteOne();
      await admin.deleteOne();
    });
  });
  describe('RECOVERY PASSWORD', () => {
    describe('POST /recovery', async () => {
      it('should send recovery password email', async () => {
        const email = 'magdiel.juarez@umvel.com';
        await global.testServer.post('/recovery').send({ email }).expect(200);
      });
    });
  });

  describe('RESTORE PASSWORD', () => {
    describe('PATCH /recovery', async () => {
      it('should restore password email', async () => {
        const email = faker.internet.email();
        const password = 'abcDEF123_.';
        const user = new ModelUser({
          admin: null,
          investor: null,
          password: global.adminPassHash,
          type: EnumUserTypes.ADMIN,
          email,
          active: true,
        });
        await user.save();
        await global.testServer.post('/recovery').send({ email }).expect(200);

        const updatedUser = await ModelUser.findOne({
          email: user.email,
        }).exec();

        await global.testServer
          .patch('/recovery')
          .send({ token: updatedUser.expirePass.token, password })
          .expect(200);

        const { body } = await global.testServer
          .post(`/admin/login`)
          .send({ user: email, password: password })
          .expect(200);

        const { response } = body;
        expect(response).to.have.property('userToken');

        await user.deleteOne();
      });
    });
  });

  describe('CHANGE PASSWORD', () => {
    describe('PATCH /password', async () => {
      it('should change password', async () => {
        const email = faker.internet.email();
        const user = new ModelUser({
          admin: null,
          investor: null,
          password: global.investorPassHash,
          type: EnumUserTypes.INVESTOR,
          email,
          active: true,
        });
        const investor = new ModelInvestor({
          user: user.id,
          name: {
            firstName: faker.name.firstName(),
            lastName: faker.name.lastName(),
          },
        });
        await investor.save();
        user.investor = investor.id;
        await user.save();

        await global.testServer
          .post(`/investor/login`)
          .send({ user: email, password: global.investorPass })
          .expect(200);

        const newPassword = 'abcDEF123_.';

        await global.testServer
          .patch('/password')
          .send({ currentPassword: global.investorPass, newPassword })
          .expect(200);

        await global.testServer
          .post(`/investor/login`)
          .send({ user: email, password: newPassword })
          .expect(200);

        await user.deleteOne();
        await investor.deleteOne();
      });
    });
  });

  describe('VERIFY CODE', () => {
    describe('POST /password', async () => {
      it('should verify code', async () => {
        const email = faker.internet.email();
        const user = new ModelUser({
          admin: null,
          investor: null,
          password: global.adminPassHash,
          type: EnumUserTypes.ADMIN,
          email,
          active: true,
          twoFactor: {
            type: 'email',
            enabled: true,
          },
        });
        const admin = new ModelAdministrator({
          user: user.id,
          name: {
            firstName: faker.name.firstName(),
            lastName: faker.name.lastName(),
          },
          type: EnumUserTypes.ADMIN,
        });
        await admin.save();
        user.admin = admin.id;
        await user.save();

        const { body } = await global.testServer
          .post(`/admin/login`)
          .send({ user: email, password: global.adminPass })
          .expect(200);

        const { response } = body;
        const { secure } = response;

        const userLogued = await ModelUser.findOne({ email });
        const { code } = userLogued.twoFactor;

        const { body: bodyVerified } = await global.testServer
          .post(`/admin/login/code`)
          .send({ user: email, secure, code })
          .expect(200);

        const { response: validatedResponse } = bodyVerified;

        // Just if verify code was approved
        expect(validatedResponse).to.have.all.keys(
          'name',
          'customToken',
          'id',
          'type'
        );

        user.deleteOne();
        admin.deleteOne();
      });
    });
  });
});
