import 'mocha';
import { expect } from 'chai';
import faker from 'faker';

import { EnumUserTypes } from '@capa/core';
import { ModelUser } from '@capa/data';

describe('User test', () => {
  describe('READ', () => {
    it('should return a user', async () => {
      const user = new ModelUser({
        admin: null,
        investor: null,
        type: EnumUserTypes.INVESTOR,
        email: faker.internet.email(),
        active: true,
      });
      await user.save();
      const res = await global.testServer.get(`/user/${user.id}`).expect(200);

      const { body } = res;
      expect(body).to.have.property('response');
      expect(body.response).to.have.all.keys(
        'id',
        'admin',
        'investor',
        'type',
        'email',
        'active'
      );

      await user.deleteOne();
    });
  });
});
