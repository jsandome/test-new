import {
  FUserAdmin,
  factoryLlcs,
  FUserInvestor,
  FCapitalCall,
  FCapitalCallInvestor,
  FContribution,
} from '../helpers';
import faker from 'faker';
import { expect } from 'chai';
import {
  IModelCapitalCall,
  IModelCapitalCallInvestor,
  ModelCapitalCall,
  ModelContribution,
} from '@capa/data';

describe('Capital Call Controller Test', () => {
  describe('POST /admin/llc/:llcId/capital-call', () => {
    it('should create a new Capital Call', async () => {
      const email = faker.internet.email();
      const emailInvestor = faker.internet.email();
      const anAdmin = await FUserAdmin(email);
      const anInvestor = await FUserInvestor(emailInvestor);
      const [userAdmin, admin] = await Promise.all(anAdmin);
      const [user, investor] = await Promise.all(anInvestor);
      const llc = await factoryLlcs();

      await global.testServer
        .post(`/admin/login`)
        .send({
          user: email,
          password: global.adminPass,
        })
        .expect(200);

      const dataCapitalCall = {
        description: faker.name.title(),
        dueDate: new Date().toISOString(),
        investors: [
          {
            id: investor.id,
            capitalAmount: faker.datatype.number(1000),
          },
        ],
      };

      const { body } = await global.testServer
        .post(`/admin/llc/${llc.id}/capital-call`)
        .send(dataCapitalCall)
        .expect(200);

      expect(body).to.have.property('response');
      expect(body).to.have.property('message');
      expect(body.response).to.have.all.keys('id');

      await userAdmin.deleteOne();
      await admin.deleteOne();
      await llc.deleteOne();
      await user.deleteOne();
      await investor.deleteOne();
    });
  });

  describe('GET /admin/llc/:llcId/transactions/capital-call', () => {
    it('should get list of Capital Call', async () => {
      const email = faker.internet.email();
      const emailInvestor = faker.internet.email();
      const anAdmin = await FUserAdmin(email);
      const anInvestor = await FUserInvestor(emailInvestor);
      const [userAdmin, admin] = await Promise.all(anAdmin);
      const [user, investor] = await Promise.all(anInvestor);
      const llc = await factoryLlcs();
      const capitalcall: IModelCapitalCall = await FCapitalCall(llc.id);
      const capitalcallInvestor: IModelCapitalCallInvestor =
        await FCapitalCallInvestor(capitalcall.id, investor.id);
      await FContribution(capitalcallInvestor.id);

      await global.testServer
        .post(`/admin/login`)
        .send({
          user: email,
          password: global.adminPass,
        })
        .expect(200);

      const { body } = await global.testServer
        .get(`/admin/llc/${llc.id}/transactions/capital-call`)
        .expect(200);

      expect(body).to.have.property('response');
      expect(body.response).to.have.all.keys('pagination', 'totals', 'details');
      expect(body.response.pagination).to.have.all.keys(
        'itemCount',
        'offset',
        'limit',
        'totalPages',
        'page',
        'nextPage',
        'prevPage'
      );
      expect(body.response.totals).to.have.all.keys('amountCalled', 'result');
      body.response.details.forEach((e) => {
        expect(e).to.have.all.keys(
          'id',
          'dueDate',
          'description',
          'amountCalled',
          'parties',
          'result'
        );
      });

      await userAdmin.deleteOne();
      await admin.deleteOne();
      await user.deleteOne();
      await investor.deleteOne();
      await llc.deleteOne();
      await capitalcall.deleteOne();
      await capitalcallInvestor.deleteOne();
    });
  });

  describe('GET /admin/llc/:llcId/capital-call/:capitalCallId', () => {
    it('should get detail for a created Capital Call', async () => {
      const email = faker.internet.email();
      const emailInvestor = faker.internet.email();
      const anAdmin = await FUserAdmin(email);
      const anInvestor = await FUserInvestor(emailInvestor);
      const [userAdmin, admin] = await Promise.all(anAdmin);
      const [user, investor] = await Promise.all(anInvestor);
      const llc = await factoryLlcs();
      const capitalcall: IModelCapitalCall = await FCapitalCall(llc.id);
      const capitalcallInvestor: IModelCapitalCallInvestor =
        await FCapitalCallInvestor(capitalcall.id, investor.id);
      await FContribution(capitalcallInvestor.id);

      await global.testServer
        .post(`/admin/login`)
        .send({
          user: email,
          password: global.adminPass,
        })
        .expect(200);

      const { body } = await global.testServer
        .get(`/admin/llc/${llc.id}/capital-call/${capitalcall.id}`)
        .expect(200);

      expect(body).to.have.property('response');
      expect(body.response).to.have.all.keys('totals', 'details');
      expect(body.response.totals).to.have.all.keys(
        'dueDate',
        'totalAmount',
        'totalAccounts',
        'received',
        'receivedAccounts',
        'outstanding',
        'outstandingAccounts'
      );
      body.response.details.forEach((e) => {
        expect(e).to.have.all.keys(
          'account',
          'commitment',
          'called',
          'lifetimeCalled',
          'received',
          'receivedDate',
          'investorId'
        );
      });

      await userAdmin.deleteOne();
      await admin.deleteOne();
      await user.deleteOne();
      await investor.deleteOne();
      await llc.deleteOne();
      await capitalcall.deleteOne();
      await capitalcallInvestor.deleteOne();
    });
  });

  describe('GET /admin/llc/:llcId/transactions/capital-call/select', () => {
    it('should get selected created Capital Call', async () => {
      const email = faker.internet.email();
      const emailInvestor = faker.internet.email();
      const anAdmin = await FUserAdmin(email);
      const anInvestor = await FUserInvestor(emailInvestor);
      const [userAdmin, admin] = await Promise.all(anAdmin);
      const [user, investor] = await Promise.all(anInvestor);
      const llc = await factoryLlcs();
      const capitalcall: IModelCapitalCall = await FCapitalCall(llc.id);
      const capitalcallInvestor: IModelCapitalCallInvestor =
        await FCapitalCallInvestor(capitalcall.id, investor.id);
      await FContribution(capitalcallInvestor.id);

      await global.testServer
        .post(`/admin/login`)
        .send({
          user: email,
          password: global.adminPass,
        })
        .expect(200);

      const { body } = await global.testServer
        .get(`/admin/llc/${llc.id}/transactions/capital-call/select`)
        .expect(200);

      expect(body).to.have.property('response');
      body.response.forEach((e) => {
        expect(e).to.have.all.keys(
          'id',
          'description',
          'amountCalled',
          'result',
        );
      });

      await userAdmin.deleteOne();
      await admin.deleteOne();
      await user.deleteOne();
      await investor.deleteOne();
      await llc.deleteOne();
      await capitalcall.deleteOne();
      await capitalcallInvestor.deleteOne();
      await ModelContribution.deleteMany();
    });

    it('should get selected created Capital Call with search', async () => {
      const email = faker.internet.email();
      const emailInvestor = faker.internet.email();
      const anAdmin = await FUserAdmin(email);
      const anInvestor = await FUserInvestor(emailInvestor);
      const [userAdmin, admin] = await Promise.all(anAdmin);
      const [user, investor] = await Promise.all(anInvestor);
      const llc = await factoryLlcs();
      const capitalcall: IModelCapitalCall = await FCapitalCall(llc.id);
      const capitalcallInvestor: IModelCapitalCallInvestor =
        await FCapitalCallInvestor(capitalcall.id, investor.id);
      await FContribution(capitalcallInvestor.id);

      await global.testServer
        .post(`/admin/login`)
        .send({
          user: email,
          password: global.adminPass,
        })
        .expect(200);

      const stringCapitalCall = 'testCapitalCall';
      capitalcall.description = stringCapitalCall;
      await capitalcall.save();

      const { body } = await global.testServer
        .get(`/admin/llc/${llc.id}/transactions/capital-call/select?search=${stringCapitalCall}`)
        .expect(200);

      expect(body).to.have.property('response');
      expect(body.response).to.have.length(1);

      await userAdmin.deleteOne();
      await admin.deleteOne();
      await user.deleteOne();
      await investor.deleteOne();
      await llc.deleteOne();
      await capitalcall.deleteOne();
      await capitalcallInvestor.deleteOne();
      await ModelContribution.deleteMany();
    });
  });

  describe('PUT /admin/llc/:llcId/capital-call/:capitalCallId', () => {
    it('should update a new Capital Call', async () => {
      const email = faker.internet.email();
      const emailInvestor = faker.internet.email();
      const anAdmin = await FUserAdmin(email);
      const anInvestor = await FUserInvestor(emailInvestor);
      const [userAdmin, admin] = await Promise.all(anAdmin);
      const [user, investor] = await Promise.all(anInvestor);
      const llc = await factoryLlcs();
      const capitalcall: IModelCapitalCall = await FCapitalCall(llc.id);

      await global.testServer
        .post(`/admin/login`)
        .send({
          user: email,
          password: global.adminPass,
        })
        .expect(200);

      const dataCapitalCall = {
        description: faker.name.title(),
        dueDate: new Date().toISOString(),
      };

      const { body } = await global.testServer
        .put(`/admin/llc/${llc.id}/capital-call/${capitalcall.id}`)
        .send(dataCapitalCall)
        .expect(200);
      expect(body).to.have.property('response');

      const updatedCapitalCall: IModelCapitalCall =
        await ModelCapitalCall.findById(capitalcall.id);
      expect(updatedCapitalCall.description).to.be.equal(
        dataCapitalCall.description
      );

      await userAdmin.deleteOne();
      await admin.deleteOne();
      await user.deleteOne();
      await investor.deleteOne();
      await llc.deleteOne();
      await capitalcall.deleteOne();
    });
  });

  describe('DELETE /admin/llc/:llcId/capital-call/:capitalCallId', () => {
    it('should delete a new Capital Call', async () => {
      const email = faker.internet.email();
      const emailInvestor = faker.internet.email();
      const anAdmin = await FUserAdmin(email);
      const anInvestor = await FUserInvestor(emailInvestor);
      const [userAdmin, admin] = await Promise.all(anAdmin);
      const [user, investor] = await Promise.all(anInvestor);
      const llc = await factoryLlcs();
      const capitalcall: IModelCapitalCall = await FCapitalCall(llc.id);

      await global.testServer
        .post(`/admin/login`)
        .send({
          user: email,
          password: global.adminPass,
        })
        .expect(200);

      const dataCapitalCall = {
        description: faker.name.title(),
        dueDate: new Date().toISOString(),
      };

      const { body } = await global.testServer
        .delete(`/admin/llc/${llc.id}/capital-call/${capitalcall.id}`)
        .send(dataCapitalCall)
        .expect(200);
      expect(body).to.have.property('response');

      const deletedCapitalCall: IModelCapitalCall =
        await ModelCapitalCall.findById(capitalcall.id);
      expect(deletedCapitalCall.deletedBy.dateTime).to.be.not.null;

      await userAdmin.deleteOne();
      await admin.deleteOne();
      await user.deleteOne();
      await investor.deleteOne();
      await llc.deleteOne();
      await capitalcall.deleteOne();
    });
  });
});
