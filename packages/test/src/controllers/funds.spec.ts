import 'mocha';
import { expect } from 'chai';
import {
  IModelFund,
  IModelFundInvestor,
  IModelFundPackage,
  IModelFundProperty,
  ModelFund,
  ModelFundInvestor,
  ModelFundPackage,
  ModelFundProperty,
} from '@capa/data';
import { FUserInvestor, FUserAdmin, FProperty, FPackage } from '../helpers';
import Bignumber from 'bignumber.js';
import faker from 'faker';
import { Ffunds } from '../helpers/funds';

describe('Fund Controller Test', () => {
  describe('POST /admin/fund', async () => {
    it('should create a fund', async () => {
      const emailAdmin = faker.internet.email();
      const email = faker.internet.email();
      const anAdmin = await FUserAdmin(emailAdmin);
      const anInvestor = await FUserInvestor(email);
      const [userAdmin, admin] = await Promise.all(anAdmin);
      const [user, investor] = await Promise.all(anInvestor);
      const thisProperty = await FProperty();
      const [property] = await Promise.all(thisProperty);
      const newPack = await FPackage();

      await global.testServer
        .post(`/admin/login`)
        .send({
          user: emailAdmin,
          password: global.adminPass,
        })
        .expect(200);

      const dataFund = {
        name: faker.name.firstName(),
        cashHand: faker.datatype.number(10000),
        investors: [
          {
            id: investor._id,
            minCommitment: faker.datatype.number(10000),
            initCommitment: faker.datatype.number(10000),
            totalCommitment: faker.datatype.number(10000),
            termYears: faker.datatype.number(10),
          },
        ],
        packages: [
          {
            id: newPack._id,
            share: 100,
          },
        ],
        properties: [
          {
            id: property._id,
            share: 100,
          },
        ],
      };

      const { body } = await global.testServer
        .post(`/admin/fund`)
        .send(dataFund)
        .expect(200);

      expect(body.response).to.have.all.keys('id');
      expect(body).to.have.property('response');
      expect(body).to.have.property('message');

      const newFund: IModelFund = await ModelFund.findById(body.response.id);
      const newFundInvestor: IModelFundInvestor =
        await ModelFundInvestor.findOne({
          fund: newFund._id.toHexString(),
        });
      const newFundProperty: IModelFundProperty =
        await ModelFundProperty.findOne({
          fund: newFund._id,
        });
      const newFundPackage: IModelFundPackage = await ModelFundPackage.findOne({
        fund: newFund._id,
      });

      expect(newFund).not.to.be.null;
      expect(newFundInvestor).not.to.be.null;
      expect(newFundProperty).not.to.be.null;
      expect(newFundPackage).not.to.be.null;

      await userAdmin.deleteOne();
      await admin.deleteOne();
      await user.deleteOne();
      await investor.deleteOne();
      await property.deleteOne();
      await newPack.deleteOne();
      await ModelFundInvestor.deleteMany();
      await ModelFundProperty.deleteMany();
      await ModelFundPackage.deleteMany();
    });
  });

  describe('PUT /admin/fund', async () => {
    it('should update a created fund', async () => {
      const emailAdmin = faker.internet.email();
      const email = faker.internet.email();
      const anAdmin = await FUserAdmin(emailAdmin);
      const anInvestor = await FUserInvestor(email);
      const [userAdmin, admin] = await Promise.all(anAdmin);
      const [user, investor] = await Promise.all(anInvestor);
      const thisProperty = await FProperty();
      const [property] = await Promise.all(thisProperty);
      const newPack = await FPackage();
      const funds = await Ffunds();

      await global.testServer
        .post(`/admin/login`)
        .send({
          user: emailAdmin,
          password: global.adminPass,
        })
        .expect(200);

      const dataFund = {
        name: faker.name.firstName(),
        cashHand: faker.datatype.number(10000),
        investors: [
          {
            id: investor._id,
            minCommitment: faker.datatype.number(10000),
            initCommitment: faker.datatype.number(10000),
            totalCommitment: faker.datatype.number(10000),
            termYears: faker.datatype.number(10),
          },
        ],
        packages: [
          {
            id: newPack._id,
            share: 100,
          },
        ],
        properties: [
          {
            id: property._id,
            share: 100,
          },
        ],
      };

      await global.testServer
        .put(`/admin/fund/${funds.id}`)
        .send(dataFund)
        .expect(200);

      const newFund: IModelFund = await ModelFund.findById(funds.id);
      expect(newFund.name).to.be.equal(dataFund.name);

      const newFundInvestor: IModelFundInvestor =
        await ModelFundInvestor.findOne({
          fund: newFund._id.toHexString(),
        });
      expect(newFundInvestor.termYears).to.be.equal(
        dataFund.investors[0].termYears
      );

      const newFundProperty: IModelFundProperty =
        await ModelFundProperty.findOne({
          fund: newFund._id,
        });
      const sharePropBig = new Bignumber(newFundProperty.share);
      const sharePropF = parseFloat(sharePropBig.toNumber().toFixed(2));
      expect(sharePropF).to.be.equal(dataFund.properties[0].share);

      const newFundPackage = await ModelFundPackage.findOne({
        fund: newFund._id,
      });
      const sharePackBig = new Bignumber(newFundPackage.share);
      const sharePackF = parseFloat(sharePackBig.toNumber().toFixed(2));
      expect(sharePackF).to.be.equal(dataFund.packages[0].share);

      await userAdmin.deleteOne();
      await admin.deleteOne();
      await user.deleteOne();
      await investor.deleteOne();
      await property.deleteOne();
      await newPack.deleteOne();
      await funds.deleteOne();
      await ModelFundInvestor.deleteMany();
      await ModelFundProperty.deleteMany();
      await ModelFundPackage.deleteMany();
    });
  });

  describe('GET /fund/select', async () => {
    it('should return Funds', async () => {
      const emailAdmin = faker.internet.email();
      const email = faker.internet.email();
      const anAdmin = await FUserAdmin(emailAdmin);
      const anInvestor = await FUserInvestor(email);
      const [userAdmin, admin] = await Promise.all(anAdmin);
      const [user, investor] = await Promise.all(anInvestor);
      const thisProperty = await FProperty();
      const [property] = await Promise.all(thisProperty);
      const funds = await Ffunds();

      await global.testServer
        .post(`/admin/login`)
        .send({
          user: emailAdmin,
          password: global.adminPass,
        })
        .expect(200);

      const { body } = await global.testServer
        .get(`/fund/select?search=${funds.name}`)
        .expect(200);
      const response = body.response;

      expect(response.length).to.be.greaterThan(0);
      expect(body).to.have.property('response');

      await userAdmin.deleteOne();
      await user.deleteOne();
      await admin.deleteOne();
      await investor.deleteOne();
      await property.deleteOne();
      await funds.deleteOne();
    });
  });

  describe('POST /admin/fund/preview', async () => {
    it('should get preview of a Fund', async () => {
      const emailAdmin = faker.internet.email();
      const email = faker.internet.email();
      const anAdmin = await FUserAdmin(emailAdmin);
      const anInvestor = await FUserInvestor(email);
      const [userAdmin, admin] = await Promise.all(anAdmin);
      const [user, investor] = await Promise.all(anInvestor);
      const thisProperty = await FProperty();
      const [property] = await Promise.all(thisProperty);
      const newPack = await FPackage();
      const funds = await Ffunds();

      await global.testServer
        .post(`/admin/login`)
        .send({
          user: emailAdmin,
          password: global.adminPass,
        })
        .expect(200);

      const dataFund = {
        cashHand: faker.datatype.number(1000),
        properties: [
          {
            id: property._id.toHexString(),
            share: 10,
          },
        ],
        packages: [
          {
            id: newPack._id.toHexString(),
            share: 10,
          },
        ],
      };

      const { body } = await global.testServer
        .post(`/admin/fund/preview`)
        .send(dataFund)
        .expect(200);
      expect(body).to.have.property('response');
      expect(body.response).to.have.property('totalValue');

      await userAdmin.deleteOne();
      await user.deleteOne();
      await admin.deleteOne();
      await investor.deleteOne();
      await property.deleteOne();
      await funds.deleteOne();
      await newPack.deleteOne();
    });
  });

  describe('GET /admin/fund/:fundId', async () => {
    it('should get detail for a fund', async () => {
      const emailAdmin = faker.internet.email();
      const email = faker.internet.email();
      const anAdmin = await FUserAdmin(emailAdmin);
      const anInvestor = await FUserInvestor(email);
      const [userAdmin, admin] = await Promise.all(anAdmin);
      const [user, investor] = await Promise.all(anInvestor);
      const thisProperty = await FProperty();
      const [property] = await Promise.all(thisProperty);
      const newPack = await FPackage();
      const funds = await Ffunds();

      await global.testServer
        .post(`/admin/login`)
        .send({
          user: emailAdmin,
          password: global.adminPass,
        })
        .expect(200);

      const { body } = await global.testServer
        .get(`/admin/fund/${funds.id}`)
        .expect(200);

      expect(body).to.have.property('response');
      expect(body.response).to.have.property('id');
      expect(body.response).to.have.property('name');
      expect(body.response).to.have.property('cashHand');
      expect(body.response).to.have.property('investors');
      expect(body.response).to.have.property('properties');
      expect(body.response.properties[0]).to.have.all.keys(
        'id',
        'mainImage',
        'name',
        'share'
      );
      expect(body.response).to.have.property('packages');
      expect(body.response.packages[0]).to.have.all.keys(
        'id',
        'mainImage',
        'name',
        'share'
      );
      expect(body.response).to.have.property('investors');
      expect(body.response.investors[0]).to.have.all.keys(
        'id',
        'name',
        'minCommitment',
        'initCommitment',
        'totalCommitment',
        'termYears'
      );

      await userAdmin.deleteOne();
      await admin.deleteOne();
      await user.deleteOne();
      await investor.deleteOne();
      await property.deleteOne();
      await newPack.deleteOne();
      await funds.deleteOne();
      await ModelFundInvestor.deleteMany();
      await ModelFundProperty.deleteMany();
      await ModelFundPackage.deleteMany();
    });
  });

  describe('DELETE /admin/fund/', async () => {
    it('should return Fund deleted', async () => {
      const funds = await Ffunds();
      const emailAdmin = faker.internet.email();
      const anAdmin = await FUserAdmin(emailAdmin);
      const [userAdmin, admin] = await Promise.all(anAdmin);

      await global.testServer
        .post(`/admin/login`)
        .send({
          user: emailAdmin,
          password: global.adminPass,
        })
        .expect(200);

      const deleteFund = await global.testServer
        .delete(`/admin/fund/${funds.id}`)
        .expect(200);
      const { body } = deleteFund;
      expect(body).to.have.all.keys('message', 'response');
      await userAdmin.deleteOne();
      await admin.deleteOne();
    });
  });
});
