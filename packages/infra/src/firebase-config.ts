import * as admin from 'firebase-admin';
import { inject, injectable } from 'inversify';
import {
  BaseError,
  BASETYPES,
  IBaseLogger,
  IFirebaseEnvironment,
} from '@capa/core';

@injectable()
export class FirebaseConfig {
  constructor(
    @inject(BASETYPES.Log) private _log: IBaseLogger,
    @inject(BASETYPES.IFirebaseConfig) private _config: IFirebaseEnvironment
  ) {}

  public init(): void {
    const serviceAccount = {
      private_key: this._config.privateKey,
      project_id: this._config.projectId,
      client_email: this._config.clientEmail,
    } as admin.ServiceAccount;
    admin.initializeApp({
      credential: admin.credential.cert(serviceAccount),
      databaseURL: this._config.databaseUrl,
    });
    this._log.info(`firebase admin config success ${admin.SDK_VERSION}`);
  }

  public generateTokenByUser(user: string): string | any {
    try {
      const customToken = admin
        .auth()
        .createCustomToken(user)
        .then((createToken) => createToken)
        .catch((error) =>
          this._log.error(`Error creating custom token: ${error}`)
        );
      return customToken;
    } catch (error) {
      throw new BaseError(error.message, error.code, error.name, error);
    }
  }
}
