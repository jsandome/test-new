import { inject, injectable } from 'inversify';
import {
  BaseError,
  BASETYPES,
  EnumEnvironments,
  IBaseLogger,
  IBndBaseGCloudInfra,
} from '@capa/core';
import { GCloudConfig } from '../gcloud-config';

/**
 * Available methods to manipulate Buckets
 */
@injectable()
export class ImplBndBaseGCloudInfra implements IBndBaseGCloudInfra {
  @inject(BASETYPES.Log) private _log: IBaseLogger;
  constructor(private storage: GCloudConfig) {}

  public async uploadFile(urlObject: string): Promise<boolean> {
    const bucket = this.storage.defaultBucket;

    await bucket.upload(urlObject);

    this._log.info(`${urlObject} uploaded to ${this.storage.bucketName}`);

    return true;
  }

  public async moveFile(
    urlTmpObject: string,
    destinationObjectPath: string
  ): Promise<boolean> {
    try {
      const bucket = this.storage.defaultBucket;

      if (process.env.NODE_ENV !== EnumEnvironments.TEST) {
        await bucket.file(urlTmpObject).move(destinationObjectPath);
        await bucket.file(destinationObjectPath).makePublic();
      }
      return true;
    } catch (error) {
      throw new BaseError(
        `Error trying to move a image - origin:${urlTmpObject}, destination ${destinationObjectPath} \n${error.message}`,
        error.code,
        error
      );
    }
  }

  public async checkFileExist(url: string): Promise<boolean> {
    try {
      if (process.env.NODE_ENV === EnumEnvironments.TEST) return true;
      const bucket = this.storage.defaultBucket;
      const file = bucket.file(url);
      const isTheUrlValid = await file.exists();
      return isTheUrlValid[0];
    } catch (error) {
      throw new BaseError(
        `Error trying to check the url:${url} - bucket:${this.storage.bucketName}\n${error.message}`,
        error.code,
        error
      );
    }
  }

  public async deleteFile(urlObject: string): Promise<void> {
    try {
      const bucket = this.storage.defaultBucket;
      if (process.env.NODE_ENV !== EnumEnvironments.TEST) {
        await bucket.file(urlObject).delete();
      }
    } catch (error) {
      throw new BaseError(
        'Error in GCS trying to deleting file',
        500,
        'BaseUseCaseProperty | deleteFile',
        error
      );
    }
  }

  public async getPublicUrl(): Promise<string> {
    return this.storage.publicUrl;
  }

  public getRelativePathFromFull(fullPath: string): string {
    const bucketName = this.storage.publicUrl;
    return fullPath.replace(`${bucketName}/`, '');
  }

  public async getListOfFiles(prefix?: string): Promise<string[]> {
    try {
      const bucket = this.storage.defaultBucket;
      const [files] = await bucket.getFiles({ prefix: prefix ? prefix : '' });
      return files.map((e) => e.name);
    } catch (error) {
      throw new BaseError(
        `Error in GCS trying to get a list of files. ${error.message}`,
        error.code,
        'BaseUseCaseProperty | getListOfFiles',
        error
      );
    }
  }

  public async dowloandFileAsBuffer(path: string): Promise<Buffer> {
    return new Promise((resolve, reject) => {
      const fileContents = [];
      const bucket = this.storage.defaultBucket;
      const file = bucket.file(path);
      file
        .createReadStream()
        .on('data', (data) => {
          fileContents.push(data);
        })
        .on('end', () => {
          const response: Buffer = Buffer.concat(fileContents);
          resolve(response);
        })
        .on('error', (error) => reject(error));
    });
  }

  public async copyFile(
    originalPath: string,
    destinationPath: string
  ): Promise<void> {
    try {
      const bucket = this.storage.defaultBucket;
      if (process.env.NODE_ENV !== EnumEnvironments.TEST) {
        await bucket.file(originalPath).copy(destinationPath);
        await bucket.file(destinationPath).makePublic();
      }
    } catch (error) {
      throw new BaseError(
        `Error trying to copy a image - origin:${originalPath}, destination ${destinationPath} \n${error.message}`,
        error.code,
        error
      );
    }
  }
}
