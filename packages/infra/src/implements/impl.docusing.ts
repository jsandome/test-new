import {
  Configuration,
  ApiClient,
  TemplatesApi,
  EnvelopesApi,
  EnvelopeDefinition,
  TemplateRole,
  Tabs,
  Text,
  RecipientViewRequest,
} from 'docusign-esign';
import { inject, injectable } from 'inversify';
import {
  IBndBaseDocusingInfra,
  BASETYPES,
  DocusingEnvironment,
  BaseError,
  IBaseLogger,
  ListTemplates,
  UserInfoDocusign,
} from '@capa/core';
import moment from 'moment';
import { Promise } from 'bluebird';
@injectable()
export class ImplBndDocusing implements IBndBaseDocusingInfra {
  @inject(BASETYPES.Log) private _log: IBaseLogger;

  // private constants and globals
  private tokenReplaceMin = 10; // The accessToken must expire at least this number of
  // minutes later or it will be replaced

  private tokenExpirationTimestamp = null; // when does the accessToken expire?
  private accessToken = null; // The bearer accessToken
  private accountId = null; // current account
  private basePath = null; // current basePath

  constructor(
    @inject(BASETYPES.DocusingEnvironment) private _config: DocusingEnvironment
  ) {}

  //TODO: Falta definir que informacion podrá ir en esta funcion y el tipo de retorno :D

  /**
   * Send an envelope.
   * @param userInfo User info
   */
  public async sendEnvelope(userInfo: UserInfoDocusign): Promise<void> {
    const { firstName, lastName, address, email, date } = userInfo;
    const signerName = firstName + ' ' + lastName;
    await this.updateApiClient();

    // Create the envelope request
    // Start with the request object
    const envDef = new EnvelopeDefinition();
    // Set the Email Subject line and email message
    envDef.emailSubject = 'Please sign this document.';
    envDef.emailBlurb = 'Please sign this document.';

    envDef.templateId = '2a6725cf-b70c-40fc-b4a9-bb286343de13'; //TODO: Cambiar por el templateID que se haya seleccionado

    const customer = TemplateRole.constructFromObject({
      name: signerName,
      email: email,
      clientUserId: this._config.DOCUSIGN_USER_ID,
      routingOrder: '1',
      recipientId: '1',
      roleName: 'CUSTOMER', // Role must exist in template
    });

    customer.tabs = new Tabs();
    const textTab1 = new Text();
    const textTab2 = new Text();
    const textTab3 = new Text();

    textTab1.tabLabel = 'NameCustomer'; // Custom field must match label
    textTab1.value = signerName;

    textTab2.tabLabel = 'CustomerAddress'; // Custom field must match label
    textTab2.value = address;

    textTab3.tabLabel = 'DateBirthday'; // Custom field must match label
    textTab3.value = date;

    customer.tabs.textTabs = [textTab1, textTab2, textTab3];
    envDef.templateRoles = [customer];

    // Set the Envelope status. For drafts, use 'created' To send the envelope right away, use 'sent'
    envDef.status = 'sent';
    // Send the envelope
    // The SDK operations are asynchronous, and take callback functions.
    // However we'd pefer to use promises.
    // So we create a promise version of the SDK's createEnvelope method.
    const envelopesApi = new EnvelopesApi();
    // createEnvelopePromise returns a promise with the results:

    const createEnvelopePromise = await Promise.promisify(
      envelopesApi.createEnvelope
    ).bind(envelopesApi);
    let results;

    try {
      const accountId = this.accountId || this._config.DOCUSIGN_ACCOUNT_ID;
      results = await createEnvelopePromise(accountId, {
        envelopeDefinition: envDef,
      });
    } catch (e) {
      const body = e.response && e.response.body;
      this._log.error(body);
      this._log.error(e);
      throw new BaseError('Error in Docusing', 500, e);
    }
    // Envelope has been created:
    const response = await this.getEnvelopeUrl(results.envelopeId, userInfo);
    return { ...results, ...response };
  }

  public async getEnvelopeUrl(
    envelopeId: string,
    userInfo: UserInfoDocusign
  ): Promise<void> {
    const { firstName, lastName, email } = userInfo;
    const signerName = firstName + ' ' + lastName;
    await this.updateApiClient();
    const envelopesApi = new EnvelopesApi();
    let results;

    try {
      const recipientViewRequest = RecipientViewRequest.constructFromObject({
        authenticationMethod: 'none',
        clientUserId: this._config.DOCUSIGN_USER_ID,
        recipientId: '1',
        returnUrl: 'https://www.google.com.mx', //TODO: CAMBIAR LA RUTA DEFINIDA POR FROTEND
        userName: signerName,
        email: email,
      });
      const createRecipientViewPromise = Promise.promisify(
        envelopesApi.createRecipientView
      ).bind(envelopesApi);

      const accountId = this.accountId || this._config.DOCUSIGN_ACCOUNT_ID;
      results = await createRecipientViewPromise(accountId, envelopeId, {
        recipientViewRequest,
      });
    } catch (e) {
      const body = e.response && e.response.body;
      this._log.error(body);
      this._log.error(e);
    }
    return results;
  }

  public async listTemplates(): Promise<ListTemplates[]> {
    try {
      await this.updateApiClient();
      const array = [];
      const envelopesApi = new TemplatesApi();
      const results: ListTemplates = await envelopesApi.listTemplates(
        this._config.DOCUSIGN_ACCOUNT_ID,
        null
      );

      for (let i = 0; i < results.envelopeTemplates.length; i++) {
        const { created, templateId, name, description, id } =
          results.envelopeTemplates[i];

        const data = {
          id: id || null,
          description,
          name,
          templateId,
          date: created,
        };
        array.push(data);
      }

      return array;
    } catch (error) {
      throw new BaseError('Error in Docusing listTemplates', 500, error);
    }
  }

  /**
   * Get the envelope information
   * @param envelopeId The envelope id
   */

  //TODO: Se necesita cambiar el envelopeID segun el documento que se requiera consultar

  public async getEnvelope() {
    await this.updateApiClient();
    const envelopesApi = new EnvelopesApi();

    const getEnvelopePromise = Promise.promisify(envelopesApi.getEnvelope).bind(
      envelopesApi
    );
    let results;
    try {
      const accountId = this.accountId || this._config.DOCUSIGN_ACCOUNT_ID;
      results = await getEnvelopePromise(
        accountId,
        'ef4bb0de-0e97-4c28-a04d-07a57a68b640'
      );
    } catch (err) {
      this._log.error(err);
    }
    return results;
  }

  /**
   * Update de current api client
   */
  async updateApiClient(): Promise<void> {
    await this.checkToken();
    const apiClient = new ApiClient();
    const basePath = this.basePath || this._config.DOCUSIGN_BASEPATH;
    apiClient.setBasePath(basePath);
    const accessToken = this.accessToken || this._config.DOCUSIGN_ACCESS_TOKEN;
    apiClient.addDefaultHeader('Authorization', 'Bearer ' + accessToken);
    // Set the DocuSign SDK components to use the apiClient object
    Configuration.default.setDefaultApiClient(apiClient);
  }

  /**
   * It should be called before any API call to DocuSign.
   * It checks that the existing access accessToken can be used.
   * If the existing accessToken is expired or doesn't exist, then
   * a new accessToken will be obtained from DocuSign by using
   * the JWT flow.
   *
   * This is an async function so call it with await.
   *
   * SIDE EFFECT: Sets the access accessToken that the SDK will use.
   * SIDE EFFECT: If the accountId et al is not set, then this method will
   *              also get the user's information
   * @return {promise} a promise with null result.
   */
  async checkToken() {
    const noToken = !this.accessToken || !this.tokenExpirationTimestamp;
    const now = moment();
    const needToken = noToken || this.tokenExpirationTimestamp.isBefore(now);
    if (noToken) {
      this._log.info('checkToken: Starting up--need an accessToken');
    }
    if (needToken && !noToken) {
      this._log.info('checkToken: Replacing old accessToken');
    }
    // if (!needToken) {this._log.info('checkToken: Using current accessToken')}

    if (needToken) {
      const results = await this.getToken();
      this.accessToken = results.accessToken;
      this.tokenExpirationTimestamp = results.tokenExpirationTimestamp;
      this._log.info('Obtained an access token. Continuing...');

      if (!this.accountId) {
        await this.getUserInfo();
      }
    }
  }

  /**
   * Async function to obtain a accessToken via JWT grant
   * For tests obtain an OAuth token from https://developers.docusign.com/oauth-token-generator
   *
   * IMPORTANT To use this method:
   *  - The user must be an administrator for company.
   *  - The app must be connected to the company with signature impersonation permissions.
   *  - The company must claim user mail domain.
   *
   * @return {accessToken, tokenExpirationTimestamp}
   */
  async getToken() {
    const jwtLifeSec = 10 * 60; // requested lifetime for the JWT is 10 min
    const scopes = 'signature'; // impersonation scope is implied due to use of JWT grant
    const dsApi = new ApiClient();

    dsApi.setOAuthBasePath(this._config.DOCUSIGN_AUTH_SERVER);
    let results;
    try {
      results = await dsApi.requestJWTUserToken(
        this._config.DOCUSIGN_APP_ID,
        this._config.DOCUSIGN_USER_ID,
        scopes,
        this._config.DOCUSIGN_PRIVATE_RSA,
        jwtLifeSec
      );
    } catch (err) {
      throw new BaseError('Error in Docusing getToken', 500, err);
    }
    const expiresAt = moment()
      .add(results.body.expires_in, 's')
      .subtract(this.tokenReplaceMin, 'm');
    return {
      accessToken: results.body.access_token,
      tokenExpirationTimestamp: expiresAt,
    };
  }

  /**
   * Sets the following variables:
   * accountId
   * basePath
   */
  async getUserInfo(): Promise<void> {
    const dsApi = new ApiClient();
    dsApi.setOAuthBasePath(this._config.DOCUSIGN_AUTH_SERVER);

    let results;
    try {
      results = await dsApi.getUserInfo(this.accessToken);
    } catch (err) {}

    const accountInfo = results.accounts.find(
      (account) => account.isDefault === 'true' || account.isDefault === true
    );

    this.basePath = accountInfo.baseUri + '/restapi';
    this.accountId = accountInfo.accountId;
  }

  /**
   * Clears the accessToken. Same as logging out
   */
  async clearToken(): Promise<void> {
    // "logout" function
    this.tokenExpirationTimestamp = false;
    this.accessToken = false;
  }
}
// oAuthBasePath:'account-d.docusign.com'
// basePath:'https://demo.docusign.net/restapi'
// basePath: "account-d.docusign.com"
