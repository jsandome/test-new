# Doscusign module

This document is provided with important information about Docusign configuration.

## OAuth

It must be created a company, then:

- The user must be an administrator for company.
- The app must be connected to the company with signature impersonation permissions.
- The company must claim user mail domain.

## Templates

Templates must be loaded on `Templates` admin section.
In case of need them, you can use the ones in `templates` folder.

### Environment Variables For Using this module

```bash

# Docusign
- DOCUSIGN_USER_ID
- DOCUSIGN_ACCOUNT_ID
- DOCUSIGN_APP_ID
- DOCUSIGN_ACCESS_TOKEN
- DOCUSIGN_PRIVATE_RSA

```
