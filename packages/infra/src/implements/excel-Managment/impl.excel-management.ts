import { injectable } from 'inversify';
import {
  BaseError,
  exPropertyBaseGralInf,
  IBaseEntityProperty,
  IExcelPropertyInformation,
  IBasePropertyExcelValidation,
  IBaseSheetProjectionData,
  IBndBaseExcelManagementInfra,
  IExcelReadParams,
  IPropertiesDuplicatedExcelParams,
  IPropertyBaseError,
  IPropertyUploadPropertyInformationByExcelParams,
  IExcelPropertiesAndProjections,
  IPropertyProfitsAndLossExcelInfo,
  IPropertyProfitsAndLossExcelResponse,
  EnumPropertyProfitAndLossSheetNames,
} from '@capa/core';
import * as XLSX from 'xlsx';
import {
  getPropertiesByExcel,
  getProjectionInformation,
  validateExcelInformationAndGetLocations,
  getProfitAndLossInfoByExcel,
} from './';
export interface ICellAddress {
  column: string;
  row: number;
}

@injectable()
export class ImplBndBaseExcelManagement
  implements IBndBaseExcelManagementInfra
{
  public async getExcelPropertiesAndProjectionsInfo(
    data: IPropertyUploadPropertyInformationByExcelParams
  ): Promise<IExcelPropertiesAndProjections> {
    try {
      const workbook: XLSX.WorkBook = await this._getXlsDocument(data);

      const propertiesInfo: IExcelPropertyInformation = !workbook.Sheets[
        exPropertyBaseGralInf.sheetName
      ]
        ? null
        : await this._getPropertiesExcelInformation(
            workbook.Sheets[exPropertyBaseGralInf.sheetName],
            data
          );
      const projectionSheets = workbook.SheetNames.filter((e) =>
        e.startsWith('Projection')
      );
      const projections: IBaseSheetProjectionData[] =
        projectionSheets.length === 0
          ? null
          : await this._getProjectionsExcelInformation(
              workbook,
              projectionSheets,
              data,
              propertiesInfo ? propertiesInfo.properties : []
            );
      if (!propertiesInfo || !projections) {
        if (!propertiesInfo && !projections) {
          throw new BaseError(
            'Cannot find properties or projections in the excel.',
            400,
            'ImplBndBaseExcelManagement | getExcelPropertiesAndProjectionsInfo'
          );
        } else if (!propertiesInfo) {
          throw new BaseError(
            'Cannot find properties in the excel.',
            400,
            'ImplBndBaseExcelManagement | getExcelPropertiesAndProjectionsInfo'
          );
        } else {
          throw new BaseError(
            'Cannot find projections in the excel.',
            400,
            'ImplBndBaseExcelManagement | getExcelPropertiesAndProjectionsInfo'
          );
        }
      }
      return {
        propertiesInfo,
        projections,
      };
    } catch (error) {
      throw new BaseError(error.message, error.code, error);
    }
  }

  public async getProfitsAndLossByExcelPreview(
    data: IPropertyUploadPropertyInformationByExcelParams
  ): Promise<IPropertyProfitsAndLossExcelResponse> {
    const workbook: XLSX.WorkBook = await this._getXlsDocument(data);
    return await this._getProfitsAndLossInformation(
      workbook.Sheets[EnumPropertyProfitAndLossSheetNames.PROFIT_AND_LOSS],
      data
    );
  }

  private async _getProfitsAndLossInformation(
    worksheet: XLSX.WorkSheet,
    data: IPropertyUploadPropertyInformationByExcelParams
  ): Promise<IPropertyProfitsAndLossExcelResponse> {
    const profitAndLossInformation: IPropertyProfitsAndLossExcelInfo[] =
      await getProfitAndLossInfoByExcel(worksheet, {
        fileName: data.fileName,
        SheetName: EnumPropertyProfitAndLossSheetNames.PROFIT_AND_LOSS,
        bndPropertyRead: data.bndPropertyRead,
      });
    //TODO add the validations for the excel profits and loss
    return {
      profitsAndLoss: profitAndLossInformation,
      errors: null,
    };
  }

  private async _getProjectionsExcelInformation(
    workbook: XLSX.WorkBook,
    sheetNames: string[],
    data: IPropertyUploadPropertyInformationByExcelParams,
    excelProperties: IBaseEntityProperty[]
  ): Promise<IBaseSheetProjectionData[]> {
    const response: IBaseSheetProjectionData[] = [];
    for (const SheetName of sheetNames) {
      const params: IExcelReadParams = {
        fileName: data.fileName,
        SheetName: SheetName,
        bndPropertyRead: data.bndPropertyRead,
      };
      const sheedData: IBaseSheetProjectionData =
        await getProjectionInformation(
          workbook.Sheets[SheetName],
          excelProperties,
          params
        );
      response.push(sheedData);
    }
    return response;
  }

  private async _getPropertiesExcelInformation(
    worksheet: XLSX.WorkSheet,
    data: IPropertyUploadPropertyInformationByExcelParams
  ): Promise<IExcelPropertyInformation> {
    const { bndPropertyRead, bndLocationsRead } = data;
    const inputProperties: IBaseEntityProperty[] = await getPropertiesByExcel(
      worksheet,
      { fileName: data.fileName, SheetName: null, bndPropertyRead: null }
    );

    const duplicatedPropertiesInitial: IPropertiesDuplicatedExcelParams = {
      address: [],
      names: [],
    };
    const duplicatedProperties: IBasePropertyExcelValidation[] =
      await bndPropertyRead.getPropertiesForExcel(
        inputProperties.reduce((p, c): IPropertiesDuplicatedExcelParams => {
          return {
            address: p.address.concat([c.address]),
            names: p.names.concat([c.name]),
          };
        }, duplicatedPropertiesInitial)
      );
    /*Not only validate the data, also save the locations information in the property object */
    const errorsInfo: IPropertyBaseError[] =
      await validateExcelInformationAndGetLocations(
        inputProperties,
        duplicatedProperties,
        bndLocationsRead
      );

    return {
      sheetName: `${exPropertyBaseGralInf.sheetName}`,
      properties: inputProperties,
      errors: errorsInfo,
    };
  }

  private async _getXlsDocument(
    data: IPropertyUploadPropertyInformationByExcelParams
  ): Promise<XLSX.WorkBook> {
    try {
      const { bndGCloudInfra, pathFile } = data;
      const bufferFile: Buffer = await bndGCloudInfra.dowloandFileAsBuffer(
        pathFile
      );
      return XLSX.read(bufferFile, { type: 'buffer' });
    } catch (error) {
      throw new BaseError(error.message, error.code, error);
    }
  }
}
