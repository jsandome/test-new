import {
  BaseError,
  IExcelReadParams,
  IProfitsAndLossExpensesRowInformationResponse,
  IProfitsAndlossIncomeExcelRows,
  IProfitsAndlossNetIncomeExcelRowsInfo,
  IProfitsAndLossRowConcept,
  IProfitsAndLossRowConceptResponse,
  IProfitsAndLossSheetRowsInformation,
  IPropertyProfitsAndLossDateInfo,
  IPropertyProfitsAndLossExcelCellInformation,
  IPropertyProfitsAndLossExcelInfo,
  IPropertyProfitsAndLossConcept,
} from '@capa/core';
import { CellObject, WorkSheet, utils } from 'xlsx';

const profitsAndLossCellAddress: IPropertyProfitsAndLossExcelCellInformation = {
  dateInfo: {
    column: 'A',
    columnNumber: 1,
    identification: 'A3',
    row: 3,
  },
  propertiesStartInfo: {
    column: 'B',
    columnNumber: 1,
    identification: 'B5',
    row: 5,
  },
};

const rowInfo = {
  firstRowInformation: 6,
  column: 'A',
  income: {
    name: 'Income',
    module: 'Investment Revenue',
  },
  grossProfit: {
    name: 'Gross Profit',
  },
  expenses: {
    name: 'Expenses',
  },
  netInfo: {
    incomeName: 'Net Income',
    operatingIncome: 'Net Operating Income',
  },
};

interface IExcelInformationParams {
  worksheet: WorkSheet;
  column: string;
}

export const getProfitAndLossInfoByExcel = async (
  worksheet: WorkSheet,
  options: IExcelReadParams
): Promise<IPropertyProfitsAndLossExcelInfo[]> => {
  const propertiesProfitsAndLoss: IPropertyProfitsAndLossExcelInfo[] = [];
  const { bndPropertyRead } = options;
  const date: IPropertyProfitsAndLossDateInfo =
    getProfitsAndLossDateByExcel(worksheet);
  const sheetConcepts: IProfitsAndLossSheetRowsInformation =
    getSheetRowInformation(worksheet);

  for (let col = 0; col < 702 /*ZZ*/; col++) {
    const column = utils.encode_col(
      profitsAndLossCellAddress.propertiesStartInfo.columnNumber + col
    );
    const params: IExcelInformationParams = {
      column,
      worksheet: worksheet,
    };
    const propertyName: string = getPropertyNameByExcel(params);
    if (!propertyName) {
      console.log(`Se termino el escaneo en la columna ${column}`);
      break;
    }
    if (propertyName === 'Not Specified' || propertyName === 'TOTAL') {
      continue;
    }
    const newProperty: IPropertyProfitsAndLossExcelInfo = {
      propertyName,
      dateInfo: date,
      income: getPropertyDataFromRowData(
        column,
        worksheet,
        sheetConcepts.income.investmentRevenue
      ),
      grossProfit: getPropertyDataFromRowData(
        column,
        worksheet,
        sheetConcepts.income.grossProfit
      ).value,
      expenses: getPropertyDataFromRowData(
        column,
        worksheet,
        sheetConcepts.expenses
      ),
      netInformation: {
        netIncome: getPropertyDataFromRowData(
          column,
          worksheet,
          sheetConcepts.netIncome.netIncome
        ),
        operatingIncome: getPropertyDataFromRowData(
          column,
          worksheet,
          sheetConcepts.netIncome.netOperatingIncome
        ),
        otherIncome: sheetConcepts.netIncome.otherIncomes
          .map(
            (
              otherIncomeRowMap: IProfitsAndLossRowConcept
            ): IPropertyProfitsAndLossConcept => {
              return getPropertyDataFromRowData(
                column,
                worksheet,
                otherIncomeRowMap
              );
            }
          )
          .filter((e) => e !== null),
      },
    };
    const propertyId: string = await bndPropertyRead.getPropertyIdByName(
      propertyName
    );
    if (propertyId) {
      newProperty.propertyId = propertyId;
    }
    propertiesProfitsAndLoss.push(newProperty);
  }
  return propertiesProfitsAndLoss;
};

const getPropertyDataFromRowData = (
  column: string,
  wS: WorkSheet,
  rowInfo: IProfitsAndLossRowConcept
): IPropertyProfitsAndLossConcept => {
  const propertyRowInfo: IPropertyProfitsAndLossConcept = {
    name: rowInfo.name,
    value: getNumberFromCellObject(wS[`${column}${rowInfo.row}`]),
  };
  const rowInfoDetails: IPropertyProfitsAndLossConcept[] = [];
  if (rowInfo.details) {
    for (const childrenRowInfo of rowInfo.details) {
      const propertyDetail: IPropertyProfitsAndLossConcept =
        getPropertyDataFromRowData(column, wS, childrenRowInfo);
      if (propertyDetail != null) {
        rowInfoDetails.push(propertyDetail);
      }
    }
  }
  if (rowInfoDetails.length > 0) {
    propertyRowInfo.details = rowInfoDetails;
    if (!rowInfo.total) {
      throw new BaseError(
        `Cannot fint total of property details`,
        500,
        'getIncomeRowInfo'
      );
    }
    propertyRowInfo.total = getNumberFromCellObject(
      wS[`${column}${rowInfo.total.row}`]
    );
  }
  const response =
    propertyRowInfo.value !== null || rowInfoDetails.length
      ? propertyRowInfo
      : null;
  return response;
};

const getSheetRowInformation = (
  worksheet: WorkSheet
): IProfitsAndLossSheetRowsInformation => {
  let currentRow: number = rowInfo.firstRowInformation;
  /* Income row information */
  const incomeData: IProfitsAndLossExpensesRowInformationResponse =
    getIncomeRowInfo(currentRow, worksheet);
  const income: IProfitsAndlossIncomeExcelRows = incomeData.income;
  currentRow = incomeData.currentRow;
  /* Expenses Row Information */
  const expensesInfo: IProfitsAndLossRowConceptResponse = getExpensesRowInfo(
    currentRow,
    worksheet
  );
  const expenses: IProfitsAndLossRowConcept = expensesInfo.concept;
  currentRow = expensesInfo.currentRow;
  const netIncome = getNetIncomeRowInfo(currentRow, worksheet);
  return {
    income,
    expenses,
    netIncome,
  };
};

const getNetIncomeRowInfo = (
  currentRow: number,
  wS: WorkSheet
): IProfitsAndlossNetIncomeExcelRowsInfo => {
  let netOperatingIncome: IProfitsAndLossRowConcept;
  let netIncome: IProfitsAndLossRowConcept;
  let otherIncomes: IProfitsAndLossRowConcept[] = [];
  let conceptName: string = getCVS(wS[`${rowInfo.column}${currentRow}`]);

  do {
    switch (conceptName.trim()) {
      /* Get Net Income Row */
      case rowInfo.netInfo.incomeName:
        netIncome = {
          name: conceptName.trim(),
          row: currentRow,
        };
        currentRow++;
        break;
      /* Get Net Operating Income Row */
      case rowInfo.netInfo.operatingIncome:
        netOperatingIncome = {
          name: conceptName.trim(),
          row: currentRow,
        };
        currentRow++;
        break;
      default:
        const otherIncomeData: IProfitsAndLossRowConceptResponse =
          getConceptsRowInformation(
            conceptName.trim(),
            currentRow,
            getRowLevel(conceptName),
            wS
          );
        currentRow = otherIncomeData.currentRow;
        otherIncomes.push(otherIncomeData.concept);
    }
    conceptName = getCVS(wS[`${rowInfo.column}${currentRow}`]);
  } while (conceptName !== null);

  return {
    netIncome,
    netOperatingIncome,
    otherIncomes,
  };
};

const getExpensesRowInfo = (
  currentRow: number,
  wS: WorkSheet
): IProfitsAndLossRowConceptResponse => {
  /* Expenses name Validation */
  const expensesName = getCVS(wS[`${rowInfo.column}${currentRow}`]);
  let rowLevel = getRowLevel(expensesName);
  if (expensesName.trim() !== rowInfo.expenses.name || rowLevel != 1) {
    throw new BaseError(
      `(ROW:${currentRow},Column:${rowInfo.column}): Cannot find concept "${rowInfo.expenses.name}"`,
      500,
      'getIncomeRowInfo'
    );
  }

  /* Expenses concepts validation */
  const expenses: IProfitsAndLossRowConceptResponse = getConceptsRowInformation(
    expensesName.trim(),
    currentRow,
    rowLevel,
    wS
  );
  return expenses;
};

//TODO improve the error messages in gral
const getIncomeRowInfo = (
  currentRow: number,
  wS: WorkSheet
): IProfitsAndLossExpensesRowInformationResponse => {
  let rowLevel: number;
  /* Income Name Validation*/
  const incomeName = getCVS(wS[`${rowInfo.column}${currentRow}`]);
  rowLevel = getRowLevel(incomeName);
  if (incomeName.trim() !== rowInfo.income.name || rowLevel != 1) {
    throw new BaseError('Cannot find income name row', 500, 'getIncomeRowInfo');
  }
  currentRow++;
  /* Investment Revenue Info */
  let conceptName: string = getCVS(wS[`${rowInfo.column}${currentRow}`]);
  rowLevel = getRowLevel(conceptName);
  const incomeConceptsInformation: IProfitsAndLossRowConceptResponse =
    getConceptsRowInformation(conceptName.trim(), currentRow, rowLevel, wS);
  const investmentRevenue = incomeConceptsInformation.concept;
  currentRow = incomeConceptsInformation.currentRow;
  /* Total Income name */
  const totalIncomeName: string = getCVS(wS[`${rowInfo.column}${currentRow}`]);
  rowLevel = getRowLevel(totalIncomeName);
  if (
    totalIncomeName.trim() !== `Total ${rowInfo.income.name}` ||
    rowLevel != 1
  ) {
    throw new BaseError('Cannot find the income total row', 500, null);
  }
  const totalIncome: IProfitsAndLossRowConcept = {
    name: totalIncomeName.trim(),
    row: currentRow,
  };
  currentRow++;
  /* Gross Profit information */
  const grossProfitName: string = getCVS(wS[`${rowInfo.column}${currentRow}`]);
  rowLevel = getRowLevel(grossProfitName);
  if (grossProfitName.trim() !== rowInfo.grossProfit.name || rowLevel != 1) {
    throw new BaseError(
      'Cannot find gross profit row',
      500,
      'getIncomeRowInfo'
    );
  }
  const grossProfit: IProfitsAndLossRowConcept = {
    name: grossProfitName.trim(),
    row: currentRow,
  };
  currentRow++;
  return {
    income: {
      grossProfit,
      investmentRevenue,
      totalIncome,
    },
    currentRow,
  };
};

const getConceptsRowInformation = (
  conceptName: string,
  currentRow: number,
  oldRowLevel: number,
  wS: WorkSheet
): IProfitsAndLossRowConceptResponse => {
  const newConcept: IProfitsAndLossRowConcept = {
    name: conceptName.trim(),
    row: currentRow,
  };
  currentRow = currentRow + 1;
  let newConceptName: string = getCVS(wS[`${rowInfo.column}${currentRow}`]);
  let rowLevel: number = getRowLevel(newConceptName);
  if (rowLevel > oldRowLevel) {
    let childConceptData: IProfitsAndLossRowConceptResponse;
    newConcept.details = [];
    while (rowLevel > oldRowLevel) {
      childConceptData = getConceptsRowInformation(
        newConceptName,
        currentRow,
        getRowLevel(newConceptName),
        wS
      );
      newConcept.details.push(childConceptData.concept);
      rowLevel = childConceptData.newRowLevel;
      currentRow = childConceptData.currentRow;
      newConceptName = childConceptData.newConceptName;
    }
    if (oldRowLevel < rowLevel) {
      throw new BaseError(
        `Cannot find the total of the concept ${conceptName}`,
        400,
        null
      );
    }
    const totalName: string = getCVS(wS[`${rowInfo.column}${currentRow}`]);
    newConcept.total = {
      name: totalName.trim(),
      row: currentRow,
    };
    currentRow = currentRow + 1;
    newConceptName = getCVS(wS[`${rowInfo.column}${currentRow}`]);
    rowLevel = getRowLevel(newConceptName);
  }
  return {
    concept: newConcept,
    newRowLevel: rowLevel,
    newConceptName: newConceptName,
    currentRow: currentRow,
  };
};
const getProfitsAndLossDateByExcel = (
  worksheet: WorkSheet
): IPropertyProfitsAndLossDateInfo => {
  const dateCell: CellObject =
    worksheet[`${profitsAndLossCellAddress.dateInfo.identification}`];
  if (dateCell) {
    const dateString: string = dateCell.v as string;
    const spaceIndex: number = dateString.indexOf(' ');
    if (spaceIndex < 0) {
      throw new BaseError(
        'Cannot read profit and loss date.',
        400,
        'getProfitsAndLossDateByExcel'
      );
    }
    const month: string = dateString.substring(0, spaceIndex);
    const year: string = dateString.substring(
      spaceIndex + 1,
      dateString.length
    );
    return {
      month,
      year,
    };
  }

  return {
    month: null,
    year: null,
  };
};

const getPropertyNameByExcel = (params: IExcelInformationParams): string => {
  const { column, worksheet } = params;
  const propertyName: CellObject =
    worksheet[`${column}${profitsAndLossCellAddress.propertiesStartInfo.row}`];
  return propertyName ? (propertyName.v as string) : null;
};

const getCVS /* Cell Value String*/ = (cellObject: CellObject): string => {
  return cellObject ? (cellObject.v as string) : null;
};

const getRowLevel = (e: string): number => {
  const spaces = e.length - e.trimLeft().length;
  let rowLevel = 1;
  if (spaces > 0) {
    if (spaces % 3 !== 0) {
      throw new BaseError('Row level error, no level by 3', 400, 'getRowLevel');
    }
    rowLevel += spaces / 3;
  }
  return rowLevel;
};

const getNumberFromCellObject = (cellObject: CellObject): number => {
  return cellObject ? (cellObject.v as number) : null;
};
