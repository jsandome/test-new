import {
  EnumPropertyTypes,
  IBaseEntityProperty,
  IBasePropertyExcelValidation,
  IBasePropertyLocation,
  IExcelReadParams,
  InterfacePropertyStrategies,
  IPropertyBaseError,
  exPropertyBaseGralInf,
  propertyExcelBaseTypes,
  IBaseSheetProjectionData,
  IPropertyProjection,
  propertyProjectionCellAddressInfo,
  ILoansByPropertyParams,
  IPropertyOperatingYear,
  IBaseEntityStates,
  IBaseEntityCounties,
  IBndBaseLocations,
} from '@capa/core';
import BigNumber from 'bignumber.js';
import * as XLSX from 'xlsx';
import Joi from 'joi';
import { DateTime } from 'luxon';

const gCM /* Get cell name */ = (c: number, r: number): string => {
  const response = `${XLSX.utils.encode_col(c)}${r}`;
  return response;
};

export const getColumnNumberByName = (columnName: string): number => {
  columnName = columnName.toUpperCase();
  let response: number = 0,
    len: number = columnName.length;
  for (let pos = 0; pos < len; pos++) {
    response += (columnName.charCodeAt(pos) - 64) * Math.pow(26, len - pos - 1);
  }
  return response;
};

/*Property Base functions and enums */

const validatePropertyBaseExcelObject = Joi.object({
  name: Joi.string()
    .min(3)
    .max(150)
    .required()
    .messages({
      'string.base': `(ROW:${exPropertyBaseGralInf.title}) Name of property should be a alphanum and cannot be empty`,
      'string.empty': `(ROW:${exPropertyBaseGralInf.title}) Name of the property cannot be empty`,
      'string.min': `(ROW:${exPropertyBaseGralInf.title}) Name should have a minimum length of {#limit}`,
      'string.max': `(ROW:${exPropertyBaseGralInf.title}) Name should have a max length of {#limit}`,
    }),

  address: Joi.string()
    .min(3)
    .max(150)
    .required()
    .messages({
      'string.base': `(ROW:${exPropertyBaseGralInf.address}) Address should be a string and cannot be empty`,
      'string.empty': `(ROW:${exPropertyBaseGralInf.address}) Address cannot be empty`,
      'string.min': `(ROW:${exPropertyBaseGralInf.address}) Address should have a minimum length of {#limit}`,
      'string.max': `(ROW:${exPropertyBaseGralInf.address}) Address should have a max length of {#limit}`,
    }),

  type: Joi.valid(...Object.values(EnumPropertyTypes))
    .required()
    .messages({
      'any.only': `(ROW:${
        exPropertyBaseGralInf.type
      }) Type must be one the next list [${Object.values(
        EnumPropertyTypes
      ).toString()}]`,
      'any.valid': `(ROW:${
        exPropertyBaseGralInf.type
      }) Type must be one the next list [${Object.values(
        EnumPropertyTypes
      ).toString()}]`,
      'any.empty': `(ROW:${exPropertyBaseGralInf.type}) Type cannot be empty`,
    }),
  state: Joi.string()
    .required()
    .messages({
      'any.base': `(ROW:${exPropertyBaseGralInf.state}) State must be an string`,
      'any.only': `(ROW:${exPropertyBaseGralInf.state}) State must be an string`,
      'any.empty': `(ROW:${exPropertyBaseGralInf.state}) State cannot be empty`,
    }),
  county: Joi.string()
    .required()
    .messages({
      'any.base': `(ROW:${exPropertyBaseGralInf.county}) County must be an string`,
      'any.only': `(ROW:${exPropertyBaseGralInf.county}) County must be an string`,
      'any.empty': `(ROW:${exPropertyBaseGralInf.county}) County cannot be empty`,
    }),
  bedRooms: Joi.number()
    .required()
    .messages({
      'number.base': `(ROW:${exPropertyBaseGralInf.bedrooms}) BedRooms must be a number`,
      'number.only': `(ROW:${exPropertyBaseGralInf.bedrooms}) BedRooms must be a number`,
      'number.empty': `(ROW:${exPropertyBaseGralInf.bedrooms}) BedRooms cannot be empty`,
    }),
  bathRooms: Joi.number()
    .required()
    .messages({
      'number.base': `(ROW:${exPropertyBaseGralInf.bathrooms}) BathRooms must be a number`,
      'number.only': `(ROW:${exPropertyBaseGralInf.bathrooms}) BathRooms must be a number`,
      'number.empty': `(ROW:${exPropertyBaseGralInf.bathrooms}) BathRooms cannot be empty`,
    }),
  size: Joi.number()
    .required()
    .messages({
      'number.base': `(ROW:${exPropertyBaseGralInf.size}) Size must be a number`,
      'number.only': `(ROW:${exPropertyBaseGralInf.size}) Size must be a number`,
      'number.empty': `(ROW:${exPropertyBaseGralInf.size}) Size cannot be empty`,
    }),
  parcelSize: Joi.number()
    .required()
    .messages({
      'number.base': `(ROW:${exPropertyBaseGralInf.parcelsize}) Parcel Size must be a number`,
      'number.only': `(ROW:${exPropertyBaseGralInf.parcelsize}) Parcel Size must be a number`,
      'number.empty': `(ROW:${exPropertyBaseGralInf.parcelsize}) Parcel Size cannot be empty`,
    }),
  parcelNumber: Joi.number()
    .required()
    .messages({
      'number.base': `(ROW:${exPropertyBaseGralInf.parcelnumber}) Parcel Number must be a number`,
      'number.only': `(ROW:${exPropertyBaseGralInf.parcelnumber}) Parcel Number must be a number`,
      'number.empty': `(ROW:${exPropertyBaseGralInf.parcelnumber}) Parcel Number cannot be empty`,
    }),
  strategies: Joi.object({
    buyAndHold: Joi.boolean()
      .required()
      .messages({
        'any.base': `(ROW:${exPropertyBaseGralInf.strategiesBuyAndHold}) Strategies-Buy and hold value not valid`,
        'any.only': `(ROW:${exPropertyBaseGralInf.strategiesBuyAndHold}) Strategies-Buy and hold value not valid`,
        'any.empty': `(ROW:${exPropertyBaseGralInf.strategiesBuyAndHold}) Error trying to read Strategies-Buy and hold`,
      }),
    repositionAndHold: Joi.boolean()
      .required()
      .messages({
        'any.base': `(ROW:${exPropertyBaseGralInf.strategiesRepositionAndHold}) Strategies-Reposition and hold value not valid`,
        'any.only': `(ROW:${exPropertyBaseGralInf.strategiesRepositionAndHold}) Strategies-Reposition and hold value not valid`,
        'any.empty': `(ROW:${exPropertyBaseGralInf.strategiesRepositionAndHold}) Error trying to read Strategies-Reposition and hold`,
      }),
    repositionAndSell: Joi.boolean()
      .required()
      .messages({
        'any.base': `(ROW:${exPropertyBaseGralInf.strategiesRepositionAndSell}) Strategies-Reposition and sell value not valid`,
        'any.only': `(ROW:${exPropertyBaseGralInf.strategiesRepositionAndSell}) Strategies-Reposition and sell value not valid`,
        'any.empty': `(ROW:${exPropertyBaseGralInf.strategiesRepositionAndSell}) Error trying to read Strategies-Reposition and Sell`,
      }),
    developAndHold: Joi.boolean()
      .required()
      .messages({
        'any.base': `(ROW:${exPropertyBaseGralInf.strategiesDevelopAndHold}) Strategies-Develop and hold value not valid`,
        'any.only': `(ROW:${exPropertyBaseGralInf.strategiesDevelopAndHold}) Strategies-Develop and hold value not valid`,
        'any.empty': `(ROW:${exPropertyBaseGralInf.strategiesDevelopAndHold}) Error trying to read Strategies-Develop and hold `,
      }),
    developAndSell: Joi.boolean()
      .required()
      .messages({
        'any.base': `(ROW:${exPropertyBaseGralInf.strategiesDevelopAndSell}) Strategies-Develop and sell value not valid`,
        'any.only': `(ROW:${exPropertyBaseGralInf.strategiesDevelopAndSell}) Strategies-Develop and sell value not valid`,
        'any.empty': `(ROW:${exPropertyBaseGralInf.strategiesDevelopAndSell}) Error trying to read Strategies-Develop and sell`,
      }),
    inmediateSell: Joi.boolean()
      .required()
      .messages({
        'any.base': `(ROW:${exPropertyBaseGralInf.strategiesInmediateSell}) Strategies-Inmediate Sell value not valid`,
        'any.only': `(ROW:${exPropertyBaseGralInf.strategiesInmediateSell}) Strategies-Inmediate Sell value not valid`,
        'any.empty': `(ROW:${exPropertyBaseGralInf.strategiesInmediateSell}) Error trying to read Strategies-Inmediate Sell`,
      }),
  }),
  propertyDescription: Joi.string()
    .min(3)
    .max(500)
    .required()
    .messages({
      'string.base': `(ROW:${exPropertyBaseGralInf.description}) Property description should be a string and cannot be empty`,
      'string.empty': `(ROW:${exPropertyBaseGralInf.description}) Property description cannot be empty`,
      'string.min': `(ROW:${exPropertyBaseGralInf.description}) Property description should have a minimum length of {#limit}`,
      'string.max': `(ROW:${exPropertyBaseGralInf.description}) Property description should have a max length of {#limit}`,
    }),

  location: Joi.object({
    lat: Joi.number()
      .min(-90)
      .max(90)
      .required()
      .messages({
        'number.base': `(ROW:${exPropertyBaseGralInf.locationLat}) Property Latitude is not a valid value`,
        'number.max': `(ROW:${exPropertyBaseGralInf.locationLat}) Property Latitude should have a max length of {#limit}`,
        'number.min': `(ROW:${exPropertyBaseGralInf.locationLat}) Property Latitude should have a min length of {#limit}`,
        'number.empty': `(ROW:${exPropertyBaseGralInf.locationLat}) Property Latitude cannot be empty`,
      }),
    lng: Joi.number()
      .min(-180)
      .max(80)
      .messages({
        'number.base': `(ROW:${exPropertyBaseGralInf.locationLng}) Property Longitude is not a valid value`,
        'number.max': `(ROW:${exPropertyBaseGralInf.locationLng}) Property Longitude should have a max length of {#limit}`,
        'number.min': `(ROW:${exPropertyBaseGralInf.locationLng}) Property Longitude should have a min length of {#limit}`,
        'number.empty': `(ROW:${exPropertyBaseGralInf.locationLng}) Property Longitude cannot be empty`,
      }),
  }).required(),
  excelImportInformation: Joi.object({
    column: Joi.string().required(),
    excelName: Joi.string().required(),
    importDate: Joi.string().required(),
    isValid: Joi.boolean().required(),
  }).error(
    () =>
      'There was an error reading the excel information, please contact support'
  ),
});

export const getPropertiesByExcel = async (
  worksheet: XLSX.WorkSheet,
  options: IExcelReadParams
): Promise<IBaseEntityProperty[]> => {
  const properties: IBaseEntityProperty[] = [];
  for (
    let col = new BigNumber(
      exPropertyBaseGralInf.propertiesStartColumn
    ).toNumber();
    col < 702 /*ZZ*/;
    col++
  ) {
    const title: string = getPropertyBaseExcelInfo(
      worksheet,
      col,
      propertyExcelBaseTypes.TITLE
    ) as string;
    if (!title) break;
    const description: string = getPropertyBaseExcelInfo(
      worksheet,
      col,
      propertyExcelBaseTypes.DESCRIPTION
    ) as string;
    const address: string = getPropertyBaseExcelInfo(
      worksheet,
      col,
      propertyExcelBaseTypes.ADDRESS
    ) as string;
    const type: EnumPropertyTypes = getPropertyBaseExcelInfo(
      worksheet,
      col,
      propertyExcelBaseTypes.TYPE
    ) as EnumPropertyTypes;
    const state: string = getPropertyBaseExcelInfo(
      worksheet,
      col,
      propertyExcelBaseTypes.STATE
    ) as string;
    const county: string = getPropertyBaseExcelInfo(
      worksheet,
      col,
      propertyExcelBaseTypes.COUNTY
    ) as string;
    const bedRooms: number = getPropertyBaseExcelInfo(
      worksheet,
      col,
      propertyExcelBaseTypes.BEDROOMS
    ) as number;
    const bathRooms: number = getPropertyBaseExcelInfo(
      worksheet,
      col,
      propertyExcelBaseTypes.BATHROOMS
    ) as number;
    const size: number = getPropertyBaseExcelInfo(
      worksheet,
      col,
      propertyExcelBaseTypes.SIZE
    ) as number;
    const parcelSize: number = getPropertyBaseExcelInfo(
      worksheet,
      col,
      propertyExcelBaseTypes.PARCELSIZE
    ) as number;
    const parcelNumber: number = getPropertyBaseExcelInfo(
      worksheet,
      col,
      propertyExcelBaseTypes.PARCELNUMBER
    ) as number;
    const location: IBasePropertyLocation = getPropertyBaseExcelInfo(
      worksheet,
      col,
      propertyExcelBaseTypes.LOCATION
    ) as IBasePropertyLocation;
    const strategies: InterfacePropertyStrategies = getPropertyBaseExcelInfo(
      worksheet,
      col,
      propertyExcelBaseTypes.STRATEGIES
    ) as InterfacePropertyStrategies;
    const property: IBaseEntityProperty = {
      name: title,
      address,
      type,
      state,
      county,
      bedRooms,
      bathRooms,
      size,
      parcelSize,
      parcelNumber,
      strategies,
      propertyDescription: description,
      location,
      excelImportInformation: {
        column: XLSX.utils.encode_col(col),
        excelName: options.fileName,
        importDate: DateTime.now().toISO(),
        isValid: true,
      },
    };
    properties.push(property);
  }

  return properties;
};

export const validateExcelInformationAndGetLocations = async (
  inputProperties: IBaseEntityProperty[],
  duplicatedProperties: IBasePropertyExcelValidation[],
  bndLocationsRead: IBndBaseLocations
): Promise<IPropertyBaseError[]> => {
  const propertiesByName = {};
  const propertyNamesErrors = {};
  const propertiesByAddress = {};
  const propertyAddressErrors = {};
  /*Structure,locations and duplicity in excel itsel validation, also save locationss information*/
  const propertiesErrors = {};

  for (const property of inputProperties) {
    const validation: Joi.ValidationResult =
      validatePropertyBaseExcelObject.validate(property, { abortEarly: false });
    if (validation.error) {
      const { error } = validation;
      propertiesErrors[`${property.excelImportInformation.column}`] = {
        column: property.excelImportInformation.column,
        errors: error.details.map((det) => det.message),
      };
    }

    /* not duplication in the excel itsel validation */
    /*Name*/
    if (propertiesByName[`${property.name}`]) {
      if (propertyNamesErrors[`${property.name}`]) {
        propertyNamesErrors[`${property.name}`].push(property);
      } else {
        propertyNamesErrors[`${property.name}`] = [
          propertiesByName[`${property.name}`],
          property,
        ];
      }
    } else {
      propertiesByName[`${property.name}`] = property; //This name will be used in the validation of the not duplicated in database
    }

    /*Address*/
    if (propertiesByAddress[`${property.address}`]) {
      if (propertyAddressErrors[`${property.address}`]) {
        propertyAddressErrors[`${property.address}`].push(property);
      } else {
        propertyAddressErrors[`${property.address}`] = [
          propertiesByAddress[`${property.address}`],
          property,
        ];
      }
    } else {
      propertiesByAddress[`${property.address}`] = property;
    }
    /* State and county validation */
    const stateInfo: IBaseEntityStates = await bndLocationsRead.getStateByName(
      property.state as string
    );
    if (!stateInfo) {
      const stateError: string = `(ROW:${exPropertyBaseGralInf.state}) State not found in the database`;
      if (propertiesErrors[`${property.excelImportInformation.column}`]) {
        propertiesErrors[
          `${property.excelImportInformation.column}`
        ].errors.push(stateError);
      } else {
        propertiesErrors[`${property.excelImportInformation.column}`] = {
          column: property.excelImportInformation.column,
          errors: [stateError],
        };
      }
    } else {
      property.state = stateInfo as IBaseEntityStates;
      const countyInfo: IBaseEntityCounties =
        await bndLocationsRead.getCountyByNameAndState(
          property.county as string,
          stateInfo.id
        );
      if (!countyInfo) {
        const countyError: string = `(ROW:${exPropertyBaseGralInf.county}) County not found in the database, be sure the name is correct and the belongs to the state`;
        if (propertiesErrors[`${property.excelImportInformation.column}`]) {
          propertiesErrors[
            `${property.excelImportInformation.column}`
          ].errors.push(countyError);
        } else {
          propertiesErrors[`${property.excelImportInformation.column}`] = {
            column: property.excelImportInformation.column,
            errors: [countyError],
          };
        }
      } else {
        property.county = countyInfo as IBaseEntityCounties;
      }
    }
  }

  /* Not duplicated in the excel itsel validation */
  /* Names */
  for (const key in propertyNamesErrors) {
    const duplicatedNameColums: string = propertyNamesErrors[key].reduce(
      (p: string, c: IBaseEntityProperty) => {
        return `${p.length > 0 ? `${p}, ` : ''}${
          c.excelImportInformation.column
        }`;
      },
      ''
    );
    const pivotColumn: string =
      propertyNamesErrors[key][0].excelImportInformation.column;
    const nameErrorMessage: string = `(ROW:${exPropertyBaseGralInf.title}) The name "${key}" is duplicated in the properties in the columns: ${duplicatedNameColums}`;
    if (propertiesErrors[pivotColumn]) {
      propertiesErrors[pivotColumn].errors.push(nameErrorMessage);
    } else {
      propertiesErrors[pivotColumn] = {
        column: pivotColumn,
        errors: [nameErrorMessage],
      };
    }
  }

  /* Address */
  for (const key in propertyAddressErrors) {
    const duplicatedAddressColumns: string = propertyAddressErrors[key].reduce(
      (p: string, c: IBaseEntityProperty) => {
        return `${p.length > 0 ? `${p}, ` : ''}${
          c.excelImportInformation.column
        }`;
      },
      ''
    );
    const addressErrorMessage: string = `(ROW:${exPropertyBaseGralInf.address}) The address "${key}" is duplicated in the properties in the columns: ${duplicatedAddressColumns}`;
    const pivotColumn: string =
      propertyAddressErrors[key][0].excelImportInformation.column;
    if (propertiesErrors[pivotColumn]) {
      propertiesErrors[pivotColumn].errors.push(addressErrorMessage);
    } else {
      propertiesErrors[pivotColumn] = {
        column: pivotColumn,
        errors: [addressErrorMessage],
      };
    }
  }

  /* Not duplicated (name and address) in database validation */
  for (const duplicatedProperty of duplicatedProperties) {
    const propertyByName = propertiesByName[`${duplicatedProperty.name}`];
    if (propertyByName) {
      const error: string = `(ROW:${exPropertyBaseGralInf.title}) Title of the property column ${propertyByName.excelImportInformation.column} is already in the database`;
      if (propertiesErrors[`${propertyByName.excelImportInformation.column}`]) {
        propertiesErrors[
          `${propertyByName.excelImportInformation.column}`
        ].errors.push(error);
      } else {
        propertiesErrors[`${propertyByName.excelImportInformation.column}`] = {
          column: propertyByName.excelImportInformation.column,
          errors: [error],
        };
      }
    }
    const propertyByAddress =
      propertiesByAddress[`${duplicatedProperty.address}`];
    if (propertyByAddress) {
      const error: string = `(ROW:${exPropertyBaseGralInf.address}) Address of the property column ${propertyByAddress.excelImportInformation.column} is already in the database`;
      if (
        propertiesErrors[`${propertyByAddress.excelImportInformation.column}`]
      ) {
        propertiesErrors[
          `${propertyByAddress.excelImportInformation.column}`
        ].errors.push(error);
      } else {
        propertiesErrors[`${propertyByAddress.excelImportInformation.column}`] =
          {
            column: propertyByAddress.excelImportInformation.column,
            errors: [error],
          };
      }
    }
  }

  const response: IPropertyBaseError[] = [];
  for (const propColumn in propertiesErrors) {
    const property = inputProperties.find(
      (e) => e.excelImportInformation.column == propColumn
    );
    property.excelImportInformation.isValid = false;
    response.push(propertiesErrors[`${propColumn}`]);
  }
  return response.length ? response : null;
};

const getPropertyBaseExcelInfo = (
  worksheet: XLSX.WorkSheet,
  col: number,
  type: propertyExcelBaseTypes
):
  | string
  | number
  | IBasePropertyLocation
  | Boolean
  | InterfacePropertyStrategies
  | EnumPropertyTypes => {
  let response: XLSX.CellObject;
  switch (type) {
    case propertyExcelBaseTypes.TITLE:
      response = worksheet[gCM(col, exPropertyBaseGralInf.title)];
      return response ? (response.v as string) : null;
    case propertyExcelBaseTypes.DESCRIPTION:
      response = worksheet[gCM(col, exPropertyBaseGralInf.description)];
      return response ? (response.v as string) : null;
    case propertyExcelBaseTypes.ADDRESS:
      response = worksheet[gCM(col, exPropertyBaseGralInf.address)];
      return response ? (response.v as string) : null;
    case propertyExcelBaseTypes.TYPE:
      response = worksheet[gCM(col, exPropertyBaseGralInf.type)];
      return response ? (response.v as EnumPropertyTypes) : null;
    case propertyExcelBaseTypes.STATE:
      response = worksheet[gCM(col, exPropertyBaseGralInf.state)];
      return response ? (response.v as string) : null;
    case propertyExcelBaseTypes.COUNTY:
      response = worksheet[gCM(col, exPropertyBaseGralInf.county)];
      return response ? (response.v as string) : null;
    case propertyExcelBaseTypes.BEDROOMS:
      response = worksheet[gCM(col, exPropertyBaseGralInf.bedrooms)];
      return response ? (response.v as number) : null;
    case propertyExcelBaseTypes.BATHROOMS:
      response = worksheet[gCM(col, exPropertyBaseGralInf.bathrooms)];
      return response ? (response.v as number) : null;
    case propertyExcelBaseTypes.SIZE:
      response = worksheet[gCM(col, exPropertyBaseGralInf.size)];
      return response ? (response.v as number) : null;
    case propertyExcelBaseTypes.PARCELSIZE:
      response = worksheet[gCM(col, exPropertyBaseGralInf.parcelsize)];
      return response ? (response.v as number) : null;
    case propertyExcelBaseTypes.PARCELNUMBER:
      response = worksheet[gCM(col, exPropertyBaseGralInf.parcelnumber)];
      return response ? (response.v as number) : null;
    case propertyExcelBaseTypes.LOCATION:
      const lat: XLSX.CellObject =
        worksheet[gCM(col, exPropertyBaseGralInf.locationLat)];
      const lng: XLSX.CellObject =
        worksheet[gCM(col, exPropertyBaseGralInf.locationLng)];
      const location: IBasePropertyLocation = {
        lat: lat ? (lat.v as string) : null,
        lng: lng ? (lng.v as string) : null,
      };
      return location as IBasePropertyLocation;
    case propertyExcelBaseTypes.STRATEGIES:
      const strategiesBuyAndHold: XLSX.CellObject =
        worksheet[gCM(col, exPropertyBaseGralInf.strategiesBuyAndHold)];
      const strategiesDevelopAndHold: XLSX.CellObject =
        worksheet[gCM(col, exPropertyBaseGralInf.strategiesDevelopAndHold)];
      const strategiesDevelopAndSell: XLSX.CellObject =
        worksheet[gCM(col, exPropertyBaseGralInf.strategiesDevelopAndSell)];
      const strategiesInmediateSell: XLSX.CellObject =
        worksheet[gCM(col, exPropertyBaseGralInf.strategiesInmediateSell)];
      const strategiesRepositionAndHold: XLSX.CellObject =
        worksheet[gCM(col, exPropertyBaseGralInf.strategiesRepositionAndHold)];
      const strategiesRepositionAndSell: XLSX.CellObject =
        worksheet[gCM(col, exPropertyBaseGralInf.strategiesRepositionAndSell)];
      const strategies: InterfacePropertyStrategies = {
        buyAndHold: strategiesBuyAndHold ? true : false,
        developAndHold: strategiesDevelopAndHold ? true : false,
        developAndSell: strategiesDevelopAndSell ? true : false,
        inmediateSell: strategiesInmediateSell ? true : false,
        repositionAndHold: strategiesRepositionAndHold ? true : false,
        repositionAndSell: strategiesRepositionAndSell ? true : false,
      };
      return strategies;
  }
};

/*Property Projection functions and objects*/

const operativeYearValidation: Joi.ObjectSchema = Joi.object({
  year: Joi.number()
    .min(0)
    .max(999999999)
    .required()
    .messages({
      'number.base': `ROW:${propertyProjectionCellAddressInfo.incomeRowInformation.year}) Operative info year should be a number and cannot be empty`,
      'number.empty': `ROW:${propertyProjectionCellAddressInfo.incomeRowInformation.year}) Operative info year cannot be empty`,
      'number.min': `ROW:${propertyProjectionCellAddressInfo.incomeRowInformation.year}) Operative info year must be greater than {#limit}`,
      'number.max': `ROW:${propertyProjectionCellAddressInfo.incomeRowInformation.year}) Operative info year have a max value of {#limit}`,
    }),
  rent: Joi.number()
    .min(0)
    .max(999999999)
    .required()
    .messages({
      'number.base': `ROW:${propertyProjectionCellAddressInfo.incomeRowInformation.rentRoll}) Operative info rent should be a number and cannot be empty`,
      'number.empty': `ROW:${propertyProjectionCellAddressInfo.incomeRowInformation.rentRoll}) Operative info rent cannot be empty`,
      'number.min': `ROW:${propertyProjectionCellAddressInfo.incomeRowInformation.rentRoll}) Operative info rent must be greater than {#limit}`,
      'number.max': `ROW:${propertyProjectionCellAddressInfo.incomeRowInformation.rentRoll}) Operative info rent have a max value of {#limit}`,
    }),
  vacancy: Joi.number()
    .min(0)
    .max(999999999)
    .required()
    .messages({
      'number.base': `ROW:${propertyProjectionCellAddressInfo.incomeRowInformation.vacancy}) Operative info vacancy should be a number and cannot be empty`,
      'number.empty': `ROW:${propertyProjectionCellAddressInfo.incomeRowInformation.vacancy}) Operative info vacancy cannot be empty`,
      'number.min': `ROW:${propertyProjectionCellAddressInfo.incomeRowInformation.vacancy}) Operative info vacancy must be greater than {#limit}`,
      'number.max': `ROW:${propertyProjectionCellAddressInfo.incomeRowInformation.vacancy}) Operative info vacancy have a max value of {#limit}`,
    }),
  propertyManagement: Joi.number()
    .min(0)
    .max(999999999)
    .required()
    .messages({
      'number.base': `ROW:${propertyProjectionCellAddressInfo.incomeRowInformation.propertyManagement}) Operative info property management should be a number and cannot be empty`,
      'number.empty': `ROW:${propertyProjectionCellAddressInfo.incomeRowInformation.propertyManagement}) Operative info property management cannot be empty`,
      'number.min': `ROW:${propertyProjectionCellAddressInfo.incomeRowInformation.propertyManagement}) Operative info property management must be greater than {#limit}`,
      'number.max': `ROW:${propertyProjectionCellAddressInfo.incomeRowInformation.propertyManagement}) Operative info property management have a max value of {#limit}`,
    }),
  assetManagement: Joi.number()
    .min(0)
    .max(999999999)
    .required()
    .messages({
      'number.base': `ROW:${propertyProjectionCellAddressInfo.incomeRowInformation.assetmanagement}) Operative asset management year should be a number and cannot be empty`,
      'number.empty': `ROW:${propertyProjectionCellAddressInfo.incomeRowInformation.assetmanagement}) Operative asset management year cannot be empty`,
      'number.min': `ROW:${propertyProjectionCellAddressInfo.incomeRowInformation.assetmanagement}) Operative info asset management must be greater than {#limit}`,
      'number.max': `ROW:${propertyProjectionCellAddressInfo.incomeRowInformation.assetmanagement}) Operative info asset management have a max value of {#limit}`,
    }),
  taxes: Joi.number()
    .min(0)
    .max(999999999)
    .required()
    .messages({
      'number.base': `ROW:${propertyProjectionCellAddressInfo.incomeRowInformation.taxes}) Operative info taxes should be a number and cannot be empty`,
      'number.empty': `ROW:${propertyProjectionCellAddressInfo.incomeRowInformation.taxes}) Operative info taxes cannot be empty`,
      'number.min': `ROW:${propertyProjectionCellAddressInfo.incomeRowInformation.taxes}) Operative info taxes must be greater than {#limit}`,
      'number.max': `ROW:${propertyProjectionCellAddressInfo.incomeRowInformation.taxes}) Operative info taxes have a max value of {#limit}`,
    }),
  insurance: Joi.number()
    .min(0)
    .max(999999999)
    .required()
    .messages({
      'number.base': `ROW:${propertyProjectionCellAddressInfo.incomeRowInformation.insurance}) Operative info insurance should be a number and cannot be empty`,
      'number.empty': `ROW:${propertyProjectionCellAddressInfo.incomeRowInformation.insurance}) Operative info insurance cannot be empty`,
      'number.min': `ROW:${propertyProjectionCellAddressInfo.incomeRowInformation.insurance}) Operative info insurance must be greater than {#limit}`,
      'number.max': `ROW:${propertyProjectionCellAddressInfo.incomeRowInformation.insurance}) Operative info insurance have a max value of {#limit}`,
    }),
  repairs: Joi.number()
    .min(0)
    .max(999999999)
    .required()
    .messages({
      'number.base': `ROW:${propertyProjectionCellAddressInfo.incomeRowInformation.repairs}) Operative info repairs should be a number and cannot be empty`,
      'number.empty': `ROW:${propertyProjectionCellAddressInfo.incomeRowInformation.repairs}) Operative info repairs cannot be empty`,
      'number.min': `ROW:${propertyProjectionCellAddressInfo.incomeRowInformation.repairs}) Operative info repairs must be greater than {#limit}`,
      'number.max': `ROW:${propertyProjectionCellAddressInfo.incomeRowInformation.repairs}) Operative info repairs have a max value of {#limit}`,
    }),
  reserve: Joi.number()
    .min(0)
    .max(999999999)
    .required()
    .messages({
      'number.base': `ROW:${propertyProjectionCellAddressInfo.incomeRowInformation.reserve}) Operative info reserve should be a number and cannot be empty`,
      'number.empty': `ROW:${propertyProjectionCellAddressInfo.incomeRowInformation.reserve}) Operative info reserve cannot be empty`,
      'number.min': `ROW:${propertyProjectionCellAddressInfo.incomeRowInformation.reserve}) Operative info reserve must be greater than {#limit}`,
      'number.max': `ROW:${propertyProjectionCellAddressInfo.incomeRowInformation.reserve}) Operative info reserve have a max value of {#limit}`,
    }),
  units: Joi.number()
    .min(0)
    .max(999999999)
    .required()
    .messages({
      'number.base': `ROW:${propertyProjectionCellAddressInfo.incomeRowInformation.units}) Operative info units should be a number and cannot be empty`,
      'number.empty': `ROW:${propertyProjectionCellAddressInfo.incomeRowInformation.units}) Operative info units cannot be empty`,
      'number.min': `ROW:${propertyProjectionCellAddressInfo.incomeRowInformation.units}) Operative info units must be greater than {#limit}`,
      'number.max': `ROW:${propertyProjectionCellAddressInfo.incomeRowInformation.units}) Operative info units have a max value of {#limit}`,
    }),
  salesPrice: Joi.number()
    .min(0)
    .max(999999999)
    .required()
    .messages({
      'number.base': `ROW:${propertyProjectionCellAddressInfo.incomeRowInformation.salesPrice}) Operative info salesPrice should be a number and cannot be empty`,
      'number.empty': `ROW:${propertyProjectionCellAddressInfo.incomeRowInformation.salesPrice}) Operative info salesPrice cannot be empty`,
      'number.min': `ROW:${propertyProjectionCellAddressInfo.incomeRowInformation.salesPrice}) Operative info salesPrice must be greater than {#limit}`,
      'number.max': `ROW:${propertyProjectionCellAddressInfo.incomeRowInformation.salesPrice}) Operative info salesPrice have a max value of {#limit}`,
    }),
  extraExpenses: Joi.array()
    .optional()
    .items(
      Joi.object({
        name: Joi.string().min(1).max(100).required(),
        years: Joi.array().items(
          Joi.object({
            year: Joi.number().min(0).max(999999999).required(),
            amount: Joi.number().min(0).max(999999999).required(),
          })
        ),
      })
    ),
  excelImportInformation: Joi.object({
    column: Joi.string().min(1).max(10).required(),
  }),
});

const validatePropertyProjectionExcel: Joi.ObjectSchema = Joi.object({
  propertyName: Joi.string()
    .min(0)
    .max(150)
    .required()
    .messages({
      'string.base': `(COLUMN:${propertyProjectionCellAddressInfo.propertyName.column},ROW:${propertyProjectionCellAddressInfo.propertyName.row}) Name of property should be a alphanum and cannot be empty`,
      'string.empty': `(COLUMN:${propertyProjectionCellAddressInfo.propertyName.column},ROW:${propertyProjectionCellAddressInfo.propertyName.row}) Name of the property cannot be empty`,
      'string.min': `(COLUMN:${propertyProjectionCellAddressInfo.propertyName.column},ROW:${propertyProjectionCellAddressInfo.propertyName.row}) Name should have a minimum length of {#limit}`,
      'string.max': `(COLUMN:${propertyProjectionCellAddressInfo.propertyName.column},ROW:${propertyProjectionCellAddressInfo.propertyName.row}) Name should have a max length of {#limit}`,
    }),
  propertyId: Joi.string().min(0).max(150).optional().messages({
    'string.base': `Error reading the information of the property that the projection belongs`,
    'string.empty': `Error reading the information of the property that the projection belongs`,
    'string.min': `Error reading the information of the property that the projection belongs`,
    'string.max': `Error reading the information of the property that the projection belongs`,
  }),
  totalValue: Joi.number()
    .min(0)
    .max(999999999999999)
    .required()
    .messages({
      'number.base': `(COLUMN:${propertyProjectionCellAddressInfo.value.totalValue.column},ROW:${propertyProjectionCellAddressInfo.value.totalValue.row}) Total value should be a number and cannot be empty`,
      'number.empty': `(COLUMN:${propertyProjectionCellAddressInfo.value.totalValue.column},ROW:${propertyProjectionCellAddressInfo.value.totalValue.row}) Total value cannot be empty`,
      'number.min': `(COLUMN:${propertyProjectionCellAddressInfo.value.totalValue.column},ROW:${propertyProjectionCellAddressInfo.value.totalValue.row}) Total value must be greater than {#limit}`,
      'number.max': `(COLUMN:${propertyProjectionCellAddressInfo.value.totalValue.column},ROW:${propertyProjectionCellAddressInfo.value.totalValue.row}) Total value have a max value of {#limit}`,
    }),
  purchasePrice: Joi.number()
    .min(0)
    .max(999999999999999)
    .required()
    .messages({
      'number.base': `(COLUMN:${propertyProjectionCellAddressInfo.value.purchasePrice.column},ROW:${propertyProjectionCellAddressInfo.value.purchasePrice.row}) Purchase price should be a number and cannot be empty`,
      'number.empty': `(COLUMN:${propertyProjectionCellAddressInfo.value.purchasePrice.column},ROW:${propertyProjectionCellAddressInfo.value.purchasePrice.row}) Purchase price cannot be empty`,
      'number.min': `(COLUMN:${propertyProjectionCellAddressInfo.value.purchasePrice.column},ROW:${propertyProjectionCellAddressInfo.value.purchasePrice.row}) Purchase price must be greater than {#limit}`,
      'number.max': `(COLUMN:${propertyProjectionCellAddressInfo.value.purchasePrice.column},ROW:${propertyProjectionCellAddressInfo.value.purchasePrice.row}) Purchase price have a max value of {#limit}`,
    }),

  aquisitionFee: Joi.number()
    .min(0)
    .max(999999999999999)
    .required()
    .messages({
      'number.base': `(COLUMN:${propertyProjectionCellAddressInfo.exitStrategy.acquisitionFee.column},ROW:${propertyProjectionCellAddressInfo.exitStrategy.acquisitionFee.row}) Acquisition Fee should be a number and cannot be empty`,
      'number.empty': `(COLUMN:${propertyProjectionCellAddressInfo.exitStrategy.acquisitionFee.column},ROW:${propertyProjectionCellAddressInfo.exitStrategy.acquisitionFee.row}) Acquisition Fee cannot be empty`,
      'number.min': `(COLUMN:${propertyProjectionCellAddressInfo.exitStrategy.acquisitionFee.column},ROW:${propertyProjectionCellAddressInfo.exitStrategy.acquisitionFee.row}) Acquisition Fee must be greater than {#limit}`,
      'number.max': `(COLUMN:${propertyProjectionCellAddressInfo.exitStrategy.acquisitionFee.column},ROW:${propertyProjectionCellAddressInfo.exitStrategy.acquisitionFee.row}) Acquisition Fee have a max value of {#limit}`,
    }),
  dispositionFee: Joi.number()
    .min(0)
    .max(100)
    .required()
    .messages({
      'number.base': `(COLUMN:${propertyProjectionCellAddressInfo.exitStrategy.dispositionFee.column},ROW:${propertyProjectionCellAddressInfo.exitStrategy.dispositionFee.column}) Disposition Fee should be a number and cannot be empty`,
      'number.empty': `(COLUMN:${propertyProjectionCellAddressInfo.exitStrategy.dispositionFee.column},ROW:${propertyProjectionCellAddressInfo.exitStrategy.dispositionFee.column}) Disposition Fee cannot be empty`,
      'number.min': `(COLUMN:${propertyProjectionCellAddressInfo.exitStrategy.dispositionFee.column},ROW:${propertyProjectionCellAddressInfo.exitStrategy.dispositionFee.column}) Disposition Fee must be greater than {#limit}`,
      'number.max': `(COLUMN:${propertyProjectionCellAddressInfo.exitStrategy.dispositionFee.column},ROW:${propertyProjectionCellAddressInfo.exitStrategy.dispositionFee.column}) Disposition Fee have a max value of {#limit}`,
    }),
  amountAllocatedToLand: Joi.number()
    .min(0)
    .max(999999999999999)
    .required()
    .messages({
      'number.base': `(COLUMN:${propertyProjectionCellAddressInfo.exitStrategy.amountAllocatedToLand.column},ROW:${propertyProjectionCellAddressInfo.exitStrategy.amountAllocatedToLand.row}) Amount allocated to land should be a number and cannot be empty`,
      'number.empty': `(COLUMN:${propertyProjectionCellAddressInfo.exitStrategy.amountAllocatedToLand.column},ROW:${propertyProjectionCellAddressInfo.exitStrategy.amountAllocatedToLand.row}) Amount allocated to land cannot be empty`,
      'number.min': `(COLUMN:${propertyProjectionCellAddressInfo.exitStrategy.amountAllocatedToLand.column},ROW:${propertyProjectionCellAddressInfo.exitStrategy.amountAllocatedToLand.row}) Amount allocated to land must be greater than {#limit}`,
      'number.max': `(COLUMN:${propertyProjectionCellAddressInfo.exitStrategy.amountAllocatedToLand.column},ROW:${propertyProjectionCellAddressInfo.exitStrategy.amountAllocatedToLand.row}) Amount allocated to land have a max value of {#limit}`,
    }),
  depreciation: Joi.number()
    .min(0)
    .max(999999999999999)
    .required()
    .messages({
      'number.base': `(COLUMN:${propertyProjectionCellAddressInfo.exitStrategy.depreciation.column},ROW:${propertyProjectionCellAddressInfo.exitStrategy.depreciation.row}) Depreciation should be a number and cannot be empty`,
      'number.empty': `(COLUMN:${propertyProjectionCellAddressInfo.exitStrategy.depreciation.column},ROW:${propertyProjectionCellAddressInfo.exitStrategy.depreciation.row}) Depreciation cannot be empty`,
      'number.min': `(COLUMN:${propertyProjectionCellAddressInfo.exitStrategy.depreciation.column},ROW:${propertyProjectionCellAddressInfo.exitStrategy.depreciation.row}) Depreciation must be greater than {#limit}`,
      'number.max': `(COLUMN:${propertyProjectionCellAddressInfo.exitStrategy.depreciation.column},ROW:${propertyProjectionCellAddressInfo.exitStrategy.depreciation.row}) Depreciation have a max value of {#limit}`,
    }),
  commissions: Joi.number()
    .min(0)
    .max(100)
    .required()
    .messages({
      'number.base': `(COLUMN:${propertyProjectionCellAddressInfo.exitStrategy.commissions.column},ROW:${propertyProjectionCellAddressInfo.exitStrategy.commissions.row}) Commissions should be a number and cannot be empty`,
      'number.empty': `(COLUMN:${propertyProjectionCellAddressInfo.exitStrategy.commissions.column},ROW:${propertyProjectionCellAddressInfo.exitStrategy.commissions.row}) Commissions cannot be empty`,
      'number.min': `(COLUMN:${propertyProjectionCellAddressInfo.exitStrategy.commissions.column},ROW:${propertyProjectionCellAddressInfo.exitStrategy.commissions.row}) Commissions must be greater than 0  {#limit}`,
      'number.max': `(COLUMN:${propertyProjectionCellAddressInfo.exitStrategy.commissions.column},ROW:${propertyProjectionCellAddressInfo.exitStrategy.commissions.row}) Commissions have a max value of {#limit}`,
    }),
  downPayment: Joi.number()
    .min(0)
    .max(999999999999999)
    .required()
    .messages({
      'number.base': `(COLUMN:${propertyProjectionCellAddressInfo.exitStrategy.downPayment.column},ROW:${propertyProjectionCellAddressInfo.exitStrategy.downPayment.row}) Down payment should be a number and cannot be empty`,
      'number.empty': `(COLUMN:${propertyProjectionCellAddressInfo.exitStrategy.downPayment.column},ROW:${propertyProjectionCellAddressInfo.exitStrategy.downPayment.row}) Down payment cannot be empty`,
      'number.min': `(COLUMN:${propertyProjectionCellAddressInfo.exitStrategy.downPayment.column},ROW:${propertyProjectionCellAddressInfo.exitStrategy.downPayment.row}) Down payment must be greater than {#limit}`,
      'number.max': `(COLUMN:${propertyProjectionCellAddressInfo.exitStrategy.downPayment.column},ROW:${propertyProjectionCellAddressInfo.exitStrategy.downPayment.row}) Down payment have a max value of {#limit}`,
    }),
  capitalGainsTaxPaid: Joi.number()
    .min(0)
    .max(999999999999999)
    .required()
    .messages({
      'number.base': `(COLUMN:${propertyProjectionCellAddressInfo.exitStrategy.capitalGainsTaxPaid.column},ROW:${propertyProjectionCellAddressInfo.exitStrategy.capitalGainsTaxPaid.row}) Capital gains tax paid should be a number and cannot be empty`,
      'number.empty': `(COLUMN:${propertyProjectionCellAddressInfo.exitStrategy.capitalGainsTaxPaid.column},ROW:${propertyProjectionCellAddressInfo.exitStrategy.capitalGainsTaxPaid.row}) Capital gains tax paid cannot be empty`,
      'number.min': `(COLUMN:${propertyProjectionCellAddressInfo.exitStrategy.capitalGainsTaxPaid.column},ROW:${propertyProjectionCellAddressInfo.exitStrategy.capitalGainsTaxPaid.row}) Capital gains tax paid must be greater than {#limit}`,
      'number.max': `(COLUMN:${propertyProjectionCellAddressInfo.exitStrategy.capitalGainsTaxPaid.column},ROW:${propertyProjectionCellAddressInfo.exitStrategy.capitalGainsTaxPaid.row}) Capital gains tax paid have a max value of {#limit}`,
    }),

  loans: Joi.array().items(
    Joi.object({
      amount: Joi.number()
        .min(0)
        .max(999999999999999)
        .required()
        .messages({
          'number.base': `(COLUMN:${propertyProjectionCellAddressInfo.projectedLoan.amount.column},ROW:${propertyProjectionCellAddressInfo.projectedLoan.amount.row}) Loan amount should be a number and cannot be empty`,
          'number.empty': `(COLUMN:${propertyProjectionCellAddressInfo.projectedLoan.amount.column},ROW:${propertyProjectionCellAddressInfo.projectedLoan.amount.row}) Loan amount cannot be empty`,
          'number.min': `(COLUMN:${propertyProjectionCellAddressInfo.projectedLoan.amount.column},ROW:${propertyProjectionCellAddressInfo.projectedLoan.amount.row}) Loan amount must be greater than {#limit}`,
          'number.max': `(COLUMN:${propertyProjectionCellAddressInfo.projectedLoan.amount.column},ROW:${propertyProjectionCellAddressInfo.projectedLoan.amount.row}) Loan amount have a max value of {#limit}`,
        }),
      interest: Joi.number()
        .min(0)
        .max(100)
        .required()
        .messages({
          'number.base': `(COLUMN:${propertyProjectionCellAddressInfo.projectedLoan.interest.column},ROW:${propertyProjectionCellAddressInfo.projectedLoan.interest.row}) Loan interest should be a number and cannot be empty`,
          'number.empty': `(COLUMN:${propertyProjectionCellAddressInfo.projectedLoan.interest.column},ROW:${propertyProjectionCellAddressInfo.projectedLoan.interest.row}) Loan interest cannot be empty`,
          'number.min': `(COLUMN:${propertyProjectionCellAddressInfo.projectedLoan.interest.column},ROW:${propertyProjectionCellAddressInfo.projectedLoan.interest.row}) Loan interest must be greater than {#limit}`,
          'number.max': `(COLUMN:${propertyProjectionCellAddressInfo.projectedLoan.interest.column},ROW:${propertyProjectionCellAddressInfo.projectedLoan.interest.row}) Loan interest have a max value of {#limit}`,
        }),
      termYears: Joi.number()
        .min(0)
        .max(100)
        .required()
        .messages({
          'number.base': `(COLUMN:${propertyProjectionCellAddressInfo.projectedLoan.loanTerms.column},ROW:${propertyProjectionCellAddressInfo.projectedLoan.loanTerms.row}) Term Years should be a number and cannot be empty`,
          'number.empty': `(COLUMN:${propertyProjectionCellAddressInfo.projectedLoan.loanTerms.column},ROW:${propertyProjectionCellAddressInfo.projectedLoan.loanTerms.row}) Term Years cannot be empty`,
          'number.min': `(COLUMN:${propertyProjectionCellAddressInfo.projectedLoan.loanTerms.column},ROW:${propertyProjectionCellAddressInfo.projectedLoan.loanTerms.row}) Term Years must be greater than {#limit}`,
          'number.max': `(COLUMN:${propertyProjectionCellAddressInfo.projectedLoan.loanTerms.column},ROW:${propertyProjectionCellAddressInfo.projectedLoan.loanTerms.row}) Term Years have a max value of {#limit}`,
        }),
      paymentsPerYear: Joi.number().min(0).max(100).optional().messages({
        'number.base': `(Cell:N/A) Payments per year should be a number and cannot be empty`,
        'number.min': `(Cell:N/A) Payments per year must be greater than {#limit}`,
        'number.max': `(Cell:N/A) Payments per year have a max value of {#limit}`,
      }),
      amortizationYears: Joi.number()
        .min(0)
        .max(100)
        .optional()
        .messages({
          'number.base': `(COLUMN:${propertyProjectionCellAddressInfo.projectedLoan.amortizationyears.column},ROW:${propertyProjectionCellAddressInfo.projectedLoan.amortizationyears.row}) Amortization years should be a number and cannot be empty`,
          'number.empty': `(COLUMN:${propertyProjectionCellAddressInfo.projectedLoan.amortizationyears.column},ROW:${propertyProjectionCellAddressInfo.projectedLoan.amortizationyears.row}) Amortization years cannot be empty`,
          'number.min': `(COLUMN:${propertyProjectionCellAddressInfo.projectedLoan.amortizationyears.column},ROW:${propertyProjectionCellAddressInfo.projectedLoan.amortizationyears.row}) Amortization years must be greater than {#limit}`,
          'number.max': `(COLUMN:${propertyProjectionCellAddressInfo.projectedLoan.amortizationyears.column},ROW:${propertyProjectionCellAddressInfo.projectedLoan.amortizationyears.row}) Amortization years have a max value of {#limit}`,
        }),
    })
  ),
  // /*Operative info */
  operativeInfo: Joi.array().items(operativeYearValidation).allow(null),
});

export const getProjectionInformation = async (
  worksheet: XLSX.WorkSheet,
  excelProperties: IBaseEntityProperty[],
  options: IExcelReadParams
): Promise<IBaseSheetProjectionData> => {
  const errorsProyection: string[] = [];
  const { bndPropertyRead } = options;
  const [totalValue, purchasePrice] = getProyectionValue(worksheet);
  const [
    aquisitionFee,
    dispositionFee,
    amountAllocatedToLand,
    depreciation,
    commissions,
    downPayment,
    capitalGainsTaxPaid,
  ] = getExitStrategy(worksheet);
  const loan: ILoansByPropertyParams = getExcelLoan(worksheet);
  const propertyName: string = getProyectionPropertyName(worksheet);
  const savedPropertyId: string = await bndPropertyRead.getPropertyIdByName(
    propertyName
  );
  const projection: IPropertyProjection = {
    propertyName: propertyName,
    /*Value */
    totalValue,
    purchasePrice,
    /*Exist Strategy */
    aquisitionFee,
    dispositionFee,
    amountAllocatedToLand,
    depreciation,
    commissions,
    downPayment,
    capitalGainsTaxPaid,
    /*Projected loans*/
    loans: [loan],
    /*Operative info */
    operativeInfo /*Income*/: null,
  };

  /* Validate the name of property in the projection  */
  if (savedPropertyId) {
    projection.propertyId = savedPropertyId;
  } else {
    /* Validate the name property */
    const indexPropertyName = excelProperties.findIndex(
      (e) => e.name === propertyName
    );
    if (indexPropertyName == -1) {
      errorsProyection.push(
        `(COLUMN:${propertyProjectionCellAddressInfo.propertyName.column},ROW:${propertyProjectionCellAddressInfo.propertyName.row}) Cannot find the property name in the database or the properties uploaded`
      );
    }
  }
  /* Validate the object without operative info*/
  const validation: Joi.ValidationResult =
    validatePropertyProjectionExcel.validate(projection, { abortEarly: false });
  if (validation.error) {
    const { error } = validation;
    errorsProyection.push(...error.details.map((det) => det.message));
  }
  const operativeInfoYears: IPropertyOperatingYear[] =
    getOperativeInfo(worksheet);
  projection.operativeInfo = operativeInfoYears;
  /* Validate the operative years*/
  for (const operativeYear of operativeInfoYears) {
    const yearValidation: Joi.ValidationResult =
      operativeYearValidation.validate(operativeYear, { abortEarly: false });
    if (yearValidation.error) {
      const { error } = yearValidation;
      errorsProyection.push(
        ...error.details.map(
          (det) =>
            `(COLUMN:${operativeYear.excelImportInformation.column},${det.message}`
        )
      );
    }
  }

  return {
    sheetName: options.SheetName,
    errors: errorsProyection.length === 0 ? null : errorsProyection,
    projection: projection,
  };
};

const getProyectionPropertyName = (worksheet: XLSX.WorkSheet): string => {
  const response =
    worksheet[propertyProjectionCellAddressInfo.propertyName.identification];
  return response ? response.v : null;
};

const getProyectionValue = (worksheet: XLSX.WorkSheet): number[] => {
  const totalValue =
    worksheet[
      propertyProjectionCellAddressInfo.value.totalValue.identification
    ];
  const purchasePrice =
    worksheet[
      propertyProjectionCellAddressInfo.value.purchasePrice.identification
    ];
  return [
    totalValue ? (totalValue.v as number) : null,
    purchasePrice ? (purchasePrice.v as number) : null,
  ];
};

const getExitStrategy = (worksheet: XLSX.WorkSheet): number[] => {
  const aquisitionFee =
    worksheet[
      propertyProjectionCellAddressInfo.exitStrategy.acquisitionFee
        .identification
    ];
  const dispositionFee =
    worksheet[
      propertyProjectionCellAddressInfo.exitStrategy.dispositionFee
        .identification
    ];
  const amountAllocatedToLand =
    worksheet[
      propertyProjectionCellAddressInfo.exitStrategy.amountAllocatedToLand
        .identification
    ];
  const depreciation =
    worksheet[
      propertyProjectionCellAddressInfo.exitStrategy.depreciation.identification
    ];
  const commissions =
    worksheet[
      propertyProjectionCellAddressInfo.exitStrategy.commissions.identification
    ];
  const downPayment =
    worksheet[
      propertyProjectionCellAddressInfo.exitStrategy.downPayment.identification
    ];
  const capitalGainsTaxPaid =
    worksheet[
      propertyProjectionCellAddressInfo.exitStrategy.capitalGainsTaxPaid
        .identification
    ];
  return [
    aquisitionFee ? (aquisitionFee.v as number) : null,
    dispositionFee ? (dispositionFee.v as number) : null,
    amountAllocatedToLand ? (amountAllocatedToLand.v as number) : null,
    depreciation ? (depreciation.v as number) : null,
    commissions ? (commissions.v as number) : null,
    downPayment ? (downPayment.v as number) : null,
    capitalGainsTaxPaid ? (capitalGainsTaxPaid.v as number) : null,
  ];
};

const getExcelLoan = (worksheet: XLSX.WorkSheet): ILoansByPropertyParams => {
  const amount: XLSX.CellObject =
    worksheet[
      propertyProjectionCellAddressInfo.projectedLoan.amount.identification
    ];
  const interest: XLSX.CellObject =
    worksheet[
      propertyProjectionCellAddressInfo.projectedLoan.interest.identification
    ];
  const amortizationYears: XLSX.CellObject =
    worksheet[
      propertyProjectionCellAddressInfo.projectedLoan.amortizationyears
        .identification
    ];
  const loanTerms: XLSX.CellObject =
    worksheet[
      propertyProjectionCellAddressInfo.projectedLoan.loanTerms.identification
    ];
  return {
    amount: amount ? (amount.v as number) : null,
    interest: interest ? (interest.v as number) : null,
    termYears: loanTerms ? (loanTerms.v as number) : null,
    amortizationYears: amortizationYears
      ? (amortizationYears.v as number)
      : null,
  };
};

const getOperativeInfo = (
  worksheet: XLSX.WorkSheet
): IPropertyOperatingYear[] => {
  const response: IPropertyOperatingYear[] = [];
  for (let i = 0; i < 10; i++) {
    const column = XLSX.utils.encode_col(
      propertyProjectionCellAddressInfo.incomeRowInformation.begginingColumn
        .columnNumber + i
    );
    const year = getPIncomeYearCell(worksheet, column);
    if (!year) break;
    const units = getPIncomeUnits(worksheet, column);
    const rentRoll = getPIncomeRentRoll(worksheet, column);
    const salesPrice = getPIncomeSalesPrice(worksheet, column);
    const propertyManagement = getPIncomePropertyManagement(worksheet, column);
    const insurance = getPIncomeInsurance(worksheet, column);
    const assetManagement = getPIncomeAssetmanagement(worksheet, column);
    const reserve = getPIncomeReserve(worksheet, column);
    const repairs = getPIncomeRepairs(worksheet, column);
    const vacancy = getPIncomeVacancy(worksheet, column);
    const taxes = getPIncomeTaxes(worksheet, column);

    response.push({
      year,
      rent: rentRoll,
      salesPrice: salesPrice,
      vacancy,
      propertyManagement,
      assetManagement,
      taxes,
      insurance,
      repairs,
      reserve,
      units,
      extraExpenses: [],
      excelImportInformation: { column: column },
    });
  }
  return response;
};

const getPIncomeYearCell = (
  worksheet: XLSX.WorkSheet,
  column: string
): number => {
  const year: XLSX.CellObject =
    worksheet[
      `${column}${propertyProjectionCellAddressInfo.incomeRowInformation.year}`
    ];
  return year ? (year.v as number) : null;
};

const getPIncomeUnits = (worksheet: XLSX.WorkSheet, column: string): number => {
  const units: XLSX.CellObject =
    worksheet[
      `${column}${propertyProjectionCellAddressInfo.incomeRowInformation.units}`
    ];
  return units ? (units.v as number) : null;
};
const getPIncomeRentRoll = (
  worksheet: XLSX.WorkSheet,
  column: string
): number => {
  const rentRoll: XLSX.CellObject =
    worksheet[
      `${column}${propertyProjectionCellAddressInfo.incomeRowInformation.rentRoll}`
    ];
  return rentRoll ? (rentRoll.v as number) : null;
};
const getPIncomeSalesPrice = (
  worksheet: XLSX.WorkSheet,
  column: string
): number => {
  const salesPrice: XLSX.CellObject =
    worksheet[
      `${column}${propertyProjectionCellAddressInfo.incomeRowInformation.salesPrice}`
    ];
  return salesPrice ? (salesPrice.v as number) : null;
};
const getPIncomePropertyManagement = (
  worksheet: XLSX.WorkSheet,
  column: string
): number => {
  const propertyManagement: XLSX.CellObject =
    worksheet[
      `${column}${propertyProjectionCellAddressInfo.incomeRowInformation.propertyManagement}`
    ];
  return propertyManagement ? (propertyManagement.v as number) : null;
};
const getPIncomeInsurance = (
  worksheet: XLSX.WorkSheet,
  column: string
): number => {
  const insurance: XLSX.CellObject =
    worksheet[
      `${column}${propertyProjectionCellAddressInfo.incomeRowInformation.insurance}`
    ];
  return insurance ? (insurance.v as number) : null;
};
const getPIncomeAssetmanagement = (
  worksheet: XLSX.WorkSheet,
  column: string
): number => {
  const assetmanagement: XLSX.CellObject =
    worksheet[
      `${column}${propertyProjectionCellAddressInfo.incomeRowInformation.assetmanagement}`
    ];
  return assetmanagement ? (assetmanagement.v as number) : null;
};
const getPIncomeReserve = (
  worksheet: XLSX.WorkSheet,
  column: string
): number => {
  const reserve: XLSX.CellObject =
    worksheet[
      `${column}${propertyProjectionCellAddressInfo.incomeRowInformation.reserve}`
    ];
  return reserve ? (reserve.v as number) : null;
};
const getPIncomeRepairs = (
  worksheet: XLSX.WorkSheet,
  column: string
): number => {
  const repairs: XLSX.CellObject =
    worksheet[
      `${column}${propertyProjectionCellAddressInfo.incomeRowInformation.repairs}`
    ];
  return repairs ? (repairs.v as number) : null;
};
const getPIncomeVacancy = (
  worksheet: XLSX.WorkSheet,
  column: string
): number => {
  const vacancy: XLSX.CellObject =
    worksheet[
      `${column}${propertyProjectionCellAddressInfo.incomeRowInformation.vacancy}`
    ];
  return vacancy ? (vacancy.v as number) : null;
};
const getPIncomeTaxes = (worksheet: XLSX.WorkSheet, column: string): number => {
  const taxes: XLSX.CellObject =
    worksheet[
      `${column}${propertyProjectionCellAddressInfo.incomeRowInformation.taxes}`
    ];
  return taxes ? (taxes.v as number) : null;
};
