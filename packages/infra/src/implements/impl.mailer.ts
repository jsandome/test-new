import { inject, injectable } from 'inversify';
import {
  BASETYPES,
  IBaseLogger,
  IBndMailerInfra,
  IBndDataMail,
  FrontendEnvironment,
} from '@capa/core';
import { MailerConfig } from '../mailer/mailer-config';
import { GCloudConfig } from '../gcloud-config';

@injectable()
export class ImplBndMailerInfra implements IBndMailerInfra {
  @inject(BASETYPES.Log) private _log: IBaseLogger;
  @inject(BASETYPES.FrontendEnvironment) private _frontURL: FrontendEnvironment;

  constructor(private mailer: MailerConfig, private gCloud: GCloudConfig) {}

  public async send(dataToMail: IBndDataMail): Promise<void> {
    const { to, subject, template, data, attachments } = dataToMail;

    // Send a mail with link
    if (data.url) {
      data.url = `${this._frontURL.url}${data.url}`;
    }
    data.bucket = this.gCloud.publicUrl;

    this.mailer.setOptions(to, subject, template, data);
    if (attachments) {
      this.mailer.setAttachments(attachments);
    }

    const urlTemplate = this.mailer.templateDir + template;
    const html = await this.mailer.templateEnginee.render(urlTemplate, data);

    this.mailer.transportConfig.html = html;
    this._log.info(`Sending mail to: ${to} to server ${data.url}`);

    try {
      return new Promise((resolve, reject) => {
        if (process.env.NODE_ENV === 'TEST') {
          this._log.info('Test mode enabled, emails will not send it');
          resolve();
        } else {
          this.mailer.transportMailer.sendMail(
            this.mailer.transportConfig,
            (err, res) => {
              if (err) {
                reject(err);
              }
              resolve(res);
            }
          );
        }
      });
    } catch (error) {
      const msgError = 'I can not send the mail';
      this._log.info(msgError);
    }
  }
}
