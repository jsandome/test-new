import { inject, injectable } from 'inversify';
import * as jwt from 'jsonwebtoken';
import * as bcrypt from 'bcrypt';
import crypto from 'crypto';
import { DateTime } from 'luxon';
import {
  BaseError,
  BASETYPES,
  IBndBaseTokenInfra,
  IEnumEncoding,
  IJwtEnvironment,
  IPasswordParams,
  IEnumAlgorithm,
  IBaseExpireDataTokenOut,
  IJwtParamsTokenIn,
} from '@capa/core';
import { FirebaseConfig } from '../firebase-config';
@injectable()
export class ImplBndBaseTokenInfra implements IBndBaseTokenInfra {
  constructor(
    @inject(BASETYPES.IJwtConfig) private _config: IJwtEnvironment,
    private fb: FirebaseConfig
  ) {}

  public generateCode(): string {
    return crypto.randomBytes(3).toString(IEnumEncoding.HEX).toUpperCase();
  }
  public async validJwtToken(token: string): Promise<IJwtParamsTokenIn> {
    try {
      const decoded = jwt.verify(token, this._config.secretWord);
      return decoded as IJwtParamsTokenIn;
    } catch (error) {
      throw new BaseError('Invalid token', 401, 'VerifyTokenError', error);
    }
  }
  public generateSecure(userId: string): IBaseExpireDataTokenOut {
    const hash: string = crypto
      .createHash(IEnumAlgorithm.SHA256)
      .update(`${userId}${DateTime.now().toMillis()}`)
      .digest(IEnumEncoding.HEX);

    const expireDate: Date = DateTime.now().plus({ minutes: 5 }).toJSDate();

    return {
      expireDateToken: expireDate,
      expireToken: hash,
    } as IBaseExpireDataTokenOut;
  }
  public generateExpireDataToken(userId: string): IBaseExpireDataTokenOut {
    const hash: string = crypto
      .createHash(IEnumAlgorithm.SHA256)
      .update(`${userId}${DateTime.now().toMillis()}`)
      .digest(IEnumEncoding.HEX);

    const expireDate: Date = DateTime.now()
      .plus({ days: this._config.expireDaysToken })
      .toJSDate();

    return {
      expireDateToken: expireDate,
      expireToken: hash,
    } as IBaseExpireDataTokenOut;
  }

  public async generateToken(data: IJwtParamsTokenIn): Promise<string> {
    return jwt.sign(
      { id: data.id, email: data.email, type: data.type },
      this._config.secretWord,
      {
        expiresIn: this._config.sessionTime,
        algorithm: IEnumAlgorithm.HS512,
      }
    );
  }

  public async generateHash(password: string): Promise<string> {
    const salt = await bcrypt.genSalt(10);
    return await bcrypt.hash(password, salt);
  }

  public async validPassword(passwordData: IPasswordParams): Promise<boolean> {
    return await bcrypt.compare(
      passwordData.inputPassword,
      passwordData.hashPassword
    );
  }
  public async generateTokenFireBase(user: string): Promise<string> {
    const data = await this.fb.generateTokenByUser(user);
    return data;
  }
}
