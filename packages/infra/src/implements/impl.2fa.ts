import { injectable } from 'inversify';
import * as speakeasy from 'speakeasy';
import * as QRCode from 'qrcode';
import {
  BaseError,
  IJwtParamsTokenIn,
  IBnd2FAInfra,
  I2FAKeys,
} from '@capa/core';
@injectable()
export class ImplBnd2FAInfra implements IBnd2FAInfra {
  public validCode(secret: string, token: string): boolean {
    return speakeasy.totp.verify({
      secret,
      encoding: 'ascii',
      token,
    });
  }
  public async createCode(params: IJwtParamsTokenIn): Promise<I2FAKeys> {
    try {
      const secretCode = speakeasy.generateSecret({
        name: `CAPA(${params.email})`,
      });
      const qrCode = await QRCode.toDataURL(secretCode.otpauth_url);
      return {
        secret: secretCode.ascii,
        base32: secretCode.base32,
        hex: secretCode.hex,
        otpauthUrl: secretCode.otpauth_url,
        qrCode,
      };
    } catch (err) {
      throw new BaseError('Error trying to create 2FA', err.code, err);
    }
  }
}
