import { inject, injectable } from 'inversify';
import { BASETYPES, IBaseLogger } from '@capa/core';

@injectable()
export class DocusingConfig {
  constructor(@inject(BASETYPES.Log) private _log: IBaseLogger) {}
  public _init(): void {
    this._log.info('Docusing Service Init');
  }
}
