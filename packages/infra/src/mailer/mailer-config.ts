import { inject, injectable } from 'inversify';
import {
  BASETYPES,
  IBaseLogger,
  IEmailEnvironment,
  IBndAttachments,
  IBndExtraData,
  BaseError,
  IBndDataMail,
} from '@capa/core';
import * as nodemailer from 'nodemailer';
import Email from 'email-templates';
import * as fs from 'fs';
import * as path from 'path';

@injectable()
export class MailerConfig {
  public templateDir: string;
  public smtpConfig;
  public transportMailer;
  public templateEnginee;
  public options;
  public transportConfig;

  constructor(
    @inject(BASETYPES.IMailerConfig) private _config: IEmailEnvironment,
    @inject(BASETYPES.Log) private _log: IBaseLogger
  ) {}

  /**
   * Set default config to send any mail
   */
  public _init(): void {
    this.templateDir = path.resolve(`${process.cwd()}/../infra/src/templates/`);
    this.smtpConfig = this._smtpConfig();
    this.transportMailer = nodemailer.createTransport(this.smtpConfig);
    this.templateEnginee = this._templateEnginee();

    this._log.info('Setting Node mailer config');
  }

  /** Set required data to send mail
   * @to
   * @subject subject to the remitent
   * @template selected template to send the mail
   * @data extra data into the template
   * @attachments ? extra attachments to send in the mail
   */
  public setOptions(
    to: string,
    subject: string,
    template: string,
    data: IBndExtraData
  ): void {
    this.options = {
      data: data,
      template: this.templateDir + template,
    };

    this.transportConfig = {
      from: this._config.smtpUser,
      to,
      subject,
    };
  }

  /**
   * @attachments extra attachments to send in the mail
   */
  public setAttachments(attachments: Array<IBndAttachments>): void {
    const validFiles = this._validateDocuments(attachments);

    if (validFiles) {
      this.transportConfig.attachments = attachments;
    } else {
      throw new BaseError(
        'Some file is not found',
        404,
        'MailerConfig | setAttachments'
      );
    }
  }

  public getOptions(): IBndDataMail {
    return this.options;
  }

  private async _validateDocuments(
    files: Array<IBndAttachments>
  ): Promise<boolean> {
    const notValid = files.map((e) => !fs.existsSync(e.path));
    return notValid.length == 0 ? true : false;
  }

  /**
   *
   * @returns json with credentials to node mailer
   */
  private _smtpConfig() {
    return {
      host: this._config.smtpHost,
      port: this._config.smtpPort,
      secure: true,
      auth: {
        user: this._config.smtpUser,
        pass: this._config.smtpPassword,
      },
      tls: {
        ciphers: 'SSLv3',
      },
    };
  }

  private _templateEnginee(): Email {
    return new Email({
      views: {
        options: {
          extension: 'pug',
        },
      },
    });
  }
}
