import { Storage, Bucket } from '@google-cloud/storage';
import { BASETYPES, IBaseLogger, IGCloudEnvironment } from '@capa/core';
import { inject, injectable } from 'inversify';

@injectable()
export class GCloudConfig {
  public defaultBucket: Bucket;
  public storage: Storage;
  public bucketName: string;
  public publicUrl: string;

  constructor(
    @inject(BASETYPES.Log) private _log: IBaseLogger,
    @inject(BASETYPES.IGCloudConfig) private _config: IGCloudEnvironment
  ) {}

  public _init(): void {
    this.storage = this._storage();
    this.defaultBucket = this.storage.bucket(this._config.bucket);
    this.bucketName = this._config.bucket;
    this.publicUrl = `https://${this.bucketName}.storage.googleapis.com`;
  }

  private _storage = (): Storage => {
    this._log.info(`Connecting to GCS and load default bucket`);

    return new Storage({
      credentials: {
        private_key: this._config.private_key,
        client_email: this._config.client_email,
      },
    });
  };
}
