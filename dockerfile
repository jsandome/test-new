FROM node:14-alpine
ENV PORT=9999
# Create app directory
WORKDIR /usr/src/app

#install python 

ENV PYTHONUNBUFFERED=1
RUN apk add --update --no-cache python3 && ln -sf python3 /usr/bin/python
RUN python3 -m ensurepip
RUN pip3 install --no-cache --upgrade pip setuptools
RUN apk add --update make
RUN apk add --update gcc

# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
# where available (npm@5+)
COPY package*.json ./

RUN npm ci 
#RUN npm install --save bcrypt-nodejs && npm uninstall --save bcrypt
#--only=production
# If you are building your code for production
# RUN npm ci --only=production

# Bundle app source
COPY . .

RUN npm run init
RUN npm run build
EXPOSE ${PORT}
CMD [ "npm", "run", "start" ]
