const scanner = require('sonarqube-scanner');

scanner(
  {
    // this example uses local instance of SQ
    serverUrl: 'http://localhost:9000/',
    options: {
      'sonar.projectVersion': '1.1.0',
      'sonar.projectKey': '07b7cb660068a9cdbec0e5755ce7b659666e4277',
      'sonar.sources': 'api/src,core/src,data/src,infra/src,test/src',
      // "sonar.tests": "test/src",
      // "sonar.typescript.lcov.reportPaths": "coverage/lcov.info",
      // "sonar.testExecutionReportPaths": "test-report.xml"
    },
  },
  () => {
    // callback is required
  }
);
