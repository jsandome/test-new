# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

Compatible with the documentation version [`1.0.0`](https://gitlab.com/grupojaque/capa/capa-specs)

## [`unreleased`]

## Fixes

- fix: change understable message in create package
- Create endpoint to validate if the server is working

## [`v0.9.0`](https://gitlab.com/grupojaque/capa/capa-be/-/compare/v0.8.0...v0.9.0) - 2021-10-26

## Chore

- Add validation to send error when missing properties or projections
- Add test unitaries for step one endpoint

## Fixes

- Added field for list investors
- Query topic was fixed and a field was added to the schema contribution
- Change deleted contribution msg
- Change msg for udpate capital call
- Fixed get list contributions
- Resolve overview issues
- The query issue was fixed and addition processes were removed
- Resolve comments the other developer
- Changes query capital call
- Add validation on onboarding
- Changes query and code in Capital call
- Added typing response in Capital Call
- Add required params to distribution list endpoint

## Features

- Update info in step One
- Add property profits and loss save module
- Endpoints to get a contribution
- Add google authenticator
- Add endpoints to create and list payments
- Add models of payments

## [`v0.8.0`](https://gitlab.com/grupojaque/capa/capa-be/-/compare/v0.7.0...v0.8.0) - 2021-10-07

## Chore

- Change operative info projections messages
- Implement standar in ientity names
- Remove only in test

## Fixes

- Add missing validations for properties and projections upload by excel
- Change id for distribution id
- Improve messages generation in schema validation for property projections
- Add an extra condition for the distribution list
- Add active field for deletion logical in distributions.
- Add extra query to validate distrubitions in mongo schema
- Add investor attribute to request in update contribution

## Features

- Endpoints to get, update and delete a distribution

## [`v0.7.0`](https://gitlab.com/grupojaque/capa/capa-be/-/compare/v0.6.0...v0.7.0) - 2021-10-05

## Chore

- save properties and property projection by excel
- docusing send envelope for investor

## Features

- add endpoints to create and list distributions and distribution section
- add validations for excel upload: duplicated name, name already existing in excel or plataform
- add endpoint to delete contribution
- add endpoint to get envelopeId of DocuSign

## Fixes

- change url for list contribution
- fix to return correct investor id
- change version the DocuSign
- fixed docusign template list issue

## [`v0.6.0`](https://gitlab.com/grupojaque/capa/capa-be/-/compare/v0.5.0...v0.6.0) - 2021-09-30

## Features

- Endpoint to get detail for a Capital Call
- endpoint to get a capital call
- add feat to upload excel and get preview data
- get list of capital calls
- endpoint to select capital call
- endpoint to create/update contributions
- add validations for the excel upload data

## Fixes

- improve property projection schema error messages
- fix property, add is complete statement when save property projection

## [`v0.5.0`](https://gitlab.com/grupojaque/capa/capa-be/-/compare/v0.4.0...v0.5.0) - 2021-09-24

## Chore

- delete initial investment of the property projection

## Features

- update a package document

## [`v0.4.0`](https://gitlab.com/grupojaque/capa/capa-be/-/compare/v0.3.0...v0.4.0) - 2021-09-22

## Chore

- change get property endpoint to get property projection and not property finantial
- add v1 of validations in property preview

## Feautures

- endpoint to create a capital call
- get packageDocusing by id and all
- add endpoints to update and delete a capital call

## Fixes

- fix property projection id save on property
- set payments per year as optional
- fix payments per year issue with save property
- fix capital gains tax paid
- change minor fix to endpoint preview llc

## [`v0.3.0`](https://gitlab.com/grupojaque/capa/capa-be/-/compare/v0.2.0...v0.3.0) - 2021-09-21

## Features

- Endpoint to delete a docusing package

## Fixes

- Fix spelling issue with management
- Add id to properties in response endpoint getDetails for llc

## [`v0.2.0`](https://gitlab.com/grupojaque/capa/capa-be/-/compare/v0.1.0...v0.2.0) - 2021-09-20

### Chore

- Change property projection extra expenses structure

### Features

- create models, interfaces and all files needed to add docusingpackage
- add docusing check, validation and unitaries test

### Fixes

- Change validations of share for fund into a class, this allow export the methods
- add method to calculate share correctly in endpoint preview packages
- set loan-ltc as optional in property preview
- fchange dburi creation behavior and delete transaction config in migration

## [`v0.1.0`](https://gitlab.com/grupojaque/capa/capa-be/-/compare/v0.0.0...v0.2.0) - 2021-09-17

### Features

- Add schemas for property projection and save projection
- Add loan projection model in property projection
- Add models and interfaces for property projection and adjust some models name
- Add property capital gaints tax paid as param endpoint to get preview of a fund
- Change multi family residential for just multi family in the enum label and text

### Fixes

- Add params to select packages
- Add name param to detail of package
- Response getDetails for llc
- return correct id for investors
- change the size of name create/update package
